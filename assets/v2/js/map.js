$( "#activitydate" ).datepicker({
	maxDate: "D",
	minDate: "-1M",
  dateFormat: "dd-mm-yy"
});

$( function() {
  var currentTime = new Date();
  var startDateTo = new Date(currentTime.getFullYear(),currentTime.getMonth() -6,1);
  
  var dateFormat = "dd-mm-yy",
    from = $( "#activitydatestart" )
      .datepicker({
        dateFormat: "dd-mm-yy",
        //defaultDate: "+1M",
        changeMonth: true,
        changeYear: true,
        numberOfMonths: 2,
        minDate: startDateTo,
        maxDate: "0D"
      })
      .on( "change", function() {
        to.datepicker( "option", "minDate", getDate( this ) );
      }),
    to = $( "#activitydateend" ).datepicker({
      dateFormat: "dd-mm-yy",
      defaultDate: "+1M",
      changeMonth: true,
      changeYear: true,
      numberOfMonths: 2,
      minDate: "-15D",
      maxDate: "0D"
    })
    .on( "change", function() {
      from.datepicker( "option", "maxDate", getDate( this ) );
    });

  function getDate( element ) {
    var date;
    try {
      date = $.datepicker.parseDate( dateFormat, element.value );
    } catch( error ) {
      date = null;
    }
    return date;
  }
});
