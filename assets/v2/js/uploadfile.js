function upload_bill(cid, ctype)
{
	$("#file_name").val('');
	$("#err_up").html('');
	$("#claimid").val('');
	$("#uploadpodModal").modal("show");
	$("#fclaimid").val(cid);
	$("#fclaimtype").val(ctype);
}

$(document).ready(function () {
	
	/*$('.viewfile').click(function(e) {
    e.preventDefault();  //stop the browser from following
    var fname = $(this).attr('href');
    window.location.href = fname;	//'uploads/file.doc';
	});*/
	
	$("#uploadfile").click(function (event) {
		//stop submit the form, we will post it manually.
	  event.preventDefault();
		
		var claim_id = $("#fclaimid").val();
		// Get form
		var form = $('#frmupload')[0];
		// Create an FormData object 
		var data = new FormData(form);
		// If you want to add an extra field for the FormData
		//data.append("CustomField", "This is some extra data, testing");
		//disabled the submit button
		$("#uploadfile").prop("disabled", true);
		$.ajax({
				type: "POST",
				enctype: 'multipart/form-data',
				url: BASE_URL+"map/upload_claim_bill",
				data: data,
				processData: false,
				contentType: false,
				cache: false,
				timeout: 600000,
				success: function (data) {
					var returnedData = JSON.parse(data);
					if(returnedData.error == 0){
						$("#uploadpodModal").modal("hide");
						var fname = BASE_URL+returnedData.msg;
						var spanval = "<a href="+fname+" target='_blank' download>View Bill</a>";
						$("#uplodedfile_"+claim_id).html(spanval);
						$("#uploadfile").prop("disabled", false);
					}
					else{
						$("#uploadfile").prop("disabled", false);
						$("#err_up").html(returnedData.msg);
						return false;
					}
					//console.log("SUCCESS : ", data);
				},
				error: function (e) {
					//$("#result").text(e.responseText);
					console.log("ERROR : ", e);
					$("#btnSubmit").prop("disabled", false);
				}
		});
	});
});
