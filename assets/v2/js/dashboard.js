google.charts.load('current', { 'packages': ['corechart'] });
google.charts.setOnLoadCallback(drawChart);

function drawChart() {
    
    var data = google.visualization.arrayToDataTable([
        ['Month', 'Visits'],
        [month_2, parseFloat(productive_2)],
        [month_1, parseFloat(productive_1)],
        [month_cur, parseFloat(productive_cur)]
    ]);

    var options = {
        vAxis: { minValue: 0 },
        colors: ['#01C0C8', '#FB9678']
    };

    var chart = new google.visualization.AreaChart(document.getElementById('chart_area'));
    chart.draw(data, options);
}

google.charts.load("current", { packages: ["corechart"] });
google.charts.setOnLoadCallback(drawChartDonut);

function drawChartDonut() {
    var dataDonut = google.visualization.arrayToDataTable([
        ['Task', 'Total #'],
        ['Trainings', my_trainings],
        ['Meetings', my_meetings],
        ['School Visits', my_school_visits],
        ['Pending Entries', pending_entry_days],
    ]);

    var optionsDonut = {
        title: 'My Daily Activities',
        pieHole: 0.4,
        colors: ['#17a689', '#f1c40f', '#2ecc71', '#e74c3c'],
        pieSliceText: 'value'
    };

    var chart = new google.visualization.PieChart(document.getElementById('chart_Donut'));
    chart.draw(dataDonut, optionsDonut);
}
