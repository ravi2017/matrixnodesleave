
  function activity_block_list(did,bid)
  {
    var bid = (bid != '' ? bid : 'none');
    $.ajax({
      url: BASE_URL+'activity/get_blocks',
      type: 'get',
      data: {
        did: did, bid : bid
      },
      dataType: 'json',
      success: function(response) {
        $("#block_id").html("<option value=''>-Select Block-</option>");
        $.each(response, function() {
          if (bid == this['id'])
            $("#block_id").append("<option value='" + this['id'] + "' selected>" + this['name'] + " </option>");
          else
            $("#block_id").append("<option value='" + this['id'] + "'>" + this['name'] + " </option>");
        });
        $("#block_id").show();
      },
      error: function(response) {
        window.console.log(response);
      }
    });
  }

  function activity_cluster_list(bid,cid)
  {
    $.ajax({
      url: BASE_URL+'dashboard/get_clusters',
      type: 'get',
      data: {
        block: bid
      },
      dataType: 'json',
      success: function(response) {
        $("#cluster_id").html("<option value=''>-Select Cluster-</option>");
        $.each(response, function() {
          if (cid == this['id'])
            $("#cluster_id").append("<option value='" + this['id'] + "' selected>" + this['value'] + " </option>");
          else
            $("#cluster_id").append("<option value='" + this['id'] + "'>" + this['value'] + " </option>");
        });
        $("#cluster_id").show();
      },
      error: function(response) {
        window.console.log(response);
      }
    });
  }

  function activity_school_list(cid,scid)
  {
    $.ajax({
      url: BASE_URL+'dashboard/get_schools_list',
      type: 'get',
      data: {
        cluster: cid
      },
      dataType: 'json',
      success: function(response) {
        $("#school_id").html("<option value=''>-Select School-</option>");
        $.each(response, function() {
          if (cid == this['id'])
            $("#school_id").append("<option value='" + this['id'] + "' selected>" + this['value'] + " </option>");
          else
            $("#school_id").append("<option value='" + this['id'] + "'>" + this['value'] + " </option>");
        });
        $("#school_id").show();
      },
      error: function(response) {
        window.console.log(response);
      }
    });
  }

