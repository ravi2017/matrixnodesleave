function initMap() {
  var directionsService = new google.maps.DirectionsService;
  var directionsDisplay = new google.maps.DirectionsRenderer;
  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 6,
    center: {lat: 41.85, lng: -87.65}
  });
  directionsDisplay.setMap(map);
  var waypts = [];
  if (lat_longs.length > 0)
  {
    activity_start = lat_longs[0].tracking_latitude+","+lat_longs[0].tracking_longitude;
    activity_end = lat_longs[0].tracking_latitude+","+lat_longs[0].tracking_longitude;
    for (var i = 1; i < (lat_longs.length-1); i++) {
      waypts.push({
        location: new google.maps.LatLng(lat_longs[i].tracking_latitude,lat_longs[i].tracking_longitude),
        stopover: true
      });
      activity_end = lat_longs[lat_longs.length-1].tracking_latitude+","+lat_longs[lat_longs.length-1].tracking_longitude
    }
    if (waypts.length == 0)
    {
      waypts.push({
        location: new google.maps.LatLng(lat_longs[0].tracking_latitude,lat_longs[0].tracking_longitude),
        stopover: true
      });
    }
    calculateAndDisplayRoute(directionsService, directionsDisplay, waypts, activity_start, activity_end);
  }
  
  document.getElementById('submit').addEventListener('click', function() {
  var waypts = [];
    var checkboxArray = document.getElementById('waypoints');
    for (var i = 0; i < checkboxArray.length; i++) {
      if (checkboxArray.options[i].selected) {
        waypts.push({
          location: checkboxArray[i].value,
          stopover: true
        });
      }
    }
    calculateAndDisplayRoute(directionsService, directionsDisplay, waypts);
  });
}

function calculateAndDisplayRoute(directionsService, directionsDisplay, waypts, activity_start, activity_end) {
  directionsService.route({
    origin: activity_start,
    destination: activity_end,
    waypoints: waypts,
    optimizeWaypoints: true,
    travelMode: 'DRIVING'
  }, function(response, status) {
    if (status === 'OK') {
      directionsDisplay.setDirections(response);
      var route = response.routes[0];
      var summaryPanel = document.getElementById('directions-panel');
      summaryPanel.innerHTML = '';
      // For each route, display summary information.
      for (var i = 0; i < route.legs.length; i++) {
        var routeSegment = i + 1;
        summaryPanel.innerHTML += '<b>Route Segment: ' + routeSegment +
            '</b><br>';
        summaryPanel.innerHTML += route.legs[i].start_address + ' to ';
        summaryPanel.innerHTML += route.legs[i].end_address + '<br>';
        summaryPanel.innerHTML += route.legs[i].distance.text + '<br><br>';
      }
    } else {
      window.alert('Directions request failed due to ' + status);
    }
  });
}
