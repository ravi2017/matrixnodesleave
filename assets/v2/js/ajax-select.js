
function get_district_list(sid,did,district='')
{
  did.html("<option value='0'>Select District</option>");
  $.ajax({
    url: BASE_URL+'admin/dashboard/get_districts',
    type: 'get',
    data: {
      state: sid
    },
    dataType: 'json',
    success: function(response) {
      did.html("<option value='0'>Select District</option>");
      $.each(response, function() {
        if (district == this['id']){
          did.append("<option value='" + this['id'] + "' selected>" + this['value'] + "</option>");
				}
        else
          did.append("<option value='" + this['id'] + "'>" + this['value'] + "</option>");
      });
      did.show();
    },
    error: function(response) {
      window.console.log(response);
    }
  });
}

function get_block_list(did,bid,block='')
{
    bid.html("<option value='0'>Select Block</option>");
		$.ajax({
			url: BASE_URL+'admin/dashboard/get_blocks',
			type: 'get',
			data: {
				district: did
			},
			dataType: 'json',
			success: function(response) {
				bid.html("<option value='0'>Select Block</option>");
				$.each(response, function() {
					if (block == this['id'])
						bid.append("<option value='" + this['id'] + "' selected>" + this['value'] + " (Total Schools - " +  this['school_count'] + ")</option>");
					else
						bid.append("<option value='" + this['id'] + "'>" + this['value'] + " (Total Schools - " +  this['school_count'] + ")</option>");
				});
				bid.show();
			},
			error: function(response) {
				window.console.log(response);
			}
		});
	
}

function get_cluster_list(bid,cid,cluster='')
{
    cid.html("<option value='0'>Select Cluster</option>");
		$.ajax({
			url: BASE_URL+'admin/dashboard/get_clusters',
			type: 'get',
			data: {
				block: bid
			},
			dataType: 'json',
			success: function(response) {
				cid.html("<option value='0'>Select Cluster</option>");
				$.each(response, function() {
					if (cluster == this['id'])
						cid.append("<option value='" + this['id'] + "' selected>" + this['value'] + " </option>");
					else
						cid.append("<option value='" + this['id'] + "'>" + this['value'] + " </option>");
				});
				cid.show();
			},
			error: function(response) {
				window.console.log(response);
			}
		});
}

$("#state_id").change(function() {
  get_district_list($(this).val(),$("#district_id"),'');
});
$("#district_id").change(function() {
  get_block_list($(this).val(),$("#block_id"),'')
});
$("#block_id").change(function() {
  get_cluster_list($(this).val(),$("#cluster_id"),'')
});
