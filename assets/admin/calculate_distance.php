<?php
error_reporting(E_ALL);
ini_set('display_errors', TRUE); 

include_once('variable.php');

// Check connection
if (mysqli_connect_errno())
{
    echo "Failed to connect to MySQL: " . mysqli_connect_error();
}

//define('GOOGLE_API_KEY', 'AIzaSyAQBbK2ICEZErl3kTEcAtGOpls3U0N9WhI');

$query = "select ssp.login_id, ssp.name, Date(sst.activity_date) as ActivityDate, sst.source_address, sst.destination_address, sst.distance from ssc_trainings sst JOIN ssc_sparks ssp ON sst.user_id = ssp.id where distance > 200 order by user_id, activity_date";
$result = $con->query($query);

echo "<table cellspacing=0 cellpadding=5 border=1>
      <tr>
        <td>Login Id</td>
        <td>Name</td>
        <td>Activity Date</td>
        <td>Source Address</td>
        <td>Destination Address</td>
        <td>Distance</td>
      </tr>";
while($row = mysqli_fetch_array($result, MYSQLI_ASSOC))
{
  echo "<tr><td>".$row['login_id']."</td><td>".$row['name']."</td><td>".$row['ActivityDate']."</td><td>".$row['source_address']."</td><td>".$row['destination_address']."</td><td>".$row['distance']."</td></tr>";
}
echo "</table>";

/*$query = "SELECT id FROM ssc_trainings WHERE Date(activity_date) > '2019-05-09' and source = 'app' ";
$result = $con->query($query);

while($row = mysqli_fetch_array($result, MYSQLI_ASSOC))
{
  $activity_id = $row['id'];
  $query_tracking = "SELECT spark_id, tracking_time, tracking_latitude, tracking_longitude, activity_type FROM ssc_trackings WHERE activity_id = '$activity_id' and activity_type in('training', 'training_out') ";
  $result_tracking = $con->query($query_tracking);
  
  $lat1= $long1= $lat2 = $long2 = '';
  while($row_tracking = mysqli_fetch_array($result_tracking, MYSQLI_ASSOC))
  {
    if($row_tracking['activity_type'] == 'training'){
      $lat1  = $row_tracking['tracking_latitude'];
      $long1 = $row_tracking['tracking_longitude'];
    }
    if($row_tracking['activity_type'] == 'training_out'){
      $lat2  = $row_tracking['tracking_latitude'];
      $long2 = $row_tracking['tracking_longitude'];
    }
  }
  
  if($lat1 !='' && $long1 !='' && $lat2 !='' && $long2 !='')
  {
    $google_distance = get_google_distance($lat1, $long1, $lat2, $long2);
  
    $update = "update ssc_trainings set source_address = '".$google_distance['source_address']."' , destination_address = '".$google_distance['destination_address']."', distance = '".$google_distance['distance']."' where id = ".$activity_id;
    $con->query($update);
  }
}*/

function get_google_distance($lat1, $long1, $lat2, $long2)
{
  $details = "https://maps.googleapis.com/maps/api/directions/json?origin=".$lat1.",".$long1."&destination=".$lat2.",".$long2."&mode=driving&alternatives=true&sensor=false&key=".GOOGLE_API_KEY;
  
    $json = file_get_contents($details);
    $details = json_decode($json, TRUE);
    
    $routes = $details['routes'];
    $countRoutes = count($routes);
    
    $shortest_distance = 0;
    $end_address = '';
    $end_address = '';
    for($i=0; $i<$countRoutes; $i++)
    {
      $distance = $routes[$i]['legs'][0]['distance']['text'];
      $source_address = $routes[$i]['legs'][0]['start_address'];
      $end_address = $routes[$i]['legs'][0]['end_address'];
      $exp = explode(" ",$distance);
      $distance_val = $exp[0];
      $distance_param = $exp[1];
      if($distance_param == 'km')
      {
        $distance_val = $distance_val*1000;
      }
      else if($distance_param == 'mi'){
        $distance_val = $distance_val*1609;
      }
      if($i == 0){
        $shortest_distance = $distance_val;
      }else{  
        if($distance_val < $shortest_distance){
          $shortest_distance = $distance_val;
        }
      }
    }
    $returnData['distance'] = round($shortest_distance,2);
    $returnData['destination_address'] = $end_address;
    $returnData['source_address'] = $source_address;
    return $returnData; 
}

mysqli_close($con);
?>
