<?php
error_reporting(E_ALL);
ini_set('display_errors', TRUE); 

include_once('variable.php');

// Check connection
if (mysqli_connect_errno())
{
    echo "Failed to connect to MySQL: " . mysqli_connect_error();
}

// REMOVE ALL CLAIM DATA FOR SSC_TRACKING DISTANCE FLAG = 0
$rating_data = array();
$state_rating_data = array();

$sql_feedback_questions = "select id,title from tutorial";
$result_feedback_questions = $con_sf->query($sql_feedback_questions);
$questions = array();
while($row_feedback_question = mysqli_fetch_array($result_feedback_questions, MYSQLI_ASSOC))
{
  array_push($questions,$row_feedback_question['id']);
}

$created_at = date('Y-m-d');

$report_dates = return_report_dates();

$year_start_date = $report_dates['year_start_date'];
$month_start_date = $report_dates['month_start_date'];
$current_date = $report_dates['current_date'];
$month_year = $report_dates['month_year'];

$sql_training_req = "SELECT DISTINCT training_request_id FROM ssc_training_acknowledge_delivery WHERE created_on Between '$month_start_date' AND '$current_date' AND feedback_filled = 2 and contact_no != ''";

$result_training_req = $con_sf->query($sql_training_req);

if($result_training_req->num_rows > 0) 
{
  while($training_req = mysqli_fetch_array($result_training_req, MYSQLI_ASSOC))
  {
    $training_request_id = $training_req['training_request_id'];
    
    $sql_activity_spark = "SELECT spark_id FROM `ssc_challan_sparks` WHERE training_request_id = $training_request_id "; // ";
    $result_activity_spark = $con_sf->query($sql_activity_spark);
    
    if ($result_activity_spark->num_rows > 0)
    {
      $row_activity_spark = mysqli_fetch_array($result_activity_spark, MYSQLI_ASSOC);
      
      $sql_all_issues = "SELECT count(id) as TOTAL_ISSUE FROM ssc_training_acknowledge_delivery where created_on Between '$month_start_date' AND '$current_date' AND `training_request_id` = '".$training_request_id."' and contact_no != ''"; // 
      $result_all_issues = $con_sf->query($sql_all_issues);
      
      $all_issue_count = 0;
      if ($result_all_issues->num_rows > 0){
        $row_all_issue = mysqli_fetch_array($result_all_issues, MYSQLI_ASSOC);
        $all_issue_count = $row_all_issue['TOTAL_ISSUE'];
      }
      
      $five_percent_value = round((5*$all_issue_count)/100);
      
      $sql_filled_issues = "SELECT count(id) as TOTAL_FILLED FROM ssc_training_acknowledge_delivery WHERE created_on Between '$month_start_date' AND '$current_date' AND `training_request_id` = '".$training_request_id."' and contact_no != ''  and feedback_filled = 2 "; // 
      $result_filled_issues = $con_sf->query($sql_filled_issues);
      
      $filled_issue_count = 0;
      if ($result_filled_issues->num_rows > 0){
        $row_filled_issue = mysqli_fetch_array($result_filled_issues, MYSQLI_ASSOC);
        $filled_issue_count = $row_filled_issue['TOTAL_FILLED'];
      }

      if($filled_issue_count >= $five_percent_value) {
        
        $sql_ack_sparks = "SELECT spark_id, team_member_id FROM `ssc_challan_sparks` WHERE training_request_id = $training_request_id "; // ";
        $result_ack_spark = $con_sf->query($sql_ack_sparks);
        
        $spark_ids = array();
        
        array_push($spark_ids,$row_activity_spark['spark_id']);
        
        while($row_ack_spark = mysqli_fetch_array($result_ack_spark, MYSQLI_ASSOC))
        {
          if($row_ack_spark['team_member_id'] != 0)
            array_push($spark_ids,$row_ack_spark['team_member_id']);
        }
        
        foreach($spark_ids as $spark_id){
          
          $sql_sparks = "select name,state_id from ssc_sparks where id = $spark_id ";
          $result_sparks = $con_sf->query($sql_sparks);
          $row_sparks = mysqli_fetch_array($result_sparks, MYSQLI_ASSOC);
          
          //$spark_id = $row_activity_spark['spark_id'];
          $spark_name = $row_sparks['name'];
          $spark_state_id = $row_sparks['state_id'];
          if (!isset($rating_data[$spark_id]))
          {
            $rating_data[$spark_id] = array();
            $rating_data[$spark_id]['ratings'] = array();
            $rating_data[$spark_id]['name'] = $spark_name;
            foreach($questions as $question_id)
            {
              $rating_data[$spark_id]['ratings'][$question_id] = 0;
            }
            $rating_data[$spark_id]['total_rating'] = 0;
            $rating_data[$spark_id]['total_feedbacks'] = 0;
          }
        
          if (!isset($state_rating_data[$spark_state_id][$spark_id]))
          {
            $state_rating_data[$spark_state_id][$spark_id] = array();
            $state_rating_data[$spark_state_id][$spark_id]['ratings'] = array();
            $state_rating_data[$spark_state_id][$spark_id]['name'] = $spark_name;
            $state_rating_data[$spark_state_id][$spark_id]['state_id'] = $spark_state_id;
            foreach($questions as $question_id)
            {
              $state_rating_data[$spark_state_id][$spark_id]['ratings'][$question_id] = 0;
            }
            $state_rating_data[$spark_state_id][$spark_id]['total_rating'] = 0;
            $state_rating_data[$spark_state_id][$spark_id]['total_feedbacks'] = 0;
          }
        
          $sql_issues = "SELECT id,contact_no,training_request_id FROM ssc_training_acknowledge_delivery where created_on Between '$month_start_date' AND '$current_date' AND training_request_id = '".$training_request_id."' and contact_no != '' and feedback_filled = 2"; // 
          $result_issues = $con_sf->query($sql_issues);
          
          while($row_issue = mysqli_fetch_array($result_issues, MYSQLI_ASSOC))
          {
            $question_check_count = 0;
            foreach($questions as $question_id)
            {
              $sql_feedback = "SELECT * FROM tutorial_users where question_id = $question_id and mobile_number = '".$row_issue['contact_no']."' and issue_detail_id = ".$row_issue['training_request_id']." order by id asc limit 1";
              $result_feedback = $con_sf->query($sql_feedback);
              $row_feedback = mysqli_fetch_array($result_feedback, MYSQLI_ASSOC);
              $row_feedback['question_id']. " : ".$row_feedback['rating']. " : "; 
              if (isset($row_feedback['question_id']))
              {
                $question_check_count = $question_check_count + 1;
              }
            }
            
            if ($question_check_count == 7)
            {
              foreach($questions as $question_id)
              {
                $sql_feedback = "SELECT * FROM tutorial_users where question_id = $question_id and mobile_number = '".$row_issue['contact_no']."' and issue_detail_id = ".$row_issue['training_request_id']." order by id asc limit 1";
                $result_feedback = $con_sf->query($sql_feedback);
                $row_feedback = mysqli_fetch_array($result_feedback, MYSQLI_ASSOC);
                $row_feedback['question_id']. " : ".$row_feedback['rating']. " : "; 
                if (isset($row_feedback['question_id']))
                {
                  $rating_data[$spark_id]['ratings'][$question_id] = $rating_data[$spark_id]['ratings'][$question_id] + $row_feedback['rating'];
                  $rating_data[$spark_id]['total_rating'] = $rating_data[$spark_id]['total_rating'] + $row_feedback['rating'];
                  
                  $state_rating_data[$spark_state_id][$spark_id]['ratings'][$question_id] = $state_rating_data[$spark_state_id][$spark_id]['ratings'][$question_id] + $row_feedback['rating'];
                  $state_rating_data[$spark_state_id][$spark_id]['total_rating'] = $state_rating_data[$spark_state_id][$spark_id]['total_rating'] + $row_feedback['rating'];
                }
              }
              $rating_data[$spark_id]['total_feedbacks'] = $rating_data[$spark_id]['total_feedbacks'] + 1;
              $state_rating_data[$spark_state_id][$spark_id]['total_feedbacks'] = $state_rating_data[$spark_state_id][$spark_id]['total_feedbacks'] + 1;
            }
            else
            {
              //echo "<br>".$sql_feedback_delete = "DELETE FROM tutorial_users where mobile_number = '".$row_issue['contact_no']."' and issue_detail_id = ".$row_issue['id'];
              //$result_feedback = $con_sf->query($sql_feedback);
            }
          }
        
        }
      }
    }
  }
}

//print_r($rating_data);
//print_r($state_rating_data);

$truncate_sql = "delete from ssc_dashboard_spark_feedback_report where month_year = '".$month_year."'";
$con_sf->query($truncate_sql);

$national_ratings = array();
$state_ratings = array();

foreach($rating_data as $spark_id=>$r_data)
{
  $spark_name = $r_data['name'];
  $total_rating = $r_data['total_rating'];
  $total_feedbacks = $r_data['total_feedbacks'];
  //if ($total_feedbacks > 0)
    $question_ratings = json_encode($r_data['ratings']);
  //else
    //$question_ratings = json_encode(array());
  if ($total_feedbacks > 0)
  {
    $average_rating = 0;
    foreach($r_data['ratings'] as $r_data_rating)
    {
      $average_rating = $average_rating + $r_data_rating/$total_feedbacks;
    }
    $average_rating = round(($average_rating / 7),2);
  }
  else
    $average_rating = "-";
    
  if (!isset($national_ratings["$average_rating"]))
  {
    $national_ratings["$average_rating"] = array();
  }
  array_push($national_ratings["$average_rating"],$spark_id);
  $insert_sql = "insert into ssc_dashboard_spark_feedback_report (spark_id, spark_name, question_ratings, total_rating, total_feedbacks, created_at, month_year) values ('$spark_id','$spark_name','$question_ratings','$total_rating','$total_feedbacks','$created_at', '$month_year')";
  $con_sf->query($insert_sql);
}


foreach($state_rating_data as $state_id=>$spark_data)
{
  foreach($spark_data as $spark_id=>$r_data){
    $spark_name = $r_data['name'];
    $total_rating = $r_data['total_rating'];
    $total_feedbacks = $r_data['total_feedbacks'];
    $question_ratings = json_encode($r_data['ratings']);

    if ($total_feedbacks > 0)
    {
      $average_rating = 0;
      foreach($r_data['ratings'] as $r_data_rating)
      {
        $average_rating = $average_rating + $r_data_rating/$total_feedbacks;
      }
      $average_rating = round(($average_rating / 7),2);
    }
    else
      $average_rating = "-";
      
    if (!isset($state_ratings[$state_id]))
    {
      $state_ratings[$state_id] = array();
    }
    if (!isset($state_ratings[$state_id]["$average_rating"])) {
      $state_ratings[$state_id]["$average_rating"] = array();
    }
    array_push($state_ratings[$state_id]["$average_rating"],$spark_id);
  }
  krsort($state_ratings[$state_id]);
}

//echo '<pre>';
krsort($national_ratings);
//echo "=====================AFTER SORT============================";

$n_rate = 1;
$s_rate = 0;
foreach($national_ratings as $average_rating=>$national_rating)
{
  foreach($national_rating as $spark_id)
  {
    if ($average_rating == "-")
      $update_sql = "update ssc_dashboard_spark_feedback_report set national_rating_month = ".$n_rate." where spark_id = '".$spark_id."' and month_year = '$month_year' ";
    else
      $update_sql = "update ssc_dashboard_spark_feedback_report set national_rating_month = ".$n_rate.", average = ".$average_rating." where spark_id = '".$spark_id."' and month_year = '$month_year' ";
    $con_sf->query($update_sql);
    $s_rate = $s_rate + 1;
  }
  $n_rate = $n_rate + $s_rate;
  $s_rate = 0;
}

foreach($state_ratings as $state_id=>$staterating){
  $st_rate = 1;
  $sp_rate = 0;
  foreach($staterating as $average_rating=>$state_rating)
  {
    foreach($state_rating as $spark_id)
    {
      if ($average_rating == "-")
        $update_sql = "update ssc_dashboard_spark_feedback_report set state_rating_month = ".$st_rate." where spark_id = '".$spark_id."' and month_year = '$month_year' ";
      else
        $update_sql = "update ssc_dashboard_spark_feedback_report set state_rating_month = ".$st_rate." where spark_id = '".$spark_id."' and month_year = '$month_year' ";
      $con_sf->query($update_sql);
      $sp_rate = $sp_rate + 1;
    }
    $st_rate = $st_rate + $sp_rate;
    $sp_rate = 0;
  }
}


$sql_new = "SELECT * FROM `ssc_spark_feedback_report_new` WHERE created_at IN (SELECT max(created_at) FROM ssc_spark_feedback_report_new)";
$result_new = $con_sf->query($sql_new);

if($result_new->num_rows > 0) 
{
  while($row_new = mysqli_fetch_array($result_new, MYSQLI_ASSOC))
  {
    $spark_id = $row_new['spark_id'];
    $spark_name = $row_new['spark_name'];
    $state_rating = $row_new['state_rating'];
    $national_rating = $row_new['national_rating'];
    $question_ratings = $row_new['question_ratings'];
    $total_rating = $row_new['total_rating'];
    $total_feedbacks = $row_new['total_feedbacks'];
    $average = $row_new['average'];
    
    $select_1 = $con_sf->query("select * from ssc_dashboard_spark_feedback_report where spark_id = '".$spark_id."' and month_year = '$month_year' ");
    
    if($select_1->num_rows > 0) 
    {
      $update_sql = "update ssc_dashboard_spark_feedback_report set state_rating_year = '$state_rating', national_rating_year = '$national_rating' where spark_id = '".$spark_id."' and month_year = '$month_year' ";
      $con_sf->query($update_sql);
    }
    else{
      $insert_sql = "insert into ssc_dashboard_spark_feedback_report (spark_id, spark_name, question_ratings, total_rating, total_feedbacks, created_at, month_year, state_rating_month, national_rating_month, state_rating_year, national_rating_year, average) values ('$spark_id','$spark_name','$question_ratings','$total_rating','$total_feedbacks','$created_at', '$month_year', '0', '0' ,'$state_rating', '$national_rating', '$average')";
      $con_sf->query($insert_sql);
    }
  }
}

function return_report_dates()
{
  $dates = array();
  $dates['year_start_date'] = date('Y-05-01');
  
  if(date('d') < 7){
    $month_start_date = date('Y-m-d', strtotime('first day of last month'));
    $current_date = date('Y-m-d', strtotime('last day of last month'));
  }else{
    //$month_start_date = date('Y-06-01');
    //$current_date = date('Y-06-30');
    $month_start_date = date('Y-m-01');
    $current_date = date('Y-m-d');
  }
  $dates['month_start_date'] = $month_start_date;
  $dates['current_date'] = $current_date;
  $dates['month_year'] = date('m_Y', strtotime($month_start_date));
  
  return $dates;
}
 

mysqli_close($con_master);
mysqli_close($con_sf);
mysqli_close($con);
?>
