<?php
error_reporting(E_ALL);
ini_set('display_errors', TRUE); 

include_once('variable.php');

// Check connection
if (mysqli_connect_errno())
{
    echo "Failed to connect to MySQL: " . mysqli_connect_error();
}

// REMOVE ALL CLAIM DATA FOR SSC_TRACKING DISTANCE FLAG = 0
//remove_claim_data($con);
//echo "<pre>";
$activity_string = "'training-in', 'meeting-in', 'schoolvisit-in', 'schoolvisit', 'review_meeting', 'training' ";

$sql_spark = "SELECT distinct spark_id FROM ssc_trackings WHERE activity_type in (".$activity_string.")  and Date(tracking_time) > '2019-05-9' and Date(tracking_time) < '2019-05-20' ";
$result_spark = $con->query($sql_spark);

$m = 0;
if($result_spark->num_rows > 0) {
    while($row_spark = mysqli_fetch_array($result_spark, MYSQLI_ASSOC)){
      
      $spark_id = $row_spark['spark_id'];
      
      $sql_track = "SELECT DISTINCT Date(tracking_time) AS tracking_date FROM ssc_trackings WHERE spark_id = '$spark_id' and activity_type in(".$activity_string.")  and Date(tracking_time) > '2019-05-9' and Date(tracking_time) < '2019-05-20' ";
      $result_track = $con->query($sql_track);
      
      if($result_track->num_rows > 0) {
        while($row = mysqli_fetch_array($result_track, MYSQLI_ASSOC)){
            
            $tracking_ids = array(); 
            $destination_ids = array(); 
            
            $date = $row['tracking_date'];
            
            //set_attendance_out($spark_id, $con, $date); // Insert attendance out if not exist

            
            // Get tracking data for all activity except attendance-in & attendance-out
            $data = get_tracking_data($spark_id, $date, $con);
            $activity_count = count($data)-1;
            
            // START PROCESS FOR ATTENDANCE-IN & FIRST ACTIVITY 
              $data_attendance_in = getAttendanceId($con, $spark_id, $date, 'in', 'tracking_time asc'); // Get tracking data
              
              $source_in = $data_attendance_in['id']; // Tracking id for attendance-in
              $destination_in = $data[0]['id'];       // Tracking id for first activity
              
              //$distance_in_arr = get_google_distance($data_attendance_in['latitude'], $data_attendance_in['longitude'], $data[0]['latitude'], $data[0]['longitude']); // Calculate distance
              //$distance_in = $distance_in_arr['distance'];
              //$destination_address_in = $distance_in_arr['destination_address'];
              
              //insert_travel_claims($con, $source_in, $destination_in, $distance_in); // Insert data into travel claims table 
              
              //update_tracking_distance_value($con, $destination_in, $distance_in_arr); // Update distance into tracking table
              
              //$tracking_ids[] = $source_in; 
            // END PROCESS FOR ATTENDANCE-IN & FIRST ACTIVITY   
             
            for($i=0; $i < $activity_count; $i++)
            {
                $source_id = $data[$i]['id'];
                $destination_id = $data[$i+1]['id'];
                
                //$sql_claim = "select * from ssc_travel_claims where track_source_id = '$source_id' AND track_destination_id = '$destination_id'";
                //$result_claim = $con->query($sql_claim);
                //if($result_claim->num_rows == 0) {
                   
                   // Calculate distance between other activity
                   //$distance_arr = get_google_distance($data[$i]['latitude'], $data[$i]['longitude'], $data[$i+1]['latitude'], $data[$i+1]['longitude']);
                   //$distance = $distance_arr['distance'];
                   //$destination_address = $distance_arr['destination_address'];
                   
                   // Insert data into travel claims table
                   //insert_travel_claims($con, $source_id, $destination_id, $distance);
                   
                   // Update distance into tracking table for other activity 
                   //update_tracking_distance_value($con, $destination_id, $distance_arr); 

                   //$tracking_ids[] = $source_id; 
                //}
                //else{
                  //echo "<br>Already inserted";
                //}
            }
            
            // START PROCESS FOR LAST ACTIVITY & ATTENDANCE-OUT
              $data_attendance_out = getAttendanceId($con, $spark_id, $date, 'out', 'tracking_time desc'); // Get attendance out data
              if(!empty($data_attendance_out)){
                //echo"<br>".$m;    // Tracking id for last activity
                $source_out = $data[$activity_count]['id'];    // Tracking id for last activity
                $destination_out = $data_attendance_out['id']; // Tracking id for attendance-out
                
                //echo"<br>".$data[$activity_count]['latitude']." >> ".$data[$activity_count]['longitude'];
                //echo"<br>".$data_attendance_out['latitude']." >> ".$data_attendance_out['longitude'];
                $distance_out_arr = get_google_distance($data[$activity_count]['latitude'], $data[$activity_count]['longitude'], $data_attendance_out['latitude'], $data_attendance_out['longitude']); // Calculate distance 
                //print_r($distance_out_arr);
                $distance_out = $distance_out_arr['distance'];
                $destination_address_out = $distance_out_arr['destination_address'];
                
                insert_travel_claims($con, $source_out, $destination_out, $distance_out); // Insert data into travel claims table 
                
                update_tracking_distance_value($con, $destination_out, $distance_out_arr); // Update distance into tracking table
                
                $tracking_ids[] = $source_out; 
                $tracking_ids[] = $destination_out; 
                $m++;
              }
            // END PROCESS FOR LAST ACTIVITY & ATTENDANCE-OUT
            
            //print_r($tracking_ids);
            //Update distance flag for all activity
            update_tracking_distance_flag($con, $tracking_ids); 
            //echo "<br>After update flag";
            //echo "<hr>";
        }
      }  
    
    }
}


function update_tracking_distance_flag($con, $tracking_ids)
{
  $tracking_id = implode(',', $tracking_ids);
  
  $update_flag = "UPDATE ssc_trackings SET distance_flag = '1' WHERE id in ($tracking_id) ";
  $con->query($update_flag);
}


function update_tracking_distance_value($con, $tracking_id, $distance_arr)
{
  $distance = $distance_arr['distance'];
  $destination_address = $distance_arr['destination_address'];
              
  $update_val = "UPDATE ssc_trackings SET distance_val = '$distance', lat_long_location = '$destination_address' WHERE id in($tracking_id) ";
  $con->query($update_val);
}


function insert_travel_claims($con, $source_id, $destination_id, $distance)
{
  $select = "select count(id) from ssc_travel_claims where track_source_id = '$source_id' and track_destination_id = '$destination_id' ";
  $result = $con->query($select);
  
  if($result->num_rows == 0) {
    $insert = "INSERT INTO ssc_travel_claims(track_source_id, track_destination_id, travel_distance) VALUES('$source_id', '$destination_id' , '$distance')";
    $con->query($insert);
  }
  else{
    $update = "update ssc_travel_claims set travel_distance = '$distance' where track_source_id = '$source_id' and track_destination_id = '$destination_id' ";
    $con->query($update);
  }
  //return $con->insert_id();
}


function getAttendanceId($con, $spark_id, $date, $type = 'in', $order = 'tracking_time desc') {
    
    $activity = 'attendance-'.$type;
    $sql = "select * from ssc_trackings where spark_id = '$spark_id' and Date(tracking_time) = '$date' 
            and activity_type = '$activity' order by $order limit 1";
    $result = $con->query($sql);
    
    $data = array();
    if($result->num_rows > 0) {
      while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)){
        $data['id'] = $row['id'];
        $data['spark_id'] = $row['spark_id'];
        $data['time'] = $row['tracking_time'];
        $data['latitude'] = $row['tracking_latitude'];
        $data['longitude'] = $row['tracking_longitude'];
      }
    }
    return $data;
}


function get_tracking_data($spark_id, $date, $con){

    $activity_string = "'training-in', 'meeting-in', 'schoolvisit-in', 'schoolvisit', 'review_meeting', 'training'";

    $sql = "select * from ssc_trackings where spark_id = '$spark_id' and tracking_time like '%".date('Y-m-d',strtotime($date))."%' and activity_type in(".$activity_string.") order by tracking_time, id ASC";
    $result = $con->query($sql);
    $data = array();
    $i=0;
    if($result->num_rows > 0) {
      while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)){
        $data[$i]['id'] = $row['id'];
        $data[$i]['spark_id'] = $row['spark_id'];
        $data[$i]['time'] = $row['tracking_time'];
        $data[$i]['latitude'] = $row['tracking_latitude'];
        $data[$i]['longitude'] = $row['tracking_longitude'];
        $i++;
      }
    }
    return $data;
}


function set_attendance_out($spark_id, $con, $date)
{
  $activity_string = "'training-in', 'meeting-in', 'schoolvisit-in', 'schoolvisit', 'schoolvisit_out', 'review_meeting', 'review_meeting_out', 'training', 'training_out', 'attendance-in', 'attendance-out'";
  
  //$sql = "select * from ssc_trackings where spark_id = '$spark_id' and Date(tracking_time)= '$date' order by tracking_time,id desc limit 1 ";
  $sql = "select * from ssc_trackings where spark_id = '$spark_id' and Date(tracking_time)= '$date' and activity_type in(".$activity_string.") order by tracking_time desc limit 1";
  $result = $con->query($sql);
  $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
  //print_r($row);
  if($result->num_rows > 0) {
    $last_activity = $row['activity_type'];
    if(strtolower($last_activity) != 'attendance-out'){
      
      $new_track_time = date('Y-m-d H:i:s',strtotime('+20 seconds',strtotime($row['tracking_time'])));
      
      $insert = "INSERT INTO ssc_trackings(spark_id, tracking_time, tracking_latitude, tracking_longitude, tracking_location, activity_type, created_by) VALUES('".$row['spark_id']."', '".$new_track_time."', '".$row['tracking_latitude']."', '".$row['tracking_longitude']."', '".$row['tracking_location']."', 'attendance-out', 'system')";
      $con->query($insert);
      return;
    }
  }
}

function remove_claim_data($con)
{
  $activity_string = "'attendance-in', 'training-in', 'meeting-in', 'schoolvisit-in', 'attendance-out','schoolvisit', 'review_meeting', 'training', 'attendance-out'";
  
  $select = "SELECT id FROM ssc_trackings WHERE distance_flag = '0' and activity_type in (".$activity_string.") ";
  $result = $con->query($select);
      
  if($result->num_rows > 0) {
    while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)){
      
      $track_id = $row['id'];
      
      $delete = "DELETE FROM ssc_travel_claims WHERE track_source_id = '$track_id' OR track_destination_id = '$track_id'";
      $con->query($delete);
    }
  }
}

function get_google_distance($lat1, $long1, $lat2, $long2)
{
    $details = "https://maps.googleapis.com/maps/api/directions/json?origin=".$lat1.",".$long1."&destination=".$lat2.",".$long2."&mode=driving&alternatives=true&sensor=false&key=AIzaSyBFCRyOEq373kNiG6m_9kIRcZs8h8_u5Ps";
  
    $json = file_get_contents($details);
    $details = json_decode($json, TRUE);
    
    $routes = $details['routes'];
    $countRoutes = count($routes);
    
    $shortest_distance = 0;
    $end_address = '';
    for($i=0; $i<$countRoutes; $i++)
    {
      $distance = $routes[$i]['legs'][0]['distance']['text'];
      $end_address = $routes[$i]['legs'][0]['end_address'];
      $exp = explode(" ",$distance);
      $distance_val = $exp[0];
      $distance_param = $exp[1];
      if($distance_param == 'm')
      {
        $distance_val = $distance_val/1000;
      }
      else if($distance_param == 'mi'){
        $distance_val = $distance_val*1.609;
      }
      if($i == 0){
        $shortest_distance = $distance_val;
      }else{  
        if($distance_val < $shortest_distance){
          $shortest_distance = $distance_val;
        }
      }
    }
    $returnData['distance'] = round($shortest_distance,2);
    $returnData['destination_address'] = $end_address;
    return $returnData; 
}


?>
