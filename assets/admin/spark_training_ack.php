<?php
error_reporting(E_ALL);
ini_set('display_errors', TRUE); 

include_once('variable.php');

// Check connection
if (mysqli_connect_errno())
{
    echo "Failed to connect to MySQL: " . mysqli_connect_error();
}

$sql_ack_request = "select id, request_json, request_date from acknowledge_data where request_flag=0";
$result_ack_request = $con->query($sql_ack_request);
//echo "<pre>";
while($row_ack_request = mysqli_fetch_array($result_ack_request, MYSQLI_ASSOC))
{
  $ack_id = $row_ack_request['id'];
  $request_date = $row_ack_request['request_date'];
  $request_json = json_decode(trim($row_ack_request['request_json']),true);
  //print_r($request_json);
  
  $data = $request_json['Data']['teachers'][0];
  
  $dise_code = $data['dise_code'];
  
  $sql_school = "select * from ssc_schools where dise_code = '$dise_code' ";
  $result_school = $con->query($sql_school);
  $school_data = mysqli_fetch_array($result_school, MYSQLI_ASSOC);
  
  $training_no = $data['challan_no'];
  
  $sql_training = "select * from ssc_sampark_training_details where training_no = '$training_no' ";
  $result_training = $con->query($sql_training);
  $training_detail = mysqli_fetch_array($result_training, MYSQLI_ASSOC);
  
  $user_id = $request_json['Data']['user'];
  $sql_user = "select * from ssc_sparks where login_id = '$user_id' ";
  $result_user = $con->query($sql_user);
  $user_detail = mysqli_fetch_array($result_user, MYSQLI_ASSOC);
  
  if(!empty($training_detail))
  {
    $spark_id = $user_detail['id'];
    $training_request_id = $training_detail['id'];
    $school_id = $school_data['id'];
    $attendance_day = $data['attendance_day'];
    $kit_count = $data['kit_count'];
    $contact_no = $data['contact_no'];
    $issue_date = date('Y-m-d', strtotime($data['issue_date'])); 
    $created_on = $request_date; 
    $teacher_name = $data['name'];     
    
    $update = "update acknowledge_data set spark_id='$spark_id', training_request_id='$training_request_id', school_id='$school_id', 
              attendance_day='$attendance_day', kit_count='$kit_count', contact_no='$contact_no', issue_date = '$issue_date', 
              created_on = '$created_on', teacher_name = '$teacher_name', request_flag=1 where id = '$ack_id' ";
    $con->query($update);
  }  
}

mysqli_close($con_master);
mysqli_close($con_sf);
mysqli_close($con);
?>
