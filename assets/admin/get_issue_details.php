<?php
error_reporting(E_ALL);
ini_set('display_errors', TRUE); 

include_once('variable.php');

// Check connection
if (mysqli_connect_errno())
{
    echo "Failed to connect to MySQL: " . mysqli_connect_error();
}

//$sql = "SELECT tad.id, tad.school_id, sss.name as school_name, sss.state_id, sss.district_id, sss.block_id, sss.cluster_id, sss.dise_code, tad.name as teacher_name, tad.contact_no, tad.issuing_date FROM `ssc_issue_details` tad, ssc_schools sss WHERE tad.school_id = sss.id AND tad.contact_no > 0 AND issue_to = 'teacher' and school_teachers_flag=0 limit 3000 ";

$sql = "SELECT tad.id, tad.school_id, sss.dise_code, tad.name as teacher_name, tad.contact_no, tad.issuing_date FROM `ssc_issue_details` tad, ssc_schools sss WHERE tad.school_id = sss.id AND tad.contact_no > 0 AND issue_to = 'teacher' and school_teachers_flag=0 limit 3500 ";
$result = $con_master->query($sql);

if($result->num_rows > 0) {
    while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)){
      
      $ack_id = $row['id'];
      $school_id = $row['school_id'];
      $dise_code = $row['dise_code'];
				
      $sql_school = "SELECT sss.name as school_name, sss.state_id, sss.district_id, sss.block_id, sss.cluster_id, sss.dise_code from ssc_schools sss 
      where sss.dise_code = '$dise_code' ";
      $result_school = $con->query($sql_school);
			
			if($result_school->num_rows > 0) {
			
				$row_school = mysqli_fetch_array($result_school, MYSQLI_ASSOC);
				
				$contact_number = $row['contact_no'];
				
				$contact_length = strlen($contact_number);
				if($contact_length > 10){
					$contact_number = substr($contact_number, -10);
				}
				
				if(strlen($contact_number) == 10 && preg_match('/^[1-9][0-9]*$/', $contact_number)){
				
					$cheksql = "SELECT * FROM ssc_school_teachers WHERE dise_code = '$dise_code' AND contact_number = '$contact_number' ";
					$chkresult = $con->query($cheksql);

					if($chkresult->num_rows == 0) {
						
						$school_name = $row_school['school_name'];
						$dise_code = $row_school['dise_code'];
						$teacher_name = $row['teacher_name'];
						$contact_number = $contact_number;
						$activity_date = $row['issuing_date'];
						$school_id = $row['school_id'];
						$state_id = $row_school['state_id'];
						$district_id = $row_school['district_id'];
						$block_id = $row_school['block_id'];
						$cluster_id = $row_school['cluster_id'];
						
						$insert = "INSERT INTO ssc_school_teachers(school_name, dise_code, teacher_name, contact_number, activity_date, school_id, state_id, district_id, block_id, cluster_id, source) VALUES ('$school_name', '$dise_code', '$teacher_name', '$contact_number', '$activity_date', '$school_id', '$state_id', '$district_id', '$block_id', '$cluster_id', 'issue_details') ";
						$con->query($insert);
						
						$update = "UPDATE ssc_issue_details SET school_teachers_flag = 1 where id= '$ack_id' ";
						$con_master->query($update);
					}
					else{
						$update = "UPDATE ssc_issue_details SET school_teachers_flag = 2 where id= '$ack_id' ";
						$con_master->query($update);
					}
				}
			}
    }
}


mysqli_close($con_master);
mysqli_close($con_sf);
mysqli_close($con);

?>
