	<?php
error_reporting(E_ALL);
ini_set('display_errors', TRUE); 

include_once('variable.php');

// Check connection
if (mysqli_connect_errno())
{
    echo "Failed to connect to MySQL: " . mysqli_connect_error();
}

$query = "SELECT ssc_travel_claims.id as record_id, ssc_travel_claims.*, ssc_trackings.* FROM ssc_trackings, ssc_travel_claims WHERE ssc_trackings.id = ssc_travel_claims.track_destination_id and ssc_trackings.tracking_time BETWEEN '2019-12-01 00:00:00' AND '2019-12-31 23:59:59' and activity_type in ('training-in', 'training', 'review_meeting', 'meeting-in', 'schoolvisit-in', 'schoolvisit', 'attendance-out') and is_claimed = 0 and claim_discard = 0 order by ssc_trackings.tracking_time, ssc_trackings.id ASC";
$results = $con_sf->query($query);

$i=1;
$user_arr = array();
while($row_result = mysqli_fetch_array($results, MYSQLI_ASSOC))
{
	$activity_type = $row_result['activity_type'];
	$activity_id = $row_result['activity_id'];
	$travel_mode_id = $row_result['travel_mode_id'];
	$travel_cost = $row_result['travel_cost'];
	$record_id = $row_result['record_id'];
	
	if($activity_type == 'schoolvisit')
	{
		$sql = "select ssv.*, sss.login_id from ssc_school_visits ssv JOIN ssc_sparks sss ON ssv.user_id=sss.id where ssv.id = '$activity_id' ";
	}
	else if($activity_type == 'review_meeting')
	{
		$sql = "select ssv.*, sss.login_id from ssc_meetings ssv JOIN ssc_sparks sss ON ssv.user_id=sss.id where ssv.id = '$activity_id' ";
	}
	else if($activity_type == 'training')
	{
		$sql = "select ssv.*, sss.login_id from ssc_trainings ssv JOIN ssc_sparks sss ON ssv.user_id=sss.id where ssv.id = '$activity_id' ";
	}
	else if($activity_type == 'attendance-out')
	{
		$sql = "select ssv.id, ssv.attendance_date as activity_date, ssv.travel_mode_id, ssv.travel_cost, sss.login_id from ssc_attendances ssv JOIN ssc_sparks sss ON ssv.spark_id=sss.id where ssv.id = '$activity_id' ";
	}
	//echo"<br>".$sql;
	$result = $con_sf->query($sql);
	while($row = mysqli_fetch_array($result, MYSQLI_ASSOC))
	{
		if($row['travel_mode_id'] == '' && ($travel_mode_id == '' || $travel_mode_id == 0)){
			echo $row['login_id']." :: ".date('Ymd_His', strtotime($row['activity_date']))." :: ".$row['travel_mode_id']." :: ".$row['travel_cost']." :: ".$i." :: ".$activity_type." :: ".$row['id']." :: ".$travel_mode_id." :: ".$travel_cost." :: ".$record_id;
			$i++;
			
			$user_arr[$row['login_id']][] = $row['id'];
			
			$row_id = $row['id'];
			
			if($activity_type == 'schoolvisit')
			{
				$sql = "update ssc_school_visits set travel_mode_id = '9999', travel_cost='0.00' where id = '$row_id' ";
			}
			else if($activity_type == 'review_meeting')
			{
				$sql = "update ssc_meetings set travel_mode_id = '9999', travel_cost='0.00' where id = '$row_id'";
			}
			else if($activity_type == 'training')
			{
				$sql = "update ssc_trainings set travel_mode_id = '9999', travel_cost='0.00' where id = '$row_id' ";
			}
			else if($activity_type == 'attendance-out')
			{
				$sql = "update from ssc_attendances set travel_mode_id = '9999', travel_cost='0.00' where id = '$row_id' ";
			}
			echo "<br>".$sql;
			$con_sf->query($sql);
			
			echo "<br>".$update = "update ssc_travel_claims set travel_mode_id = '9999', travel_cost='0.00' where id = '$record_id' ";
			$con_sf->query($update);
			echo "<br>";
		}
	}
}

/*$sql = "select ssv.*, sss.login_id from ssc_school_visits ssv JOIN ssc_sparks sss ON ssv.user_id=sss.id where travel_mode_id is NULL and Date(activity_date) like '2019-12%' and source='app' ";
$result = $con_sf->query($sql);

$i=1;
while($row = mysqli_fetch_array($result, MYSQLI_ASSOC))
{
		echo $i." :: ".$row['id']." :: ".$row['login_id']." :: ".date('Ymd_His', strtotime($row['activity_date']))." :: ".$row['school_id']." :: ".$row['source_address']." :: ".$row['travel_cost'];
		echo "<br>";
		$i++;
}

echo "<hr>";

$sql = "select ssv.*, sss.login_id from ssc_meetings ssv JOIN ssc_sparks sss ON ssv.user_id=sss.id where travel_mode_id is NULL and Date(activity_date) like '2019-12%' and source='app' ";
$result = $con_sf->query($sql);

$i=1;
while($row = mysqli_fetch_array($result, MYSQLI_ASSOC))
{
		echo $i." :: ".$row['id']." :: ".$row['login_id']." :: ".date('Ymd_His', strtotime($row['activity_date']))." :: ".$row['meet_with']." :: ".$row['source_address']." :: ".$row['travel_cost'];
		echo "<br>";
		$i++;
}

echo "<hr>";

$sql = "select ssv.*, sss.login_id from ssc_trainings ssv JOIN ssc_sparks sss ON ssv.user_id=sss.id where travel_mode_id is NULL and Date(activity_date) like '2019-12%' and source='app' ";
$result = $con_sf->query($sql);

$i=1;
while($row = mysqli_fetch_array($result, MYSQLI_ASSOC))
{
		echo $i." :: ".$row['id']." :: ".$row['login_id']." :: ".date('Ymd_His', strtotime($row['activity_date']))." :: ".$row['mode']." :: ".$row['source_address']." :: ".$row['travel_cost'];
		echo "<br>";
		$i++;
}


echo "<hr>";

$sql = "select ssv.*, sss.login_id from ssc_other_activities ssv JOIN ssc_sparks sss ON ssv.user_id=sss.id where travel_mode_id is NULL and Date(activity_date) like '2019-12%' and source='app' ";
$result = $con_sf->query($sql);

$i=1;
while($row = mysqli_fetch_array($result, MYSQLI_ASSOC))
{
		echo $i." :: ".$row['id']." :: ".$row['login_id']." :: ".date('Ymd_His', strtotime($row['activity_date']))." :: ".$row['reason']." :: ".$row['from_address']." :: ".$row['travel_cost'];
		echo "<br>";
		$i++;
}*/

?>
