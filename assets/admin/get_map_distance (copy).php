<?php
error_reporting(E_ALL);
ini_set('display_errors', TRUE); 

include_once('variable.php');

// Check connection
if (mysqli_connect_errno())
{
    echo "Failed to connect to MySQL: " . mysqli_connect_error();
}

$activity_string = "'attendance-in', 'training-in', 'meeting-in', 'schoolvisit-in', 'attendance-out','schoolvisit', 'review_meeting', 'training'";

$current_date = (!isset($_GET['date']) ? date('Y-m-d') : date('Y-m-d', strtotime($_GET['date'])));

$sql_spark = "select distinct spark_id from ssc_trackings where tracking_time like '%".$current_date."%' and activity_type in (".$activity_string.") ";
$result_spark = $con->query($sql_spark);

if($result_spark->num_rows > 0) {
    while($row_spark = mysqli_fetch_array($result_spark, MYSQLI_ASSOC)){
      
      echo"<br>spark_id : ".$spark_id = $row_spark['spark_id'];
      
      set_attendance_out($spark_id, $con); // insert attendance out if not exist
      
      $data = get_tracking_data($spark_id, $current_date, $con);

      for($i=0; $i < count($data)-1; $i++)
      {
          $source_id = $data[$i]['id'];
          $destination_id = $data[$i+1]['id'];
          
          $sql_track = "select * from ssc_travel_claims where track_source_id = '$source_id' AND track_destination_id = '$destination_id'";
          $result_track = $con->query($sql_track);
          if($result_track->num_rows == 0) {
             
             echo"<br>".$distance = get_google_distance($data[$i]['latitude'], $data[$i]['longitude'], $data[$i+1]['latitude'], $data[$i+1]['longitude']);
             $distance = round($distance,2); 
             echo"<br>".$insert = "INSERT INTO ssc_travel_claims(track_source_id, track_destination_id, travel_distance) VALUES('$source_id', '$destination_id' , '$distance')";
             $con->query($insert);
          }
          else{
            echo "<br>Already inserted";
          }
      }
      
    }
}
else{
  echo 'No Spark Activity Found';
}

function get_google_distance($lat1, $long1, $lat2, $long2)
{
  $details = "https://maps.googleapis.com/maps/api/directions/json?origin=".$lat1.",".$long1."&destination=".$lat2.",".$long2."&mode=driving&alternatives=true&sensor=false&key=AIzaSyAQBbK2ICEZErl3kTEcAtGOpls3U0N9WhI";
  
    $json = file_get_contents($details);
    $details = json_decode($json, TRUE);
    
    $routes = $details['routes'];
    $countRoutes = count($routes);
    
    $shortest_distance = 0;
    for($i=0; $i<$countRoutes; $i++)
    {
      $distance = $routes[$i]['legs'][0]['distance']['text'];
      $exp = explode(" ",$distance);
      $distance_val = $exp[0];
      $distance_param = $exp[1];
      if($distance_param == 'm')
      {
        $distance_val = $distance_val/1000;
      }
      else if($distance_param == 'mi'){
        $distance_val = $distance_val*1.609;
      }
      if($i == 0){
        $shortest_distance = $distance_val;
      }else{  
        if($distance_val < $shortest_distance){
          $shortest_distance = $distance_val;
        }
      }
    }
    return $shortest_distance; 
}

function get_tracking_data($spark_id, $date, $con){
    $activity_string = "'attendance-in', 'training-in', 'meeting-in', 'schoolvisit-in', 'attendance-out','schoolvisit', 'review_meeting', 'training'";
    
    $sql = "select * from ssc_trackings where spark_id = '$spark_id' and tracking_time like '%".date('Y-m-d',strtotime($date))."%' and activity_type in(".$activity_string.") order by tracking_time ASC";
    $result = $con->query($sql);
    $data = array();
    $i=0;
    if($result->num_rows > 0) {
      while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)){
        $data[$i]['id'] = $row['id'];
        $data[$i]['spark_id'] = $row['spark_id'];
        $data[$i]['time'] = $row['tracking_time'];
        $data[$i]['latitude'] = $row['tracking_latitude'];
        $data[$i]['longitude'] = $row['tracking_longitude'];
        $i++;
      }
    }
    return $data;
}

function set_attendance_out($spark_id, $con)
{
  $sql = "select * from ssc_trackings where spark_id = '$spark_id' order by id desc limit 1 ";
  $result = $con->query($sql);
  $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
  //print_r($row);
  echo"<br>activity : ".$last_activity = $row['activity_type'];
  if(strtolower($last_activity) != 'attendance-out'){
    $insert = "INSERT INTO ssc_trackings(spark_id, tracking_time, tracking_latitude, tracking_longitude, tracking_location, activity_type, created_by) ";
    echo"<br>".$insert .= " VALUES('".$row['spark_id']."', '".$row['tracking_time']."', '".$row['tracking_latitude']."', '".$row['tracking_longitude']."', '".$row['tracking_location']."', 'attendance-out', 'system')";
    $con->query($insert);
    return;
  }
}

?>
