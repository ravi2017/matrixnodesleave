<?php
error_reporting(E_ALL);
ini_set('display_errors', TRUE); 

include_once('variable.php');

// Check connection
if (mysqli_connect_errno())
{
   echo "Failed to connect to MySQL: " . mysqli_connect_error();
}

$current_date = date('Y-m-d');

  $sql_1 = "SELECT distinct spark_id FROM ssc_trackings WHERE distance_flag = '0' AND activity_type in ('attendance-in') ";
  $result_1 = $con->query($sql_1);

  if($result_1->num_rows > 0) {
    while($row_1 = mysqli_fetch_array($result_1, MYSQLI_ASSOC)){
      
      $spark_id = $row_1['spark_id'];
      
      $sql_2 = "SELECT DISTINCT Date(tracking_time) AS tracking_date FROM ssc_trackings WHERE spark_id = '$spark_id' and distance_flag = 0  and activity_type in('attendance-in') and Date(tracking_time) < '$current_date' ";
      $result_2 = $con->query($sql_2);
      
      if($result_2->num_rows > 0) {
        while($row_2 = mysqli_fetch_array($result_2, MYSQLI_ASSOC)){
            $date = $row_2['tracking_date'];
            set_attendance_out($spark_id, $con, $date); // Insert attendance out if not exist
        }
      }
      
    }
  }
  else{
    //echo 'No Spark Activity Found';
  }


function set_attendance_out($spark_id, $con, $date)
{
  //$activity_string = "'training-in', 'meeting-in', 'schoolvisit-in', 'schoolvisit', 'review_meeting', 'training'";
  $activity_string = "'training-in', 'meeting-in', 'schoolvisit-in', 'schoolvisit', 'review_meeting', 'training', 'attendance-in', 'attendance-out'";
  
  //$sql = "select * from ssc_trackings where spark_id = '$spark_id' and Date(tracking_time)= '$date' order by tracking_time,id desc limit 1 ";
  $sql = "select * from ssc_trackings where spark_id = '$spark_id' and Date(tracking_time)= '$date' and activity_type in(".$activity_string.") order by tracking_time desc limit 1";
  $result = $con->query($sql);
  $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
  //print_r($row);
  if($result->num_rows > 0) {
    //echo"<hr>".date('Y-m-d', strtotime($row['tracking_time']));
    $last_activity = $row['activity_type'];
    if(strtolower($last_activity) != 'attendance-out'){
      
      $user_group = return_spark_group($con, $spark_id);
      if($user_group == 'head_office')
      {
				$new_track_time = date('Y-m-d 19:00:00',strtotime($row['tracking_time']));
			}
			else{
				$new_track_time = date('Y-m-d H:i:s',strtotime('+20 seconds',strtotime($row['tracking_time'])));
			}
      
      $insert = "INSERT INTO ssc_trackings(spark_id, tracking_time, tracking_latitude, tracking_longitude, tracking_location, activity_type, created_by) VALUES('".$row['spark_id']."', '".$new_track_time."', '".$row['tracking_latitude']."', '".$row['tracking_longitude']."', '".$row['tracking_location']."', 'attendance-out', 'system')";
      $con->query($insert);
      
      $update = "UPDATE ssc_trackings SET distance_flag = 1 WHERE spark_id = '$spark_id' and Date(tracking_time)= '$date' and activity_type = 'attendance-in' ";
      $con->query($update);
      
      //return;
    }
  }
}

function return_spark_group($con, $spark_id)
{
	$select = "SELECT user_group FROM ssc_sparks WHERE id = '$spark_id' ";
  $result = $con->query($select);
	
	$user_group = '';			
  if($result->num_rows > 0) {
    while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)){
      $user_group = $row['user_group'];
		}
	}
	return $user_group;
}

/*$activity_string = "'training-in', 'meeting-in', 'schoolvisit-in', 'schoolvisit', 'schoolvisit_out', 'review_meeting', 'review_meeting_out', 'training', 'training_out', 'attendance-in'";
  
  $sql = "SELECT id, max(tracking_time) as track_time, created_by, spark_id FROM ssc_trackings where activity_type='attendance-out' AND Date(tracking_time) > '2019-05-09' group by Date(tracking_time), spark_id ";
  $result = $con->query($sql); 

  //print_r($row);
  if($result->num_rows > 0) {
    while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)){
      
      $date = date('Y-m-d', strtotime($row['track_time']));
      $track_id = $row['id'];
      
      if($date != '2019-05-27'){
        
        echo"<br>out_time : ".$attendance_out_time = $row['track_time'];
        echo"<br>source : ".$created_by = $row['created_by'];
        echo"<br>spark_id : ".$spark_id = $row['spark_id'];
        
        echo"<br>".$sql1 = "SELECT spark_id, activity_type, tracking_time, created_at FROM ssc_trackings WHERE spark_id='$spark_id' AND Date(tracking_time)= '$date' AND activity_type in(".$activity_string.") ORDER BY tracking_time desc limit 1";
        $result1 = $con->query($sql1);
        $row1 = mysqli_fetch_array($result1, MYSQLI_ASSOC);
        //print_r($row);
        if($result1->num_rows > 0) {
          echo"<br>last_activity : ".$last_activity = $row1['activity_type'];
          echo"<br>last_time : ".$last_activity_time = $row1['tracking_time'];
          
          if(strtotime($attendance_out_time) < strtotime($last_activity_time))
          {
              $new_track_time = date('Y-m-d H:i:s',strtotime('+20 seconds',strtotime($last_activity_time)));
              echo"<br>update : ".$update = "UPDATE ssc_trackings set tracking_time = '$new_track_time' where id='$track_id' ";
              $con->query($update); 
          }
        }
        echo "<hr>";
      }
    }
  }*/
  
  /*$sql = "select * from ssc_trackings where spark_id = '$spark_id' and Date(tracking_time)= '$date' and activity_type in(".$activity_string.") order by tracking_time desc limit 1";
  $result = $con->query($sql);
  $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
  //print_r($row);
  if($result->num_rows > 0) {
    $last_activity = $row['activity_type'];
    if(strtolower($last_activity) != 'attendance-out'){
      
      $new_track_time = date('Y-m-d H:i:s',strtotime('+20 seconds',strtotime($row['tracking_time'])));
      
      $insert = "INSERT INTO ssc_trackings(spark_id, tracking_time, tracking_latitude, tracking_longitude, tracking_location, activity_type, created_by) VALUES('".$row['spark_id']."', '".$new_track_time."', '".$row['tracking_latitude']."', '".$row['tracking_longitude']."', '".$row['tracking_location']."', 'attendance-out', 'system')";
      $con->query($insert);
      return;
    }
  }*/



mysqli_close($con_master);
mysqli_close($con_sf);
mysqli_close($con);
?>
