<?php
error_reporting(E_ALL);
ini_set('display_errors', TRUE); 

include_once('variable.php');

// Check connection
if (mysqli_connect_errno())
{
    echo "Failed to connect to MySQL: " . mysqli_connect_error();
}

$query_copy = "CREATE TABLE ssc_trackings_20july AS SELECT * FROM ssc_trackings";
$con->query($query_copy);

$querycopy = "CREATE TABLE ssc_travel_claims_20july AS SELECT * FROM ssc_travel_claims";
$con->query($querycopy);

$sql_spark = "SELECT * FROM ssc_trackings WHERE activity_type = 'attendance-out' and Date(tracking_time) > '2019-06-30' and distance_flag=0 and created_by='app' ";
$result_spark = $con->query($sql_spark);

$m = 0;
$tracking_ids = array();
if($result_spark->num_rows > 0) {
    while($row_spark = mysqli_fetch_array($result_spark, MYSQLI_ASSOC)){
      
      $spark_id = $row_spark['spark_id'];
      $tracking_time = $row_spark['tracking_time'];
      $created_at = $row_spark['created_at'];
      $created_by = $row_spark['created_by'];
      
      if(date('Y-m-d',strtotime($tracking_time)) != date('Y-m-d', strtotime($created_at)) || (date('H:i', strtotime($created_at)) > '23:00' && $created_by == 'app'))
      {
        
        $sql_out = "SELECT * FROM ssc_trackings WHERE activity_type = 'attendance-out' and Date(tracking_time) = '".date('Y-m-d',strtotime($tracking_time))."' and spark_id = '$spark_id' ";
        $result_out = $con->query($sql_out);
        $count_attendance_out = $result_out->num_rows;
        
        $sql_in = "SELECT * FROM ssc_trackings WHERE activity_type = 'attendance-in' and Date(tracking_time) = '".date('Y-m-d',strtotime($tracking_time))."' and spark_id = '$spark_id' ";
        $result_in = $con->query($sql_in);
        $count_attendance_in = $result_in->num_rows;
        
        //echo"<br>".$count_attendance_out." >> ".$count_attendance_in;
        if($count_attendance_out > $count_attendance_in)
        {
          $activity_string = "'training-in', 'meeting-in', 'schoolvisit-in', 'schoolvisit', 'review_meeting', 'training' ";
          
          $sql_1 = "SELECT * FROM ssc_trackings WHERE activity_type IN ($activity_string) and Date(tracking_time) = '".date('Y-m-d',strtotime($tracking_time))."' and spark_id = '$spark_id' ";
          $result_1 = $con->query($sql_1);
          if($result_1->num_rows > 0) {
            
            $sql_2 = "select * FROM ssc_trackings WHERE activity_type = 'attendance-out' and Date(tracking_time) = '".date('Y-m-d',strtotime($tracking_time))."' and spark_id = '$spark_id' and created_by = 'system'  ";
            $result_2 = $con->query($sql_2);
            if($result_2->num_rows > 0) {
                $row_2 = mysqli_fetch_array($result_2, MYSQLI_ASSOC);
                
                $tracking_id = $row_2['id'];
                
                $sql_3 = "select * FROM ssc_travel_claims WHERE track_destination_id = '$tracking_id' ";
                $result_3 = $con->query($sql_3);
                
                if($result_3->num_rows > 0) {
                  
                  $del = "delete FROM ssc_trackings WHERE id = '$tracking_id' ";
                  $con->query($del);
                  $del1 = "delete FROM ssc_travel_claims WHERE track_destination_id = '$tracking_id' ";
                  $con->query($del1);
                }
            }
            $update = "update ssc_trackings set distance_flag = 0  WHERE Date(tracking_time) = '".date('Y-m-d',strtotime($tracking_time))."' and spark_id = '$spark_id' ";
            $con->query($update);
            
            if(!in_array($row_spark['id'], $tracking_ids))
              array_push($tracking_ids,$row_spark['id']);
          }
        }
      }
    }
}
//print_r($tracking_ids);

?>
