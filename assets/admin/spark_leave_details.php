<?php
error_reporting(E_ALL);
ini_set('display_errors', TRUE); 

include_once('variable.php');

// Check connection
if (mysqli_connect_errno())
{
    echo "Failed to connect to MySQL: " . mysqli_connect_error();
}

$sdate = "2019-06-01";
$edate = "2019-06-30";

$extra_state_holiday = array();
$extra_sat_sun = array();

$start_date = "2019-06-01 00:00:00";
$end_date = "2019-06-30 23:59:59";

$select = "SELECT distinct(user_id) FROM `ssc_leaves` WHERE ((`leave_from_date` BETWEEN '$start_date' AND '$end_date') OR (`leave_end_date` BETWEEN '$start_date' AND '$end_date')) AND `status` = 'approved'";

$result_request = $con_sf->query($select);
while($row_request = mysqli_fetch_array($result_request, MYSQLI_ASSOC))
{
  $spark_id = $row_request['user_id'];
  //$spark_id = 1;
  
  $sql = "SELECT * FROM `ssc_leaves` WHERE `user_id` = '$spark_id' AND ((`leave_from_date` BETWEEN '$start_date' AND '$end_date') OR (`leave_end_date` BETWEEN '$start_date' AND '$end_date')) AND `status` = 'approved'";
  $result = $con_sf->query($sql);
  
  $leaves_arr = array();
  while($row = mysqli_fetch_array($result, MYSQLI_ASSOC))
  {
    $leaves_arr[] = $row;
  }
  //print_r($leaves_arr);exit;
  $all_leaves = return_month_leave_distinct($leaves_arr, $sdate, $edate);
  
  $query = "select * from ssc_spark_user_state where user_id = '$spark_id' ";
  $query .= " AND start_date <= '$sdate' AND (ssc_spark_user_state.end_date >= '$edate' OR end_date IS NULL)";
  $result1 = $con_sf->query($query);
  $row1 = mysqli_fetch_array($result1, MYSQLI_ASSOC);
  
  $user_state = $row1['state_id'];
  
  $state_holidays = get_state_holidays($con_sf, $user_state, $sdate, $edate);
  
  $sat_sun = get_2_3_saturday_list($con_sf, $sdate, $edate);
  
  foreach($all_leaves as $leave)
  {
    if(in_array($leave, $state_holidays))
    {
      $extra_state_holiday[$spark_id][] = $leave;
    }
    
    if(in_array($leave, $sat_sun))
    {
      $extra_sat_sun[$spark_id][] = $leave;
    }
  }
  //print_r($sat_sun);
  //exit;
}

echo "<pre>";
print_r($extra_state_holiday);
echo "</pre>";
echo "<hr>";
echo "<pre>";
print_r($extra_sat_sun);
echo "</pre>";
  


function get_state_holidays($con_sf, $state_id, $_fromDate, $_toDate){
  $query = "SELECT * FROM ssc_holidays ssch WHERE `activity_date` BETWEEN '".$_fromDate." 00:00:00' AND '".$_toDate." 23:59:59' AND (stateid = '$state_id' OR NATIONAL = 1)";
  $result = $con_sf->query($query);
  
  $holidays_date = array();
  while($row = mysqli_fetch_array($result, MYSQLI_ASSOC))
  {
    $holidays_date[] = $row['activity_date'];
  }
  return $holidays_date;
}

function get_2_3_saturday_list($con_sf, $_fromDate, $_toDate){
    $query = "SELECT activity_date FROM ssc_offdays ssch WHERE `activity_date` BETWEEN '".$_fromDate." 00:00:00' AND '".$_toDate." 23:59:59'";
    $result = $con_sf->query($query);
     
    $holidays_date = array();
    while($row = mysqli_fetch_array($result, MYSQLI_ASSOC))
    {
      $holidays_date[] = $row['activity_date'];
    }
    return $holidays_date;
  }


function return_month_leave_distinct($leaves, $start_date, $end_date)
{
  $leavedate = array();
  
  foreach($leaves as $leave)
  {
    $leave_from_date = $leave['leave_from_date'];
    $leave_end_date = $leave['leave_end_date'];
    
    if($leave_from_date == $leave_end_date)
    {
      if((strtotime($leave_from_date) >= strtotime($start_date)) && (strtotime($leave_from_date) <= strtotime($end_date)))
      {
        if(!in_array($leave_from_date, $leavedate))
         array_push($leavedate, $leave_from_date);
      }
    }
    else{
      $dates = createRange($leave_from_date, $leave_end_date);
      foreach($dates as $date){
        if((strtotime($date) >= strtotime($start_date)) && (strtotime($date) <= strtotime($end_date))){
          if(!in_array($date, $leavedate))
            array_push($leavedate, $date);
        }
      }  
    }
  }
  return $leavedate;
}

function createRange($start, $end, $format = 'Y-m-d') {
      $start  = new DateTime($start);
      $end    = new DateTime($end);
      $invert = $start > $end;

      $dates = array();
      $dates[] = $start->format($format);
      while ($start != $end) {
          $start->modify(($invert ? '-' : '+') . '1 day');
          $dates[] = $start->format($format);
      }
      return $dates;
  }
  

mysqli_close($con_master);
mysqli_close($con_sf);
mysqli_close($con);
?>
