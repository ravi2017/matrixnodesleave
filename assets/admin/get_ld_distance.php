<?php
error_reporting(E_ALL);
ini_set('display_errors', TRUE); 
include_once('variable.php');

// Check connection
if (mysqli_connect_errno())
{
    echo "Failed to connect to MySQL: " . mysqli_connect_error();
}

// REMOVE ALL CLAIM DATA FOR SSC_TRACKING DISTANCE FLAG = 0
$sql = "SELECT * FROM ssc_other_activities WHERE distance_flag = '0' ";
$result = $con->query($sql);

if($result->num_rows > 0) {
    while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)){
      //print_r($row);
      $record_id = $row['id'];
      $spark_id = $row['user_id'];
      $distance_travel = get_google_distance($row['from_lat'], $row['from_lon'], $row['to_lat'], $row['to_lon']); // Calculate distance
      //print_r($distance_travel);
      $distance = $distance_travel['distance'];
      $update_val = "UPDATE ssc_other_activities SET travel_distance = '$distance', distance_flag = 1 WHERE id =$record_id ";
			$con->query($update_val);
    }
}

function get_google_distance($lat1, $long1, $lat2, $long2)
{
		$details = "https://maps.googleapis.com/maps/api/directions/json?origin=".$lat1.",".$long1."&destination=".$lat2.",".$long2."&mode=driving&alternatives=true&sensor=false&key=AIzaSyBFCRyOEq373kNiG6m_9kIRcZs8h8_u5Ps";
		//die;
    $json = file_get_contents($details);
    $details = json_decode($json, TRUE);
    
    $routes = $details['routes'];
    $countRoutes = count($routes);
    
    $shortest_distance = 0;
    $end_address = '';
    for($i=0; $i<$countRoutes; $i++)
    {
      $distance = $routes[$i]['legs'][0]['distance']['text'];
      $end_address = $routes[$i]['legs'][0]['end_address'];
      $exp = explode(" ",$distance);
      $distance_val = $exp[0];
      $distance_param = $exp[1];
      if($distance_param == 'm')
      {
        $distance_val = $distance_val/1000;
      }
      else if($distance_param == 'mi'){
        $distance_val = $distance_val*1.609;
      }
      if($i == 0){
        $shortest_distance = $distance_val;
      }else{  
        if($distance_val < $shortest_distance){
          $shortest_distance = $distance_val;
        }
      }
    }
    $returnData['distance'] = round($shortest_distance,2);
    $returnData['destination_address'] = $end_address;
    return $returnData; 
}
?>
