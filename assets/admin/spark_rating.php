<?php
error_reporting(E_ALL);
ini_set('display_errors', TRUE); 

include_once('variable.php');

// Check connection
if (mysqli_connect_errno())
{
    echo "Failed to connect to MySQL: " . mysqli_connect_error();
}

// REMOVE ALL CLAIM DATA FOR SSC_TRACKING DISTANCE FLAG = 0
$rating_data = array();

$sql_feedback_questions = "select id,title from tutorial";
$result_feedback_questions = $con_master->query($sql_feedback_questions);
$questions = array();
while($row_feedback_question = mysqli_fetch_array($result_feedback_questions, MYSQLI_ASSOC))
{
  array_push($questions,$row_feedback_question['id']);
}

$sql_answers = "SELECT * FROM `ssc_assessment_new` WHERE question_id in (SELECT id FROM `ssc_observation_questions` WHERE `question` LIKE '%challan%' and `activity_type` = 'training') and answer != ''";
$result_answers = $con_sf->query($sql_answers);

if($result_answers->num_rows > 0) 
{
  while($row_answer = mysqli_fetch_array($result_answers, MYSQLI_ASSOC))
  {
    $answer = $row_answer['answer'];
    $answer = preg_replace('/[^0-9]/', '', $answer);
    $issue_no = "CSR-".str_pad( $answer,5,"0",STR_PAD_LEFT);
    $activity_id = $row_answer['activity_id'];
    $sql_activity_spark = "SELECT ssc_trainings.user_id, ssc_sparks.name FROM `ssc_trainings`, ssc_sparks WHERE ssc_trainings.user_id = ssc_sparks.id and ssc_trainings.id = $activity_id and ssc_sparks.role in('field_user', 'manager') "; // ";
    $result_activity_spark = $con_sf->query($sql_activity_spark);
    if ($result_activity_spark->num_rows > 0)
    {
      $row_activity_spark = mysqli_fetch_array($result_activity_spark, MYSQLI_ASSOC);
      $spark_id = $row_activity_spark['user_id'];
      $spark_name = $row_activity_spark['name'];
      if (!isset($rating_data[$spark_id]))
      {
        $rating_data[$spark_id] = array();
        $rating_data[$spark_id]['ratings'] = array();
        $rating_data[$spark_id]['name'] = $spark_name;
        foreach($questions as $question_id)
        {
          $rating_data[$spark_id]['ratings'][$question_id] = 0;
        }
        $rating_data[$spark_id]['total_rating'] = 0;
        $rating_data[$spark_id]['total_feedbacks'] = 0;
      }
      
      $sql_all_issues = "SELECT count(id) as TOTAL_ISSUE FROM ssc_issue_details where issue_id in (select id FROM `ssc_issues` WHERE `issue_no` = '".$issue_no."') and contact_no != ''"; // 
      $result_all_issues = $con_master->query($sql_all_issues);
      $row_all_issue = mysqli_fetch_array($result_all_issues, MYSQLI_ASSOC);
      $all_issue_count = $row_all_issue['TOTAL_ISSUE'];
      
      $five_percent_value = round((5*$all_issue_count)/100);
      
      $sql_filled_issues = "SELECT count(id) as TOTAL_FILLED FROM ssc_issue_details where issue_id in (select id FROM `ssc_issues` WHERE `issue_no` = '".$issue_no."') and contact_no != ''  and feedback_filled = 2 "; // 
      $result_filled_issues = $con_master->query($sql_filled_issues);
      $row_filled_issue = mysqli_fetch_array($result_filled_issues, MYSQLI_ASSOC);
      $filled_issue_count = $row_filled_issue['TOTAL_FILLED'];

      if($filled_issue_count >= $five_percent_value) {
      
        $sql_issues = "SELECT id,contact_no FROM ssc_issue_details where issue_id in (select id FROM `ssc_issues` WHERE `issue_no` = '".$issue_no."') and contact_no != '' and feedback_filled = 2"; // 
        $result_issues = $con_master->query($sql_issues);
        
        while($row_issue = mysqli_fetch_array($result_issues, MYSQLI_ASSOC))
        {
          $question_check_count = 0;
          foreach($questions as $question_id)
          {
            $sql_feedback = "SELECT * FROM tutorial_users where question_id = $question_id and mobile_number = '".$row_issue['contact_no']."' and issue_detail_id = ".$row_issue['id']." order by id asc limit 1";
            $result_feedback = $con_master->query($sql_feedback);
            $row_feedback = mysqli_fetch_array($result_feedback, MYSQLI_ASSOC);
            $row_feedback['question_id']. " : ".$row_feedback['rating']. " : "; 
            if (isset($row_feedback['question_id']))
            {
              $question_check_count = $question_check_count + 1;
            }
          }
          
          if ($question_check_count == 7)
          {
            foreach($questions as $question_id)
            {
              $sql_feedback = "SELECT * FROM tutorial_users where question_id = $question_id and mobile_number = '".$row_issue['contact_no']."' and issue_detail_id = ".$row_issue['id']." order by id asc limit 1";
              $result_feedback = $con_master->query($sql_feedback);
              $row_feedback = mysqli_fetch_array($result_feedback, MYSQLI_ASSOC);
              $row_feedback['question_id']. " : ".$row_feedback['rating']. " : "; 
              if (isset($row_feedback['question_id']))
              {
                $rating_data[$spark_id]['ratings'][$question_id] = $rating_data[$spark_id]['ratings'][$question_id] + $row_feedback['rating'];
                $rating_data[$spark_id]['total_rating'] = $rating_data[$spark_id]['total_rating'] + $row_feedback['rating'];
              }
            }
            $rating_data[$spark_id]['total_feedbacks'] = $rating_data[$spark_id]['total_feedbacks'] + 1;
          }
          else
          {
            //echo "<br>".$sql_feedback_delete = "DELETE FROM tutorial_users where mobile_number = '".$row_issue['contact_no']."' and issue_detail_id = ".$row_issue['id'];
            //$result_feedback = $con_master->query($sql_feedback);
          }
        }
      
      }
    }
  }
}

$created_at = date('Y-m-d');
$truncate_sql = "delete from ssc_spark_feedback_report where created_at = '".$created_at."'";
$con_sf->query($truncate_sql);

$national_ratings = array();

foreach($rating_data as $spark_id=>$r_data)
{
  $spark_name = $r_data['name'];
  $total_rating = $r_data['total_rating'];
  $total_feedbacks = $r_data['total_feedbacks'];
  //if ($total_feedbacks > 0)
    $question_ratings = json_encode($r_data['ratings']);
  //else
    //$question_ratings = json_encode(array());
  if ($total_feedbacks > 0)
  {
    $average_rating = 0;
    foreach($r_data['ratings'] as $r_data_rating)
    {
      $average_rating = $average_rating + $r_data_rating/$total_feedbacks;
    }
    echo " : ".$average_rating = round(($average_rating / 7),2);
  }
  else
    $average_rating = "-";
    
  if (!isset($national_ratings["$average_rating"]))
  {
    $national_ratings["$average_rating"] = array();
  }
  
  array_push($national_ratings["$average_rating"],$spark_id);
  $insert_sql = "insert into ssc_spark_feedback_report (spark_id, spark_name, question_ratings, total_rating, total_feedbacks, created_at) values ('".$spark_id."','".$spark_name."','".$question_ratings."','".$total_rating."','".$total_feedbacks."','".$created_at."')";
  $con_sf->query($insert_sql);
}

//echo '<pre>';
krsort($national_ratings);
//echo "=====================AFTER SORT============================";

$n_rate = 1;
$s_rate = 0;
foreach($national_ratings as $average_rating=>$national_rating)
{
  foreach($national_rating as $spark_id)
  {
    if ($average_rating == "-")
      $update_sql = "update ssc_spark_feedback_report set national_rating = ".$n_rate." where spark_id = '".$spark_id."' and created_at = '$created_at' ";
    else
      echo " <br> ".$update_sql = "update ssc_spark_feedback_report set national_rating = ".$n_rate.", average = ".$average_rating." where spark_id = '".$spark_id."' and created_at = '$created_at' ";
    $con_sf->query($update_sql);
    $s_rate = $s_rate + 1;
  }
  $n_rate = $n_rate + $s_rate;
  $s_rate = 0;
}
//echo '</pre>';
mysqli_close($con_master);
mysqli_close($con_sf);
mysqli_close($con);
?>
