	<?php
error_reporting(E_ALL);
ini_set('display_errors', TRUE); 

include_once('variable.php');

// Check connection
if (mysqli_connect_errno())
{
    echo "Failed to connect to MySQL: " . mysqli_connect_error();
}

$state_arr = array('1'=>'CG', '2'=>'UK', '3'=>'JH', '4'=>'HR', '5'=>'HP', '6'=>'UP');
$current_date = date('Y-m-d',strtotime('-1 day'));

// FETCH MEETING DETAILS
$sql_meeting = "SELECT user_id, activity_date FROM ssc_meetings where status = 'approved' AND Date(created_at)  = '$current_date' ";
$results_meeting = $con_sf->query($sql_meeting);

while($row_meeting = mysqli_fetch_array($results_meeting, MYSQLI_ASSOC))
{
	$spark_id = $row_meeting['user_id'];
	$activity_date = date('Y-m-d', strtotime($row_meeting['activity_date']));
	$activity_type = 'meeting';
	
	$insert_meeting  = "INSERT INTO ssc_spark_activity_dates(spark_id, activity_type, activity_date) ";
	$insert_meeting .= " VALUSE('$spark_id', '$activity_type', '$activity_date') ";
	$con_sf->query($insert_meeting);
}

// FETCH TRAINING DETAILS
$sql_training = "SELECT user_id, activity_date FROM ssc_trainings where status = 'approved' AND Date(created_at)  = '$current_date' ";
$results_training = $con_sf->query($sql_training);

while($row_training = mysqli_fetch_array($results_training, MYSQLI_ASSOC))
{
	$spark_id = $row_training['user_id'];
	$activity_date = date('Y-m-d', strtotime($row_training['activity_date']));
	$activity_type = 'training';
	
	$insert_training  = "INSERT INTO ssc_spark_activity_dates(spark_id, activity_type, activity_date) ";
	$insert_training .= " VALUSE('$spark_id', '$activity_type', '$activity_date') ";
	$con_sf->query($insert_training);
}

// FETCH SCHOOL VISIT DETAILS
$sql_sv = "SELECT user_id, activity_date FROM ssc_school_visits where status = 'approved' AND Date(created_at)  = '$current_date' ";
$results_sv = $con_sf->query($sql_sv);

while($row_sv = mysqli_fetch_array($results_sv, MYSQLI_ASSOC))
{
	$spark_id = $row_sv['user_id'];
	$activity_date = date('Y-m-d', strtotime($row_sv['activity_date']));
	$activity_type = 'schoolvisit';
	
	$insert_sv  = "INSERT INTO ssc_spark_activity_dates(spark_id, activity_type, activity_date) ";
	$insert_sv .= " VALUSE('$spark_id', '$activity_type', '$activity_date') ";
	$con_sf->query($insert_sv);
}

// FETCH LD travel DETAILS
$sql_ld = "SELECT user_id, activity_date, activity_end_date FROM ssc_other_activities where Date(created_at)  = '$current_date' ";
$results_ld = $con_sf->query($sql_ld);

while($row_ld = mysqli_fetch_array($results_ld, MYSQLI_ASSOC))
{
	$spark_id = $row_ld['user_id'];
	$start_date = date('Y-m-d', strtotime($row_ld['activity_date']));
	$end_date = date('Y-m-d', strtotime($row_ld['activity_end_date']));
	$activity_type = 'ld_travel';
	
	$getDate = createRangeDates($start_date, $end_date);
	
	foreach($getDate as $dates){
		$insert_ld  = "INSERT INTO ssc_spark_activity_dates(spark_id, activity_type, activity_date) ";
		$insert_ld .= " VALUSE('$spark_id', '$activity_type', '$dates') ";
		$con_sf->query($insert_ld);
	}
}

// FETCH LEAVE DETAILS
$sql_leave = "SELECT user_id, leave_from_date, leave_end_date FROM ssc_leaves where Date(created_at)  = '$current_date' ";
$results_leave = $con_sf->query($sql_leave);

while($row_leave = mysqli_fetch_array($results_leave, MYSQLI_ASSOC))
{
	$spark_id = $row_leave['user_id'];
	$start_date = date('Y-m-d', strtotime($row_leave['leave_from_date']));
	$end_date = date('Y-m-d', strtotime($row_leave['leave_end_date']));
	$activity_type = 'leave';
	
	$getDate = createRangeDates($start_date, $end_date);
	
	foreach($getDate as $dates){
		$insert_leave  = "INSERT INTO ssc_spark_activity_dates(spark_id, activity_type, activity_date) ";
		$insert_leave .= " VALUSE('$spark_id', '$activity_type', '$dates') ";
		$con_sf->query($insert_leave);
	}
}


$previous_date = date('Y-m-d',strtotime('-7 day'));

// CHECK PREVIOUS DATE FOR USER HAVING GROUP TYPE "FIELD USER"

$cur_date = date('Y-m-d');
$user_group = 'field_user';

//check previous date with off days
$sql_offday = "SELECT * FROM ssc_offdays WHERE activity_date = '$previous_date' and user_group = '$user_group' ";
$results_offday = $con_sf->query($sql_offday);

if($result_offday->num_rows == 0) {
	
	//check previous date with holidays table for national holiday
	$sql_nholiday = "SELECT * FROM ssc_holidays WHERE activity_date = '$previous_date' and holiday_type = '$user_group' and national = 1 ";
	$results_nholiday = $con_sf->query($sql_nholiday);
	
	if($result_offday->num_rows == 0) {	
		foreach($state_arr as $state_id=>$state_name)
		{
			//check previous date with holidays table for state holiday
			$sql_holiday = "SELECT * FROM ssc_holidays WHERE activity_date = '$previous_date' and holiday_type = '$user_group' and stateid = '$state_id' ";
			$results_holiday = $con_sf->query($sql_holiday);
			
			if($results_holiday->num_rows == 0) {	
				/*$sql_sparks = "SELECT id FROM ssc_sparks WHERE state_id = '$state_id' and status = 1 and user_group = '$user_group' ";
				
				$sparks_arr = array();
				while($row_sparks = mysqli_fetch_array($results_sparks, MYSQLI_ASSOC))
				{
					$sparks_arr[] = $row_sparks->id;
				}
				$sparks_list = implode(',', $sparks_arr);*/
				
				// GET SPARKS WHICH ARE NOT HAVING ENTRY IN SPARK ACTIVITY DATE
				$sql_sparks = "SELECT id FROM ssc_sparks WHERE state_id = '$state_id' and status = 1 and user_group = '$user_group' and id NOT in (select spark_id from ssc_spark_activity_dates where activity_date = '$previous_date') ";
				
				while($row_sparks = mysqli_fetch_array($results_sparks, MYSQLI_ASSOC))
				{
					$spark_id = $row_sparks->id;
					
					//GET SPARK LEAVE CREDIT DETAILS WITH LEAVE TYPE
					$leave_arr = array();
					$sql_spark_leave = "SELECT leave_type, leave_earned, leave_taken FROM ssc_leave_credits where spark_id = '$spark_id' ";
					while($row_spark_leave = mysqli_fetch_array($results_spark_leave, MYSQLI_ASSOC))
					{
						$leave_arr[$row_spark_leave->leave_type] = $row_spark_leave->leave_earned - $row_spark_leave->leave_taken;	 
					}
					
					if(isset($leave_arr['Earned Leave']) && $leave_arr['Earned Leave'] >= 1)
					{
						$leave_type = 'Earned Leave';
					}
					else if(isset($leave_arr['Casual Leave']) && $leave_arr['Casual Leave'] >= 1)
					{
						$leave_type = 'Casual Leave';
					}
					else if(isset($leave_arr['Sick Leave']) && $leave_arr['Sick Leave'] >= 1)
					{
						$leave_type = 'Sick Leave';
					}
					else if(isset($leave_arr['Block Leave']) && $leave_arr['Block Leave'] >= 1)
					{
						$leave_type = 'Block Leave';
					}
					else if(isset($leave_arr['Comp-off']) && $leave_arr['Comp-off'] >= 1)
					{
						$leave_type = 'Comp-off';
					}
					else{
						$leave_type = 'LWP';
					}
					
					//INSERT SPARK LEAVE DETAILS
					$insert_leave  = "INSERT INTO ssc_leaves(user_id, state_id, leave_type, leave_from_date, leave_end_date, total_days, created_at, source, reason, status) ";
					$insert_leave .= " VALUES('$spark_id', '$state_id', '$leave_type', '$previous_date', '$previous_date', 1, '$cur_date', 'admin', 'pending_days', 'approved')";
					$con_sf->query($insert_leave);
					
				}
			}
		}
	}
}


  function createRangeDates($start, $end, $format = 'Y-m-d') {
    $start  = new DateTime($start);
    $end    = new DateTime($end);
    $invert = $start > $end;

    $dates = array();
    $dates[] = $start->format($format);
    while ($start != $end) {
        $start->modify(($invert ? '-' : '+') . '1 day');
        $dates[] = $start->format($format);
    }
    return $dates;
  }
?>
