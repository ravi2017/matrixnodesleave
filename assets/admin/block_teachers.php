<?php
error_reporting(E_ALL);
ini_set('display_errors', TRUE); 
include_once('variable.php');

// Check connection
if (mysqli_connect_errno())
{
    echo "Failed to connect to MySQL: " . mysqli_connect_error();
}

$state_arr = array('1'=>'CG', '2'=>'UK', '3'=>'JH', '4'=>'HR', '5'=>'HP', '6'=>'UP');

$sql = "SELECT sst.state_id, sst.block_id, ssb.name as block_name, sst.district_id, ssd.name as district_name, COUNT(sst.id) AS total_teacher FROM ssc_school_teachers sst, ssc_blocks ssb, ssc_districts ssd WHERE sst.block_id = ssb.id AND sst.district_id = ssd.id AND sst.contact_number > 0 "; 
if(isset($_GET['state_id'])){
	$state_id = $_GET['state_id'];
	$sql .= " AND sst.state_id = '$state_id' ";
}
$sql .= " GROUP BY block_id order by sst.state_id, ssd.name, total_teacher";
$result = $con->query($sql);

?>
<table cellspacing=0 cellpadding=5 border=1>
    <tr>
      <td>S.No.</td>
      <td>State Name</td>
      <td>District ID</td>
      <td>District Name</td>
      <td>Block ID</td>
      <td>Block Name</td>
      <td>Total Teacher</td>
    </tr>  
<?
$i=1;
if($result->num_rows > 0) {
    while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)){
      ?>
      <tr>
				<td><?=$i?></td>
				<td><?=$state_arr[$row['state_id']];?></td>
				<td><?=$row['district_id']?></td>
				<td><?=$row['district_name']?></td>
				<td><?=$row['block_id']?></td>
				<td><?=$row['block_name']?></td>
				<td><?=$row['total_teacher']?></td>
			</tr>  
      <?
      $i++;
    }
}
?>
</table>
