<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');
define('MAX_PREVIOUS_DAYS',	 5);

define('SESSION_START_MONTH',	5);
define('SESSION_START_MONTH_ABC',	10);
define('GOOGLE_API_KEY', 'AIzaSyBFCRyOEq373kNiG6m_9kIRcZs8h8_u5Ps');
define('GOOGLE_API_KEY_MAP', 'AIzaSyAy31bXa5Fqhvx1waKyP-_RyfBfoFmA_VM');

define('TELEGRAM_TOKEN', '878987885:AAHxJiQ5rq7D-xxyi2e8H2_o1zElNhCOnRs');

define('ACTIVITY_ARRAY',	serialize(array('school_visit'=>'School Visit', 'training'=>'Training', 'meeting'=>'Meeting', 'other'=>'Other')));

$month_arr = array('01'=>'Jan', '02'=>'Feb', '03'=>'Mar', '04'=>'Apr', '05'=>'May', '06'=>'Jun', '07'=>'Jul', '08'=>'Aug', '09'=>'Sep', '10'=>'Oct', '11'=>'Nov', '12'=>'Dec');
$year_arr = array('2019','2020');

define('MONTH_ARRAY',	serialize($month_arr));
define('YEAR_ARRAY',	serialize($year_arr));

$meet_with = array('CS'=>'CS', 'Secretary'=>'Secretary', 'SPD'=>'SPD', 'DM'=>'DM', 'DEO'=>'DEO', 'BRC'=>'BRC', 'CRC'=>'CRC');

define('MEET_WITH',	serialize($meet_with));

define('MEETING_TYPE',	json_encode(array('Weekly', 'Monthly', 'Quaterly', 'Annual')));

define('MIN_DISTANCE',	500);

define('INVENTORY_URL',	'http://localhost/sampark_inventory/');
define('INVENTORY_DOMAIN',	"");

define('TESTLINE_DOMAIN',	"http://localhost/sampark/");

define('TEST_LINE_ARRAY',	serialize(array('base_line'=>'Base Line', 'end_line'=>'End Line')));

define('MONTHLY_WEB_ACTIVITY_LIMIT',	3);

define('CLAIM_EXCEED_PERCENT',	10);

define('CLAIM_MONTHLY_LIMIT',	6000);
define('LD_MONTHLY_LIMIT',	6000);
define('OTHER_CLAIM_MONTHLY_LIMIT',	6000);
define('STAY_CLAIM_MONTHLY_LIMIT',6000);
define('FOOD_CLAIM_MONTHLY_LIMIT',8000);
define('PERDAY_FOOD_CLAIM', 400);
define('STAY_WITH_FRIEND', 200);

define('INDEX_COLOR',   serialize(array('1'=>'Green', '2'=>'Red', '3'=>'Yellow')));

$meet_with_SH = array('CS', 'Secretary', 'Director', 'SPD', 'Quality head/APD');
$meet_with_manager = array('CS', 'Secretary', 'Director', 'SPD', 'Quality head/APD', 'DM', 'DM&C');
$meet_with_field_user = array('DM', 'DM&C', 'DEO', 'DMC/DEO/BSA/CEO', 'BRC/BRP/BEO');

define('MEET_WITH_SH',	serialize($meet_with_SH));
define('MEET_WITH_MGR',	serialize($meet_with_manager));
define('MEET_WITH_FUSER',	serialize($meet_with_field_user));
define('TESTDATA_DOMAIN',	'');

$travel_role = array('state_person'=>'State Person', 'manager'=>'Manager', 'field_user'=>'Field User', 'accounts'=> 'Accounts', 'reports'=> 'Reports', 'executive'=> 'Executive');
define('TREVEL_ROLES',	serialize($travel_role));

define('USER_GROUP', serialize(array('head_office'=>'HO', 'field_user'=>'Field')));
define('LEAVE_TYPES', serialize(array('Earned Leave'=>'Earned Leave', 'Casual Leave'=>'Casual Leave', 'Sick Leave'=>'Sick Leave', 'Block Holiday'=>'Block Holiday', 'Comp-off'=>'Comp-off', 'LWP'=>'Leave without Pay', 'PL'=>'Paternity Leave', 'ML'=>'Maternity Leave', 'Bereavement Leave'=>'Bereavement Leave')));

define('EMP_BRANCH', serialize(array('Sampark Foundation'=>'Sampark Foundation', 'Sampark Management Consultancy LLP'=>'Sampark Management Consultancy LLP', 'I M Infotech'=>'I M Infotech')));
define('EMP_DEPARTMENT', serialize(array('Management'=>'Management', 'Operations'=>'Operations', 'Communications & Marketing'=>'Communications & Marketing', 'IT'=>'IT', 'Administations'=>'Administations', 'Finance'=>'Finance', 'Human Resource'=>'Human Resource', 'Pedagogy'=>'Pedagogy', 'Creative'=>'Creative')));
define('EMP_GRADE', serialize(array('G1'=>'G1', 'G2'=>'G2', 'G3'=>'G3', 'G4'=>'G4', 'G5'=>'G5', 'G6'=>'G6', 'G7'=>'G7')));
define('EMP_CATEGORY', serialize(array('Staff'=>'Staff', 'Contract'=>'Contract')));
define('EMP_DIVISIONS', serialize(array('HO'=>'HO', 'VN Support'=>'VN Support', 'CG'=>'CG', 'UK'=>'UK', 'JH'=>'JH', 'HR'=>'HR', 'HP'=>'HP', 'UP'=>'UP')));

define('EMP_ROLES', serialize(array('field_user'=>'Spark', 'manager'=>'Manager', 'state_person'=>'State Person', 'accounts'=>'Accounts', 'reports'=>'Reports', 'executive'=>'Executive', 'HR'=>'HR')));

define('EMP_GENDER', serialize(array('male'=>'Male', 'female'=>'Female', 'transgender'=>'Transgender', 'undeclared'=>'Undeclared')));
define('PAYMENT_MODE', serialize(array('bank_transfer'=>'Bank Transfer', 'cash'=>'Cash')));
define('BLOOD_GROUP', serialize(array('A+'=>'A+', 'A-'=>'A-', 'B+'=>'B+', 'B-'=>'B-', 'AB+'=>'AB+', 'AB-'=>'AB-', 'O+'=>'O+', 'O-'=>'O-')));
define('ATTENDANCE_REG_TYPE', serialize(array('Full Day'=>'Full Day', 'First Half'=>'First Half', 'Second Half'=>'Second Half')));

define('APPRAISAL_MONTHS', serialize(array('4'=>'April', '10'=>'October')));

define('EMP_DESIGNATIONS', serialize(array('DGM-FINANCE'=>'DGM-FINANCE',
'DISTRICT PROGRAM MANAGER'=>'DISTRICT PROGRAM MANAGER',
'PROGRAM CONTENT DEVELOPMENT & TRAINING HEAD'=>'PROGRAM CONTENT DEVELOPMENT & TRAINING HEAD',
'CREATIVE DIRECTOR'=>'CREATIVE DIRECTOR',
'NATIONAL MANAGER OPERATIONS'=>'NATIONAL MANAGER OPERATIONS',
'STATE PROGRAM HEAD'=>'STATE PROGRAM HEAD',
'PROGRAM COORDINATOR'=>'PROGRAM COORDINATOR',
'OFFICE ASSISTANT'=>'OFFICE ASSISTANT',
'CALL CENTRE EXECUTIVE'=>'CALL CENTRE EXECUTIVE',
'District Program Coordinator'=>'District Program Coordinator',
'EXECUTIVE FINANCE'=>'EXECUTIVE FINANCE',
'OFFICE ASSISTANT CUM DRIVER'=>'OFFICE ASSISTANT CUM DRIVER',
'Program Manager- Training & Learning'=>'Program Manager- Training & Learning',
'ASSISTANT PROGRAM COORDINATOR'=>'ASSISTANT PROGRAM COORDINATOR',
'GRAPHIC DESIGNER'=>'GRAPHIC DESIGNER',
'Head HR'=>'Head HR',
'Manager Communications'=>'Manager Communications',
'CFO'=>'CFO',
'Motion Designer'=>'Motion Designer',
'OFFICE EXECUTIVE'=>'OFFICE EXECUTIVE',
'ACCOUNTS EXECUTIVE'=>'ACCOUNTS EXECUTIVE',
'Store Executive'=>'Store Executive',
'MANAGER-ADMINISTRATION'=>'MANAGER-ADMINISTRATION',
'Sub Editor'=>'Sub Editor',
'Animation Designer'=>'Animation Designer',
'Stock Accounts Executive'=>'Stock Accounts Executive',
'CONSULTANT'=>'CONSULTANT',
'PRESIDENT'=>'PRESIDENT',
'Chief Technical Officer'=>'Chief Technical Officer',
'National Head-Strategic Partnership'=>'National Head-Strategic Partnership',
'HEAD COMMUNICATIONS'=>'HEAD COMMUNICATIONS',
'EA & Administrative Assistant'=>'EA & Administrative Assistant',
'Security Guard'=>'Security Guard',
'MANAGER CONTENT AND PEDAGOGY'=>'MANAGER CONTENT AND PEDAGOGY',
'Team Leader Creative'=>'Team Leader Creative',
'Head Pedagogy & Content Development'=>'Head Pedagogy & Content Development',
'Product Manager -Pedagogy & Content'=>'Product Manager -Pedagogy & Content')));

define('EMP_UNITS', serialize(array('FINANCE'=>'FINANCE',
'Gurugram'=>'Gurugram',
'PEDAGOGY'=>'PEDAGOGY',
'CONSULTANT'=>'CONSULTANT',
'Ranchi'=>'Ranchi',
'Panchkula'=>'Panchkula',
'Jamshedpur'=>'Jamshedpur',
'PAURI'=>'PAURI',
'ROORKEE'=>'ROORKEE',
'CHAMPAWAT'=>'CHAMPAWAT',
'JANJGIR-CHAMPA'=>'JANJGIR-CHAMPA',
'BEMETARA'=>'BEMETARA',
'PITHORAGARH'=>'PITHORAGARH',
'RAJNANDGAON'=>'RAJNANDGAON',
'KAWARDHA'=>'KAWARDHA',
'RAIGARH'=>'RAIGARH',
'ADMINISTRATION'=>'ADMINISTRATION',
'Raipur'=>'Raipur',
'VN SUPPORT'=>'VN SUPPORT',
'Shimla'=>'Shimla',
'KONDAGAON'=>'KONDAGAON',
'JASHPUR'=>'JASHPUR',
'BALOD'=>'BALOD',
'MUNGELI'=>'MUNGELI',
'Lucknow'=>'Lucknow',
'BASTAR'=>'BASTAR',
'GARIABAND'=>'GARIABAND',
'DURG'=>'DURG',
'COMMUNICATION & MKT'=>'COMMUNICATION & MKT',
'HUMAN RESOURCE'=>'HUMAN RESOURCE',
'MANAGEMENT'=>'MANAGEMENT',
'KANKER'=>'KANKER',
'KORIYA'=>'KORIYA',
'Hazaribagh'=>'Hazaribagh',
'Palamu'=>'Palamu',
'Dumka'=>'Dumka',
'Kodarma'=>'Kodarma',
'Rohtak'=>'Rohtak',
'Ambala&Kaithal '=>'Ambala&Kaithal ',
'Jind& Hisar'=>'Jind& Hisar',
'BHATAPARA'=>'BHATAPARA',
'HO'=>'HO',
'MAHASAMUND'=>'MAHASAMUND',
'Palwal/Faridabad'=>'Palwal/Faridabad',
'Sukma'=>'Sukma',
'Dantewara'=>'Dantewara',
'Gurugram& Mewat'=>'Gurugram& Mewat',
'Sirsa&Fatehabad'=>'Sirsa&Fatehabad',
'NANITAL'=>'NANITAL',
'Yamuna Nagar '=>'Yamuna Nagar ',
'Karnal'=>'Karnal',
'Solan'=>'Solan',
'Noida'=>'Noida',
'CG'=>'CG',
'Kangra'=>'Kangra',
'Sirmour'=>'Sirmour',
'Hisar'=>'Hisar',
'Hamirpur'=>'Hamirpur',
'Chamba- Uttarkashi'=>'Chamba- Uttarkashi',
'BHAGESHWAR'=>'BHAGESHWAR',
'Sonipat'=>'Sonipat',
'DEHRADUN'=>'DEHRADUN',
'Jhajjar'=>'Jhajjar',
'Rewari'=>'Rewari',
'Panipat'=>'Panipat',
'Kurukshetra'=>'Kurukshetra',
'BILASPUR'=>'BILASPUR',
'Meerut'=>'Meerut',
'Bareilly'=>'Bareilly',
'Agra'=>'Agra',
'Gorakhpur'=>'Gorakhpur',
'Varanasi'=>'Varanasi',
'Jhansi'=>'Jhansi',
'Kanpur'=>'Kanpur',
'BALRAMPUR'=>'BALRAMPUR',
'Chamba'=>'Chamba',
'Pakur'=>'Pakur',
'Shaibgunj'=>'Shaibgunj',
'Azamgarh'=>'Azamgarh',
'Mirzapur'=>'Mirzapur',
'Moradabad'=>'Moradabad',
'Behraich '=>'Behraich ',
'Ayodhya'=>'Ayodhya',
'Basti'=>'Basti',
'Charki Dadri'=>'Charki Dadri',
'SARGUJA'=>'SARGUJA',
'Kullu'=>'Kullu',
'ALMORA'=>'ALMORA')));

define('EMP_PROJECTS', serialize(array('HEAD OFFICE'=>'HEAD OFFICE','NATIONAL'=>'NATIONAL','ADMIN'=>'ADMIN','VN SUPPORT'=>'VN SUPPORT','MANAGEMENT'=>'MANAGEMENT')));

define('LEAVE_TYPES_SORT', serialize(array('earned leave'=>'EL', 'casual leave'=>'CL', 'sick leave'=>'SL', 'block holiday'=>'BH', 'comp-off'=>'COL', 'lwp'=>'LWP', 'pl'=>'PL', 'ml'=>'ML', 'bereavement'=>'BRL', 'other leave'=>'OL')));

define('OTHER_CLAIM_TYPES', serialize(array('Telephone'=>'Telephone', 'Travel'=>'Travel (reimbursment)', 'Local Con'=>'Local Con', 'Food'=>'Food Claims', 'Stay'=>'Stay Claims', 'Other'=>'Other Claims', 'Petrol reimbursement'=>'Petrol reimbursement')));

define('SSS_APP_POINTS', serialize(array('POST'=>10, 'COMMENT'=>3, 'LIKE'=>1)));
define('SSS_APP_TEACHER_POINTS', serialize(array('POST'=>10, 'COMMENT'=>3, 'LIKE'=>1)));

define('STATES_ARR', serialize(array('1'=>'CG', '2'=>'UK', '3'=>'JH', '4'=>'HR', '5'=>'HP', '6'=>'UP')));

//Attendance regulization time
define('DAY_START', '9:00:00');
define('DAY_MID', '14:00:00');
define('DAY_END', '19:00:00');


define('NOTICE_DAYS', '30');

define('SSS_APP_CALLING_STATE', serialize(array('4'=>'HR', '5'=>'HP', '6'=>'UP')));

/* End of file constants.php */
/* Location: ./application/config/constants.php */



if (!(PHP_SAPI === 'cli' OR defined('STDIN')))
{
	// Base URL with directory support
	$protocol = (!empty($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS'])!== 'off') ? 'https' : 'http';
	$base_url = $protocol.'://'.$_SERVER['HTTP_HOST'];
	$base_url.= dirname($_SERVER['SCRIPT_NAME']);
	define('BASE_URL', $base_url);
	define('BASE_URL_WITHOUT_PROTOCOL', $_SERVER['HTTP_HOST']);
	
	// For API prefix in Swagger annotation (/application/modules/api/swagger/info.php)
	define('API_HOST', $_SERVER['HTTP_HOST'].dirname($_SERVER['SCRIPT_NAME']));
}

