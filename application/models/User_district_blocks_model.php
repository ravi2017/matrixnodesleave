<?php
class User_District_Blocks_model extends CI_Model {

  public function __construct()
  {
    parent::__construct();
    $this->defaultdb = $this->load->database('default', TRUE);
  }

  public function get_all($user_id, $limit = '')
  {
    $this->defaultdb->where('user_id',$user_id);
    $this->defaultdb->join('blocks', 'blocks.id = spark_users_district_block.block_id');
    $this->defaultdb->join('districts', 'districts.id = spark_users_district_block.district_id');
    $this->defaultdb->select('districts.name as district_name');
    $this->defaultdb->select('blocks.name as block_name');
    $this->defaultdb->select('spark_users_district_block.*');
    if ($limit != "")
      $query = $this->defaultdb->get('spark_users_district_block', $limit, $offset);
    else
      $query = $this->defaultdb->get('spark_users_district_block');
    return $query->result();
  }

  public function insert_district_to_db($data)
  {
    $this->defaultdb->insert('spark_users_district_block', $data);
    return $this->defaultdb->insert_id();
  }

  public function getById($id)
  {
    $query = $this->defaultdb->get_where('spark_users_district_block',array('id'=>$id));
    return $query->row_array();		  
  }
  
  public function get_count($user_id='', $block_id = '')
  {
    $this->defaultdb->where("user_id",$user_id);
    $this->defaultdb->where("block_id",$block_id);
		$this->defaultdb->select('count(*) as total');
    $query = $this->defaultdb->get('spark_users_district_block');
    return $query->result();
  }
  
  public function delete_user_district_block($user_id) {
        $this->defaultdb->where('user_id', $user_id);
        return $this->defaultdb->delete('spark_users_district_block');
    }

}

?>
