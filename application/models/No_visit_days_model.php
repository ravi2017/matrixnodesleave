<?php
class No_visit_days_model extends CI_Model {

  public function __construct()
  {
    parent::__construct();
    $this->defaultdb = $this->load->database('default', TRUE);
  }

  public function get_all($user_id, $start_date, $end_date)
  {
    $this->defaultdb->where("user_id",$user_id);
    $this->defaultdb->where("activity_date >= ",$start_date." 00:00:00");
    $this->defaultdb->where("activity_date <= ",$end_date." 23:59:59");
    $this->defaultdb->where("status = ","approved");
    $this->defaultdb->group_by("activity_date");
    $query = $this->defaultdb->get('no_visit_days');
    //echo"<br>".$this->defaultdb->last_query();
    return $query->result();
  }

  public function get_all_count($user_ids, $start_date, $end_date)
  {
    $this->defaultdb->where_in("user_id",$user_ids);
    $this->defaultdb->where("activity_date >= ",$start_date." 00:00:00");
    $this->defaultdb->where("activity_date <= ",$end_date." 23:59:59");
    $this->defaultdb->where("status = ","approved");
    $this->defaultdb->select("count(distinct activity_date) as total");
    $query = $this->defaultdb->get('no_visit_days');
    //echo $this->defaultdb->last_query();
    return $query->row_array();
  }

  public function get_all_user_visit($user_id, $start_date, $end_date)
  {
    $this->defaultdb->where_in("user_id",$user_id);
    $this->defaultdb->where("activity_date >= ",$start_date." 00:00:00");
    $this->defaultdb->where("activity_date <= ",$end_date." 23:59:59");
    $this->defaultdb->where("status = ","approved");
    $query = $this->defaultdb->get('no_visit_days');
    return $query->result();
  }

  public function get_approval_activities($user_id, $start_date, $end_date, $status = '')
  {
    $this->defaultdb->where("user_id",$user_id);
    $this->defaultdb->where("activity_date >= ",$start_date." 00:00:00");
    $this->defaultdb->where("activity_date <= ",$end_date." 23:59:59");
    if($status != 'all')
		$this->defaultdb->where("status != ","approved");
    $query = $this->defaultdb->get('no_visit_days');
    return $query->result();
  }

  public function add($data)
  {
    $this->defaultdb->insert('no_visit_days', $data);
    return $this->defaultdb->insert_id();
  }

  public function getById($id)
  {
    $query = $this->defaultdb->get_where('no_visit_days',array('id'=>$id));
    return $query->row_array();		  
  }
		public function get_district_training($district_id, $start_date, $end_date)
  {
    $this->defaultdb->where("district_id",$district_id);
    //$this->defaultdb->where("block_id",0);
    $this->defaultdb->where("activity_date >= ",$start_date);
    $this->defaultdb->where("activity_date <= ",$end_date);
    $query = $this->defaultdb->get('no_visit_days');
    return $query->result();
  }
	public function get_block_training($block_id, $start_date, $end_date)
  {
    $this->defaultdb->where("block_id",$block_id);
    //$this->defaultdb->where("block_id",0);
    $this->defaultdb->where("activity_date >= ",$start_date);
    $this->defaultdb->where("activity_date <= ",$end_date);
    $query = $this->defaultdb->get('no_visit_days');
    return $query->result();
  }
	public function get_traning($district_id,$user_id, $start_date, $end_date)
  {
    $this->defaultdb->where("district_id",$district_id);
    $this->defaultdb->where("user_id",$user_id);
    //$this->defaultdb->where("block_id",0);
    $this->defaultdb->where("activity_date >= ",$start_date);
    $this->defaultdb->where("activity_date <= ",$end_date);
		$this->defaultdb->select('count(*) as total');
    $query = $this->defaultdb->get('no_visit_days');
    return $query->result();
  }
	public function get_user_traning($state_id,$district_id,$user_id, $start_date, $end_date)
  {
    $this->defaultdb->where("state_id",$state_id);
    $this->defaultdb->where("district_id",$district_id);
    $this->defaultdb->where("user_id",$user_id);
    //$this->defaultdb->where("block_id",0);
    $this->defaultdb->where("activity_date >= ",$start_date);
    $this->defaultdb->where("activity_date <= ",$end_date);
    $this->defaultdb->select('*');
		
    $query = $this->defaultdb->get('no_visit_days');
    return $query->result();
  }
  public function getActivityInfoForNoVisit($user_id, $limit=10)
  {
    $this->defaultdb->where("user_id",$user_id);
    $this->defaultdb->order_by("id", "desc");
    $query = $this->defaultdb->get('no_visit_days',$limit);
   
    return $query->result();
  
  }

}

?>
