<?php
class blocks_model extends CI_Model {

  public function __construct()
  {
    parent::__construct();
    $this->defaultdb = $this->load->database('default', TRUE);
  }

  public function get_all_block($district = 0, $limit = 0, $offset = 0)
  {
    if ($district > 0)
      $this->defaultdb->where('blocks.district_id',$district);
      
    $this->defaultdb->order_by("id", "desc");
    if ($limit != "")
      $query = $this->defaultdb->get('blocks', $limit, $offset);
    else
      $query = $this->defaultdb->get('blocks');
    return $query->result();
  }
  
  public function get_blocks_by_name($district_id, $name)
  {
    $query = $this->defaultdb->get_where('blocks',array("LOWER(name)"=>strtolower($name),"district_id"=>$district_id));
    return $query->result();
  }
  
  public function get_question($id,$user)
  {
    $query = $this->defaultdb->query("select * from sv_questions where block_id = ".$id." and id not in (select question_id from sv_responses where block_id = ".$id." and user_id = ".$user.") limit 1");
   # return $query->result();
    return $query->row_array();	
  }
  
  public function get_district_blocks($district, $limit = 5, $offset = 0)
  {
    $this->defaultdb->order_by("name", "asc");
    if ($limit != "")
      $query = $this->defaultdb->get_where('blocks', array('district_id' => $district), $limit, $offset);
    else
      $query = $this->defaultdb->get_where('blocks', array('district_id' => $district));
    return $query->result();
  }

  public function get_block_count()
  {
    $query = $this->defaultdb->count_all_results('blocks');
    return $query;
  }

  public function insert_block_to_db($data)
  {
    $this->defaultdb->insert('blocks', $data);
    return $this->defaultdb->insert_id();
  }

  public function getById($id)
  {
    $query = $this->defaultdb->get_where('blocks',array('id'=>$id));
    return $query->row_array();		  
  }

  public function update_info($data,$id)
  {
    $this->defaultdb->where('blocks.id',$id);
    return $this->defaultdb->update('blocks', $data);
  }

  public function delete_a_block($id)
  {
    $this->defaultdb->where('blocks.id',$id);
    return $this->defaultdb->delete('blocks');
  }
  
  
  public function get_block($state_id, $limit = 5, $offset = 0)
  {
    $this->defaultdb->order_by("name", "asc");
    $this->defaultdb->select("name");
    $this->defaultdb->select("dise_code");
    if ($limit != "")
      $query = $this->defaultdb->get_where('blocks', array('state_id' => $state_id), $limit, $offset);
    else
      $query = $this->defaultdb->get_where('blocks', array('state_id' => $state_id));
    return $query->result();
  }
  
  public function get_block_auto($q){
    $this->defaultdb->select('*');
    $this->defaultdb->like('dise_code', $q);
     $query = $this->defaultdb->get_where('blocks');
    if($query->num_rows > 0){
      foreach ($query->result_array() as $row){
	   $row1['label'] = htmlentities(stripslashes($row['dise_code']." (".$row['name'].")"));
	   $row1['value'] = htmlentities(stripslashes($row['dise_code']." (".$row['name'].")"));
	   $row1['id'] = $row['id'];
	   $row1['block_id'] = $row['id'];
     $row1['state_id'] = $row['state_id'];
     $row1['district_id'] = $row['district_id'];
		 $row_set[] = $row1;
      }
      echo json_encode($row_set); //format the array into json data
    }
  }
   public function check_block_by_DistrictState($block_id,$district_ids,$state_id){
	  
    $this->defaultdb->where('state_id',$state_id);
    $this->defaultdb->where('id',$block_id);
    $this->defaultdb->where_in('district_id',$district_ids);
	  $query = $this->defaultdb->get_where('blocks');
	  return $query->result();
  }
  
  public function getByIds($ids)
  {
    $this->defaultdb->where_in('id',$ids);
    $query = $this->defaultdb->get('blocks');
    return $query->result();		  
  }
}

?>
