<?php
class clusters_model extends CI_Model {

  public function __construct()
  {
    parent::__construct();
    $this->defaultdb = $this->load->database('default', TRUE);
  }

  public function get_all_cluster($block = 0, $limit = 0, $offset = 0)
  {
    if ($block > 0)
      $this->defaultdb->where('clusters.block_id',$block);
      
    $this->defaultdb->order_by("id", "desc");
    if ($limit != "")
      $query = $this->defaultdb->get('clusters', $limit, $offset);
    else
      $query = $this->defaultdb->get('clusters');
    return $query->result();
  }
  
  public function get_question($id,$user)
  {
    $query = $this->defaultdb->query("select * from sv_questions where cluster_id = ".$id." and id not in (select question_id from sv_responses where cluster_id = ".$id." and user_id = ".$user.") limit 1");
   # return $query->result();
    return $query->row_array();	
  }

  
  public function get_clusters_by_name($block_id, $name)
  {
    $query = $this->defaultdb->get_where('clusters',array("LOWER(name)"=>strtolower($name),"block_id"=>$block_id));
    return $query->result();
  }

  public function get_cluster_count()
  {
    $query = $this->defaultdb->count_all_results('clusters');
    return $query;
  }
  
  public function get_block_clusters($block, $limit = 5, $offset = 0)
  {
    $this->defaultdb->order_by("name", "asc");
    if ($limit != "")
      $query = $this->defaultdb->get_where('clusters', array('block_id' => $block), $limit, $offset);
    else
      $query = $this->defaultdb->get_where('clusters', array('block_id' => $block));
    return $query->result();
  }
  
  public function get_district_clusters($district, $limit = 5, $offset = 0)
  {
    $this->defaultdb->order_by("name", "asc");
    if ($limit != "")
      $query = $this->defaultdb->get_where('clusters', array('district_id' => $district), $limit, $offset);
    else
      $query = $this->defaultdb->get_where('clusters', array('district_id' => $district));
    return $query->result();
  }
  
  public function get_state_clusters($state, $limit = 5, $offset = 0)
  {
    $this->defaultdb->order_by("name", "asc");
    if ($limit != "")
      $query = $this->defaultdb->get_where('clusters', array('state_id' => $state), $limit, $offset);
    else
      $query = $this->defaultdb->get_where('clusters', array('state_id' => $state));
    return $query->result();
  }

  public function insert_cluster_to_db($data)
  {
    $this->defaultdb->insert('clusters', $data);
    return $this->defaultdb->insert_id();
  }

  public function getById($id)
  {
    $query = $this->defaultdb->get_where('clusters',array('id'=>$id));
    return $query->row_array();		  
  }

  public function update_info($data,$id)
  {
    $this->defaultdb->where('clusters.id',$id);
    return $this->defaultdb->update('clusters', $data);
  }

  public function delete_a_cluster($id)
  {
    $this->defaultdb->where('clusters.id',$id);
    return $this->defaultdb->delete('clusters');
  }
  
  public function get_cluster($state_id, $limit = 5, $offset = 0)
  {
    $this->defaultdb->order_by("name", "asc");
	$this->defaultdb->select("name");
	$this->defaultdb->select("dise_code");
    if ($limit != "")
      $query = $this->defaultdb->get_where('clusters', array('state_id' => $state_id), $limit, $offset);
    else
      $query = $this->defaultdb->get_where('clusters', array('state_id' => $state_id));
    return $query->result();
  }
   public function get_cluster_auto($q){
    $this->defaultdb->select('*');
    $this->defaultdb->like('name', $q);
     $query = $this->defaultdb->get_where('clusters');
    if($query->num_rows > 0){
      foreach ($query->result_array() as $row){
	   $row1['label'] = htmlentities(stripslashes($row['name']));
	   $row1['value'] = htmlentities(stripslashes($row['name']));
	   $row1['id'] = $row['id'];
	   $row1['cluster_id'] = $row['id'];
     $row1['state_id'] = $row['state_id'];
     $row1['district_id'] = $row['district_id'];
     $row1['block_id'] = $row['block_id'];
		 $row_set[] = $row1;
      }
      echo json_encode($row_set); //format the array into json data
    }
  }
  
  public function check_cluster_by_blockDistrictState($cluster_id,$block_id,$district_ids,$state_id){
	                                 
    $this->defaultdb->where('state_id',$state_id);
    $this->defaultdb->where('block_id',$block_id);
    $this->defaultdb->where('id',$cluster_id);
    $this->defaultdb->where_in('district_id',$district_ids);
	  $query = $this->defaultdb->get_where('clusters');
    
	  return $query->result();
  }
  
  public function get_by_block_ids($block_ids){
	                                 
    $this->defaultdb->where_in('block_id',$block_ids);
    $this->defaultdb->join('blocks', 'blocks.id = clusters.block_id');
    $this->defaultdb->select('blocks.name as block_name');
    $this->defaultdb->select('clusters.*');
	  $query = $this->defaultdb->get_where('clusters');
    
	  return $query->result();
  }
  
  public function getByIds($ids)
  {
    $this->defaultdb->where_in('id',$ids);
    $query = $this->defaultdb->get('clusters');
    return $query->result();		  
  }
}

?>
