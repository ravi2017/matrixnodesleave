<?php

class Timeoverlaptest_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        error_log(" TimeoverlapTest model construct ");
        $this->defaultdb = $this->load->database('default', TRUE);
    }

    public function checkAvailability($user_id, $activity_dateTime, $duration) {
		return true;
		$activity_date = (string)date('d/m/Y',strtotime($activity_dateTime));
        if($activity_date  == '07/10/2017' || $activity_date == '11/10/2017'){
            return true;
        }
        error_log(" TimeoverlapTest model checkAvailability() function user_id=".$user_id." activity_dateTime=".$activity_dateTime." duration=".$duration);
        $sql1 = "CALL sp_TimeOverlap('$user_id', '$activity_dateTime', '$duration', @outputparam)";
        $svOverlapResult = $this->defaultdb->query($sql1);
        $overresult = $svOverlapResult->result();
        $availabilityFound = $overresult[0]->time_overlap;
        mysqli_next_result($this->defaultdb->conn_id);
        error_log(" TimeoverlapTest model checkAvailability()  ".$availabilityFound);
        //Value =0  means non availability due to time overlap
        return ($availabilityFound == 1 );
    }
    
	/*  CHECKING FULL DAY ACTIVITY ALREADY MARKED AGAINST Leaves , No_Visit_Day , Holiday AND VICE - VERSA
      Case 1:
      flag = productive  (When Activities already marked)
      Message : Days already marked for activity , can't take this request.

      Case 2:
      flag = unproductive (When Leaves and No_Visit_Day already added)
      Message : Leave/Local Holiday/No Visit Day already marked , can't add activity.

     */

    public function checkFullDayActivity($user_id, $activity_dateTime, $flag, $endDateTime) {
		return true;
		$activity_date = (string)date('d/m/Y',strtotime($activity_dateTime));
        if($activity_date  == '07/10/2017' || $activity_date == '11/10/2017'){
            return true;
        }
        error_log(" checkFullDayActivity model  function user_id=" . $user_id . " activity_dateTime=" . $activity_dateTime . " End-date ". $endDateTime ." Flag=" . $flag);
        $sql1 = "CALL sp_checkFulldayActivity('$user_id', '$activity_dateTime', '$flag',@outputparam,'$endDateTime')";
        $fulldayResult = $this->defaultdb->query($sql1);
        $markedResult = $fulldayResult->result();
        $markedFound = $markedResult[0]->productivity;
        mysqli_next_result($this->defaultdb->conn_id);
        error_log("Full day marked -->" . $markedFound);
        // markedFound==0 means not FULLDAY EVENT IS MARKED FOR THIS DATE SO new activity is allowed
        return ($markedFound == 0 );
    }

}

?>
