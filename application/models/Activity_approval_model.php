<?php
//error_reporting(0);
class Activity_approval_model extends CI_Model {

  public function __construct() {
      parent::__construct();
  }

  public function get_sparks($id){
    return $this->db->select('*')->where('manager_id', $id)->order_by('name')->get('sparks')->result();
  }

  public function get_managers($id){
    $result = $this->db->select('*')->where(array('state_id'=>$id,'role'=>'manager'))->get('sparks')->result();
    return $result;
  }


  public function approve_activity($activity_type, $id){
    $this->db->where('id', $id)->update($activity_type, array('status'=>"approved"));
    return $this->db->affected_rows();
  }

  public function reject_activity($activity_type, $id, $reason,$cur_user_id){
    $activity = $this->db->where('id', $id)->get($activity_type)->row();
    $data['spark_id'] = $activity->user_id;
    $data['rejector_id'] = $cur_user_id;
    $data['reason'] = $reason;
    $data['activity_date'] = $activity->activity_date;
    $data['activity_type'] = $activity_type;
    $data['rejected_on'] = date('Y-m-d H:i:s');
    $data['additional_details'] = json_encode($activity);
    $this->db->insert('rejected_activities', $data);

    $this->db->where('id', $id)->delete($activity_type);

    return 1;
  }

  public function get_rejected_activities($id){
    return $this->db->select('*')->where('spark_id', $id)->get('rejected_activities')->result();
  }

  public function rejected_activity_details($id){
    return $this->db->select('*')->where('id', $id)->get('rejected_activities')->result();
  }
}
