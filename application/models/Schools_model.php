<?php
class schools_model extends CI_Model {

  public function __construct()
  {
    parent::__construct();
    error_log(" Inside schools_model construct ");
    $this->defaultdb = $this->load->database('default', TRUE);
    error_log(" Exit schools_model construct ");    
  }

  public function school_by_dice_number($dice_number)
  {
    $query = $this->defaultdb->get_where('schools', array('dise_code' => $dice_number));
    return $query->row_array();		 
  }
  
  public function get_schools($school_ids)
  {
    $this->defaultdb->select('schools.*');
    $this->defaultdb->where_in('schools.id',$school_ids);
    $query = $this->defaultdb->get('schools');
    return $query->result();
  }
  
  public function get_schools_details($school_ids)
  {
    $this->defaultdb->join('states', 'states.id = schools.state_id');
    $this->defaultdb->join('districts', 'districts.id = schools.district_id');
    $this->defaultdb->join('blocks', 'blocks.id = schools.block_id');
    $this->defaultdb->join('clusters', 'clusters.id = schools.cluster_id');
    $this->defaultdb->select('states.name as state_name');
    $this->defaultdb->select('districts.name as district_name');
    $this->defaultdb->select('clusters.name as cluster_name');
    $this->defaultdb->select('blocks.name as block_name');
    $this->defaultdb->select('schools.*');
    $this->defaultdb->where_in('schools.id',$school_ids);
      
    $this->defaultdb->order_by("clusters.name asc, schools.name asc");
    $query = $this->defaultdb->get('schools');
    return $query->result();
  }
  
  public function get_cluster_schools($cluster_ids, $limit = 0, $offset = 0)
  {
    $this->defaultdb->join('clusters', 'clusters.id = schools.cluster_id');
    $this->defaultdb->select('clusters.name as cluster_name');
    $this->defaultdb->select('schools.*');
    $this->defaultdb->where_in('schools.cluster_id',$cluster_ids);
      
    $this->defaultdb->order_by("clusters.name", "asc");
    if ($limit > 0)
      $query = $this->defaultdb->get('schools', $limit, $offset);
    else
      $query = $this->defaultdb->get('schools');
    return $query->result();
  }
    
  
  public function get_school_count($state_id = 0, $district_id = 0, $block_id = 0, $cluster_id = 0, $village_id = 0)
  {
    if ($state_id > 0)
      $this->defaultdb->where('schools.state_id',$state_id);
    if ($district_id > 0)
      $this->defaultdb->where('schools.district_id',$district_id);
    if ($block_id > 0)
      $this->defaultdb->where('schools.block_id',$block_id);
    if ($cluster_id > 0)
      $this->defaultdb->where('schools.cluster_id',$cluster_id);
    if ($village_id > 0)
      $this->defaultdb->where('schools.village_id',$village_id);
      
    $this->defaultdb->order_by("name", "asc");
    $query = $this->defaultdb->get('schools');
    return $rowcount = $query->num_rows();
  }
  
  public function search_schools($state_id = 0, $district_id = 0, $block_id = 0, $cluster_id = 0, $village_id = 0, $limit = 0, $offset = 0)
  {
    if ($state_id > 0)
      $this->defaultdb->where('schools.state_id',$state_id);
    if ($district_id > 0)
      $this->defaultdb->where('schools.district_id',$district_id);
    if ($block_id > 0)
      $this->defaultdb->where('schools.block_id',$block_id);
    if ($cluster_id > 0)
      $this->defaultdb->where('schools.cluster_id',$cluster_id);
    if ($village_id > 0)
      $this->defaultdb->where('schools.village_id',$village_id);
      
    $this->defaultdb->order_by("blocks.name asc, clusters.name asc, schools.name asc");
    
    $this->defaultdb->join('blocks', 'blocks.id = schools.block_id');
    $this->defaultdb->join('clusters', 'clusters.id = schools.cluster_id');
    $this->defaultdb->select('clusters.name as cluster_name');
    $this->defaultdb->select('blocks.name as block_name');
    $this->defaultdb->select('schools.*');
    if ($limit > 0)
      $query = $this->defaultdb->get('schools', $limit, $offset);
    else
      $query = $this->defaultdb->get('schools');
    return $query->result();
  }
  
  public function search_district_schools($district_ids, $limit = 0, $offset = 0)
  {
    $this->defaultdb->where_in('schools.district_id', $district_ids);
      
    $this->defaultdb->order_by("name", "asc");
    if ($limit > 0)
      $query = $this->defaultdb->get('schools', $limit, $offset);
    else
      $query = $this->defaultdb->get('schools');
    return $query->result();
  }
  
  public function get_allowed_schools($user)
  {
    $this->load->model('users_model');
    $row_set = array();
    $allowed = 0;
    
    $aPermissions = $this->users_model->get_permissions($user['id']);
    $aSchools = array();
    foreach($aPermissions as $aPermission)
    {
      $loop = 1;
      if ($aPermission->school_id > 0)
      {
        $loop = 0;
        $school = $this->schools_model->getById($aPermission->school_id);
        $aSchools[$school['id']] = $aPermission->permission;
      }
      else if ($aPermission->village_id > 0)
      {
        $schools = $this->search_schools(0, 0, 0, 0, $aPermission->village_id);
      }
      else if ($aPermission->cluster_id > 0)
      {
        $schools = $this->search_schools(0, 0, 0, $aPermission->cluster_id, 0);
      }
      else if ($aPermission->block_id > 0)
      {
        $schools = $this->search_schools(0, 0, $aPermission->block_id, 0, 0);
      }
      else if ($aPermission->district_id > 0)
      {
        $schools = $this->search_schools(0, $aPermission->district_id, 0, 0, 0);
      }
      else if ($aPermission->state_id > 0)
      {
        $schools = $this->search_schools($aPermission->state_id);
      }
      if ($loop == 1) {
        foreach($schools as $school) {
          $aSchools[$school->id] = $aPermission->permission;
        }
      }
    }
    
    return $aSchools;
  }
  
  public function search_user_schools($user, $allowed_schools, $state_id = 0, $district_id = 0, $block_id = 0, $cluster_id = 0, $village_id = 0)
  {
    if ($allowed_schools == 1)
    {      
      if ($state_id > 0)
        $this->defaultdb->where('schools.state_id',$state_id);
      
      if ($district_id > 0)
        $this->defaultdb->where('schools.district_id',$district_id);
      
      if ($block_id > 0)
        $this->defaultdb->where('schools.block_id',$block_id);
      
      if ($cluster_id > 0)
        $this->defaultdb->where('schools.cluster_id',$cluster_id);
      
      if ($village_id > 0)
        $this->defaultdb->where('schools.village_id',$village_id);
        
      $this->defaultdb->order_by("name", "asc");
      $query = $this->defaultdb->get('schools');
      
      return $query->result();
    }
    else
    { 
      $this->defaultdb->where_in('id', array_keys($allowed_schools));
      
      if ($state_id > 0)
        $this->defaultdb->where('schools.state_id',$state_id);
      
      if ($district_id > 0)
        $this->defaultdb->where('schools.district_id',$district_id);
      
      if ($block_id > 0)
        $this->defaultdb->where('schools.block_id',$block_id);
      
      if ($cluster_id > 0)
        $this->defaultdb->where('schools.cluster_id',$cluster_id);
      
      if ($village_id > 0)
        $this->defaultdb->where('schools.village_id',$village_id);
        
      $this->defaultdb->order_by("name", "asc");
      $query = $this->defaultdb->get('schools');
      
      return $query->result();
    }        
  }
  
  
  public function get_all_school($village = 0, $limit = 0, $offset = 0)
  {
    if ($village > 0)
      $this->defaultdb->where('schools.village_id',$village);
      
    $this->defaultdb->order_by("id", "desc");
    if ($limit != "")
      $query = $this->defaultdb->get('schools', $limit, $offset);
    else
      $query = $this->defaultdb->get('schools');
    return $query->result();
  }
  
  public function get_question($id,$user)
  {
    $query = $this->defaultdb->query("select * from sv_questions where school_id = ".$id." and id not in (select question_id from sv_responses where school_id = ".$id." and user_id = ".$user.") limit 1");
   # return $query->result();
    return $query->row_array();	
  }


  public function get_all_school_count()
  {
    $query = $this->defaultdb->count_all_results('schools');
    return $query;
  }
  
  public function get_village_schools($village, $limit = 5, $offset = 0)
  {
    $this->defaultdb->order_by("name", "asc");
    if ($limit != "")
      $query = $this->defaultdb->get_where('schools', array('village_id' => $village), $limit, $offset);
    else
      $query = $this->defaultdb->get_where('schools', array('village_id' => $village));
    return $query->result();
  }

  public function insert_school_to_db($data)
  {
    $this->defaultdb->insert('schools', $data);
    return $this->defaultdb->insert_id();
  }

  public function insert_school_selection($data)
  {
    return $this->defaultdb->insert('school_selections', $data);
  }

  public function delete_school_selection($user_id)
  {
    $this->defaultdb->where('school_selections.user_id',$user_id);
    return $this->defaultdb->delete('school_selections');
  }

  public function update_school_selection($data,$user_id)
  {
    $this->defaultdb->where('school_selections.user_id',$user_id);
    return $this->defaultdb->update('school_selections', $data);
  }

  public function getById($id)
  {
    error_log("--Inside school model/getById() method--- id=".$id,0);
    $query = $this->defaultdb->get_where('schools',array('id'=>$id));
    return $query->row_array();		  
  }
  
  public function getByDisecode($id)
  {
    $query = $this->defaultdb->get_where('schools',array('dise_code'=>$id));
    return $query->row_array();		  
  }

  public function update_info($data,$id)
  {
    $this->defaultdb->where('schools.id',$id);
    return $this->defaultdb->update('schools', $data);
  }

  public function delete_a_school($id)
  {
    $this->defaultdb->where('schools.id',$id);
    return $this->defaultdb->delete('schools');
  }
  
  public function get_school($state_id, $limit = 5, $offset = 0)
  {
    $this->defaultdb->order_by("name", "asc");
    $this->defaultdb->select("name");
    $this->defaultdb->select("dise_code");
    if ($limit != "")
      $query = $this->defaultdb->get_where('schools', array('state_id' => $state_id), $limit, $offset);
    else
      $query = $this->defaultdb->get_where('schools', array('state_id' => $state_id));
    return $query->result();
  }
  
  public function get_school_auto($q, $school_ids, $cluster_id = 0){
    $this->defaultdb->select('*');
    $this->defaultdb->where_in('schools.id',$school_ids);    
    if ($q != "")
    {
      $this->defaultdb->like('dise_code', $q, 'after');
    }
    if ($cluster_id > 0)
    {
      $this->defaultdb->where_in('schools.cluster_id',$cluster_id);    
    }
    $query = $this->defaultdb->get_where('schools');
    if($query->num_rows > 0){
      foreach ($query->result_array() as $row){
       $row1['label'] = htmlentities(stripslashes($row['dise_code']." (".$row['name'].")"));
       $row1['value'] = htmlentities(stripslashes($row['dise_code']." (".$row['name'].")"));
       $row1['state_id'] = $row['state_id'];
       $row1['district_id'] = $row['district_id'];
       $row1['block_id'] = $row['block_id'];
       $row1['cluster_id'] = $row['cluster_id'];
       $row1['village_id'] = $row['village_id'];
       $row1['school_id'] = $row['id'];
       $row1['id'] = $row['id'];
       $row_set[] = $row1;
      }
      echo json_encode($row_set); //format the array into json data
    }
  }
  
  
  
  
  public function get_cluster_school_auto($q, $cluster_id = 0){
    $this->defaultdb->select('*');
    if ($cluster_id > 0)
      $this->defaultdb->where('cluster_id', $cluster_id);
    
    $this->defaultdb->like('dise_code', $q);
    $query = $this->defaultdb->get_where('schools');
    if($query->num_rows > 0){
      foreach ($query->result_array() as $row){
       $row1['label'] = $row['dise_code']." (".$row['name'].")";
       $row1['value'] = $row['dise_code'];
       $row1['name'] = $row['name'];
       $row1['id'] = $row['id'];
       $row_set[] = $row1;
      }
      echo json_encode($row_set); //format the array into json data
    }
  }
  
  public function get_block_school_auto($q, $block_id = 0){
    $this->defaultdb->select('*');
    if ($block_id > 0)
      $this->defaultdb->where('block_id', $block_id);
    
    $this->defaultdb->like('dise_code', $q);
     $query = $this->defaultdb->get_where('schools');
    if($query->num_rows > 0){
      foreach ($query->result_array() as $row){
       $row1['label'] = $row['dise_code']." (".$row['name'].")";
       $row1['value'] = $row['dise_code'];
       $row1['name'] = $row['name'];
       $row1['id'] = $row['id'];
       $row_set[] = $row1;
      }
      echo json_encode($row_set); //format the array into json data
    }
  }
  
  public function get_by_block_ids($block_ids, $status = ''){
	                                 
    $this->defaultdb->where_in('block_id',$block_ids);
    if($status != '')
			$this->defaultdb->where('status', $status);
	  $query = $this->defaultdb->get_where('schools');
    
	  return $query->result();
  }
  
  public function getByIds($ids)
  {
    $this->defaultdb->where_in('id',$ids);
    $query = $this->defaultdb->get('schools');
    return $query->result();		  
  }  
  
  public function get_school_district_avg($state_id = 0, $district_id = 0)
  {
    if ($district_id > 0)
      $cond = " and district_id = ".$district_id;
    elseif ($state_id > 0)
      $cond = " and state_id = ".$state_id;
    else
      $cond = "";
      
    $query = $this->defaultdb->query("SELECT district_id, (avg(no_of_students) + 0.1 * avg(no_of_students)) AS AVG_VAL FROM ssc_schools where status = 1 ".$cond." GROUP by district_id ");
    return $query->result();	
  }  
  
  public function set_high_enroll_school($district_id, $avg)
  {
    $update_avg = "UPDATE ssc_schools SET avg_student='$avg' where district_id = '$district_id' and status = 1 ";
    $this->defaultdb->query($update_avg);
    
    $update = "UPDATE ssc_schools SET is_high_enrollment =1 where district_id = '$district_id' and no_of_students >= '$avg' and status = 1 ";
    $this->defaultdb->query($update);
  } 
  
  public function reset_school_rating($state_id = 0, $district_id = 0)
  {
    if ($district_id > 0)
      $cond = "where district_id = ".$district_id;
    elseif ($state_id > 0)
      $cond = "where state_id = ".$state_id;
    else
      $cond = "";
    
    $update = "UPDATE ssc_schools SET is_high_enrollment =0 ".$cond;
    $this->defaultdb->query($update);
  }
  
  
  public function get_total_school_count($state_id = array(), $district_ids = array(), $block_ids = array(), $cluster_ids = array(), $school_ids = array())
  {
    if (!empty($state_ids)){
			$select_field = " state_id ";
			$group_by = " state_id ";
      $this->defaultdb->where_in('state_id',$state_ids);
		}
    if (!empty($district_ids)){
			$select_field = " district_id ";
			$group_by = " district_id ";
      $this->defaultdb->where_in('district_id',$district_ids);
		}
    if (!empty($block_ids)){
			$select_field = " block_id ";
			$group_by = " block_id ";
      $this->defaultdb->where_in('block_id',$block_ids);
		}
    if (!empty($cluster_ids)){
			$select_field = " cluster_id ";
			$group_by = " cluster_id ";
      $this->defaultdb->where_in('cluster_id',$cluster_ids);
		}
    if (!empty($school_ids)){
			$select_field = " id ";
			$group_by = " ";
      $this->defaultdb->where_in('id',$school_ids);
		}
    $this->defaultdb->where('status',1);  
    $this->defaultdb->select(" count(id) as TOTAL_RECORD, $select_field as field_id ");
    if($group_by != '')
			$this->defaultdb->group_by($group_by);
    $query = $this->defaultdb->get('schools');
    return $rowcount = $query->result();
  }
  
  public function addSSSAppUsage($data) {
		$this->defaultdb->insert('app_usage_details', $data);
		$return_id = $this->defaultdb->insert_id();
		//echo"<br>".$this->defaultdb->last_query();
    return $return_id;
	}
	
	public function removeSSSAppUsage($month_year)
	{
		$this->defaultdb->where('month_year',$month_year);
    return $this->defaultdb->delete('ssc_app_usage_details');
	}
  
  public function get_school_ack_delivery($select_field ='', $field_cond = '', $field_val='')
  {
		$sel = "SELECT count(sss.id) as TOTAL_USER, sss.$select_field as field_id FROM `ssc_schools` sss, ssc_training_acknowledge_delivery as ssa WHERE sss.id = ssa.school_id AND sss.$field_cond = '$field_val' GROUP BY sss.$select_field";
		
		$query = $this->defaultdb->query($sel);
		return $query->result();	
	}
	
  public function get_school_app_usage($select_field ='', $field_cond = '', $field_val='', $month_year=array(), $usertype='')
  {
		$sel = "SELECT count(ssa.id) as TOTAL_REGISTERED, ssa.$select_field AS field_id, SUM(items_viewed) as total_item, SUM(video_viewed) as video_hr FROM ssc_app_usage_details ssa  WHERE ssa.$field_cond = '$field_val' "; 
		if(!empty($month_year)){
			$monthyear = implode("','", $month_year);
			$sel .= " AND ssa.month_year IN('$monthyear') ";
		}
		if($usertype != '')
			$sel .= " AND ssa.usertype = '$usertype' ";
		$sel .= " GROUP BY ssa.$select_field";
		$query = $this->defaultdb->query($sel);
		return $query->result();	
	}
	
	public function get_teacher_gr_1hr($select_field ='', $field_cond = '', $field_val='', $month_year=array(), $usertype='')
  {
		$sel = "SELECT count(ssa.id) as Total_teacher, ssa.$select_field AS field_id FROM ssc_app_usage_details ssa WHERE ssa.$field_cond = '$field_val' AND video_viewed > 1 ";
		if(!empty($month_year)){
			$monthyear = implode("','", $month_year);
			$sel .= " AND ssa.month_year IN('$monthyear') ";
		}
		if($usertype != '')
			$sel .= " AND ssa.usertype = '$usertype' ";
		$sel .= " GROUP BY ssa.$select_field";
		$query = $this->defaultdb->query($sel);
		return $query->result();	
	}
  
	public function get_app_school_registered($distinct, $select_field ='', $field_cond = '', $field_val='', $date_range = array(), $usertype='')
  {
		$sel = "SELECT count(DISTINCT $distinct) AS TOTAL_SCHOOLS, $select_field AS field_id FROM ssc_app_registrations 
		WHERE $field_cond = '$field_val' ";
		if(!empty($date_range)){
			$sdate = $date_range[0];
			$edate = $date_range[1];
			$sel .= " and register_date between '$sdate' AND '$edate' ";
		}
		if($usertype != '')
			$sel .= " AND usertype = '$usertype' ";
		$sel .= " GROUP BY $select_field ";	
		$query = $this->defaultdb->query($sel);
		return $query->result();	
	}
	
	public function get_app_school_not_registered($select_field ='', $field_cond = '', $field_val='', $date_range = array())
  {
		$sdate = $date_range[0];
		$edate = $date_range[1];
		//$sel = "SELECT COUNT(dise_code) AS REGISTER_SCHOOLS, $select_field AS field_id FROM ssc_schools WHERE $field_cond = '$field_val' AND map_dise_code = 1 AND dise_code IN (SELECT DISTINCT diseCode FROM ssc_app_registrations WHERE $field_cond = '$field_val' and register_date between '$sdate' AND '$edate' and map_dise_code = 1) ";
		$sel = "SELECT COUNT(dise_code) AS REGISTER_SCHOOLS, $select_field AS field_id FROM ssc_schools WHERE $field_cond = '$field_val' AND map_dise_code = 1 AND app_register_date between '$sdate' AND '$edate' and status = 1 ";
		$sel .= " GROUP BY $select_field ";	
		$query = $this->defaultdb->query($sel);
		return $query->result();	
	}
	
	
	public function get_app_teacher_registered($select_field ='', $field_cond = '', $field_val='', $date_range=array(), $usertype='')
  {
		$sel = "SELECT count(DISTINCT phone_number) AS TOTAL_SCHOOLS, $select_field AS field_id FROM ssc_app_registrations 
		WHERE $field_cond = '$field_val' ";
		if(!empty($date_range)){
			$sdate = $date_range[0];
			$edate = $date_range[1];
			$sel .= " and register_date between '$sdate' AND '$edate' ";
		}
		if($usertype != '')
			$sel .= " AND usertype = '$usertype' ";
		$sel .= " GROUP BY $select_field ";	
		$query = $this->defaultdb->query($sel);
		return $query->result();	
	}
	
	
  /*public function get_app_school_registered($select_field ='', $field_cond = '', $field_val='', $month_year='')
  {
		$sel = "SELECT count(DISTINCT school_id) AS TOTAL_SCHOOLS, $select_field AS field_id FROM ssc_app_usage_details 
		WHERE $field_cond = '$field_val' ";
		if($month_year != '')
			$sel .= " and month_year = '$month_year' ";
		$sel .= " GROUP BY $select_field ";	
		$query = $this->defaultdb->query($sel);
		return $query->result();	
	}*/
	


  public function get_active_teachers($select_field ='', $field_cond = '', $field_val='', $month_year='')
  {
		$sel = "SELECT count(ssa.id) as Total_teacher, ssa.$select_field AS field_id FROM ssc_app_usage_details ssa WHERE ssa.$field_cond = '$field_val' AND video_viewed > 0 ";
		if($month_year != '')
			$sel .= " AND ssa.month_year = '$month_year' ";
		$sel .= " GROUP BY ssa.$select_field";
		$query = $this->defaultdb->query($sel);
		return $query->result();	
	}
  
	public function get_app_usage($month_year='', $cluster_id, $usertype='')
  {
		//$sel = "SELECT ssa.*, sss.name as school_name FROM `ssc_schools` sss, ssc_app_usage_details as ssa WHERE sss.dise_code = ssa.diseCode AND ssa.cluster_id = '$cluster_id'";
		//$sel = "SELECT ssa.*, ssa.diseCode as school_name FROM ssc_app_usage_details as ssa WHERE ssa.cluster_id = '$cluster_id'";
		$sel = "SELECT ssa.*, sss.name as school_name FROM `ssc_schools` sss RIGHT JOIN ssc_app_usage_details as ssa ON sss.dise_code = ssa.diseCode WHERE ssa.cluster_id = '$cluster_id' ";
		if($month_year != '')
			$sel .= " AND ssa.month_year = '$month_year' ";
		if($usertype != '')
			$sel .= " AND ssa.usertype = '$usertype' ";
		//echo"<br>".$sel;	
		$query = $this->defaultdb->query($sel);
		return $query->result();	
	}
	
	public function get_app_usage_sparks($month_year='', $cluster_id, $usertype='')
  {
		//$sel = "SELECT ssa.*, sss.name as school_name FROM `ssc_schools` sss, ssc_app_usage_spark_details as ssa WHERE sss.dise_code = ssa.diseCode AND ssa.cluster_id = '$cluster_id'";
		//$sel = "SELECT ssa.*, ssa.diseCode as school_name FROM ssc_app_usage_spark_details as ssa WHERE ssa.cluster_id = '$cluster_id'";
		$sel = "SELECT ssa.*, sss.name as school_name FROM `ssc_schools` sss RIGHT JOIN ssc_app_usage_spark_details as ssa ON sss.dise_code = ssa.diseCode WHERE ssa.cluster_id = '$cluster_id' "; 
		if($month_year != '')
			$sel .= " AND ssa.month_year = '$month_year' ";
		if($usertype != '')
			$sel .= " AND ssa.usertype = '$usertype' ";
		//echo"<br>".$sel;	
		$query = $this->defaultdb->query($sel);
		return $query->result();	
	}
	
	function get_spark_appusage($block_ids=array(), $month_year=array(), $group_by='', $video_viewed_hr='', $usertype='')
	{
		//$this->defaultdb->select('count(id) as TOTAL_RESULTS');
		if(!empty($block_ids))
			$this->defaultdb->where_in('block_id',$block_ids);
		if(!empty($month_year))	
			$this->defaultdb->where_in('month_year',$month_year);
		if($video_viewed_hr != '')
			$this->defaultdb->where('video_viewed >', $video_viewed_hr);
		if($usertype != '')
			$this->defaultdb->where('usertype', $usertype);
		if($group_by != '')	
			$this->defaultdb->group_by($group_by);
		$query = $this->defaultdb->get('app_usage_details');
    return $query->num_rows();
	}
	
	//function to insert spark sss app usage details
  public function addSparkAppUsage($data) {
		$this->defaultdb->insert('app_usage_spark_details', $data);
		$return_id = $this->defaultdb->insert_id();
		//echo"<br>".$this->defaultdb->last_query();
    return $return_id;
	}
	
	//function to remove spark sss app usage details
	public function removeSparkAppUsage($month_year)
	{
		$this->defaultdb->where('month_year',$month_year);
    return $this->defaultdb->delete('app_usage_spark_details');
	}
	
	function get_spark_app_data($login_id, $month_year)
	{
		$this->defaultdb->select('SUM(total_posts) as POST_COUNT, SUM(total_comments) AS COMMENT_COUNT, SUM(total_likes) AS LIKE_COUNT ');
		$this->defaultdb->where('empCode', $login_id);
		$this->defaultdb->where_in('month_year', $month_year);
		$this->defaultdb->group_by('empCode');
		$query = $this->defaultdb->get('app_usage_spark_details');
		//echo"<br>".$this->defaultdb->last_query();
    return $query->row_array();
	}
	
	function get_spark_app_block_data($block_ids, $month_year, $usertype='')
	{
		$this->defaultdb->select('SUM(total_posts) as POST_COUNT, SUM(total_comments) AS COMMENT_COUNT, SUM(total_likes) AS LIKE_COUNT ');
		$this->defaultdb->where_in('block_id', $block_ids);
		$this->defaultdb->where_in('month_year', $month_year);
		if($usertype != '')
				$this->defaultdb->where('usertype', $usertype);
		$query = $this->defaultdb->get('app_usage_details');
		//echo"<br>".$this->defaultdb->last_query();
    return $query->row_array();
	}
	
	public function get_schools_by_disecode($disecode=array())
  {
    $this->defaultdb->select("id, dise_code");
    if(!empty($disecode))
			$this->defaultdb->where_in('dise_code', $disecode);
    $this->defaultdb->order_by("id", "desc");
    $query = $this->defaultdb->get('schools');
    return $query->result();
  }
  
  public function get_school_teachers($conds)
  {
    $this->defaultdb->where($conds);
    $query = $this->defaultdb->get('school_teachers');
    return $query->result();
  }
  
  public function add_school_teachers($data) {
		$this->defaultdb->insert('school_teachers', $data);
		return $this->defaultdb->insert_id();
	}
	
	public function update_school_teachers($data,$id)
  {
    $this->defaultdb->where('id',$id);
    $this->defaultdb->update('school_teachers', $data);
    //echo"<br>".$this->defaultdb->last_query();
    return;
  }
  
  public function get_report_school_count($report_type='', $district_ids = array(), $block_ids = array(), $cluster_ids = array())
  {
    if (!empty($district_ids)){
			$select_field = " district_id ";
			$group_by = " district_id ";
      $this->defaultdb->where_in('district_id',$district_ids);
		}
    if (!empty($block_ids)){
			$select_field = " block_id ";
			$group_by = " block_id ";
      $this->defaultdb->where_in('block_id',$block_ids);
		}
    if (!empty($cluster_ids)){
			$select_field = " cluster_id ";
			$group_by = " cluster_id ";
      $this->defaultdb->where_in('cluster_id',$cluster_ids);
		}
		
		if($report_type == 'call_status')		
			$this->defaultdb->select(" SUM(last_status_count) as TOTAL_RECORD, $select_field as field_id ");
    else
			$this->defaultdb->select(" count(id) as TOTAL_RECORD, SUM(last_status_count) as TOTAL_CALL, $select_field as field_id ");
    if($group_by != '')
			$this->defaultdb->group_by($group_by);
    $query = $this->defaultdb->get('school_teachers');
    return $rowcount = $query->result();
  }
  
  /*public function get_not_register_schools($cluster_id)
  {
		$sel = "SELECT ssa.*, sss.name as school_name FROM `ssc_schools` sss, ssc_school_teachers as ssa WHERE sss.id = ssa.school_id AND sss.cluster_id = '$cluster_id' AND ssa.contact_number NOT IN (SELECT DISTINCT phone_number FROM ssc_app_registrations WHERE cluster_id = '$cluster_id')";
		
		$query = $this->defaultdb->query($sel);
		return $query->result();	
	}*/
	
	public function get_not_register_schools($cluster_id)
  {
		$sel = "SELECT ssa.* FROM ssc_school_teachers as ssa WHERE ssa.cluster_id = '$cluster_id' AND map_flag=0 AND ssa.contact_number NOT IN (SELECT DISTINCT phone_number FROM ssc_app_registrations WHERE cluster_id = '$cluster_id')";
		
		$query = $this->defaultdb->query($sel);
		return $query->result();	
	}
	
	function get_ack_delivery_data()
	{
		$sql = "SELECT tad.id, tad.school_id, sss.name, sss.state_id, sss.district_id, sss.block_id, sss.cluster_id, sss.dise_code, tad.teacher_name, tad.contact_no, tad.issue_date FROM `ssc_training_acknowledge_delivery` tad, ssc_schools sss WHERE tad.school_id = sss.id AND school_teachers_flag=0 limit 10 ";
    
    $query = $this->defaultdb->query($sql);
    return $query->result();
		
	} 	
	
	public function update_ack_info($data,$id)
  {
    $this->defaultdb->where('id',$id);
    return $this->defaultdb->update('training_acknowledge_delivery', $data);
  }
  
  public function check_teacher_registraion($cond)
  {
		$this->defaultdb->where($cond);
		$query = $this->defaultdb->get('app_registrations');
		//echo"<br>".$this->defaultdb->last_query();
		return $query->result();
	}
	
	public function add_teacher_registraion($data)
  {
    $this->defaultdb->insert('app_registrations', $data);
    $id = $this->defaultdb->insert_id();
    //echo"<br>".$this->defaultdb->last_query();
    return $id;
  }
	
	function get_teacher_regristerd($block_ids=array(), $start_date='', $end_date='', $usertype = '')
	{
		$blockids = implode(',',$block_ids);
		
		$sel = "SELECT DISTINCT phone_number FROM ssc_app_registrations WHERE block_id in ($blockids) ";
		if($start_date != '' && $end_date != ''){
			$start_date = date('Y-m-d', strtotime($start_date));
			$end_date = date('Y-m-d', strtotime($end_date));
			$sel .= " and register_date BETWEEN '$start_date' AND '$end_date' ";
		}
		if($usertype != '')
			$sel .= " AND usertype = '$usertype' ";
		$query = $this->defaultdb->query($sel);
		return $query->num_rows();
	}
	
	function get_last_teacher_regristerd()
	{
		$sql = "SELECT MAX(updated_at) AS LAST_UPDATE from ssc_app_registrations";
		$query = $this->defaultdb->query($sql);
		return $query->row_array();
	}
	
	public function get_ack_school_registered($distinct, $select_field ='', $field_cond = '', $field_val='', $month_year='')
  {
		$sel = "SELECT count(sar.$distinct) AS TOTAL_SCHOOLS, sar.$select_field AS field_id FROM ssc_app_registrations sar, ssc_school_teachers sst 
		WHERE sar.$field_cond = '$field_val' AND  sst.$field_cond = '$field_val' AND sar.$select_field = sst.$select_field AND sst.contact_number = sar.phone_number ";
		if($month_year != ''){
			$exp = explode('_',$month_year);
			$register_date = $exp[1]."-".$exp[0];
			$sel .= " and sar.register_date like '".$register_date."%' ";
		}
		$sel .= " GROUP BY sar.$select_field ";	
		$query = $this->defaultdb->query($sel);
		return $query->result();	
	}
	
	
	public function update_data($table, $data, $cond)
  {
    $this->defaultdb->where($cond);
    $result = $this->defaultdb->update($table, $data);
    //echo"<br>".$this->defaultdb->last_query();
    return $result;
  }	
  
  public function get_data($table, $cond, $limit = 0)
  {
    $this->defaultdb->where($cond);
    if ($limit > 0)
      $query = $this->defaultdb->get($table, $limit);
    else
      $query = $this->defaultdb->get($table);
    
	  $result = $query->result();
	  //echo $query = $this->defaultdb->last_query();
	  return $result;
  }
	
	public function get_unmapped_disecode($limit = 0)
  {
		$sql = "SELECT DISTINCT diseCode from ssc_app_registrations where map_dise_code = 0 ";
		//$sql .= " limit 0, 5000 ";
	  $query = $this->defaultdb->query($sql);
		return $query->result();	
  }	
  
  public function get_mapped_disecode_date($dise_code)
  {
		$sql = "SELECT register_date from ssc_app_registrations where map_dise_code = 1 and diseCode = '$dise_code' order by id limit 0,1";
	  $query = $this->defaultdb->query($sql);
		return $query->row_array();	
  }	
  
  public function add_data($table, $data)
  {
    $this->defaultdb->insert($table, $data);
    $id = $this->defaultdb->insert_id();
    //echo"<br>".$this->defaultdb->last_query();
    return $id;
  }
  
  public function remove_date($table, $cond)
	{
		$this->defaultdb->where($cond);
    return $this->defaultdb->delete($table);
	}
	
	public function get_ack_school_not_registered($distinct, $select_field ='', $field_cond = '', $field_val='')
  {
		$sel = "SELECT count(sst.$distinct) AS TOTAL_SCHOOLS, sst.$select_field AS field_id FROM ssc_school_teachers sst 
		WHERE sst.$field_cond = '$field_val' AND sst.map_flag = 0 ";
		$sel .= " GROUP BY sst.$select_field ";	
		$query = $this->defaultdb->query($sel);
		return $query->result();	
	}
	
	public function get_app_registered($start_date='', $end_date='', $cluster_id='', $usertype='')
  {
		$sel = "SELECT ssa.*, sss.name as school_name FROM `ssc_schools` sss RIGHT JOIN ssc_app_registrations as ssa ON sss.dise_code = ssa.diseCode WHERE ssa.cluster_id = '$cluster_id' ";
		if($start_date != '' && $end_date != '')
			$sel .= " AND ssa.register_date between '$start_date' AND '$end_date' ";
		if($usertype != '')
			$sel .= " AND ssa.usertype = '$usertype' ";
		//echo"<br>".$sel;	
		$query = $this->defaultdb->query($sel);
		return $query->result();	
	}
	
	public function get_app_registered_teachers($start_date='', $end_date='', $field_name = '', $field_id='', $usertype='')
  {
		$sel = "SELECT ssa.*, sss.name as school_name FROM `ssc_schools` sss RIGHT JOIN ssc_app_registrations as ssa ON sss.dise_code = ssa.diseCode WHERE ssa.$field_name = '$field_id' ";
		if($start_date != '' && $end_date != '')
			$sel .= " AND ssa.register_date between '$start_date' AND '$end_date' ";
		if($usertype != '')
			$sel .= " AND ssa.usertype = '$usertype' ";
		$sel .= " group by ssa.phone_number ";
		//echo"<br>".$sel;	
		$query = $this->defaultdb->query($sel);
		return $query->result();	
	}
  
  function get_not_registered_schools($field_name, $record_id)
  {
		$this->defaultdb->join('states', 'states.id = schools.state_id');
    $this->defaultdb->join('districts', 'districts.id = schools.district_id');
    $this->defaultdb->join('blocks', 'blocks.id = schools.block_id');
    $this->defaultdb->join('clusters', 'clusters.id = schools.cluster_id');
    $this->defaultdb->select('states.name as state_name');
    $this->defaultdb->select('districts.name as district_name');
    $this->defaultdb->select('clusters.name as cluster_name');
    $this->defaultdb->select('blocks.name as block_name');
    $this->defaultdb->select('schools.*');
    $this->defaultdb->where("schools.app_register_date  IS NULL");
    $this->defaultdb->where("schools.status",1);
    $this->defaultdb->where("schools.$field_name",$record_id);
    $this->defaultdb->where("schools.$field_name",$record_id);
    $this->defaultdb->order_by("schools.name asc");
    $query = $this->defaultdb->get('schools');
    return $query->result();
	}
	
	public function get_teacher_not_registered($field_name ='', $field_val='')
  {
		$sel = "SELECT * FROM ssc_school_teachers sst 
		WHERE sst.$field_name = '$field_val' AND sst.map_flag = 0 ";
		$sel .= " GROUP BY sst.contact_number ";	
		$query = $this->defaultdb->query($sel);
		return $query->result();	
	}
	
	function get_school_regristerd($block_ids=array(), $start_date='', $end_date='', $usertype = '')
	{
		$blockids = implode(',',$block_ids);
		
		$sel = "SELECT DISTINCT diseCode FROM ssc_app_registrations WHERE block_id in ($blockids) ";
		if($start_date != '' && $end_date != ''){
			$start_date = date('Y-m-d', strtotime($start_date));
			$end_date = date('Y-m-d', strtotime($end_date));
			$sel .= " and register_date BETWEEN '$start_date' AND '$end_date' ";
		}
		if($usertype != '')
			$sel .= " AND usertype = '$usertype' ";
		$query = $this->defaultdb->query($sel);
		return $query->num_rows();
	}
	
}
?>	
