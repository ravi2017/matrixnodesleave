<?php
//error_reporting(0);
class Team_members_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        //Enable profiler
        $this->output->enable_profiler(false);
        $this->defaultdb = $this->load->database('default', TRUE);
    }

    public function get_member_districts($member)
    {        
      $this->defaultdb->select('districts.*');
      //
      $this->defaultdb->join('spark_users_district_block','spark_users_district_block.district_id = districts.id');
      $this->defaultdb->where('spark_users_district_block.user_id',$member);
      $this->defaultdb->order_by("id", "desc");
      $this->defaultdb->group_by("district_id");
      $query = $this->defaultdb->get('districts');
      return $query->result();
    }

    public function get_member_blocks($member,$district)
    {        
      $this->defaultdb->select('blocks.*');
      $this->defaultdb->join('spark_users_district_block','spark_users_district_block.block_id = blocks.id');
      $this->defaultdb->where('spark_users_district_block.user_id',$member);
      $this->defaultdb->where('spark_users_district_block.district_id',$district);
      $this->defaultdb->order_by("id", "desc");
      //$this->defaultdb->group_by("block_id");
      $query = $this->defaultdb->get('blocks');
      //echo $this->defaultdb->last_query();
      return $query->result();
    }

    public function get_state_users($state_id, $role = ''){
        $this->defaultdb->select('name, id, role, login_id');
        if ($role != "")
        {
          $this->defaultdb->where('role', $role);
        }
        $this->defaultdb->where('state_id', $state_id);
        //$this->defaultdb->where('role', 'state_person');
        //$this->defaultdb->or_where('role', 'manager');
        $res = $this->defaultdb->get('sparks');
        return $res->result();
    }

    public function get_state_persons_managers(){
        $this->defaultdb->select('name, id, state_id');
        $this->defaultdb->where('role', 'state_person');
        $this->defaultdb->or_where('role', 'manager');
        $res = $this->defaultdb->get('sparks');
        return $res->result();
    }

    public function save_spark_district_block($user_id, $data){
        $this->defaultdb->insert('spark_users_district_block', array('user_id'=>$user_id, 'district_id'=>$data[0], 'block_id'=>$data[1]));
    }

    public function delete_spark_district_block($user_id, $data){
      if (count($data) == 2)
        $this->defaultdb->where(array('user_id'=>$user_id, 'district_id'=>$data[0], 'block_id'=>$data[1]));
      else
        $this->defaultdb->where(array('user_id'=>$user_id));
        $this->defaultdb->delete('spark_users_district_block');
        //echo $this->defaultdb->last_query(); exit;
    }

    public function get_all_team_members($id){
        $this->defaultdb->select('sparks.*, u.name as manager')->from('sparks')->join('sparks u', 'sparks.manager_id = u.id');
        $this->defaultdb->where('sparks.manager_id', $id)->order_by('sparks.name asc');
        $query = $this->defaultdb->get();
        return $query->result();
    }

    public function get_team($id){
        $this->defaultdb->select('sparks.*, u.name as manager')->from('sparks')->join('sparks u', 'sparks.manager_id = u.id');
        $this->defaultdb->where('sparks.manager_id', $id);
        return $this->defaultdb->get()->result();
    }

    public function check_team($id){
        $this->defaultdb->select('count(ssc_sparks.id) as teamcount')->from('sparks')->join('sparks u', 'sparks.manager_id = u.id');
        $this->defaultdb->where('sparks.manager_id', $id);
        $data = $this->defaultdb->get()->result();
        return $data[0]->teamcount;
    }

    public function get_team_ids($id){
        $this->defaultdb->select('sparks.id')->from('sparks')->join('sparks u', 'sparks.manager_id = u.id');
        $this->defaultdb->where('sparks.manager_id', $id);
        return $this->defaultdb->get()->result();
    }

    public function get_member_district_ids($id){
        $this->defaultdb->select('district_id');
        $this->defaultdb->where('user_id', $id);
        return $this->defaultdb->get('spark_users_district_block')->result();
    }

    public function get_member_block_ids($id){
        $this->defaultdb->select('block_id');
        $this->defaultdb->where('user_id', $id);
        return $this->defaultdb->get('spark_users_district_block')->result();
    }

    public function check_user_id($id){
      $count = $this->db->select('*')->where('login_id', $id)->get('sparks')->num_rows();
      if($count == 0)
        return 1;
      else
        return 0;

    }

    public function change_manager($data){
      $this->db->where('id', $data['spark_id'])->update('sparks', array('manager_id'=>$data['manager_id']));
      $this->db->where('user_id', $data['spark_id'])->delete('spark_users_district_block');
    }
    
    public function temp_script_blocks(){
      $users = $this->defaultdb->select('id, district_id')->get('sparks')->result();
      foreach ($users as $user) {
        $blocks = $this->defaultdb->select('id')->where('district_id', $user->district_id)->get('blocks')->result();
        foreach ($blocks as $block) {
          $data = array('user_id'=>$user->id, 'district_id'=>$user->district_id, 'block_id'=>$block->id);
          $this->defaultdb->insert('spark_users_district_block', $data);
        }
      }
      return 'done';
    }

    public function temp_script_managers(){
      $users = $this->defaultdb->select('id, state_id')->where('role', 'field_user')->get('sparks')->result();
      foreach ($users as $user) {
        $managers = $this->defaultdb->select('id')->where(array('state_id'=>$user->state_id, 'role'=>'state_person'))->get('sparks')->result();
        if(!empty($managers)){
          foreach ($managers as $manager) {
            $this->defaultdb->where('id', $user->id)->update('sparks', array('manager_id'=>$manager->id));
            break;
          }
        }
      }
      return 'done';
    }

    public function temp_script_activities(){
      $no_visit_days = $this->defaultdb->select('id')->where("activity_date <= ","2018-08-18 23:59:59")->get('no_visit_days')->result();
      $school_visits = $this->defaultdb->select('id')->where("activity_date <= ","2018-08-18 23:59:59")->get('school_visits')->result();
      $meetings = $this->defaultdb->select('id')->where("activity_date <= ","2018-08-18 23:59:59")->get('meetings')->result();
      $trainings = $this->defaultdb->select('id')->where("activity_date <= ","2018-08-18 23:59:59")->get('trainings')->result();
      $leaves = $this->defaultdb->select('id')->where("leave_end_date <= ","2018-08-18 23:59:59")->get('leaves')->result();

      foreach ($no_visit_days as $no_visit_day) {
        $this->defaultdb->where('id', $no_visit_day->id)->update('no_visit_days', array('status'=>"approved"));
      }

      foreach ($school_visits as $school_visit) {
        $this->defaultdb->where('id', $school_visit->id)->update('school_visits', array('status'=>"approved"));
      }

      foreach ($meetings as $meeting) {
        $this->defaultdb->where('id', $meeting->id)->update('meetings', array('status'=>"approved"));
      }

      foreach ($trainings as $training) {
        $this->defaultdb->where('id', $training->id)->update('trainings', array('status'=>"approved"));
      }

      foreach ($leaves as $leave) {
        $this->defaultdb->where('id', $leave->id)->update('leaves', array('status'=>"approved"));
      }

      return 'done';
    }

    public function temp_script_spark_states(){
      $users = $this->defaultdb->select('id, state_id')->where('status', 1)->get('sparks')->result();
      $old_users = $this->defaultdb->select('id, state_id')->where('status', 0)->get('sparks')->result();
      foreach ($users as $user) {
        $data['user_id'] = $user->id;
        $data['state_id'] = $user->state_id;
        $data['start_date'] = "2018-04-01";
        $this->defaultdb->insert('spark_user_state', $data);
      }
      foreach ($old_users as $old_user) {
        $data['user_id'] = $old_user->id;
        $data['state_id'] = $old_user->state_id;
        $data['start_date'] = "2018-04-01";
        $data['end_date'] = "2018-08-18";
        $this->defaultdb->insert('spark_user_state', $data);
      }
      return "done";
    } 
}
