<?php
class Teacher_feedback_model extends CI_Model {

  public function __construct()
  {
    parent::__construct();
    $this->defaultdb = $this->load->database('inventory', TRUE);
  }
  
  public function get_district_name($district_id)
  {
    $query = $this->defaultdb->query("SELECT * from ssc_districts where id = ".$district_id);
    return $query->result();
  }
  
  public function get_block_name($block_id)
  {
    $query = $this->defaultdb->query("SELECT * from ssc_blocks where id = ".$block_id);
    return $query->result();
  }
  
  public function get_questions()
  {
    $query = $this->defaultdb->query("SELECT * from tutorial order by id");
    return $query->result();
  }
  
  public function get_all($state_id = 0, $district_ids = array())
  {
    if ($state_id > 0)
      $state_cond = "ssc_issues.state_id = ".$state_id;
    else
      $state_cond = "ssc_issues.state_id > 0";
      
    if (count($district_ids) > 0)
      $district_cond = "ssc_issues.district_id in (".implode(',',$district_ids).")";
    else
      $district_cond = "ssc_issues.district_id > 0";
    $query = $this->defaultdb->query("SELECT ssc_issues.district_id, ssc_districts.name, tutorial_users.question_id, AVG(rating) as avg_rating FROM `tutorial_users` JOIN `ssc_issue_details` ON `tutorial_users`.`issue_detail_id` = `ssc_issue_details`.`id` JOIN `ssc_issues` ON `ssc_issue_details`.`issue_id` = `ssc_issues`.`id` JOIN ssc_districts ON ssc_districts.id = ssc_issues.district_id where $state_cond AND $district_cond GROUP BY `tutorial_users`.`question_id`, ssc_issues.district_id");
    return $query->result();
  }
  
  public function get_all_block_wise($state_id = 0, $district_ids = array())
  {
    if ($state_id > 0)
      $state_cond = "ssc_issues.state_id = ".$state_id;
    else
      $state_cond = "ssc_issues.state_id > 0";
      
    if (count($district_ids) > 0)
      $district_cond = "ssc_issues.district_id in (".implode(',',$district_ids).")";
    else
      $district_cond = "ssc_issues.district_id > 0";
    
    $query = $this->defaultdb->query("SELECT ssc_issues.block_id, ssc_blocks.name, tutorial_users.question_id, AVG(rating) as avg_rating FROM `tutorial_users` JOIN `ssc_issue_details` ON `tutorial_users`.`issue_detail_id` = `ssc_issue_details`.`id` JOIN `ssc_issues` ON `ssc_issue_details`.`issue_id` = `ssc_issues`.`id` JOIN ssc_blocks ON ssc_blocks.id = ssc_issues.block_id where $state_cond AND $district_cond GROUP BY `tutorial_users`.`question_id`, ssc_issues.block_id");
    return $query->result();
  }
}

?>
