<?php
class Person_details_model extends CI_Model {

  public function __construct()
  {
    parent::__construct();
    $this->defaultdb = $this->load->database('default', TRUE);
  }

  public function add($data)
  {
    $this->defaultdb->insert('ssc_person_details', $data);
    return $this->defaultdb->insert_id();
  }

 

  public function getById($id)
  {
    $query = $this->defaultdb->get_where('ssc_person_details',array('id'=>$id));
    return $query->row_array();		  
  }
}

?>
