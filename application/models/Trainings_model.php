<?php
class Trainings_model extends CI_Model {

  public function __construct()
  {
    parent::__construct();
    $this->defaultdb = $this->load->database('default', TRUE);
  }
  
  public function get_all_count($user_ids, $start_date, $end_date, $source='')
  {
    $this->defaultdb->where_in("user_id",$user_ids);
    $this->defaultdb->where("activity_date >= ",$start_date." 00:00:00");
    $this->defaultdb->where("activity_date <= ",$end_date." 23:59:59");
    $this->defaultdb->where("status = ","approved");
    if($source != '')
      $this->defaultdb->where("source", $source);
    $this->defaultdb->select("count(*) as total");
    $query = $this->defaultdb->get('trainings');
    return $query->row_array();
  }

  public function get_all($user_id, $start_date, $end_date)
  {
    $this->defaultdb->where("user_id",$user_id);
    $this->defaultdb->where("activity_date >= ",$start_date." 00:00:00");
    $this->defaultdb->where("activity_date <= ",$end_date." 23:59:59");
    $this->defaultdb->where("status = ","approved");
    $query = $this->defaultdb->get('trainings');
    //echo "<br>".$this->defaultdb->last_query();
    return $query->result();
  }

  public function get_approval_activities($user_id='', $start_date, $end_date, $status = '', $state_id='')
  {
    if($user_id != '')
      $this->defaultdb->where("user_id",$user_id);
    $this->defaultdb->where("activity_date >= ",$start_date." 00:00:00");
    $this->defaultdb->where("activity_date <= ",$end_date." 23:59:59");
    if($state_id != '')
      $this->defaultdb->where("state_id", $state_id);
    //if($status != 'all')
		//$this->defaultdb->where("status != ","approved");
    $this->defaultdb->order_by("activity_date", "asc");
    $query = $this->defaultdb->get('trainings');
    return $query->result();
  }

  public function add($data)
  {
    $this->defaultdb->insert('trainings', $data);
    return $this->defaultdb->insert_id();
  }

  public function getById($id)
  {
    $query = $this->defaultdb->get_where('trainings',array('id'=>$id));
    return $query->row_array();		  
  }
		public function get_district_training($district_id, $start_date, $end_date)
  {
    $this->defaultdb->where("district_id",$district_id);
    //$this->defaultdb->where("block_id",0);
    $this->defaultdb->where("activity_date >= ",$start_date);
    $this->defaultdb->where("activity_date <= ",$end_date);
    $query = $this->defaultdb->get('trainings');
    return $query->result();
  }
	public function get_block_training($block_id, $start_date, $end_date)
  {
    $this->defaultdb->where("block_id",$block_id);
    //$this->defaultdb->where("block_id",0);
    $this->defaultdb->where("activity_date >= ",$start_date);
    $this->defaultdb->where("activity_date <= ",$end_date);
    $query = $this->defaultdb->get('trainings');
    return $query->result();
  }
	public function get_traning($district_id,$user_id, $start_date, $end_date)
  {
    $this->defaultdb->where("district_id",$district_id);
    $this->defaultdb->where("user_id",$user_id);
    //$this->defaultdb->where("block_id",0);
    $this->defaultdb->where("activity_date >= ",$start_date);
    $this->defaultdb->where("activity_date <= ",$end_date);
		$this->defaultdb->select('count(*) as total');
    $query = $this->defaultdb->get('trainings');
    return $query->result();
  }
	public function get_user_traning($state_id,$district_id,$user_id, $start_date, $end_date)
  {
    $this->defaultdb->where("state_id",$state_id);
    $this->defaultdb->where("district_id",$district_id);
    $this->defaultdb->where("user_id",$user_id);
    //$this->defaultdb->where("block_id",0);
    $this->defaultdb->where("activity_date >= ",$start_date);
    $this->defaultdb->where("activity_date <= ",$end_date);
    $this->defaultdb->select('*');
		
    $query = $this->defaultdb->get('trainings');
    return $query->result();
  }
  
  public function getActivityInfoForTraining($user_id, $limit=10)
  {
    $this->defaultdb->where("user_id",$user_id);
    $this->defaultdb->order_by("id", "desc");
    $query = $this->defaultdb->get('trainings',$limit);
   
    return $query->result();
  
  }

  public function get_fullday_training($user_id, $start_date, $end_date)
  {
    $this->defaultdb->where("user_id",$user_id);
    $this->defaultdb->where("activity_date >= ",$start_date." 00:00:00");
    $this->defaultdb->where("activity_date <= ",$end_date." 23:59:59");
    $this->defaultdb->where("training_duration", "24:00");
    $query = $this->defaultdb->get('trainings');
    return $query->result();
  }

  public function get_halfday_training($user_id, $start_date, $end_date)
  {
    $this->defaultdb->where("user_id",$user_id);
    $this->defaultdb->where("activity_date >= ",$start_date." 00:00:00");
    $this->defaultdb->where("activity_date <= ",$end_date." 23:59:59");
    $this->defaultdb->where("training_duration <>", "24:00");
    $query = $this->defaultdb->get('trainings');
    return $query->result();
  }
  
  public function get_spark_telegram_count($spark_id, $telegram_flag=0)
  {
    $this->defaultdb->where("user_id",$spark_id);
    $this->defaultdb->where("telegram_flag", $telegram_flag)->order_by('activity_date','asc');
    $query = $this->defaultdb->get('trainings');
    //echo"<br>".$this->defaultdb->last_query();
    return $query->result();
  }
  
  public function get_training_date_data()
  {
      $query = "SELECT count(DISTINCT(Date(activity_date))) AS date_count, user_id FROM `ssc_trainings` GROUP BY user_id";
      $qry = $this->db->query($query); 
      $result = $qry->result();
      return $result;
  }

  public function get_training_teacher_data()
  {
      $query = "SELECT DISTINCT(user_id), SUM(no_of_teachers_trained) teacher_trained FROM `ssc_trainings` GROUP BY user_id";
      $qry = $this->db->query($query); 
      $result = $qry->result();
      return $result;
  }
  
  public function get_spark_training_report($spark_id='', $state_id='', $created_at='', $order_by='')
  {
    if($spark_id != '')
      $this->defaultdb->where("spark_id", $spark_id);
    if($state_id != '')
      $this->defaultdb->where("state_id", $state_id);  
    if($created_at != '')
      $this->defaultdb->where("created_at", $created_at);    
    if($order_by != '')
      $this->defaultdb->order_by($order_by);
    $query = $this->defaultdb->get('spark_training_report');
    //echo $this->defaultdb->last_query();      
    return $query->result();
  }
  
  public function save_spark_training_report($data)
  {
    $this->defaultdb->insert('spark_training_report', $data);
    return $this->defaultdb->insert_id();
  }
  
  public function update_spark_training_report($data, $spark_id) {
      $this->defaultdb->where('spark_id', $spark_id);
      $this->defaultdb->update('spark_training_report', $data);
  }    

  public function update_spark_training_ranking($data, $id) {
      $this->defaultdb->where('id', $id);
      $this->defaultdb->update('spark_training_report', $data);
  }    

  public function update_training_data($data, $id) {
      $this->defaultdb->where('id', $id);
      $this->defaultdb->update('trainings', $data);
  }
  
  function get_all_trainings($current_date='')
  {
		if($current_date != '')
			$this->defaultdb->where("Date(activity_date)", $current_date);
    $this->defaultdb->order_by("user_id", "desc");
    $query = $this->defaultdb->get('trainings');
   
    return $query->result();
      
  } 
  
  public function check_future_trainings($state_id)
  {
    $today = date("Y-m-d");
    $this->db->where(" status = 'active' and (start_date >= '".$today."' OR end_date >= '".$today."') and state_id = ".$state_id);   
    $this->db->order_by("start_date", "asc");
    $query = $this->db->get('training_availabilities');
    return $query->num_rows();
  } 
  
  function get_training_observations($state_id, $report_flag)
  {
    $this->db->where("status", 1);   
    $this->db->where("state_id", $state_id);   
    $this->db->where("report_flag", $report_flag);   
    $this->db->order_by("id");   
    $query = $this->db->get('observation_questions');
    return $query->result();
  }
  
  public function get_training_report($start_date, $end_date, $question_ids=array(), $state_id='', $district_id='', $block_id='')
  {
    $sdate = date('Y-m-d', strtotime($start_date));
    $edate = date('Y-m-d', strtotime($end_date));
    
    $select = "SELECT sst.*, ssa.question_id, ssa.answer FROM ssc_trainings sst JOIN ssc_assessment_new ssa ON sst.id=ssa.activity_id where activity_date between '$sdate' AND '$edate' ";
    if(!empty($question_ids)){
      $questionids = implode(',',$question_ids);
      $select .= " and ssa.question_id in ($questionids) "; 
    }
    if(!empty($state_id))
      $select .= " and sst.state_id = '$state_id' "; 
    if(!empty($district_id))  
      $select .= " and sst.district_id = '$district_id' ";
    if(!empty($block_id))  
      $select .= " and sst.block_id = '$block_id' ";
    echo $select;  
    $qry = $this->db->query($select); 
    $result = $qry->result();  
    return $result;
  }
  
  public function get_trained_teacher_data($district_id=array(), $block_id=array(), $cluster_id=array())
  {
    $slect_val = '';
    $group_by = '';
    $condition = '';
    if(!empty($district_id)){
        $slect_val = " district_id as filter_type ";
        $group_by = 'district_id';
        $district_ids = implode("','", $district_id);
        $condition = " district_id IN ('$district_ids') ";
    }
    if(!empty($block_id)){
        $slect_val = " block_id  as filter_type ";
        $group_by = 'block_id';
        $block_ids = implode("','", $block_id);
        $condition = " block_id IN ('$block_ids') ";
    }
    if(!empty($cluster_id)){
        $slect_val = " cluster_id  as filter_type ";
        $group_by = 'cluster_id';
        $cluster_ids = implode("','", $cluster_id);
        $condition = " cluster_id IN ('$cluster_ids') ";
    }
    $query = "SELECT $slect_val, subject_id, SUM(no_of_teachers_trained) teacher_trained FROM `ssc_trainings` where $condition GROUP BY subject_id, $group_by";
    $qry = $this->db->query($query); 
    $result = $qry->result();
    return $result;
  }
  
  function get_challan_assessment($start_date, $end_date, $state_id='', $district_id='', $block_id='')
  {
    $sdate = date('Y-m-d', strtotime($start_date));
    $edate = date('Y-m-d', strtotime($end_date));
    
    $query = "SELECT distinct answer FROM `ssc_assessment_new` WHERE question_id in (SELECT id FROM `ssc_observation_questions` WHERE `question` LIKE '%challan%' and `activity_type` = 'training' and state_id='$state_id') and answer != '' and activity_id IN ( ";
    $query .= "SELECT id FROM ssc_trainings sst where activity_date between '$sdate' AND '$edate' ";
    if(!empty($state_id))
      $query .= " and sst.state_id = '$state_id' "; 
    if(!empty($district_id))  
      $query .= " and sst.district_id = '$district_id' ";
    if(!empty($block_id))  
      $query .= " and sst.block_id = '$block_id' ";
    $query .= ")";
    $q = $this->db->query($query); 
    $result1 = $q->result();
    
    $training_no = array();
    foreach($result1 as $results)
    {
      if(!in_array($results->answer, $training_no))
        array_push($training_no, $results->answer);
    }
    $training_nos = implode("','",$training_no);
    
    $result = array();
    if($training_nos != ''){
      $select = "select spark_id, training_request_id, issue_date from ssc_training_acknowledge_delivery where training_request_id IN (select id from ssc_sampark_training_details where training_no IN('$training_nos')) and issue_date between '$sdate' AND '$edate' group by spark_id, training_request_id, issue_date";  
      $qry = $this->db->query($select); 
      $result = $qry->result();
    }
    return $result;
  }
  
  public function get_trainings_report($spark_ids = array(), $activity_dates = array(), $question_ids=array())
  {
    $sparkids = implode(',', $spark_ids);
    $activitydates = implode("','", $activity_dates);
    
    $select = "SELECT sst.*, ssa.question_id, ssa.answer FROM ssc_trainings sst JOIN ssc_assessment_new ssa ON sst.id=ssa.activity_id 
    where sst.user_id IN ($sparkids) AND Date(sst.activity_date) IN ('$activitydates') ";
    if(!empty($question_ids)){
      $questionids = implode(',',$question_ids);
      $select .= " and ssa.question_id in ($questionids) "; 
    }
    //echo $select;  
    $qry = $this->db->query($select); 
    $result = $qry->result();  
    return $result;
  }
  
  function get_previous_activity($spark_id, $current_date)
	{
		$sql = "SELECT * from ssc_trainings where user_id = '$spark_id' and activity_date < '$current_date' order by activity_date desc limit 1 ";
		$qry = $this->db->query($sql); 
    $result = $qry->row_array();  
    return $result;
	}	
	
	function get_dates_novisit_days($spark_id, $date_arr)		
	{
		$this->defaultdb->where("user_id", $spark_id);
		$this->defaultdb->where_in("activity_date", $date_arr);
    $this->defaultdb->order_by("id", "desc");
    $query = $this->defaultdb->get('no_visit_days');
   
    return $query->result();
	}
	
	function get_dates_offdays($user_group, $date_arr)		
	{
		$this->defaultdb->where("user_group", $user_group);
		$this->defaultdb->where_in("activity_date", $date_arr);
    $this->defaultdb->order_by("id", "desc");
    $query = $this->defaultdb->get('offdays');
   
    return $query->result();
	}
	
	function get_dates_holidays($user_group, $state_id, $date_arr)
	{
		$this->defaultdb->where("holiday_type", $user_group);
		$this->defaultdb->where("stateid", $state_id);
		$this->defaultdb->where_in("activity_date", $date_arr);
    $this->defaultdb->order_by("id", "desc");
    $query = $this->defaultdb->get('holidays');
   
    return $query->result();
	}
	
	
}

?>
