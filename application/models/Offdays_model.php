<?php
class Offdays_model extends CI_Model {

  public function __construct()
  {
    parent::__construct();
    error_log(" Holidays model construct ");
    $this->defaultdb = $this->load->database('default', TRUE);
  }

  public function get_all($user_id, $start_date, $end_date, $user_group = 'field_user')
  {
    $this->defaultdb->where("user_group",$user_group);
    $this->defaultdb->where("user_id",$user_id);
    $this->defaultdb->where("activity_date >= ",$start_date." 00:00:00");
    $this->defaultdb->where("activity_date <= ",$end_date." 23:59:59");
    $query = $this->defaultdb->get('offdays');
    return $query->result();
  }

  public function get_all_offdays_admin($start_date='')
  {
		if($start_date != '')
			$this->db->where("activity_date >= ",$start_date." 00:00:00");	
    $this->db->select('offdays.*');
    $query = $this->db->get('offdays');
    return $query->result();
  }

  public function add($data)
  {
    $this->defaultdb->insert('offdays', $data);
    return $this->defaultdb->insert_id();
  }

  public function delete_a_offdays($id)
  {
    $this->db->where('id',$id);
    return $this->db->delete('offdays');
  }

}

?>
