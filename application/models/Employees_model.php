<?php
//error_reporting(0);
class Employees_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        //Enable profiler
        $this->output->enable_profiler(false);
        $this->defaultdb = $this->load->database('default', TRUE);
    }
    
    public function get_all($status = '') {
				if($status != '')
          $this->defaultdb->where('status', $status);
        $query = $this->defaultdb->get('sparks');
        return $query->result();
    }
    
    public function get_employees_by_role($role = array()){
				if(!empty($role))
					$this->defaultdb->where_in('role', $role);
        $query = $this->defaultdb->select('sparks.*')->join('sparks_details', 'sparks.id = sparks_details.spark_id')->order_by('joining_date','desc')->get('sparks');
        //echo $this->defaultdb->last_query();
        return $query->result();
    }

    
    public function get_all_branch() {
        $query = $this->defaultdb->get('bank_branches');
        return $query->result();
    }
    
    public function get_all_company_banks() {
        $query = $this->defaultdb->get('company_banks');
        return $query->result();
    }
    
    public function getById($id) {
			  $sql  = "select * from ssc_sparks JOIN ssc_sparks_details ON ssc_sparks.id = ssc_sparks_details.spark_id";
			  $sql .= " where ssc_sparks.id = '$id' ";
			  $qry = $this->db->query($sql); 
				$result = $qry->row_array();  
				return $result;
    }
    
    public function add_spark_details($data, $table='sparks_details') {
			$this->defaultdb->insert($table, $data);
			return $this->defaultdb->insert_id();
		}
		
		public function get_all_reporting_user() {
        $this->defaultdb->where('status', 1);
        $this->defaultdb->where('role !=', 'field_user');
        $this->defaultdb->order_by('name');
        $query = $this->defaultdb->get('sparks');
        return $query->result();
    }
    
    function update_spark_details($data, $spark_id)
    {
			$this->defaultdb->where('spark_id', $spark_id);
      $this->defaultdb->update('sparks_details', $data);
      //echo "<br>".$this->defaultdb->last_query();
		}
		
		function get_employee_addresses($spark_id='', $type='') {
				if($spark_id != '')
					$this->defaultdb->where('spark_id', $spark_id);
				if($type != '')
					$this->defaultdb->where('type', $type);
        $query = $this->defaultdb->get('sparks_addresses');
        return $query->result();
    }
		
		function add_spark_address($data) {
			$this->defaultdb->insert('sparks_addresses', $data);
			return $this->defaultdb->insert_id();
		}
		
		function update_spark_address($data, $id)
    {
			$this->defaultdb->where('id', $id);
      $this->defaultdb->update('sparks_addresses', $data);
      //echo "<br>".$this->defaultdb->last_query();
		}
		
		function get_bank_address($spark_id = '')
		{
				if($spark_id != '')
					$this->defaultdb->where('spark_id', $spark_id);
        $query = $this->defaultdb->get('bank_details');
        return $query->result();
		}
		
		function add_bank_address($data) {
			$this->defaultdb->insert('bank_details', $data);
			return $this->defaultdb->insert_id();
		}
		
		function update_bank_address($data, $spark_id)
    {
			$this->defaultdb->where('spark_id', $spark_id);
      $this->defaultdb->update('bank_details', $data);
      //echo "<br>".$this->defaultdb->last_query();
		}
		
		public function save_user_district_block_history($user_id, $data, $created_by){
      $this->defaultdb->insert('spark_district_block_history', array('user_id'=>$user_id, 'district_id'=>$data[0], 'block_id'=>$data[1], 
      'created_by'=>$created_by, 'start_date'=>$data[1]));
    }
    
    public function update_user_district_block_history($user_id, $dist_block=array(), $updated_by, $date='')
    {
			if($date != '')
			{
				$new_date = date_format(date_create("01-".$date), "Y-m-d");
        $temp = date('Y-m', strtotime("-1 months", strtotime($new_date)));
        $end_date = $temp."-30 00:00:00";
			}else{
				$end_date = date('Y-m-d H:i:s');
			}
			$data['end_date'] = $end_date;
			$data['updated_by'] = $updated_by;
			$this->defaultdb->where('user_id', $user_id);
			$this->defaultdb->where('end_date', NULL);
			if(!empty($dist_block)){
				$this->defaultdb->where('district_id', $dist_block[0]);
				$this->defaultdb->where('block_id', $dist_block[1]);
			}
      $this->defaultdb->update('spark_district_block_history', $data);
		}
    
    function check_employee_details($table, $cond=array()) {
				if(!empty($cond))
					$this->defaultdb->where($cond);
        $query = $this->defaultdb->get($table);
        return $query->result();
    }
    
    function get_employee_salary_details($spark_id, $limit='') {
				$this->defaultdb->where('spark_id', $spark_id);
				if($limit != '')
					$this->defaultdb->limit($limit);
				$this->defaultdb->order_by('id', 'desc');
        $query = $this->defaultdb->get('sparks_salary_details');
        //echo "<br>".$this->defaultdb->last_query();
        return $query->result();
    }
    
    function update_details($table, $data, $cond)
    {
			$this->defaultdb->where($cond);
      $this->defaultdb->update($table, $data);
      //echo "<br>".$this->defaultdb->last_query();
		}
		
		function check_detail_exist($spark_id='')
		{
			if($spark_id != '')
         $this->defaultdb->where('spark_id', $spark_id);
			$query = $this->defaultdb->get('sparks_details');
			return $query->row_array();	
		}
		
		public function get_employees_list($role=array(), $status=1, $state=''){
        
        $this->db->select('sparks.*, states.name as state_name, managers.name as manager_name, managers.login_id as manager_login,
													spd.joining_date, spd.exit_date, spd.division, spd.unit_name ');
        $this->db->join('sparks_details spd', 'sparks.id = spd.spark_id');
        $this->db->join('states', 'sparks.state_id = states.id')->join('sparks managers','sparks.manager_id = managers.id','LEFT');
        if($state != '')
					$this->db->where_in('sparks.state_id', $state);
        if(!empty($status))
					$this->db->where_in('sparks.status', $status);
        if(!empty($role))
					$this->db->where_in('sparks.role', $role);
        return $this->db->get('sparks')->result();
    }
    
}
