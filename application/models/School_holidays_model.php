<?php
class School_holidays_model extends CI_Model {

  public function __construct()
  {
    parent::__construct();
    error_log(" Holidays model construct ");
    $this->defaultdb = $this->load->database('default', TRUE);
  }

  public function get_all($user_id='', $start_date, $end_date)
  {
		if($user_id != '')
			$this->defaultdb->where("user_id",$user_id);
    $this->defaultdb->where("start_date >= ",$start_date." 00:00:00");
    $this->defaultdb->where("end_date <= ",$end_date." 23:59:59");
    $query = $this->defaultdb->get('school_holidays');
    return $query->result();
  }

  public function get_all_holidays_admin($start_date='', $end_date='', $state_id = '')
  {
    $this->db->join('states', 'states.id = school_holidays.state_id','left');
    if($state_id != '')
			$this->db->where('state_id', $state_id);
		if($start_date != '')
			$this->defaultdb->where("leave_from_date >= ",$start_date." 00:00:00");
		if($end_date != '')	
			$this->defaultdb->where("leave_end_date <= ",$end_date." 23:59:59");	
    $this->db->select('states.name as statename');
    $this->db->select('school_holidays.*');
    //$this->db->select('GROUP_CONCAT(ssc_states.name) as statenames');
    //$this->db->group_by('holidays.activity_date');
    $query = $this->db->get('school_holidays');
    return $query->result();
  }


  public function add($data)
  {
    $this->defaultdb->insert('school_holidays', $data);
    return $this->defaultdb->insert_id();
  }
  
  public function add_holiday_dates($data)
  {
    $this->defaultdb->insert('school_holiday_dates', $data);
    return $this->defaultdb->insert_id();
  }
  
  public function isHoliday($activity_date_only,$state_id,$user_id) {
  		//returning no hoiday because of bug fix
  		return false;
        //send only date without time example Y-d-m        
        error_log("Holidays model isHoliday() function ");
        $Lactivity_date_only=date("Y-m-d", strtotime($activity_date_only));
        $holiday_Procedure = "CALL sp_FindHoliday('$Lactivity_date_only','$state_id','$user_id', @outputparam)";
        $mHolidayResult = $this->defaultdb->query($holiday_Procedure);
        $mHolidayResultval = $mHolidayResult->result();
        $holiday = $mHolidayResultval[0]->Total_Holiday;
        mysqli_next_result($this->defaultdb->conn_id);
        //$holiday = $overresult->'Total_Holiday';
        //any value greater than 0 means there is a holiday on provided date
        return ($holiday > 0);
    }

 
  public function delete_a_holidays($id)
  {
    $this->db->where('id',$id);
    return $this->db->delete('school_holidays');
  }
  
  public function delete_a_holiday_dates($holiday_id)
  {
    $this->db->where('school_holiday_id',$holiday_id);
    return $this->db->delete('school_holiday_dates');
  }
  
  
  function get_state_holidays($login_id, $state_id, $_fromDate, $_toDate){
    $query = "SELECT COUNT(DISTINCT activity_date) as STATE_HOLIDAY FROM ssc_school_holidays ssch WHERE `activity_date` BETWEEN '".$_fromDate." 00:00:00' AND '".$_toDate." 23:59:59' AND (state_id = '$state_id' OR NATIONAL = 1 OR user_id = '$login_id')";
    $state_holiday = $this->db->query($query); 
    $row = $state_holiday->row_array();
    return $row['STATE_HOLIDAY'];
  }

  function get_state_holidays_list($state_id, $_fromDate, $_toDate){
    $query = "SELECT activity_date FROM ssc_school_holidays ssch WHERE `activity_date` BETWEEN '".$_fromDate." 00:00:00' AND '".$_toDate." 23:59:59' AND (state_id = ".$state_id." OR NATIONAL = 1) GROUP BY activity_date";
    $state_holiday = $this->db->query($query); 
    //echo "<br>".$this->db->last_query();
    return $state_holiday->result();
  }

 
  function get_dates_holidays($state_id, $dates)
  {
		$this->defaultdb->where_in("state_id",$state_id);
		$this->defaultdb->where_in("holiday_date",$dates);
    $query = $this->defaultdb->get('school_holiday_dates');
    return $query->result();
	}

	
}

?>
