<?php
class Telegrams_model extends CI_Model {

  public function __construct()
  {
    parent::__construct();
    $this->defaultdb = $this->load->database('default', TRUE);
  }

  public function get_all()
  {      
    $this->defaultdb->order_by("id", "desc");
    $query = $this->defaultdb->get('telegram_groups');
    return $query->result();
  }
  
  public function get_by_ids($ids)
  {
    $this->defaultdb->where_in('id',$ids);
    $query = $this->defaultdb->get('telegram_groups');
    return $query->result();
  }
  
  public function get_all_telegrams($id = 0, $limit = 0, $offset = 0)
  {
    if ($id > 0)
      $this->defaultdb->where('id',$id);
      
    $this->defaultdb->order_by("id", "desc");
    if ($limit != "")
      $query = $this->defaultdb->get('telegram_groups', $limit, $offset);
    else
      $query = $this->defaultdb->get('telegram_groups');
    return $query->result();
  }
  
  public function get_all_states_specific( $limit = 0, $offset = 0)
  {
    if ($limit != "")
      $query = $this->defaultdb->get('telegram_groups', $limit, $offset);
    else
      $query = $this->defaultdb->get('telegram_groups');
    return $query->result();
  }

  public function get_telegram_count()
  {
    $query = $this->defaultdb->count_all_results('telegram_groups');
    return $query;
  }
  
  public function insert_to_db($data)
  {
    return $this->defaultdb->insert('telegram_groups', $data);
  }

  public function getById($id)
  {
    $query = $this->defaultdb->get_where('telegram_groups',array('id'=>$id));
    return $query->row_array();		  
  }
  public function getByName($id)
  {
    $query = $this->defaultdb->get_where('telegram_groups',array('name'=>$id));
    return $query->row_array();		  
  }

  public function update_info($data,$id)
  {
    $this->defaultdb->where('id',$id);
    return $this->defaultdb->update('telegram_groups', $data);
  }

  public function delete_a_telegram($id)
  {
    $this->defaultdb->where('id',$id);
    return $this->defaultdb->delete('telegram_groups');
  }
  
  public function getByField($field, $value)
  {
    $query = $this->defaultdb->get_where('telegram_groups',array($field =>$value));
    return $query->row_array();		  
  }
  
  public function getGroupByStateId()
  {
    $select = "SELECT group_id, ssc_states.id FROM `ssc_telegram_groups` JOIN ssc_states WHERE ssc_states.name = ssc_telegram_groups.state_id";
    $query = $this->db->query($select); 
    return $query->result();
  }
    
}

?>
