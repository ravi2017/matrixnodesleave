<?php
class Claim_model extends CI_Model {

  public function __construct()
  {
    parent::__construct();
    $this->defaultdb = $this->load->database('default', TRUE);
  }

  public function get_all($user_id, $start_date, $end_date)
  {
    $this->defaultdb->where("user_id",$user_id);
    $this->defaultdb->where("activity_date >= ",$start_date." 00:00:00");
    $this->defaultdb->where("activity_date <= ",$end_date." 23:59:59");
    $query = $this->defaultdb->get('claims');
    return $query->result();
  }

  public function add($data)
  {
    $this->defaultdb->insert('claims', $data);
    return $this->defaultdb->insert_id();
  }

  public function getById($id)
  {
    $query = $this->defaultdb->get_where('claims',array('id'=>$id));
    return $query->row_array();		  
  }
  
  public function get_district_training($district_id, $start_date, $end_date)
  {
    $this->defaultdb->where("district_id",$district_id);
    //$this->defaultdb->where("block_id",0);
    $this->defaultdb->where("activity_date >= ",$start_date);
    $this->defaultdb->where("activity_date <= ",$end_date);
    $query = $this->defaultdb->get('claims');
    return $query->result();
  }
	public function get_block_training($block_id, $start_date, $end_date)
  {
    $this->defaultdb->where("block_id",$block_id);
    //$this->defaultdb->where("block_id",0);
    $this->defaultdb->where("activity_date >= ",$start_date);
    $this->defaultdb->where("activity_date <= ",$end_date);
    $query = $this->defaultdb->get('claims');
    return $query->result();
  }
	public function get_traning($district_id,$user_id, $start_date, $end_date)
  {
    $this->defaultdb->where("district_id",$district_id);
    $this->defaultdb->where("user_id",$user_id);
    //$this->defaultdb->where("block_id",0);
    $this->defaultdb->where("activity_date >= ",$start_date);
    $this->defaultdb->where("activity_date <= ",$end_date);
		$this->defaultdb->select('count(*) as total');
    $query = $this->defaultdb->get('claims');
    return $query->result();
  }
	public function get_user_traning($state_id,$district_id,$user_id, $start_date, $end_date)
  {
    $this->defaultdb->where("state_id",$state_id);
    $this->defaultdb->where("district_id",$district_id);
    $this->defaultdb->where("user_id",$user_id);
    //$this->defaultdb->where("block_id",0);
    $this->defaultdb->where("activity_date >= ",$start_date);
    $this->defaultdb->where("activity_date <= ",$end_date);
    $this->defaultdb->select('*');
		
    $query = $this->defaultdb->get('claims');
    return $query->result();
  }
  
  public function addOtherActivity($data)
  {
    $this->defaultdb->insert('other_activities', $data);
    return $this->defaultdb->insert_id();
  }

  public function getTravelById($id)
  {
    $query = $this->defaultdb->get_where('other_activities',array('id'=>$id));
    return $query->row_array();		  
  }  
  
  public function get_longdistance_activities($user_id='', $start_date, $end_date, $state_id='', $distance_flag='')
  {
    if($user_id != '')
      $this->defaultdb->where("user_id",$user_id);
    if($state_id != '')
      $this->defaultdb->where("state_id", $state_id);
    if($distance_flag != '')
      $this->defaultdb->where("distance_flag", $distance_flag);
    $this->defaultdb->where("activity_date >= ",$start_date." 00:00:00");
    $this->defaultdb->where("activity_date <= ",$end_date." 23:59:59");
		$this->defaultdb->order_by("activity_date", "asc");
    $query = $this->defaultdb->get('other_activities');
    //echo"<br>".$this->defaultdb->last_query();
    return $query->result();
  }
  
  public function get_distince_ld_activities($login_id, $_fromDate, $_toDate)
  {
    $qry = "SELECT DISTINCT cast(ssm.activity_date AS DATE) AS dat
                FROM ssc_other_activities ssm
                WHERE DATE (ssm.activity_date) BETWEEN '".$_fromDate."' AND '".$_toDate."' AND ssm.user_id = '$login_id'";
    $result = $this->defaultdb->query($qry);
    return $result->result();
  }
  
  public function get_process_dates()
  {
		$sql = "select distinct process_date from ssc_travel_claims where is_claimed = 1 and process_date IS NOT NULL order by process_date desc";
		$result = $this->defaultdb->query($sql);
    return $result->result();
	}

	public function add_other_claims($data)
  {
    $this->defaultdb->insert('other_claims', $data);
    return $this->defaultdb->insert_id();
  }
	
	public function get_other_claims($id)
  {
    $query = $this->defaultdb->get_where('other_claims',array('id'=>$id));
    return $query->row_array();		  
  }
	
	public function add_claims_proof($data)
  {
    $this->defaultdb->insert('claim_proofs', $data);
    return $this->defaultdb->insert_id();
  }

	function get_other_claims_list($spark_id, $start_date, $end_date, $status='')
	{
		//$select = "SELECT soc.*, scp.file_name FROM `ssc_other_claims` soc, ssc_claim_proofs scp WHERE soc.id=scp.claim_id AND scp.claim_type='Other Claim'
		$select = "SELECT soc.* FROM `ssc_other_claims` soc WHERE `spark_id` = '$spark_id' AND ((`from_date` BETWEEN '$start_date' AND '$end_date') OR (`to_date` BETWEEN '$start_date' AND '$end_date')) ";
		if($status != '')	
			$select .= " AND `soc.status` = 'approved' ";
		$select .= " order by Date(soc.created_on) desc";
		$query = $this->defaultdb->query($select);
		//echo"<br>".$this->defaultdb->last_query();
    return $query->result();
	}
	
	public function update_other_claim($data, $id)
  {
    $this->defaultdb->where('id', $id);
    return $this->defaultdb->update('other_claims', $data);
  }
	
	public function update_claim_proof($data,$cond)
  {
    $this->defaultdb->where($cond);
    return $this->defaultdb->update('claim_proofs', $data);
  }

	public function get_claim_proof($cond)
  {
    $this->defaultdb->where($cond);
    $query = $this->defaultdb->get('claim_proofs');
    return $query->result();
  }
	
	public function update_ld_claim($data, $id)
  {
    $this->defaultdb->where('id', $id);
    $result = $this->defaultdb->update('other_activities', $data);
    //echo"<br>".$this->defaultdb->last_query();
    return $result;
  }
  
  public function get_ld_process_dates()
  {
		$sql = "select distinct process_date from ssc_other_activities where claim_status = 'processed' and process_date IS NOT NULL order by process_date desc";
		$result = $this->defaultdb->query($sql);
    return $result->result();
	}
	
  public function get_other_process_dates()
  {
		$sql = "select distinct process_date from ssc_other_claims where claim_status = 'processed' and process_date IS NOT NULL order by process_date desc";
		$result = $this->defaultdb->query($sql);
    return $result->result();
	}
	
	public function addStayActivity($data, $table = 'stay_activities')
  {
    $this->defaultdb->insert($table, $data);
    //echo $this->defaultdb->last_query();
    return $this->defaultdb->insert_id();
  }	
  
  function get_stay_activity_list($spark_id, $start_date, $end_date, $status=0)
	{
		$select = "SELECT * FROM `ssc_stay_activities` WHERE `spark_id` = '$spark_id' AND Date(`activity_date`) BETWEEN '$start_date' AND '$end_date' ";
		$select .= " AND is_processed = '$status' ";
		//echo "<br>".$select;	
		$query = $this->defaultdb->query($select);
    return $query->result();
	}
	
	/*function get_stay_claims_list($spark_id, $start_date, $end_date, $status='')
	{
		$select = "SELECT soc.* FROM `ssc_stay_claims` soc WHERE `spark_id` = '$spark_id' AND ((`from_date` BETWEEN '$start_date' AND '$end_date') OR (`to_date` BETWEEN '$start_date' AND '$end_date')) ";
		if($status != '')	
			$select .= " AND `soc.claim_status` = 'approved' ";
		$query = $this->defaultdb->query($select);
    return $query->result();
	}*/
	
	function get_stay_claims_list($spark_id, $start_date, $end_date, $status='')
	{
		$select = "SELECT soc.*, scp.file_name FROM `ssc_stay_claims` soc, ssc_claim_proofs scp WHERE soc.id=scp.claim_id AND scp.claim_type='Stay Claim'
		AND `spark_id` = '$spark_id' AND ((`from_date` BETWEEN '$start_date' AND '$end_date') OR (`to_date` BETWEEN '$start_date' AND '$end_date')) ";
		if($status != '')	
			$select .= " AND `soc.status` = 'approved' ";
		$query = $this->defaultdb->query($select);
    return $query->result();
	}
	
	function get_food_claims_list($spark_id, $start_date, $end_date, $status='')
	{
		$select = "SELECT soc.*, ssc.stay_reason FROM `ssc_food_claims` soc, `ssc_stay_claims` ssc WHERE ssc.id = soc.stay_claim_id AND soc.`spark_id` = '$spark_id' AND ((soc.`from_date` BETWEEN '$start_date' AND '$end_date') OR (soc.`to_date` BETWEEN '$start_date' AND '$end_date')) ";
		if($status != '')	
			$select .= " AND `soc.claim_status` = 'approved' ";
		$query = $this->defaultdb->query($select);
    return $query->result();
	}
	
	
	public function get_data($table, $cond)
  {
    $this->defaultdb->where($cond);
    $query = $this->defaultdb->get($table);
    return $query->result();
  }	
	
	public function update_data($table, $data, $cond)
  {
    $this->defaultdb->where($cond);
    return $this->defaultdb->update($table, $data);
  }	
  
  function check_month_claim($claim_type, $spark_id, $month_yr)
	{
		$sql = "select SUM(date_expense) as total_expense from ssc_spark_month_claims where claim_type = '$claim_type' AND spark_id = '$spark_id' and activity_month = '$month_yr' ";
		$claims = $this->db->query($sql); 
    $result = $claims->row_array();
    return $result;
	}
  
  function check_date_claim($claim_type, $spark_id, $claim_date)
	{
		$sql = "select * from ssc_spark_month_claims where claim_type = '$claim_type' AND spark_id = '$spark_id' and activity_date = '$claim_date' ";
		$claims = $this->db->query($sql); 
    $result = $claims->row_array();
    return $result;
	}
	
	public function get_stay_claims($id)
  {
    $query = $this->defaultdb->get_where('stay_claims',array('id'=>$id));
    return $query->row_array();		  
  }
	
	public function get_stay_process_dates($table = 'ssc_stay_claims')
  {
		$sql = "select distinct process_date from $table where claim_status = 'processed' and process_date IS NOT NULL order by process_date desc";
		$result = $this->defaultdb->query($sql);
    return $result->result();
	}
	
	 public function get_stay_exceed_claims_list($spark_ids, $start_date='', $end_date = '', $claim_status='') {
      
		$sparkids = implode(', ', $spark_ids);
		
		$select = "SELECT ssc.*, scp.file_name FROM `ssc_stay_claims` ssc, ssc_claim_proofs scp WHERE ssc.id=scp.claim_id AND 
		`spark_id` IN ($sparkids) AND ((`from_date` BETWEEN '$start_date' AND '$end_date') OR (`to_date` BETWEEN '$start_date' AND '$end_date')) ";
		
		if($claim_status == 'processed'){	$select .= " AND ssc.`claim_status` = 'processed' ";	}
		if($claim_status == 'unprocessed'){	$select .= " AND ssc.`claim_status` != 'processed' AND ssc.`claim_status` = 'rejected' ";	}
		if($claim_status == 'rejected'){	$select .= " AND ssc.`claim_status` = 'rejected' ";	}  
		if($claim_status == 'referback'){	$select .= " AND ssc.`claim_status` = 'referback' ";	}  
		if($claim_status == 'approved'){	$select .= " AND ssc.`claim_status` = 'approved' ";	}  
		$select .= " order by ssc.created_on asc";
		$query = $this->defaultdb->query($select);
		return $query->result();
	}
			
		public function get_stay_unprocessed_list($spark_ids, $start_date, $end_date = '', $table = 'stay_claims') {
      
      if(!empty($spark_ids))
				$this->defaultdb->where_in("spark_id",$spark_ids);
			$this->defaultdb->where('claim_status', 'approved');
			$this->defaultdb->where("from_date BETWEEN '$start_date' AND '$end_date' ");
			$this->defaultdb->where("to_date BETWEEN '$start_date' AND '$end_date' ");
			$this->defaultdb->order_by("from_date", "asc");
			$query = $this->defaultdb->get($table);
			$result = $query->result();
			//echo"<br>".$this->defaultdb->last_query();
      return $result;
    }
  		
		public function get_track_tally_stay_list($spark_ids=array(), $process_date, $table='stay_claims') {
      
      $claim_status = array('processed', 'rejected');
      
      if(!empty($spark_ids))
				$this->defaultdb->where_in("spark_id",$spark_ids);
			$this->defaultdb->where("process_date",$process_date);
			$this->defaultdb->where_in("claim_status", $claim_status);
			$this->defaultdb->where("manual_cost > 0");
			$this->defaultdb->order_by("from_date", "asc");
			$query = $this->defaultdb->get($table);
			$result = $query->result();
      return $result;
    }
    
    public function get_food_claims($id)
		{
			$query = $this->defaultdb->get_where('food_claims',array('id'=>$id));
			return $query->row_array();		  
		}
		
		public function get_food_exceed_claims_list($spark_ids, $start_date='', $end_date = '', $claim_status='') {
      
			$sparkids = implode(', ', $spark_ids);
			
			$select = "SELECT sfc.*, ssc.stay_reason FROM `ssc_food_claims` sfc, `ssc_stay_claims` ssc  WHERE ssc.id = sfc.stay_claim_id AND
			sfc.`spark_id` IN ($sparkids) AND ((sfc.`from_date` BETWEEN '$start_date' AND '$end_date') OR (sfc.`to_date` BETWEEN '$start_date' AND '$end_date')) ";
			
			if($claim_status == 'processed'){	$select .= " AND sfc.`claim_status` = 'processed' ";	}
			if($claim_status == 'unprocessed'){	$select .= " AND sfc.`claim_status` != 'processed' AND `claim_status` = 'rejected' ";	}
			if($claim_status == 'rejected'){	$select .= " AND sfc.`claim_status` = 'rejected' ";	}  
			if($claim_status == 'referback'){	$select .= " AND sfc.`claim_status` = 'referback' ";	}  
			$select .= " order by created_on asc";
			$query = $this->defaultdb->query($select);
			return $query->result();
		}
		
}
?>
