<?php

class contact_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->defaultdb = $this->load->database('default', TRUE);
    }


    public function getContactByClusterId($district_id, $block_id, $cluster_id) {
        
        if ($district_id > 0) {
            $this->defaultdb->where("district_id", $district_id);
        }
        if ($block_id > 0) {
            $this->defaultdb->where("block_id", $block_id);
        }
        if ($cluster_id > 0) {
            $this->defaultdb->where("cluster_id", $cluster_id);
        }

        $query = $this->defaultdb->get('contacts');
        return $query->result();
    }
    public function getContacts($state_id, $district_id, $block_id,$cluster_id, $source){
        //$this->defaultdb->from('contacts');
        
        //source description is given below 
        // SA=state
        // DI= district
        // BL= block
        // CL= cluster
        // SC= school
        //currently we are not using state and school but they are there for future requirements
        
        switch ($source) {
            case "DI":
            case "di":
                $this->defaultdb->where("district_id", $district_id);
                break;
            case "BL":
            case "bl":    
                $this->defaultdb->where("district_id", $district_id);
                $this->defaultdb->where("block_id", $block_id);
                break;
            case "CL":
            case "cl":    
                $this->defaultdb->where("district_id", $district_id);
                $this->defaultdb->where("block_id", $block_id);
                $this->defaultdb->where("cluster_id", $cluster_id);
                break;
        }
        $this->defaultdb->where("source", $source);
        $query = $this->defaultdb->get('contacts');
        return $query->result_array();
    }

    public function add($data) {
        $this->defaultdb->insert('contacts', $data);
        return $this->defaultdb->insert_id();
    }
            
     public function edit($id,$data) {
          error_log("Inside account -edit  contact model  method-----", 0);
      $this->defaultdb->where("id", $id);
      $query=$this->defaultdb->update('contacts',$data);
      error_log("Inside account -edit  contact model  query method-----", 0);
       return $this->defaultdb->insert_id();
        error_log("Inside account -edit  contact model  query method-----".$query->result());
        
    }
    public function getById($district_id) {
        $this->defaultdb->where("district_id", $district_id);
        $query = $this->defaultdb->get('contacts');
         return $query->result_array();
       /* $query = $this->defaultdb->get_where('contacts', array('id' => $district_id));
        return $query->result_array(); */
    }

    public function geContacttById($id) {
        $query = $this->defaultdb->get_where('contacts', array('id' => $id));
        return $query->row_array();
    }

}

?>
