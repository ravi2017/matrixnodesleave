<?php
class Dashboard_model extends CI_Model {

  public function __construct()
  {
    parent::__construct();
    $this->defaultdb = $this->load->database('default', TRUE);
  }
  
  public function user_distinct_blocks($user_id)
  {
    $this->defaultdb->join('ssc_spark_users_district_block', 'blocks.id = spark_users_district_block.block_id');
    $this->defaultdb->where('ssc_spark_users_district_block.user_id', $user_id);
    $this->defaultdb->select('blocks.*');
    $this->defaultdb->group_by('block_id');
    $query = $this->defaultdb->get('ssc_blocks');
    //echo"<br>".$this->defaultdb->last_query();
    return $query->result();      
  }

  public function user_distinct_districts($user_id)
  {
    $this->defaultdb->join('ssc_spark_users_district_block', 'districts.id = spark_users_district_block.district_id');
    $this->defaultdb->where('ssc_spark_users_district_block.user_id', $user_id);
    $this->defaultdb->select('districts.*');
    $this->defaultdb->group_by('district_id');
    $query = $this->defaultdb->get('ssc_districts');
    return $query->result();      
  }

  public function get_spark_training_report($state_id='', $spark_id='', $start_date='', $end_date='')
  {
    if($spark_id != '')
      $this->defaultdb->where("spark_id", $spark_id);
    if($state_id != '')
      $this->defaultdb->where("state_id", $state_id);  
    if($start_date != '' && $end_date != '')
      $this->defaultdb->where("created_at between '$start_date' and '$end_date' ");    
    $this->defaultdb->order_by('spark_id');
    $query = $this->defaultdb->get('spark_training_report');
    //echo $this->defaultdb->last_query();      
    return $query->result();
  }
  
  public function get_training_date_data($spark_ids=array(), $start_date='', $end_date='')
  {
      $query  = "SELECT count(DISTINCT(Date(activity_date))) AS date_count, user_id FROM `ssc_trainings` where status='approved' ";
      if(!empty($spark_ids)){
        $sparkids = implode(',', $spark_ids);
        $query .= " AND user_id IN($sparkids) ";
      }
      if($start_date !='' && $end_date !='')
        $query .= " AND  Date(activity_date) BETWEEN '$start_date' AND '$end_date' ";
      $query .= " GROUP BY user_id ORDER BY date_count DESC";
      $qry = $this->db->query($query); 
      $result = $qry->result();
      return $result;
  }

  function spark_feedback_details($spark_ids, $month_year, $duration_type)
  {
    if($duration_type == 'month'){
      $this->defaultdb->select("spark_id, state_rating_month as state_rating, national_rating_month as national_rating");
    }
    else{
      $this->defaultdb->select("spark_id, state_rating_year as state_rating, national_rating_year as national_rating");
    }
    $this->defaultdb->where_in("spark_id", $spark_ids);
    $query = $this->defaultdb->get('ssc_dashboard_spark_feedback_report');
    //echo $this->defaultdb->last_query();      
    return $query->result();
  }
  
  function get_sparks_dashboard_fields()
  {
    $this->defaultdb->where("status", 1);
    $query = $this->defaultdb->get('ssc_dashboard_spark_fields');
    return $query->result();
  }
 
  function get_state_dashboard_fields($fields_for='')
  {
    $this->defaultdb->where("status", 1);
    if($fields_for != '')
      $this->defaultdb->where("fields_for", $fields_for);
    $query = $this->defaultdb->get('ssc_dashboard_state_fields');
    return $query->result();
  }
  
  function get_sparks_dashboard_field_id($cond)
  {
    $this->defaultdb->select("id");
    $this->defaultdb->where($cond);
    $query = $this->defaultdb->get('ssc_dashboard_spark_fields');
    return $query->row_array();
  }
  
  function get_state_dashboard_field_id($cond)
  {
    $this->defaultdb->select("id");
    $this->defaultdb->where($cond);
    $query = $this->defaultdb->get('ssc_dashboard_state_fields');
    return $query->row_array();
  }
  
  function get_travel_modes()
  {
    $sql = "select * from ssc_travels";
    $result = $this->defaultdb->query($sql);
    return $result->result();
  }
  
  function get_travel_details($spark_id, $start_date='', $end_date='', $activity_arr = array())
  {
    $sql = "SELECT st.spark_id, st.activity_type, Date(st.tracking_time) as activity_date, stc.travel_distance, stc.manual_distance, stc.travel_cost, stc.travel_mode_id, stc.is_claimed FROM `ssc_travel_claims` stc JOIN ssc_trackings st ON stc.track_destination_id = st.id WHERE Date(st.tracking_time) BETWEEN '$start_date' AND '$end_date' and st.spark_id = '$spark_id' ";
    if(!empty($activity_arr))
      $sql .= " AND activity_type IN('".implode("','", $activity_arr)."')";
    $sql .= " ORDER BY st.tracking_time";
    $result = $this->defaultdb->query($sql);
    return $result->result();
  } 
  
  function get_address_mismatch($spark_ids, $start_date, $end_date)
  {
    $activity_string = "'training-in', 'meeting-in', 'schoolvisit-in', 'schoolvisit', 'review_meeting', 'training' ";
    $spark_ids = implode("','", $spark_ids);
    
    $sql = "SELECT spark_id, tracking_time, tracking_location,lat_long_location,activity_type FROM `ssc_trackings` JOIN ssc_sparks ON ssc_sparks.id=ssc_trackings.spark_id WHERE lat_long_location IS NOT NULL AND TRIM(tracking_location) != TRIM(lat_long_location) AND activity_type in (".$activity_string.") AND Date(tracking_time) BETWEEN '$start_date' AND '$end_date' and spark_id in ('$spark_ids') ";
    $result = $this->defaultdb->query($sql);
    return $result->result();
  }
  
  function get_address_mismatch_count($spark_id, $start_date, $end_date)
  {
    $trainingData = $this->get_distance('ssc_trainings', $spark_id, $start_date, $end_date);
    $schoolvisitData = $this->get_distance('ssc_school_visits', $spark_id, $start_date, $end_date);
    $meetingData = $this->get_distance('ssc_meetings', $spark_id, $start_date, $end_date);
    
    //echo "<br>".$spark_id." >> ".count($trainingData)." >> ".count($schoolvisitData)." >> ".count($meetingData);
    $total_mismatch = count($trainingData) + count($schoolvisitData) + count($meetingData);
    
    return $total_mismatch;
  }
  
  public function get_distance($activity_table, $spark_id, $start_date='', $end_date='')
   {
     $query  = "select ssp.login_id, ssp.name, Date(sst.activity_date) as ActivityDate, sst.source_address, sst.destination_address, sst.distance";
     $query .= " from $activity_table sst JOIN ssc_sparks ssp ON sst.user_id = ssp.id ";
     $query .= " where distance > ".MIN_DISTANCE;
     $query .= " and user_id = '$spark_id' ";
     if(!empty($start_date) && !empty($end_date)){
       $query .= " AND sst.activity_date BETWEEN '".$start_date." 00:00:00' AND '".$end_date." 23:59:59'  ";
     }
     $query .= " order by user_id, activity_date";
     $qry = $this->db->query($query); 
     $result = $qry->result();
     return $result;
   }
  
  function select_spark_dashboard_value($cond)
  {
    $this->defaultdb->where($cond);
    $query = $this->defaultdb->get('dashboard_spark_values');
    $result = $query->row_array();
    //echo $this->defaultdb->last_query();
    return $result;
  }
  
  function insert_spark_dashboard_value($data)
  {
    $this->defaultdb->insert('dashboard_spark_values', $data);
    return $this->defaultdb->insert_id();
  }
  
  function update_spark_dashboard_value($cond, $data)
  {
    $this->defaultdb->where($cond);
    $this->defaultdb->update('dashboard_spark_values', $data);
    //echo "<br>".$this->defaultdb->last_query();
  }
    
  function get_spark_dashboard_field_values($spark_id, $month_year)
  {
    $sql = "SELECT ssdv.month_value, ssdv.year_value, ssdv.month_percent, ssdv.year_percent, ssdf.fields_value, ssdf.report_heading, ssdv.created_at, ssdv.updated_at FROM ssc_dashboard_spark_values ssdv JOIN ssc_dashboard_spark_fields ssdf ON ssdv.field_id = ssdf.id 
            WHERE ssdv.spark_id = '$spark_id' AND ssdv.month_year = '$month_year' ";
    $result = $this->defaultdb->query($sql);
    return $result->result();
  }
  
  function get_spark_dashboard_mistake_values($month_year)
  {
    $sql = "SELECT ssc_sparks.state_id, ssdv.spark_id, SUM(month_value) AS MONTH_VAL, SUM(year_value) AS YEAR_VAL FROM `ssc_dashboard_spark_values` ssdv JOIN ssc_dashboard_spark_fields ssdf ON ssdf.id = ssdv.field_id JOIN ssc_sparks ON ssc_sparks.id = ssdv.spark_id WHERE ssdf.id IN (SELECT id FROM ssc_dashboard_spark_fields WHERE report_heading = 'TOTAL MISTAKES')  AND ssdv.month_year = '$month_year' GROUP by ssdv.spark_id ";
    $result = $this->defaultdb->query($sql);
    return $result->result();
  }
  
  function insert_spark_dashboard_mistake_value($data)
  {
    $this->defaultdb->insert('dashboard_spark_mistake_score', $data);
    return $this->defaultdb->insert_id();
  }
  
  function update_spark_dashboard_mistake_value($cond, $data)
  {
    $this->defaultdb->where($cond);
    $this->defaultdb->update('dashboard_spark_mistake_score', $data);
  }
  
  function delete_spark_dashboard_mistake_value()
  {
    $delete = "truncate table ssc_dashboard_spark_mistake_score";
    $this->defaultdb->query($delete);
  }
  
  function get_spark_dashboard_mistake_score($type, $state_id='', $month_year='', $sel_val=array(), $order_by)
  {
    $sel_fields = implode(',',$sel_val);
    $sql = "select $sel_fields from ssc_dashboard_spark_mistake_score";
    $sql .= " where type = '$type' ";
    if($state_id != '')
      $sql .= " and state_id = '$state_id' ";
    if($month_year != '')
      $sql .= " and month_year = '$month_year' ";
    $sql .= " order by $order_by ";  
    $result = $this->defaultdb->query($sql);
    //echo"<br>".$this->defaultdb->last_query();
    return $result->result();
  }
  
  function check_spark_dashboard_mistake_score($type, $state_id='', $spark_id='', $month_year='')
  {
    $sql = "select * from ssc_dashboard_spark_mistake_score";
    $sql .= " where type = '$type' ";
    if($state_id != '')
      $sql .= " and state_id = '$state_id' ";
    if($spark_id != '')
      $sql .= " and spark_id = '$spark_id' ";
    if($month_year != '')
      $sql .= " and month_year = '$month_year' ";
    $result = $this->defaultdb->query($sql);
    //echo"<br>".$this->defaultdb->last_query();
    return $result->result();
  }
  
  
  function get_spark_school_visit($spark_id, $start_date, $end_date, $having_cond)
  {
    $sql  = "SELECT user_id, school_id FROM `ssc_school_visits` WHERE user_id = '$spark_id' and school_id != 0 ";
    $sql .= " and Date(activity_date) BETWEEN '$start_date' AND '$end_date' ";
    $sql .= " GROUP by school_id HAVING $having_cond ";
    $result = $this->defaultdb->query($sql);
    return $result->num_rows();
  }
  
  function ssc_dashboard_state_fields($fields_for)
  {
    $this->defaultdb->where("status", 1);
    $this->defaultdb->where("fields_for", $fields_for);
    $query = $this->defaultdb->get('ssc_dashboard_state_fields');
    return $query->result();
  }
  
  
  function get_state_sparks_dashboard_data($spark_ids, $month_year, $duration_type, $role='')
  {
    $sparkids = implode(",",$spark_ids);
    
    $sql = "SELECT field_id, sdf.fields_value, SUM(sdv.$duration_type) AS FIELD_VAL FROM `ssc_dashboard_spark_values` sdv JOIN ssc_dashboard_spark_fields sdf ON sdv.field_id = sdf.id WHERE month_year = '$month_year' and spark_id IN ($sparkids) ";
    if($role != '')
      $sql .= " and role='$role' ";
    $sql .= " GROUP BY field_id";
    $result = $this->defaultdb->query($sql);
    return $result->result();
  }
  
  function get_dashboard_field_values($spark_ids=array(), $month_year, $duration_type, $field)
  {
    $sql = "SELECT spark_id, sdv.$duration_type FIELD_VAL FROM `ssc_dashboard_spark_values` sdv JOIN ssc_dashboard_spark_fields sdf ON sdv.field_id = sdf.id WHERE month_year = '$month_year' ";
    if(!empty($spark_ids)){
      $sparkids = implode(",",$spark_ids);
      $sql .= " AND spark_id IN ($sparkids) ";
    }
    $sql .= " AND sdf.fields_value = '$field' order by sdv.$duration_type desc ";
    $result = $this->defaultdb->query($sql);
    return $result->result();
  }
  
  function select_state_dashboard_value($cond,$order_by='')
  {
    $this->defaultdb->where($cond);
    if($order_by != '')
      $this->defaultdb->order_by($order_by);
    $query = $this->defaultdb->get('dashboard_state_values');
    $result = $query->result();
    //echo $this->defaultdb->last_query();
    return $result;
  }
  
  function insert_state_dashboard_value($data)
  {
    $this->defaultdb->insert('dashboard_state_values', $data);
    return $this->defaultdb->insert_id();
  }
  
  function update_state_dashboard_value($cond, $data)
  {
    $this->defaultdb->where($cond);
    $this->defaultdb->update('dashboard_state_values', $data);
    //echo "<br>".$this->defaultdb->last_query();
  }
  
  function get_state_dashboard_field_values($state_id='', $spark_id='', $month_year, $fields_for = 'for_spark', $report_heading='')
  {
    $sql = "SELECT ssdv.month_value, ssdv.year_value, ssdv.month_percent, ssdv.year_percent, ssdf.fields_value, ssdf.report_heading, ssdv.created_at, ssdv.updated_at FROM ssc_dashboard_state_values ssdv JOIN ssc_dashboard_state_fields ssdf ON ssdv.field_id = ssdf.id 
            WHERE  ssdv.month_year = '$month_year' ";
    if($state_id != '')
       $sql .= " AND ssdv.state_id = '$state_id' ";       
    if($spark_id != '')
       $sql .= " AND ssdv.spark_id = '$spark_id' ";       
    if($report_heading != '')
       $sql .= " AND ssdf.report_heading = '$report_heading' ";       
    $sql .= " AND ssdf.fields_for = '$fields_for' ";       
    $result = $this->defaultdb->query($sql);
    return $result->result();
  }  
  
  function get_activity_district($activity_type, $state_id, $sdate, $edate)
  {
    $sql = "SELECT DISTINCT district_id FROM `$activity_type` WHERE state_id = '$state_id' and Date(activity_date) BETWEEN '$sdate' AND '$edate'";
    $result = $this->defaultdb->query($sql);
    return $result->result();
  }
  
  function get_sparks_dashboard_rankings($spark_ids, $month_year, $duration ,$fields)
  {
    $sparkids = implode(',', $spark_ids);
    $fields_val = implode("','", $fields);
    
    $sql = "SELECT spark_id, sum(sdv.$duration) as FIELD_VAL FROM `ssc_dashboard_spark_values` sdv JOIN ssc_dashboard_spark_fields sdf ON sdv.field_id = sdf.id WHERE month_year = '$month_year' and spark_id IN ($sparkids) AND sdf.fields_value IN ('".$fields_val."') group by spark_id order by sdv.spark_id ";
    $result = $this->defaultdb->query($sql);
    return $result->result();
  }
  
  function get_all_sparks_dashboard_rankings($month_year, $duration ,$fields, $role='')
  {
    //$sparkids = implode(',', $spark_ids);
    $fields_val = implode("','", $fields);
    
    $sql = "SELECT spark_id, state_id, (sum(sdv.$duration)/4) as FIELD_VAL FROM `ssc_dashboard_spark_values` sdv JOIN ssc_dashboard_spark_fields sdf ON sdv.field_id = sdf.id WHERE month_year = '$month_year' AND sdf.fields_value IN ('".$fields_val."') and fields_value != '9999' ";
    if($role != '')
      $sql .= " AND role = '$role' ";
    $sql .= " group by spark_id order by FIELD_VAL";
    $result = $this->defaultdb->query($sql);
    //echo"<br>".$this->defaultdb->last_query();
    return $result->result();
  }
  
  function get_all_state_dashboard_rankings($month_year, $duration ,$fields)
  {
    //$sparkids = implode(',', $spark_ids);
    $fields_val = implode("','", $fields);
    
    $sql = "SELECT state_id, (sum(sdv.$duration)/4) as FIELD_VAL FROM `ssc_dashboard_state_values` sdv JOIN ssc_dashboard_state_fields sdf ON sdv.field_id = sdf.id WHERE month_year = '$month_year' AND sdf.fields_value IN ('".$fields_val."') group by state_id order by FIELD_VAL";
    $result = $this->defaultdb->query($sql);
    //echo"<br>".$this->defaultdb->last_query();
    return $result->result();
  }
  
  function get_all_state_person_dashboard_rankings($month_year, $duration ,$fields, $role, $state_id)
  {
    //$sparkids = implode(',', $spark_ids);
    $fields_val = implode("','", $fields);
    
    $sql = "SELECT sdv.state_id, spark_id, (sum(sdv.$duration)/4) as FIELD_VAL FROM `ssc_dashboard_state_values` sdv JOIN ssc_dashboard_state_fields sdf ON sdv.field_id = sdf.id JOIN ssc_sparks sss ON sss.id = sdv.spark_id WHERE sss.role = '$role' and sdv.state_id = '$state_id' and month_year = '$month_year' AND sdf.fields_value IN ('".$fields_val."') group by spark_id order by FIELD_VAL";
    $result = $this->defaultdb->query($sql);
    //echo"<br>".$this->defaultdb->last_query();
    return $result->result();
  }
  
  
  function get_spark_join_count($state_id, $start_date, $end_date)
  {
    $sql = "SELECT count(ssp.id) as total_sparks FROM ssc_sparks ssp JOIN ssc_spark_user_state sst ON ssp.id = sst.user_id where sst.state_id = '$state_id' and Date(created_on) between '$start_date' and '$end_date' ";
    $query = $this->defaultdb->query($sql);
    $result = $query->row_array();  
    return $result;
  }
  
  function get_spark_inactive_count($state_id, $start_date, $end_date)
  {
    $sql = "SELECT count(ssp.id) as total_sparks FROM ssc_sparks ssp JOIN ssc_spark_user_state sst ON ssp.id = sst.user_id where sst.state_id = '$state_id' and Date(updated_on) between '$start_date' and '$end_date' and ssp.status = 0 ";
    $query = $this->defaultdb->query($sql);
    $result = $query->row_array();  
    return $result;
  }
  
  function get_spark_dashboard_data($state_id, $field_ids=array(), $month_year, $spark_ids = array())
  {
    $this->defaultdb->where('month_year', $month_year);
    $this->defaultdb->where('state_id', $state_id);
    if(!empty($spark_ids))
      $this->defaultdb->where_in('spark_id', $spark_ids);
    $this->defaultdb->where_in('field_id', $field_ids);
    //$this->defaultdb->order_by('field_id', '68');
    $query = $this->defaultdb->get('dashboard_spark_values');
    $result = $query->result();
    //echo $this->defaultdb->last_query();
    return $result;
  }
  
  function get_state_dashboard_data($state_id, $field_ids=array(), $month_year)
  {
    $this->defaultdb->where('month_year', $month_year);
    $this->defaultdb->where('state_id', $state_id);
    $this->defaultdb->where_in('field_id', $field_ids);
    $query = $this->defaultdb->get('dashboard_state_values');
    $result = $query->result();
    //echo $this->defaultdb->last_query();
    return $result;
  }
  
  function return_sparks_count($month_year, $state_id='')
  {
    $sql = "SELECT DISTINCT spark_id FROM `ssc_dashboard_spark_values` WHERE month_year = '$month_year' ";
    if($state_id != '')
      $sql .= " AND state_id = '$state_id' ";
    $query = $this->defaultdb->query($sql);
    return $query->num_rows();
  }  
  
  public function  getAllSparkByStateDuration($state_id, $from_date = '', $end_date = '', $role = array('field_user','manager','state_person')) {

    if($from_date !='' && $end_date !=''){

      $month_end = date('Y-m-t', strtotime($end_date));

      $data = $this->getMonthsInRange($from_date, $end_date);
      $dates = implode("','", $data);
      $roles = implode("','", $role);

      $query = "SELECT * FROM (`ssc_sparks`) JOIN `ssc_spark_user_state` ON `ssc_sparks`.`id` = `ssc_spark_user_state`.`user_id` WHERE `ssc_sparks`.`status` = 1 AND `ssc_spark_user_state`.`state_id` = '$state_id' AND `ssc_sparks`.`role` IN ('$roles') ";
      $query .=" AND (ssc_sparks.exit_date >= '$from_date' OR ssc_sparks.exit_date IS NULL)  ";		//condition adding to check spark Exit Date
      $query .=" AND (DATE_FORMAT(ssc_spark_user_state.start_date, '%Y-%m') in('$dates') OR DATE_FORMAT(ssc_spark_user_state.end_date, '%Y-%m') in('$dates') OR (";
      $query .= " ssc_spark_user_state.start_date <= '$from_date' AND (ssc_spark_user_state.end_date >= '$month_end' OR ssc_spark_user_state.end_date IS NULL))) ORDER BY name ";  
    }
    //echo "<br>".$query;
    $query_result = $this->defaultdb->query($query);
    $row = $query_result->result();
    return $row;
  }
  
  function getAllSparksByJoiningDate($joining_date){
    $query = "SELECT * FROM (`ssc_sparks`) where Date(`ssc_sparks`.created_on) > '$joining_date'";
    $query_result = $this->defaultdb->query($query);
    $row = $query_result->result();
    return $row;
  }
  
  function getMonthsInRange($startDate, $endDate) {
    $dates = array();
    while (strtotime($startDate) <= strtotime($endDate)) {
        //$months[] = array('year' => date('Y', strtotime($startDate)), 'month' => date('m', strtotime($startDate)), );
        $dates[] = date('Y', strtotime($startDate))."-".date('m', strtotime($startDate));
        $startDate = date('d M Y', strtotime($startDate.'+ 1 month'));
    }
    return $dates;
  }
  
  function get_spark_dashboard_score($field_id, $state_id='', $month_year='', $sel_val=array(), $order_by, $role='')
  {
    $sel_fields = implode(',',$sel_val);
    $sql = "select $sel_fields from ssc_dashboard_spark_values";
    $sql .= " where field_id = '$field_id' ";
    if($state_id != '')
      $sql .= " and state_id = '$state_id' ";
    if($month_year != '')
      $sql .= " and month_year = '$month_year' ";
    if($role != '')
      $sql .= " and role = '$role' ";
    $sql .= " order by $order_by ";  
    $result = $this->defaultdb->query($sql);
    //echo"<br>".$this->defaultdb->last_query();
    return $result->result();
  }
  
  function get_state_dashboard_score($field_id, $month_year='', $sel_val=array(), $order_by)
  {
    $sel_fields = implode(',',$sel_val);
    $sql = "select $sel_fields from ssc_dashboard_state_values";
    $sql .= " where field_id = '$field_id' ";
    if($month_year != '')
      $sql .= " and month_year = '$month_year' ";
    $sql .= " order by $order_by ";  
    $result = $this->defaultdb->query($sql);
    //echo"<br>".$this->defaultdb->last_query();
    return $result->result();
  }
  
  function get_total_feedbacks($spark_id, $sdate, $edate)
  {
    $sql = "SELECT total_feedbacks,average FROM `ssc_spark_feedback_report_new` WHERE spark_id='$spark_id' AND created_at = (SELECT MAX(created_at) FROM `ssc_spark_feedback_report_new` WHERE created_at BETWEEN '$sdate' AND '$edate') ";
    $result = $this->defaultdb->query($sql);
    //echo"<br>".$this->defaultdb->last_query();
    return $result->row_array();
  }

  function get_month_total_feedbacks($spark_id, $month_year)
  {
    $sql = "SELECT total_feedbacks,average FROM `ssc_dashboard_spark_feedback_report` WHERE spark_id='$spark_id' AND month_year = '$month_year' ";
    $result = $this->defaultdb->query($sql);
    //echo"<br>".$this->defaultdb->last_query();
    return $result->row_array();
  }

  function get_state_program_maturity($state_id, $month_year)
  {
    $sql = "SELECT * FROM `ssc_program_maturity` WHERE state_id='$state_id' AND month_year = '$month_year' ";
    $result = $this->defaultdb->query($sql);
    //echo"<br>".$this->defaultdb->last_query();
    return $result->row_array();
  }
  
  function get_state_person_meeting_data($field_name, $month_year, $duration, $role)
  {
    $sql = "SELECT spark_id, $duration FROM ssc_dashboard_state_values dsv JOIN ssc_dashboard_state_fields dsf ON dsv.field_id = dsf.id JOIN ssc_sparks sss ON sss.id = dsv.spark_id WHERE sss.role = '$role' and dsf.fields_value = '$field_name' AND month_year='$month_year' ORDER BY $duration";
    $result = $this->defaultdb->query($sql);
    //echo"<br>".$this->defaultdb->last_query();
    return $result->result();
  }
  
  function get_state_person_dashboard_score($field_id, $month_year='', $sel_val=array(), $order_by, $role='')
  {
    $sel_fields = implode(',',$sel_val);
    $sql = "select $sel_fields from ssc_dashboard_state_values sdv JOIN ssc_sparks sss ON sss.id = sdv.spark_id ";
    $sql .= " where field_id = '$field_id' ";
    if($month_year != '')
      $sql .= " and month_year = '$month_year' ";
    if($role != '')
      $sql .= " and sss.role = '$role' ";
    $sql .= " order by $order_by ";  
    $result = $this->defaultdb->query($sql);
    //echo"<br>".$this->defaultdb->last_query();
    return $result->result();
  }
  
  function return_dashboard_sparks_count($month_year, $state_id='')
  {
    $sql = "SELECT DISTINCT spark_id FROM `ssc_dashboard_spark_values` WHERE month_year = '$month_year' ";
    if($state_id != '')
      $sql .= " AND state_id = '$state_id' ";
    $sql .= "  AND spark_id IN (SELECT id FROM ssc_sparks WHERE role = 'field_user') ";
    $query = $this->defaultdb->query($sql);
    return $query->num_rows();
  }  
  
  
  function insert_attendance($data)
  {
	  $query = $this->defaultdb->insert('trackings', $data);
	  //echo"<br>".$this->defaultdb->last_query();
	  return $query;
  }
    
  function last_attendance($user_id, $source)  
  {
		$sql = "select * from ssc_trackings where spark_id = '$user_id' and created_by = '$source' order by id desc limit 1 ";
		$result = $this->defaultdb->query($sql);
		//echo"<br>".$this->defaultdb->last_query();
		return $result->row_array();
  }  
  
  function get_baithak_fields($field_name='')
  {
		$sql = "select * from ssc_baithak_fields where status = 1";
		if($field_name != '')
			$sql .= " AND field_name = '$field_name' ";
		$result = $this->defaultdb->query($sql);
		//echo"<br>".$this->defaultdb->last_query();
		return $result->result();
	}
	
	function select_spark_baithak_value($cond, $result_type='', $order_by = '')
  {
    $this->defaultdb->where($cond);
    
    if($order_by != '')
			$this->defaultdb->order_by($order_by);
    $query = $this->defaultdb->get('baithak_values');
    if($result_type == 'all')
			$result = $query->result();
    else
			$result = $query->row_array();
    //echo $this->defaultdb->last_query();
    return $result;
  }
  
  function insert_spark_baithak_value($data)
  {
    $this->defaultdb->insert('baithak_values', $data);
    //echo "<br>".$this->defaultdb->last_query();
    return $this->defaultdb->insert_id();
  }
  
  function update_spark_baithak_value($cond, $data)
  {
    $this->defaultdb->where($cond);
    $this->defaultdb->update('baithak_values', $data);
    //echo "<br>".$this->defaultdb->last_query();
  }
  
  function get_baithak_fields_rankings($month_year,$state_id='', $duration ,$fields, $role='')
  {
    //$sparkids = implode(',', $spark_ids);
    $fields_val = implode("','", $fields);
    
    $sql = "SELECT spark_id, state_id, field_id, field_name, sdv.$duration as FIELD_VAL FROM `ssc_baithak_values` sdv JOIN ssc_baithak_fields sdf ON sdv.field_id = sdf.id WHERE month_year = '$month_year' AND sdf.field_name IN ('".$fields_val."') and sdv.$duration != 9999 ";
    if($state_id != '')
      $sql .= " AND state_id = '$state_id' ";
    if($role != '')
      $sql .= " AND role = '$role' ";
    $sql .= " order by spark_id";
    $result = $this->defaultdb->query($sql);
    //echo"<br>".$this->defaultdb->last_query();
    return $result->result();
  }
  
  function get_baithak_fields_data($state_id, $field_ids=array(), $month_year, $spark_ids = array())
  {
    $this->defaultdb->where('month_year', $month_year);
    $this->defaultdb->where('state_id', $state_id);
    if(!empty($spark_ids))
      $this->defaultdb->where_in('spark_id', $spark_ids);
    $this->defaultdb->where_in('field_id', $field_ids);
    //$this->defaultdb->order_by('field_id', '68');
    $query = $this->defaultdb->get('baithak_values');
    $result = $query->result();
    //echo $this->defaultdb->last_query();
    return $result;
  }
  
  function get_state_baithak_data($state_id = array(), $field_name = array(), $month_year, $duration_type = '', $role='', $group_by = 'field_id', $orderby='')
  {
		if(!empty($state_id))
			$stateids = implode(",", $state_id);
		
		if(!empty($field_name))
			$fieldnames = implode("','", $field_name);
		
		if($duration_type != '')
			$select = " SUM(sdv.$duration_type) AS $duration_type ";
		else	
			$select = " SUM(sdv.month_value) AS month_value, SUM(sdv.year_value) AS year_value ";
		
		$sql = "SELECT state_id, field_id, sdf.field_name, $select FROM `ssc_baithak_values` sdv JOIN ssc_baithak_fields sdf ON sdv.field_id = sdf.id WHERE month_year = '$month_year' ";
		if(!empty($state_id))
			$sql .= " and state_id IN ($stateids) ";
		if(!empty($field_name))	
			$sql .= " AND field_name IN('$fieldnames') ";
		if($role != '')
			$sql .= " and role='$role' ";
    $sql .= " GROUP BY $group_by";
    if($orderby != '')
			$sql .= " ORDER BY $orderby";
    //echo"<br>".$sql;
    $result = $this->defaultdb->query($sql);
    return $result->result();
  }
  
	
}

?>
