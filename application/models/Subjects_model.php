<?php
class Subjects_model extends CI_Model {

  public function __construct()
  {
    parent::__construct();
    $this->defaultdb = $this->load->database('default', TRUE);
  }

  public function get_all($status='')
  {
    $this->defaultdb->order_by("name", "desc");
    if($status != '')
       $this->defaultdb->where('status', $status); 
    $query = $this->defaultdb->get('subjects');
    return $query->result();
  }

  public function getById($id)
  {
    $query = $this->defaultdb->get_where('subjects',array('id'=>$id));
    return $query->row_array();		  
  }
  
  public function getByIds($ids)
  {
    $this->defaultdb->where_in('id', $ids);
    $query = $this->defaultdb->get('subjects');
    return $query->result();		  
  }
  
  public function getByName($name)
  {
    $query = $this->defaultdb->get_where('subjects',array('name'=>$name));
    return $query->row_array();		  
  }
  
  public function getByOtherName($id, $name)
  {
    $query = $this->defaultdb->get_where('subjects',array('id !='=>$id,'name'=>$name));
    return $query->row_array();		  
  }
  
  public function add($data)
  {
    $this->defaultdb->insert('subjects', $data);
    return $this->defaultdb->insert_id();
  }

  public function update_subject($id, $data){
    $this->db->where('id', $id)->update('subjects', $data);
  }

  public function delete_a_subject($id)
  {
    $this->db->where('id',$id);
    return $this->db->delete('subjects');
  }
  
  public function add_state_subject_map($data)
  {
    $this->defaultdb->insert('state_subjects', $data);
    return $this->defaultdb->insert_id();
  }

  public function check_mapping($state_id='', $subject_id='')
  {
    if(!empty($state_id) != '')
      $this->defaultdb->where('state_id',$state_id);
    if(!empty($subject_id))
      $this->defaultdb->where('subject_id', $subject_id);
    $query = $this->defaultdb->get('state_subjects');
    return $query->result();		  
  }
  
  
  public function delete_mapping($state_id, $subject_id)
  {
    $this->defaultdb->where_in('state_id',$state_id);
    $this->defaultdb->where('subject_id',$subject_id);
    //$this->db->get('subject_id',$subject_id);
    
    $this->defaultdb->delete('state_subjects');
    echo $this->defaultdb->last_query();
    return ;
  }
  
  public function get_subject_count($status)
  {
    if($status != '')
       $this->defaultdb->where('status', $status); 
    $query = $this->defaultdb->count_all_results('subjects');
    return $query;
  }
  
}

?>
