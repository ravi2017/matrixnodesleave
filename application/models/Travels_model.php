<?php
class Travels_model extends CI_Model {

  public function __construct()
  {
    parent::__construct();
    $this->defaultdb = $this->load->database('default', TRUE);
  }

  public function get_all()
  {
    $this->defaultdb->order_by("travel_mode", "asc");
    $query = $this->defaultdb->get('travels');
    return $query->result();
  }

  public function getById($id)
  {
    $query = $this->defaultdb->get_where('travels',array('id'=>$id));
    return $query->row_array();		  
  }
  
  public function getByName($name)
  {
    $query = $this->defaultdb->get_where('travels',array('travel_mode'=>$name));
    return $query->row_array();		  
  }
  
  public function getByOtherName($id, $name)
  {
    $query = $this->defaultdb->get_where('travels',array('id !='=>$id,'travel_mode'=>$name));
    return $query->row_array();		  
  }
  
  public function add($data)
  {
    $this->defaultdb->insert('travels', $data);
    return $this->defaultdb->insert_id();
  }

  public function update_travel($id, $data){
    $this->db->where('id', $id)->update('travels', $data);
  }

  public function delete_a_travel($id)
  {
    $this->db->where('id',$id);
    return $this->db->delete('travels');
  }
  
  function get_travelmode_by_role($role, $status='')
  {
		$sql = "SELECT * FROM `ssc_travels` WHERE find_in_set('$role', role) ";
		if($status != '')
			$sql .= " AND status = '$status' ";
		$sql .= " order by travel_mode";
		$query = $this->defaultdb->query($sql);
    return $query->result();
	}

}

?>
