<?php
//error_reporting(0);
class Observations_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        //Enable profiler
        $this->output->enable_profiler(false);
        $this->defaultdb = $this->load->database('default', TRUE);
    }

    public function get_all($status = '1') {
        if($status != '')
          $this->defaultdb->where('status', $status);
        $this->defaultdb->order_by('activity_type asc, state_id asc, class_id asc, sort_order asc');
        $query = $this->defaultdb->get('ssc_observation_questions');
        return $query->result();
    }

    public function get_all_observations($current_user_id = 0, $state_id='', $activity_type='', $orderby='', $status='') {
        if($current_user_id > 0)
          $this->defaultdb->where('id != ', $current_user_id);
        if($state_id != '')
          $this->defaultdb->where('state_id', $state_id);
        if($activity_type != '')
          $this->defaultdb->where('activity_type', $activity_type);  
        if($status != '')
          $this->defaultdb->where('status', $status);  
        if($orderby !='')
            $this->defaultdb->order_by($orderby);
        $query = $this->defaultdb->get('ssc_observation_questions');
        return $query->result();
    }

    public function add($data) {
        $this->defaultdb->insert('ssc_observation_questions', $data);
        return $this->defaultdb->insert_id();
    }

    public function add_option($data) {
        $this->defaultdb->insert('ssc_question_options', $data);
        return $this->defaultdb->insert_id();
    }

    public function edit_option($data,$id) {
        $this->defaultdb->where('id', $id);
        $this->defaultdb->update('ssc_question_options', $data);
    }

    public function disable_options($observation_id,$not_ids) {
        $data['status'] = 0;
        $this->defaultdb->where('question_id', $observation_id);
        $this->defaultdb->where_not_in('id', $not_ids);
        $this->defaultdb->update('ssc_question_options', $data);
    }
    
    public function disable_observations($ids) {
        $data['status'] = 0;
        $this->defaultdb->where_in('id', $ids);
        $this->defaultdb->update('ssc_observation_questions', $data);
    }

    public function edit($data, $id) {
        $this->defaultdb->where('id', $id);
        $this->defaultdb->update('ssc_observation_questions', $data);
    }    

    public function getById($id) {
        $query = $this->defaultdb->get_where('ssc_observation_questions', array('id' => $id));
        return $query->row_array();
    }

    public function getObservationBystate($state_id = 0) {
        //echo $state_id;echo $district_id;
        //exit();
        if ($state_id > 0) {
            $this->defaultdb->where('state_id', $state_id);
        }
        $query = $this->defaultdb->get('ssc_observation_questions');
        return $query->result();
    }

    public function getObtionsByObservation($observation_id = 0, $option_value = '', $status = '1') {
        $this->defaultdb->where('question_id', $observation_id);
        if($status != '')
          $this->defaultdb->where('status', $status);
        if($option_value != '')
          $this->defaultdb->where('option_value', $option_value);
        $query = $this->defaultdb->get('ssc_question_options');
        return $query->result();
    }
    
    public function getOptionById($id)
    {
        $this->defaultdb->where('id', $id);
        $query = $this->defaultdb->get('ssc_question_options');
        return $query->result(); 
    }
    
    public function getObservationBystateClass($state_id = 0, $activity_type = '', $class_id = 0) {
        //echo $state_id;echo $district_id;
        //exit();
        if($state_id > 0)
            $this->defaultdb->where('state_id', $state_id);
        if($activity_type != "")
            $this->defaultdb->where('activity_type', $activity_type);
        if($class_id > 0)
            $this->defaultdb->where('class_id', $class_id);    
        $this->defaultdb->where('status', 1);    
        $this->defaultdb->select('id, question, question_type, subject_wise, state_id, class_id');
        $this->defaultdb->order_by('activity_type asc, state_id asc, class_id asc, sort_order asc');
        $query = $this->defaultdb->get('ssc_observation_questions');
        //echo"<br>".$this->defaultdb->last_query();
        return $query->result();
    }

    public function update_info($data, $id) {
        $this->defaultdb->where('id', $id);
        return $this->defaultdb->update('ssc_observation_questions', $data);
    }

    public function delete_a_observation($id) {
        $this->defaultdb->where('id', $id);
        $this->defaultdb->delete('ssc_observation_questions');
    }    
    
    public function add_mapping($data) {
        $this->defaultdb->insert('ssc_state_subjects', $data);
        return $this->defaultdb->insert_id();
    }
    
    public function check_mapping($state_id, $subject_id, $class_id){
        $this->defaultdb->where('state_id', $state_id);
        $this->defaultdb->where('subject_id', $subject_id);
        $query = $this->defaultdb->where('class_id', $class_id)->get('ssc_state_subjects');
        return $query->result();
    }
    
    public function get_state_subject_count() {
        $this->defaultdb->where("class_id > 0");
        $this->defaultdb->where("subject_id > 0");
        $this->defaultdb->where("state_id > 0");
        $query = $this->defaultdb->count_all_results('state_subjects');
        return $query;
    }
    
    public function get_all_mapping() {
        $this->defaultdb->select('ssc_state_subjects.id as id, ssc_state_subjects.state_id as state_id, ssc_state_subjects.class_id as class_id, ssc_state_subjects.subject_id as subject_id, ssc_state_subjects.status as status, ssc_states.name as state_name, ssc_classes.name as class_name, ssc_subjects.name as subject_name');
        $this->defaultdb->join('ssc_states', 'ssc_states.id = ssc_state_subjects.state_id');
        $this->defaultdb->join('ssc_classes', 'ssc_classes.id = ssc_state_subjects.class_id');
        $this->defaultdb->join('ssc_subjects', 'ssc_subjects.id = ssc_state_subjects.subject_id');
        $result = $this->defaultdb->get('ssc_state_subjects')->result();
        return $result;
    }
    
    public function get_mapping($class_id,$state_id) {
        $this->defaultdb->where('state_id', $state_id);
        $query = $this->defaultdb->where('class_id', $class_id)->get('ssc_state_subjects');
        return $query->result();
    }
    
    public function getMapById($id) {
        $query = $this->defaultdb->get_where('ssc_state_subjects', array('id' => $id));
        return $query->row_array();
    }

    public function update_map_info($data, $id) {
        $this->defaultdb->where('id', $id);
        return $this->defaultdb->update('ssc_state_subjects', $data);
    }
    
    public function delete_mapping($id) {
        $this->defaultdb->where('id', $id);
        $this->defaultdb->delete('ssc_state_subjects');
    }    
    
    public function getReportsObservation($state_id = 0, $class_ids = array(), $subject_wise = 1) {
        if($state_id > 0)
            $this->defaultdb->where('state_id', $state_id);
        if(!empty($class_ids))
            $this->defaultdb->where_in('class_id', $class_ids);    
        $this->defaultdb->where(array('question_type'=>'yes-no-na', 'productivity_flag'=>1, 'subject_wise'=>$subject_wise, 'activity_type'=>'school_visit'));    
        $this->defaultdb->select('id, question, weightage, class_id');
        $this->defaultdb->order_by('class_id asc, sort_order asc');
        $query = $this->defaultdb->get('ssc_observation_questions');
        //echo $this->defaultdb->last_query();
        return $query->result();
    }
    
    public function getAllReportsObservation($state_id = 0, $class_ids = array(), $activity_type='') {
        if($state_id > 0)
            $this->defaultdb->where('state_id', $state_id);
        //if(!empty($class_ids))
            //$this->defaultdb->where_in('class_id', $class_ids);    
        //$this->defaultdb->where(array('status'=>1, 'activity_type'=>$activity_type));    
        $this->defaultdb->where(array('activity_type'=>$activity_type));    
        $this->defaultdb->select('id, question, class_id, subject_wise');
        $this->defaultdb->order_by('class_id asc, sort_order asc');
        $query = $this->defaultdb->get('ssc_observation_questions');
        //echo $this->defaultdb->last_query();
        return $query->result();
    }
    
    public function get_subject_count()
    {
      $query = $this->defaultdb->count_all_results('subjects');
      return $query;
    }
}
