<?php
class Concepts_model extends CI_Model {

  public $defaultdb = '';
  public function __construct()
  {
    parent::__construct();
    $this->defaultdb = $this->load->database('default', TRUE);
  }
	
  public function get_all_concepts($class_id = 0, $subject_id = 0, $progress_chart = '', $base_line = '', $end_line = '')
  {
    $this->defaultdb->join('subjects', 'subjects.id = concepts.subject_id');
    $this->defaultdb->join('classes', 'classes.id = concepts.class_id');
    $this->defaultdb->select('subjects.name as subject_name');
    $this->defaultdb->select('classes.name as class_name');
    $this->defaultdb->select('concepts.*');
		
    if ($progress_chart != '')
    {
      $this->defaultdb->where(array('progress_chart'=>$progress_chart));
    }
    if ($base_line > 0)
    {
      $this->defaultdb->where(array('base_line'=>$base_line));
    }
    if ($end_line > 0)
    {
      $this->defaultdb->where(array('end_line'=>$end_line));
    }
    if ($class_id > 0)
    {
      $this->defaultdb->where(array('class_id'=>$class_id));
    }
    if ($subject_id > 0)
    {
      $this->defaultdb->where(array('concepts.subject_id'=>$subject_id));
    }
	  $query = $this->defaultdb->get('concepts');
    return $query->result();
  }
 
  public function insert_concepts_to_db($data)
  {
    return $this->defaultdb->insert('concepts', $data);
  }

  public function getById($id)
  {
    $query = $this->defaultdb->get_where('concepts',array('id'=>$id));
    return $query->row_array();		  
  }

  public function getBySlug($slug)
  {
    $query = $this->defaultdb->get_where('concepts',array('slug'=>$slug));
    return $query->row_array();		  
  }

  public function getByEmail($slug)
  {
    $query = $this->defaultdb->get_where('concepts',array('email'=>$slug));
    return $query->row_array();		  
  }

  public function getCode($id)
  {
	 	$this->defaultdb->select('site_id');
    $query = $this->defaultdb->get_where('admin_sites',array('admin_id'=>$id));
	
    return $query->result();	  
  }

  public function update_info($data,$id)
  {
    $this->defaultdb->where('concepts.id',$id);
    return $this->defaultdb->update('concepts', $data);
  }

  public function update_info_email($data,$id)
  {
    $this->defaultdb->where('concepts.email',$id);
    return $this->defaultdb->update('concepts', $data);
  }

  public function delete_a_concepts_sites($id)
  {
    $this->defaultdb->where('admin_sites.admin_id',$id);
    return $this->defaultdb->delete('admin_sites');
  }

  public function delete_a_concepts($id)
  {
    $this->defaultdb->where('concepts.id',$id);
    return $this->defaultdb->delete('concepts');
  }
  
  public function get_concepts($q)
  {
    $this->defaultdb->select('id,name');
    $this->defaultdb->like('name', $q);
    $query = $this->defaultdb->get_where('concepts');
    if($query->num_rows > 0){
      foreach ($query->result_array() as $row){
	   $row1['label'] = htmlentities(stripslashes($row['name']));
	   $row1['value'] = htmlentities(stripslashes($row['name']));
	   $row1['id'] = $row['id'];
		 $row_set[] = $row1;
      }
      echo json_encode($row_set); //format the array into json data
    }
  }  
}
