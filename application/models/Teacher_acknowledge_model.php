<?php
class Teacher_acknowledge_model extends CI_Model {

  public function __construct()
  {
    parent::__construct();
    $this->defaultdb = $this->load->database('default', TRUE);
  }
  
   public function get_district_name($district_id)
  {
    $query = $this->defaultdb->query("SELECT * from ssc_districts where id = ".$district_id);
    return $query->result();
  }
  
  public function get_block_name($block_id)
  {
    $query = $this->defaultdb->query("SELECT * from ssc_blocks where id = ".$block_id);
    return $query->result();
  }
  
  public function get_questions()
  {
    $query = $this->defaultdb->query("SELECT * from tutorial order by id");
    return $query->result();
  }
  
  /*public function get_all($state_id = 0, $district_ids = array())
  {
    if ($state_id > 0)
      $state_cond = "ssc_issues.state_id = ".$state_id;
    else
      $state_cond = "ssc_issues.state_id > 0";
      
    if (count($district_ids) > 0)
      $district_cond = "ssc_issues.district_id in (".implode(',',$district_ids).")";
    else
      $district_cond = "ssc_issues.district_id > 0";
    $query = $this->defaultdb->query("SELECT ssc_issues.district_id, ssc_districts.name, tutorial_users.question_id, AVG(rating) as avg_rating FROM `tutorial_users` JOIN `ssc_issue_details` ON `tutorial_users`.`issue_detail_id` = `ssc_issue_details`.`id` JOIN `ssc_issues` ON `ssc_issue_details`.`issue_id` = `ssc_issues`.`id` JOIN ssc_districts ON ssc_districts.id = ssc_issues.district_id where $state_cond AND $district_cond GROUP BY `tutorial_users`.`question_id`, ssc_issues.district_id");
    return $query->result();
  }
  
  public function get_all_block_wise($state_id = 0, $district_ids = array())
  {
    if ($state_id > 0)
      $state_cond = "ssc_issues.state_id = ".$state_id;
    else
      $state_cond = "ssc_issues.state_id > 0";
      
    if (count($district_ids) > 0)
      $district_cond = "ssc_issues.district_id in (".implode(',',$district_ids).")";
    else
      $district_cond = "ssc_issues.district_id > 0";
    
    $query = $this->defaultdb->query("SELECT ssc_issues.block_id, ssc_blocks.name, tutorial_users.question_id, AVG(rating) as avg_rating FROM `tutorial_users` JOIN `ssc_issue_details` ON `tutorial_users`.`issue_detail_id` = `ssc_issue_details`.`id` JOIN `ssc_issues` ON `ssc_issue_details`.`issue_id` = `ssc_issues`.`id` JOIN ssc_blocks ON ssc_blocks.id = ssc_issues.block_id where $state_cond AND $district_cond GROUP BY `tutorial_users`.`question_id`, ssc_issues.block_id");
    return $query->result();
  }*/

 
  
  public function get_all_ack($state_id = 0, $district_ids = array(), $current_date='')
  {
    if ($state_id > 0)
      $state_cond = "state_id = ".$state_id;
    else
      $state_cond = "state_id > 0";
    
    if(!empty($district_ids) > 0)
      $district_cond = "district_id in (".implode(',',$district_ids).")";
    else
      $district_cond = "district_id > 0";
    
    if($current_date == '')
      $current_date = date('Y-m-d');
    
    $sql = "select * from ssc_teacher_feedback_rankings where $state_cond AND $district_cond and created_at ='$current_date' ";
    $query = $this->defaultdb->query($sql);
    $result = $query->result();
    return $result;
  }
  
  function check_feedback_data($cond)
  {
    $this->defaultdb->where($cond);
    $query = $this->defaultdb->get('ssc_teacher_feedback_rankings');
    return $query->num_rows();
  }
  
  public function select_all_ack($state_id = 0, $district_ids = array())
  {
    if ($state_id > 0)
      $state_cond = "ssc_districts.state_id = ".$state_id;
    else
      $state_cond = "ssc_districts.state_id > 0";
      
    if (count($district_ids) > 0)
      $district_cond = "ssc_sampark_training_details.district_id in (".implode(',',$district_ids).")";
    else
      $district_cond = "ssc_sampark_training_details.district_id > 0";
      
    $sql = "SELECT ssc_sampark_training_details.district_id, ssc_districts.name, tutorial_users.question_id, AVG(rating) as avg_rating FROM `tutorial_users` JOIN `ssc_sampark_training_details` ON `tutorial_users`.`issue_detail_id` = `ssc_sampark_training_details`.`id` JOIN `ssc_training_acknowledge_delivery` ON `ssc_sampark_training_details`.`id` = `ssc_training_acknowledge_delivery`.`training_request_id` JOIN ssc_districts ON ssc_districts.id = ssc_sampark_training_details.district_id where $state_cond AND $district_cond GROUP BY `tutorial_users`.`question_id`, ssc_sampark_training_details.district_id";  
    $query = $this->defaultdb->query($sql);
    $result = $query->result();
    //echo $this->defaultdb->last_query();
    return $result;
  }
  
  public function get_all_ack_block_wise($state_id = 0, $district_ids = array())
  {
    if ($state_id > 0)
      $state_cond = "ssc_blocks.state_id = ".$state_id;
    else
      $state_cond = "ssc_blocks.state_id > 0";
      
    if (count($district_ids) > 0)
      $district_cond = "ssc_sampark_training_details.district_id in (".implode(',',$district_ids).")";
    else
      $district_cond = "ssc_sampark_training_details.district_id > 0";
      
    $query = $this->defaultdb->query("SELECT ssc_sampark_training_details.block_id, ssc_blocks.name, tutorial_users.question_id, AVG(rating) as avg_rating FROM `tutorial_users` JOIN `ssc_sampark_training_details` ON `tutorial_users`.`issue_detail_id` = `ssc_sampark_training_details`.`id` JOIN `ssc_training_acknowledge_delivery` ON `ssc_sampark_training_details`.`id` = `ssc_training_acknowledge_delivery`.`training_request_id` JOIN ssc_blocks ON ssc_blocks.id = ssc_sampark_training_details.block_id where $state_cond AND $district_cond GROUP BY `tutorial_users`.`question_id`, ssc_sampark_training_details.block_id");
    $result = $query->result();
    //echo $this->defaultdb->last_query();
    return $result;
  }
  
  public function insert_teacher_feedback_rankings($data)
  {
    $this->defaultdb->insert('teacher_feedback_rankings', $data);
    return $this->defaultdb->insert_id();
  }
  
}

?>
