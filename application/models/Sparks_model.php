<?php
//error_reporting(0);
class Sparks_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        //Enable profiler
        $this->output->enable_profiler(false);
        $this->defaultdb = $this->load->database('default', TRUE);
    }

    public function get_all_users($current_user_id = 0) {
        if ($current_user_id > 0) {
            $this->defaultdb->where('id != ', $current_user_id);
        }
        $query = $this->defaultdb->get('sparks');
        return $query->result();
    }

    public function add($data) {
        $count = $this->db->select('*')->where('login_id', $data['login_id'])->get('sparks')->num_rows();
        if($count == 0){
            $this->defaultdb->insert('sparks', $data);
            return $this->defaultdb->insert_id();
        }else{
            return 0;
        }
    }

    public function edit($data, $id) {
        $this->defaultdb->where('id', $id);
        $this->defaultdb->update('sparks', $data);
    }    

    public function getById($id) {
        $query = $this->defaultdb->get_where('sparks', array('id' => $id));
        return $query->row_array();
    }

    public function getuserBystate($state_id = 0, $district_id = 0) {
        //echo $state_id;echo $district_id;
        //exit();
        if ($state_id > 0) {
            $this->defaultdb->where('ssc_sparks.state_id', $state_id);
        }
        if ($district_id > 0) {
            $this->defaultdb->where('ssc_sparks.district_id', $district_id);
        } else {
            $this->defaultdb->where('ssc_sparks.district_id != ', 0);
        }
        $query = $this->defaultdb->get('ssc_sparks');
        return $query->result();
    }

    public function update_info($data, $id='') {
        if($id != '')
          $this->defaultdb->where('id', $id);
        return $this->defaultdb->update('sparks', $data);
    }

    public function delete_a_user($id) {
        $this->defaultdb->where('id', $id);
        $this->defaultdb->delete('sparks');

        $this->defaultdb->where('user_id', $id)->delete('spark_disrict_block');
    }
    
     public function getAlluser() {
        $query = $this->defaultdb->get('ssc_sparks');
        return $query->result();
    }
    
    public function  getSparkByState($state_id) {
    if ($state_id > 0) {
        $this->defaultdb->where('ssc_sparks.state_id', $state_id);
        $this->defaultdb->where_in('ssc_sparks.role', array('field_user'));
    }

    $query = $this->defaultdb->get('ssc_sparks');
    return $query->result();
    }

    public function add_user_state($data)
    {
      $this->defaultdb->insert('spark_user_state', $data);
      return $this->defaultdb->insert_id();
    }

    public function get_sparks_by_role($role){
        return $this->db->select('sparks.*, states.name as state_name, managers.name as manager_name, managers.login_id as manager_login')->join('states', 'sparks.state_id = states.id')->join('sparks managers','sparks.manager_id = managers.id','LEFT')->where_in('sparks.role', $role)->get('sparks')->result();
    }

    /*public function change_state($state_id, $user_id, $d, $state_head_id = 0){
        $user = $this->db->select('state_id')->where('id', $user_id)->get('sparks')->row();
        $new_date = date_format(date_create("01-".$d), "Y-m-d");
        $temp = date('Y-m', strtotime("-1 months", strtotime($new_date)));
        $old_date = $temp."-30";
        if($user->state_id != $state_id){
          $this->db->where('id', $user_id)->update('sparks', array('state_id'=>$state_id, 'manager_id'=>$state_head_id));

          $this->db->where('user_id', $user_id)->update('spark_user_state', array('end_date'=>$old_date));

          $this->db->insert('spark_user_state', array('user_id'=>$user_id, 'state_id'=>$state_id, 'start_date'=>$new_date));
        }
    }*/
    
    public function change_state($state_id, $user_id, $d, $state_head_id = 0){
        $user = $this->db->select('state_id')->where('id', $user_id)->get('sparks')->row();
        $userState = $this->db->select('id')->where('user_id', $user_id)->order_by('id', 'desc')->limit(1)->get('spark_user_state')->row();
        $new_date = date_format(date_create("01-".$d), "Y-m-d");
        $temp = date('Y-m', strtotime("-1 months", strtotime($new_date)));
        $old_date = $temp."-30";
        if($user->state_id != $state_id){
          $this->db->where('id', $user_id)->update('sparks', array('state_id'=>$state_id, 'manager_id'=>$state_head_id));

          $this->db->where('id', $userState->id)->update('spark_user_state', array('end_date'=>$old_date));

          $this->db->insert('spark_user_state', array('user_id'=>$user_id, 'state_id'=>$state_id, 'start_date'=>$new_date));
        }
    }

    public function check_user_id($id){
        $count = $this->db->select('*')->where('login_id', $id)->get('sparks')->num_rows();
        if($count == 0)
            return 1;
        else
            return 0;

    }

    public function save_user_district_block($user_id, $data){
      $this->defaultdb->insert('spark_users_district_block', array('user_id'=>$user_id, 'district_id'=>$data[0], 'block_id'=>$data[1]));
    }

    public function delete_user_district_block($user_id, $data = array()){
        if (count($data) == 2)
          $this->defaultdb->where(array('user_id'=>$user_id, 'district_id'=>$data[0], 'block_id'=>$data[1]));
        else
          $this->defaultdb->where(array('user_id'=>$user_id));
          $this->defaultdb->delete('spark_users_district_block');
    }

    public function get_member_district_ids($id){
        $this->defaultdb->select('district_id');
        $this->defaultdb->where('user_id', $id);
        return $this->defaultdb->get('spark_users_district_block')->result();
    }

    public function get_member_block_ids($id){
        $this->defaultdb->select('block_id');
        $this->defaultdb->where('user_id', $id);
        return $this->defaultdb->get('spark_users_district_block')->result();
    }
    
    public function save_high_spark_summary($data){
      $this->defaultdb->insert('ssc_high_enrol_spark_report', $data);
      //echo"<br>".$this->defaultdb->last_query();
    }
    
    public function check_high_spark_summary($cond){
      $this->defaultdb->where($cond);
      $this->defaultdb->from('ssc_high_enrol_spark_report');
      $result = $this->defaultdb->count_all_results();
      //echo"<br>".$this->defaultdb->last_query();
      return $result;
    }
    
    public function fetchHighEnrolSpark($state_id, $login_id, $date_1){
      //echo "<br>".$state_id." >>> ".$login_id." >>> ".$date_1;
      $this->defaultdb->where('state_id', $state_id);
      $this->defaultdb->where('spark_id', $login_id);
      $this->defaultdb->where('month_year', $date_1);
      $result = $this->defaultdb->get('ssc_high_enrol_spark_report')->result();
      //echo"<br>".$this->defaultdb->last_query();exit;
      return $result;
    }
    
    public function get_state_high_enrol_report($month_year){
      $this->defaultdb->select('ss.name as state_name, SUM(ser.school_visited) AS SCHOOL_VISITED, SUM(ser.he_visit) AS HE_visit, SUM(ser.le_visit) AS LE_visit')->join('ssc_states as ss', 'state_id = ss.id');
      $this->defaultdb->where('month_year', $month_year);
      $this->defaultdb->group_by('state_id');
      return $this->defaultdb->get('ssc_high_enrol_spark_report as ser')->result();
    }
    
    public function update_high_spark_summary($data, $cond) {
        $this->defaultdb->where($cond);
        $this->defaultdb->update('ssc_high_enrol_spark_report', $data);
    }    
    
  public function user_distinct_districts($user_id)
  {
    $this->defaultdb->join('ssc_spark_users_district_block', 'districts.id = spark_users_district_block.district_id');
    $this->defaultdb->where('ssc_spark_users_district_block.user_id', $user_id);
    $this->defaultdb->select('districts.*');
    $this->defaultdb->group_by('district_id');
    $query = $this->defaultdb->get('ssc_districts');
    return $query->result();      
  }
  
  public function insert_user_home_block($data)
  {
    return $this->defaultdb->insert('spark_home_blocks', $data);
  }
  
  function get_last_home_blocks($user_id='', $limit='')
  {
		if($user_id != '')
			$this->defaultdb->where('user_id', $user_id);
    $this->defaultdb->order_by('id', 'desc');
    if($limit != '')
      $this->defaultdb->limit($limit);
    $query = $this->defaultdb->get('spark_home_blocks');
    return $query->result();
  }
  
  public function update_home_blocks($data, $id) {
      $this->defaultdb->where('id', $id);
      return $this->defaultdb->update('spark_home_blocks', $data);
  }  
  
  public function  getSparkHomeBlockByDuration($user_id, $from_date = '', $end_date = '') {

      $row = array();
      if($from_date !='' && $end_date !=''){

        $month_end = date('Y-m-t', strtotime($end_date));

        $data = $this->getMonthsInRange($from_date, $end_date);
        $dates = implode("','", $data);
    
        $query = "SELECT * FROM ssc_spark_home_blocks WHERE  user_id = '$user_id' ";
        //$query .=" AND (DATE_FORMAT(start_date, '%Y-%m') in('$dates') OR DATE_FORMAT(end_date, '%Y-%m') in('$dates') OR (";
        //$query .= " start_date <= '$from_date' AND (end_date >= '$month_end' OR end_date IS NULL))) ORDER BY name ";  
      
        $query_result = $this->defaultdb->query($query);
        $row = $query_result->result();
      }
      return $row;
  }
  
  function getMonthsInRange($startDate, $endDate) {
    $dates = array();
    while (strtotime($startDate) <= strtotime($endDate)) {
        //$months[] = array('year' => date('Y', strtotime($startDate)), 'month' => date('m', strtotime($startDate)), );
        $dates[] = date('Y', strtotime($startDate))."-".date('m', strtotime($startDate));
        $startDate = date('d M Y', strtotime($startDate.'+ 1 month'));
    }
    return $dates;
  }
  
  function set_spark_state_end_date($spark_id)
  {
		$end_date = date('Y-m-d');
		$userState = $this->db->select('id')->where('user_id', $spark_id)->order_by('id', 'desc')->limit(1)->get('spark_user_state')->row();
		$this->db->where('id', $userState->id)->update('spark_user_state', array('end_date'=>$end_date));
	}
    
}
