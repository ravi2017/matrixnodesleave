<?php
class Assessment_model extends CI_Model {

  public function __construct()
  {
    parent::__construct();
     error_log(" Assessment_model model construct ---------");
    $this->defaultdb = $this->load->database('default', TRUE);
  }

  public function getBySchoolVisitId($school_visit_id)
  {
	$this->defaultdb->where("school_visit_id",$school_visit_id);   
    $this->defaultdb->order_by("assessment_id", "desc");
    $query = $this->defaultdb->get('assessment');
    return $query->result();
  }

  public function getById($id)
  {
    $query = $this->defaultdb->get_where('assessment',array('assessment_id'=>$id));
    return $query->row_array();		  
  }
  
  public function add($data)
  {
    $this->defaultdb->insert('assessment', $data);
    return $this->defaultdb->insert_id();
  }
  
  public function addNew($data)
  {
    $this->defaultdb->insert('assessment_new', $data);
    return $this->defaultdb->insert_id();
  }
  
  public function getByActivityId($type, $activity_id)
  {
    $this->defaultdb->join('ssc_observation_questions', 'ssc_observation_questions.id = ssc_assessment_new.question_id');
    $this->defaultdb->where(array('ssc_assessment_new.activity_type'=>$type, 'ssc_assessment_new.activity_id'=>$activity_id));
    $query = $this->defaultdb->get('ssc_assessment_new');
    return $query->result();		  
  }
  
  public function getWhatsappByActivityId($type, $activity_id)
  {
    $this->defaultdb->join('ssc_observation_questions', 'ssc_observation_questions.id = ssc_assessment_new.question_id');
    $this->defaultdb->where(array('ssc_assessment_new.activity_type'=>$type, 'ssc_assessment_new.activity_id'=>$activity_id, 'ssc_observation_questions.whatsapp_flag'=>1, 'status'=>1));
    $query = $this->defaultdb->get('ssc_assessment_new');
    return $query->result();		  
  }
  
  function getStateClassAssessment($startdate, $enddate, $state_id, $classess)
  {
    $sdate = date('Y-m-d', strtotime($startdate));
    $edate = date('Y-m-d', strtotime($enddate));
    
    $month_start = date('Y-m-d', strtotime($startdate));
    $month_end = date('Y-m-t', strtotime($enddate));
    $data = $this->Users_model->getMonthsInRange($startdate, $enddate);
    $dates = implode("','", $data);
    
    $class_str = implode(',', $classess);
    
    $select  = "select ssv.id as ssv_id, ssv.state_id as state_id, district_id, cluster_id, block_id, school_id, user_id, activity_date,  
    ssa.subject_id, sso.class_id, assessment_id, question, question_id, answer
    from ssc_school_visits ssv JOIN ssc_assessment_new ssa ON ssv.id = ssa.activity_id JOIN ssc_observation_questions sso ON ssa.question_id = sso.id ";
    $select .= " WHERE ssa.activity_type='school_visit' AND ssv.state_id = '$state_id' and sso.class_id IN ($class_str) ";
    $select .= " AND sso.subject_wise=1 AND sso.question_type = 'yes-no-na' AND sso.productivity_flag='1' ";
    $select .= " and date(ssv.activity_date) between str_to_date('$startdate','%d-%m-%Y') and  str_to_date('$enddate','%d-%m-%Y') ";
    $select .= " and ssa.answer in (1, 2) "; // and ssv.status='approved'
    $select .= " and ssv.user_id in (
              SELECT `ssc_spark_user_state`.`user_id` FROM (`ssc_sparks`) JOIN `ssc_spark_user_state` ON `ssc_sparks`.`id` = `ssc_spark_user_state`.`user_id` WHERE `ssc_sparks`.`status` = 1 AND `ssc_spark_user_state`.`state_id` = '$state_id' AND `ssc_sparks`.`role` IN ('field_user')  AND (DATE_FORMAT(ssc_spark_user_state.start_date, '%Y-%m') in('$dates') OR DATE_FORMAT(ssc_spark_user_state.end_date, '%Y-%m') in('$dates') OR ( ssc_spark_user_state.start_date <= '$month_start' AND (ssc_spark_user_state.end_date >= '$month_end' OR ssc_spark_user_state.end_date IS NULL)))
            ) order by sso.sort_order asc, sso.class_id asc ";  
    $select_query = $this->defaultdb->query($select);
    $row = $select_query->result(); 
    return $row;    
  }
 
  
  function getSparkSchoolAssessment($startdate, $enddate, $state_id='', $classes, $spark_id, $subject_wise = 1)
  {
    $sdate = date('Y-m-d', strtotime($startdate));
    $edate = date('Y-m-d', strtotime($enddate));
    
    $month_start = date('Y-m-d', strtotime($startdate));
    $month_end = date('Y-m-t', strtotime($enddate));
    $data = $this->Users_model->getMonthsInRange($startdate, $enddate);
    $dates = implode("','", $data);
    
    
    $select  = "select ssv.id as ssv_id, ssv.state_id as state_id, district_id, cluster_id, block_id, school_id, user_id, activity_date,  
    ssa.subject_id, sso.class_id, assessment_id, question, question_id, answer, subjects, classes
    from ssc_school_visits ssv JOIN ssc_assessment_new ssa ON ssv.id = ssa.activity_id JOIN ssc_observation_questions sso ON ssa.question_id = sso.id ";
    $select .= " WHERE ssa.activity_type='school_visit' ";
    
    if($state_id != '')
      $select .= "  AND ssv.state_id = '$state_id' ";
    if (count($classes) > 0) {
      $class_str = implode(',', $classes);
      $select .= " AND sso.class_id IN ($class_str) ";
    }
    if ($subject_wise != "") {
      $select .= " AND sso.subject_wise=1";
    }

    $select .= " AND sso.question_type = 'yes-no-na' AND sso.productivity_flag='1' ";
    $select .= " and date(ssv.activity_date) between str_to_date('$startdate','%d/%m/%Y') and  str_to_date('$enddate','%d/%m/%Y') ";
    $select .= " and ssv.user_id = '$spark_id' and ssa.answer in (1, 2) "; 
    //$select .= " and ssv.status='approved' ";
    $select .= " order by sso.sort_order asc, sso.class_id asc ";  
    $select_query = $this->defaultdb->query($select);
    $row = $select_query->result(); 
    return $row;    
  }
  
  function getSchoolAssessmentList($startdate, $enddate, $classes, $state_id, $district_id='', $block_id='', $subject_wise = 1)
  {
    $class_str = implode(',', $classes);
    
    $select  = "select ssv.id as ssv_id, ssv.state_id as state_id, district_id, cluster_id, block_id, school_id, user_id, activity_date,  
    ssa.subject_id, sso.class_id, assessment_id, question, question_id, answer, subjects, classes
    from ssc_school_visits ssv JOIN ssc_assessment_new ssa ON ssv.id = ssa.activity_id JOIN ssc_observation_questions sso ON ssa.question_id = sso.id ";
    $select .= " WHERE ssa.activity_type='school_visit' AND ssv.state_id = '$state_id'";
    
    if (count($classes) > 0) {
      $class_str = implode(',', $classes);
      $select .= " AND sso.class_id IN ($class_str) ";
    }
    if ($subject_wise != "") {
      $select .= " AND sso.subject_wise=1";
    }

    $select .= " AND sso.question_type = 'yes-no-na' AND sso.productivity_flag='1' ";
    
    $select .= " and date(ssv.activity_date) between str_to_date('$startdate','%d/%m/%Y') and  str_to_date('$enddate','%d/%m/%Y') ";
    $select .= " and ssa.answer in (1, 2) "; 
    //$select .= " and ssv.status='approved' ";
    if(!empty($district_id))
        $select .= " and ssv.district_id = '$district_id' ";
    if(!empty($block_id))
        $select .= " and ssv.block_id = '$block_id' ";
    $select .= " order by sso.sort_order asc, sso.class_id asc ";  
    $select_query = $this->defaultdb->query($select);
    $row = $select_query->result(); 
    return $row;    
  }
 
  function getDistrictSchoolAssessmentList($startmonth, $startyear, $classes, $state_id, $district_id='')
  {
    $class_str = implode(',', $classes);
    
    $select  = "select ssv.id as ssv_id, ssv.state_id as state_id, district_id, cluster_id, block_id, school_id, user_id, activity_date,  
    ssa.subject_id, sso.class_id, assessment_id, question, question_id, answer
    from ssc_school_visits ssv JOIN ssc_assessment_new ssa ON ssv.id = ssa.activity_id JOIN ssc_observation_questions sso ON ssa.question_id = sso.id ";
    $select .= " WHERE ssa.activity_type='school_visit' AND ssv.state_id = '$state_id' and sso.class_id IN ($class_str) ";
    $select .= " AND sso.subject_wise=1 AND sso.question_type = 'yes-no-na' AND sso.productivity_flag='1' ";
    $select .= " and month(ssv.activity_date) = '$startmonth' and year(ssv.activity_date) = '$startyear' ";
    $select .= " and ssa.answer in (1, 2) "; 
    //$select .= " and ssv.status='approved' ";
    if(!empty($district_id))
        $select .= " and ssv.district_id = '$district_id' ";
    $select .= " order by sso.sort_order asc, sso.class_id asc ";  
    $select_query = $this->defaultdb->query($select);
    $row = $select_query->result(); 
    return $row;    
  }
  
  
  function selectSparkRating($cond)
  {
    $this->defaultdb->where($cond);   
    $query = $this->defaultdb->get('spark_rating_reports');
    return $query->result();
  }
  
  function addSparkRating($data)
  {
    $this->defaultdb->insert('spark_rating_reports', $data);
    return $this->defaultdb->insert_id();
  }
  
  function updateSparkRating($data, $id) {
      $this->defaultdb->where('id', $id);
      $this->defaultdb->update('spark_rating_reports', $data);
  }    
  
  function selectSparkRatingDetail($cond)
  {
    $this->defaultdb->where($cond);   
    $query = $this->defaultdb->get('spark_rating_detail_reports');
    return $query->result();
  }
  
  function addSparkRatingDetails($data)
  {
    $this->defaultdb->insert('spark_rating_detail_reports', $data);
    return $this->defaultdb->insert_id();
  }
  
  function updateSparkRatingDetails($data, $id) {
      $this->defaultdb->where('id', $id);
      $this->defaultdb->update('spark_rating_detail_reports', $data);
  }    
  
  function getSparkRatingReport($startmonth, $startyear, $state_id='', $district_id='', $duration = '')
  {
    $smonth = sprintf('%02d', $startmonth);
    if($duration == ''){
      $date_select = " where month(srr.activity_date) = '$smonth' and year(srr.activity_date) = '$startyear' ";
    }
    else{
      if($duration == 'YTD'){
        $sess_month = SESSION_START_MONTH_ABC;
        $sess_year = date('Y');
        if($startmonth < $sess_month || $startyear < $sess_year){
          $sess_year = date('Y') - 1;
        }
        $startdate        = $sess_year."-".sprintf('%02d', $sess_month)."-01";
      }
      else{
        $startdate = $startyear.'-'.$smonth.'-01';
      }
      $enddate = date('Y-m-t', strtotime($startyear.'-'.$smonth.'-01'));
      
      $date_select = " where activity_date between '$startdate' AND '$enddate' ";
    }
    $select  = "SELECT * FROM ssc_spark_rating_reports srr ".$date_select;
    if($district_id != '')
      $select .= " AND srr.district_id = '$district_id' ";
    if($state_id != '')
      $select .= " AND srr.state_id = '$state_id' ";
    //echo $select;  
    $select_query = $this->defaultdb->query($select);
    $row = $select_query->result(); 
    return $row;    
  }
  
  function getSparkRatingCount($report_ids)
  {
    $select = "SELECT count(id) Total, subject_rating FROM `ssc_spark_rating_detail_reports` WHERE `rating_report_id` IN (".implode(',', $report_ids).") GROUP by subject_rating ";
    $select_query = $this->defaultdb->query($select);
    $row = $select_query->result_array(); 
    return $row;    
  }
  
  function getReportAssessmentList($question_ids, $class_id, $district_id='')
  {
    $class_str = implode(',', $classes);
    
    $select  = "select ssv.id as ssv_id, ssv.state_id as state_id, district_id, cluster_id, block_id, school_id, user_id, activity_date,  
    ssa.subject_id, sso.class_id, assessment_id, question, question_id, answer
    from ssc_school_visits ssv JOIN ssc_assessment_new ssa ON ssv.id = ssa.activity_id JOIN ssc_observation_questions sso ON ssa.question_id = sso.id ";
    $select .= " WHERE ssa.activity_type='school_visit' AND ssv.state_id = '$state_id' and sso.class_id IN ($class_str) ";
    $select .= " AND sso.subject_wise=1 AND sso.question_type = 'yes-no-na' AND sso.productivity_flag='1' ";
    $select .= " and month(ssv.activity_date) = '$startmonth' and year(ssv.activity_date) = '$startyear' ";
    $select .= " and ssa.answer in (1, 2) "; 
    //$select .= " and ssv.status='approved' ";
    if(!empty($district_id))
        $select .= " and ssv.district_id = '$district_id' ";
    $select .= " order by sso.sort_order asc, sso.class_id asc ";  
    $select_query = $this->defaultdb->query($select);
    $row = $select_query->result(); 
    return $row;    
  }  
  
  function getSchoolAssessmentData($startdate, $enddate, $state_id, $district_id='', $block_id='', $cluster_id='')
  {
    $sdate = date('Y-m-d', strtotime($startdate));
    $edate = date('Y-m-d', strtotime($enddate));
    
    $month_start = date('Y-m-d', strtotime($startdate));
    $month_end = date('Y-m-t', strtotime($enddate));
    $data = $this->Users_model->getMonthsInRange($startdate, $enddate);
    $dates = implode("','", $data);
    
    
    $select  = "select ssv.id as ssv_id, ssv.state_id as state_id, district_id, cluster_id, block_id, school_id, user_id, activity_date,  
    ssa.subject_id, sso.class_id, assessment_id, question, question_id, answer, subjects, classes
    from ssc_school_visits ssv JOIN ssc_assessment_new ssa ON ssv.id = ssa.activity_id JOIN ssc_observation_questions sso ON ssa.question_id = sso.id ";
    $select .= " WHERE ssa.activity_type='school_visit' AND ssv.state_id = '$state_id'";
    if($district_id != '')
      $select .= " AND ssv.district_id = '$district_id'";
    if($block_id != '')
      $select .= " AND ssv.block_id = '$block_id'";
    if($cluster_id != '')
      $select .= " AND ssv.cluster_id = '$cluster_id'";
    $select .= " AND sso.question_type = 'yes-no-na' AND sso.productivity_flag='1' ";
    $select .= " and date(ssv.activity_date) between str_to_date('$startdate','%d/%m/%Y') and  str_to_date('$enddate','%d/%m/%Y') ";
    $select .= " and ssa.answer in (1, 2) "; 
    //$select .= " and ssv.status='approved' ";
    $select .= " order by ssv.id desc, sso.sort_order asc, sso.class_id asc ";  
    $select_query = $this->defaultdb->query($select);
    $row = $select_query->result(); 
    return $row;    
  }
  
  function getClusterRatingReport($startmonth, $startyear, $state_id, $district_id, $cluster_ids=array(), $duration='')
  {
    $smonth = sprintf('%02d', $startmonth);
    $clusterids = implode(',', $cluster_ids);
    if($duration == ''){
      $date_select = " where month(srr.activity_date) = '$startmonth' and year(srr.activity_date) = '$startyear' ";
    }
    else{
      if($duration == 'YTD'){
        $sess_month = SESSION_START_MONTH_ABC;
        $sess_year = date('Y');
        if($startmonth < $sess_month || $startyear < $sess_year){
          $sess_year = date('Y') - 1;
        }
        $startdate        = $sess_year."-".sprintf('%02d', $sess_month)."-01";
      }
      else{
        $startdate = $startyear.'-'.$smonth.'-01';
      }
      $enddate = date('Y-m-t', strtotime($startyear.'-'.$smonth.'-01'));
      
      $date_select = " where activity_date between '$startdate' AND '$enddate' ";
    }
    
    $select  = "SELECT srr.*, sss.cluster_id FROM ssc_spark_rating_reports srr JOIN ssc_schools sss ON sss.id = srr.school_id ".$date_select;
    $select .= " AND srr.district_id = '$district_id' ";
    $select .= " AND srr.state_id = '$state_id' ";
    $select .= " AND sss.cluster_id in($clusterids) ";
    $select_query = $this->defaultdb->query($select);
    $row = $select_query->result(); 
    return $row;    
  }
  
  function getSchoolRatingReport($startmonth, $startyear, $state_id, $district_id, $cluster_id, $duration= '')
  {
    $smonth = sprintf('%02d', $startmonth);
    if($duration == ''){
      $date_select = " where month(srr.activity_date) = '$startmonth' and year(srr.activity_date) = '$startyear' ";
    }
    else{
      if($duration == 'YTD'){
        $sess_month = SESSION_START_MONTH_ABC;
        $sess_year = date('Y');
        if($startmonth < $sess_month || $startyear < $sess_year){
          $sess_year = date('Y') - 1;
        }
        $startdate        = $sess_year."-".sprintf('%02d', $sess_month)."-01";
      }
      else{
        $startdate = $startyear.'-'.$smonth.'-01';
      }
      $enddate = date('Y-m-t', strtotime($startyear.'-'.$smonth.'-01'));
      
      $date_select = " where activity_date between '$startdate' AND '$enddate' ";
    }
    $select  = "SELECT srr.*, count(school_id) as school_count, sss.name as school_name, dise_code FROM ssc_spark_rating_reports srr JOIN ssc_schools sss ON sss.id = srr.school_id ".$date_select;
    $select .= " AND srr.district_id = '$district_id' ";
    $select .= " AND srr.state_id = '$state_id' ";
    $select .= " AND sss.cluster_id = '$cluster_id' group by school_id ";
    $select_query = $this->defaultdb->query($select);
    $row = $select_query->result(); 
    return $row;    
  }
  
}

?>
