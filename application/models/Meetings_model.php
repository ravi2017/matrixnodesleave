<?php
class Meetings_model extends CI_Model {

  public function __construct()
  {
    parent::__construct();
    $this->defaultdb = $this->load->database('default', TRUE);
  }
  
  public function get_all_count($user_ids, $start_date, $end_date, $source='')
  {
    $this->defaultdb->where_in("user_id",$user_ids);
    $this->defaultdb->where("activity_date >= ",$start_date." 00:00:00");
    $this->defaultdb->where("activity_date <= ",$end_date." 23:59:59");
    $this->defaultdb->where("status = ","approved");
    if($source != '')
      $this->defaultdb->where("source", $source);
    $this->defaultdb->select("count(*) as total");
    $query = $this->defaultdb->get('meetings');
    //echo "<br>query : ".$this->defaultdb->last_query();
    return $query->row_array();
  }

  public function get_all($user_id='', $start_date, $end_date, $state_id='', $district_id='', $block_id='')
  {
    if($state_id != '')
      $this->defaultdb->where("state_id", $state_id);
    if($district_id != '')
      $this->defaultdb->where("district_id",$district_id);
    if($block_id != '')
      $this->defaultdb->where("block_id",$block_id);
    if($user_id != '')
      $this->defaultdb->where("user_id",$user_id);
    $this->defaultdb->where("activity_date >= ",$start_date." 00:00:00");
    $this->defaultdb->where("activity_date <= ",$end_date." 23:59:59");
    $this->defaultdb->where("status = ","approved");
    $query = $this->defaultdb->get('meetings');
    //echo "<br>".$this->defaultdb->last_query();
    return $query->result();
  }

  public function get_approval_activities($user_id='', $start_date, $end_date, $status = '', $state_id='')
  {
    if($user_id != '')
      $this->defaultdb->where("user_id",$user_id);
    $this->defaultdb->where("activity_date >= ",$start_date." 00:00:00");
    $this->defaultdb->where("activity_date <= ",$end_date." 23:59:59");
    if($state_id != '')
      $this->defaultdb->where("state_id", $state_id);
    if($status != 'all')
		$this->defaultdb->where("status != ","approved");
    $this->defaultdb->order_by("activity_date", "asc");
    $query = $this->defaultdb->get('meetings');
    return $query->result();
  }
  
  public function get_cluster_meetings($cluster_id, $start_date, $end_date)
  {
    $this->defaultdb->where("cluster_id",$cluster_id);
    $this->defaultdb->where("activity_date >= ",$start_date);
    $this->defaultdb->where("activity_date <= ",$end_date);
    $query = $this->defaultdb->get('meetings');
    return $query->result();
  }
  
  public function get_block_meetings($block_id, $start_date, $end_date)
  {
    $this->defaultdb->where("block_id",$block_id);
    //$this->defaultdb->where("cluster_id",0);
    $this->defaultdb->where("activity_date >= ",$start_date);
    $this->defaultdb->where("activity_date <= ",$end_date);
    $query = $this->defaultdb->get('meetings');
    return $query->result();
  }
  
  public function get_district_meetings($district_id, $start_date, $end_date)
  {
    $this->defaultdb->where("district_id",$district_id);
   // $this->defaultdb->where("block_id",0);
    $this->defaultdb->where("activity_date >= ",$start_date);
    $this->defaultdb->where("activity_date <= ",$end_date);
    $query = $this->defaultdb->get('meetings');
    return $query->result();
  } 
	public function get_state_meetings($state_id, $start_date, $end_date)
  {
    $this->defaultdb->where("state_id",$state_id);
    //$this->defaultdb->where("block_id",0);
    $this->defaultdb->where("activity_date >= ",$start_date);
    $this->defaultdb->where("activity_date <= ",$end_date);
    $query = $this->defaultdb->get('meetings');
    return $query->result();
  }
	public function get_meetings($district_id,$user_id, $start_date, $end_date)
  {
    $this->defaultdb->where("district_id",$district_id);
    $this->defaultdb->where("user_id",$user_id);
    //$this->defaultdb->where("block_id",0);
    $this->defaultdb->where("activity_date >= ",$start_date);
    $this->defaultdb->where("activity_date <= ",$end_date);
		$this->defaultdb->select('count(*) as total');
    $query = $this->defaultdb->get('meetings');
    return $query->result();
  }
	public function get_user_meetings($state_id,$district_id,$user_id, $start_date, $end_date)
  {
    $this->defaultdb->where("state_id",$state_id);
    $this->defaultdb->where("district_id",$district_id);
    $this->defaultdb->where("user_id",$user_id);
    //$this->defaultdb->where("block_id",0);
    $this->defaultdb->where("activity_date >= ",$start_date);
    $this->defaultdb->where("activity_date <= ",$end_date);
    $this->defaultdb->select('*');
		
    $query = $this->defaultdb->get('meetings');
    return $query->result();
  }
  public function add($data)
  {
    $this->defaultdb->insert('meetings', $data);
    return $this->defaultdb->insert_id();
  }

  public function getByClusterId($cluster_id)
  {
    $query = $this->defaultdb->get_where('meetings',array('cluster_id'=>$cluster_id));
    return $query->row_array();		  
  }

  public function getById($id)
  {
    $query = $this->defaultdb->get_where('meetings',array('id'=>$id));
    return $query->row_array();		  
  }
  
  public function getActivityInfoForMeeting($user_id, $limit=10)
  {
    $this->defaultdb->where("user_id",$user_id);
    $this->defaultdb->order_by("id", "desc");
    $query = $this->defaultdb->get('meetings',$limit);
   
    return $query->result();
  
  }
  
  public function get_spark_telegram_count($spark_id, $telegram_flag=0)
  {
    $this->defaultdb->where("user_id",$spark_id);
    $this->defaultdb->where("telegram_flag", $telegram_flag)->order_by('activity_date','asc');
    $query = $this->defaultdb->get('meetings');
    //echo"<br>".$this->defaultdb->last_query();
    return $query->result();
  }
  
  public function update_meeting_data($data, $id) {
      $this->defaultdb->where('id', $id);
      $this->defaultdb->update('meetings', $data);
  }
  
  public function get_internal_meetings($spark_ids, $start_date, $end_date, $state_id='', $district_id='', $block_id='')
  {
    if($state_id != '')
      $this->defaultdb->where("state_id", $state_id);
    if($district_id != '')
      $this->defaultdb->where("district_id",$district_id);
    if($block_id != '')
      $this->defaultdb->where("block_id",$block_id);
    $this->defaultdb->where_in("user_id", $spark_ids);
    $this->defaultdb->where("is_internal", 0);
    $this->defaultdb->where("activity_date >= ",$start_date." 00:00:00");
    $this->defaultdb->where("activity_date <= ",$end_date." 23:59:59");
    $this->defaultdb->where("status = ","approved");
    $query = $this->defaultdb->get('meetings');
    //echo "<br>".$this->defaultdb->last_query();
    return $query->result();
  }
  
  public function get_meetwith_meetings($spark_ids=array(), $start_date, $end_date, $meet_with=array())
  {
    $this->defaultdb->where_in("meet_with", $meet_with);
    $this->defaultdb->where_in("user_id", $spark_ids);
    $this->defaultdb->where("activity_date >= ",$start_date." 00:00:00");
    $this->defaultdb->where("activity_date <= ",$end_date." 23:59:59");
    $this->defaultdb->where("status = ","approved");
    $query = $this->defaultdb->get('meetings');
    //echo "<br>".$this->defaultdb->last_query();
    return $query->result();
  }
  
  
}

?>
