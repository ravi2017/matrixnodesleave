<?php
class Classes_model extends CI_Model {

  public function __construct()
  {
    parent::__construct();
    $this->defaultdb = $this->load->database('default', TRUE);
  }

  public function get_class_count()
  {
    $query = $this->defaultdb->count_all_results('classes');
    return $query;
  }
  
  function get_by_states($state_id)
  {
    $query = $this->defaultdb->query("select class_id, group_concat(subject_id) as subjects from ssc_state_subjects where state_id = ".$state_id." group by class_id");
    $row = $query->result_array();
    return $row;
  }
  
  public function get_all()
  {
    $this->defaultdb->order_by("name", "asc");
    $query = $this->defaultdb->get('classes');
    return $query->result();
  }

  public function getById($id)
  {
    $query = $this->defaultdb->get_where('classes',array('id'=>$id));
    return $query->row_array();		  
  }
  
  public function getByIds($ids)
  {
    $this->defaultdb->where_in('id', $ids);
    $query = $this->defaultdb->get('classes');
    return $query->result();		  
  }
  
  public function getByName($name)
  {
    $query = $this->defaultdb->get_where('classes',array('name'=>$name));
    return $query->row_array();		  
  }
  
  public function getByOtherName($id, $name)
  {
    $query = $this->defaultdb->get_where('classes',array('id !='=>$id,'name'=>$name));
    return $query->row_array();		  
  }
  
  public function add($data)
  {
    $this->defaultdb->insert('classes', $data);
    return $this->defaultdb->insert_id();
  }

  public function update_class($id, $data){
    $this->db->where('id', $id)->update('classes', $data);
  }

  public function delete_a_class($id)
  {
    $this->db->where('id',$id);
    return $this->db->delete('classes');
  }

}

?>
