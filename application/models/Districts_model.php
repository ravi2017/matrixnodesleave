<?php
class districts_model extends CI_Model {

  public function __construct()
  {
    parent::__construct();
    $this->defaultdb = $this->load->database('default', TRUE);
  }

  public function get_all_district($state = 0, $limit = 0, $offset = 0)
  {
    if ($state > 0)
      $this->defaultdb->where('districts.state_id',$state);
      
    $this->defaultdb->join('states', 'states.id = districts.state_id');
    $this->defaultdb->select('states.name as state_name');
    $this->defaultdb->select('districts.*');
    
    $this->defaultdb->order_by("id", "desc");
    if ($limit != "")
      $query = $this->defaultdb->get('districts', $limit, $offset);
    else
      $query = $this->defaultdb->get('districts');
    return $query->result();
  }

  public function get_districts_by_name($state_id, $name)
  {
    $query = $this->defaultdb->get_where('districts',array("LOWER(name)"=>strtolower($name),"state_id"=>$state_id));
    return $query->result();
  }

  public function get_districts_from_dise($state_id, $state_dise, $dise)
  {
    $query = $this->defaultdb->query("select id from sv_districts where state_id = ".$state_id." and concat('".$state_dise."',dise_code) in (".$dise.")");
    return $query->result();
  }
  
  public function get_district_count()
  {
    $query = $this->defaultdb->count_all_results('districts');
    return $query;
  }
  
  public function get_state_districts($state, $district = 0, $limit = 0, $offset = 0)
  {
//    if ($district > 0)
//      $this->defaultdb->where('districts.id',$district);
//      
//    $this->defaultdb->order_by("name", "asc");
//    if ($limit != "")
//      $query = $this->defaultdb->get_where('districts', array('state_id' => $state), $limit, $offset);
//    else
//      $query = $this->defaultdb->get_where('districts', array('state_id' => $state));
    $this->defaultdb->order_by("name", "asc");
    $query = $this->defaultdb->get_where('districts', array('state_id' => $state));
    return $query->result();
  }

  public function insert_district_to_db($data)
  {
    $this->defaultdb->insert('districts', $data);
    return $this->defaultdb->insert_id();
  }

  public function getById($id)
  {
    $query = $this->defaultdb->get_where('districts',array('id'=>$id));
    return $query->row_array();		  
  }

  public function getByIds($ids)
  {
    $this->defaultdb->where_in('id',$ids);
    $query = $this->defaultdb->get('districts');
    return $query->result();		  
  }

  public function update_info($data,$id)
  {
    $this->defaultdb->where('districts.id',$id);
    return $this->defaultdb->update('districts', $data);
  }

  public function delete_a_district($id)
  {
    $this->defaultdb->where('districts.id',$id);
    return $this->defaultdb->delete('districts');
  }
  
  public function get_district($state_id, $limit = 5, $offset = 0)
  {
    $this->defaultdb->order_by("name", "asc");
    $this->defaultdb->select("name");
    $this->defaultdb->select("id");
    $this->defaultdb->select("dise_code");
    if ($limit != "")
      $query = $this->defaultdb->get_where('districts', array('state_id' => $state_id), $limit, $offset);
    else
      $query = $this->defaultdb->get_where('districts', array('state_id' => $state_id));
    return $query->result();
  }
  
  
  public function get_district_auto($q){
    $this->defaultdb->select('*');
    $this->defaultdb->like('dise_code', $q);
     $query = $this->defaultdb->get_where('districts');
    if($query->num_rows > 0){
      foreach ($query->result_array() as $row){
	   $row1['label'] = htmlentities(stripslashes($row['dise_code']." (".$row['name'].")"));
	   $row1['value'] = htmlentities(stripslashes($row['dise_code']." (".$row['name'].")"));
	   $row1['id'] = $row['id'];
	   $row1['district_id'] = $row['id'];
     $row1['state_id'] = $row['state_id'];
		 $row_set[] = $row1;
      }
      echo json_encode($row_set); //format the array into json data
    }
  }
  
   public function check_district_by_State($district_ids,$state_id){
	  
    $this->defaultdb->where('state_id',$state_id);
    $this->defaultdb->where_in('id',$district_ids);
	  $query = $this->defaultdb->get_where('districts');
	  return $query->result();
  }
  
   public function check_district_name_by_State($district_ids,$state_id=""){
	  
    $query = "select group_concat(name) as districts from ssc_districts where id in (".implode(",",$district_ids).")";
    if($state_id != '')
      $query .= " and state_id = ".$state_id." ";
    $query = $this->defaultdb->query($query);  
    $row = $query->result_array();
    return $row;
    
  }
}

?>
