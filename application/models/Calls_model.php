<?php

class Calls_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        error_log("----inside call model construct method ------",0);
        $this->defaultdb = $this->load->database('default', TRUE);
    }
    
    public function get_all($user_id, $start_date, $end_date) {
        $this->defaultdb->where("user_id", $user_id);
    $this->defaultdb->where("activity_date >= ",$start_date." 00:00:00");
    $this->defaultdb->where("activity_date <= ",$end_date." 23:59:59");
        $query = $this->defaultdb->get('calls');
        return $query->result();
    }

    public function add($data) {
        error_log("--inside call model add method---",0);
        $this->defaultdb->insert('calls', $data);
        return $this->defaultdb->insert_id();
    }

    public function getById($id) {
        $query = $this->defaultdb->get_where('calls', array('id' => $id));
        return $query->row_array();
    }

    public function get_district_training($district_id, $start_date, $end_date) {
        $this->defaultdb->where("district_id", $district_id);
        //$this->defaultdb->where("block_id",0);
        $this->defaultdb->where("activity_date >= ", $start_date);
        $this->defaultdb->where("activity_date <= ", $end_date);
        $query = $this->defaultdb->get('calls');
        return $query->result();
    }

    public function get_block_training($block_id, $start_date, $end_date) {
        $this->defaultdb->where("block_id", $block_id);
        //$this->defaultdb->where("block_id",0);
        $this->defaultdb->where("activity_date >= ", $start_date);
        $this->defaultdb->where("activity_date <= ", $end_date);
        $query = $this->defaultdb->get('calls');
        return $query->result();
    }
    
     public function get_cluster_training($cluster_id, $start_date, $end_date) {
        $this->defaultdb->where("cluster_id", $cluster_id);
        //$this->defaultdb->where("block_id",0);
        $this->defaultdb->where("activity_date >= ", $start_date);
        $this->defaultdb->where("activity_date <= ", $end_date);
        $query = $this->defaultdb->get('calls');
        return $query->result();
    }

    public function get_traning($district_id, $user_id, $start_date, $end_date) {
        $this->defaultdb->where("district_id", $district_id);
        $this->defaultdb->where("user_id", $user_id);
        //$this->defaultdb->where("block_id",0);
        $this->defaultdb->where("activity_date >= ", $start_date);
        $this->defaultdb->where("activity_date <= ", $end_date);
        $this->defaultdb->select('count(*) as total');
        $query = $this->defaultdb->get('calls');
        return $query->result();
    }

    public function get_user_traning($state_id, $district_id, $user_id, $start_date, $end_date) {
        $this->defaultdb->where("state_id", $state_id);
        $this->defaultdb->where("district_id", $district_id);
        $this->defaultdb->where("user_id", $user_id);
        //$this->defaultdb->where("block_id",0);
        $this->defaultdb->where("activity_date >= ", $start_date);
        $this->defaultdb->where("activity_date <= ", $end_date);
        $this->defaultdb->select('*');

        $query = $this->defaultdb->get('calls');
        return $query->result();
    }

    public function getActivityInfoForCall($user_id, $limit = 10) {
        $this->defaultdb->where("user_id", $user_id);
        $this->defaultdb->order_by("id", "desc");
        $query = $this->defaultdb->get('calls', $limit);

        return $query->result();
    }

    public function get_approval_activities($user_id, $start_date, $end_date)
    {
        $this->defaultdb->select("calls.*, contacts.firstname, contacts.lastname");
        $this->defaultdb->join("contacts", "contacts.id = calls.contact_person_id", "left");
        $this->defaultdb->where("calls.user_id",$user_id);
        $this->defaultdb->where("calls.activity_date >= ",$start_date." 00:00:00");
        $this->defaultdb->where("calls.activity_date <= ",$end_date." 23:59:59");
        $query = $this->defaultdb->get('calls');
        return $query->result();
    }

}

?>
