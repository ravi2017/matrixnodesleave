<?php
class Holidays_model extends CI_Model {

  public function __construct()
  {
    parent::__construct();
    error_log(" Holidays model construct ");
    $this->defaultdb = $this->load->database('default', TRUE);
  }

  public function get_all($user_id, $start_date, $end_date, $holiday_type = 'field_user')
  {
    $this->defaultdb->where("holiday_type",$holiday_type);
    $this->defaultdb->where("user_id",$user_id);
    $this->defaultdb->where("activity_date >= ",$start_date." 00:00:00");
    $this->defaultdb->where("activity_date <= ",$end_date." 23:59:59");
    $query = $this->defaultdb->get('holidays');
    return $query->result();
  }

  public function get_all_holidays_admin($start_date='')
  {
		$this->db->join('states', 'states.id = holidays.stateid','left');
		if($start_date != '')
			$this->db->where("activity_date >= ",$start_date." 00:00:00");	
    $this->db->select('states.name as statename');
    $this->db->select('holidays.*');
    //$this->db->order_by('activity_date', 'desc');
    //$this->db->select('GROUP_CONCAT(ssc_states.name) as statenames');
    //$this->db->group_by('holidays.activity_date');
    $query = $this->db->get('holidays');
    return $query->result();
  }

  public function get_all_holidays($class_id = 0, $subject_id = 0, $progress_chart = '', $base_line = '', $end_line = '')
  {
    $this->db->select('holiday_master.*');    
    $query = $this->db->get('holiday_master');
    return $query->result();
  }

  public function add($data)
  {
    $this->defaultdb->insert('holidays', $data);
    return $this->defaultdb->insert_id();
  }

  public function isHoliday($activity_date_only,$state_id,$user_id) {
  		//returning no hoiday because of bug fix
  		return false;
        //send only date without time example Y-d-m        
        error_log("Holidays model isHoliday() function ");
        $Lactivity_date_only=date("Y-m-d", strtotime($activity_date_only));
        $holiday_Procedure = "CALL sp_FindHoliday('$Lactivity_date_only','$state_id','$user_id', @outputparam)";
        $mHolidayResult = $this->defaultdb->query($holiday_Procedure);
        $mHolidayResultval = $mHolidayResult->result();
        $holiday = $mHolidayResultval[0]->Total_Holiday;
        mysqli_next_result($this->defaultdb->conn_id);
        //$holiday = $overresult->'Total_Holiday';
        //any value greater than 0 means there is a holiday on provided date
        return ($holiday > 0);
    }

  
  public function delete_a_holidays($id)
  {
    $this->db->where('id',$id);
    return $this->db->delete('holidays');
  }
  
  function get_state_holidays($login_id, $state_id, $_fromDate, $_toDate, $holiday_type=''){
    $query = "SELECT COUNT(DISTINCT activity_date) as STATE_HOLIDAY FROM ssc_holidays ssch WHERE `activity_date` BETWEEN '".$_fromDate." 00:00:00' AND '".$_toDate." 23:59:59' AND (stateid = '$state_id' OR NATIONAL = 1 OR user_id = '$login_id')";
    if($holiday_type != '')
		$query .= " and ssch.holiday_type = '$holiday_type' ";
    $state_holiday = $this->db->query($query); 
    $row = $state_holiday->row_array();
    return $row['STATE_HOLIDAY'];
  }

  function get_state_holidays_list($state_id, $_fromDate, $_toDate, $holiday_type=''){
    $query = "SELECT activity_date,name FROM ssc_holidays ssch WHERE `activity_date` BETWEEN '".$_fromDate." 00:00:00' AND '".$_toDate." 23:59:59' AND (stateid = ".$state_id." OR NATIONAL = 1) ";
    if($holiday_type != '')
			$query .= " and ssch.holiday_type = '$holiday_type' ";
		$query .= " GROUP BY activity_date ";
    $state_holiday = $this->db->query($query); 
    //echo "<br>".$this->db->last_query();
    return $state_holiday->result();
  }

  function get_2_3_saturday_list($_fromDate, $_toDate, $user_group=''){
    $query = "SELECT activity_date,type FROM ssc_offdays ssch WHERE `activity_date` BETWEEN '".$_fromDate." 00:00:00' AND '".$_toDate." 23:59:59' ";
    if($user_group != '')
			$query .= " and ssch.user_group = '$user_group' ";
    $query .= "  GROUP BY activity_date";
    $state_holiday = $this->db->query($query); 
    return $state_holiday->result();
  }

  function get_2_3_saturday($_fromDate, $_toDate, $user_group=''){
    $query = "SELECT COUNT(DISTINCT activity_date) as SUN_2_3_SAT FROM ssc_offdays ssch WHERE `activity_date` BETWEEN '".$_fromDate." 00:00:00' AND '".$_toDate." 23:59:59'";
    if($user_group != '')
			$query .= " and ssch.user_group = '$user_group' ";
    $sun_2_3_sat = $this->db->query($query); 
    $row = $sun_2_3_sat->row_array();
    return $row['SUN_2_3_SAT'];
  }
  
  
  function get_all_holidays_list($_fromDate, $_toDate){
    $query = "SELECT activity_date, name, holiday_type, stateid FROM ssc_holidays ssch WHERE `activity_date` BETWEEN '".$_fromDate." 00:00:00' AND '".$_toDate." 23:59:59' ";
		$query .= " GROUP BY activity_date ";
    $state_holiday = $this->db->query($query); 
    //echo "<br>".$this->db->last_query();
    return $state_holiday->result();
  }

  function get_all_sat_sun_list($_fromDate, $_toDate){
    $query = "SELECT activity_date,type, user_group FROM ssc_offdays ssch WHERE `activity_date` BETWEEN '".$_fromDate." 00:00:00' AND '".$_toDate." 23:59:59' ";
    $query .= "  GROUP BY activity_date";
    $state_holiday = $this->db->query($query); 
    return $state_holiday->result();
  }
  
  
  /*public function get_holidays($q)
  {
    $this->db->select('id,name');
    $this->db->like('name', $q);
    $query = $this->db->get_where('holiday_master');
    if($query->num_rows > 0){
      foreach ($query->result_array() as $row){
     $row1['label'] = htmlentities(stripslashes($row['name']));
     $row1['value'] = htmlentities(stripslashes($row['name']));
     $row1['id'] = $row['id'];
     $row_set[] = $row1;
      }
      echo json_encode($row_set); //format the array into json data
    }
  }
  
   public function get_all_holidays($class_id = 0, $subject_id = 0, $progress_chart = '', $base_line = '', $end_line = '')
  {
    $this->db->select('holiday_master.*');    
    $query = $this->db->get('holiday_master');
    return $query->result();
  }
  
	public function insert_holidays_to_db($data)
  {
    $this->db->insert('holiday_master', $data);
    return $this->db->insert_id();
  }

  public function getById($id)
  {
    $query = $this->db->select('holiday_master.*, states_holidays.state_id')->from('holiday_master')->join('states_holidays', 'states_holidays.holiday_id = holiday_master.id')->where('holiday_id', $id)->get()->result_array();
    return $query;      
  }

  public function getBySlug($slug)
  {
    $query = $this->db->get_where('holiday_master',array('slug'=>$slug));
    return $query->row_array();     
  }

  public function getByEmail($slug)
  {
    $query = $this->db->get_where('holiday_master',array('email'=>$slug));
    return $query->row_array();     
  }

  public function getCode($id)
  {
    $this -> db -> select('site_id');
    $query = $this->db->get_where('admin_sites',array('admin_id'=>$id));
    return $query->result();    
  }

  public function update_holiday($id, $data){
    $this->db->where('id', $id)->update('holiday_master', $data);
  }

  public function delete_state_holidays($holiday_id){
    $this->db->where('holiday_id', $holiday_id)->delete('states_holidays');
  }

  public function update_info($data,$id)
  {
    $this->db->where('holiday_master.id',$id);
    return $this->db->update('holiday_master', $data);
  }

  public function update_info_email($data,$id)
  {
    $this->db->where('holiday_master.email',$id);
    return $this->db->update('holiday_master', $data);
  }

  public function delete_a_holidays_sites($id)
  {
    $this->db->where('admin_sites.admin_id',$id);
    return $this->db->delete('admin_sites');
  }
	*/
}

?>
