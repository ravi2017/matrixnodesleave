<?php
class Leaves_model extends CI_Model {

  public function __construct()
  {
    parent::__construct();
    $this->defaultdb = $this->load->database('default', TRUE);
  }

  public function get_all($user_id, $start_date='', $end_date='', $order_by='')
  {
    $this->defaultdb->where("user_id",$user_id);
    if($start_date != '')
			$this->defaultdb->where("leave_from_date >= ",$start_date." 00:00:00");
		if($end_date != '')	
			$this->defaultdb->where("leave_end_date <= ",$end_date." 23:59:59");
    $this->defaultdb->where("status = ","approved");
		if($order_by != '')
				$this->defaultdb->order_by($order_by);
    $query = $this->defaultdb->get('leaves');
    //echo $this->defaultdb->last_query();
    return $query->result();
  }
  
  public function get_all_leave($user_id, $start_date, $end_date)
  {
    $start_date = $start_date." 00:00:00";
    $end_date = $end_date." 23:59:59";
    
    $select = "SELECT * FROM `ssc_leaves` WHERE `user_id` = '$user_id' AND ((`leave_from_date` BETWEEN '$start_date' AND '$end_date') OR (`leave_end_date` BETWEEN '$start_date' AND '$end_date')) AND `status` = 'approved'";
    $query = $this->defaultdb->query($select);
    return $query->result();
  }

  public function get_all_applied($user_id, $start_date, $end_date)
  {
    $this->defaultdb->where("user_id",$user_id);
    $this->defaultdb->where("leave_from_date >= ",$start_date." 00:00:00");
    $this->defaultdb->where("leave_end_date <= ",$end_date." 23:59:59");
    $query = $this->defaultdb->get('leaves');
    return $query->result();
  }

  public function get_approval_activities($user_id, $start_date, $end_date, $status = '')
  {
    $this->defaultdb->where("user_id",$user_id);
    $this->defaultdb->where("leave_from_date >= ",$start_date." 00:00:00");
    if ($end_date != "")
      $this->defaultdb->where("leave_end_date <= ",$end_date." 23:59:59");
    else
      $this->defaultdb->where("leave_from_date <= ",date("Y-m-t",strtotime($start_date))." 00:00:00");
    if($status != 'all')
		$this->defaultdb->where("status != ","approved");
    $query = $this->defaultdb->get('leaves');
    return $query->result();
  }

  public function add($data)
  {
    $this->defaultdb->insert('leaves', $data);
    return $this->defaultdb->insert_id();
  }

  public function getById($id)
  {
    $query = $this->defaultdb->get_where('leaves',array('id'=>$id));
    return $query->row_array();		  
  }
		public function get_district_training($district_id, $start_date, $end_date)
  {
    $this->defaultdb->where("district_id",$district_id);
    //$this->defaultdb->where("block_id",0);
    $this->defaultdb->where("activity_date >= ",$start_date);
    $this->defaultdb->where("activity_date <= ",$end_date);
    $query = $this->defaultdb->get('leaves');
    return $query->result();
  }
	public function get_block_training($block_id, $start_date, $end_date)
  {
    $this->defaultdb->where("block_id",$block_id);
    //$this->defaultdb->where("block_id",0);
    $this->defaultdb->where("activity_date >= ",$start_date);
    $this->defaultdb->where("activity_date <= ",$end_date);
    $query = $this->defaultdb->get('leaves');
    return $query->result();
  }
	public function get_traning($district_id,$user_id, $start_date, $end_date)
  {
    $this->defaultdb->where("district_id",$district_id);
    $this->defaultdb->where("user_id",$user_id);
    //$this->defaultdb->where("block_id",0);
    $this->defaultdb->where("activity_date >= ",$start_date);
    $this->defaultdb->where("activity_date <= ",$end_date);
		$this->defaultdb->select('count(*) as total');
    $query = $this->defaultdb->get('leaves');
    return $query->result();
  }
	public function get_user_traning($state_id,$district_id,$user_id, $start_date, $end_date)
  {
    $this->defaultdb->where("state_id",$state_id);
    $this->defaultdb->where("district_id",$district_id);
    $this->defaultdb->where("user_id",$user_id);
    //$this->defaultdb->where("block_id",0);
    $this->defaultdb->where("activity_date >= ",$start_date);
    $this->defaultdb->where("activity_date <= ",$end_date);
    $this->defaultdb->select('*');
		
    $query = $this->defaultdb->get('leaves');
    return $query->result();
  }

  public function get_all_group_leave()
  {      
    $this->defaultdb->order_by("id", "desc");
    //$this->defaultdb->limit(1);
    $query = $this->defaultdb->get('usergroup_leaves');
    //echo $this->defaultdb->last_query();
    return $query->result();
  }
  
  public function groupLeaveById($id)
  {
    $query = $this->defaultdb->get_where('usergroup_leaves',array('id'=>$id));
    return $query->row_array();		  
  }
  
  public function get_by_group($group, $limit='')
  {
    $this->defaultdb->where('user_group',$group);
    $this->defaultdb->order_by('id','desc');
    if($limit != '')
		$this->defaultdb->limit($limit);
    $query = $this->defaultdb->get('usergroup_leaves');
    return $query->result();
  }
 
  public function insert_leave_group($data)
  {
    return $this->defaultdb->insert('usergroup_leaves', $data);
  }
		
  public function update_leave_group($data,$id)
  {
    $this->defaultdb->where('id',$id);
    return $this->defaultdb->update('usergroup_leaves', $data);
  }

  public function delete_a_group_leave($id)
  {
    $this->defaultdb->where('id',$id);
    return $this->defaultdb->delete('usergroup_leaves');
  }	
  
  public function get_all_state_leaves()
  {      
    $this->defaultdb->order_by("id", "desc");
    //$this->defaultdb->limit(1);
    $query = $this->defaultdb->get('state_leave_types');
    //echo $this->defaultdb->last_query();
    return $query->result();
  }
  
  public function stateLeaveById($id)
  {
    $query = $this->defaultdb->get_where('state_leave_types',array('id'=>$id));
    return $query->row_array();		  
  }
  
  public function get_by_state_leave($state_code, $leave_type, $limit='')
  {
    $this->defaultdb->where('state_code',$state_code);
    $this->defaultdb->where('leave_type',$leave_type);
    $this->defaultdb->order_by('id','desc');
    if($limit != '')
		$this->defaultdb->limit($limit);
    $query = $this->defaultdb->get('state_leave_types');
    return $query->result();
  }
 
  public function insert_state_leave($data)
  {
    return $this->defaultdb->insert('state_leave_types', $data);
  }
		
  public function update_state_leave($data,$id)
  {
    $this->defaultdb->where('id',$id);
    return $this->defaultdb->update('state_leave_types', $data);
  }

  public function delete_a_state_leave($id)
  {
    $this->defaultdb->where('id',$id);
    return $this->defaultdb->delete('state_leave_types');
  }
  
  function get_leave_credit_details($spark_id='', $leave_type='', $earned_type='', $earned_id='', $earned_date='')
  { 
	if($spark_id != '')       
		$this->defaultdb->where("spark_id", $spark_id);
	if($leave_type != '')       
		$this->defaultdb->where("leave_type", $leave_type);
	if($earned_type != '')       
		$this->defaultdb->where("earned_type", $earned_type);
	if($earned_id != '')       
		$this->defaultdb->where("earned_id", $earned_id);
	if($earned_date != '')       
		$this->defaultdb->where("earned_date", $earned_date);
    $query = $this->defaultdb->get("leave_credit_details");
    //echo $this->defaultdb->last_query();
    return $query->result();
  } 	
  
  public function insert_leave_credit_details($data)
  {
    return $this->defaultdb->insert('leave_credit_details', $data);
  }
  

  function get_spark_group_leave($spark_group)
  {
	 $current_date = date('Y-m-d'); 
	 $sql = "SELECT leave_count from ssc_usergroup_leaves where user_group = '$spark_group' and start_date <= '$current_date' order by id desc"; 
	 $query = $this->defaultdb->query($sql);
     return $query->row_array();
  }
  
  function get_leave_credits($spark_id='', $leave_type='')
  { 
		if($spark_id != '')       
			$this->defaultdb->where("spark_id", $spark_id);
		if($leave_type != '')       
			$this->defaultdb->where("leave_type", $leave_type);
		$query = $this->defaultdb->get("ssc_leave_credits");
		//echo $this->defaultdb->last_query();
    return $query->result();
  } 	
  
   public function insert_leave_credit($data)
  {
    return $this->defaultdb->insert('leave_credits', $data);
  }
  
  
  public function update_leave_credit($data,$id)
  {
    $this->defaultdb->where('id',$id);
    return $this->defaultdb->update('leave_credits', $data);
  }

  public function get_all_carry_forward_policies()
  {      
    $this->defaultdb->order_by("id", "desc");
    //$this->defaultdb->limit(1);
    $query = $this->defaultdb->get('carry_forward_policies');
    //echo $this->defaultdb->last_query();
    return $query->result();
  }
  
  public function carryForwardPolicyById($id)
  {
    $query = $this->defaultdb->get_where('carry_forward_policies',array('id'=>$id));
    return $query->row_array();		  
  }
   
  public function insert_carry_forward_policy($data)
  {
    return $this->defaultdb->insert('carry_forward_policies', $data);
  }
		
  public function update_carry_forward_policy($data,$id)
  {
    $this->defaultdb->where('id',$id);
    return $this->defaultdb->update('carry_forward_policies', $data);
  }

  public function delete_carry_forward_policy($id)
  {
    $this->defaultdb->where('id',$id);
    return $this->defaultdb->delete('carry_forward_policies');
  }	
     		  
  function get_credit_leave_details($id)
  {
		$sql = "SELECT sld.* FROM `ssc_leave_credit_details` sld, ssc_leave_credits slc WHERE sld.spark_id = slc.spark_id AND sld.leave_type = slc.leave_type AND slc.id=$id";
		$query = $this->defaultdb->query($sql);
    return $query->result();
	}
	
	function get_school_holidays($state_id, $all_dates)
	{
		$this->defaultdb->where("state_id", $state_id);
		$this->defaultdb->where_in("holiday_date", $all_dates);
		$this->defaultdb->group_by("holiday_date");
		$query = $this->defaultdb->get("school_holiday_dates");
		//echo $this->defaultdb->last_query();
    return $query->result();
	}
	
	public function delete_spark_leave($id)
  {
    $this->defaultdb->where('id',$id);
    return $this->defaultdb->delete('leaves');
  }	
  
   public function get_all_leave_list($user_id='', $start_date, $end_date)
  {
    $start_date = $start_date." 00:00:00";
    $end_date = $end_date." 23:59:59";
    
    $select = "SELECT ssp.login_id, sl.* FROM `ssc_leaves` sl, ssc_sparks ssp WHERE ssp.id = sl.user_id AND ((`leave_from_date` BETWEEN '$start_date' AND '$end_date') OR (`leave_end_date` BETWEEN '$start_date' AND '$end_date')) AND sl.status = 'approved'";
    if($user_id != '')
			$select .= " AND `user_id` = '$user_id' ";
		$query = $this->defaultdb->query($select);
    return $query->result();
  }

	
}

?>
