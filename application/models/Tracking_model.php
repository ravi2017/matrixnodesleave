<?php
//error_reporting(0);
class Tracking_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        //Enable profiler
        $this->output->enable_profiler(false);
        $this->defaultdb = $this->load->database('default', TRUE);
    }

    public function getAll($spark_id = 0, $date = '') {
      if ($spark_id > 0)
      {
        $this->defaultdb->where('spark_id',$spark_id);
      }
      if ($date != "")
      {
        $day = Date("d",strtotime($date));
        $month = Date("m",strtotime($date));
        $year = Date("Y",strtotime($date));
        $this->defaultdb->where('day(tracking_time)',$day);
        $this->defaultdb->where('month(tracking_time)',$month);
        $this->defaultdb->where('year(tracking_time)',$year);
      }
      $this->defaultdb->order_by('tracking_time asc');
      $query = $this->defaultdb->get('ssc_trackings');
      return $query->result();
    }

    public function add($data) {
        $this->defaultdb->insert('ssc_trackings', $data);
        return $this->defaultdb->insert_id();
    }

    public function update_attendance_out($data,$id) {
        $this->defaultdb->where('id',$id);
        $result = $this->defaultdb->update('ssc_trackings', $data);
        //echo $this->defaultdb->last_query(); 
        return $result;
    }

    public function getById($id) {
        $query = $this->defaultdb->get_where('ssc_trackings', array('id' => $id));
        return $query->row_array();
    }    

    public function getAttendanceStatus($spark_id,$date)
    {
      $day = Date("d",strtotime($date));
      $month = Date("m",strtotime($date));
      $year = Date("Y",strtotime($date));
      $this->defaultdb->where('spark_id',$spark_id);
      $this->defaultdb->where_in('activity_type',array('attendance-in','attendance-out'));
      $this->defaultdb->where('day(tracking_time)',$day);
      $this->defaultdb->where('month(tracking_time)',$month);
      $this->defaultdb->where('year(tracking_time)',$year);
      $this->defaultdb->order_by('tracking_time desc');
      $this->defaultdb->limit(1);
        
      $query = $this->defaultdb->get_where('ssc_trackings');
      return $query->row_array();
    }
    
    public function getAttendance($spark_id,$date,$status = 'in',$order = 'tracking_time desc',$limit = '') {
        $day = Date("d",strtotime($date));
        $month = Date("m",strtotime($date));
        $year = Date("Y",strtotime($date));
        $this->defaultdb->where('spark_id',$spark_id);
        $this->defaultdb->where('activity_type','attendance-'.$status);
        $this->defaultdb->where('day(tracking_time)',$day);
        $this->defaultdb->where('month(tracking_time)',$month);
        $this->defaultdb->where('year(tracking_time)',$year);
        $this->defaultdb->order_by($order);
        
        if ($limit != "")
          $this->defaultdb->limit($limit);
          
        $query = $this->defaultdb->get_where('ssc_trackings');
        return $query->result();
    }    

    public function checkAttendance($spark_id,$date,$status = 'in') {
        $day = Date("d",strtotime($date));
        $month = Date("m",strtotime($date));
        $year = Date("Y",strtotime($date));
        $this->defaultdb->where('spark_id',$spark_id);
        $this->defaultdb->where('activity_type','attendance-'.$status);
        $this->defaultdb->where('day(tracking_time)',$day);
        $this->defaultdb->where('month(tracking_time)',$month);
        $this->defaultdb->where('year(tracking_time)',$year);
        $query = $this->defaultdb->get_where('ssc_trackings');
        return $query->row_array();
    }    
    
    public function checkAttendanceTime($spark_id, $date, $status = 'in') {
        $this->defaultdb->where('spark_id',$spark_id);
        $this->defaultdb->where('activity_type','attendance-'.$status);
        $this->defaultdb->where('tracking_time',$date);
        $query = $this->defaultdb->get_where('ssc_trackings');
        return $query->row_array();
    }    
    
    public function checkSystemAttendanceOut($spark_id, $date) {
        $this->defaultdb->where('spark_id',$spark_id);
        $this->defaultdb->where('activity_type','attendance-out');
        $this->defaultdb->where('created_by','system');
        $this->defaultdb->where('Date(tracking_time)',$date);
        $query = $this->defaultdb->get_where('ssc_trackings');
        return $query->row_array();
    }    
    
    public function deleteTrackings($id) {
        $this->defaultdb->where('id', $id);
        $this->defaultdb->delete('ssc_trackings');
    }
    
    public function update_tracking_distance_flag($spark_id, $date, $data) {
      $this->defaultdb->where('Date(tracking_time)',$date);
      $this->defaultdb->where('spark_id', $spark_id);
      $this->defaultdb->update('ssc_trackings', $data);
    }
    
    
    public function add_claims($data) {
        $this->defaultdb->insert('ssc_travel_claims', $data);
        return $this->defaultdb->insert_id();
    }
    
    public function get_claims($sourec_id='', $destination_id='', $claim_id='') {
        
        if($sourec_id != '')
          $this->defaultdb->where('track_source_id', $sourec_id);
        if($destination_id != '')  
          $this->defaultdb->where('track_destination_id', $destination_id);
        if($claim_id != '')  
          $this->defaultdb->where('id', $claim_id);  
        $query = $this->defaultdb->get_where('ssc_travel_claims');
        return $query->row_array();
    }
    
    public function get_track_claims_list($spark_id, $tracking_date_start, $tracking_date_end = '') {
      
      $activity_string = "'training-in', 'training', 'review_meeting', 'meeting-in', 'schoolvisit-in', 'schoolvisit', 'attendance-out'";
      
      $query = "SELECT ssc_travel_claims.id as record_id, ssc_travel_claims.*, ssc_trackings.* FROM ssc_trackings, ssc_travel_claims WHERE ssc_trackings.id = ssc_travel_claims.track_destination_id and ssc_trackings.tracking_time BETWEEN '".$tracking_date_start." 00:00:00' AND '".$tracking_date_end." 23:59:59' AND spark_id = '$spark_id' and activity_type in (".$activity_string.") order by ssc_trackings.tracking_time, ssc_trackings.id ASC ";
      $claims = $this->db->query($query); 
      $result = $claims->result();
      return $result;
    }
    
    public function get_track_attendance_list($spark_ids=array(), $tracking_date_start, $tracking_date_end = '', $activity_type = array()) {
      if ($tracking_date_end == "")
      {
        $query  = "SELECT ssc_sparks.*, ssc_trackings.tracking_time, ssc_trackings.activity_type, ssc_trackings.tracking_location, ssc_trackings.lat_long_location, ssc_trackings.created_by FROM ssc_sparks JOIN ssc_trackings ON ssc_sparks.id = ssc_trackings.spark_id ";
        $query .= " WHERE activity_type in ('".implode("','",$activity_type)."') ";
        if(!empty($spark_ids))
					$query .= " AND ssc_sparks.id in (".implode(',', $spark_ids).") ";
        $query .= " AND DATE(tracking_time) = '$tracking_date_start' ) ";
        $query .= " order by ssc_trackings.tracking_time, ssc_trackings.spark_id ";
        $qry = $this->db->query($query); 
        $result = $qry->result();
      }
      else
      {
        $query  = "SELECT ssc_sparks.*, ssc_trackings.tracking_time, ssc_trackings.activity_type, ssc_trackings.tracking_location, ssc_trackings.lat_long_location, ssc_trackings.created_by FROM ssc_sparks JOIN ssc_trackings ON ssc_sparks.id = ssc_trackings.spark_id ";
        $query .= " WHERE activity_type in ('".implode("','",$activity_type)."') ";
        if(!empty($spark_ids))
					$query .= " AND ssc_sparks.id in (".implode(',', $spark_ids).") ";
        $query .= " AND ssc_trackings.tracking_time BETWEEN '".$tracking_date_start." 00:00:00' AND '".$tracking_date_end." 23:59:59'  ";
        $query .= " order by ssc_trackings.tracking_time, ssc_trackings.spark_id ";
        $qry = $this->db->query($query); 
        $result = $qry->result();
      }
      return $result;
    }
    
    public function update_claims($id, $data){
      $this->db->where('id', $id)->update('ssc_travel_claims', $data);
      //echo"<br>".$this->db->last_query();
    }

    
    public function get_activity_list($spark_id = 0, $type = '') {
      $this->defaultdb->where('spark_id', $spark_id);
      $this->defaultdb->like('activity_type', $type);
      $this->defaultdb->order_by('tracking_time desc')->limit('24');
      if($type == 'attendance-in'){
        $this->defaultdb->select('MIN(tracking_time), ssc_trackings.* ')->group_by('Date(tracking_time)');
      }
      $query = $this->defaultdb->get('ssc_trackings');
      return $query->result();
    }
    
    public function get_activity_tracking($activity_type, $activity_id)
    {
      $this->defaultdb->where('activity_id', $activity_id);
      $this->defaultdb->where('activity_type', $activity_type);
      $this->defaultdb->order_by('tracking_time desc');
      $query = $this->defaultdb->get('ssc_trackings');
      return $query->result();
    }
    
    public function get_track_training_list($spark_ids, $tracking_date_start, $tracking_date_end = '', $activity_type = array()) {

      $query  = "SELECT ssc_sparks.*, ssc_trackings.tracking_time, ssc_trackings.activity_type, ssc_trackings.activity_id, ssc_trainings.training_duration  FROM ssc_sparks JOIN ssc_trackings ON ssc_sparks.id = ssc_trackings.spark_id JOIN ssc_trainings ON ssc_trainings.id = ssc_trackings.activity_id ";
      $query .= " WHERE ssc_sparks.id in (".implode(',', $spark_ids).")  AND activity_type in ('".implode("','",$activity_type)."') ";
      $query .= " AND ssc_trackings.tracking_time BETWEEN '".$tracking_date_start." 00:00:00' AND '".$tracking_date_end." 23:59:59'  ";
      $query .= " order by ssc_trackings.spark_id, ssc_trackings.tracking_time ";
      $qry = $this->db->query($query); 
      $result = $qry->result();
      return $result;
    }
   
   public function get_distance($activity_table, $spark_ids=array(), $start_date='', $end_date='')
   {
     $query  = "select ssp.login_id, ssp.name, Date(sst.activity_date) as ActivityDate, sst.source_address, sst.destination_address, sst.distance";
     $query .= " from $activity_table sst JOIN ssc_sparks ssp ON sst.user_id = ssp.id ";
     $query .= " where distance > ".MIN_DISTANCE;
     if(!empty($spark_ids)){
        $query .= " and user_id in(".implode(',', $spark_ids).") ";
     }
     if(!empty($start_date) && !empty($end_date)){
       $query .= " AND sst.activity_date BETWEEN '".$start_date." 00:00:00' AND '".$end_date." 23:59:59'  ";
     }
     $query .= " order by user_id, activity_date";
     $qry = $this->db->query($query); 
     $result = $qry->result();
     return $result;
   }
   
   /*public function get_track_claims_exceed_list($spark_id, $tracking_date_start, $tracking_date_end = '') {
      
      $activity_string = "'training-in', 'training', 'review_meeting', 'meeting-in', 'schoolvisit-in', 'schoolvisit', 'attendance-out'";
      
      $query = "SELECT ssc_travel_claims.id as record_id, ssc_travel_claims.*, ssc_trackings.* FROM ssc_trackings, ssc_travel_claims WHERE ssc_trackings.id = ssc_travel_claims.track_destination_id and ssc_trackings.tracking_time BETWEEN '".$tracking_date_start." 00:00:00' AND '".$tracking_date_end." 23:59:59' AND spark_id = '$spark_id' and activity_type in (".$activity_string.") order by ssc_trackings.tracking_time, ssc_trackings.id ASC ";
      $claims = $this->db->query($query); 
      $result = $claims->result();
      return $result;
    }*/
    
    public function get_track_exceed_claims_list($spark_ids, $tracking_date_start, $tracking_date_end = '', $travel_mode_id='', $claim_type='') {
      
      $activity_string = "'training-in', 'training', 'review_meeting', 'meeting-in', 'schoolvisit-in', 'schoolvisit', 'attendance-out'";
      
      $sparkids = implode(',', $spark_ids);
      
      $query = "SELECT ssc_travel_claims.id as record_id, ssc_travel_claims.updated_by as process_by , ssc_travel_claims.*, ssc_trackings.* FROM ssc_trackings, ssc_travel_claims WHERE ssc_trackings.id = ssc_travel_claims.track_destination_id and ssc_trackings.tracking_time BETWEEN '".$tracking_date_start." 00:00:00' AND '".$tracking_date_end." 23:59:59' "; 
      if($travel_mode_id != ''){
				$query .= " AND ssc_travel_claims.travel_mode_id = '$travel_mode_id' ";
			}
			if($claim_type == 'processed'){
				$query .= " AND ssc_travel_claims.is_claimed = 1 ";
			}
			if($claim_type == 'unprocessed'){
				$query .= " AND ssc_travel_claims.is_claimed != 1 ";
			}
			if($claim_type == 'rejected'){
				$query .= " AND ssc_travel_claims.claim_discard = 1 ";
			}
      $query .= " AND spark_id in ($sparkids) and activity_type in (".$activity_string.") order by spark_id, ssc_trackings.tracking_time, ssc_trackings.id ASC ";
      $claims = $this->db->query($query); 
      $result = $claims->result();
      return $result;
    }
  
  public function get_track_tally_list($spark_ids=array(), $process_date) {
      
    $activity_string = "'training-in', 'training', 'review_meeting', 'meeting-in', 'schoolvisit-in', 'schoolvisit', 'attendance-out'";
    
    $sparkids = implode(',', $spark_ids);
    
    $query = "SELECT ssc_travel_claims.id as record_id, ssc_travel_claims.*, ssc_trackings.* FROM ssc_trackings, ssc_travel_claims WHERE ssc_trackings.id = ssc_travel_claims.track_destination_id "; 
    $query .= " AND ssc_travel_claims.process_date = '$process_date' ";
    //$query .= " and ssc_trackings.tracking_time BETWEEN '".$tracking_date_start." 00:00:00' AND '".$tracking_date_end." 23:59:59' ";
    $query .= " AND spark_id in($sparkids) and activity_type in (".$activity_string.") and (is_claimed = 1 OR claim_discard = 1) and travel_cost > 0 order by ssc_trackings.spark_id, ssc_trackings.tracking_time, ssc_trackings.id ASC ";
    $claims = $this->db->query($query); 
    $result = $claims->result();
    return $result;
  }
  
  public function get_claim_unprocess_list($spark_ids=array(), $tracking_date_start, $tracking_date_end = '') {
      
    $activity_string = "'training-in', 'training', 'review_meeting', 'meeting-in', 'schoolvisit-in', 'schoolvisit', 'attendance-out'";
    
    $sparkids = implode(',', $spark_ids);
    
    $query = "SELECT ssc_travel_claims.id as record_id, ssc_travel_claims.*, ssc_trackings.* FROM ssc_trackings, ssc_travel_claims WHERE ssc_trackings.id = ssc_travel_claims.track_destination_id and ssc_trackings.tracking_time BETWEEN '".$tracking_date_start." 00:00:00' AND '".$tracking_date_end." 23:59:59' AND spark_id in($sparkids) and activity_type in (".$activity_string.") and is_claimed = 0 and 
    claim_discard = 0 order by ssc_trackings.spark_id, ssc_trackings.tracking_time, ssc_trackings.id ASC ";
    $claims = $this->db->query($query); 
    $result = $claims->result();
    return $result;
  }
              
              
  function get_claim_data_no_travel_cost()           
  {
		$current_date = date('Y-m-d H:i:s');
		$sql = "SELECT ssc_travel_claims.id as record_id, ssc_travel_claims.*, ssc_trackings.* FROM ssc_trackings, ssc_travel_claims WHERE ssc_trackings.id = ssc_travel_claims.track_destination_id AND ssc_trackings.tracking_time BETWEEN '2019-11-01 00:00:00' AND '$current_date' and activity_type in ('training-in', 'training', 'review_meeting', 'meeting-in', 'schoolvisit-in', 'schoolvisit', 'attendance-out') AND is_claimed = 0 AND activity_id IS NOT NULL and travel_cost IS NULL order by ssc_trackings.spark_id, ssc_trackings.tracking_time, ssc_trackings.id ASC";
		$claims = $this->db->query($sql); 
    $result = $claims->result();
    return $result;
	}
	
	function check_month_expense($spark_id, $month_yr)
	{
		$sql = "select SUM(date_expense) as total_expense from ssc_spark_travel_claims where spark_id = '$spark_id' and tracking_month = '$month_yr' ";
		$claims = $this->db->query($sql); 
    $result = $claims->row_array();
    return $result;
	}
      
	function check_date_expense($spark_id, $tracking_date)
	{
		$sql = "select * from ssc_spark_travel_claims where spark_id = '$spark_id' and tracking_date = '$tracking_date' ";
		$claims = $this->db->query($sql); 
    $result = $claims->row_array();
    return $result;
	}
	
	public function update_date_expense($data,$id) {
			$this->defaultdb->where('id',$id);
			$result = $this->defaultdb->update('spark_travel_claims', $data);
			//echo $this->defaultdb->last_query(); 
			return $result;
	}
      
	public function insert_date_expense($data) {
			$this->defaultdb->insert('spark_travel_claims', $data);
      return $this->defaultdb->insert_id();
	}
	
	  public function get_ld_exceed_claims_list($spark_ids, $date_start, $date_end = '', $travel_mode_id='', $claim_status='') {
      
      if(!empty($spark_ids))
				$this->defaultdb->where_in("user_id",$spark_ids);
			if($travel_mode_id != '')
				$this->defaultdb->where("travel_mode_id", $travel_mode_id);
			if($claim_status == 'processed'){
				$this->defaultdb->where('claim_status', 'processed');
			}
			if($claim_status == 'unprocessed'){
				$this->defaultdb->where('claim_status !=', 'processed');
				$this->defaultdb->where('claim_status !=', 'rejected');
			}
			if($claim_status == 'rejected'){
				$this->defaultdb->where('claim_status', 'rejected');
			}  
			if($claim_status == 'referback'){
				$this->defaultdb->where('claim_status', 'referback');
			}  
			$this->defaultdb->where("activity_date >= ",$date_start." 00:00:00");
			$this->defaultdb->where("activity_date <= ",$date_end." 23:59:59");
			$this->defaultdb->order_by("activity_date", "asc");
			$query = $this->defaultdb->get('other_activities');
			$result = $query->result();
      return $result;
    }
    
    public function get_ld_unprocessed_list($spark_ids, $date_start, $date_end = '') {
      
      if(!empty($spark_ids))
				$this->defaultdb->where_in("user_id",$spark_ids);
			$this->defaultdb->where('claim_status', 'approved');
			$this->defaultdb->where("activity_date >= ",$date_start." 00:00:00");
			$this->defaultdb->where("activity_date <= ",$date_end." 23:59:59");
			$this->defaultdb->order_by("activity_date", "asc");
			$query = $this->defaultdb->get('other_activities');
			$result = $query->result();
      return $result;
    }
  
    public function get_track_tally_ld_list($spark_ids=array(), $process_date) {
      
      $claim_status = array('processed', 'rejected');
      
      if(!empty($spark_ids))
				$this->defaultdb->where_in("user_id",$spark_ids);
			$this->defaultdb->where("process_date",$process_date);
			$this->defaultdb->where_in('claim_status', $claim_status);
			$this->defaultdb->where('travel_cost > 0');
			$this->defaultdb->order_by("activity_date", "asc");
			$query = $this->defaultdb->get('other_activities');
			$result = $query->result();
      return $result;
    }
		
		 public function get_other_exceed_claims_list($spark_ids, $start_date='', $end_date = '', $claim_status='') {
      
      $sparkids = implode(', ', $spark_ids);
      
			$select = "SELECT * FROM `ssc_other_claims` WHERE 
			`spark_id` IN ($sparkids) AND ((`from_date` BETWEEN '$start_date' AND '$end_date') OR (`to_date` BETWEEN '$start_date' AND '$end_date')) ";
			
			if($claim_status == 'processed'){	$select .= " AND `claim_status` = 'processed' ";	}
			if($claim_status == 'unprocessed'){	$select .= " AND `claim_status` != 'processed' AND `claim_status` = 'rejected' ";	}
			if($claim_status == 'rejected'){	$select .= " AND `claim_status` = 'rejected' ";	}  
			if($claim_status == 'referback'){	$select .= " AND `claim_status` = 'referback' ";	}  
			$select .= " order by created_on asc";
			$query = $this->defaultdb->query($select);
			return $query->result();
    }
		
		public function get_other_unprocessed_list($spark_ids, $start_date, $end_date = '') {
      
      if(!empty($spark_ids))
				$this->defaultdb->where_in("spark_id",$spark_ids);
			$this->defaultdb->where('claim_status', 'approved');
			$this->defaultdb->where("from_date BETWEEN '$start_date' AND '$end_date' ");
			$this->defaultdb->where("to_date BETWEEN '$start_date' AND '$end_date' ");
			$this->defaultdb->order_by("from_date", "asc");
			$query = $this->defaultdb->get("other_claims");
			$result = $query->result();
			//echo"<br>".$this->defaultdb->last_query();
      return $result;
    }
  
    public function get_track_tally_other_list($spark_ids=array(), $process_date) {
      
      $claim_status = array('processed', 'rejected');
      
      if(!empty($spark_ids))
				$this->defaultdb->where_in("spark_id",$spark_ids);
			$this->defaultdb->where("process_date",$process_date);
			$this->defaultdb->where_in("claim_status", $claim_status);
			$this->defaultdb->where("amount > 0");
			$this->defaultdb->order_by("from_date", "asc");
			$query = $this->defaultdb->get("other_claims");
			$result = $query->result();
      return $result;
    }
   
   //FUNCTION ADDED TO DISPLAY NOTIFICATION LIST 
   public function get_notification_claims_list($record_type = '', $spark_ids= array(), $tracking_date_start, $tracking_date_end = '', $status = '') {
      
      //$activity_string = "'training-in', 'training', 'review_meeting', 'meeting-in', 'schoolvisit-in', 'schoolvisit', 'attendance-out'";      
      //$query = "SELECT ssc_travel_claims.id as record_id, ssc_travel_claims.*, ssc_trackings.* FROM ssc_trackings, ssc_travel_claims WHERE ssc_trackings.id = ssc_travel_claims.track_destination_id and ssc_trackings.tracking_time BETWEEN '".$tracking_date_start." 00:00:00' AND '".$tracking_date_end." 23:59:59' and activity_type in (".$activity_string.") ";      
      
      $activity_string = "'training-in', 'training', 'review_meeting', 'meeting-in', 'schoolvisit-in', 'schoolvisit'";
      
      $query = "SELECT ssc_travel_claims.id as record_id, ssc_travel_claims.*, ssc_trackings.* FROM ssc_trackings, ssc_travel_claims WHERE ssc_trackings.id = ssc_travel_claims.track_destination_id and activity_type in (".$activity_string.") ";      
      if(!empty($spark_ids)){
				$sparkids = implode(',', $spark_ids);
				$query .= " AND  spark_id IN ($sparkids) ";      
			}
      if($status !=''){
				if($status == 'approved'){
					$query .= " AND is_claimed = 0 AND claim_discard = 0 ";      
					$query .= " AND ssc_travel_claims.created_at BETWEEN '".$tracking_date_start." 00:00:00' AND '".$tracking_date_end." 23:59:59' ";      					
				}
				if($status == 'rejected'){
					$query .= " AND claim_discard = 1 ";      
					$query .= " AND ssc_travel_claims.updated_on BETWEEN '".$tracking_date_start."' AND '".$tracking_date_end."' ";      					
				}
				if($status == 'processed'){
					$query .= " AND is_claimed = 1 ";      
					$query .= " AND ssc_travel_claims.process_date BETWEEN '".$tracking_date_start."' AND '".$tracking_date_end."' ";      					
				}
			}
			else{
				$query .= " AND ssc_trackings.tracking_time BETWEEN '".$tracking_date_start." 00:00:00' AND '".$tracking_date_end." 23:59:59' ";
			}
      
      $query .= " order by ssc_trackings.tracking_time, ssc_trackings.id ASC ";
      $claims = $this->defaultdb->query($query); 
      if($record_type == 'count'){
				$result = $claims->num_rows();
			}
			else{
				$result = $claims->result();
			}
			//echo"<br>".$this->defaultdb->last_query();
      return $result;
    }
    
  public function get_longdistance_notification($record_type = '', $spark_ids= array(), $start_date, $end_date, $claim_status='')
  {
    if(!empty($spark_ids))
      $this->defaultdb->where_in("user_id",$spark_ids);
    if($claim_status != '')
      $this->defaultdb->where("claim_status", $claim_status);
    $this->defaultdb->where("distance_flag", 1);
    if($claim_status == 'approved'){
			$this->defaultdb->where("created_at >= ",$start_date." 00:00:00");
			$this->defaultdb->where("created_at <= ",$end_date." 23:59:59");
		}
		else if($claim_status == 'rejected' || $claim_status == 'referback'){
			$this->defaultdb->where("updated_on >= ",$start_date." 00:00:00");
			$this->defaultdb->where("updated_on <= ",$end_date." 23:59:59");
		}
		else if($claim_status == 'processed'){
			$this->defaultdb->where("process_date >= ",$start_date);
			$this->defaultdb->where("process_date <= ",$end_date);
		}
		$this->defaultdb->order_by("activity_date", "asc");
    $query = $this->defaultdb->get('other_activities');
    //echo"<br>".$this->defaultdb->last_query();
    if($record_type == 'count'){
			$result = $query->num_rows();
		}
		else{
			$result = $query->result();
		}
    return $result;
  }
  
	function get_stay_notification($record_type = '', $spark_ids= array(), $start_date, $end_date, $claim_status='')
	{
		
		$select = "SELECT soc.*, scp.file_name FROM `ssc_stay_claims` soc, ssc_claim_proofs scp WHERE soc.id=scp.claim_id AND scp.claim_type='Stay Claim' ";
		//AND ((`from_date` BETWEEN '$start_date' AND '$end_date') OR (`to_date` BETWEEN '$start_date' AND '$end_date')) ";
		
		if(!empty($spark_ids)){
			$sparkids = implode(',', $spark_ids);
			$select .= " AND  spark_id IN ($sparkids) ";     
		}
		
		if($claim_status != '')	
			$select .= " AND soc.claim_status = '$claim_status' ";
		
		if($claim_status == 'approved'){
			$select .= " AND soc.created_on BETWEEN '".$start_date." 00:00:00' AND '".$end_date." 23:59:59' ";      					
		}
		if($claim_status == 'rejected'  || $claim_status == 'referback'){
			$select .= " AND soc.updated_on BETWEEN '".$start_date." 00:00:00' AND '".$end_date." 23:59:59' ";      					
		}
		if($claim_status == 'processed'){
			$select .= " AND soc.process_date BETWEEN '".$start_date."' AND '".$end_date."' ";      					
		}
			
		$query = $this->defaultdb->query($select);
    if($record_type == 'count'){
			$result = $query->num_rows();
		}
		else{
			$result = $query->result();
		}
    return $result;
	}
	
	
	function get_food_notification($record_type = '', $spark_ids = array(), $start_date, $end_date, $claim_status='')
	{
		$select = "SELECT soc.*, ssc.stay_reason FROM `ssc_food_claims` soc, `ssc_stay_claims` ssc WHERE ssc.id = soc.stay_claim_id ";
		//AND ((soc.`from_date` BETWEEN '$start_date' AND '$end_date') OR (soc.`to_date` BETWEEN '$start_date' AND '$end_date')) ";
		
		if(!empty($spark_ids)){
			$sparkids = implode(',', $spark_ids);
			$select .= " AND soc.spark_id IN ($sparkids) ";     
		}
		
		if($claim_status != '')	
			$select .= " AND soc.claim_status = '$claim_status' ";
		
		if($claim_status == 'approved'){
			$select .= " AND soc.created_on BETWEEN '".$start_date." 00:00:00' AND '".$end_date." 23:59:59' ";      					
		}
		if($claim_status == 'rejected'  || $claim_status == 'referback'){
			$select .= " AND soc.updated_on BETWEEN '".$start_date." 00:00:00' AND '".$end_date." 23:59:59' ";      					
		}
		if($claim_status == 'processed'){
			$select .= " AND soc.process_date BETWEEN '".$start_date."' AND '".$end_date."' ";      					
		}			
			
		$query = $this->defaultdb->query($select);
    if($record_type == 'count'){
			$result = $query->num_rows();
		}
		else{
			$result = $query->result();
		}
    return $result;
	}
	
	function get_other_claims_notification($record_type = '', $spark_ids, $start_date, $end_date, $claim_status='')
	{
		$select = "SELECT soc.* FROM `ssc_other_claims` soc WHERE spark_id != '' ";
		//$select = "SELECT soc.*, scp.file_name FROM `ssc_other_claims` soc, ssc_claim_proofs scp WHERE soc.id=scp.claim_id AND scp.claim_type='Other Claim' ";
		//AND ((`from_date` BETWEEN '$start_date' AND '$end_date') OR (`to_date` BETWEEN '$start_date' AND '$end_date')) ";
		
		if(!empty($spark_ids)){
			$sparkids = implode(',', $spark_ids);
			$select .= " AND soc.spark_id IN ($sparkids) ";     
		}
		
		if($claim_status != '')	
			$select .= " AND soc.claim_status = '$claim_status' ";
		
		if($claim_status == 'approved'){
			$select .= " AND soc.created_on BETWEEN '".$start_date." 00:00:00' AND '".$end_date." 23:59:59' ";      					
		}
		if($claim_status == 'rejected'  || $claim_status == 'referback'){
			$select .= " AND soc.updated_on BETWEEN '".$start_date." 00:00:00' AND '".$end_date." 23:59:59' ";      					
		}
		if($claim_status == 'processed'){
			$select .= " AND soc.process_date BETWEEN '".$start_date."' AND '".$end_date."' ";      					
		}		
				
		//echo"<br>".$this->defaultdb->last_query();	
		$query = $this->defaultdb->query($select);
    if($record_type == 'count'){
			$result = $query->num_rows();
		}
		else{
			$result = $query->result();
		}
    return $result;
	}
	
	
  public function get_leave_notifications($record_type = '', $spark_ids, $start_date, $end_date, $status = '')
  {
		if($status == 'approved'){
			$this->defaultdb->where_in("user_id", $spark_ids);
			$this->defaultdb->where("created_at >= ",$start_date." 00:00:00");
			if ($end_date != "")
				$this->defaultdb->where("created_at <= ",$end_date." 23:59:59");
			else
				$this->defaultdb->where("created_at <= ",date("Y-m-t",strtotime($start_date))." 00:00:00");
			if($status != '')
				$this->defaultdb->where("status", $status);
			$query = $this->defaultdb->get('leaves');
			if($record_type == 'count'){
				$result = $query->num_rows();
			}
			else{
				$result = $query->result();
			}
			return $result;
		}
		else if($status == 'rejected'){
			$this->defaultdb->where_in("spark_id", $spark_ids);
			$this->defaultdb->where("rejected_on >= ",$start_date." 00:00:00");
			if ($end_date != "")
				$this->defaultdb->where("rejected_on <= ",$end_date." 23:59:59");
			else
				$this->defaultdb->where("rejected_on <= ",date("Y-m-t",strtotime($start_date))." 00:00:00");
			
			$this->defaultdb->where("activity_type", "leaves");
			
			$query = $this->defaultdb->get('rejected_activities');
			if($record_type == 'count'){
				$result = $query->num_rows();
			}
			else{
				$result = $query->result();
			}
			//echo"<br>".$this->defaultdb->last_query();
			return $result;
		}    
  }
		
}
