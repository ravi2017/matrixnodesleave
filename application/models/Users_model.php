<?php
//error_reporting(0);
class Users_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        //Enable profiler
        $this->output->enable_profiler(false);
        $this->defaultdb = $this->load->database('default', TRUE);
    }

    function login($username, $password) {
        $this->defaultdb->from('sparks');
        $this->defaultdb->where('status',1);
        $this->defaultdb->where('login_status',1);
        $this->defaultdb->where('login_id', $username);
        $this->defaultdb->where('password', md5($password));
        $this->defaultdb->limit(1);
        $query = $this->defaultdb->get();
        if ($query->num_rows() == 1) {
            return $query->result();
        } else {
            error_log("Last SQL Query ".$this->db->last_query());
            return false;
        }
    }

    //validate auth token of user
    /*
     * select * from ssc_sparks
        where ssc_sparks.login_id='2201'
        and ssc_sparks.auth_token='47775933e7ca579521fdb413057c0397'
     */
    function validate_auth_token($username, $auth_token) {
        $this->defaultdb->from('sparks');
        $this->defaultdb->where('login_status',1);
        $this->defaultdb->where('login_id', $username);
        $this->defaultdb->where('auth_token', $auth_token);
        $this->defaultdb->limit(1);
        error_log("inside model user_model - validate auth token  1 username=".$username. " ; "."auth token=".$auth_token, 0);
        $query = $this->defaultdb->get();
        error_log("inside model user_model - validate auth token after get user from users table", 0);
        if ($query->num_rows() == 1) {
            error_log("inside model user_model - validate auth token found record ", 0);
            return $query->row_array();
        } else {
            return false;
        }
    }
    
    function validate_auth_token_temp($username, $auth_token) {
        $this->defaultdb->from('sparks');
        $this->defaultdb->where('login_status',1);
        $this->defaultdb->where('login_id', $username);
        $this->defaultdb->limit(1);
        error_log("inside model user_model - validate auth token  1 username=".$username. " ; "."auth token=".$auth_token, 0);
        $query = $this->defaultdb->get();
        error_log("inside model user_model - validate auth token after get user from users table", 0);
        if ($query->num_rows() == 1) {
            error_log("inside model user_model - validate auth token found record ", 0);
            return $query->row_array();
        } else {
            return false;
        }
    }

    //end validate user token function
    public function get_all_users($current_user_id = 0, $status='', $roles = array()) {
        if($current_user_id > 0)
           $this->defaultdb->where('id != ', $current_user_id);
        if($status != '')
           $this->defaultdb->where('status', $status);
        if(!empty($roles))
           $this->defaultdb->where_in('role', $roles);
        
        $query = $this->defaultdb->get('sparks');
        return $query->result();
    }

    public function add($data) {
        $count = $this->db->select('*')->where('login_id', $data['login_id'])->get('sparks')->num_rows();
        if($count == 0){
            $this->defaultdb->insert('sparks', $data);
            return $this->defaultdb->insert_id();
        }else{
            return 0;
        }
    }

    public function edit($data, $id) {
        $this->defaultdb->where('id', $id);
        $this->defaultdb->update('sparks', $data);
    }    

    public function getById($id='', $login_id='') {
				if($login_id != '')
					$this->defaultdb->where('login_id', $login_id);
        if($id != '')
					$this->defaultdb->where('id',$id);
        $query = $this->defaultdb->get('sparks');
        return $query->row_array();
    }
    
    public function getByIds($id) {
        $this->defaultdb->where_in('id', $id);
        $query = $this->defaultdb->get('sparks');
        return $query->result();
    }

    public function getuserBystate($state_id = 0, $district_id = 0) {
        //echo $state_id;echo $district_id;
        //exit();
        if ($state_id > 0) {
            $this->defaultdb->where('ssc_sparks.state_id', $state_id);
        }
        if ($district_id > 0) {
            $this->defaultdb->where('ssc_sparks.district_id', $district_id);
        } else {
            $this->defaultdb->where('ssc_sparks.district_id != ', 0);
        }
        $query = $this->defaultdb->get('ssc_sparks');
        return $query->result();
    }

    public function update_info($data, $id) {
        $this->defaultdb->where('id', $id);
        return $this->defaultdb->update('sparks', $data);
    }

    public function delete_a_user($id) {
        $this->defaultdb->where('id', $id);
        return $this->defaultdb->delete('sparks');
    }
    
     public function getAlluser() {
        $query = $this->defaultdb->get('ssc_sparks');
        return $query->result();
    }
    
    public function  getSparkByState_bk($state_id) {
      if ($state_id > 0) {
          $this->defaultdb->where('ssc_sparks.state_id', $state_id);
          $this->defaultdb->where_in('ssc_sparks.role', array('field_user'));
      }
      $this->defaultdb->order_by('name');
      $query = $this->defaultdb->get('ssc_sparks');
      return $query->result();
    }

    function get_worked_districts($login_id, $state_id, $_fromDate = '', $_toDate = '')
    {
      $query = "SELECT district_id from ssc_school_visits sv where sv.state_id = ".$state_id." AND sv.user_id = ".$login_id." AND `activity_date` BETWEEN '".$_fromDate." 00:00:00' AND '".$_toDate." 23:59:59' AND sv.status = 'approved'
                UNION
                SELECT district_id from ssc_meetings sv where sv.state_id = ".$state_id." AND sv.user_id = ".$login_id." AND `activity_date` BETWEEN '".$_fromDate." 00:00:00' AND '".$_toDate." 23:59:59' AND sv.status = 'approved'
                UNION
                SELECT district_id from ssc_trainings sv where sv.state_id = ".$state_id." AND mode = 'internal' AND sv.user_id = ".$login_id." AND `activity_date` BETWEEN '".$_fromDate." 00:00:00' AND '".$_toDate." 23:59:59' AND sv.status = 'approved'
                UNION
                SELECT district_id from ssc_calls sv where sv.state_id = ".$state_id." AND sv.user_id = ".$login_id." AND `activity_date` BETWEEN '".$_fromDate." 00:00:00' AND '".$_toDate." 23:59:59'
                UNION
                SELECT district_id from ssc_no_visit_days sv where sv.state_id = ".$state_id." AND sv.user_id = ".$login_id." AND `activity_date` BETWEEN '".$_fromDate." 00:00:00' AND '".$_toDate." 23:59:59' AND sv.status = 'approved'
                UNION
                SELECT district_id from ssc_leaves sv where sv.state_id = ".$state_id." AND sv.user_id = ".$login_id." AND `leave_from_date` >= '".$_fromDate." 00:00:00' AND leave_end_date <= '".$_toDate." 23:59:59' AND sv.status = 'approved'";
      $query_result = $this->defaultdb->query($query);
      $row = $query_result->result_array();
      return $row;
    }

    function get_all_worked_districts($login_id, $state_id, $_fromDate = '', $_toDate = '')
    {
      $query = "SELECT district_id from ssc_school_visits sv where sv.user_id = ".$login_id." AND `activity_date` BETWEEN '".$_fromDate." 00:00:00' AND '".$_toDate." 23:59:59' AND sv.status = 'approved'
                UNION
                SELECT district_id from ssc_meetings sv where sv.user_id = ".$login_id." AND `activity_date` BETWEEN '".$_fromDate." 00:00:00' AND '".$_toDate." 23:59:59' AND sv.status = 'approved'
                UNION
                SELECT district_id from ssc_trainings sv where  mode = 'internal' AND sv.user_id = ".$login_id." AND `activity_date` BETWEEN '".$_fromDate." 00:00:00' AND '".$_toDate." 23:59:59' AND sv.status = 'approved'
                UNION
                SELECT district_id from ssc_calls sv where sv.user_id = ".$login_id." AND `activity_date` BETWEEN '".$_fromDate." 00:00:00' AND '".$_toDate." 23:59:59'
                UNION
                SELECT district_id from ssc_no_visit_days sv where sv.user_id = ".$login_id." AND `activity_date` BETWEEN '".$_fromDate." 00:00:00' AND '".$_toDate." 23:59:59' AND sv.status = 'approved'
                UNION
                SELECT district_id from ssc_leaves sv where sv.user_id = ".$login_id." AND `leave_from_date` >= '".$_fromDate." 00:00:00' AND leave_end_date <= '".$_toDate." 23:59:59' AND sv.status = 'approved'";
      $query_result = $this->defaultdb->query($query);
      $row = $query_result->result_array();
      return $row;
    }
    
    public function  getSparkByState($state_id, $role=array('field_user', 'manager'), $user_group = array()) {

      $this->defaultdb->join('ssc_spark_user_state', 'sparks.id = ssc_spark_user_state.user_id');
      $this->defaultdb->where('ssc_sparks.status', 1);
      //$this->defaultdb->where('ssc_spark_user_state.end_date is NULL');
      if ($state_id > 0) {
          //$this->defaultdb->where('ssc_spark_user_state.state_id', $state_id);
          $this->defaultdb->where('ssc_spark_user_state.state_id', $state_id);
          $this->defaultdb->where_in('ssc_sparks.role', $role);
      }
      if(!empty($user_group)){
				$this->defaultdb->where_in('ssc_sparks.user_group', $user_group);
			}
      $this->defaultdb->order_by('name');
      $query = $this->defaultdb->get('ssc_sparks');
      //echo"<br>".$this->defaultdb->last_query();
      return $query->result();
    }
    
    public function  getManagerByState($state_id) {

      $this->defaultdb->join('ssc_spark_user_state', 'sparks.id = ssc_spark_user_state.user_id');
      $this->defaultdb->where('ssc_sparks.status', 1);
      if ($state_id > 0) {
          $this->defaultdb->where('ssc_spark_user_state.state_id', $state_id);
          $this->defaultdb->where_in('ssc_sparks.role', array('manager','state_person'));
      }
      $this->defaultdb->order_by('name');
      $query = $this->defaultdb->get('ssc_sparks');
      //echo"<br>".$this->defaultdb->last_query();
      return $query->result();
    }
    
    public function  getSparkByStateDuration($state_id, $from_date = '', $end_date = '', $user_id = '', $role=array('field_user'), $user_group = array('field_user')) {

      if($from_date !='' && $end_date !=''){

        $month_end = date('Y-m-t', strtotime($end_date));

        $data = $this->getMonthsInRange($from_date, $end_date);
        $dates = implode("','", $data);
        
        $roles = implode("', '",$role);
        $user_groups = implode("', '",$user_group);

        $query = "SELECT * FROM (`ssc_sparks`) JOIN `ssc_spark_user_state` ON `ssc_sparks`.`id` = `ssc_spark_user_state`.`user_id` WHERE `ssc_sparks`.`status` = 1 AND `ssc_spark_user_state`.`state_id` = '$state_id' AND `ssc_sparks`.`role` IN ('$roles') ";
        if($user_id != ''){
          $query .= " AND ssc_spark_user_state.user_id = '$user_id' ";
        }
        $query .= " AND ssc_sparks.user_group IN ('$user_groups') ";
        
        $query .=" AND (ssc_sparks.exit_date >= '$from_date' OR ssc_sparks.exit_date IS NULL)  ";		//condition adding to check spark Exit Date
        $query .=" AND (DATE_FORMAT(ssc_spark_user_state.start_date, '%Y-%m') in('$dates') OR DATE_FORMAT(ssc_spark_user_state.end_date, '%Y-%m') in('$dates') OR (";
        $query .= " ssc_spark_user_state.start_date <= '$from_date' AND (ssc_spark_user_state.end_date >= '$month_end' OR ssc_spark_user_state.end_date IS NULL))) ORDER BY name ";  
      }
      $query_result = $this->defaultdb->query($query);
      $row = $query_result->result_array();
      return $row;
    }
    
    public function  getAllSparkByStateDuration($state_id, $from_date = '', $end_date = '', $user_id = '') {

      if($from_date !='' && $end_date !=''){

        $month_end = date('Y-m-t', strtotime($end_date));

        $data = $this->getMonthsInRange($from_date, $end_date);
        $dates = implode("','", $data);

        $query = "SELECT * FROM (`ssc_sparks`) JOIN `ssc_spark_user_state` ON `ssc_sparks`.`id` = `ssc_spark_user_state`.`user_id` WHERE `ssc_sparks`.`status` = 1 AND `ssc_spark_user_state`.`state_id` = '$state_id' AND `ssc_sparks`.`role` IN ('field_user','manager','state_person') ";
        if($user_id != ''){
          $query .= " AND ssc_spark_user_state.user_id = '$user_id' ";
        }
        $query .=" AND (DATE_FORMAT(ssc_spark_user_state.start_date, '%Y-%m') in('$dates') OR DATE_FORMAT(ssc_spark_user_state.end_date, '%Y-%m') in('$dates') OR (";
        $query .= " ssc_spark_user_state.start_date <= '$from_date' AND (ssc_spark_user_state.end_date >= '$month_end' OR ssc_spark_user_state.end_date IS NULL))) ORDER BY name ";  
      }
      $query_result = $this->defaultdb->query($query);
      $row = $query_result->result_array();
      return $row;
    }


    function getMonthsInRange($startDate, $endDate) {
      $dates = array();
      while (strtotime($startDate) <= strtotime($endDate)) {
          //$months[] = array('year' => date('Y', strtotime($startDate)), 'month' => date('m', strtotime($startDate)), );
          $dates[] = date('Y', strtotime($startDate))."-".date('m', strtotime($startDate));
          $startDate = date('d M Y', strtotime($startDate.'+ 1 month'));
      }
      return $dates;
    }

    public function add_user_state($data)
    {
      $this->defaultdb->insert('spark_user_state', $data);
      return $this->defaultdb->insert_id();
    }

    public function user_blocks($user_id)
    {
      $this->defaultdb->join('ssc_spark_users_district_block', 'blocks.id = spark_users_district_block.block_id');
      $this->defaultdb->where('ssc_spark_users_district_block.user_id', $user_id);
      $this->defaultdb->select('blocks.*');
      $query = $this->defaultdb->get('ssc_blocks');
      return $query->result();      
    }

    public function user_districts($user_id)
    {
      $this->defaultdb->join('ssc_spark_users_district_block', 'districts.id = spark_users_district_block.district_id');
      $this->defaultdb->where('ssc_spark_users_district_block.user_id', $user_id);
      $this->defaultdb->select('districts.*');
      $query = $this->defaultdb->get('ssc_districts');
      return $query->result();      
    }

    public function spark_user_districts($user_id)
    {
      $this->defaultdb->join('ssc_spark_users_district_block', 'districts.id = spark_users_district_block.district_id');
      $this->defaultdb->where('ssc_spark_users_district_block.user_id', $user_id);
      $this->defaultdb->select('distinct(ssc_spark_users_district_block.district_id) AS id, districts.name');
      $query = $this->defaultdb->get('ssc_districts');
      return $query->result();      
    }

    public function spark_user_blocks($user_id, $district_id)
    {
      $this->defaultdb->join('ssc_spark_users_district_block', 'blocks.id = spark_users_district_block.block_id');
      if(!empty($user_id)){
        $this->defaultdb->where('ssc_spark_users_district_block.user_id', $user_id);
      }
      $this->defaultdb->where('ssc_spark_users_district_block.district_id', $district_id);
      $this->defaultdb->select('blocks.*');
      $this->defaultdb->group_by('blocks.id');
      $query = $this->defaultdb->get('ssc_blocks');
      return $query->result();      
    }

    public function get_managers_user_count($user_id)
    {
      $this->defaultdb->where(array('manager_id'=> $user_id,'role'=>'field_user'));
      $query = $this->defaultdb->get('ssc_sparks');
      return $query->num_rows();
    }
    
    
    public function  getSparkByCurrentState($state_id, $role=array('field_user','manager','state_person')) {

      $this->defaultdb->join('ssc_spark_user_state', 'sparks.id = ssc_spark_user_state.user_id');
      $this->defaultdb->where('ssc_sparks.status', 1);
      $this->defaultdb->where('ssc_spark_user_state.end_date IS NULL');
      if ($state_id > 0) {
          $this->defaultdb->where('ssc_spark_user_state.state_id', $state_id);
          $this->defaultdb->where_in('ssc_sparks.role', $role);
      }
      $this->defaultdb->order_by('name');
      $query = $this->defaultdb->get('ssc_sparks');
      //echo"<br>".$this->defaultdb->last_query();
      return $query->result();
    }  
    
    public function insert_device_info($data) {
        $this->defaultdb->insert('spark_devices', $data);
        return $this->defaultdb->insert_id();
    }
    
    public function check_device_info($spark_id, $device_id)
    {
      $this->defaultdb->where(array('spark_id'=> $spark_id, 'device_id'=>$device_id));
      $query = $this->defaultdb->get('spark_devices');
      return $query->num_rows();
    }
    
    public function  getTelegramSparks($state_id, $role=array('field_user','manager','state_person'), $telegram_flag=0) {
      
      $roles = implode("','", $role); 
      
      $query  = "SELECT * FROM `ssc_sparks` JOIN `ssc_spark_user_state` ON `ssc_sparks`.`id` = `ssc_spark_user_state`.`user_id` "; 
      $query .= " WHERE `ssc_sparks`.`status` = 1 AND `ssc_spark_user_state`.`end_date` IS NULL AND `ssc_spark_user_state`.`state_id` = '$state_id' ";
      if($telegram_flag == 0){
        $query .= " AND `ssc_sparks`.`role` IN('$roles') AND `ssc_sparks`.`telegram_report_flag` != 1 ";
      }
      else{
        $query .= " AND (`ssc_sparks`.`role` IN('$roles') OR `ssc_sparks`.`telegram_report_flag` = 1) ";
      }
      $query .= " ORDER BY `name`";
      $query_result = $this->defaultdb->query($query);
      $row = $query_result->result();
      return $row;
    }
    
    function get_spark_by_date($state_id='', $date='')
    {
      $sql = "SELECT * FROM `ssc_spark_user_state` WHERE state_id='$state_id' AND start_date <= '$date' AND (end_date IS NULL OR end_date >= '$date')";
      $query_result = $this->defaultdb->query($sql);
      $row = $query_result->result();
      return $row;
    }
    
    public function getSparksByCurrentState($state_id = 0, $spark_role = array(), $user_group = array()) {
        
        $this->defaultdb->where('status', 1);
        if ($state_id > 0)
            $this->defaultdb->where('state_id', $state_id);
        if(!empty($spark_role))
          $this->defaultdb->where_in('role', $spark_role);
        if(!empty($user_group))
          $this->defaultdb->where_in('user_group', $user_group);
        
        $this->defaultdb->order_by('name');
          
        $query = $this->defaultdb->get('ssc_sparks');
        //echo"<br>".$this->defaultdb->last_query();
        return $query->result();
    }
    
    function update_spark_tally_app_no($spark_id, $type = '')
    {
			if($type == '')
				$sql = "UPDATE ssc_sparks SET tally_app_no = (tally_app_no+1) where id = '$spark_id' ";
			else if($type == 'ld')
				$sql = "UPDATE ssc_sparks SET ld_tally_app_no = (ld_tally_app_no+1) where id = '$spark_id' ";
			else if($type == 'other')
				$sql = "UPDATE ssc_sparks SET other_tally_app_no = (other_tally_app_no+1) where id = '$spark_id' ";
			else if($type == 'stay')
				$sql = "UPDATE ssc_sparks SET stay_tally_app_no = (stay_tally_app_no+1) where id = '$spark_id' ";
			else if($type == 'food')
				$sql = "UPDATE ssc_sparks SET food_tally_app_no = (food_tally_app_no+1) where id = '$spark_id' ";
      $this->defaultdb->query($sql);
    }
    
    function get_userid_by_login($userids)
    {
				$this->defaultdb->where_in('login_id', $userids);
        $this->defaultdb->select('id');
				$query = $this->defaultdb->get('sparks');
				//echo"<br>".$this->defaultdb->last_query();
        return $query->result();
		}
		
		public function  getAppSparksList($role=array('field_user', 'manager'), $group = array('field_user')) {

      $this->defaultdb->join('ssc_spark_user_state', 'sparks.id = ssc_spark_user_state.user_id');
      $this->defaultdb->where('ssc_sparks.status', 1);
      $this->defaultdb->where_in('ssc_sparks.user_group', $group);
      $this->defaultdb->where_in('ssc_sparks.role', $role);
      $this->defaultdb->order_by('name');
      $query = $this->defaultdb->get('ssc_sparks');
      //echo"<br>".$this->defaultdb->last_query();
      return $query->result();
    }
    
    public function  getAllSparksByState($state_id, $from_date = '', $end_date = '', $user_id = '', $role=array('field_user'), $user_group = array()) {

      if($from_date !='' && $end_date !=''){

        $month_end = date('Y-m-t', strtotime($end_date));

        $data = $this->getMonthsInRange($from_date, $end_date);
        $dates = implode("','", $data);
        
        $query = "SELECT * FROM (`ssc_sparks`) JOIN `ssc_spark_user_state` ON `ssc_sparks`.`id` = `ssc_spark_user_state`.`user_id` WHERE  `ssc_sparks`.`login_id` != '' AND `ssc_sparks`.`state_id` != '0' ";
        if($state_id > 0){
					$query .= " AND`ssc_spark_user_state`.`state_id` = '$state_id' ";
				}
        if($user_id != ''){
          $query .= " AND ssc_spark_user_state.user_id = '$user_id' ";
        }
        if(!empty($role)){
					$roles = implode("', '",$role);
					$query .= "  AND `ssc_sparks`.`role` IN ('$roles') ";
				}
        if(!empty($user_group)){
					$usergroups = implode("', '", $user_group);
					$query .= " AND ssc_sparks.user_group IN ('$usergroups') ";
				}
        $query .=" AND (ssc_sparks.exit_date >= '$from_date' OR ssc_sparks.exit_date IS NULL)  ";		//condition adding to check spark Exit Date
        $query .=" AND (DATE_FORMAT(ssc_spark_user_state.start_date, '%Y-%m') in('$dates') OR DATE_FORMAT(ssc_spark_user_state.end_date, '%Y-%m') in('$dates') OR (";
        $query .= " ssc_spark_user_state.start_date <= '$from_date' AND (ssc_spark_user_state.end_date >= '$month_end' OR ssc_spark_user_state.end_date IS NULL))) group by ssc_sparks.login_id ORDER BY name ";  
        
        //echo"<br>".$query;
        $query_result = $this->defaultdb->query($query);
				$row = $query_result->result_array();
				return $row;
      }
      
    }
    
    public function getByLoginIds($login_ids) {
        $this->defaultdb->where_in('login_id', $login_ids);
        $query = $this->defaultdb->get('sparks');
        return $query->result();
    }
    
    public function  getAllHOTeam($manager_id = '', $from_date = '', $end_date = '', $role=array('field_user'), $user_group = array()) {

      if($from_date !='' && $end_date !=''){

        $month_end = date('Y-m-t', strtotime($end_date));

        $data = $this->getMonthsInRange($from_date, $end_date);
        $dates = implode("','", $data);
        
        $query = "SELECT * FROM (`ssc_sparks`) JOIN `ssc_spark_user_state` ON `ssc_sparks`.`id` = `ssc_spark_user_state`.`user_id` WHERE  `ssc_sparks`.`login_id` != '' AND `ssc_sparks`.`state_id` != '0' ";
        
        if($manager_id != '')
        {
					$query .= " AND ssc_sparks.manager_id = '$manager_id' ";
				}
        if(!empty($role)){
					$roles = implode("', '",$role);
					//$query .= "  AND `ssc_sparks`.`role` IN ('$roles') ";
				}
        if(!empty($user_group)){
					$usergroups = implode("', '", $user_group);
					$query .= " AND ssc_sparks.user_group IN ('$usergroups') ";
				}
        $query .=" AND (ssc_sparks.exit_date >= '$from_date' OR ssc_sparks.exit_date IS NULL)  ";		//condition adding to check spark Exit Date
        $query .=" AND (DATE_FORMAT(ssc_spark_user_state.start_date, '%Y-%m') in('$dates') OR DATE_FORMAT(ssc_spark_user_state.end_date, '%Y-%m') in('$dates') OR (";
        $query .= " ssc_spark_user_state.start_date <= '$from_date' AND (ssc_spark_user_state.end_date >= '$month_end' OR ssc_spark_user_state.end_date IS NULL))) group by ssc_sparks.login_id ORDER BY name ";  
        
        //echo"<br>".$query;die;
        $query_result = $this->defaultdb->query($query);
				$row = $query_result->result_array();
				return $row;
      }
      else{
					$this->defaultdb->join('ssc_spark_user_state', 'sparks.id = ssc_spark_user_state.user_id');
					$this->defaultdb->where('ssc_sparks.status', 1);
					if($manager_id != ''){
						$this->defaultdb->where('ssc_sparks.manager_id', $manager_id);
					}
					if(!empty($user_group)){
						$this->defaultdb->where_in('ssc_sparks.user_group', $user_group);
					}
					$this->defaultdb->order_by('name');
					$query = $this->defaultdb->get('ssc_sparks');
					//echo"<br>".$this->defaultdb->last_query();
					return $query->result();
			}
      
    }
    
    
}
