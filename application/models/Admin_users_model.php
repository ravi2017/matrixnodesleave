<?php
class Admin_users_model extends CI_Model {

  public function __construct()
  {
    parent::__construct();
    $this->defaultdb = $this->load->database('default', TRUE);
  }
  
  function login($username, $password)
	{
    //echo $username;exit;
		$this -> defaultdb -> from('users');
		$this -> defaultdb -> where('login_id', $username);
		$this -> defaultdb -> where('password', md5($password));
		$this -> defaultdb -> where('status', 1);
		$this -> defaultdb -> limit(1);
 
		$query = $this -> defaultdb -> get();
 
		if($query -> num_rows() == 1)
		{
			return $query->row_array();
		}
		else
    {
      return false;
    }
	}	
	
  public function get_all_users($current_user_id = 0)
  {
    if ($current_user_id > 0)
    {
		$this -> defaultdb -> where('id != ', $current_user_id);
    }
    $query = $this->defaultdb->get('users');
    return $query->result();
  }

  public function add($data)
  {
    $this->defaultdb->insert('users', $data);
    return $this->defaultdb->insert_id();
  }

  public function getById($id)
  {
    $query = $this->defaultdb->get_where('users',array('id'=>$id));
    return $query->row_array();		  
  }

  public function update_info($data,$id)
  {
    $this->defaultdb->where('users.id',$id);
    return $this->defaultdb->update('users', $data);
  }
  
  public function delete_a_user($id)
  {
    $this->defaultdb->where('users.id',$id);
    return $this->defaultdb->delete('users');
  }
}
