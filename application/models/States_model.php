<?php
class states_model extends CI_Model {

  public function __construct()
  {
    parent::__construct();
    $this->defaultdb = $this->load->database('default', TRUE);
  }

  public function get_all()
  {      
    $this->defaultdb->order_by("id", "desc");
    //$this->defaultdb->limit(1);
    $query = $this->defaultdb->get('states');
    return $query->result();
  }
  
  public function get_by_ids($state_ids)
  {
    $this->defaultdb->where_in('id',$state_ids);
    $query = $this->defaultdb->get('states');
    return $query->result();
  }
  
  public function get_by_id($state_id)
  {
    $this->defaultdb->where('id',$state_id);
    $query = $this->defaultdb->get('states');
    return $query->result();
  }

  public function get_all_states($state = 0, $limit = 0, $offset = 0)
  {
    if ($state > 0)
      $this->defaultdb->where('id',$state);
      
    $this->defaultdb->order_by("id", "desc");
    if ($limit != "")
      $query = $this->defaultdb->get('states', $limit, $offset);
    else
      $query = $this->defaultdb->get('states');
    return $query->result();
  }
  
  public function get_all_states_specific( $limit = 0, $offset = 0)
  {
    if ($limit != "")
      $query = $this->defaultdb->get('states', $limit, $offset);
    else
      $query = $this->defaultdb->get('states');
    return $query->result();
  }

  public function get_state_count()
  {
    $query = $this->defaultdb->count_all_results('states');
    return $query;
  }
  
  public function get_month_state($month, $limit = 5, $offset = 0)
  {
    $this->defaultdb->order_by("id", "desc");
    if ($limit != "")
      $query = $this->defaultdb->get_where('states', array('month' => $month), $limit, $offset);
    else
      $query = $this->defaultdb->get_where('states', array('month' => $month));
    return $query->result();
  }

  public function get_month_state_count($month)
  {    
    $this->defaultdb->where('month',$month);
    $this->defaultdb->from('states');
    return $this->defaultdb->count_all_results();
	}

  public function insert_state_to_db($data)
  {
    return $this->defaultdb->insert('states', $data);
  }

  public function getById($id)
  {
    $query = $this->defaultdb->get_where('states',array('id'=>$id));
    return $query->row_array();		  
  }
  public function getByName($id)
  {
    $query = $this->defaultdb->get_where('states',array('name'=>$id));
    return $query->row_array();		  
  }

  public function update_info($data,$id)
  {
    $this->defaultdb->where('states.id',$id);
    return $this->defaultdb->update('states', $data);
  }

  public function delete_a_state($id)
  {
    $this->defaultdb->where('states.id',$id);
    return $this->defaultdb->delete('states');
  }
  
  public function get_state($state_id, $limit = 5, $offset = 0)
  {
    $this->defaultdb->order_by("name", "asc");
    $this->defaultdb->select("id");
    $this->defaultdb->select("name");
    $this->defaultdb->select("dise_code");
    if ($limit != "")
      $query = $this->defaultdb->get_where('states', array('id' => $state_id), $limit, $offset);
    else
      $query = $this->defaultdb->get_where('states', array('id' => $state_id));
    return $query->result();
  }
  
  public function get_state_auto($q)
  {
    $this->defaultdb->select('id,name,dise_code');
    $this->defaultdb->like('dise_code', $q);
    $query = $this->defaultdb->get_where('states');
    if ($query->num_rows > 0) {
      foreach ($query->result_array() as $row)
      {
        $row1['label'] = htmlentities(stripslashes($row['dise_code']." (".$row['name'].")"));
        $row1['value'] = htmlentities(stripslashes($row['dise_code']." (".$row['name'].")"));
        $row1['id'] = $row['id'];
        $row1['state_id'] = $row['id'];
        $row_set[] = $row1;
      }
      //print_r($row_set);exit();
      echo json_encode($row_set); //format the array into json data
    }
  }
  
  public function get_maturity_index($state_id='', $month_year='')
  { 
    if($state_id !='')
      $this->defaultdb->where('state_id', $state_id);      
    if($month_year !='')
      $this->defaultdb->where('month_year', $month_year);      
    $this->defaultdb->order_by("id", "desc");
    //$this->defaultdb->limit(1);
    $query = $this->defaultdb->get('program_maturity');
    return $query->result();
  }
  
  public function delete_a_maturity($id)
  {
    $this->defaultdb->where('id',$id);
    return $this->defaultdb->delete('program_maturity');
  }
  
  public function insert_maturity_index($data)
  {
    return $this->defaultdb->insert('program_maturity', $data);
  }  
  
  public function get_all_master_states()
  {      
    $this->defaultdb->order_by("name");
    //$this->defaultdb->limit(1);
    $query = $this->defaultdb->get('master_states');
    return $query->result();
  }
}

?>
