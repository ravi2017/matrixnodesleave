<?php
class Attendance_model extends CI_Model {

  public function __construct()
  {
    parent::__construct();
    $this->defaultdb = $this->load->database('default', TRUE);
  }

  public function add($data)
  {
    $this->defaultdb->insert('attendances', $data);
    return $this->defaultdb->insert_id();
  }  
  
  public function getById($id) {
			$query = $this->defaultdb->get_where('attendances', array('id' => $id));
			return $query->row_array();
	}
	
	function check_attendance($spark_id='', $attendance_date='')
  {
		if($spark_id != '')       
			$this->defaultdb->where("spark_id", $spark_id);
		if($attendance_date != '')       
			$this->defaultdb->where("attendance_date", $attendance_date);
			$query = $this->defaultdb->get("attendance_regularizations");
    //echo $this->defaultdb->last_query();
    return $query->result();
  }

  public function insert_regularizations($data)
  {
    $this->defaultdb->insert('attendance_regularizations', $data);
    return $this->defaultdb->insert_id();
  }
  
  function get_attendance_list($spark_id= '', $activitydatestart='', $activitydateend='', $status='', $sparkids = array())
  {
		if($spark_id != '')
			$this->defaultdb->where("spark_id",$spark_id);
		if(!empty($sparkids))
			$this->defaultdb->where_in("spark_id",$sparkids);
		if($status != '')
			$this->defaultdb->where("status",$status);
		if($activitydatestart !='' && $activitydateend != ''){	
			$this->defaultdb->where("attendance_date >= ", $activitydatestart);
			$this->defaultdb->where("attendance_date <= ", $activitydateend);
		}
		$this->defaultdb->order_by('attendance_date', 'desc');
    $query = $this->defaultdb->get("attendance_regularizations");
    //echo $this->defaultdb->last_query();
    return $query->result();
    
		/*$select = "SELECT * FROM ssc_attendance_regularizations where attendance_date between '$activitydatestart' and '$activitydateend' ";  
		if($spark_id != '')
			$select .= " and spark_id = '$spark_id' ";
		$query = $this->defaultdb->query($select);
			return $query->result();*/
  }
  
  function get_attendance_reg_list($spark_ids= array(), $activitydatestart='', $activitydateend='', $status = '')
  {
		$sparkids = '';
		if(!empty($spark_ids))
		{
			$sparkids = implode("','", $spark_ids);
		}	
    
		$select = "SELECT sar.*, ssp.name, ssp.login_id FROM ssc_attendance_regularizations sar, ssc_sparks ssp where sar.spark_id = ssp.id and attendance_date between '$activitydatestart' and '$activitydateend' ";  
		if(!empty($spark_ids))
			$select .= " and spark_id in('$sparkids') ";
		if($status != '')
			$select .= " and sar.status = '$status' ";
		$select .= " order by attendance_date ";	
		$query = $this->defaultdb->query($select);
			return $query->result();
  }
  
  public function updateregularizations($data,$id)
  {
    $this->defaultdb->where('id',$id);
    return $this->defaultdb->update('attendance_regularizations', $data);
  }
  
  //Attendance list from tracking attendance
  function attendance_list($spark_id= '', $activitydatestart='', $activitydateend='')
  {
		if($spark_id != '')
			$this->defaultdb->where("spark_id",$spark_id);
		if($activitydatestart !='' && $activitydateend != ''){	
			$this->defaultdb->where("attendance_date >= ", $activitydatestart);
			$this->defaultdb->where("attendance_date <= ", $activitydateend);
		}
		$this->defaultdb->order_by('attendance_date', 'desc');
    $query = $this->defaultdb->get("attendances");
    //echo $this->defaultdb->last_query();
    return $query->result();
    
  }
  
}

?>
