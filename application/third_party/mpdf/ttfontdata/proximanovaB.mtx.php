<?php
$name='ProximaNova-Bold';
$type='TTF';
$desc=array (
  'CapHeight' => 667,
  'XHeight' => 483,
  'FontBBox' => '[-102 -199 1041 903]',
  'Flags' => 262148,
  'Ascent' => 903,
  'Descent' => -199,
  'Leading' => 0,
  'ItalicAngle' => 0,
  'StemV' => 165,
  'MissingWidth' => 483,
);
$unitsPerEm=2048;
$up=-123;
$ut=20;
$strp=333;
$strs=50;
$ttffile='/var/www/html/techspatch/scripts/mpdf/ttfonts/proximanova-Bold.ttf';
$TTCfontID='0';
$originalsize=44916;
$sip=false;
$smp=false;
$BMPselected=false;
$fontkey='proximanovaB';
$panose=' 0 0 2 0 5 6 3 0 0 2 0 4';
$haskerninfo=false;
$haskernGPOS=false;
$hassmallcapsGSUB=false;
$fontmetrics='win';
// TypoAscender/TypoDescender/TypoLineGap = 790, -210, 0
// usWinAscent/usWinDescent = 903, -218
// hhea Ascent/Descent/LineGap = 903, -218, 0
$useOTL=0x0000;
$rtlPUAstr='';
?>