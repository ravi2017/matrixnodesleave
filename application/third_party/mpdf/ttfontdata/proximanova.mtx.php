<?php
$name='ProximaNova-Regular';
$type='TTF';
$desc=array (
  'CapHeight' => 667,
  'XHeight' => 483,
  'FontBBox' => '[-98 -199 1051 890]',
  'Flags' => 4,
  'Ascent' => 890,
  'Descent' => -199,
  'Leading' => 0,
  'ItalicAngle' => 0,
  'StemV' => 87,
  'MissingWidth' => 507,
);
$unitsPerEm=2048;
$up=-123;
$ut=20;
$strp=333;
$strs=50;
$ttffile='/var/www/html/techspatch/scripts/mpdf/ttfonts/proximanova.ttf';
$TTCfontID='0';
$originalsize=45168;
$sip=false;
$smp=false;
$BMPselected=false;
$fontkey='proximanova';
$panose=' 0 0 2 0 5 6 3 0 0 2 0 4';
$haskerninfo=false;
$haskernGPOS=false;
$hassmallcapsGSUB=false;
$fontmetrics='win';
// TypoAscender/TypoDescender/TypoLineGap = 790, -210, 0
// usWinAscent/usWinDescent = 890, -218
// hhea Ascent/Descent/LineGap = 890, -218, 0
$useOTL=0x0000;
$rtlPUAstr='';
?>