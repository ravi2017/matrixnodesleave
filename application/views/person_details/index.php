<div class="row">
    <div class="box col-md-6 col-md-offset-3">
        <div class="box-inner">
          <form method="post" action="<?php echo base_url();?>person_details/add" role="form" name="frmexternalmeeting">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-th"></i> Enter Person Detail </h2>
            </div>
            
         
            
            <div class="row">
              <div class="form-group col-md-10 col-md-offset-1">
                <label for="title">TYPE</label><span class="err" id="err_title"></span>
                <select class="form-control"  name="type"  id="type">
                  <option value="">Select Type</option>
                  <option value="cluster">CRC</option>
									<option value="block">BRC</option>
									<option value="district">DM</option>
									<option value="teacher">Teacher</option>
                </select>
              </div>
            </div>
						
            <? if ($role != "field_user") { ?>
						<div class="row selectcluster selectplace selectblock selectdistrict selectstate selectteacher">
              <div class="form-group col-md-10 col-md-offset-1">
                <label for="title">Select State</label><span class="err" id="err_title"></span>
                <select class="form-control" id="state_id" name="state_id">
                  <option value="">Select State</option>
                  <? foreach($states as $state) { ?>
                  <option value="<?=$state->id?>" <?=(isset($state_id) and $state->id == $state_id) ? "selected" : ""?>><?=$state->name?></option>
                  <? } ?>
                </select>
              </div>
            </div>
            
            <div class="row selectcluster selectplace selectblock selectdistrict selectteacher">
              <div class="form-group col-md-10 col-md-offset-1">
                <label for="title">Select District</label><span class="err" id="err_title"></span>
                <select class="form-control" id="district_id" name="district_id">
                  <option>Select District</option>
                </select>
              </div>
            </div>
            
            <div class="row selectcluster selectplace selectblock selectteacher">
              <div class="form-group col-md-10 col-md-offset-1">
                <label for="title">Select Block</label><span class="err" id="err_title"></span>
                <select class="form-control" id="block_id" name="block_id">
                  <option>Select Block</option>
                </select>
              </div>
            </div>
						<?}else{?>
							<div class="row selectcluster selectplace selectblock selectdistrict selectstate selectteacher">
              <div class="form-group col-md-10 col-md-offset-1">
                <label for="title">Select State</label><span class="err" id="err_title"></span>
                <select class="form-control" id="state_id" name="state_id">
                  <option value="">Select State</option>
                  <? foreach($states as $state) { ?>
                  <option value="<?=$state->id?>" <?=(isset($state_id) and $state->id == $state_id) ? "selected" : ""?>><?=$state->name?></option>
                  <? } ?>
                </select>
              </div>
            </div>
            
            <div class="row selectcluster selectplace selectblock selectdistrict selectteacher">
              <div class="form-group col-md-10 col-md-offset-1">
                <label for="title">Select District</label><span class="err" id="err_title"></span>
                <select class="form-control" id="district_id" name="district_id">
                 <? foreach($districts as $district) { ?>
					<option value="<?=$district->id?>" selected><?=$district->name?></option>
					<? } ?>
                </select>
              </div>
            </div>
            
            <div class="row selectcluster selectplace selectblock selectteacher">
              <div class="form-group col-md-10 col-md-offset-1">
                <label for="title">Select Block</label><span class="err" id="err_title"></span>
                <select class="form-control" id="block_id" name="block_id">
                 <? foreach($blocks as $block) { ?>
				<option value="<?=$block->id?>" <?=(isset($state_id) and $block->id == $state_id) ? "selected" : ""?>><?=$block->name?></option>
					<? } ?>
                </select>
              </div>
            </div>
						
						<?}?>
            <div class="row selectcluster selectplace selectteacher">
              <div class="form-group col-md-10 col-md-offset-1">
                <label for="title">Select Cluster</label><span class="err" id="err_title"></span>
                <select class="form-control" id="cluster_id" name="cluster_id">
                  <option>Select Cluster</option>
                </select>
              </div>
            </div>
            <div class="row selectplace selectteacher">
              <div class="form-group col-md-10 col-md-offset-1">
                <label for="title">Select School</label><span class="err" id="err_title"></span>
                <select class="form-control" id="school_id" name="school_id">
                  <option>Select School</option>
                </select>
              </div>
            </div>
            
            <div class="row">
              <div class="form-group col-md-10 col-md-offset-1">
                <label for="title">Contact Person Name</label><span class="err" id="err_title"></span>
                <input type="text" name="person_name" id="person_name" value="" class="form-control">
              </div>
            </div>
            
            <div class="row">
              <div class="form-group col-md-10 col-md-offset-1">
                <label for="title">Contact Person Number</label><span class="err" id="err_title"></span>
                <input type="text" name="contact_number" id="contact_number" value="" class="form-control">
              </div>
            </div>
           
           
            
            <div class="row">
              <div class="form-group col-md-10 col-md-offset-1 text-center">
                <a href="<?=base_url()?>reports">Cancel</a>
                <button type="submit" class="btn btn-default">Submit</button>
              </div>
            </div>
            
          </form>
        </div>
    </div>
</div>

<script type="text/javascript">
$('[id^="select"]').hide();
$(".selectplace").hide()

$(".designation").change(function(){
  if ($(this).val() == "others")
  {
    $(".selectothers").show()
  }
  else
  {
    $(".selectothers").hide()
  }
})
  
$("#type").change(function() {
  var name = $(this).val();
  if (name == "HO")
  {
    $(".selectothers").show()
  }
  else
  {
    $(".selectothers").hide()
  }
  $('[id^="selectmeetwith"]').hide();
  $(".selectplace").hide()
  $("#selectmeetwith"+name).show()
  $(".select"+name).show()
});

function get_district_list(sid,did)
{
  $.ajax({
    url: '<?=base_url()?>dashboard/get_districts',
    type: 'get',
    data: {
      state: sid
    },
    dataType: 'json',
    success: function(response) {
      $("#district_id").html("<option value=''>Select District</option>");
      $.each(response, function() {
        if (did == this['id'])
          $("#district_id").append("<option value='" + this['id'] + "' selected>" + this['value'] + " </option>");
        else
          $("#district_id").append("<option value='" + this['id'] + "'>" + this['value'] + " </option>");
      });
      $("#district_id").show();
    },
    error: function(response) {
      window.console.log(response);
    }
  });
}

function get_block_list(did,bid)
{
  $.ajax({
    url: '<?=base_url()?>dashboard/get_blocks',
    type: 'get',
    data: {
      district: did
    },
    dataType: 'json',
    success: function(response) {
      $("#block_id").html("<option value=''>Select Block</option>");
      $.each(response, function() {
        if (bid == this['id'])
          $("#block_id").append("<option value='" + this['id'] + "' selected>" + this['value'] + " </option>");
        else
          $("#block_id").append("<option value='" + this['id'] + "'>" + this['value'] + " </option>");
      });
      $("#block_id").show();
    },
    error: function(response) {
      window.console.log(response);
    }
  });
}

function get_cluster_list(bid,cid)
{
  $.ajax({
    url: '<?=base_url()?>dashboard/get_clusters',
    type: 'get',
    data: {
      block: bid
    },
    dataType: 'json',
    success: function(response) {
      $("#cluster_id").html("<option value=''>Select Cluster</option>");
      $.each(response, function() {
        if (cid == this['id'])
          $("#cluster_id").append("<option value='" + this['id'] + "' selected>" + this['value'] + " </option>");
        else
          $("#cluster_id").append("<option value='" + this['id'] + "'>" + this['value'] + " </option>");
      });
      $("#cluster_id").show();
    },
    error: function(response) {
      window.console.log(response);
    }
  });
}

function get_school_list(cid,scid)
{
  $.ajax({
    url: '<?=base_url()?>dashboard/get_schools_list',
    type: 'get',
    data: {
      cluster: cid
    },
    dataType: 'json',
    success: function(response) {
      $("#school_id").html("<option value=''>Select School</option>");
      $.each(response, function() {
        if (cid == this['id'])
          $("#school_id").append("<option value='" + this['id'] + "' selected>" + this['value'] + " </option>");
        else
          $("#school_id").append("<option value='" + this['id'] + "'>" + this['value'] + " </option>");
      });
      $("#school_id").show();
    },
    error: function(response) {
      window.console.log(response);
    }
  });
}

$("#state_id").change(function() {
  get_district_list($(this).val(),'')
});

$("#district_id").change(function() {
  get_block_list($(this).val(),'')
});

$("#block_id").change(function() {
  get_cluster_list($(this).val(),'')
});

$("#cluster_id").change(function() {
  get_school_list($(this).val(),'')
});

</script>
