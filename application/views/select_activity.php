<div class="row">
    <div class="box col-md-6 col-md-offset-3">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-th"></i> Select Activity for <?=strftime("%d-%b-%Y",strtotime($activity_date))?></h2>
            </div>
            
            <div class="box-content">
                <div class="row">
                    <div class="col-md-offset-4 text-center col-md-4"><a href="<?=base_url()?>school_visit"><h6>School Visit</h6></a></div>
                </div>
            </div>
            <div class="box-content">
                <div class="row">
                    <div class="col-md-offset-4 text-center col-md-4"><a href="<?=base_url()?>training"><h6>Training</h6></a></div>
                </div>
            </div>
            <div class="box-content">
                <div class="row">
                    <div class="col-md-offset-4 text-center col-md-4"><a href="<?=base_url()?>meeting"><h6>Meeting</h6></a></div>
                </div>
            </div>
            <div class="box-content">
                <div class="row">
                    <div class="col-md-offset-4 text-center col-md-4"><a href="<?=base_url()?>holiday"><h6>Holiday</h6></a></div>
                </div>
            </div>
            <div class="box-content">
                <div class="row">
                    <div class="col-md-offset-4 text-center col-md-4"><a href="#"><h6>Others</h6></a></div>
                </div>
            </div>
            <div class="box-content">
                <div class="row">
                    <div class="col-md-offset-4 text-center col-md-4"><a href="<?=base_url()?>"><h6>Change Date</h6></a></div>
                </div>
            </div>
        </div>
    </div>
</div>
