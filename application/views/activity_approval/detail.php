<table align="center" width="100%">
<tr>
  <td align="center" style="text-align:center;">Daily Activity of <strong><?=$spark_name['name'];?></strong>
  <?php if (date("d-m-Y",strtotime($report_end_date)) == date("d-m-Y",strtotime($report_date))) { ?>
   for <strong><?=date("d-M-Y",strtotime($report_date));?></strong>
  <?php } else { ?>
   from <strong><?=date("d-M-Y",strtotime($report_date));?></strong> to <strong><?=date("d-M-Y",strtotime($report_end_date));?></strong>
  <?php } ?>
  <?php if($pdf != 1 and $excel != 1){?><a href="<?=base_url()?>activity_approval" style="float:right">Back</a><?php } ?></td>
</tr>
</table>

<div class="scrollme">
<table class="table table-striped table-bordered bootstrap-datatable datatable1 responsive">
	<tbody>
		<tr>
			<th><span style="font-weight: bold;">Activity</span></th>
			<th><span style="font-weight: bold;">Location</span></th>
			<th><span style="font-weight: bold;">Activity Details</span></th>
			<th><span style="font-weight: bold;">Assessment</span></th>
			<th><span style="font-weight: bold;">Status</span></th>
			<th data-priority="1" data-sort=0 class="no-sort action"><input type="button" name="approve" value="Approve" id="approve"> <br />
        <span style="font-weight: bold;">Check All</span> <input type="checkbox" name="approve_all" id="approve_all" value="all"> </th>
		</tr>
    <? foreach($activities as $date=>$activity_detail) { ?>
      <? $new = 1; ?>
            
      <? if (count($activity_detail) == 0) { ?>
    <!--  <tr>
        <td><? date("d-m-Y",strtotime($date))?></td>
        <td>-</td>
        <td>-</td>
        <td>-</td>
        <td>-</td>
        <td>-</td>
        <td>-</td>
        <td>-</td>
        <td>-</td>
      </tr> -->
      <? } else { ?>
        <? foreach($activity_detail as $activity) { ?>
        <tr valign="top">
          <td><?=$activity['activity_type']?> <br>(<?=date("d-m-Y",strtotime($date))?>)</td>
          <td>
            <? echo (!empty($school_names[$activity['school']]) ? '<strong>School : </strong>'.$school_names[$activity['school']].'<br>' : ''); ?>
            <? echo (!empty($cluster_names[$activity['cluster']]) ? '<strong>Cluster : </strong>'.$cluster_names[$activity['cluster']].'<br>' : ''); ?>
            <? echo (!empty($block_names[$activity['block']]) ? '<strong>Block : </strong>'.$block_names[$activity['block']].'<br>' : ''); ?>
            <? echo (!empty($district_names[$activity['district']]) ? '<strong>District : </strong>'.$district_names[$activity['district']].'<br>' : ''); ?>
            <? echo (!empty($state_names[$activity['state']]) ? '<strong>State : </strong>'.$state_names[$activity['state']].'<br>' : ''); ?>
          </td>
          <td><?=$activity['details']?></td>
          <td><a href="javascript:void(0)" class="view_assessment" alt="<?=str_replace(' ','_',strtolower($activity['activity_type']))."@".$activity['id']?>">View</a></td>
          <td id="status_cell">
          <?php if($activity['status'] == "pending"){ ?>
            <div id="status<?php echo $activity['id']?>">
              <a href="javascript:void(0)" class="btn btn-success btn-sm status" action="approve_activity" activity_type="<?php echo $activity['type']?>" activity_id="<?php echo $activity['id']?>" >Approve</a>

              <a href="javascript:void(0)" class="btn btn-danger btn-sm reject_status" action="reject_activity" activity_type="<?php echo $activity['type']?>" activity_id="<?php echo $activity['id']?>" style="margin-top: 2px;">Reject&nbsp;&nbsp;&nbsp;</a>
            </div>
          <?php }else if($activity['status'] == "approved"){ ?>
           <div id="status<?php echo $activity['id']?>"> 
            <a href="javascript:void(0)" class="btn btn-sm btn-success" id="approve<?php echo $activity['id']?>">Approved</a>
            <a href="javascript:void(0)" class="btn btn-danger btn-sm reject_status" action="reject_activity" activity_type="<?php echo $activity['type']?>" activity_id="<?php echo $activity['id']?>" style="margin-top: 2px;">Reject&nbsp;&nbsp;&nbsp;</a>
           </div>  
          <?  } ?>  
            <a href="javascript:void(0)" class="btn btn-sm btn-success" style="display: none;" id="approve<?php echo $activity['id']?>">Approved</a><br>
            <a href="javascript:void(0)" class="btn btn-sm btn-danger" style="display: none;" id="reject<?php echo $activity['id']?>">Rejected</a>
          </td>
          <td><input type="checkbox" class="approve_activity" name="approve['<?php echo $activity['id']?>']" id="approve_<?php echo $activity['id']?>" value="<?php echo $activity['id']?>" activity_type="<?php echo $activity['type']?>"></td>
        </tr>
        <? $new = 0; ?>
        <? } ?>
      <? } ?>
    <? } ?>
	</tbody>
</table>
</div>

<div class="modal fade" id="show_assessment" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Assessment Details</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body" id="assessment_data">
        </div>
      </div>
    </div>
  </div>

<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Reason</h4>
        </div>
        <div class="modal-body">
          <form id="modal_form" method="POST">
            <div class="row">   
            <textarea class="form-control" name="reason"></textarea>      
           </div>
           <br>
           <input type="hidden" id="modal_activity_type" name="activity_type" value="">
           <input type="hidden" id="modal_activity_id" name="activity_id" value="">
           <button type="submit" id="submit_modal" class="btn btn-primary" style="float:right">Submit</button>
          </form>
        </div>
        <div class="modal-footer">
        </div>
      </div>

    </div>
  </div>
</div>
<script>
  $(document).on('click', '.view_assessment', function(){
      var cur_val = $(this).attr('alt');
      var var_arr = cur_val.split('@');
      var activity_type = var_arr[0];
      var activity_id = var_arr[1];

      $.ajax({
        url: BASE_URL+'activity/get_assessment_data',
        type: 'post',
        data: 'activity_type='+activity_type+'&activity_id='+activity_id,
        dataType: 'html',
        success: function(response) {
          $("#assessment_data").html(response)
          $("#show_assessment").modal('show');
        },
        error: function(response, error) {
          window.console.log(error);
        }
      });
  });  
  
  $('#approve_all').click(function(){
    $('input:checkbox').not(this).prop('checked', this.checked);
  });

  $(document).ready(function() {
        $("#approve").click(function(){
            var ids = [];
            var types = [];
            $.each($("input:checked"), function(){
              if($(this).val() != "all"){   
                ids.push($(this).val());
                types.push($(this).attr('activity_type'));
              }
            });
            //alert(ids)
            $.ajax({
              url: '<?php echo base_url()?>activity_approval/approve_multiple_activities',
              type: 'get',
              data: {
                ids : ids,
                types : types
              },
              dataType: 'json',
              success: function(response) {
                $.each(ids, function(i, val){
                  $('#status'+val).hide();
                  $('#approve'+val).show();
                });
              },
              error: function(response) {
                window.console.log(response);
              }
            });
        });
    });


  $(document).on('click', '.status', function(){
    var action = $(this).attr('action')
    var type = $(this).attr('activity_type')
    var id = $(this).attr('activity_id')
    $.ajax({
    url: '<?=base_url()?>activity_approval/'+action+'/'+type+'/'+id,
    type: 'get',
    data: {
      activity_type: type,
      activity_id: id
    },
    dataType: 'json',
    success: function(response) {
      $('#status'+id).hide();
      $('#approve'+id).show();
    },
    error: function(response) {
      window.console.log(response);
    }
  });
  });

  $(document).on('click', '.reject_status', function(){
    $('#myModal').modal('show')
    $('#modal_activity_type').val($(this).attr('activity_type'))
    $('#modal_activity_id').val($(this).attr('activity_id'))
  });

  $("#modal_form").submit(function(e) {
    e.preventDefault(); 
    var form = $(this);
    var id = $('#modal_activity_id').val()
    $.ajax({
         type: "POST",
         url: '<?=base_url()?>activity_approval/reject_activity',
         data: form.serialize(), 
         success: function(data)
         {
            $('#status'+id).hide();
            $('#reject'+id).show();
         }
       });
    $('#myModal').modal('hide');
  });
</script>


