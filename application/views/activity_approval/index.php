<div class="row">
  <div class="col-md-12">
    <form name="frmreport" id="frmreport" action="<?php echo base_url();?>activity_approval/submit_activity_approval" method="post">
      <div class="row">
        <div class="form-group col-md-3">
          <label for="Date Of Visit">Select Spark</label><span class="err" id="err_date"></span>
          <select class="form-control required" id="user_login_id" name="user_login_id">
            <option value="">Select Spark</option>
            <?php foreach ($sparks as $spark) { ?>
            <option value="<?php echo $spark->id?>"><?php echo $spark->name." (".$spark->login_id.")"?></option>
            <?php } ?>
          </select>
        </div>
        <div class="form-group col-md-3">
          <label for="Date Of Visit">From</label><span class="err" id="err_date"></span>
          <input type="text" id="activitystartdate" name="activitystartdate" class="form-control required" value="<?php echo $activitystartdate; ?>">
        </div>
        <div class="form-group col-md-3 field_user">
          <label for="Date Of Visit">To</label><span class="err" id="err_date"></span>
          <input type="text" id="activityenddate" name="activityenddate" class="form-control required" value="<?php echo $activityenddate; ?>">
        </div>
        <div class="form-group col-md-3">
          <label for="Date Of Visit">&nbsp;</label><span class="err" id="err_date"></span>
          <button type="submit" class="btn btn-success btn-sm">Submit</button>
        </div>
      </div>
    </form>
  </div>
</div>



<script type="text/javascript">
	
$(document).ready(function ()
{
	$("#frmreport").validate();
});
  
$(function(){
    var dateFormat = "dd-mm-yy",
      from = $( "#activitystartdate" )
        .datepicker({
          //defaultDate: "+1w",
          changeMonth: true,
          numberOfMonths: 1,
          dateFormat: dateFormat
        })
        .on( "change", function() {
          to.datepicker( "option", "minDate", getDate( this ) );
        }),
      to = $( "#activityenddate" ).datepicker({
        //defaultDate: "+1w",
        changeMonth: true,
        numberOfMonths: 1,
        dateFormat: dateFormat
      })
      .on( "change", function() {
        from.datepicker( "option", "maxDate", getDate( this ) );
      });
 
    function getDate( element ) {
      var date;
      try {
        date = $.datepicker.parseDate( dateFormat, element.value );
      } catch( error ) {
        date = null;
      } 
      return date;
    }
});

</script>


