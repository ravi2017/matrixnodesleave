<table align="center" border="1" style="margin-top:50px;">
	<tbody>
		<tr>
			<th><span style="font-weight: bold;">Activity Date</span></th>
			<th><span style="font-weight: bold;">Activity Type</span></th>
			<th><span style="font-weight: bold;">Reason</span></th>
      <th><span style="font-weight: bold;">Rejected on</span></th>
			<th><span style="font-weight: bold;">Details</span></th>
			<th><span style="font-weight: bold;">Action</span></th>
		</tr>
    <? foreach($activities as $activity) { ?>
      <?php if($activity['activity_type'] == "Leave") continue;?>
    <tr valign="top">
      <td><?=date_format(date_create($activity['activity_date']), "d-m-Y h:i a")?></td>
      <td><?php $act_type = ucwords($activity['activity_type']); echo $act_type?></td>
      <td><?=$activity['reason']?></td>
      <td><?=date_format(date_create($activity['rejected_on']), "d-m-Y h:i a")?></td>
      <td><?php 
          if($activity['activity_type'] == "leaves"){
            $state = $this->db->select('name')->where('id', $activity['state_id'])->get('states')->row();
            $district = $this->db->select('name')->where('id', $activity['district_id'])->get('districts')->row();
            $leave = $this->db->select('leave_from_date, leave_end_date')->where('id', $activity['id'])->get('leaves')->row();
          ?>  
          <?php if(!empty($state->name)){?><b>State:</b> <?php echo $state->name;?>&nbsp;<?php } ?><?php if(!empty($district->name)){?><b>District:</b> <?php echo $district->name;?>&nbsp;<?php } ?><?php if(!empty($leave->leave_from_date)){?><b>Leave from:</b> <?php echo $leave->leave_from_date;?>&nbsp;<?php } ?><?php if(!empty($leave->leave_end_date)){?><b>To:</b> <?php echo $leave->leave_end_date;?>&nbsp;<?php } ?>
        <?php } ?>
        <?php 
          if($activity['activity_type'] != "leaves"){
            $state = $this->db->select('name')->where('id', $activity['state_id'])->get('states')->row();
            $district = $this->db->select('name')->where('id', $activity['district_id'])->get('districts')->row();
            $block = $this->db->select('name')->where('id', $activity['block_id'])->get('blocks')->row();
          ?>  
          <?php if(!empty($state->name)){?><b>State:</b> <?php echo $state->name;?>,&nbsp;<?php } ?><?php if(!empty($district->name)){?><b>District:</b> <?php echo $district->name;?>,&nbsp;<?php } ?><?php if(!empty($block->name)){?><b>Block:</b> <?php echo $block->name;?>&nbsp;<?php } ?>
        <?php } ?>
        <?php 
          if($activity['activity_type'] == "meetings"){
            $meeting = $this->db->select('meeting_type')->where('id', $activity['id'])->get('meetings')->row();
          ?>  
          <?php if(!empty($meeting->meeting_type)){?><b>Meeting type:</b> <?php echo $meeting->meeting_type;?><?php } ?>
        <?php } ?>
        <?php 
          if($activity['activity_type'] == "school_visits"){
            $school = $this->db->select('name')->where('id', $activity['school_id'])->get('schools')->row();
          ?>  
          <?php if(!empty($school->name)){?><b>School:</b> <?php echo $school->name;?>&nbsp;<?php } ?>
        <?php } ?>
        <?php 
          if($activity['activity_type'] == "trainings"){
            $training = $this->db->select('training_type, training_duration')->where('id', $activity['id'])->get('trainings')->row();
          ?>  
          <?php if(!empty($training->training_type)){?><b>Training type:</b> <?php echo $training->training_type;?>,&nbsp;<?php } ?><?php if(!empty($training->duration)){?><b>Duration:</b> <?php echo $training->duration;?><?php } ?>
        <?php } ?>
      </td>
      <td>
        <a href="javascript:void(0)" class="btn btn-success reject_status" action="reject_activity" activity_id="<?php echo $activity['activity_id']?>" style="margin-top: 2px;">Re-Enter&nbsp;&nbsp;&nbsp;</a>
      </td>
    </tr>
    <? } ?>
	</tbody>
</table>
<div  style="text-align: center;">
  <a class="btn btn-primary" href="<?php echo base_url();?>dashboard" style="margin-top: 20px;">
      <i class="glyphicon glyphicon-add icon-white"></i>
      Go back
  </a>
</div>

  <div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Choose Activity To Re-Enter</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="form-group activity_list col-md-12 col-md-offset-1">
              
            </div>
          </div>
        </div>
        <div class="modal-footer">
        </div>
      </div>
    </div>
  </div>

<script>
  $(document).on('click', '.reject_status', function(){
    $('#myModal').modal('show');
    var id = $(this).attr('activity_id');
    //var type = $(this).attr('activity_type');
    $('.activity_list').html('<li><label for="school_visit"><a href="<?=base_url()?>activity/school_visit?id='+id+'">School Visit</a></label></li><li><label for="review_meeting"><a href="<?=base_url()?>activity/review_meeting?id='+id+'">Review Meeting</a></label></li><li><label for="training"><a href="<?=base_url()?>activity/training?id='+id+'">Training</a></label></li><li><label for="call"><a href="<?=base_url()?>activity/call?id='+id+'">Call</a></label></li><li><label for="no_visit"><a href="<?=base_url()?>activity/no_visit?id='+id+'">NO Visit</a></label></li>');
  });

</script>  
