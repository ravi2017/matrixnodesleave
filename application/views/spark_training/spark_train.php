<div class="box col-md-6 col-md-offset-3">
        <div class="box-inner">
<form name="frmreport" action="<?php echo base_url();?>spark_training/sparktrainingDetail" method="get" onsubmit="return validatereport()">
   <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-th"></i>Spark Training Detailed Report</h2>
            </div>
    <div class="row">
    <div class="form-group col-md-10 col-md-offset-1 field_user">
      <select class="form-control" id="state_id" name="state_id">
        <option value="">Select State</option>
        <? foreach($states as $s) { ?>
        <option value="<?=$s->id?>" <?=(isset($state_id) and $s->id == $state_id) ? "selected" : ""?>><?=$s->name?></option>
        <? } ?>
      </select>
    </div>

   
    <div class="form-group col-md-5 col-md-offset-1 field_user">
      <select class="form-control" id="month" name="month">
        <option value="" <?=($month == '') ? 'selected' : ''?>>Start Month</option>
        <option value=1 <?=($month == 1) ? 'selected' : ''?>>Jan</option>
        <option value=2 <?=($month == 2) ? 'selected' : ''?>>Feb</option>
        <option value=3 <?=($month == 3) ? 'selected' : ''?>>Mar</option>
        <option value=4 <?=($month == 4) ? 'selected' : ''?>>Apr</option>
        <option value=5 <?=($month == 5) ? 'selected' : ''?>>May</option>
        <option value=6 <?=($month == 6) ? 'selected' : ''?>>Jun</option>
        <option value=7 <?=($month == 7) ? 'selected' : ''?>>Jul</option>
        <option value=8 <?=($month == 8) ? 'selected' : ''?>>Aug</option>
        <option value=9 <?=($month == 9) ? 'selected' : ''?>>Sep</option>
        <option value=10 <?=($month == 10) ? 'selected' : ''?>>Oct</option>
        <option value=11 <?=($month == 11) ? 'selected' : ''?>>Nov</option>
        <option value=12 <?=($month == 12) ? 'selected' : ''?>>Dec</option>
      </select>
    </div>
    <div class="form-group col-md-5 col-md-offset-0 field_user">
      <select class="form-control" id="year" name="year">
        <option value="" <?=($year == '') ? 'selected' : ''?>>Start Year</option>
        <option value="2017" <?=($year == 2017) ? 'selected' : ''?>>2017</option>
        <option value="2018" <?=($year == 2018) ? 'selected' : ''?>>2018</option>
        <option value="2019" <?=($year == 2019) ? 'selected' : ''?>>2019</option>
      </select>
    </div>
    <div class="form-group col-md-10 col-md-offset-1 text-center">
      <button type="submit" class="btn btn-red">Get Report</button>
    </div>
  </div>
</form>
<? if (isset($district)) { ?>
<input type="button" class="btn btn-success" onclick="state_report();" value="Print Report" />
<!--<a href="<?=base_url()?>print_reports/state?state_id=<?=$state_id?>&year=<?=$year?>&month=<?=$month?>&download=1"><input type="button" value="Download Report"class="btn btn-success"></a>--></br><br>
<table class="table table-striped table-bordered bootstrap-datatable datatable1 responsive">
	<thead>
		<tr>
			<th>State</th>
			<th>District</th>
			<th>Total No Of Schools</th>
			<th>No Of Schools visited with CRC</th>
			<th>CRC met physically (0/1)</th>
      <th>Review over phone with CRC where school visit not done (0/1)</th>
      <th>No. of schools in which TLM usage is 100% (0)</th>
      <th>No. of schools in which TLM is partially used (1)</th>
      <th>No of schools where TLM is not used (2)</th>
      <th>Monthly meeting attended at BRC (0/1)</th>
      <th>Collector/DC/CEO/DEO  Met (0/1)</th>
      <th>Training</th>
		</tr>
	</thead>
	<tbody>
	<?$total_school_visit_count=0?>
	<?$total_physical_meet=0?>
	<?$total_telephonic_meet=0?>
	<?$total_tlm_0=0?>
	<?$total_tlm_1=0?>
	<?$total_tlm_2=0?>
	<?$total_meet_brc=0?>
	<?$total_meet_dc=0?>
	<?$total_traning=0?>
	<?php foreach ($district as $districts){ ?>
		<tr>
			<td><?php echo $state->name; ?></td>
			<td><?php echo $districts->district_name; ?></td>
			
			<td><?php echo $districts->total_schools; ?></td>
			<td><?php echo $school_visit_count[$districts->district_id]; ?></td>
				<?php $total_school_visit_count=$total_school_visit_count+$school_visit_count[$districts->district_id]; ?>
      <td><?php echo ($physical_meet[$districts->district_id]); ?></td>
      	<?php $total_physical_meet=$total_physical_meet+$physical_meet[$districts->district_id]; ?>
      <td><?php echo ($telephonic_meet[$districts->district_id]); ?></td>
				<?php  $total_telephonic_meet=$total_telephonic_meet+$telephonic_meet[$districts->district_id]; ?>
			<td><?php echo round($tlm_0[$districts->district_id]); ?></td>
				<?php $total_tlm_0=$total_tlm_0+$tlm_0[$districts->district_id]; ?>
			<td><?php echo round($tlm_1[$districts->district_id]); ?></td>
				<?php  $total_tlm_1=$total_tlm_1+$tlm_1[$districts->district_id]; ?>
			<td><?php echo round($tlm_2[$districts->district_id]); ?></td>
				<?php  $total_tlm_2=$total_tlm_2+$tlm_2[$districts->district_id]; ?>
      <td><?php echo $meet_brc[$districts->district_id] ?></td>
				<?php  $total_meet_brc=$total_meet_brc+$meet_brc[$districts->district_id]; ?>
      <td><?php echo $meet_dc[$districts->district_id] ?></td>
				<?php  $total_meet_dc=$total_meet_dc+$meet_dc[$districts->district_id]; ?>
      <td><?php echo $traning[$districts->district_id] ?></td>
				<?php  $total_traning=$total_traning+$traning[$districts->district_id]; ?>
		</tr>
	<?php } ?>
	<tr>
		
			<td colspan="3">TOTAL</td>
			<td><?php echo $total_school_visit_count; ?></td>
			<td><?php echo $total_physical_meet; ?></td>
			<td><?php echo $total_telephonic_meet; ?></td>
			<td><?php echo $total_tlm_0; ?></td>
			<td><?php echo $total_tlm_1; ?></td>
			<td><?php echo $total_tlm_2; ?></td>
			<td><?php echo $total_meet_brc; ?></td>
			<td><?php echo $total_meet_dc; ?></td>
			<td><?php echo $total_traning; ?></td>
		
		</tr>
  </tbody>
</table>
<? } ?>

<script>
  
function get_district_list(sid,did)
{
  $.ajax({
    url: '<?=base_url()?>dashboard/get_districts',
    type: 'get',
    data: {
      state: sid
    },
    dataType: 'json',
    success: function(response) {
      $("#district_id").html("<option value=''>Select District</option>");
      $.each(response, function() {
        if (did == this['id'])
          $("#district_id").append("<option value='" + this['id'] + "' selected>" + this['value'] + "</option>");
        else
          $("#district_id").append("<option value='" + this['id'] + "'>" + this['value'] + "</option>");
      });
      $("#district_id").show();
    },
    error: function(response) {
      window.console.log(response);
    }
  });
}

function get_block_list(did,bid)
{
  $.ajax({
    url: '<?=base_url()?>dashboard/get_blocks',
    type: 'get',
    data: {
      district: did
    },
    dataType: 'json',
    success: function(response) {
      $("#block_id").html("<option value=''>Select Block</option>");
      $.each(response, function() {
        if (bid == this['id'])
          $("#block_id").append("<option value='" + this['id'] + "' selected>" + this['value'] + " (Total Schools - " +  this['school_count'] + ")</option>");
        else
          $("#block_id").append("<option value='" + this['id'] + "'>" + this['value'] + " (Total Schools - " +  this['school_count'] + ")</option>");
      });
      $("#block_id").show();
    },
    error: function(response) {
      window.console.log(response);
    }
  });
}

$("#state_id").change(function() {
  get_district_list($(this).val(),'')
});




function validatereport()
{
  if ($("#state_id").val() == "")
  {
    alert("Please select state")
    return false;
  }
  if ($("#monthno").val() == "")
  {
    alert("Please select month")
    return false;
  }
    if ($("#year").val() == "")
  {
    alert("Please select year")
    return false;
  }

  return true
}
</script>
<script type="text/javascript">
    
    function state_report() {
       $.ajax({
         
        url: '<?=base_url()?>print_reports/state?state_id=<?=(isset($state_id) ) ? $state_id : ""?>&year=<?=(isset($year) ) ? $year : ""?>&month=<?=(isset($month) ) ? $month : ""?>',
        type: 'get',
        dataType: 'html',
        success: function(response) {
          var divContents = response;
          var printWindow = window.open('', '', 'height=400,width=800');
          printWindow.document.write('<html><head><title>Block Report</title>');
          printWindow.document.write('</head><body >');
          printWindow.document.write(divContents);
          printWindow.document.write('</body></html>');
          printWindow.document.close();
          printWindow.print();
        },
        error: function(response) {
          window.console.log(response);
        }
      });
    }

    
</script>
