<? if(!isset($download) && !isset($pdf)) {?>
<form name="frmtraining" id="frmtraining" method="post" action="" class="">
  <div class="row">
    <div class="col-md-3">
      <div class="form-group">
        <input type="text" id="activitydatestart" name="activitydatestart" class="form-control required" value="<? echo (!empty($activitydatestart) ? $activitydatestart : '');?>" autocomplete="off">
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <input type="text" id="activitydateend" name="activitydateend" class="form-control required" value="<? echo (!empty($activitydateend) ? $activitydateend : '');?>" autocomplete="off">
      </div>
    </div>
    <? if($current_role == "super_admin" or $current_role == "admin" or $current_role == "accounts" or $current_role == "HR") { ?>
    <div class="col-md-3">
      <div class="form-group">
        <select name="state_id" id="state_id" class="form-control required">
          <option value=''>-Select State-</option>
          <? foreach($state_list as $states) { ?>
            <option value="<?=$states->id;?>" <?php echo (isset($state_id) && $state_id == $states->id ? 'selected' : ''); ?> ><?=$states->name;?></option>
          <?  } ?>  
        </select>
      </div>
    </div>
    <?  } ?>
    <? $time_filters = array("all"=>"All", "filter"=>"Less than 6 hour"); ?>
    <div class="form-group col-md-3">
      <select name="time_filter" id="time_filter" class="form-control">
        <? foreach($time_filters as $key=>$value) { ?>
          <option value="<?=$key?>" <? if($time_filter == $key){ echo 'selected'; }?> ><?=$value?></option>
        <?  } ?>  
      </select>
    </div>  
  </div>  
  <div class="row">
    <div class="col-md-4"></div>
    <div class="col-md-3">
      <div class="form-group">
        <input type="submit" name="submit" class="form-control btn btn-sm btn-success" value="Get training">
      </div>
    </div>
  </div>
</form>
<? } ?>

<? if(!isset($download) && !isset($pdf) && !empty($training_array))  { ?>
    <div class="form-group row">
      <div class="form-group col-md-3">
      <form name="frmreport" action="<?php echo base_url();?>activity/training_list" method="post">
          <input type="hidden" name="activitydatestart" value="<? echo (!empty($activitydatestart) ? $activitydatestart : '');?>">
          <input type="hidden" name="activitydateend" value="<? echo (!empty($activitydateend) ? $activitydateend : '');?>">
          <input type="hidden" name="state_id" value="<?=$state_id?>">
          <input type="hidden" name="time_filter" value="<?=$time_filter?>">
          <input type="hidden" name="download" value="1">
          <button type="submit" name="submit" value="Get training List" class="btn btn-success">Download Report</button>
      </form>
      </div>
      <div class="form-group col-md-3">
      <form name="frmreport" action="<?php echo base_url();?>activity/training_list" method="post">
          <input type="hidden" name="activitydatestart" value="<? echo (!empty($activitydatestart) ? $activitydatestart : '');?>">
          <input type="hidden" name="activitydateend" value="<? echo (!empty($activitydateend) ? $activitydateend : '');?>">
          <input type="hidden" name="state_id" value="<?=$state_id?>">
          <input type="hidden" name="time_filter" value="<?=$time_filter?>">
          <input type="hidden" name="pdf" value="1">
        <button type="submit" name="submit" value="Get training List" class="btn btn-success">Download PDF</button>
      </form>
      </div>
      <div class="col-md-6" align="right"><a href="<?=base_url()?>activity/training_list" style="float:right">Back</a></div>
    </div>
<? } ?>

<? if(!empty($training_array)) {?>
<div class="scrollme">
<table class="table table-striped table-bordered bootstrap-datatable datatable1 responsive customdatatable" width="100%">
	<thead>
		<tr style="background-color: #d5ecf9;">
			<th>S.No.</th>
			<th>Empolyee</th>
			<th>Date</th>
			<th>In Time</th>
			<th>Out Time</th>
			<th>Duration</th>
		</tr>
	</thead>
	<tbody>
    <?php $i=1; 
          foreach($training_array as $date=>$user_arr) {
            foreach($user_arr as $user_id=>$data_arr) { 
              if(!empty($data_arr['training-in'])) { 
                foreach($data_arr['training-in'] as $key=>$value) { 
                  
                  if($time_filter != 'all'){
                    
                    $in_time = strtotime($value);
                    $out_time = (isset($data_arr['training-out'][$key]) ? strtotime($data_arr['training-out'][$key]) : '');

                    $time_diff = 0;
                    if($out_time != '')
                    {
                       $time_diff = ($out_time-$in_time)/(60*60);
                    }
                    if($time_diff < 6){
                    ?>
                    <tr>
                      <td align="center"><?=$i?></td>
                      <td><?=$data_arr['login_name'][$key].' ('.$user_id.')'?></td>
                      <td><?=$date; ?></td>
                      <td><? echo date('H:i', strtotime($value)); ?></td>
                      <td><? echo (isset($data_arr['training-out'][$key]) ? date('H:i', strtotime($data_arr['training-out'][$key])) : '--');?></td>
                      <td><? echo (isset($data_arr['training_duration'][$key]) ? $data_arr['training_duration'][$key] : '--');?></td>
                    </tr>
                    <? $i++; 
                    }
                }else{
                  ?>
                  <tr>
                    <td align="center"><?=$i?></td>
                    <td><?=$data_arr['login_name'][$key].' ('.$user_id.')'?></td>
                    <td><?=$date; ?></td>
                    <td><? echo date('H:i', strtotime($value)); ?></td>
                    <td><? echo (isset($data_arr['training-out'][$key]) ? date('H:i', strtotime($data_arr['training-out'][$key])) : '--');?></td>
                    <td><? echo (isset($data_arr['training_duration'][$key]) ? $data_arr['training_duration'][$key] : '--');?></td>
                  </tr>
                <? $i++; 
                  
                }
              } 
            } 
          }   
        } 
      ?>  
  </tbody>
</table>  
</div>

<? } ?>

<? if(!isset($download) && !isset($pdf)) {?>

<script>
$(document).ready(function() {
    $("#frmtraining").validate();
});
</script>
<? } ?>
