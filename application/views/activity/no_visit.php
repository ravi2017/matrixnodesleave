<div class="row">
  <div class="col-md-12">
    <form method="post" action="<?php echo base_url();?>activity/no_visit" role="form" name="frmnovisit" id="frmnovisit">        
      <div class="row">
        <label class="col-md-2 offset-md-3" for="title">Date Of NO Visit</label><span class="err" id="err_title"></span>
        <div class="form-group col-md-3">
          <?php if($mode == 'edit'){ ?>
            <div class='input-group date'>
              <input type="text" name="no_visit_date" value="<?=date('Y-m-d', strtotime($activity_date));?>" readonly class="form-control required" id="no_visit_date" />
              <span class="input-group-addon">
                <span class="icon-calendar"></span>
              </span>
            </div>
          <?php } else { ?>  
            <div class='input-group date' id='datetimepicker1'>
              <input type="text" name="no_visit_date" value="" class="form-control required" id="no_visit_date" autocomplete="off" />
              <span class="input-group-addon">
                <span class="icon-calendar"></span>
              </span>
            </div>
          <?php } ?>
        </div>
      </div>
      <!--<div class="row">
        <label class="col-md-2 offset-md-3" for="title">Time Of No Visit</label><span class="err" id="err_title"></span>
        <div class="form-group col-md-3">
          <div class='input-group date' id='datetimepicker5'>
              <input type="text" name="no_visit_time" value="" class="form-control required" id="no_visit_time"  autocomplete="off" />
              <span class="input-group-addon">
                <span class="icon-clock"></span>
              </span>
          </div>
        </div>
      </div>-->
      <div class="row">
        <label class="col-md-2 offset-md-3" for="title">No Visit Reason</label><span class="err" id="err_title"></span>
        <div class="form-group col-md-3">
          <select name="reason" id="reason" class="form-control required">
            <option value="">-Select Reason-</option>  
            <option value="Local Authority Declared Holiday">Local Authority Declared Holiday</option>  
            <option value="Off Day During Training">Off Day During Training</option>  
            <option value="travel">Travel</option>  
            <option value="other">Other</option>  
          </select>
        </div>
      </div>

      <div class="row" id="other_reason_div" style="display:none;">
        <label class="col-md-2 offset-md-3" for="title">Time Of No Visit</label><span class="err" id="err_title"></span>
        <div class="form-group col-md-3">
          <textarea name="other_reason" value="" placeholder="Reason(Max 120 character)" class="form-control" row="2" col="" id="other_reason" maxlength="120"></textarea>
        </div>
      </div>
      
      <div class="form-group row">
        <input type="hidden" name="mode" id="mode" value="<?=$mode?>" />
        <input type="hidden" name="edit_id" value="<?=$edit_id?>" />
        <div class="col-md-6"><br>
          <a class="btn btn-sm btn-danger f-left" href="<?=base_url()?>activity">Cancel</a>
        </div>
        <div class="col-md-6"><br>
          <button type="submit" class="btn btn-sm btn-success f-right" name="Submit">Submit</button>
        </div>
      </div>
    </form>
  </div>
</div>

<script type="text/javascript">
  $(document).ready(function ()
  {
     //var date = new Date();
     //date.setDate(date.getDate() - 4);
     var date = new Date('2019-06-01');
                
      $('#datetimepicker1 #no_visit_date').datepicker({
        timepicker: false,
        autoclose:true,
        format: "m/d/Y",
        minDate:date,
        maxDate:new Date()
      });

      $('#datetimepicker5 #no_visit_time').datetimepicker({
        datepicker: false,
        format: "H:i"
      });
      
     $("#frmnovisit").validate();  

     $("#other_reason_div").hide();
     $("#other_reason").removeClass('required');   
  });

  $("#reason").change(function() {
    if($(this).val() == 'other'){
      $("#other_reason_div").show();
      $("#other_reason").addClass('required');
    }
    else{
      $("#other_reason_div").hide();
      $("#other_reason").removeClass('required');  
    }
  });
</script>
