<?php if(!isset($download) && !isset($pdf)) {?>
<div class="row">
	<?php if($current_group == 'head_office') { ?>	
	<div class="form-group col-md-3">
		<a href="<?php echo base_url();?>activity/add_attendance_regularization" class="btn btn-success">Add</a>
	</div>	
	<?php } ?>
	<? if($current_role == 'HR') { ?>	
	<div class="col-md-3">
		<a class="btn btn-sm btn-info" href="#" id="upload"> <i class="glyphicon glyphicon-upload icon-white"></i> Upload Attendance</a><br>
		<a href="<?php echo base_url(); ?>assets/admin/attendance.csv" onclick="return confirm('Do not make any changes in the first row of the downloaded file.')">Download Attendance Template</a>
	</div>
	<?	} ?>
</div>	
<form id="frmattendance" name="frmattendance" action="" method="post" onSubmit="return formvalidation();">
	<div class="row">
	 <? if(!empty($sparks) && ($current_role == 'super_admin' || $current_role == 'HR' || $current_role == 'state_person' || $current_role == 'manager')) { ?>	
	  <div class="form-group col-md-3">
		<select class="form-control" id="spark_id" name="spark_id">
		  <option value="">Select Spark</option>
		  <?php foreach($sparks as $spark) { ?>
		  <option value="<?=$spark->id?>" <?php echo ((isset($spark_id) && $spark_id == $spark->id) ? 'selected' : '');?>><?php echo $spark->name." (".$spark->login_id.")"?></option>
		  <?php } ?>
		</select>
	  </div>
	  <? }else{	?>
		  <input type="hidden" id="spark_id" name="spark_id" class="form-control" value="<?=$current_user_id?>">
	  <? } ?>	  
		<div class="form-group col-md-3">
		  <div class="form-group">
			<input type="text" id="attendancestart" name="attendancestart" class="form-control" value="<?=$attendancestart?>">
		  </div>
        </div>
        <div class="form-group col-md-3">
		  <div class="form-group">
			<input type="text" id="attendanceend" name="attendanceend" class="form-control" value="<?=$attendanceend?>">
		  </div>
        </div>
	  <div class="form-group col-md-3">
        <input type="submit" name="submit" value="Get List" class="btn btn-sm btn-info">
	  </div>
  </div>
</form>
<? } ?>

<table class="table table-striped table-bordered bootstrap-datatable datatable responsive customdatatable">
	<thead>
		<tr>
			<th>Spark</th>
			<th>Date</th>
			<th>Type</th>
			<th>Reason</th>
			<th>Current Status</th>
			<? if($current_role == 'HR' || $current_role == 'state_person' || $current_role == 'manager') { ?>
				<th>Action</th>
			<?	}	?>
		</tr>
	</thead>
	<tbody>
	
	<?php
	  if(!empty($attendancelist)){	
		foreach ($attendancelist as $e_key){ ?>
		<tr>
			<td><?php echo $sparkNames[$e_key->spark_id]; ?></td>
			<td><?php echo date('d-m-Y', strtotime($e_key->attendance_date)); ?></td>
			<td><?php echo $e_key->attendance_type; ?></td>
			<td><?php echo $e_key->reason; ?></td>
			<td id="status-<?=$e_key->id?>"><?php echo ucfirst($e_key->status); ?></td>
			<? if($current_role == 'HR' || $current_role == 'state_person' || $current_role == 'manager') {  
          $btn_reject = 'display:block';
          $btn_approve = 'display:block';
          $reject_reason = '';
          if($e_key->status == 'rejected'){ 
            $btn_reject = 'display:none';
            $btn_approve = 'display:block';
            $reject_reason = $e_key->rejected_reason;
          } 
          if($e_key->status == 'approved'){ 
            $btn_reject = 'display:block';
            $btn_approve = 'display:none';
          }
          ?>  
          <td style="vertical-align:top">
					 <? //if($e_key->status = ''){ ?>	
					 
          <div id="approved_<?=$e_key->id?>" style="<?=$btn_approve?>">
            <input type="button" name="approved_attendance" id="approved_attendance" class="btn btn-sm btn-success" value="Approve" onClick="return approve_attendance('<?=$e_key->id?>')"> 
          </div> <br />
          <div id="reject_<?=$e_key->id?>" style="<?=$btn_reject?>">
            <input type="button" name="reject_attendance" id="reject_attendance" onClick="return rejectattendance('<?=$e_key->id?>')" class="btn btn-sm btn-danger" value="Reject"> 
          </div>
						<?	//}else{ echo '&nbsp;'; }?>
          </td>
       <? }	?>	 
		</tr>
	<?php } } ?>
  </tbody>
</table>

<? if(!isset($download) && !isset($pdf)) {?>
	
<div class="modal fade" id="rejectModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4>Reject Attendance</h4>
        <button type="button" class="close" data-dismiss="modal">×</button>
      </div>
      <form method="post" action="" role="form" name="frmreject" id="frmreject">
        <input type="hidden" name="attendanceid" id="attendanceid" value="">
        <div class="modal-body">                      
          <div class="row">
            <div class="form-group col-md-12">
              <textarea name="rejected_reason" id="rejected_reason" placeholder="Reason to reject" class="form-control" maxlength="200"></textarea>
            </div>
          </div>
          <div class="row">  
            <div class="form-group col-md-4">&nbsp;</div>
            <div class="form-group col-md-6">
              <label for="brc_name">&nbsp;</label>
              <input type="button" id="submit" name="submit" value="Save" class="btn btn-success" onClick="return reject_attendance()">
            </div>  
          </div>  
        </div>  
      </form>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="uploadModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Upload Schools</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <form action="<?php echo base_url(); ?>admin/employees/upload" method="post" enctype="multipart/form-data">
            <div class="modal-body">
              <label>Upload CSV File</label>
              <input type="file" name="fileToUpload" id="fileToUpload" class="form-control">
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
              <input type="submit" class="btn btn-sm btn-primary" value="Upload" name="subupload">
            </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->	


<script>
$(document).ready(function(){
  $("#upload").click(function(){
    $("#uploadModal").modal();
  });
});
</script>

<script>

function formvalidation()
{
	var spark_id = $("#spark_id").val();
	var attendancestart = $("#attendancestart").val();
	var attendanceend = $("#attendanceend").val();
	
	if(spark_id == '' && attendancestart == '' && attendanceend == '')
	{
		alert('Either select spark or date range to get results.');
		return false;
	} 
}	
	
$(document).ready(function() {
   // $("#frmattendance").validate();
});

function rejectattendance(cid)
{
  $("#rejected_reason").val('');
  $("#attendanceid").val(cid);
  $("#rejectModel").modal("show");
}

function reject_attendance()
{
	var attendance_id = $("#attendanceid").val();
  var reason = $("#rejected_reason").val();
  
  if(reason == ''){
    alert('Please add reason');
    return false;
  }
  
  $.ajax({
      url: '<?=base_url()?>activity/update_regularization',
      type: 'post',
      data: {
        status_type: 'rejected',
        attendance_id: attendance_id,
        reason : reason
      },
      success: function(response) {
        $("#status-"+attendance_id).html('Rejected');
        $("#reject_"+attendance_id).hide();
        $("#approved_"+attendance_id).show();
        $("#rejectModel").modal("hide");
        $("#rreason_"+attendance_id).html(reason);
        $(".attendance_reject_"+attendance_id).val('1');
        $("#row_"+attendance_id).css("background-color", "#fd6252");
      },
      error: function(response) {
        window.console.log(response);
      }
    });
}

function approve_attendance(curid)
{
	if(confirm('Are you sure to approve')){
  
  var attendance_id = curid;
  $.ajax({
      url: '<?=base_url()?>activity/update_regularization',
      type: 'post',
      data: {
        status_type: 'approved',
        attendance_id: attendance_id
      },
      success: function(response) {
				$("#status-"+attendance_id).html('Approved');
				$("#reject_"+attendance_id).show();
        $("#approved_"+attendance_id).hide();
        $("#row_"+attendance_id).css("background-color", "#FFF");
      },
      error: function(response) {
        window.console.log(response);
      }
    });
	}
}

$( function() {
  var currentTime = new Date();
  var startDateTo = new Date(currentTime.getFullYear(),currentTime.getMonth() -2,1);
  
  var dateFormat = "dd-mm-yy",
    from = $( "#attendancestart" )
      .datepicker({
        dateFormat: "dd-mm-yy",
        //defaultDate: "+1M",
        changeMonth: true,
        numberOfMonths: 2,
        minDate: startDateTo,
        maxDate: "0D"
      })
      .on( "change", function() {
        to.datepicker( "option", "minDate", getDate( this ) );
        if($("#attendanceend").val() == ''){
					$("#attendanceend").val($(this).val());
				}
      }),
    to = $( "#attendanceend" ).datepicker({
      dateFormat: "dd-mm-yy",
      defaultDate: "+1M",
      changeMonth: true,
      numberOfMonths: 2,
      minDate: "-15D",
      maxDate: "0D"
    })
    .on( "change", function() {
      from.datepicker( "option", "maxDate", getDate( this ) );
      if($("#attendancestart").val() == ''){
					$("#attendancestart").val($(this).val());
				}
    });

  function getDate( element ) {
    var date;
    try {
      date = $.datepicker.parseDate( dateFormat, element.value );
    } catch( error ) {
      date = null;
    }
    return date;
  }
});
</script>
<? } ?>
