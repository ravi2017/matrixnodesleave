<div class="row">
  <div class="box col-md-6 col-md-offset-3">
    <div class="box-inner">
      <form method="post" action="<?php echo base_url();?>activity/call" role="form" name="frmcall" id="frmcall" onSubmit="return validatefrm()">
        <div class="box-header well" data-original-title="">
            <h2><i class="glyphicon glyphicon-th"></i> Call</h2>
            <span style="float:right"><a href="<?=base_url()?>activity">Spark Activity</a></span>
        </div>

        <div class="row">
          <div class="form-group col-md-10 col-md-offset-1">
            <label for="title">Contact Person Level</label>
            <input type="radio" name="source" id="level_d" class="source" value="DI"> District &nbsp;&nbsp;
            <input type="radio" name="source" id="level_b" class="source" value="BL"> Block &nbsp;&nbsp;
            <input type="radio" name="source" id="level_c" class="source" value="CL" checked="checked"> Cluster
          </div>
        </div>
        
        <div class="row">
          <div class="form-group col-md-5 col-md-offset-1">
            <label for="date">Date Of Call</label><span class="err" id="err_date"></span>
            <?php if($mode == 'edit'){ ?>
              <div class='input-group date'>
                <input type="text" name="call_date" value="<?=date('Y-m-d', strtotime($activity_date));?>" readonly class="form-control required" id="call_date" />
            <?php } else { ?>  
              <div class='input-group date' id='datetimepicker1'>
                  <input type="text" name="call_date" value="" class="form-control required" id="call_date" autocomplete="off" />
            <?php } ?>
                  <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                  </span>
              </div>
          </div>
          <div class="form-group col-md-5 col-md-offset-1">
            <label for="date">Time Of Call</label><span class="err" id="err_date"></span>
            <div class='input-group date' id='datetimepicker5'>
                <input type="text" name="call_time" value="" class="form-control required" id="call_time"  autocomplete="off" />
                <span class="input-group-addon">
                  <span class="glyphicon glyphicon-calendar"></span>
                </span>
            </div>
          </div>
        </div>

         <div class="row">
          <div class="form-group col-md-10 col-md-offset-1">
            <label for="district name">District</label><span class="err" id="err_district"></span>
            <select id="district_id" name="district_id" class="form-control required">
              <option value="">-Select District-</option>
              <?php foreach($user_districts as $user_district){ ?>
                <option value="<?php echo $user_district->id;?>"><?php echo $user_district->name;?></option>
              <? } ?>  
            </select>
          </div>
        </div>

        <div class="row" id="block_div">
          <div class="form-group col-md-10 col-md-offset-1">
            <label for="block">Block</label><span class="err" id="err_block"></span>
            <select id="block_id" name="block_id" class="form-control required">
              <option value="">-Select Block-</option>  
            </select>
          </div>
        </div>

        <div class="row" id="cluster_div">
          <div class="form-group col-md-10 col-md-offset-1">
            <label for="cluster">Cluster</label><span class="err" id="err_cluster"></span>
            <select id="cluster_id" name="cluster_id" class="form-control required" >
              <option value="">-Select Cluster-</option>  
            </select>
          </div>
        </div>

        <div class="row">
          <div class="form-group col-md-10 col-md-offset-1">
            <label for="contact">Contact Person</label><span class="err" id="err_cperson"></span>
            <select id="contact_person" class="form-control required" name="contact_person">
              <option value="">-Select Person-</option>  
              <option value="other">Other</option>  
            </select>
          </div>
        </div>

        <div id="contact_div" style="display:none">
          <div class="row">
            <div class="form-group col-md-10 col-md-offset-1">
              <label for="designation">Designation</label><span class="err" id="err_designation"></span>
              <input type="text" id="designation" name="designation" class="form-control cdetails">
            </div>
          </div>
          <div class="row">
            <div class="form-group col-md-10 col-md-offset-1">
              <label for="firstname">First Name</label><span class="err" id="err_fname"></span>
              <input type="text" id="firstname" name="firstname" class="form-control cdetails">
            </div>
          </div>
          <div class="row">
            <div class="form-group col-md-10 col-md-offset-1">
              <label for="lastname">Last Name</label><span class="err" id="err_lname"></span>
              <input type="text" id="lastname" name="lastname" class="form-control cdetails">
            </div>
          </div>
           <div class="row">
            <div class="form-group col-md-10 col-md-offset-1">
              <label for="mobile">Mobile</label><span class="err" id="err_mobile"></span>
              <input type="text" id="mobile" name="mobile" class="form-control cdetails">
            </div>
          </div>
        </div>
        
        <div class="row">
          <div class="form-group col-md-10 col-md-offset-1 text-center">
            <input type="hidden" name="mode" id="mode" value="<?=$mode?>" />
            <input type="hidden" name="edit_id" value="<?=$edit_id?>" />
            <a href="<?=base_url()?>activity">Cancel</a>
            <button type="submit" class="btn btn-default" name="Submit">Submit</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>

<script type="text/javascript" src="<? echo base_url(); ?>assets/v2/js/activity.js"></script>
<script type="text/javascript">
  $(document).ready(function ()
  {
     var date = new Date();
     date.setDate(date.getDate() - 4);
                
      $('#datetimepicker1 #call_date').datetimepicker({
        timepicker: false,
        format: "m/d/Y",
        minDate:date,
        maxDate:new Date()
      });

      $('#datetimepicker5 #call_time').datetimepicker({
        datepicker: false,
        format: "H:i"
      });
     //$("#frmcall").validate();  
  });  
   
  $(".source").change(function() {
    var source = $(this).val();
    
    if(source == 'DI'){
      $("#block_div").hide();  
      $("#cluster_div").hide();
      //$("#block_id").removeClass('required');
      //$("#cluster_id").removeClass('required');
    }
    else if(source == 'BL'){
      $("#block_div").show();  
      $("#cluster_div").hide();
      //$("#block_id").addClass('required');
      //$("#cluster_id").removeClass('required');
    }
    else{
      $("#block_div").show();  
      $("#cluster_div").show();
      //$("#block_id").addClass('required');
      //$("#cluster_id").addClass('required');
    }
  });

function validatefrm()
{
  var source = $('input:radio[name="source"]:checked').val();
  //alert(source);
  //return false;
  $("#err_date").html('');
  $("#err_district").html('');
  $("#err_block").html('');
  $("#err_cluster").html('');
  $("#err_cperson").html('');
  $("#err_fname").html('');
  $("#err_lname").html('');
  $("#err_designation").html('');
  $("#err_mobile").html('');

  var error = 0;
  if($("#call_date").val() == ''){
    $("#err_date").html('Please select meeting date.');
    error = 1;
  }

  if($("#district_id").val() == ''){
    $("#err_district").html('Please select district.');
    error = 1;
  }

  if(source != 'DI' && $("#block_id").val() == ''){
    $("#err_block").html('Please select block.');
    error = 1;
  }
  
  if(source != 'DI' && source != 'BL' && $("#cluster_id").val() == ''){
    $("#err_cluster").html('Please select cluster.');
    error = 1;
  }
  
  if($("#contact_person").val() == ''){
    $("#err_cperson").html('Please select contact person.');
    error = 1;
  }

  if($("#contact_person").val() == 'other'){
    if($("#designation").val() == ''){
      $("#err_designation").html('Please enter designation.');
      error = 1;
    }  
    if($("#firstname").val() == ''){
      $("#err_fname").html('Please enter first name.');
      error = 1;
    }  
    if($("#lastname").val() == ''){
      $("#err_lname").html('Please enter last name.');
      error = 1;
    }
    if($("#mobile").val() == ''){
      $("#err_mobile").html('Please enter mobile.');
      error = 1;
    }
  }  

  if(error == 1){
    return false;
  }
}  

    
  $("#district_id").change(function() {
    var source = $('input:radio[name="source"]:checked').val();
    activity_block_list($(this).val(),'');
    if(source == 'DI')
    {
      get_contact_list();
    }
  });

  $("#block_id").change(function() {
    var source = $('input:radio[name="source"]:checked').val();
    activity_cluster_list($(this).val(),'');
    if(source == 'BL')
    {
      get_contact_list();
    }
  });
  
  $("#cluster_id").change(function() {
    var source = $('input:radio[name="source"]:checked').val();
    if(source == 'CL')
    {
      get_contact_list();
    }
  });

  function get_contact_list()
  {
    var district_id = $("#district_id").val();
    var block_id = $("#block_id").val();
    var cluster_id = $("#cluster_id").val();
    var source = $('input:radio[name="source"]:checked').val();

    $.ajax({
      url: BASE_URL+'activity/get_call_contact_list',
      type: 'post',
      data: {
        did: district_id, bid : block_id, cid : cluster_id, source : source
      },
      dataType: 'json',
      success: function(response) {
        $("#contact_person").html("<option value=''>-Select Person-</option>");
        $.each(response, function() {
            $("#contact_person").append("<option value='" + this['id'] + "'>" + this['name'] + " </option>");
        });
        $("#contact_person").append("<option value='other'>Other</option>");
        //$("#contact_person").show();
      },
      error: function(response) {
        window.console.log(response);
      }
    });
  }
  
  $("#contact_person").change(function() {
    if($(this).val() == 'other')
    {
      $("#contact_div").show();
      //$(".cdetails").addClass('required');
    }
    else{
      $("#contact_div").hide();
      //$(".cdetails").removeClass('required');
    }
  });
</script>
