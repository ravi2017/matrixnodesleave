<?
error_reporting(0);
    ini_set('display_errors', 'Off');
?>

<? if(!isset($download) && !isset($pdf)) {?>

<div class="box col-md-12">
  <div class="box-inner">
    <form name="frmreport" action="" method="post" onsubmit="return validatereport()">
        <div class="row">
          <?php if($current_role == 'super_admin' || $current_role == 'accounts' || $current_role == 'reports') { ?>
          <div class="form-group col-md-3 field_user">
            <select class="form-control" id="state_id" name="state_id">
              <option value="">Select State</option>
              <? foreach($states as $s) { ?>
              <option value="<?=$s->id?>" <?=(isset($state_id) and $s->id == $state_id) ? "selected" : ""?>><?=$s->name?></option>
              <? } ?>
            </select>
          </div>
          <?  }else{  ?>
            <input type="hidden" name="state_id" id="state_id" value="<?=$user_state_id; ?>">
          <? } ?>  
          <div class="form-group col-md-2 field_user">
            <select class="form-control" id="monthno" name="monthno">
              <option value="">Month</option>
                <? foreach($month_arr as $key=>$value) { ?>
                  <option value="<?=$key?>" <? if(isset($month) && $month == $key){ echo 'selected'; }?>><?=$value?></option>
                <? } ?>
            </select>
          </div>
          <div class="form-group col-md-2 field_user">
            <select class="form-control" id="year" name="year">
              <option value="">Year</option>
                <? foreach($year_arr as $year_val) { ?>
                  <option value="<?=$year_val?>" <? if(isset($year) && $year == $year_val){ echo 'selected'; }?>><?=$year_val?></option>
                <? } ?>
            </select>
          </div>
          <div class="form-group col-md-2 field_user">
            <select class="form-control" id="duration" name="duration">
              <option value="">Month/Year</option>
              <option value="month_value" <? if($duration == 'month_value') { echo 'selected'; } ?>>Month</option>
              <option value="year_value" <? if($duration == 'year_value') { echo 'selected'; } ?>>Year</option>
            </select>
          </div>
          <div class="form-group col-md-1 col-md-offset-1 text-center">
            <input type="submit" name="submit" value="Get Report" class="btn btn-sm btn-success">
          </div>
      </div>
    </form>
    
    <div class="form-group row">
      <div class="form-group col-md-3">
      <form name="frmreport" action="<?php echo base_url();?>activity/sparks_data" method="post">
          <input type="hidden" name="monthno" value="<? echo (!empty($month) ? $month : '');?>">
          <input type="hidden" name="year" value="<? echo (!empty($year) ? $year : '');?>">
          <input type="hidden" name="state_id" value="<?=$state_id?>">
          <input type="hidden" name="duration" value="<?=$duration?>">
          <input type="hidden" name="download" value="1">
          <button type="submit" name="submit" value="Get Report" class="btn btn-success">Download Report</button>
      </form>
      </div>
      <div class="form-group col-md-3">
      <form name="frmreport" action="<?php echo base_url();?>activity/sparks_data" method="post">
          <input type="hidden" name="monthno" value="<? echo (!empty($month) ? $month : '');?>">
          <input type="hidden" name="year" value="<? echo (!empty($year) ? $year : '');?>">
          <input type="hidden" name="state_id" value="<?=$state_id?>">
          <input type="hidden" name="duration" value="<?=$duration?>">
          <input type="hidden" name="pdf" value="1">
        <button type="submit" name="submit" value="Get Report" class="btn btn-success">Download PDF</button>
      </form>
      </div>
    </div>
    
    <div class="form-group">
    <form id="dasboard_data" action="<?=base_url();?>dashboard/state_dashboard" method="post" target='_blank'>
        <input type="hidden" name="start_month" value="<? echo (!empty($month) ? $month : '');?>">
        <input type="hidden" name="start_year" value="<? echo (!empty($year) ? $year : '');?>">
        <? if($current_role == 'super_admin' || $current_role == 'accounts' || $current_role == 'reports') { ?>
        <input type="hidden" name="spark_state_id" value="<?=$state_id?>">
        <input type="hidden" id="spark_id" name="spark_id" value=''>
        <?  }else if($current_role == 'state_person' || $current_role == 'manager') { ?>
          <input type="hidden" name="spark_state_id" value="<?=$state_id?>">
          <input type="hidden" id="spark_id" name="sel_spark" value=''>
        <?  } ?>
        <input type="hidden" name="dashboard_summary" value="1">
    </form>
    
  </div>
</div>

<? } ?>

<div class="scrollme">
  <? if(!isset($download) && !isset($pdf)) {?>
  <table class="table table-striped table-bordered bootstrap-datatable datatable1 responsive customdatatable" width="100%">
  <? }else{ ?>
    <table cellspacing=0 cellpadding=0 border=1>
  <?  } ?>  
	<thead>
    <tr style="background-color: #d5ecf9;">
      <th rowspan=2>Spark</th>
      <? if($duration == '') { ?><th rowspan=2>Duration</th> <? } ?>
      <th colspan=2>Productivity</th>
			<th colspan=2>Training</th>
			<th colspan=2>Feedback</th>
			<th colspan=2>Non<br>Compliance</th>
			<th rowspan=2>ACT<br>Days</th>
      <th rowspan=2>No-ACT<br>DAYS</th>
      <th rowspan=2>UNPRD<br>DAYS</th>
      <th rowspan=2>OFF<br>DAYS</th>
      <th rowspan=2>KM/Visit</th>
      <th rowspan=2>Total<br/>Cost</th>
      <th colspan=2>National</th>
			<th colspan=2>State</th>
      
		</tr>
		<tr style="background-color: #d5ecf9;">
      <th title="Score">S</th><th title="Rating">R</th>
			<th title="Score">S</th><th title="Rating">R</th>
			<th title="Score">S</th><th title="Rating">R</th>
			<th title="Score">S</th><th title="Rating">R</th>
			<th>Rank</th><th>Rating</th>
			<th>Rank</th><th>Rating</th>
		</tr>
	</thead>
  <tbody>
    <?php $i=1; 
    if(!empty($data_arr)){
      
      if(!empty($state_data_arr)){
        foreach($state_data_arr as $state_name=>$data) { ?>
          <? if($duration == '' || $duration == 'month_value') { 
             
             $Productivity_data =  return_rating_val_summary($data['month_value'][$sfield_level_ids['Productivity Rating']], $data['month_value'][$sfield_level_ids['Productivity Score']]);
             $Training_data =  return_rating_val_summary($data['month_value'][$sfield_level_ids['Training Rating']], $data['month_value'][$sfield_level_ids['Training Score']]);
             $Feedback_data =  return_rating_val_summary($data['month_value'][$sfield_level_ids['Feedback Rating']], $data['month_value'][$sfield_level_ids['Feedback Score']]);
             $Mistake_data =  return_rating_val_summary($data['month_value'][$sfield_level_ids['Mistake Rating']], $data['month_value'][$sfield_level_ids['Mistake Score']]);
            
            $avg_rank  = round(($data['month_value'][$sfield_level_ids['Productivity Rank']]+$data['month_value'][$sfield_level_ids['Training Rank']]+$data['month_value'][$sfield_level_ids['Feedback Rank']]+$data['month_value'][$sfield_level_ids['Mistake Rank']])/4);
                  
            $avg_score =  round(($data['month_value'][$sfield_level_ids['Productivity Score']]+$data['month_value'][$sfield_level_ids['Training Score']]+$data['month_value'][$sfield_level_ids['Feedback Score']]+$data['month_value'][$sfield_level_ids['Mistake Score']])/4);
                          
            $avg_rating =  ($data['month_value'][$sfield_level_ids['Productivity Rating']]+$data['month_value'][$sfield_level_ids['Training Rating']]+$data['month_value'][$sfield_level_ids['Feedback Rating']]+$data['month_value'][$sfield_level_ids['Mistake Rating']])/4;
            
            $NOR_ranking = check_ranking($data['month_value'][$sfield_level_ids['NOR']]);
            //$NOR_data =  return_rating_val_summary($avg_rating, $avg_score);
            $NOR_data =  return_rating_val_summary($data['month_value'][$sfield_level_ids['Nationl Rating']], $data['month_value'][$sfield_level_ids['NOR']]);
            
            ?>
          <tr style="background-color: #D6D6D6;vertical-align:middle">
            <td <? if($duration == '') { ?> rowspan=2 <? } ?>><?=$state_name;?></td>
            <? if($duration == '') { ?> <td>Month</td> <? } ?>
            <td class="<?php echo $Productivity_data['color'];?>"><?php echo $data['month_value'][$sfield_level_ids['Productivity Score']]?></td>
            <td ><?php echo $Productivity_data['value'];?></td>
            
            <td class="<?php echo $Training_data['color'];?>"><?php echo (int) $data['month_value'][$sfield_level_ids['Training Score']]?></td>
            <td ><?php echo $Training_data['value'];?></td>
            
            <td class=" <?php echo $Feedback_data['color'];?>"><?php echo $data['month_value'][$sfield_level_ids['Feedback Score']]?></td>
            <td ><?php echo $Feedback_data['value'];?></td>
            
            <td class=" <?php echo $Mistake_data['color'];?>"><?php echo (int) $data['month_value'][$sfield_level_ids['Mistake Score']]?></td>
            <td ><?php echo $Mistake_data['value'];?></td>
            
            <td ><?php echo round($data['month_value'][$sfield_level_ids['PRODUCTIVE DAYS']]/$spark_count,1);?></td>
            <td ><?php echo round($data['month_value'][$sfield_level_ids['NO ACTIVITY DAYS']]/$spark_count,1);?></td>
            <td ><?php echo round($data['month_value'][$sfield_level_ids['UNPRODUCTIVE DAYS']]/$spark_count,1);?></td>
            <td ><?php echo round($data['month_value'][$sfield_level_ids['OFF DAYS']]/$spark_count,1);?></td>
            <td ><?php echo round($data['month_value'][$sfield_level_ids['KM/Visit']]/$spark_count,1);?></td>
            <td ><?php echo round($data['month_value'][$sfield_level_ids['Total Conveyance cost']]/$spark_count,1);?></td>
            
            <td class="<?php echo $NOR_data['color'];?>"><?php echo $NOR_ranking; //check_ranking($avg_rank); ?></td>
            <td><?php echo $NOR_data['value'];?></td>
            <td>--</td>
            <td >--</td>
            
            
          </tr>
          <?  } ?>
          <? if($duration == '' || $duration == 'year_value') { 
            
            $NOR_data =  return_rating_val_summary($data['year_value'][$sfield_level_ids['Nationl Rating']], $data['year_value'][$sfield_level_ids['NOR']]);
            $Productivity_data =  return_rating_val_summary($data['year_value'][$sfield_level_ids['Productivity Rating']], $data['year_value'][$sfield_level_ids['Productivity Score']]);
            $Training_data =  return_rating_val_summary($data['year_value'][$sfield_level_ids['Training Rating']], $data['year_value'][$sfield_level_ids['Training Score']]);
            $Feedback_data =  return_rating_val_summary($data['year_value'][$sfield_level_ids['Feedback Rating']], $data['year_value'][$sfield_level_ids['Feedback Score']]);
            $Mistake_data =  return_rating_val_summary($data['year_value'][$sfield_level_ids['Mistake Rating']], $data['year_value'][$sfield_level_ids['Mistake Score']]);
            
            $avg_rank  = round(($data['year_value'][$sfield_level_ids['Productivity Rank']]+$data['year_value'][$sfield_level_ids['Training Rank']]+$data['year_value'][$sfield_level_ids['Feedback Rank']]+$data['year_value'][$sfield_level_ids['Mistake Rank']])/4);
                  
            $avg_score =  round(($data['year_value'][$sfield_level_ids['Productivity Score']]+$data['year_value'][$sfield_level_ids['Training Score']]+$data['year_value'][$sfield_level_ids['Feedback Score']]+$data['year_value'][$sfield_level_ids['Mistake Score']])/4);
                          
            $avg_rating =  ($data['year_value'][$sfield_level_ids['Productivity Rating']]+$data['year_value'][$sfield_level_ids['Training Rating']]+$data['year_value'][$sfield_level_ids['Feedback Rating']]+$data['year_value'][$sfield_level_ids['Mistake Rating']])/4;
            
            $NOR_ranking = check_ranking($data['year_value'][$sfield_level_ids['NOR']]);
            $NOR_data =  return_rating_val_summary($data['year_value'][$sfield_level_ids['Nationl Rating']], $data['year_value'][$sfield_level_ids['NOR']]);              
            
            //$NOR_data =  return_rating_val_summary($avg_rating, $avg_score);
            
          ?>
          <tr style="background-color: #D6D6D6;vertical-align:middle">
            <? if($duration != '') { ?>
            <td><?=$state_name;?></td>
            <? }else{ ?>
            <td>Year</td>
            <?  } ?>
            <td class="<?php echo $Productivity_data['color'];?>"><?php echo $data['year_value'][$sfield_level_ids['Productivity Score']]?></td>
            <td ><?php echo $Productivity_data['value'];?></td>
            
            <td class="<?php echo $Training_data['color'];?>"><?php echo (int) $data['year_value'][$sfield_level_ids['Training Score']]?></td>
            <td ><?php echo $Training_data['value'];?></td>
            
            <td class="<?php echo $Feedback_data['color'];?>"><?php echo $data['year_value'][$sfield_level_ids['Feedback Score']]?></td>
            <td ><?php echo $Feedback_data['value'];?></td>
            
            <td class="<?php echo $Mistake_data['color'];?>"><?php echo (int) $data['month_value'][$sfield_level_ids['Mistake Score']]?></td>
            <td ><?php echo $Mistake_data['value'];?></td>
            
            <td ><?php echo round($data['year_value'][$sfield_level_ids['PRODUCTIVE DAYS']]/$spark_count,1);?></td>
            <td ><?php echo round($data['year_value'][$sfield_level_ids['NO ACTIVITY DAYS']]/$spark_count,1);?></td>
            <td ><?php echo round($data['year_value'][$sfield_level_ids['UNPRODUCTIVE DAYS']]/$spark_count,1);?></td>
            <td ><?php echo round($data['year_value'][$sfield_level_ids['OFF DAYS']]/$spark_count,1);?></td>
            <td ><?php echo round($data['year_value'][$sfield_level_ids['KM/Visit']]/$spark_count,1)?></td>
            <td ><?php echo round($data['year_value'][$sfield_level_ids['Total Conveyance cost']]/$spark_count,1)?></td>
            
            <td class=" <?php echo $NOR_data['color'];?>"><?php echo check_ranking($avg_rank); ?></td>
            <td ><?php echo $NOR_data['value'];?></td>
            <td >--</td>
            <td >--</td>
            
          </tr>
          <?  } ?>
          <?
         $i++; 
        }
      }
      
      foreach($data_arr as $sparks_id=>$data) { ?>
        <? if($duration == '' || $duration == 'month_value') { 
           
          //$NOR_data =  return_rating_val_summary($data['month_value'][$field_level_ids['Nationl Rating']], $data['month_value'][$field_level_ids['NOR']]);
          $SOR_data =  return_rating_val_summary($data['month_value'][$field_level_ids['State Rating']], $data['month_value'][$field_level_ids['SOR']]);
          $Productivity_data =  return_rating_val_summary($data['month_value'][$field_level_ids['Productivity Rating']], $data['month_value'][$field_level_ids['Productivity Score']]);
          $Training_data =  return_rating_val_summary($data['month_value'][$field_level_ids['Training Rating']], $data['month_value'][$field_level_ids['Training Score']]);
          $Feedback_data =  return_rating_val_summary($data['month_value'][$field_level_ids['Feedback Rating']], $data['month_value'][$field_level_ids['Feedback Score']]);
          $Mistake_data =  return_rating_val_summary($data['month_value'][$field_level_ids['Mistake Rating']], $data['month_value'][$field_level_ids['Mistake Score']]); 
          
          $avg_rank  = round(($data['month_value'][$field_level_ids['Productivity Rank']]+$data['month_value'][$field_level_ids['Training Rank']]+$data['month_value'][$field_level_ids['Feedback Rank']]+$data['month_value'][$field_level_ids['Mistake Rank']])/4);
                  
          $avg_score =  round(($data['month_value'][$field_level_ids['Productivity Score']]+$data['month_value'][$field_level_ids['Training Score']]+$data['month_value'][$field_level_ids['Feedback Score']]+$data['month_value'][$field_level_ids['Mistake Score']])/4);
                        
          $avg_rating =  ($data['month_value'][$field_level_ids['Productivity Rating']]+$data['month_value'][$field_level_ids['Training Rating']]+$data['month_value'][$field_level_ids['Feedback Rating']]+$data['month_value'][$field_level_ids['Mistake Rating']])/4;
           
          $NOR_ranking = check_ranking($data['month_value'][$field_level_ids['NOR']]);
          $NOR_data =  return_rating_val_summary($data['month_value'][$field_level_ids['Nationl Rating']], $data['month_value'][$field_level_ids['NOR']]);                            
          //$NOR_data =  return_rating_val_summary($avg_rating, $avg_score);
          
          $month_color = (int) $data['month_value'][$field_level_ids['SPARK COLOR']];
          $index_month_color = return_index_color($month_color);
          
          $td_bg_color = ($index_month_color != '' ? "style='$index_month_color'" : '');
        ?>
        <tr>
          <td <? if($duration == '') { ?> rowspan=2 <? } ?> style="background-color: #FFF;vertical-align:middle"><a href="javascript:void(0);" onClick="dashboard_data('<?=$sparks_id?>');"><?=$sparks_name[$sparks_id]; //$INDEX_COLOR[$index_month_color];?></a></td>
          <? if($duration == '') { ?> <td>Month</td> <? } ?>
          <td class="1 <?php echo $Productivity_data['color'];?>" <?=$td_bg_color;?>><?php echo $data['month_value'][$field_level_ids['Productivity Score']]?></td>
          <td <?=$td_bg_color;?>><?php echo $Productivity_data['value'];?></td>
          
          <td class="1 <?php echo $Training_data['color'];?>" <?=$td_bg_color;?>><?php echo (int) $data['month_value'][$field_level_ids['Training Score']]?></td>
          <td <?=$td_bg_color;?>><?php echo $Training_data['value'];?></td>
          
          <td class=" <?php echo $Feedback_data['color'];?>" <?=$td_bg_color;?>><?php echo $data['month_value'][$field_level_ids['Feedback Score']]?></td>
          <td <?=$td_bg_color;?>><?php echo $Feedback_data['value'];?></td>
          
          <td class=" <?php echo $Mistake_data['color'];?>" <?=$td_bg_color;?>><?php echo (int) $data['month_value'][$field_level_ids['Mistake Score']]?></td>
          <td <?=$td_bg_color;?>><?php echo $Mistake_data['value'];?></td>
          
          <td <?=$td_bg_color;?>><?php echo (int) $data['month_value'][$field_level_ids['PRODUCTIVE DAYS']]?></td>
          <td <?=$td_bg_color;?>><?php echo (int) $data['month_value'][$field_level_ids['NO ACTIVITY DAYS']]?></td>
          <td <?=$td_bg_color;?>><?php echo (int) $data['month_value'][$field_level_ids['UNPRODUCTIVE DAYS']]?></td>
          <td <?=$td_bg_color;?>><?php echo (int) $data['month_value'][$field_level_ids['OFF DAYS']]?></td>
          <td <?=$td_bg_color;?>><?php echo (int) $data['month_value'][$field_level_ids['KM/Visit']]?></td>
          <td <?=$td_bg_color;?>><?php echo (int) $data['month_value'][$field_level_ids['Total Conveyance cost']]?></td>
          
          <td class=" <?php echo $NOR_data['color'];?>" <?=$td_bg_color;?>><?php echo $NOR_ranking; //check_ranking($avg_rank); ?></td>
          <td <?=$td_bg_color;?>><?php echo $NOR_data['value'];?></td>
          <td class="<?php echo $SOR_data['color'];?>" <?=$td_bg_color;?>><?php echo check_ranking($data['month_value'][$field_level_ids['SOR']]);?></td>
          <td <?=$td_bg_color;?>><?php echo $SOR_data['value'];?></td>

        </tr>
        <?  } ?>
        <? if($duration == '' || $duration == 'year_value') { 
            
          $NOR_data =  return_rating_val_summary($data['year_value'][$field_level_ids['Nationl Rating']], $data['year_value'][$field_level_ids['NOR']]);
          $SOR_data =  return_rating_val_summary($data['year_value'][$field_level_ids['State Rating']], $data['year_value'][$field_level_ids['SOR']]);
          $Productivity_data =  return_rating_val_summary($data['year_value'][$field_level_ids['Productivity Rating']], $data['year_value'][$field_level_ids['Productivity Score']]);
          $Training_data =  return_rating_val_summary($data['year_value'][$field_level_ids['Training Rating']], $data['year_value'][$field_level_ids['Training Score']]);
          $Feedback_data =  return_rating_val_summary($data['year_value'][$field_level_ids['Feedback Rating']], $data['year_value'][$field_level_ids['Feedback Score']]);
          $Mistake_data =  return_rating_val_summary($data['year_value'][$field_level_ids['Mistake Rating']], $data['year_value'][$field_level_ids['Mistake Score']]); 
          
          $avg_rank  = round(($data['year_value'][$field_level_ids['Productivity Rank']]+$data['year_value'][$field_level_ids['Training Rank']]+$data['year_value'][$field_level_ids['Feedback Rank']]+$data['year_value'][$field_level_ids['Mistake Rank']])/4);
                  
          $avg_score =  round(($data['year_value'][$field_level_ids['Productivity Score']]+$data['year_value'][$field_level_ids['Training Score']]+$data['year_value'][$field_level_ids['Feedback Score']]+$data['year_value'][$field_level_ids['Mistake Score']])/4);
                        
          $avg_rating =  ($data['year_value'][$field_level_ids['Productivity Rating']]+$data['year_value'][$field_level_ids['Training Rating']]+$data['year_value'][$field_level_ids['Feedback Rating']]+$data['year_value'][$field_level_ids['Mistake Rating']])/4;
                        
          $NOR_ranking = check_ranking($data['year_value'][$field_level_ids['NOR']]);
          $NOR_data =  return_rating_val_summary($data['year_value'][$field_level_ids['Nationl Rating']], $data['year_value'][$field_level_ids['NOR']]);                            
          //$NOR_data =  return_rating_val_summary($avg_rating, $avg_score);
          
          $yr_color = (int) $data['year_value'][$field_level_ids['SPARK COLOR']];
          $index_year_color = return_index_color($yr_color);
          
          $td_bg_color = ($index_month_color != '' ? "style='$index_year_color'" : '');
        ?>
        <tr>
          <? if($duration != '') { ?>
          <td style="background-color: #FFF;vertical-align:middle"><?=$sparks_name[$sparks_id];?></td>
          <? }else{ ?>
          <td>Year</td>
          <?  } ?>
          <td class="<?php echo $Productivity_data['color'];?>" <?=$td_bg_color;?>><?php echo $data['year_value'][$field_level_ids['Productivity Score']]?></td>
          <td <?=$td_bg_color;?>><?php echo $Productivity_data['value'];?></td>
          
          <td class="<?php echo $Training_data['color'];?>" <?=$td_bg_color;?>><?php echo (int) $data['year_value'][$field_level_ids['Training Score']]?></td>
          <td <?=$td_bg_color;?>><?php echo $Training_data['value'];?></td>
          
          <td class="<?php echo $Feedback_data['color'];?>" <?=$td_bg_color;?>><?php echo $data['year_value'][$field_level_ids['Feedback Score']]?></td>
          <td <?=$td_bg_color;?>><?php echo $Feedback_data['value'];?></td>
          
          <td class="<?php echo $Mistake_data['color'];?>" <?=$td_bg_color;?>><?php echo (int) $data['year_value'][$field_level_ids['Mistake Score']]?></td>
          <td <?=$td_bg_color;?>><?php echo $Mistake_data['value'];?></td>
          
          <td <?=$td_bg_color;?>><?php echo (int) $data['year_value'][$field_level_ids['PRODUCTIVE DAYS']]?></td>
          <td <?=$td_bg_color;?>><?php echo (int) $data['year_value'][$field_level_ids['NO ACTIVITY DAYS']]?></td>
          <td <?=$td_bg_color;?>><?php echo (int) $data['year_value'][$field_level_ids['UNPRODUCTIVE DAYS']]?></td>
          <td <?=$td_bg_color;?>><?php echo (int) $data['year_value'][$field_level_ids['OFF DAYS']]?></td>
          <td <?=$td_bg_color;?>><?php echo (int) $data['year_value'][$field_level_ids['KM/Visit']]?></td>
          <td <?=$td_bg_color;?>><?php echo (int) $data['year_value'][$field_level_ids['Total Conveyance cost']]?></td>
          
          <td class="<?php echo $NOR_data['color'];?>" <?=$td_bg_color;?>><?php echo $NOR_ranking; //check_ranking($avg_rank);?></td>
          <td <?=$td_bg_color;?>><?php echo $NOR_data['value'];?></td>
          <td class="<?php echo $SOR_data['color'];?>" <?=$td_bg_color;?>><?php echo check_ranking($data['year_value'][$field_level_ids['SOR']]);?></td>
          <td <?=$td_bg_color;?>><?php echo $SOR_data['value'];?></td>

        </tr>
        <?  } ?>
        <?
       $i++; 
      }
    }else{
      ?>
      <tr><td colspan="19" align="center">No Record Found</td></tr>
      <?
    }
    ?>  
  </tbody>  
</table>  
</div>

<script type="text/javascript">
  function validatereport()
  {
    if ($("#state_id").val() == "")
    {
      alert("Please select state")
      return false;
    }
    if ($("#monthno").val() == "")
    {
      alert("Please select month")
      return false;
    }
      if ($("#year").val() == "")
    {
      alert("Please select year")
      return false;
    }

    return true
  }    
  
  function dashboard_data(sid)
  {
    $("#spark_id").val(sid);
    $("#dasboard_data").submit();
  }
</script>
<style>
tr th{
  text-align:center;
  vertical-align:top;
}
.light-grey{
  background-color:#EFEFEF;
}
.score-red{
  background-color:red;
}
</style>
