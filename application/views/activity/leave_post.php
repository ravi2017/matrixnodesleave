<div class="row">
  <div class="col-md-12">
    <form method="post" action="<?php echo base_url();?>activity/leave_request" role="form" name="frmleave" id="frmleave" onSubmit="return validatefrm()">
      <div class="row">
        <label class="col-md-2 offset-md-3" for="title">Leave Type</label><span class="err" id="err_title"></span>
        <div class="form-group col-md-3">
          <select name="leave_type" id="leave_type" class="form-control required">
            <option value="">-Select Type-</option>  
            <? foreach($leaves_types as $key=>$value){	
				$leave_left = (isset($earnedLeaves[$value]) ? $earnedLeaves[$value] : 0);
				?>
				<option value="<?=$key."_".$leave_left; ?>"><?=$value." (".$leave_left.")"; ?></option>  
			<?	} ?>	
          </select>
          <span class="err" id="err_ltype"></span>
        </div>
      </div>
      
      <div class="row">
        <label class="col-md-2 offset-md-3" for="title">From Date</label><span class="err" id="err_title"></span>
        <div class="form-group col-md-3">
          <div class='input-group date' id='datetimepicker1'>
            <input type="text" name="from_date" value="" class="form-control" id="from_date" autocomplete="off" />
            <span class="input-group-addon">
              <span class="icon-calendar"></span>
            </span>
          </div>
          <span class="err" id="err_fdate"></span>
        </div>
      </div>
      <div class="row">
        <label class="col-md-2 offset-md-3" for="title">End Date</label><span class="err" id="err_title"></span>
        <div class="form-group col-md-3">
          <div class='input-group date' id='datetimepicker2'>
            <input type="text" name="to_date" value="" class="form-control endDate" id="to_date" autocomplete="off" />
            <span class="input-group-addon">
              <span class="icon-calendar"></span>
            </span>
          </div>
          <span class="err" id="err_edate"></span>
        </div>
      </div>

      <div class="row">
        <label class="col-md-2 offset-md-3" for="title">Total Days</label>
        <div class="form-group col-md-3">
          <input type="text" name="total_days" class="form-control" id="total_days" readonly>
        </div>
      </div>
      
      <div class="row" id="div_day_type" style="display:none">
        <label class="col-md-2 offset-md-3" for="title">Day Type</label>
        <div class="form-group col-md-3">
          <select name="day_type" id="day_type">
						<? foreach($ATTENDANCE_REG_TYPE as $key=>$value) { ?>
							<option value="<?=$key?>"><?=$value;?></option>
						<?	}	?>
          </select>
        </div>
      </div>
      
       <div class="row" id="div_day_type">
        <label class="col-md-2 offset-md-3" for="title">Reason</label>
        <div class="form-group col-md-3">
           <textarea name="reason" class="form-control" id="reason" maxlength="150"></textarea>
        </div>
      </div>
      
      <div class="form-group row">
        <input type="hidden" name="mode" id="mode" value="<?=$mode?>" />
        <input type="hidden" name="edit_id" value="<?=$edit_id?>" />
        <div class="col-md-6"><br>
          <a class="btn btn-sm btn-danger f-left" href="<?=base_url()?>activity">Cancel</a>
        </div>
        <div class="col-md-6"><br>
          <button type="submit" class="btn btn-sm btn-success f-right" name="Submit">Submit</button>
        </div>
      </div>
    </form>
  </div>
</div>

<script type="text/javascript">
  $(document).ready(function ()
  {
     var date = new Date();
     date.setDate(date.getDate() - 4);

      $('#datetimepicker1 #from_date').datetimepicker({
         timepicker: false,
         format: "m/d/Y",
         autoclose: true
      });

      $('#datetimepicker2 #to_date').datetimepicker({
        timepicker: false,
        format: "m/d/Y",
        autoclose: true
      });

      $.validator.addMethod("endDate", function(value, element) {
          var startDate = $('#from_date').val();
          return Date.parse(startDate) <= Date.parse(value) || value == "";
      }, "* End date must be after from date");

      $("#day_type").change(function(){
				if($(this).val() == 'First Half' || $(this).val() == 'Second Half')
					$("#total_days").val('0.5');
				else	
					$("#total_days").val('1');
			});
			
      $("#leave_type").change(function(){
				 var ltype = $(this).val();
         var exp_ltype = ltype.split('_');
         var leave_type = exp_ltype[0]; 
         var leave_count = parseFloat(exp_ltype[1]); 
				 var days = $("#total_days").val();
				 if(leave_type != 'LWP' && leave_count < days) {
						if(leave_count == 0){
							alert('Applied leave days should not exceed leave left');
							$("#total_days").val('');    
							$("#to_date").val('');
						}
						else if(leave_count > 0 && days !=1){
							alert('Applied leave days should not exceed leave left');
							$("#total_days").val('');    
							$("#to_date").val('');
						}
					}
					if(days >0 && days <= 1){
						$("#div_day_type").show();
					}
					else{
						$("#div_day_type").prop('selectedIndex',0);
						$("#div_day_type").hide();
					}
				});
	  
      $("#to_date").change(function(){
        if($("#from_date").val() != '' &&  $("#to_date").val() != ''){
          var from_date = $('#from_date').val();
          var to_date = $('#to_date').val();
          var ltype = $('#leave_type').val();
          var exp_ltype = ltype.split('_');
          var leave_type = exp_ltype[0]; 
          var leave_count = parseFloat(exp_ltype[1]);
          var timeDiff  = (new Date(to_date)) - (new Date(from_date));
          var days      = (timeDiff / (1000 * 60 * 60 * 24))+1;
          $("#total_days").val(parseFloat(days));    
          
          if(days < 0){
            alert('Please select a valid date');
            $("#total_days").val('');    
            $("#to_date").val('');
          }
          else if(leave_type != 'LWP' && leave_count < days) {
						if(leave_count == 0){
							alert('Applied leave days should not exceed leave left');
							$("#total_days").val('');    
							$("#to_date").val('');
						}
						else if(leave_count > 0 && days !=1){
							alert('Applied leave days should not exceed leave left');
							$("#total_days").val('');    
							$("#to_date").val('');
						}
						else if(leave_count == '0.5'){
							$("#day_type").val('First Half');
							$("#day_type option:selected").text('First Half');
							$("#total_days").val('0.5');    
							//alert('Applied leave days should not exceed leave left');
							//$("#total_days").val('0.5');    
							//$("#to_date").val('');
						}
						
					}
					if(days >0 && days <= 1){
						$("#div_day_type").show();
					}
					else{
						$("#div_day_type").prop('selectedIndex',0);
						$("#div_day_type").hide();
					}
        }
      });

      $("#from_date").change(function(){
        if($("#from_date").val() != '' &&  $("#to_date").val() != ''){
          var from_date = $('#from_date').val();
          var to_date = $('#to_date').val();
          var timeDiff  = (new Date(to_date)) - (new Date(from_date));
          var days = (timeDiff / (1000 * 60 * 60 * 24))+1;
          $("#total_days").val(parseFloat(days));    
          var ltype = $('#leave_type').val();
          var exp_ltype = ltype.split('_');
          var leave_type = exp_ltype[0]; 
          var leave_count = parseFloat(exp_ltype[1]);
          if(days < 0){
            alert('Please select a valid date');
            $("#total_days").val('');    
            $("#from_date").val('');
          }
          else if(leave_type!= 'LWP' && leave_count < days) {
						if(leave_count == 0){
							alert('Applied leave days should not exceed leave left');
							$("#total_days").val('');    
							$("#to_date").val('');
						}
						else if(leave_count > 0 && days !=1){
							alert('Applied leave days should not exceed leave left');
							$("#total_days").val('');    
							$("#to_date").val('');
						}
					}
					if(days >0 && days <= 1){
						$("#div_day_type").show();
					}
					else{
						$("#div_day_type").prop('selectedIndex',0);
						$("#div_day_type").hide();
					}
        }
      });
      

      //$("#frmleave").validate();  

  });

  function validatefrm()
  {
    $("err_ltype").html('');
    $("err_fdate").html('');
    $("err_edate").html('');
    error = 0;

    if($("#leave_type").val() == ''){
      $("#err_ltype").html('Please select leave type.');
      error = 1;
    }
    if($("#from_date").val() == ''){
      $("#err_fdate").html('Please select from date.');
      error = 1;
    }
    if($("#to_date").val() == ''){
      $("#err_edate").html('Please select end date.');
      error = 1;
    }
    if($("#from_date").val() != '' && $("#to_date").val() != ''){
      var from_date = $('#from_date').val();
      var to_date = $('#to_date').val();
      var timeDiff  = (new Date(to_date)) - (new Date(from_date));
      var days = timeDiff / (1000 * 60 * 60 * 24)
      if(days < 0){
        $("#err_edate").html('End date should be greater than from date.');
         error = 1;
      }
    }
    if(error == 1){
      return false;
    }
    
  }
</script>
