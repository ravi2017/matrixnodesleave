<table class="table table-striped table-bordered bootstrap-datatable responsive">
<tr>
  <?php if (date("d-m-Y",strtotime($report_end_date)) == date("d-m-Y",strtotime($report_date))) { ?>
  <td align="center" style="text-align:center;"><?=$spark_name['name'];?> Daily Activity Report for <strong><?=date("d-M-Y",strtotime($report_date));?></strong><?php if($pdf != 1 and $excel != 1){?><a href="<?=base_url()?>activity" style="float:right">Back</a><?php } ?></td>
  <?php } else { ?>
  <td align="center" style="text-align:center;"><?=$spark_name['name'];?> Daily Activity Report from <strong><?=date("d-M-Y",strtotime($report_date));?></strong> to <strong><?=date("d-M-Y",strtotime($report_end_date));?></strong><?php if($pdf != 1 and $excel != 1){?><a href="<?=base_url()?>activity" style="float:right">Back</a><?php } ?></td>
  <?php } ?>
</tr>
</table>

<div class="scrollme">
<table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
	<thead>
		<tr>
			<th><span style="font-weight: bold;">Date<br>(DD:MM:YY)</span></th>
			<th><span style="font-weight: bold;">Time<br>(HH:MM)</span></th>
			<th><span style="font-weight: bold;">Activity</span></th>
			<th><span style="font-weight: bold;">Location</span></th>
			<th><span style="font-weight: bold;">Duration<br>(HH:MM)</span></th>
      <th data-priority="1" data-sort=0 class="no-sort action"><span style="font-weight: bold;">Assessment</span></th>
      <!--<th class="no-sort action"><span style="font-weight: bold;">Status</span></th>-->
		</tr>
  </thead>  
  <tbody>  
    <? foreach($activities as $date=>$activity_detail) { ?>
      <? $new = 1; ?>
            
      <? if (count($activity_detail) == 0) { ?>
            <!--No Record Found-->
      <? } else { ?>
        <? foreach($activity_detail as $activity) { ?>
        <tr valign="top">
          <? //if ($new == 1) { ?>
          <td><?=date("d-m-Y",strtotime($date))?></td>
          <td><?=$activity['time']?></td>
          <? //} ?>
          <td><?=$activity['activity_type']?></td>
          <td>
            <? echo (!empty($school_names[$activity['school']]) ? '<strong>School : </strong>'.$school_names[$activity['school']].'<br>' : ''); ?>
            <? echo (!empty($cluster_names[$activity['cluster']]) ? '<strong>Cluster : </strong>'.$cluster_names[$activity['cluster']].'<br>' : ''); ?>
            <? echo (!empty($block_names[$activity['block']]) ? '<strong>Block : </strong>'.$block_names[$activity['block']].'<br>' : ''); ?>
            <? echo (!empty($district_names[$activity['district']]) ? '<strong>District : </strong>'.$district_names[$activity['district']].'<br>' : ''); ?>
            <? echo (!empty($state_names[$activity['state']]) ? '<strong>State : </strong>'.$state_names[$activity['state']].'<br>' : ''); ?>
            <? echo (!empty($activity['details']) ? $activity['details'].'<br>' : ''); ?>
          </td>
          <td><?=$activity['duration']?></td>
          <td>
            <a href="javascript:void(0)" class="view_assessment" alt="<?=str_replace(' ','_',strtolower($activity['activity_type']))."@".$activity['id']?>">View</a>
            <?php if($activity['source'] != 'app') { echo '<br />Source : '.ucfirst($activity['source']); } ?>
          </td>
          <?php /* if(isset($activity['status'])){?>
          <td style="color:green;"><?php echo $activity['status']; ?></td>
          <?php }else{ ?>
          <td style="color:red;">Pending</td>
          <?php } */ ?>  
        </tr>
        <? $new = 0; ?>
        <? } ?>
      <? } ?>
    <? } ?>
	</tbody>
</table>
</div>
  <div class="modal fade" id="show_assessment" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Assessment Details</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body" id="assessment_data">
        </div>
      </div>
    </div>
  </div>

</div>
<script>
  $(document).on('click', '.view_assessment', function(){
      var cur_val = $(this).attr('alt');
      var var_arr = cur_val.split('@');
      var activity_type = var_arr[0];
      var activity_id = var_arr[1];

      $.ajax({
        url: BASE_URL+'activity/get_assessment_data',
        type: 'post',
        data: 'activity_type='+activity_type+'&activity_id='+activity_id,
        dataType: 'html',
        success: function(response) {
          $("#assessment_data").html(response)
          $("#show_assessment").modal('show');
        },
        error: function(response, error) {
          window.console.log(error);
        }
      });
  });
</script>


