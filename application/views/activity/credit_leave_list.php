<? /* if(!isset($download) && !isset($pdf)) {?>
<form id="frmattendance" name="frmcreditleave" action="" method="post">
	  <div class="form-group row">
			<label for="login_id" class="col-sm-2 control-label text-right">Start Date</label>
			<div class="col-sm-3 text-left">
				<input type="text" autocomplete="off" id="creditleavestart" name="creditleavestart" class="form-control required" value="<?=date('d-m-Y', strtotime($creditleavestart));?>">
			</div>
			
			<label for="login_id" class="col-sm-2 control-label text-right">End Date</label>
			<div class="col-sm-3 text-left">
				<input type="text" autocomplete="off" id="creditleaveend" name="creditleaveend" class="form-control required" value="<?=date('d-m-Y', strtotime($creditleaveend));?>">
			</div>
			
			<div class="col-sm-2">
				<input type="submit" name="submit" value="Get List" class="btn btn-sm btn-info">
			</div>
		</div>	
</form>
<div class="form-group row">
    <div class="form-group col-md-3">
      <form name="frmreport" action="" method="post">
          <input type="hidden" name="creditleavestart" value="<?=$creditleavestart?>">
          <input type="hidden" name="creditleaveend" value="<?=$creditleaveend?>">
          <input type="hidden" name="download" value="1">
          <button type="submit" name="submit"  value="download" class="btn btn-success">Download Report</button>
      </form>
    </div>
    <div class="form-group col-md-3">
      <form name="frmreport" action="" method="post">
          <input type="hidden" name="creditleavestart" value="<?=$creditleavestart?>">
          <input type="hidden" name="creditleaveend" value="<?=$creditleaveend?>">
          <input type="hidden" name="pdf" value="1">
          <button type="submit" name="submit"  value="download" class="btn btn-success">Download PDF</button>
      </form>
    </div>
</div>
<?	} */	?>
<h4>Leave Taken Details</h4>
<table class="table table-striped table-bordered bootstrap-datatable datatable responsive customdatatable" width="100%">
	<thead>
		<tr style="background-color:#D5ECF9">
			<th align="center">Type</th>
			<th align="center">From Date</th>
			<th align="center">End Date</th>
			<th align="center">Total Days</th>
			<th align="center">Day Type</th>
			<th align="center">Reason</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
	
	<?php
	  if(!empty($leaveslist)){	
		foreach ($leaveslist as $e_key){ ?>
		<tr id="row_<?=$e_key->id?>">
			<td align="left"><?php echo ucfirst($e_key->leave_type); ?></td>
			<td align="right"><?php echo date('d-m-Y', strtotime($e_key->leave_from_date)); ?></td>
			<td align="right"><?php echo date('d-m-Y', strtotime($e_key->leave_end_date)); ?></td>
			<td align="right"><?php echo $e_key->total_days; ?></td>
			<td align="right"><?php echo ($e_key->day_type != '' ? $e_key->day_type : 'N/A'); ?></td>
			<td align="right"><?php echo ($e_key->reason != '' ? $e_key->reason : 'N/A'); ?></td>
			<td>
				<? if(time() < strtotime($e_key->leave_end_date)){ ?>
				<a href="javascript:void(0)" alt="<?=$e_key->id?>" class="cancel_leaves btn btn-sm btn-danger">Cancel</a>
				<?	} ?>
			</td>
		</tr>
	<?php } } ?>
  </tbody>
</table>

<h4>Leave Credit Details</h4>
<table class="table table-striped table-bordered bootstrap-datatable datatable1 responsive customdatatable" width="100%" id="credittable">
	<thead>
		<tr style="background-color:#D5ECF9">
			<th align="center">Type</th>
			<th align="center">Earned</th>
			<th align="center">Taken</th>
			<th align="center">Left</th>
		</tr>
	</thead>
	<tbody>
	
	<?php
	  if(!empty($creditleavelist)){	
		foreach ($creditleavelist as $e_key){ 
			
					$leave_left = ($e_key->leave_taken < 0 ? $e_key->leave_earned+$e_key->leave_taken : $e_key->leave_earned-$e_key->leave_taken);
			?>
		<tr>
			<td align="left"><?php echo ucfirst($e_key->leave_type); ?></td>
			<td align="right"><?php echo $e_key->leave_earned; ?></td>
			<td align="right"><?php echo abs($e_key->leave_taken); ?></td>
			<td align="right"><?php echo $leave_left; ?></td>
		</tr>
	<?php } } ?>
  </tbody>
</table>

<script>
$(document).on('click', '.cancel_leaves', function(){
		
		if(confirm('Are you sure to cancel.')){
			var cur_id = $(this).attr('alt');
			
			$.ajax({
				url: BASE_URL+'activity/cancel_leave',
				type: 'post',
				data: 'cur_id='+cur_id,
				dataType: 'html',
				success: function(response) {
					$('#row_'+cur_id).remove();
				},
				error: function(response, error) {
					window.console.log(error);
				}
			});
		}
});	
</script>

<? /* if(!isset($download) && !isset($pdf)) {?>
<div class="modal fade" id="show_leavecredit" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Credit Leaves Details</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body" id="leavecredit_data">
			</div>
		</div>
	</div>
</div>

<script>
$('#creditleavestart').datepicker({
	dateFormat: "dd-mm-yy"
});	
$('#creditleaveend').datepicker({
	dateFormat: "dd-mm-yy"
});	

$(document).on('click', '.cancel_leaves', function(){
		var cur_id = $(this).attr('alt');

		$.ajax({
			url: BASE_URL+'admin/leaves/credit_leave_details',
			type: 'post',
			data: 'cur_id='+cur_id,
			dataType: 'html',
			success: function(response) {
				$("#leavecredit_data").html(response)
				$("#show_leavecredit").modal('show');
			},
			error: function(response, error) {
				window.console.log(error);
			}
		});
});	
</script>
<?	}	*/ ?>
