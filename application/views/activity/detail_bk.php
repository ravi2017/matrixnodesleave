<table class="table table-striped table-bordered bootstrap-datatable responsive">
<tr>
  <?php if (date("d-m-Y",strtotime($report_end_date)) == date("d-m-Y",strtotime($report_date))) { ?>
  <td align="center" style="text-align:center;">My Daily Activity Report for <strong><?=date("d-M-Y",strtotime($report_date));?></strong><?php if($pdf != 1 and $excel != 1){?><a href="<?=base_url()?>activity" style="float:right">Back</a><?php } ?></td>
  <?php } else { ?>
  <td align="center" style="text-align:center;">My Daily Activity Report from <strong><?=date("d-M-Y",strtotime($report_date));?></strong> to <strong><?=date("d-M-Y",strtotime($report_end_date));?></strong><?php if($pdf != 1 and $excel != 1){?><a href="<?=base_url()?>activity" style="float:right">Back</a><?php } ?></td>
  <?php } ?>
</tr>
</table>

<table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
	<thead>
		<tr>
			<th><span style="font-weight: bold;">Activity Date</span></th>
			<th><span style="font-weight: bold;">Activity Type</span></th>
			<th><span style="font-weight: bold;">State</span></th>
			<th><span style="font-weight: bold;">District</span></th>
			<th><span style="font-weight: bold;">Block</span></th>
			<th><span style="font-weight: bold;">Cluster</span></th>
			<th><span style="font-weight: bold;">School</span></th>
      <th><span style="font-weight: bold;">Activity Details</span></th>
      <th data-priority="1" data-sort=0 class="no-sort action"><span style="font-weight: bold;">Assessment Details</span></th>
      <th class="no-sort action"><span style="font-weight: bold;">Status</span></th>
		</tr>
  </thead>  
  <tbody>  
    <? foreach($activities as $date=>$activity_detail) { ?>
      <? $new = 1; ?>
            
      <? if (count($activity_detail) == 0) { ?>
    <!--  <tr>
        <td><? date("d-m-Y",strtotime($date))?></td>
        <td>-</td>
        <td>-</td>
        <td>-</td>
        <td>-</td>
        <td>-</td>
        <td>-</td>
        <td>-</td>
        <td>-</td>
      </tr> -->
      <? } else { ?>
        <? foreach($activity_detail as $activity) { ?>
        <tr valign="top">
          <? //if ($new == 1) { ?>
          <td><?=date("d-m-Y",strtotime($date))?></td>
          <? //} ?>
          <td><?=$activity['activity_type']?></td>
          <td><? echo (!empty($state_names[$activity['state']]) ? $state_names[$activity['state']] : ''); ?></td>
          <td><? echo (!empty($district_names[$activity['district']]) ? $district_names[$activity['district']] : ''); ?></td>
          <td><? echo (!empty($block_names[$activity['block']]) ? $block_names[$activity['block']] : ''); ?></td>
          <td><? echo (!empty($cluster_names[$activity['cluster']]) ? $cluster_names[$activity['cluster']] : ''); ?></td>
          <td><? echo (!empty($school_names[$activity['school']]) ? $school_names[$activity['school']] : ''); ?></td>
          <td><?=$activity['details']?></td>
          <td>
            <a href="javascript:void(0)" class="show_assess" alt="<?=str_replace(' ','_',$activity['activity_type'])."_".$activity['id']?>">View</a>
          </td>
          <?php if(isset($activity['status'])){?>
          <td style="color:green;"><?php echo $activity['status']; ?></td>
          <?php }else{ ?>
          <td style="color:red;">Pending</td>
          <?php } ?>  
        </tr>
        <? $new = 0; ?>
        <? } ?>
      <? } ?>
    <? } ?>
	</tbody>
</table>
<? foreach($activities as $date=>$activity_detail) { ?>
<? foreach($activity_detail as $activity) { ?>
    <div id="<?=str_replace(' ','_',$activity['activity_type'])."_".$activity['id']?>" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Assessment</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body">
            <? 
            //echo "<pre>";
            //print_r($activity['assessment']);
            if(!empty($activity['assessment'])){
            foreach($activity['assessment'] as $class=>$class_data) { ?>
            
              <? if($class != 'no_class') { ?>
                <div class="row">   
                 <div class="col-md-2"><strong>Class: </strong></div><div class="col-md-4"> <?php echo ucfirst($class);?></div> 
                </div> 
                <hr>
               <? } ?>  
               
               <? foreach($class_data as $subjects=>$subject_data) { ?>
               
               <? if($subjects != 'no_subject') { ?>
                 <div class="row">   
                 <div class="col-md-2"><strong>Subject: </strong></div><div class="col-md-4"> <?php echo ucfirst($subjects);?></div> 
                 </div>  
               <? } ?>  
                <? //foreach($subject_data as $ques) {  
                  //print_r($subject_data); ?>
                  <? for($k=0; $k<count($subject_data['question']); $k++) { ?>
                    <div class="row">   
                       <div class="col-md-2"><strong>Question: </strong></div><div class="col-md-10"><?=$subject_data['question'][$k];?></div> 
                     </div>   
                     <div class="row">   
                       <div class="col-md-2"><strong>Answer: </strong></div><div class="col-md-10"><?=$subject_data['answer'][$k];?></div> 
                    </div>
                
                  <? //} ?>
                  <hr>
                <? } ?>
                
              <? } ?>
              
            <? } 
            }else{ ?>
              <div class="row text-center">No Record Found.</div>
            <? } ?>  
          </div>
          <div class="modal-footer"></div>
        </div>
      </div>
    </div>
<? } ?>
<? } ?>

  <div id="myModal" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Reason</h4>
          </div>
          <div class="modal-body">
            <form id="modal_form" method="POST">
              <div class="row">   
              <textarea class="form-control" name="reason"></textarea>      
             </div>
             <br>
             <input type="hidden" id="modal_activity_type" name="activity_type" value="">
             <input type="hidden" id="modal_activity_id" name="activity_id" value="">
             <button type="submit" id="submit_modal" class="btn btn-primary" style="float:right">Submit</button>
            </form>
          </div>
          <div class="modal-footer">
          </div>
        </div>
      </div>
  </div>

</div>
<script>
  
  $(document).on('click', '.show_assess', function(){
       var cur_id = $(this).attr('alt');
       $("#"+cur_id).modal('show')
    });
  
  $(document).on('click', '.status', function(){
    var action = $(this).attr('action')
    var type = $(this).attr('activity_type')
    var id = $(this).attr('activity_id')
    $.ajax({
    url: '<?=base_url()?>activity_approval/'+action+'/'+type+'/'+id,
    type: 'get',
    data: {
      activity_type: type,
      activity_id: id
    },
    dataType: 'json',
    success: function(response) {
      $('#status'+id).hide();
      $('#approve'+id).show();
    },
    error: function(response) {
      window.console.log(response);
    }
  });
  });

  $(document).on('click', '.reject_status', function(){
    $('#myModal').modal('show');
    $('#modal_activity_type').val($(this).attr('activity_type'))
    $('#modal_activity_id').val($(this).attr('activity_id'))
  });

  $("#modal_form").submit(function(e) {
    e.preventDefault(); 
    var form = $(this);
    var id = $('#modal_activity_id').val()
    $.ajax({
         type: "POST",
         url: '<?=base_url()?>activity_approval/reject_activity',
         data: form.serialize(), 
         success: function(data)
         {
            $('#status'+id).hide();
            $('#reject'+id).show();
         }
       });
    $('#myModal').modal('hide');
  })
</script>


