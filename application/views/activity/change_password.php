<div class="row">
  <div class="col-md-12">
    <form method="post" action="" role="form" name="frmleave" id="frmleave" onSubmit="return validatefrm()">
      
      <div class="row">
        <label class="col-md-2 offset-md-3" for="title">New Password</label>
        <div class="form-group col-md-3">
          <input type="text" name="new_pwd" class="form-control" id="new_pwd" maxlength="30">
        </div>
        <span class="err" id="err_pwd"></span>
      </div>
      
      <div class="form-group row">
        <div class="col-md-6"><br>
          <input type="submit" class="btn btn-sm btn-success f-right" name="Submit" value="Save"/>
        </div>
      </div>
    </form>
  </div>
</div>

<script type="text/javascript">
  function validatefrm()
  {
    error = 0;
    if($("#new_pwd").val() == ''){
      $("#err_pwd").html('Please enter password.');
      error = 1;
    }
    if(error == 1){
      return false;
    }
    
  }
</script>
