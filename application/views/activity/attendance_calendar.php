<link href='<?=base_url()?>assets/v2/calendardata/fullcalendar.min.css' rel='stylesheet' />
<link href='<?=base_url()?>assets/v2/calendardata/fullcalendar.print.min.css' rel='stylesheet' media='print' />
<script src='<?=base_url()?>assets/v2/calendardata/lib/moment.min.js'></script>
<script src='<?=base_url()?>assets/v2/calendardata/lib/jquery.min.js'></script>
<script src='<?=base_url()?>assets/v2/calendardata/fullcalendar.min.js'></script>
<script src='<?=base_url()?>assets/v2/calendardata/gcal.min.js'></script>

<?
	$sdate	= $start_year.'-'.$start_month.'-01';
  $edate 	= date('Y-m-t', strtotime($sdate));
?>	

<? if(!isset($download) && !isset($pdf)) {?>
<form name="frmactivities" method="post" action="" class="">
	<input type="hidden" name="spark_start" id="spark_start" value="<?=$sdate?>">
	<input type="hidden" name="spark_end" id="spark_end" value="<?=$edate?>">
  <div class="row">
    <div class="form-group col-md-3">
			<select class="form-control" id="start_month" name="start_month">
				<option value="">Select Month</option>
				<? foreach($month_arr as $key=>$value) { ?>
					<option value="<?=$key?>" <? echo ($key == $start_month ? 'selected' : '');?>><?=$value?></option>
				<? } ?>
			</select>
		</div>
		<div class="form-group col-md-3">
			<select class="form-control" id="start_year" name="start_year">
					<option value="">Select Year</option>
					<? foreach($year_arr as $year) { ?>
						<option value="<?=$year?>" <? echo ($year == $start_year ? 'selected' : '');?>><?=$year?></option>
					<? } ?>
			</select>
		</div>
    <?php if ($current_role != "field_user") { ?>
    <div class="col-md-6">
    <?php echo $this->load->view('team_members/hr_spark','',true) ?>
    </div>
    <?php } ?>
    <div class="col-md-3">
      <div class="form-group">
        <input type="submit" name="submit" class="form-control btn btn-sm btn-success" value="Get Calendar">
      </div>
    </div>
  </div>
</form>
<? } ?>


<div class="row">
	<div class="col-md-12">
		<div id="alert_holder"></div>
		<div class="calendar">                                
			<div id="calendarappointment"></div>                            
		</div>
	</div>
</div>
<br />
<br />
<div class="row">
	<div class="col-md-12 offset-md-1">
		<div class="row">
			<div class="col-md-2"><span class="checkmark1" title="Attendance Marked"></span> <label class="legend1">Attendance</label></div>	
			<div class="col-md-2"><span class="checkmark2" title="Attendance-Out marked before 4PM"></span> <label class="legend1">Early Attendance</label></div>	
			<div class="col-md-2"><span class="checkmark3" title="Leave Marked"></span> <label class="legend1">Leave</label></div>	
			<div class="col-md-2"><span class="checkmark4" title="No attendance and no leave"></span> <label class="legend1">Not Marked</label></div>	
			<div class="col-md-2"><span class="checkmark5" title="Week-Off"></span> <label class="legend1">Week-Off</label></div>	
			<div class="col-md-2"><span class="checkmark6" title="Holiday"></span> <label class="legend1">Holiday</label></div>	
		</div>
	</div>
</div>	
<?
if(isset($selected_month)){
	$selected_month = date('Y-m-d', strtotime($selected_month));
?>
<script>
$("#start_month").change(function(){
	var y = $("#start_year").val();
	var m = $("#start_month").val();
	if(y != '' && m != ''){
		var lastDay = new Date(y, m, 0).getDate();
		var sdate = y+"-"+m+"-01";
		var edate = y+"-"+m+"-"+lastDay;
		$("#spark_start").val(sdate);
		$("#spark_end").val(edate);
	}
	reset_values();
});

$("#start_year").change(function(){
	var y = $("#start_year").val();
	var y = $("#start_year").val();
	var m = $("#start_month").val();
	if(y != '' && m != ''){
		var lastDay = new Date(y, m, 0).getDate();
		var sdate = y+"-"+m+"-01";
		var edate = y+"-"+m+"-"+lastDay;
		$("#spark_start").val(sdate);
		$("#spark_end").val(edate);
	}
	reset_values();
});	
	
	var date_diff = <?php echo json_encode($date_diff); ?>;
	var leaves_dates = <?php echo json_encode($leaves_dates_array); ?>;
	var attendance_dates_array = <?php echo json_encode($attendance_dates_array); ?>;
	var attendance_befor_array = <?php echo json_encode($attendance_befor_array); ?>;
	var holiday_data = <?php echo json_encode($holiday_dates_array); ?>;
	var off_data = <?php echo json_encode($off_dates_array); ?>;
	
	$('#calendarappointment').fullCalendar({
		displayEventTime: false,
		height:500,
		header: {
			//left: 'prev,next today',
			left: '',
			center: 'title',
			//right: 'month,agendaWeek,agendaDay'
			right: ''
		},
		defaultDate: '<?=$selected_month?>',
		buttonText: {
			//prev: "",
			//next: "",
			today: 'Today',
			month: 'Month',
			week: 'Week',
			day: 'Day'
		},
		dayRender: function (date, cell) {

        var newDate = new Date(date).toISOString().slice(0,10);
        //var today = new Date();
        //var end = new Date();
        //end.setDate(today.getDate()+7);
        
        if($.inArray(newDate, date_diff) >= 0) {
            cell.css("background-color", "#FFB2B2");
        }
        if($.inArray(newDate, holiday_data) >= 0) {
            cell.css("background-color", "#B2FFFF");
        }
        if($.inArray(newDate, off_data) >= 0) {
            cell.css("background-color", "#CCCCCC");
        }
        if($.inArray(newDate, leaves_dates) >= 0) {
            cell.css("background-color", "#B7D2FF");
        }
        if($.inArray(newDate, attendance_dates_array) >= 0) {
            cell.css("background-color", "#AEEF9F");
        }
        if($.inArray(newDate, attendance_befor_array) >= 0) {
            cell.css("background-color", "#FFFF8B");
        }
        //if(date > today && date <= end) {
            //cell.css("background-color", "yellow");
        //}
      
    },
		events: [
		<? if(!empty($attendance_in_array)){ foreach($attendance_in_array as $date=>$value){	?>
				{
						title: "<?=$value['title']?>",
						start: "<?=date('Y-m-d', strtotime($date))?>",
						color: "<?=$value['color']?>"
				},
		<?	}	} ?>		
		<? if(!empty($attendance_reg_in)){ foreach($attendance_reg_in as $date=>$value){	?>
				{
						title: "<?=$value['title']?>",
						start: "<?=date('Y-m-d', strtotime($date))?>",
						color: "<?=$value['color']?>"
				},
		<?	}	} ?>		
		<? if(!empty($holiday_data)){ foreach($holiday_data as $value){	?>
				{
						title: "<?=$value['title']?>",
						start: "<?=date('Y-m-d', strtotime($value['start_date']))?>",
						color: "#B2FFFF"
				},
		<?	}	} ?>	
		<? if(!empty($off_data)){ foreach($off_data as $value){	?>
				{
						title: "<?=$value['title']?>",
						start: "<?=date('Y-m-d', strtotime($value['start_date']))?>",
						color: "#CCCCCC"
				},
		<?	}	} ?>	
		<? if(!empty($leaves_array)){ foreach($leaves_array as $value){	?>
				{
						title: "<?=$value['title']?>",
						start: "<?=date('Y-m-d', strtotime($value['start_date']))?>",
						color: "<?=$value['color']?>"
				},
		<?	}	} ?>		
		],
		eventColor: '#FFFFFF',
	});
</script>
<?	} ?>
<style>
.fc-header-toolbar{
	display:none;
}	
.fc {
    font-size: 0.96em !important;
}

.fc th {
background:#A6A6A6;
color:#fff;
border-color: #f1592a;
}

.legend1{
	padding-left:10px;
	font-size:0.675em;
}
.checkmark1 {
  position: absolute;
  top: 0;
  left: 0;
  height: 20px;
  width: 20px;
  background-color: #AEEF9F;
}
.checkmark2 {
  position: absolute;
  top: 0;
  left: 0;
  height: 20px;
  width: 20px;
  background-color: #FFFF8B;
}
.checkmark3 {
  position: absolute;
  top: 0;
  left: 0;
  height: 20px;
  width: 20px;
  background-color: #B7D2FF;
}
.checkmark4 {
  position: absolute;
  top: 0;
  left: 0;
  height: 20px;
  width: 20px;
  background-color: #FFB2B2;
}
.checkmark5 {
  position: absolute;
  top: 0;
  left: 0;
  height: 20px;
  width: 20px;
  background-color: #CCCCCC;
}
.checkmark6 {
  position: absolute;
  top: 0;
  left: 0;
  height: 20px;
  width: 20px;
  background-color: #B2FFFF;
}

</style>
