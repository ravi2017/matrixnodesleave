<?php if (isset($observations) and count($observations) > 0) { ?>
<h5 class="card-header-text">OBSERVATIONS</h5>
<div class="card-block accordion-block color-accordion-block">
  <div id="accordion" role="tablist" aria-multiselectable="true">
        
  <?php $collapsed = 'collapsed'; ?>
  <?php $collapsed_show = 'show'; ?>
  <?php $m = 1;
  $required = '';
  //if($is_madatory){
    //$required = 'required';
  //}
  
  foreach($observations as $class=>$data) {  ?>
  <?php if(!empty($data)){ ?>
  <?
    $class_info = get_class_info($class);
    $subject_ids = get_class_state_subject_ids($class_info['id'],$user_state_id);
    if (isset($subject_ids) and count($subject_ids) > 0)
    {
      $subjects = get_subjects_info($subject_ids);
    }
  ?>  
    <div class="accordion-panel">
      <div class="accordion-heading" role="tab" id="classHead<?php echo $m; ?>">
        <h3 class="card-title accordion-title">
          <a class="accordion-msg bg-default <?php echo $collapsed; ?>" data-toggle="collapse" data-parent="#accordion" href="#class<?php echo $m; ?>" aria-expanded="true" aria-controls="class<?php echo $m; ?>">
            <?php echo (!empty($class) ? ucfirst($class) : 'General Question(s)');?>
          </a>
        </h3>
      </div>
      <div id="class<?php echo $m; ?>" class="panel-collapse collapse in <?php echo $collapsed_show; ?>" role="tabpanel" aria-labelledby="classHead<?php echo $m; ?>">
        <div class="accordion-content accordion-desc">
          <?php foreach($data as $key=>$record) { ?>
            <?php if($record->subject_wise == 1) { ?>
              <?php if($key == 0) { ?>
              <div class="row form-group">
                <div class="col-md-4"><label></label></div>
                <div class="col-md-8">
                  <div class="row">
                    <?php if(!empty($subjects)){ foreach($subjects as $subject) {?>
                    <div class="col-md-4">
                      <div class=""><?=$subject->name?></div>
                    </div>
                    <?php } } ?>
                  </div>
                </div>
              </div>
              <?php } ?>
              
              <div class="row form-group">
                <div class="col-md-4"><label><?=$record->question?></label></div>
                <div class="col-md-8">
                  <div class="row">
                    <?php if(!empty($subjects)){ 
                      foreach($subjects as $subject) { ?>
                      <div class="col-md-3">
                      <?php if($record->question_type == 'objective') { ?>
                        <select class="form-control <?=$required;?>" id="ques_<?=$record->id?>" name="ques[<?=$record->id?>][<?=$subject->id?>]">
                          <option>-Select-</option>
                          <?php foreach($record->options as $option_id=>$option) { ?>
                          <option value="<?php echo $option_id; ?>"><?php echo $option; ?></option>
                          <?php } ?>
                        </select>
                      <?php }else if($record->question_type == 'yes-no-na') { ?>  
                        <select class="form-control" id="ques_<?=$record->id?>" name="ques[<?=$record->id?>][<?=$subject->id?>]">
                          <option value="0">N/A</option>
                          <option value="1">Yes</option>
                          <option value="2">No</option>
                        </select>
                      <? }else if($record->question_type == 'yes-no') { ?>  
                        <select class="form-control" id="ques_<?=$record->id?>" name="ques[<?=$record->id?>][<?=$subject->id?>]">
                          <option value="0">N/A</option>
                          <option value="1">Yes</option>
                          <option value="2">No</option>
                        </select>
                      <? }else{ ?>  
                        <label class="switch"><input type="checkbox" name="ques[<?=$record->id?>][<?=$subject->id?>]" value="1"><span class="slider round"></span></label>
                        <?  } ?>
                        </div>
                    <?php } 
                    } ?>
                  </div>
                </div>
              </div>
            <?php } ?>
            <?php if($record->subject_wise == 0) { ?>
              <div class="row form-group p-t-10">
                <div class="col-md-4"><label><?=$record->question; ?></label></div>
                
                  <?php if ($record->question_type == "subjective") { ?>
                    <div class="col-md-8">
                    <textarea row="2" col="4" name="ques[<?=$record->id?>]" value="" class="form-control required" id="ques_<?=$record->id?>" maxlength="250"></textarea>
                  <?php }
                    else if($record->question_type == 'yes-no') {
                        ?>
                        <div class="col-md-3">
                        <select class="form-control <?=$required;?>" id="ques_<?=$record->id?>" name="ques[<?=$record->id?>]">
                          <option>-Select-</option>
                          <option value="0">No</option>
                          <option value="1">Yes</option>
                        </select>
                        <?
                    }
                    else if($record->question_type == 'yes-no-na') { ?>  
                      <div class="col-md-3">
                        <select class="form-control" id="ques_<?=$record->id?>" name="ques[<?=$record->id?>][<?=$subject->id?>]">
                          <option value="0">N/A</option>
                          <option value="1">Yes</option>
                          <option value="2">No</option>
                        </select>
                    <? }   
                   else { ?>
                    <div class="col-md-3">
                    <select class="form-control <?=$required;?>" id="ques_<?=$record->id?>" name="ques[<?=$record->id?>]">
                      <option>-Select-</option>
                      <?php foreach($record->options as $option_id=>$option) { ?>
                      <option value="<?php echo $option_id; ?>"><?php echo $option; ?></option>
                      <?php } ?>
                    </select>
                  <?php } ?>
                </div>
              </div>
            <?php } ?>
          <?php } ?>
        </div>
      </div>
    </div>            
  <? } ?>
  <?php $collapsed = ''; ?>
  <?php $collapsed_show = ''; ?>
  <? 
  $m++;
  } ?>
  </div>
</div>
<?php } ?>
