<? if(!isset($download) && !isset($pdf)) {?>
<form name="frmtraining" id="frmtraining" method="post" action="" class="">
  <div class="row">
    <div class="col-md-3">
      <div class="form-group">
        <input type="text" id="activitydatestart" name="activitydatestart" class="form-control required" value="<? echo (!empty($activitydatestart) ? $activitydatestart : '');?>" autocomplete="off">
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <input type="text" id="activitydateend" name="activitydateend" class="form-control required" value="<? echo (!empty($activitydateend) ? $activitydateend : '');?>" autocomplete="off">
      </div>
    </div>
    <? if($current_role == "super_admin" or $current_role == "admin" or $current_role == "accounts" or $current_role == "HR") { ?>
    <div class="col-md-3">
      <div class="form-group">
        <select name="state_id" id="state_id" class="form-control required">
          <option value=''>-Select State-</option>
          <? foreach($state_list as $states) { ?>
            <option value="<?=$states->id;?>" <?php echo (isset($state_id) && $state_id == $states->id ? 'selected' : ''); ?> ><?=$states->name;?></option>
          <?  } ?>  
        </select>
      </div>
    </div>
    <?  } ?>
    <?
      $activity_arr = array('trainings'=>'Training', 'meetings'=>'Meeting', 'school_visits'=>'School Visit');
    ?>
    <div class="form-group col-md-3">
      <select name="activity_type" id="activity_type" class="form-control required">
        <option value="">-Select Activity-</option>
        <? foreach($activity_arr as $key=>$value) { ?>
          <option value="<?=$key?>" <? if($activity_type == $key) { echo 'selected'; } ?> ><?=$value?></option>
        <?  } ?>  
      </select>
    </div>  
  </div>  
  <div class="row">
    <div class="col-md-4"></div> 
    <div class="col-md-3">
      <div class="form-group">
        <input type="submit" name="submit" class="form-control btn btn-sm btn-success" value="Get List">
      </div>
    </div>
  </div>
</form>
<? } ?>

<? if(!isset($download) && !isset($pdf) && !empty($activity_data))  { ?>
    <div class="form-group row">
      <div class="form-group col-md-3">
      <form name="frmreport" action="<?php echo base_url();?>activity/address_mismatch" method="post">
          <input type="hidden" name="activitydatestart" value="<? echo (!empty($activitydatestart) ? $activitydatestart : '');?>">
          <input type="hidden" name="activitydateend" value="<? echo (!empty($activitydateend) ? $activitydateend : '');?>">
          <input type="hidden" name="activity_type" value="<?=$activity_type?>">
          <input type="hidden" name="state_id" value="<?=$state_id?>">
          <input type="hidden" name="download" value="1">
          <button type="submit" name="submit" value="Get training List" class="btn btn-success">Download Report</button>
      </form>
      </div>
      <div class="form-group col-md-3">
      <form name="frmreport" action="<?php echo base_url();?>activity/address_mismatch" method="post">
          <input type="hidden" name="activitydatestart" value="<? echo (!empty($activitydatestart) ? $activitydatestart : '');?>">
          <input type="hidden" name="activitydateend" value="<? echo (!empty($activitydateend) ? $activitydateend : '');?>">
          <input type="hidden" name="activity_type" value="<?=$activity_type?>">
          <input type="hidden" name="state_id" value="<?=$state_id?>">
          <input type="hidden" name="pdf" value="1">
        <button type="submit" name="submit" value="Get training List" class="btn btn-success">Download PDF</button>
      </form>
      </div>
      <div class="col-md-6" align="right"><a href="<?=base_url()?>activity/training_list" style="float:right">Back</a></div>
    </div>
<? } ?>

<? if(!empty($activity_data)) {?>
<div class="scrollme">
<table class="table table-striped table-bordered bootstrap-datatable datatable1 responsive customdatatable" width="100%">
	<thead>
		<tr style="background-color: #d5ecf9;">
			<th>S.No</th>
      <th>Name</th>
      <th>Activity Date</th>
      <th>In Address</th>
      <th>Out Address</th>
      <th>Distance <br>(In KM)</th>
		</tr>
	</thead>
	<tbody>
    <?php $i=1; 
    foreach($activity_data as $activity){
      
      $source_address = trim(strip_tags($activity->source_address));
      $source_address = implode(',<br>',explode(',',$source_address));
      $destination_address = trim(strip_tags($activity->destination_address));
      $destination_address = implode(',<br>',explode(',',$destination_address));
      
      $distance = round($activity->distance/1000,2);
      ?>
      <tr>
      <td><?=$i?></td>
      <td><?=$activity->name." (".$activity->login_id.")"?></td>
      <td><? echo date('d-m-Y', strtotime($activity->ActivityDate));?></td>
      <td><?=$source_address; ?></td>
      <td><?=$destination_address; ?></td>
      <td><?=$distance; ?></td>
      </tr>
      <?
      $i++;
    }
  ?>  
  </tbody>
</table>  
</div>

<? } ?>

<? if(!isset($download) && !isset($pdf)) {?>

<script>
$(document).ready(function() {
    $("#frmtraining").validate();
});
</script>
<? } ?>
