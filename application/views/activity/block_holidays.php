<table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
	<thead>
		<tr>
			<th>Holiday Name</th>
			<th>Holiday Type</th>
			<th>Holiday Date</th>
		</tr>
	</thead>
	<tbody>
	
	<? $holiday_count = 0; ?>
	<?php foreach ($holidays_list as $e_key){ 
			
			if($e_key->start_date == $e_key->end_date)
				$holiday_date = date("d-m-Y",strtotime($e_key->start_date));
			else	
				$holiday_date = date("d-m-Y",strtotime($e_key->start_date))." To ".date("d-m-Y",strtotime($e_key->end_date)); 
		?>
		<tr>
			<td><?php echo $e_key->name; ?></td>
			<td><?php echo $e_key->holiday_type; ?></td>
			<td><?php echo $holiday_date; ?></td>
		</tr>
	<?php } ?>
		</tbody>
	</table>
	
	
	
