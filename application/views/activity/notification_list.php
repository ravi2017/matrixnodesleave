<? 
	include_once('top_form.php');
?>

<div class="row">
	<div class="form-group col-md-12">
		<h6><?php echo ucfirst($status); ?> Result from <?php echo $activitydatestart." to ".$activitydateend ?></h6>	
	</div>
</div>
<div class="scrollme">

<!-- START LOCAL CONV CLAIM -->	
<?
$display_buttons = 1;
$is_processed = 1;
$net_claim_cost = 0;
$net_local_claim_cost = 0;
$net_ld_claim_cost = 0;
$net_stay_claim_cost = 0;
$net_stay_bill_cost = 0;
$net_food_actual_claim_cost = 0;
$net_food_claim_cost = 0;
$net_other_claim_cost = 0;
?>
<div class="row"><div class="col-md-12 text-center"  style="background-color: #A6A6A6;"><a href="<?=base_url();?>map/claims_list"><h4>Local Convence Claim</h4></a></div></div>
<table class="table table-striped table-bordered bootstrap-datatable datatable1 responsive customdatatable withscrolltable lcclaim">
	<thead>
		<tr style="background-color: #d5ecf9;">
			<th align="center">S.No.</th>
			<th align="center">Spark</th>
			<th align="center">Activity</th>
			<th align="center">Details</th>
			<th align="center">Date<br>(DD:MM:YY)</th>
			<th align="center">Time <br>(HH:MM)</th>
			<th align="center">Duration <br>(HH:MM)</th>
			<th align="center">Distance <br>(KM)</th>
			<th align="center">Manual Distance <br>(KM)</th>
			<th align="center">Travel Mode</th>
			<th align="center">Travel Cost</th>
			<!--<th align="center">Remark</th>-->
      <th align="center">
			<?php if($current_role == 'accounts') { echo 'Action'; }else{ echo 'Account Status'; } ?>
      </th>
		</tr>
	</thead>
	<tbody>
	<?php 
    $r=1;
    $activity_date = '';
    $previous_travel_mode_id = '';
    foreach($local_claim_data as $e_key){ 
      $record_id = $e_key->record_id;
      $bg_color = '';
      $travel_mode_id = 0;
      $travel_cost = 0;
      $duration = '--';
      if(strtolower($e_key->activity_type) != 'attendance-out' && $e_key->activity_id != 0){
        $activity_data = get_activity_data($e_key->activity_id, $e_key->activity_type);
        $duration = $activity_data['duration'];
        $travel_mode_id = $activity_data['travel_mode_id'];
        $travel_cost = $activity_data['travel_cost'];
        $previous_travel_mode_id = $travel_mode_id;
      }
      
      if(strtolower($e_key->activity_type) == 'attendance-out'){
				if(empty($e_key->activity_id)){
					$travel_mode_id = $previous_travel_mode_id;
				}
				else{
					$activity_data = get_activity_data($e_key->activity_id, $e_key->activity_type);
					if(!empty($activity_data)){
						$duration = $activity_data['duration'];
						$travel_mode_id = $activity_data['travel_mode_id'];
						$travel_cost = $activity_data['travel_cost'];
					}
					else{
						$travel_mode_id = $previous_travel_mode_id;
					}
				}
      }
      
      /*if($activity_date != date('d-m-Y', strtotime($e_key->tracking_time))){
        $activity_date = date('d-m-Y', strtotime($e_key->tracking_time));
        
        $tacking_data = get_tracking_data($e_key->track_source_id);
        
        $activity_location = trim(strip_tags($tacking_data['tracking_location']));
        $exp_loc = implode(',<br>', explode(',',$activity_location));
        
        ?>
          <tr>
            <td><?=$r?></td>
            <td><? echo get_activity_type($tacking_data['activity_type']);?></td>
            <td><? echo $exp_loc;?></td>
            <td><?php echo date('d-m-Y', strtotime($tacking_data['tracking_time'])); ?></td>
            <td><?php echo date('H:i', strtotime($tacking_data['tracking_time'])); ?></td>
            <td>--</td>
            <td>--</td>
            <td>--</td>
            <td>--</td>
            <td>--</td>
            <!--<td>--</td>-->
            <td>--</td>
          </tr>
        <?
        $r++;
      }*/
      $activity_type = get_activity_type($e_key->activity_type);
      
      $activity_location = trim(strip_tags($e_key->tracking_location));
      $exp_loc = implode(',<br>', explode(',',$activity_location));
      
      $lat_long_location = trim(strip_tags($e_key->lat_long_location));
        
      if($activity_location != $lat_long_location)
      {
        $lat_long_location = implode(',<br>', explode(',',$lat_long_location));
        if($current_role == "super_admin")
          $exp_loc .= '<hr><span style="color:red">'.$lat_long_location.'</span>';
      }
      
      $remarks = ($e_key->remarks != '' ? $e_key->remarks : '&nbsp;');
      
      if($e_key->travel_mode_id == ''){
        $display_buttons = 0;
      }
      
      if(empty($e_key->is_claimed)){
        $is_processed = 0;
      }
      
      //if($e_key->travel_mode_id == '' && $travel_mode_id !='')
      if(empty($e_key->travel_mode_id) && $travel_mode_id !='')
      {
        $e_key->travel_mode_id = $travel_mode_id;
        $e_key->travel_cost = $travel_cost;
      }
      
      //if(strtolower($e_key->activity_type) == 'attendance-out'){
        //$e_key->travel_mode_id = $previous_travel_mode_id;
      //}
      
      $travelCost = '';
      foreach($travels_mode as $travel_mode) {
        if($e_key->travel_mode_id == $travel_mode->id){
          $tdistance = round($e_key->travel_distance);
          $travelCost = $tdistance*$travel_mode->travel_cost;
        }
      }
      
      if(empty($e_key->travel_cost) || $e_key->travel_cost == 0.00)
      {
        $e_key->travel_cost = $travelCost;
      }
      
      $readonly_fields = '';
      $disable_fields = '';
      $bg_color = '';
      if($e_key->claim_discard == 1){
        $readonly_fields = 'readonly';
        $disable_fields = ' disabled="true"';
        //$bg_color = 'style="background-color:#fd6252;"';
      }
      
      $created_date = strtotime(date('Y-m-d', strtotime($e_key->created_at)));
      $current_date = strtotime(date('Y-m-d'));
      $days = (($current_date-$created_date)/(24*60*60));
      
      $not_update = '';
      if($days > 3 && ($current_role != 'accounts')){
				$readonly_fields = 'readonly';
				$not_update = 'not_update';
			}
			
			$net_local_claim_cost = $net_local_claim_cost + round($e_key->travel_cost);
  ?>
		<tr id="row_<?php echo $record_id;?>" <?=$bg_color;?>>
      <td><?=$r;?></td>
			<td><?=$spark_with_names[$e_key->spark_id];?></td>
      <td>
        <input type="hidden" name="record_id[]" value="<?=$record_id?>">
        <?php echo $activity_type; ?>
      </td>
			<td><?php echo $exp_loc; ?></td>
			<td><?php echo date('d-m-Y', strtotime($e_key->tracking_time)); ?></td>
			<td><?php echo date('H:i', strtotime($e_key->tracking_time)); ?></td>
      <td><?=$duration;?></td>
			<td align="right">
				<?php echo round($e_key->travel_distance); ?>
      </td>
			<td>
				<?=(!empty($e_key->manual_distance) && $e_key->manual_distance > 0 ? $e_key->manual_distance : round($e_key->travel_distance));?>
      </td>
			<td>
					<select disabled name="travel_mode[]" id="travel_mode_<?=$record_id?>" alt="<?=$record_id?>" class="sel_mode form-control required">
					  <? if($e_key->travel_mode_id == ''){ ?>
							<option value="">-Mode-</option>
						<?	}else if($e_key->travel_mode_id == '9999'){ ?>
							<option value="9999">With Other</option>
						<? } else { ?>
						<?php foreach($travels_mode as $travel_mode) { 
							if($e_key->travel_mode_id == $travel_mode->id){	?>
								<option value="<?=$travel_mode->id."_".$travel_mode->travel_cost;?>"><?=$travel_mode->travel_mode;?></option>
						<?php } } } ?>  
					</select>
      </td>
      <td><?=round($e_key->travel_cost);?></td>
      <!--<td><?=$remarks;?></td>-->
      <td>
        <?php 
        if($e_key->is_claimed == 1) {  
						$process_date = (!empty($e_key->process_date) ? '('.date('d-m-Y', strtotime($e_key->process_date)).')' : '');
						$user_data = get_spark_data($e_key->updated_by);
            ?><span id="rreason_<?=$record_id?>">Processed <?=$process_date." (By ".$user_data['login_id'].")";?></span><?
        }else if($e_key->claim_discard == 1) { 
					?>  
          <span>Rejected (<?=$e_key->discard_reason;?>)</span>
        <?php }else{ ?>  
					<span>Approved</span>	
				<?php	} ?>	
      </td>      
		</tr>
	<?php $r++; } ?>
  </tbody>
</table>
	
<div class="row"><div class="col-md-12 text-center"  style="background-color: #A6A6A6;"><a href="<?=base_url();?>map/ld_travel_claims"><h4>LD Travel Claim</h4></a></div></div>
<table class="table table-striped table-bordered bootstrap-datatable datatable1 responsive customdatatable withscrolltable ldclaim">
	<thead>
		<tr style="background-color: #d5ecf9;">
			<th align="center">S.No.</th>
			<th align="center">Spark</th>
			<th align="center">Details</th>
			<th align="center">Date<br>(DD:MM:YY)</th>
			<th align="center">Time <br>(HH:MM)</th>
			<th align="center">Duration <br>(HH:MM)</th>
			<th align="center">Distance <br>(KM)</th>
			<th align="center">Manual Distance <br>(KM)</th>
			<th align="center">Travel Mode</th>
			<th align="center">Travel Cost</th>
			<!--<th align="center">Remark</th>-->
      <th align="center">
			<?php if($current_role == 'accounts') { echo 'Action'; }else{ echo 'Account Status'; } ?>
      </th>
		</tr>
	</thead>
	<tbody>

	<?php 
    $r=1;
    foreach($ld_claim_data as $e_key){ 
      $record_id = $e_key->id;
      $bg_color = '';
			$duration = $e_key->duration;
			$travel_mode_id = $e_key->travel_mode_id;
			$travel_cost = $e_key->travel_cost;
			
      $activity_type = 'LD Travel';
      
      $uploadData = get_upload_file(array('claim_type'=>'LD Travel', 'claim_id'=>$e_key->id));
      
      $file_name = '';
      if(!empty($uploadData))
      $file_name = $uploadData[0]->file_name; 
      
      $activity_location = trim(strip_tags($e_key->from_address));
      $exp_loc = '<strong>IN:</strong> '.implode(',<br>', explode(',',$activity_location));
      
      $lat_long_location = trim(strip_tags($e_key->to_address));
        
      //if($activity_location != $lat_long_location)
      //{
        $lat_long_location = implode(',<br>', explode(',',$lat_long_location));
        //if($current_role == "super_admin")
         $exp_loc .= '<hr><strong>OUT:</strong> '.$lat_long_location;
      //}
      
      $status_remark = ($e_key->status_remark != '' ? $e_key->status_remark : '&nbsp;');
      $remarks = ($e_key->remarks != '' ? $e_key->remarks : '&nbsp;');
      
      if($e_key->travel_mode_id == ''){
        $display_buttons = 0;
      }
      
      if($e_key->claim_status == 'approved'){
        $is_processed = 0;
      }
      
      if(empty($e_key->travel_mode_id) && $travel_mode_id !='')
      {
        $e_key->travel_mode_id = $travel_mode_id;
        $e_key->travel_cost = $travel_cost;
      }
      
      $travelCost = '';
      foreach($travels_mode as $travel_mode) {
        if($e_key->travel_mode_id == $travel_mode->id){
          $tdistance = round($e_key->travel_distance);
          $travelCost = $tdistance*$travel_mode->travel_cost;
        }
      }
      
      if(empty($e_key->travel_cost) || $e_key->travel_cost == 0.00)
      {
        $e_key->travel_cost = $travelCost;
      }
      
      $readonly_fields = '';
      $disable_fields = '';
      if($e_key->claim_status == 'rejected'){
        $readonly_fields = 'readonly';
        $disable_fields = ' disabled="true"';
        $bg_color = 'style="background-color:#fd6252;"';
      }
      
      $created_date = strtotime(date('Y-m-d', strtotime($e_key->created_at)));
      $current_date = strtotime(date('Y-m-d'));
      $days = (($current_date-$created_date)/(24*60*60));
      
      $not_update = '';
      if($days > 3 && ($current_role != 'accounts')){
				//$readonly_fields = 'readonly';
				$not_update = 'not_update';
			}
			
			$net_ld_claim_cost = $net_ld_claim_cost + round($e_key->travel_cost);
  ?>
		<tr id="other_activities_row_<?php echo $record_id;?>" <?=$bg_color;?>>
      <td><?=$r;?></td>
      <td><?=$spark_with_names[$e_key->spark_id];?></td>
			<td><?php echo $exp_loc; ?></td>
			<td><?php echo date('d-m-Y', strtotime($e_key->activity_date)); ?></td>
			<td><?php echo date('H:i', strtotime($e_key->activity_date)); ?></td>
      <td><?=$duration;?></td>
			<td align="right">
				<?php echo round($e_key->travel_distance); ?>
      </td>
			<td><? echo (!empty($e_key->manual_distance) && $e_key->manual_distance > 0 ? $e_key->manual_distance : round($e_key->travel_distance));?>
      </td>
      <td>				
					<select disabled name="travel_mode[]" id="travel_mode_<?=$record_id?>" alt="<?=$record_id?>" class="sel_mode form-control required">
					  <? if($e_key->travel_mode_id == ''){ ?>
							<option value="">-Mode-</option>
						<?	}else if($e_key->travel_mode_id == '9999'){ ?>
							<option value="9999">With Other</option>
						<? } else { ?>
						<?php foreach($travels_mode as $travel_mode) { 
							if($e_key->travel_mode_id == $travel_mode->id){	?>
								<option value="<?=$travel_mode->id."_".$travel_mode->travel_cost;?>"><?=$travel_mode->travel_mode;?></option>
						<?php } } } ?>  
					</select>
      </td>
      <td><?=round($e_key->travel_cost);?></td>
      <!--<td><?=$remarks;?></td>-->
      <td>
        <div><span id="other_claims_rreason_<?=$record_id?>"><?php echo ($e_key->status_remark != '' ? ucfirst($e_key->claim_status)." (".$e_key->status_remark.")" : ucfirst($e_key->claim_status));?></span></div>  
      </td>
		</tr>
	<?php $r++; }  ?>
  </tbody>
</table>  

<div class="row"><div class="col-md-12 text-center"  style="background-color: #A6A6A6;"><a href="<?=base_url();?>map/stay_claims_list"><h4>Stay Claim</h4></a></div></div>
<table class="table table-striped table-bordered bootstrap-datatable datatable1 responsive customdatatable withscrolltable stclaim">
	<thead>
		<tr style="background-color: #d5ecf9;">
			<th align="center">S.No.</th>
			<th align="center">Spark</th>
			<th align="center">Claim Duration</th>
			<th align="center">Activity</th>
			<th align="center">Stay With</th>
			<th align="center">Bill Details</th>
			<th align="center">Bill Cost</th>
			<th align="center">Claim Cost</th>
			<!--<th align="center">Remark</th>-->
      <th align="center">
			<?php if($current_role == 'accounts') { echo 'Action'; }else{ echo 'Account Status'; } ?>
      </th>
		</tr>
	</thead>
	<tbody>
	<?php 
		$r=1;
    foreach($stay_claim_data as $e_key){ 

      $bg_color = '';
      $record_id = $e_key->id;
      $comment = ($e_key->comment != '' ? $e_key->comment : '&nbsp;');
      $readonly_fields = '';
      $disable_fields = '';
      if($e_key->claim_status == 'rejected'){
        $readonly_fields = 'readonly';
        $disable_fields = ' disabled="true"';
        $bg_color = 'style="background-color:#fd6252;"';
      }
      
      $created_date = strtotime(date('Y-m-d', strtotime($e_key->created_on)));
      $current_date = strtotime(date('Y-m-d'));
      $days = (($current_date-$created_date)/(24*60*60));
      
      if(strtotime($e_key->from_date) != strtotime($e_key->to_date))
				$claim_duration = date('d-m-Y', strtotime($e_key->from_date))." to ".date('d-m-Y', strtotime($e_key->to_date));
			else	
				$claim_duration = date('d-m-Y', strtotime($e_key->from_date));
      
      $not_update = '';
      if($days > 15 && ($current_role != 'accounts')){
				$readonly_fields = 'readonly';
				$not_update = 'not_update';
			}
			
			$stay_activity = json_decode($e_key->stay_reason);
			$stay_with = json_decode($e_key->paid_spark_id);
			$spark_names = array();
			if(!empty($stay_with))
			{
				foreach($stay_with as $stays)
				{
					$spark_names[] = $spark_with_names[$stays];	
				}
			}
			if($e_key->file_name == '' && $current_role == 'accounts' && $e_key->claim_amount !=0) {  
					// not display
			}
			else{
			
			if($e_key->claim_status == 'approved'){ 
				$is_processed = 0;
			}
				
			$net_stay_bill_cost = $net_stay_bill_cost + $e_key->manual_cost;	
			$net_stay_claim_cost = $net_stay_claim_cost + $e_key->claim_amount;	
  ?>
		<tr id="stay_claims_row_<?php echo $record_id;?>" <?=$bg_color;?>>
      <td><?=$r;?></td>
      <td><?=$spark_with_names[$e_key->spark_id];?></td>
      <td><?=$claim_duration;?></td>
			<td><?php echo implode(', ', $stay_activity); ?></td>
			<td><?php echo (!empty($spark_names) ? implode(',<br/>',$spark_names) : '');; ?></td>
			<td><?php echo '<strong>Bill No.:</strong> '.$e_key->bill_number.'<br /><strong>Bill Date:</strong> '.date('d-m-Y', strtotime($e_key->bill_date)); ?></td>
			<td><?php echo $e_key->manual_cost; ?></td>
			<td><?php echo $e_key->claim_amount; ?></td>
      <!--<td><?=$e_key->comment;?></td>-->
			<td>
        <div><span id="other_claims_rreason_<?=$record_id?>"><?php echo ($e_key->status_remark != '' ? ucfirst($e_key->claim_status)." (".$e_key->status_remark.")" : ucfirst($e_key->claim_status));?></span></div>  
      </td>
		</tr>
	<?php } $r++; }  ?>
  </tbody>
</table>  
	
<div class="row"><div class="col-md-12 text-center"  style="background-color: #A6A6A6;"><a href="<?=base_url();?>map/food_claims_list"><h4>Food Claim</h4></a></div></div>
<table class="table table-striped table-bordered bootstrap-datatable datatable1 responsive customdatatable withscrolltable fdclaim">
	<thead>
		<tr style="background-color: #d5ecf9;">
			<th align="center">S.No.</th>
			<th align="center">Spark</th>
			<th align="center">Claim Duration</th>
			<th align="center">Activity</th>
			<th align="center">Stay With</th>
			<th align="center">Actual Cost</th>
			<th align="center">Manual Cost</th>
			<!--<th align="center">Remark</th>-->
      <th align="center">
			<?php if($current_role == 'accounts') { echo 'Action'; }else{ echo 'Account Status'; } ?>
      </th>
		</tr>
	</thead>
	<tbody>
	<?php 
		$r = 1;
    foreach($food_claim_data as $e_key){ 

      $bg_color = '';
      $record_id = $e_key->id;
      $comment = ($e_key->comment != '' ? $e_key->comment : '&nbsp;');
      $readonly_fields = '';
      $disable_fields = '';
      if($e_key->claim_status == 'rejected'){
        $readonly_fields = 'readonly';
        $disable_fields = ' disabled="true"';
        $bg_color = 'style="background-color:#fd6252;"';
      }
      
      $created_date = strtotime(date('Y-m-d', strtotime($e_key->created_on)));
      $current_date = strtotime(date('Y-m-d'));
      $days = (($current_date-$created_date)/(24*60*60));
      
      if(strtotime($e_key->from_date) != strtotime($e_key->to_date))
				$claim_duration = date('d-m-Y', strtotime($e_key->from_date))." to ".date('d-m-Y', strtotime($e_key->to_date));
			else	
				$claim_duration = date('d-m-Y', strtotime($e_key->from_date));
      
      $not_update = '';
      if($days > 3 && ($current_role != 'accounts')){
				$readonly_fields = 'readonly';
				$not_update = 'not_update';
			}
			if($e_key->claim_status == 'approved'){ 
				$is_processed = 0;
			}
			
			$stay_activity = json_decode($e_key->stay_reason);
			$stay_with = json_decode($e_key->paid_spark_id);
			$spark_names = array();
			if(!empty($stay_with))
			{
				foreach($stay_with as $stays)
				{
					$spark_names[] = $spark_with_names[$stays];	
				}
			}
			$net_food_claim_cost = $net_food_claim_cost + $e_key->manual_cost;	
			$net_food_actual_claim_cost = $net_food_actual_claim_cost + $e_key->actual_cost;	
  ?>
		<tr id="food_claims_row_<?php echo $record_id;?>" <?=$bg_color;?>>
      <td><?=$r;?></td>
      <td><?=$spark_with_names[$e_key->spark_id];?></td>
      <td><?=$claim_duration;?></td>
			<td><?php echo implode(', ', $stay_activity); ?></td>
			<td><?php echo (!empty($spark_names) ? implode(',<br/>',$spark_names) : '');; ?></td>
			<td><?php echo $e_key->actual_cost; ?></td>
			<td><?php echo $e_key->manual_cost; ?>
			</td>
      <!--<td><?=$e_key->comment;?></td>-->
			<td>
        <div><span id="other_claims_rreason_<?=$record_id?>"><?php echo ($e_key->status_remark != '' ? ucfirst($e_key->claim_status)." (".$e_key->status_remark.")" : ucfirst($e_key->claim_status));?></span></div>  
      </td>
		</tr>
	<?php $r++; }  ?>
  </tbody>
</table>

<div class="row"><div class="col-md-12 text-center"  style="background-color: #A6A6A6;"><a href="<?=base_url();?>map/other_claims_list"><h4>Other Claim</h4></a></div></div>
<table class="table table-striped table-bordered bootstrap-datatable datatable1 responsive customdatatable withscrolltable otclaim">
	<thead>
		<tr style="background-color: #d5ecf9;">
			<th align="center">S.No.</th>
			<th align="center">Spark</th>
			<th align="center">Claim Duration</th>
			<th align="center">Claim Type</th>
			<th align="center">Bill Number</th>
			<th align="center">Bill Date</th>
			<th align="center">Cost</th>
			<th align="center">Created On</th>
			<!--<th align="center">Remark</th>-->
      <th align="center">
			<?php if($current_role == 'accounts') { echo 'Action'; }else{ echo 'Account Status'; } ?>
      </th>
		</tr>
	</thead>
	
	<tbody>
	<?php 
    $r=1;
    foreach($other_claim_data as $e_key){ 

      $bg_color = '';
      $record_id = $e_key->id;
      $comment = ($e_key->comment != '' ? $e_key->comment : '&nbsp;');
      $readonly_fields = '';
      $disable_fields = '';
      if($e_key->claim_status == 'rejected'){
        $readonly_fields = 'readonly';
        $disable_fields = ' disabled="true"';
        $bg_color = 'style="background-color:#fd6252;"';
      }
      
      $created_date = strtotime(date('Y-m-d', strtotime($e_key->created_on)));
      $current_date = strtotime(date('Y-m-d'));
      $days = (($current_date-$created_date)/(24*60*60));
      
      if(strtotime($e_key->from_date) != strtotime($e_key->to_date))
				$claim_duration = date('d-m-Y', strtotime($e_key->from_date))." to ".date('d-m-Y', strtotime($e_key->to_date));
			else	
				$claim_duration = date('d-m-Y', strtotime($e_key->from_date));
      
      $not_update = '';
      if($days > 3 && ($current_role != 'accounts')){
				$readonly_fields = 'readonly';
				$not_update = 'not_update';
			}
			
			if($e_key->claim_status == 'approved'){ 
				$is_processed = 0;
			}
			$net_other_claim_cost = $net_other_claim_cost + $e_key->amount;
  ?>
		<tr id="other_claims_row_<?php echo $record_id;?>" <?=$bg_color;?>>
      <td><?=$r;?></td>
      <td><?=$spark_with_names[$e_key->spark_id];?></td>
      <td><?=$claim_duration;?></td>
			<td><?php echo ucfirst($e_key->claim_type); ?></td>
			<td><?php echo $e_key->bill_number; ?></td>
			<td><?php echo date('d-m-Y', strtotime($e_key->bill_date)); ?></td>
			<td><?php echo $e_key->amount; ?></td>
			<td><?php echo date('d-m-Y', strtotime($e_key->created_on)); ?></td>
      <!--<td><?=$e_key->comment;?></td>-->
			<td>
        <div><span id="other_claims_rreason_<?=$record_id?>"><?php echo ($e_key->status_remark != '' ? ucfirst($e_key->claim_status)." (".$e_key->status_remark.")" : ucfirst($e_key->claim_status));?></span></div> 
      </td>
		</tr>
	<?php $r++; }  ?>
	</tbody>
</table>	
<?
	$net_claim_cost = $net_ld_claim_cost + $net_stay_claim_cost + $net_food_claim_cost + $net_other_claim_cost;
?>
<div class="row"><div class="col-md-12 text-center"  style="background-color: #A6A6A6;"><h4>Leaves Details</h4></div></div>
<div class="scrollme">
<table class="table table-striped table-bordered bootstrap-datatable datatable1 responsive">
	<tbody>
		<tr>
			<th><span style="font-weight: bold;">Spark Name</span></th>
			<th><span style="font-weight: bold;">Leave Date</span></th>
			<th><span style="font-weight: bold;">Activity Details</span></th>
			<th><span style="font-weight: bold;">Status</span></th>
		</tr>
      <? $new = 1; ?>
            
      <? if (count($activities) == 0) { ?>
    
      <? } else { ?>
        <? foreach($activities as $activity) { ?>
        <tr valign="top">
          <td><?=$spark_with_names[$activity['user_id']]?></td>
          <td><?=date("d-m-Y",strtotime($activity['leave_from_date']))?></td>
          <td><?=$activity['details']?></td>
          <td id="status_cell"><?php echo strtoupper($activity['status']); ?>
          </td>
        </tr>
        <? $new = 0; ?>
        <? } ?>
      <? } ?>

	</tbody>
</table>
</div>
<!--END OTHER CLAIM -->
</div>
