<div class="row">
	<div class="form-group col-md-6">
		<a href="<?php echo base_url();?>activity/attendance_regularizations" class="btn btn-success btn-sm">Regularizations List</a>
	</div>	
</div>	
<form name="frmattendance" action="" method="post" onsubmit="return validatereport()">
	<?php if($current_role == 'super_admin' || $current_role == 'HR') { ?>
		<div class="row">
			<div class="form-group col-md-3">Spark</div>
			<div class="form-group col-md-6">
			<select class="form-control" id="spark_id" name="spark_id">
				<option value="">Select Spark</option>
				<?php foreach($sparks as $spark) { ?>
				<option value="<?=$spark->id?>" <? echo (!empty($sel_spark_id) && $sel_spark_id == $spark->id ? 'selected' : '') ?>><?php echo $spark->name." (".$spark->login_id.")"?></option>
				<?php } ?>
			</select>
			</div>
		 </div>	
   <?	}else{	?>
		<input type="hidden" id="spark_id" name="spark_id" class="form-control" value="<?=$current_user_id?>">
	 <?	}	?>	 
	 <div class="row">
		<div class="form-group col-md-3">Attendance Date</div> 
		<div class="form-group col-md-6">
			<input type="text" id="attendancedate" name="attendancedate" autocomplete="off" class="form-control" value="<? echo (!empty($attendancedate) ? $attendancedate : '');?>" placeholder="Select Attendance Date">
		</div>
   </div>
   <div class="row">
			<div class="form-group col-md-3">Attendance Type</div>
			<div class="form-group col-md-6">
				<select class="form-control" id="attendance_type" name="attendance_type">
					<option value="">Select Type</option>
					<?php
						foreach($ATTENDANCE_REG_TYPE as $key=>$value)
						{
							?><option value="<?=$key?>" <? echo (!empty($attendance_type) && $attendance_type == $key ? 'selected' : '');?>><?=$value?></option><?
						}
					?>
				</select>
			</div>
		</div>
		<div class="row" id="div_day_type">
			<label class="col-md-3" for="title">Reason</label>
			<div class="form-group col-md-6">
				 <textarea name="reason" class="form-control" id="reason" maxlength="150"></textarea>
			</div>
		</div>
		
	  <div class="row">
		<div class="form-group col-md-9 text-center">
			<input type="submit" name="submit" value="Save" class="btn btn-sm btn-info">
		</div>
	 </div>
</form>

<script>
$("#spark_state_id").change(function() {
  get_all_user_list($(this).val())
});

function get_all_user_list(ssid,sid = 0)
{
  $.ajax({
    url: '<?=base_url()?>dashboard/getSparkFromState',
    type: 'get',
    data: {
      state: ssid
    },
    dataType: 'json',
    success: function(response) {
      $("#spark_id").html('');
      $("#spark_id").html("<option value=''>Select Spark</option>");
      $.each(response, function() {
        if (sid > 0 && sid == this['id'])
          $("#spark_id").append("<option selected value='" + this['id']+"-"+ this['name'] +"-" +this['state_id']+"'>" + this['name'] + "</option>");
        else
          $("#spark_id").append("<option value='" + this['id']+"-"+ this['name'] +"-" +this['state_id']+"'>" + this['name'] + "</option>");
      });
      $("#spark_id").show();
    },
    error: function(response) {
      window.console.log(response);
    }
  });
}
	
$("#attendancedate" ).datepicker({
	maxDate: "D",
	minDate: "-1M",
  dateFormat: "dd-mm-yy"
});
	
function validatereport()
{
  if ($("#state_id").val() == "")
  {
    alert("Please select state")
    return false;
  }
  if ($("#user_login_id").val() == "")
  {
    alert("Please select spark")
    return false;
  }
  if ($("#attendancedate").val() == "")
  {
    alert("Please attendance date")
    return false;
  }
    if ($("#start_year").val() == "")
  {
    alert("Please select year")
    return false;
  }
  return true
}

</script>
