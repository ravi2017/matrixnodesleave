<?
		error_reporting(0);
    ini_set('display_errors', 'Off');
?>

<? if(!isset($download) && !isset($pdf)) {?>

<div class="box col-md-12">
  <div class="box-inner">
    <form name="frmreport" action="" method="post" onsubmit="return validatereport()">
        <div class="row">
          <?php if($current_role == 'super_admin' || $current_role == 'accounts' || $current_role == 'reports') { ?>
          <div class="form-group col-md-3 field_user">
            <select class="form-control" id="state_id" name="state_id">
              <option value="">Select State</option>
              <? foreach($states as $s) { ?>
              <option value="<?=$s->id?>" <?=(isset($state_id) and $s->id == $state_id) ? "selected" : ""?>><?=$s->name?></option>
              <? } ?>
            </select>
          </div>
          <?  }else{  ?>
            <input type="hidden" name="state_id" id="state_id" value="<?=$user_state_id; ?>">
          <? } ?>  
          <div class="form-group col-md-2 field_user">
            <select class="form-control" id="monthno" name="monthno">
              <option value="">Month</option>
                <? foreach($month_arr as $key=>$value) { ?>
                  <option value="<?=$key?>" <? if(isset($month) && $month == $key){ echo 'selected'; }?>><?=$value?></option>
                <? } ?>
            </select>
          </div>
          <div class="form-group col-md-2 field_user">
            <select class="form-control" id="year" name="year">
              <option value="">Year</option>
                <? foreach($year_arr as $year_val) { ?>
                  <option value="<?=$year_val?>" <? if(isset($year) && $year == $year_val){ echo 'selected'; }?>><?=$year_val?></option>
                <? } ?>
            </select>
          </div>
          <div class="form-group col-md-2 field_user">
            <select class="form-control" id="duration" name="duration">
              <option value="">Month/Year</option>
              <option value="month_value" <? if($duration == 'month_value') { echo 'selected'; } ?>>Month</option>
              <option value="year_value" <? if($duration == 'year_value') { echo 'selected'; } ?>>Year</option>
            </select>
          </div>
          <div class="form-group col-md-1 col-md-offset-1 text-center">
            <input type="submit" name="submit" value="Get Report" class="btn btn-sm btn-success">
          </div>
      </div>
    </form>
    
    <div class="form-group row">
      <div class="form-group col-md-3">
      <form name="frmreport" action="<?php echo base_url();?>activity/baithak_summary" method="post">
          <input type="hidden" name="monthno" value="<? echo (!empty($month) ? $month : '');?>">
          <input type="hidden" name="year" value="<? echo (!empty($year) ? $year : '');?>">
          <input type="hidden" name="state_id" value="<?=$state_id?>">
          <input type="hidden" name="duration" value="<?=$duration?>">
          <input type="hidden" name="download" value="1">
          <button type="submit" name="submit" value="Get Report" class="btn btn-success">Download Report</button>
      </form>
      </div>
      <div class="form-group col-md-3">
      <form name="frmreport" action="<?php echo base_url();?>activity/baithak_summary" method="post">
          <input type="hidden" name="monthno" value="<? echo (!empty($month) ? $month : '');?>">
          <input type="hidden" name="year" value="<? echo (!empty($year) ? $year : '');?>">
          <input type="hidden" name="state_id" value="<?=$state_id?>">
          <input type="hidden" name="duration" value="<?=$duration?>">
          <input type="hidden" name="pdf" value="1">
        <button type="submit" name="submit" value="Get Report" class="btn btn-success">Download PDF</button>
      </form>
      </div>
    </div>
    
    <div class="form-group">
    <form id="dasboard_data" action="<?=base_url();?>dashboard/state_dashboard" method="post" target='_blank'>
        <input type="hidden" name="start_month" value="<? echo (!empty($month) ? $month : '');?>">
        <input type="hidden" name="start_year" value="<? echo (!empty($year) ? $year : '');?>">
        <? if($current_role == 'super_admin' || $current_role == 'accounts' || $current_role == 'reports') { ?>
        <input type="hidden" name="spark_state_id" value="<?=$state_id?>">
        <input type="hidden" id="spark_id" name="spark_id" value=''>
        <?  }else if($current_role == 'state_person' || $current_role == 'manager') { ?>
          <input type="hidden" name="spark_state_id" value="<?=$state_id?>">
          <input type="hidden" id="spark_id" name="sel_spark" value=''>
        <?  } ?>
        <input type="hidden" name="dashboard_summary" value="1">
    </form>
    
  </div>
</div>

<? } ?>

<div class="scrollme">
  <? if(!isset($download) && !isset($pdf)) {?>
  <table class="table table-striped table-bordered bootstrap-datatable datatable1 responsive customdatatable" width="100%">
  <? }else{ ?>
    <table cellspacing=0 cellpadding=0 border=1>
  <?  } ?>  
	<thead>
    <tr style="background-color: #d5ecf9;">
      <th rowspan=2>Spark</th>
      <? if($duration == '') { ?><th rowspan=2>Duration</th> <? } ?>
      <th colspan=2>Baithak</th>
      <th rowspan=2>% Schools <br /> registered</th>
      <th rowspan=2>Registered <br /> Teachers/Total schools</th>
      <th rowspan=2>Teacher <br /> Engagement/Total Teacher</th>
      <th rowspan=2>Spark <br /> Engagement</th>
      <th colspan=2>National</th>
			<th colspan=2>State</th>
      
		</tr>
		<tr style="background-color: #d5ecf9;">
      <th title="Score">S</th><th title="Rating">R</th>
			<th>Rank</th><th>Rating</th>
			<th>Rank</th><th>Rating</th>
		</tr>
	</thead>
  <tbody>
    <?php $i=1; 
    if(!empty($data_arr)){
      
      foreach($data_arr as $sparks_id=>$data) { ?>
        <? if($duration == '' || $duration == 'month_value') { 
           
          $baithak_state_data =  return_rating_val_summary($data['month_value'][$field_level_ids['baithak_state_rating']], $data['month_value'][$field_level_ids['baithak_state_rank']]);
          $baithak_data =  return_rating_val_summary($data['month_value'][$field_level_ids['baithak_rating']], $data['month_value'][$field_level_ids['baithak_rank']]);
          
          $m_baithak_score = get_baithak_weitage_value('school_registered', $data['month_value'][$field_level_ids['school_registered']])+get_baithak_weitage_value('registered_teacher', $data['month_value'][$field_level_ids['registered_teacher']])+get_baithak_weitage_value('avg_teacher_engagement', $data['month_value'][$field_level_ids['avg_teacher_engagement']])+get_baithak_weitage_value('spark_engagement', $data['month_value'][$field_level_ids['spark_engagement']]);
					
          $NOR_ranking = check_ranking($data['month_value'][$field_level_ids['baithak_rank']]);
          $NOR_data =  return_rating_val_summary($data['month_value'][$field_level_ids['baithak_rating']], $data['month_value'][$field_level_ids['baithak_rank']]);                            
          
          //$month_color = (int) $data['month_value'][$field_level_ids['SPARK COLOR']];
          //$index_month_color = return_index_color($month_color);
          
          $td_bg_color = '';
        ?>
        <tr>
          <td <? if($duration == '') { ?> rowspan=2 <? } ?> style="background-color: #FFF;vertical-align:middle">
          <? if(!isset($download) && !isset($pdf)) {?>
						<a href="javascript:void(0);" onClick="dashboard_data('<?=$sparks_id?>');"><?=$sparks_name[$sparks_id]; ?></a>
          <?	}else{	?>
						<?=$sparks_name[$sparks_id]; ?>
					<?	}	?>	
          </td>
          <? if($duration == '') { ?> <td>Month</td> <? } ?>
          <td <?=$td_bg_color;?>><?php echo (int) $m_baithak_score;?></td>
          <td <?=$td_bg_color;?>><?php echo (int) $data['month_value'][$field_level_ids['baithak_rank']];?></td>
          
          <td <?=$td_bg_color;?>><?php echo $data['month_value'][$field_level_ids['school_registered']]?></td>
          <td <?=$td_bg_color;?>><?php echo $data['month_value'][$field_level_ids['registered_teacher']]?></td>
          <td <?=$td_bg_color;?>><?php echo $data['month_value'][$field_level_ids['avg_teacher_engagement']]?></td>
          <td <?=$td_bg_color;?>><?php echo $data['month_value'][$field_level_ids['spark_engagement']]?></td>
          
          <td class=" <?php echo $baithak_data['color'];?>" <?=$td_bg_color;?>><?php echo check_ranking($data['month_value'][$field_level_ids['baithak_rank']]); //check_ranking($avg_rank); ?></td>
          <td <?=$td_bg_color;?>><?php echo $baithak_data['value'];?></td>
          <td class="<?php echo $baithak_state_data['color'];?>" <?=$td_bg_color;?>><?php echo check_ranking($data['month_value'][$field_level_ids['baithak_state_rank']]);?></td>
          <td <?=$td_bg_color;?>><?php echo $baithak_state_data['value'];?></td>

        </tr>
        <?  } ?>
        <? if($duration == '' || $duration == 'year_value') { 
            
          $baithak_state_data =  return_rating_val_summary($data['year_value'][$field_level_ids['baithak_state_rating']], $data['year_value'][$field_level_ids['baithak_state_rank']]);
          $baithak_data =  return_rating_val_summary($data['year_value'][$field_level_ids['baithak_rating']], $data['year_value'][$field_level_ids['baithak_rank']]);
          
          $y_baithak_score = get_baithak_weitage_value('school_registered', $data['year_value'][$field_level_ids['school_registered']])+get_baithak_weitage_value('registered_teacher', $data['year_value'][$field_level_ids['registered_teacher']])+get_baithak_weitage_value('avg_teacher_engagement', $data['year_value'][$field_level_ids['avg_teacher_engagement']])+get_baithak_weitage_value('spark_engagement', $data['year_value'][$field_level_ids['spark_engagement']]);
          
          //$NOR_ranking = check_ranking($data['year_value'][$field_level_ids['baithak_rank']]);
          //$NOR_data =  return_rating_val_summary($data['year_value'][$field_level_ids['baithak_rating']], $data['year_value'][$field_level_ids['baithak_rank']]);                            
          
          $td_bg_color = '';
        ?>
        <tr>
          <? if($duration != '') { ?>
          <td style="background-color: #FFF;vertical-align:middle"><?=$sparks_name[$sparks_id];?></td>
          <? }else{ ?>
          <td>Year</td>
          <?  } ?>
          <td <?=$td_bg_color;?>><?php echo (int) $y_baithak_score;?></td>
          <td <?=$td_bg_color;?>><?php echo check_ranking($data['year_value'][$field_level_ids['baithak_rank']]);?></td>
          
          <td <?=$td_bg_color;?>><?php echo $data['year_value'][$field_level_ids['school_registered']]?></td>
          <td <?=$td_bg_color;?>><?php echo $data['year_value'][$field_level_ids['registered_teacher']]?></td>
          <td <?=$td_bg_color;?>><?php echo $data['year_value'][$field_level_ids['avg_teacher_engagement']]?></td>
          <td <?=$td_bg_color;?>><?php echo $data['year_value'][$field_level_ids['spark_engagement']]?></td>
          
          <td class=" <?php echo $baithak_data['color'];?>" <?=$td_bg_color;?>><?php echo check_ranking($data['year_value'][$field_level_ids['baithak_rank']]); //check_ranking($avg_rank); ?></td>
          <td <?=$td_bg_color;?>><?php echo $baithak_data['value'];?></td>
          <td class="<?php echo $baithak_state_data['color'];?>" <?=$td_bg_color;?>><?php echo check_ranking($data['year_value'][$field_level_ids['baithak_state_rank']]);?></td>
          <td <?=$td_bg_color;?>><?php echo $baithak_state_data['value'];?></td>

        </tr>
        <?  } ?>
        <?
       $i++; 
      }
    }else{
      ?>
      <tr><td colspan="19" align="center">No Record Found</td></tr>
      <?
    }
    ?>  
  </tbody>  
</table>  
</div>

<script type="text/javascript">
  function validatereport()
  {
    if ($("#state_id").val() == "")
    {
      alert("Please select state")
      return false;
    }
    if ($("#monthno").val() == "")
    {
      alert("Please select month")
      return false;
    }
      if ($("#year").val() == "")
    {
      alert("Please select year")
      return false;
    }

    return true
  }    
  
  function dashboard_data(sid)
  {
    $("#spark_id").val(sid);
    $("#dasboard_data").submit();
  }
</script>
<style>
tr th{
  text-align:center;
  vertical-align:top;
}
.light-grey{
  background-color:#EFEFEF;
}
.score-red{
  background-color:red;
}
</style>
