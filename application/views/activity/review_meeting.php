<div class="row">
  <div class="col-md-12">
    <? if($activity_count > MONTHLY_WEB_ACTIVITY_LIMIT) { ?>
      <div class="row">
        <div class="col-md-12 text-center text-danger">
           <h6>Your this month limit exceed. Please contact to admin.</h6>
        </div> 
      </div> 
    <? }else{ ?>  
    <form method="post" action="<?php echo base_url();?>activity/review_meeting" role="form" name="frmreviewmeeting" id="frmreviewmeeting" onSubmit="return validatefrm()">
      <div class="row">
        <div class="form-group col-md-3">
          <label for="title">Date Of Meeting</label>
          <?php if($mode == 'edit'){ ?>
            <div class='input-group date'>
              <input type="text" name="meeting_date" value="<?=date('Y-m-d', strtotime($activity_date));?>" readonly class="form-control required" id="meeting_date" />
              <span class="input-group-addon">
                <span class="ion-calendar"></span>
              </span>
            </div>
          <?php } else { ?>  
            <div class='input-group date' id='datetimepicker1'>
              <input type="text" name="meeting_date" value="" class="form-control required" id="meeting_date" autocomplete="off" />
              <span class="input-group-addon">
                <span class="ion-calendar"></span>
              </span>
            </div>
          <?php } ?>
            <span class="err" id="err_date"></span>
        </div>
        <div class="form-group col-md-3">
          <label for="date">Time Of Meeting</label>
          <div class='input-group date' id='datetimepicker5'>
              <input type="text" name="meeting_time" value="" class="form-control required" id="meeting_time"  autocomplete="off" />
              <span class="input-group-addon">
                <span class="ion-clock"></span>
              </span>
          </div>
        </div>
        <div class="form-group col-md-3">
          <label for="title">Duration</label>
          <div class="duration"></div>
          <br><span class="err" id="err_duration"></span>
        </div>
        <div class="form-group col-md-3">
          <label for="title">Number Of Attendes</label>
          <input type="text" name="number_of_attendes" id="number_of_attendes" class="form-control"><span class="err" id="err_attendes"></span>
        </div>
      </div>
      
      <div class="row">
        <div class="form-group col-md-3">
          <label for="date">Address</label>
          <input type="text" name="location" value="" class="form-control required" id="location"  autocomplete="off" />
        </div>
        <div class="form-group col-md-3">
          <label for="date">Meet With</label>
          <select id="meet_with" name="meet_with">
            <? foreach($MEET_WITH as $key=>$value) { ?>
              <option value="<?=$key?>"><?=$value?></option>
            <? } ?>  
          </select>
        </div>
        <div class="form-group col-md-3">
          <label for="title">Is Meeting Internal</label>
          <input type="checkbox" class="form-control" name="internal_meeting" id="internal_meeting">
        </div>
      </div>      
      
      <div id='external_meeting'>
        <div class="row">
          <div class="form-group col-md-3">
            <label for="title">District</label>
            <select id="district_id" name="district_id" class="ext_meet_div form-control required">
              <option value="">-Select District-</option>
              <?php foreach($user_districts as $user_district){ ?>
                <option value="<?php echo $user_district->id;?>"><?php echo $user_district->name;?></option>
              <? } ?>  
              <option value="other">Other</option>
            </select>
            <span class="err" id="err_district"></span>
          </div>
          <div class="form-group col-md-3 other_district_div" style="display:none;">
            <label for="state">State</label>
            <select id="state_id" name="state_id" class="ext_meet_div other_state form-control required">
              <option value="">-Select State-</option>
            </select>
            <span class="err" id="err_state"></span>
          </div>
          <div class="form-group col-md-3 other_district_div" style="display:none;">
            <label for="district">District</label>
            <select id="other_district_id" name="other_district_id" class="ext_meet_div form-control required">
              <option value="">-Select District-</option>
            </select>
            <span class="err" id="err_other_district"></span>
          </div>
          
          <div class="form-group col-md-3">
            <label for="title">Block</label>
            <select id="block_id" name="block_id" class="ext_meet_div form-control">
              <option value="0">-Select Block-</option>  
            </select>
          </div>
          <div class="form-group col-md-3">
            <label for="title">Cluster</label>
            <select id="cluster_id" name="cluster_id" class="ext_meet_div form-control" >
              <option value="0">-Select Cluster-</option>  
            </select>
          </div>
        </div>
      </div>
    
      
      <div class="row">
        <div class="form-group col-md-3">
          <label for="date">Travel Mode</label>
          <select id="travel_mode" name="travel_mode">
            <? foreach($travel_modes as $travel_mode) { ?>
              <option value="<?=$travel_mode->travel_mode?>" autc="<?=$travel_mode->is_autocalculated?>"><?=$travel_mode->travel_mode?></option>
            <? } ?>  
          </select>
        </div>
        <div class="form-group col-md-3 tcost">
          <label for="date">Travel Cost</label>
          <input type="text" id="travel_cost" name="travel_cost" value="" autocomplete="off" class="form-control">
        </div>
      </div>
      
      <?php echo $this->load->view('activity/observations','',true) ?>
        
      <div class="form-group row">
        <input type="hidden" name="mode" id="mode" value="<?=$mode?>" />
        <input type="hidden" name="edit_id" value="<?=$edit_id?>" />
        <div class="col-md-6"><br>
          <a class="btn btn-sm btn-danger f-left" href="<?=base_url()?>activity">Cancel</a>
        </div>
        <div class="col-md-6"><br>
          <button type="submit" class="btn btn-sm btn-success f-right" name="Submit">Submit</button>
        </div>
      </div>
    </form>
    <?  } ?>
  </div>
</div>

<script type="text/javascript" src="<? echo base_url(); ?>assets/v2/js/activity.js"></script>
<script type="text/javascript">

 $(document).ready(function ()
{
    var options1 = {
      hour: {
          value: 0,
          min: 0,
          max: 24,
          step: 1,
          symbol: "hrs"
      },
      minute: {
          value: 0,
          min: 0,
          max: 60,
          step: 5,
          symbol: "mins"
      },
      direction: "increment", // increment or decrement
      inputHourTextbox: null, // hour textbox
      inputMinuteTextbox: null, // minutes textbox
      postfixText: "", // text to display after the input fields
      numberPaddingChar: '0' // number left padding character ex: 00052
    };

    $(".duration").timesetter(options1);

    //var date = new Date();
    //date.setDate(date.getDate() - 2);
    var cur_date = new Date();
    cur_date.setDate(cur_date.getDate() - 2);
    var pre_date = new Date();
    pre_date.setDate(pre_date.getDate() - 8);
              
    $('#datetimepicker1 #meeting_date').datetimepicker({
      timepicker: false,
      format: "m/d/Y",
      minDate:pre_date,
      maxDate:cur_date
    });

    $('#datetimepicker5 #meeting_time').datetimepicker({
      datepicker: false,
      format: "H:i"
    });
    
    $('#datetimepicker1 #meeting_date').change(function(){
       var sel_date = this.value;
       if((new Date(sel_date) < new Date(pre_date)) || (new Date(sel_date) > new Date(cur_date))){
          $("#meeting_date").val('');
       }
    });
    
});

function validatefrm()
{
  $("#err_date").html('');
  $("#err_district").html('');
  $("#err_block").html('');
  $("#err_cluster").html('');
  $("#err_duration").html('');

  var error = 0;
  if($("#meeting_date").val() == ''){
    $("#err_date").html('Required meeting date.');
    error = 1;
  }
  if ($("#internal_meeting").prop('checked')== false){ 
    if($("#district_id").val() == ''){
        $("#err_district").html('Please select district.');
        error = 1;
      }
      
    if($("#district_id").val() == 'other'){
      if($("#state_id").val() == ''){
        $("#err_state").html('Please select state.');
        error = 1;
      }
      if($("#other_district_id").val() == ''){
        $("#err_other_district").html('Please select district.');
        error = 1;
      }
    }
    /*if($("#block_id").val() == ''){
      $("#err_block").html('Please select block.');
      error = 1;
    }
    if($("#cluster_id").val() == ''){
      $("#err_cluster").html('Please select cluster.');
      error = 1;
    }*/
  }
  if($("#number_of_attendes").val() == ''){
    $("#err_attendes").html('Requried number of attendes.');
    error = 1;
  }

  if($("#txtHours").val() == '00' && $("#txtMinutes").val() == '00'){
    $("#err_duration").html('Required duration.');
    error = 1;
  }

  if(error == 1){
    return false;
  }
}

  $(document).ready(function ()
  {
    //$("#frmreviewmeeting").validate();  
  });

  $("#travel_mode").change(function() {
    var option = $('option:selected', this).attr('autc');
    $("#travel_cost").val('');
    if(option == 1){
      $(".tcost").hide();  
    }
    else{
      $(".tcost").show();
    }
  });

  $("#internal_meeting").change(function() {
    if ($(this).prop('checked')==true){ 
        $("#external_meeting").hide();
        $(".ext_meet_div").removeClass("required");
    }
    else{
      $("#external_meeting").show();
      $(".ext_meet_div").addClass("required");
    }
  });
  
  $("#state_id").change(function() {
    $("#block_id").html("<option value=''>-Select Block-</option>");
    if($(this).val() != ''){
      var sid = $(this).val();
      $.ajax({
        url: BASE_URL+'activity/get_districts',
        type: 'get',
        data: {
          sid: sid
        },
        dataType: 'json',
        success: function(response) {
          $("#other_district_id").html("<option value=''>-Select District-</option>");
          $.each(response, function() {
              $("#other_district_id").append("<option value='" + this['id'] + "'>" + this['name'] + " </option>");
          });
        },
        error: function(response) {
          window.console.log(response);
        }
      });
    }else{
      $("#other_district_id").html("<option value=''>-Select District-</option>");
    }
  });
  
  $("#other_district_id").change(function() {
    activity_block_list($(this).val(),'other');  
  });
  
  $("#district_id").change(function() {
    if($(this).val() == 'other'){
      $("#block_id").html("<option value=''>-Select Block-</option>");
      $.ajax({
        url: BASE_URL+'activity/get_state',
        type: 'get',
        data: {
          did: 'other'
        },
        dataType: 'json',
        success: function(response) {
          $("#state_id").html("<option value=''>-Select State-</option>");
          $.each(response, function() {
              $("#state_id").append("<option value='" + this['id'] + "'>" + this['name'] + " </option>");
          });
        },
        error: function(response) {
          window.console.log(response);
        }
      });
      $(".other_district_div").show();
    }
    else{
      $(".other_district_div").hide();
      $("#state_id").html("<option value=''>-Select State-</option>");
      $("#other_district_id").html("<option value=''>-Select District-</option>");
      activity_block_list($(this).val(),'');
    }
    
  });

  $("#block_id").change(function() {
    if($("#district_id").val() == 'other'){
      $("#cluster_id").html("<option value=''>-Select Cluster-</option><option value='000'>Other</option>");
      $("#err_block").html('');
    }
    else{
      activity_cluster_list($(this).val(),'');
    }
  });

  $("#cluster_id").change(function() {
    if($("#district_id").val() == 'other'){
      $("#school_id").html("<option value=''>-Select School-</option><option value='000'>Other</option>");
      $("#err_cluster").html('');
    }
    else{
      activity_school_list($(this).val(),'');
      $("#err_cluster").html('');
    }
  });
</script>
