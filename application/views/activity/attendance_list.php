<? if(!isset($download) && !isset($pdf)) {?>
<form name="frmattendance" id="frmattendance" method="post" action="" class="">
  <div class="row">
    <div class="col-md-3">
      <div class="form-group">
        <input type="text" id="activitydatestart" name="activitydatestart" class="form-control required" value="<? echo (!empty($activitydatestart) ? $activitydatestart : '');?>" autocomplete="off">
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <input type="text" id="activitydateend" name="activitydateend" class="form-control required" value="<? echo (!empty($activitydateend) ? $activitydateend : '');?>" autocomplete="off">
      </div>
    </div>
    <? if($current_role == "super_admin" or $current_role == "admin" or $current_role == "accounts" or $current_role == "HR") { ?>
    <div class="col-md-3">
      <div class="form-group">
        <select name="state_id" id="state_id" class="form-control required">
          <option value=''>-Select State-</option>
          <? foreach($state_list as $states) { ?>
            <option value="<?=$states->id;?>" <?php echo (isset($state_id) && $state_id == $states->id ? 'selected' : ''); ?> ><?=$states->name;?></option>
          <?  } ?>  
        </select>
      </div>
    </div>
    <?  } ?>
   <? $time_filters = array("all"=>"All", "filter"=>"Less than 7 hour"); ?>
    <div class="form-group col-md-3">
      <select name="time_filter" id="time_filter" class="form-control">
        <? foreach($time_filters as $key=>$value) { ?>
          <option value="<?=$key?>" <? if($time_filter == $key){ echo 'selected'; }?> ><?=$value?></option>
        <?  } ?>  
      </select>
    </div>  
  </div>  
  <div class="row">
    <div class="col-md-4"></div> 
    <div class="col-md-3">
      <div class="form-group">
        <input type="submit" name="submit" class="form-control btn btn-sm btn-success" value="Get Attendance">
      </div>
    </div>
  </div>
</form>
<? } ?>

<? if(!isset($download) && !isset($pdf) && !empty($attendance_array))  { ?>
    <div class="form-group row">
      <div class="form-group col-md-3">
      <form name="frmreport" action="<?php echo base_url();?>activity/attendance_list" method="post">
          <input type="hidden" name="activitydatestart" value="<? echo (!empty($activitydatestart) ? $activitydatestart : '');?>">
          <input type="hidden" name="activitydateend" value="<? echo (!empty($activitydateend) ? $activitydateend : '');?>">
          <input type="hidden" name="state_id" value="<?=$state_id?>">
          <input type="hidden" name="time_filter" value="<?=$time_filter?>">
          <input type="hidden" name="download" value="1">
          <button type="submit" name="submit" value="Get Attendance List" class="btn btn-success">Download Report</button>
      </form>
      </div>
      <div class="form-group col-md-3">
      <form name="frmreport" action="<?php echo base_url();?>activity/attendance_list" method="post">
          <input type="hidden" name="activitydatestart" value="<? echo (!empty($activitydatestart) ? $activitydatestart : '');?>">
          <input type="hidden" name="activitydateend" value="<? echo (!empty($activitydateend) ? $activitydateend : '');?>">
          <input type="hidden" name="state_id" value="<?=$state_id?>">
          <input type="hidden" name="time_filter" value="<?=$time_filter?>">
          <input type="hidden" name="pdf" value="1">
        <button type="submit" name="submit" value="Get Attendance List" class="btn btn-success">Download PDF</button>
      </form>
      </div>
      <div class="col-md-6" align="right"><a href="<?=base_url()?>activity/attendance_list" style="float:right">Back</a></div>
    </div>
<? } ?>

<? if(!empty($attendance_dates_array)) {?>
<div class="scrollme">
<table class="table table-striped table-bordered bootstrap-datatable datatable1 responsive customdatatable" width="100%">
	<thead>
		<tr style="background-color: #d5ecf9;">
			<th>S.No.</th>
			<th>Empolyee Code</th>
      <?  if(isset($download) || isset($pdf)) { ?>  
        <th>Empolyee Name/Code</th>
      <?  } ?>  
			<th>Date</th>
			<th>In Time</th>
			<th>In Address</th>
			<th>Out Time</th>
			<th>Out Address</th>
    <?  if(isset($download) || isset($pdf)) { ?>  
			<th>Attendance Type</th>
      <th>Time Differece</th>
      <th>Source</th>
      <th>Remark</th>
    <?  } ?>  
		</tr>
	</thead>
	<tbody>
    <?php $i=1; 
         /* foreach($attendance_array as $date=>$user_arr) {
            foreach($user_arr as $user_id=>$data_arr) { 
              if(!empty($data_arr['attendance-in'])) { 
                
                $out_count = (isset($data_arr['attendance-out']) ? count($data_arr['attendance-out']) : 0);
                $login_name = $data_arr['login_name'][0];
                  
                if($time_filter != 'all'){
                  $in_time = strtotime($data_arr['attendance-in'][0]);
                  $out_time = (isset($data_arr['attendance-out'][$out_count-1]) ? strtotime($data_arr['attendance-out'][$out_count-1]) : '');
                  
                  $time_diff = 0;
                  if($out_time != '')
                  {
                     $time_diff = ($out_time-$in_time)/(60*60);
                  }
                  if($time_diff < 7){
                ?>
                  <tr>
                    <td align="center"><?=$i?></td>
                    <? if(isset($download) || isset($pdf)) { ?>  
                      <td><?=$user_id; ?></td>
                    <?  }else{ ?>
                      <td><?=$login_name." (".$user_id.")"?></td>
                    <?  } ?>  
                    <td><?=$date; ?></td>
                    <td><? echo date('H:i', strtotime($data_arr['attendance-in'][0])); ?></td>
                    <td><? echo (isset($data_arr['attendance-out'][$out_count-1]) ? date('H:i', strtotime($data_arr['attendance-out'][$out_count-1])) : 'NA');?></td>
                    <? if(isset($download) || isset($pdf)) { ?>  
                    <td>PST</td>
                    <? } ?>  
                  </tr>
                <? $i++; 
                }
               }
               else{
                 ?>
                  <tr>
                    <td align="center"><?=$i?></td>
                    <? if(isset($download) || isset($pdf)) { ?>  
                      <td><?=$user_id; ?></td>
                    <?  }else{ ?>
                      <td><?=$login_name." (".$user_id.")"?></td>
                    <?  } ?>  
                    <td><?=$date; ?></td>
                    <td><? echo date('H:i', strtotime($data_arr['attendance-in'][0])); ?></td>
                    <td><? echo (isset($data_arr['attendance-out'][$out_count-1]) ? date('H:i', strtotime($data_arr['attendance-out'][$out_count-1])) : 'NA');?></td>
                    <? if(isset($download) || isset($pdf)) { ?>  
                    <td>PST</td>
                    <? } ?>  
                  </tr>
                <? $i++;  
                 
               } 
            } 
          }   
        } */
        foreach($attendance_dates_array as $date){
					  if(isset($attendance_reg[$date])){
								foreach($attendance_reg[$date] as $user_id=>$data_arr) { 
									
									$login_name = $data_arr['login_name'];
									$in_time = strtotime($data_arr['attendance-in']);
									$out_time = (isset($data_arr['attendance-out']) ? strtotime($data_arr['attendance-out']) : '');
									
									$time_diff = 0;
									if($out_time != '')
									{
										 $time_diff = ($out_time-$in_time)/(60*60);
									} 
									if($time_filter != 'all'){
										if($time_diff < 7){
											?>
											<tr>
												<td align="center"><?=$i?></td>
												<? if(isset($download) || isset($pdf)) { ?>  
													<td><?=$user_id; ?></td>
												<?  }else{ ?>
													<td><?=$login_name." (".$user_id.")"?></td>
												<?  } ?>  
												<? if(isset($download) || isset($pdf)) { ?>  
													<td><?=$login_name." (".$user_id.")"?></td>
												<?  } ?>
												<td><?=$date; ?></td>
												<td><? echo date('H:i', strtotime($attendance_reg[$date][$user_id]['attendance-in'])); ?></td>
												<td><? echo 'N/A'; ?></td>
												<td><? echo (isset($attendance_reg[$date][$user_id]['attendance-out']) ? date('H:i', strtotime($attendance_reg[$date][$user_id]['attendance-out'])) : 'NA');?></td>
												<td><? echo 'N/A';?></td>
												<? if(isset($download) || isset($pdf)) { ?>  
												<td>PST</td>
												<td><?=round($time_diff,1);?></td>
												<td><?=(isset($data_arr['created_by'][0]) ? $data_arr['created_by'][0] : 'NA')?></td>
												<td>&nbsp;</td>
												<? } ?>  
											</tr>
											<?
											$i++;
										}
									}
									else{
										?>
										<tr>
												<td align="center"><?=$i?></td>
												<? if(isset($download) || isset($pdf)) { ?>  
													<td><?=$user_id; ?></td>
												<?  }else{ ?>
													<td><?=$login_name." (".$user_id.")"?></td>
												<?  } ?>  
												<? if(isset($download) || isset($pdf)) { ?>  
													<td><?=$login_name." (".$user_id.")"?></td>
												<?  } ?>
												<td><?=$date; ?></td>
												<td><? echo date('H:i', strtotime($attendance_reg[$date][$user_id]['attendance-in'])); ?></td>
												<td><? echo 'N/A'; ?></td>
												<td><? echo (isset($attendance_reg[$date][$user_id]['attendance-out']) ? date('H:i', strtotime($attendance_reg[$date][$user_id]['attendance-out'])) : 'NA');?></td>
												<td><? echo 'N/A';?></td>
												<? if(isset($download) || isset($pdf)) { ?>  
												<td>PST</td>
												<td><?=round($time_diff,1);?></td>
												<td><?=(isset($data_arr['created_by'][0]) ? $data_arr['created_by'][0] : 'NA')?></td>
												<td>&nbsp;</td>
												<? } ?>  
											</tr>
										<?
									}
								}
						}
						else {
						foreach($attendance_array[$date] as $user_id=>$data_arr) { 
              if(!empty($data_arr['attendance-in'])) { 
                
                $out_count = (isset($data_arr['attendance-out']) ? count($data_arr['attendance-out']) : 0);
                $login_name = $data_arr['login_name'][0];
                
                if(isset($download) || isset($pdf)) {
                  $location_in = trim($data_arr['attendance-in-location'][0]);
                  $location_out = (isset($data_arr['attendance-out-location'][$out_count-1]) ? trim($data_arr['attendance-out-location'][$out_count-1]) : 'NA');
                }
                else{
                  $in_add = explode(',',trim($data_arr['attendance-in-location'][0]));
                  $location_in = implode(', <br>',$in_add);
                  
                  $out_address = (isset($data_arr['attendance-out-location'][$out_count-1]) ? trim($data_arr['attendance-out-location'][$out_count-1]) : 'NA');
                  $out_add = explode(',',trim($out_address));
                  $location_out = implode(', <br>',$out_add);
                }
                $in_time = strtotime($data_arr['attendance-in'][0]);
                $out_time = (isset($data_arr['attendance-out'][$out_count-1]) ? strtotime($data_arr['attendance-out'][$out_count-1]) : '');
                
                $time_diff = 0;
                if($out_time != '')
                {
                   $time_diff = ($out_time-$in_time)/(60*60);
                }  
                if($time_filter != 'all'){
                  
                  if($time_diff < 7){
                ?>
                  <tr>
                    <td align="center"><?=$i?></td>
                    <? if(isset($download) || isset($pdf)) { ?>  
                      <td><?=$user_id; ?></td>
                    <?  }else{ ?>
                      <td><?=$login_name." (".$user_id.")"?></td>
                    <?  } ?>  
                    <? if(isset($download) || isset($pdf)) { ?>  
                      <td><?=$login_name." (".$user_id.")"?></td>
                    <?  } ?>
                    <td><?=$date; ?></td>
                    <td><? echo date('H:i', strtotime($data_arr['attendance-in'][0])); ?></td>
                    <td><? echo $location_in; ?></td>
                    <td><? echo (isset($data_arr['attendance-out'][$out_count-1]) ? date('H:i', strtotime($data_arr['attendance-out'][$out_count-1])) : 'NA');?></td>
                    <td><? echo $location_out?></td>
                    <? if(isset($download) || isset($pdf)) { ?>  
                    <td>PST</td>
                    <td><?=round($time_diff,1);?></td>
                    <td><?=(isset($data_arr['created_by'][0]) ? $data_arr['created_by'][0] : 'NA')?></td>
                    <td>&nbsp;</td>
                    <? } ?>  
                  </tr>
                <? $i++; 
									if(key_exists($date, $attendance_reg)){
										?>
										<tr>
											<td align="center">&nbsp;</td>
											<? if(isset($download) || isset($pdf)) { ?>  
												<td><?=$user_id; ?></td>
											<?  }else{ ?>
												<td><?=$login_name." (".$user_id.")"?></td>
											<?  } ?>  
											<? if(isset($download) || isset($pdf)) { ?>  
												<td><?=$login_name." (".$user_id.")"?></td>
											<?  } ?>
											<td><?=$date; ?></td>
											<td><? echo date('H:i', strtotime($attendance_reg[$date]['attendance-in'])); ?></td>
											<td><? echo $location_in; ?></td>
											<td><? echo (isset($attendance_reg[$date]['attendance-out']) ? date('H:i', strtotime($attendance_reg[$date]['attendance-out'])) : 'NA');?></td>
											<td><? echo $location_out?></td>
											<? if(isset($download) || isset($pdf)) { ?>  
											<td>PST</td>
											<td><?=round($time_diff,1);?></td>
											<td><?=(isset($data_arr['created_by'][0]) ? $data_arr['created_by'][0] : 'NA')?></td>
											<td>&nbsp;</td>
											<? } ?>  
										</tr>
										<?
									}
                }
               }
               else{
                 ?>
                  <tr>
                    <td align="center"><?=$i?></td>
                    <? if(isset($download) || isset($pdf)) { ?>  
                      <td><?=$user_id; ?></td>
                    <?  }else{ ?>
                      <td><?=$login_name." (".$user_id.")"?></td>
                    <?  } ?>  
                    <? if(isset($download) || isset($pdf)) { ?>  
                      <td><?=$login_name." (".$user_id.")"?></td>
                    <?  } ?>
                    <td><?=$date; ?></td>
                    <td><? echo date('H:i', strtotime($data_arr['attendance-in'][0])); ?></td>
                    <td><? echo $location_in; ?></td>
                    <td><? echo (isset($data_arr['attendance-out'][$out_count-1]) ? date('H:i', strtotime($data_arr['attendance-out'][$out_count-1])) : 'NA');?></td>
                    <td><? echo $location_out?></td>
                    <? if(isset($download) || isset($pdf)) { ?>  
                    <td>PST</td>
                    <td><?=round($time_diff,1);?></td>
                    <td><?=(isset($data_arr['created_by'][0]) ? $data_arr['created_by'][0] : 'NA')?></td>
                    <td>&nbsp;</td>
                    <? } ?>  
                  </tr>
                <? $i++;  
                 if(key_exists($date, $attendance_reg)){
										?>
										<tr>
											<td align="center">&nbsp;</td>
											<? if(isset($download) || isset($pdf)) { ?>  
												<td><?=$user_id; ?></td>
											<?  }else{ ?>
												<td><?=$login_name." (".$user_id.")"?></td>
											<?  } ?>  
											<? if(isset($download) || isset($pdf)) { ?>  
												<td><?=$login_name." (".$user_id.")"?></td>
											<?  } ?>
											<td><?=$date; ?></td>
											<td><? echo date('H:i', strtotime($attendance_reg[$date][$user_id]['attendance-in'])); ?></td>
											<td><? echo 'N/A'; ?></td>
											<td><? echo (isset($attendance_reg[$date][$user_id]['attendance-out']) ? date('H:i', strtotime($attendance_reg[$date][$user_id]['attendance-out'])) : 'NA');?></td>
											<td><? echo 'N/A';?></td>
											<? if(isset($download) || isset($pdf)) { ?>  
											<td>PST</td>
											<td><?=round($time_diff,1);?></td>
											<td><?=(isset($data_arr['created_by'][0]) ? $data_arr['created_by'][0] : 'NA')?></td>
											<td>&nbsp;</td>
											<? } ?>  
										</tr>
										<?
									}
               } 
            } 
          }   
						}
				}
        
       /* foreach($attendance_array as $date=>$user_arr) {
            foreach($user_arr as $user_id=>$data_arr) { 
              if(!empty($data_arr['attendance-in'])) { 
                
                $out_count = (isset($data_arr['attendance-out']) ? count($data_arr['attendance-out']) : 0);
                $login_name = $data_arr['login_name'][0];
                
                if(isset($download) || isset($pdf)) {
                  $location_in = trim($data_arr['attendance-in-location'][0]);
                  $location_out = (isset($data_arr['attendance-out-location'][$out_count-1]) ? trim($data_arr['attendance-out-location'][$out_count-1]) : 'NA');
                }
                else{
                  $in_add = explode(',',trim($data_arr['attendance-in-location'][0]));
                  $location_in = implode(', <br>',$in_add);
                  
                  $out_address = (isset($data_arr['attendance-out-location'][$out_count-1]) ? trim($data_arr['attendance-out-location'][$out_count-1]) : 'NA');
                  $out_add = explode(',',trim($out_address));
                  $location_out = implode(', <br>',$out_add);
                }
                $in_time = strtotime($data_arr['attendance-in'][0]);
                $out_time = (isset($data_arr['attendance-out'][$out_count-1]) ? strtotime($data_arr['attendance-out'][$out_count-1]) : '');
                
                $time_diff = 0;
                if($out_time != '')
                {
                   $time_diff = ($out_time-$in_time)/(60*60);
                }  
                if($time_filter != 'all'){
                  
                  if($time_diff < 7){
                ?>
                  <tr>
                    <td align="center"><?=$i?></td>
                    <? if(isset($download) || isset($pdf)) { ?>  
                      <td><?=$user_id; ?></td>
                    <?  }else{ ?>
                      <td><?=$login_name." (".$user_id.")"?></td>
                    <?  } ?>  
                    <? if(isset($download) || isset($pdf)) { ?>  
                      <td><?=$login_name." (".$user_id.")"?></td>
                    <?  } ?>
                    <td><?=$date; ?></td>
                    <td><? echo date('H:i', strtotime($data_arr['attendance-in'][0])); ?></td>
                    <td><? echo $location_in; ?></td>
                    <td><? echo (isset($data_arr['attendance-out'][$out_count-1]) ? date('H:i', strtotime($data_arr['attendance-out'][$out_count-1])) : 'NA');?></td>
                    <td><? echo $location_out?></td>
                    <? if(isset($download) || isset($pdf)) { ?>  
                    <td>PST</td>
                    <td><?=round($time_diff,1);?></td>
                    <td><?=(isset($data_arr['created_by'][0]) ? $data_arr['created_by'][0] : 'NA')?></td>
                    <td>&nbsp;</td>
                    <? } ?>  
                  </tr>
                <? $i++; 
                }
               }
               else{
                 ?>
                  <tr>
                    <td align="center"><?=$i?></td>
                    <? if(isset($download) || isset($pdf)) { ?>  
                      <td><?=$user_id; ?></td>
                    <?  }else{ ?>
                      <td><?=$login_name." (".$user_id.")"?></td>
                    <?  } ?>  
                    <? if(isset($download) || isset($pdf)) { ?>  
                      <td><?=$login_name." (".$user_id.")"?></td>
                    <?  } ?>
                    <td><?=$date; ?></td>
                    <td><? echo date('H:i', strtotime($data_arr['attendance-in'][0])); ?></td>
                    <td><? echo $location_in; ?></td>
                    <td><? echo (isset($data_arr['attendance-out'][$out_count-1]) ? date('H:i', strtotime($data_arr['attendance-out'][$out_count-1])) : 'NA');?></td>
                    <td><? echo $location_out?></td>
                    <? if(isset($download) || isset($pdf)) { ?>  
                    <td>PST</td>
                    <td><?=round($time_diff,1);?></td>
                    <td><?=(isset($data_arr['created_by'][0]) ? $data_arr['created_by'][0] : 'NA')?></td>
                    <td>&nbsp;</td>
                    <? } ?>  
                  </tr>
                <? $i++;  
                 if(key_exists($date, $attendance_reg)){
										?>
										<tr>
											<td align="center">&nbsp;</td>
											<? if(isset($download) || isset($pdf)) { ?>  
												<td><?=$user_id; ?></td>
											<?  }else{ ?>
												<td><?=$login_name." (".$user_id.")"?></td>
											<?  } ?>  
											<? if(isset($download) || isset($pdf)) { ?>  
												<td><?=$login_name." (".$user_id.")"?></td>
											<?  } ?>
											<td><?=$date; ?></td>
											<td><? echo date('H:i', strtotime($attendance_reg[$date][$user_id]['attendance-in'])); ?></td>
											<td><? echo 'N/A'; ?></td>
											<td><? echo (isset($attendance_reg[$date][$user_id]['attendance-out']) ? date('H:i', strtotime($attendance_reg[$date][$user_id]['attendance-out'])) : 'NA');?></td>
											<td><? echo 'N/A';?></td>
											<? if(isset($download) || isset($pdf)) { ?>  
											<td>PST</td>
											<td><?=round($time_diff,1);?></td>
											<td><?=(isset($data_arr['created_by'][0]) ? $data_arr['created_by'][0] : 'NA')?></td>
											<td>&nbsp;</td>
											<? } ?>  
										</tr>
										<?
									}
               } 
            } 
          }   
        } 
        
        foreach($attendance_reg as $date=>$user_arr){
					if(!key_exists($date, $attendance_array)){
						foreach($user_arr as $user_id=>$data_arr) { 
							
							$login_name = $data_arr['login_name'];
							?>
							<tr>
								<td align="center">&nbsp;</td>
								<? if(isset($download) || isset($pdf)) { ?>  
									<td><?=$user_id; ?></td>
								<?  }else{ ?>
									<td><?=$login_name." (".$user_id.")"?></td>
								<?  } ?>  
								<? if(isset($download) || isset($pdf)) { ?>  
									<td><?=$login_name." (".$user_id.")"?></td>
								<?  } ?>
								<td><?=$date; ?></td>
								<td><? echo date('H:i', strtotime($attendance_reg[$date][$user_id]['attendance-in'])); ?></td>
								<td><? echo 'N/A'; ?></td>
								<td><? echo (isset($attendance_reg[$date][$user_id]['attendance-out']) ? date('H:i', strtotime($attendance_reg[$date][$user_id]['attendance-out'])) : 'NA');?></td>
								<td><? echo 'N/A';?></td>
								<? if(isset($download) || isset($pdf)) { ?>  
								<td>PST</td>
								<td><?=round($time_diff,1);?></td>
								<td><?=(isset($data_arr['created_by'][0]) ? $data_arr['created_by'][0] : 'NA')?></td>
								<td>&nbsp;</td>
								<? } ?>  
							</tr>
							<?
						}
					}
				}*/
        
      ?>
  </tbody>
</table>  
</div>

<? } ?>

<? if(!isset($download) && !isset($pdf)) {?>

<script>
$(document).ready(function() {
    $("#frmattendance").validate();
});
</script>
<? } ?>
