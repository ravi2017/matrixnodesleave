            <? 
            if(!empty($assessment)){
            foreach($assessment as $class=>$class_data) { ?>
              <? if($class != 'no_class') { ?>
                <div class="row" style="background-color:#CACACA;padding:5px 5px 5px 0px;">   
                 <div class="col-md-10"><strong><?php echo ucfirst($class);?></strong></div>
                </div> 
               <? }else{ ?>  
                <div class="row" style="background-color:#CACACA;padding:5px 5px 5px 0px;">   
                 <div class="col-md-10"><strong>General Question(s)</strong></div>
                </div>   
               <? } ?>
               <? foreach($class_data as $subjects=>$subject_data) { ?>
               
               <? if($subjects != 'no_subject') { ?>
                 <div class="row">   
                 <div class="col-md-2"><strong>Subject: </strong></div><div class="col-md-4"> <?php echo ucfirst($subjects);?></div> 
                 </div>  
               <? } ?>  
                  <? for($k=0; $k<count($subject_data['question']); $k++) { ?>
                    <div class="row">   
                       <div class="col-md-2"><strong>Ques: </strong></div><div class="col-md-10"><?=$subject_data['question'][$k];?></div> 
                     </div>   
                     <div class="row">   
                       <div class="col-md-2"><strong>Ans: </strong></div><div class="col-md-10"><?=$subject_data['answer'][$k];?></div> 
                    </div>
                  <hr>
                <? } ?>
              <? } ?>
            <? } 
            }else{ ?>
              <div class="row text-center">No Record Found.</div>
            <? } ?>  
          
