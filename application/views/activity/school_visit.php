<div class="row">
  <div class="col-md-12">
    <? if($activity_count > MONTHLY_WEB_ACTIVITY_LIMIT) { ?>
      <div class="row">
        <div class="col-md-12 text-center text-danger">
           <h6>Your this month limit exceed. Please contact to admin.</h6>
        </div> 
      </div> 
    <? }else{ ?>  
    <form method="post" action="<?php echo base_url();?>activity/school_visit" role="form" name="frmschoolvisit" id="frmschoolvisit" onSubmit="return validatefrm()">
      <div class="row">
        <div class="form-group col-md-3">
          <label for="Date Of Visit">Date Of Visit</label>
          <?php 
            if($mode == 'edit'){ 
              $activity_date = date('Y-m-d', strtotime($activity_date));
              $activity_id = '';
            } else {  
                $activity_date = '';
                $activity_id = "id='datetimepicker1'";
            } 
          ?>
          <div class='input-group date' <? echo $activity_id?> >
            <input type="text" name="visit_date" value="<?php echo $activity_date;?>" placeholder="Select Date" class="form-control required" id="visit_date"  autocomplete="off" />
            <span class="input-group-addon">
              <span class="ion-calendar"></span>
            </span>
            
          </div>
          <span class="err" id="err_date"></span>
        </div>
        <div class="form-group col-md-3">
          <label for="Date Of Visit">Time Of Visit</label>
          <div class='input-group date' id='datetimepicker5'>
              <input type="text" name="visit_time" value="" placeholder="Select Time" class="form-control required" id="visit_time"  autocomplete="off" />
              <span class="input-group-addon">
                <span class="ion-clock"></span>
              </span>
          </div>
        </div>
        <div class="form-group col-md-3">
          <label for="Duration">Duration</label>
          <div class="duration"></div><span class="err" id="err_duration"></span>
        </div>
      </div>
      
      <div class="row">
        <div class="form-group col-md-3">
          <label for="district">District</label>
          <select id="district_id" name="district_id" class="form-control required">
            <option value="">-Select District-</option>
            <?php foreach($user_districts as $user_district){ ?>
              <option value="<?php echo $user_district->id;?>"><?php echo $user_district->name;?></option>
            <? } ?>
            <option value="other">Other</option>  
          </select>
          <span class="err" id="err_district"></span>
        </div>
        
        <div class="form-group col-md-3 other_district_div" style="display:none;">
          <label for="state">State</label>
          <select id="state_id" name="state_id" class="ext_meet_div other_state form-control required">
            <option value="">-Select State-</option>
          </select>
          <span class="err" id="err_state"></span>
        </div>
        <div class="form-group col-md-3 other_district_div" style="display:none;">
          <label for="district">District</label>
          <select id="other_district_id" name="other_district_id" class="ext_meet_div form-control required">
            <option value="">-Select District-</option>
          </select>
          <span class="err" id="err_other_district"></span>
        </div>
        
        <div class="form-group col-md-3">
          <label for="block">Block</label>
          <select id="block_id" name="block_id" class="ext_meet_div form-control required">
            <option value="">-Select Block-</option>  
          </select>
          <span class="err" id="err_block"></span>
        </div>
        
        <div class="form-group col-md-3">
          <label for="cluster">Cluster</label>
          <select id="cluster_id" name="cluster_id" class="form-control required" >
            <option value="">-Select Cluster-</option>  
          </select>
          <span class="err" id="err_cluster"></span>
        </div>
        
        <div class="form-group col-md-3">
          <label for="school">School</label>
          <select id="school_id" name="school_id" class="form-control required" >
            <option value="">-Select School-</option>  
          </select>
          <span class="err" id="err_school"></span>
        </div>
      </div>
      
      <div class="row">
        <div class="form-group col-md-3">
          <label for="date">Subjects (Active In State)</label>
          <select id="sv_subject" name="sv_subject[]" multiple>
            <? foreach($subjects as $subject) { ?>
              <option value="<?=$subject->id?>"><?=$subject->name?></option>
            <? } ?>  
          </select>
        </div>
        <div class="form-group col-md-3">
          <label for="date">Classes Observed</label>
          <select id="sv_class" name="sv_class[]" multiple>
            <? foreach($classes as $class) { ?>
              <option value="<?=$class->id?>"><?=$class->name?></option>
            <? } ?>  
          </select>
        </div>
        <div class="form-group col-md-3">
          <label for="date">Travel Mode</label>
          <select id="travel_mode" name="travel_mode">
            <? foreach($travel_modes as $travel_mode) { ?>
              <option value="<?=$travel_mode->travel_mode?>" autc="<?=$travel_mode->is_autocalculated?>"><?=$travel_mode->travel_mode?></option>
            <? } ?>  
          </select>
        </div>
        <div class="form-group col-md-3 tcost">
          <label for="date">Travel Cost</label>
          <input type="text" id="travel_cost" name="travel_cost" value="" autocomplete="off" class="form-control">
        </div>
      </div>  
      
      <?php echo $this->load->view('activity/observations','',true) ?>
          
      <div class="form-group row">
        <input type="hidden" name="mode" id="mode" value="<?=$mode?>" />
        <input type="hidden" name="edit_id" value="<?=$edit_id?>" />
        <div class="col-md-6"><br>
          <a class="btn btn-sm btn-danger f-left" href="<?=base_url()?>activity">Cancel</a>
        </div>
        <div class="col-md-6"><br>
          <button type="submit" class="btn btn-sm btn-success f-right" name="Submit">Submit</button>
        </div>
      </div>
    </form>
    <?  } ?>  
  </div>
</div>

<script type="text/javascript" src="<? echo base_url(); ?>assets/v2/js/activity.js"></script>
<script type="text/javascript">
  
 $(document).ready(function ()
{
    var options1 = {
      hour: {
          value: 0,
          min: 0,
          max: 24,
          step: 1,
          symbol: "hrs"
      },
      minute: {
          value: 0,
          min: 0,
          max: 60,
          step: 5,
          symbol: "mins"
      },
      direction: "increment", // increment or decrement
      inputHourTextbox: null, // hour textbox
      inputMinuteTextbox: null, // minutes textbox
      postfixText: "", // text to display after the input fields
      numberPaddingChar: '0' // number left padding character ex: 00052
    };

    $(".duration").timesetter(options1);

    //var date = new Date('2019-06-01');
    var cur_date = new Date();
    cur_date.setDate(cur_date.getDate() - 2);
    var pre_date = new Date();
    pre_date.setDate(pre_date.getDate() - 8);
              
    $('#datetimepicker1 #visit_date').datetimepicker({
      timepicker: false,
      format: "m/d/Y",
      minDate:pre_date,
      maxDate:cur_date
    });

    $('#datetimepicker5 #visit_time').datetimepicker({
      datepicker: false,
      format: "H:i"
    });
    
    $('#datetimepicker1 #visit_date').change(function(){
       var sel_date = this.value;
       if((new Date(sel_date) < new Date(pre_date)) || (new Date(sel_date) > new Date(cur_date))){
          $("#visit_date").val('');
       }
    });
});

function validatefrm()
{
  $("#err_date").html('');
  $("#err_district").html('');
  $("#err_block").html('');
  $("#err_cluster").html('');
  $("#err_school").html('');
  $("#err_duration").html('');

  var error = 0;
  if($("#visit_date").val() == ''){
    $("#err_date").html('Please select visit date.');
    error = 1;
  }
  if($("#district_id").val() == ''){
    $("#err_district").html('Please select district.');
    error = 1;
  }
  if($("#district_id").val() == 'other'){
      if($("#state_id").val() == ''){
        $("#err_state").html('Please select state.');
        error = 1;
      }
      if($("#other_district_id").val() == ''){
        $("#err_other_district").html('Please select district.');
        error = 1;
      }
  }
  if($("#block_id").val() == ''){
    $("#err_block").html('Please select block.');
    error = 1;
  }
  if($("#cluster_id").val() == ''){
    $("#err_cluster").html('Please select cluster.');
    error = 1;
  }
  if($("#school_id").val() == ''){
    $("#err_school").html('Please select school.');
    error = 1;
  }
  if($("#txtHours").val() == '00' && $("#txtMinutes").val() == '00'){
    $("#err_duration").html('Please select duration.');
    error = 1;
  }

  if(error == 1){
    return false;
  }
}

  $(document).ready(function ()
  {
    //$("#frmschoolvisit").validate();  
  });

  $("#travel_mode").change(function() {
    var option = $('option:selected', this).attr('autc');
    $("#travel_cost").val('');
    if(option == 1){
      $(".tcost").hide();  
    }
    else{
      $(".tcost").show();
    }
  });
  
  $("#state_id").change(function() {
    $("#block_id").html("<option value=''>-Select Block-</option>");
    if($(this).val() != ''){
      var sid = $(this).val();
      $.ajax({
        url: BASE_URL+'activity/get_districts',
        type: 'get',
        data: {
          sid: sid
        },
        dataType: 'json',
        success: function(response) {
          $("#other_district_id").html("<option value=''>-Select District-</option>");
          $.each(response, function() {
              $("#other_district_id").append("<option value='" + this['id'] + "'>" + this['name'] + " </option>");
          });
        },
        error: function(response) {
          window.console.log(response);
        }
      });
    }else{
      $("#other_district_id").html("<option value=''>-Select District-</option>");
    }
  });
  
  $("#other_district_id").change(function() {
    activity_block_list($(this).val(),'other');  
  });
  
  $("#district_id").change(function() {
    if($(this).val() == 'other'){
      $("#block_id").html("<option value=''>-Select Block-</option>");
      $.ajax({
        url: BASE_URL+'activity/get_state',
        type: 'get',
        data: {
          did: 'other'
        },
        dataType: 'json',
        success: function(response) {
          $("#state_id").html("<option value=''>-Select State-</option>");
          $.each(response, function() {
              $("#state_id").append("<option value='" + this['id'] + "'>" + this['name'] + " </option>");
          });
        },
        error: function(response) {
          window.console.log(response);
        }
      });
      $(".other_district_div").show();
    }
    else{
      $(".other_district_div").hide();
      $("#state_id").html("<option value=''>-Select State-</option>");
      $("#other_district_id").html("<option value=''>-Select District-</option>");
      activity_block_list($(this).val(),'');
    }
    
  });

  $("#block_id").change(function() {
    if($("#district_id").val() == 'other'){
      $("#cluster_id").html("<option value=''>-Select Cluster-</option><option value='000'>Other</option>");
      $("#err_block").html('');
    }
    else{
      activity_cluster_list($(this).val(),'');
    }
  });

  $("#cluster_id").change(function() {
    if($("#district_id").val() == 'other'){
      $("#school_id").html("<option value=''>-Select School-</option><option value='000'>Other</option>");
      $("#err_cluster").html('');
    }
    else{
      activity_school_list($(this).val(),'');
      $("#err_cluster").html('');
    }
  });
  
  $("#school_id").change(function() {
      $("#err_school").html('');
  });
  
  
</script>
