<div class="row">
  <div class="col-md-12">
    <form name="myActivity" id="myActivity" action="<?php echo base_url();?>activity/my_activity" method="post" onsubmit="return validatereport()">
			<input type="hidden" name="spark_start" id="spark_start">
			<input type="hidden" name="spark_end" id="spark_end">
      <div class="row">
        <div class="form-group col-md-3">
          <input type="hidden" value="<?=$user_login_id;?>" id="user_login_id" name="user_login_id">
          <span class="err" id="err_date"></span>
          <input type="text" id="activitystartdate" name="activitystartdate" class="form-control required" placeholder="From Date" value="<?php echo $activitystartdate; ?>">
        </div>
        <div class="form-group col-md-3 field_user">
          <span class="err" id="err_date"></span>
          <input type="text" id="activityenddate" name="activityenddate" class="form-control required" placeholder="To Date" value="<?php echo $activityenddate; ?>">
        </div>
        <?php if ($current_role != "field_user") { ?>
        <div class="col-md-6">
        <?php echo $this->load->view('team_members/select_spark','',true) ?>
        </div>
        <?php } ?>
        <div class="form-group col-md-3">
          <label for="Date Of Visit">&nbsp;</label><span class="err" id="err_date"></span>
          <button type="submit" class="btn btn-success btn-sm">Submit</button>
        </div>
      </div>
    </form>
  </div>
</div>



<script>

$("#activitystartdate").change(function(){
	var sdate = $("#activitystartdate").val();
	var edate = $("#activityenddate").val();
	$("#spark_start").val(sdate);
	$("#spark_end").val(edate);
	reset_values();
});

$("#activityenddate").change(function(){
	var sdate = $("#activitystartdate").val();
	var edate = $("#activityenddate").val();
	$("#spark_start").val(sdate);
	$("#spark_end").val(edate);
	reset_values();
});	
	
	
$(document).ready(function ()
{
  $("#myActivity").validate();  
});  
  
$(function(){
    var dateFormat = "dd-mm-yy",
      from = $( "#activitystartdate" )
        .datepicker({
          changeMonth: true,
          numberOfMonths: 1,
          dateFormat: dateFormat
        })
        .on( "change", function() {
          to.datepicker( "option", "minDate", getDate( this ) );
        }),
      to = $( "#activityenddate" ).datepicker({
        changeMonth: true,
        numberOfMonths: 1,
        dateFormat: dateFormat
      })
      .on( "change", function() {
        from.datepicker( "option", "maxDate", getDate( this ) );
      });
 
    function getDate( element ) {
      var date;
      try {
        date = $.datepicker.parseDate( dateFormat, element.value );
      } catch( error ) {
        date = null;
      }
 
      return date;
    }
});


</script>

<style>
.head_activity{
background-color:#f5f5f5;padding:5px 0px 2px 10px;color:#000;
}
</style>
