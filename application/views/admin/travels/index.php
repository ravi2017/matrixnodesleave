<script type="text/javascript">
function show_confirm(act,gotoid)
{
if(act=="edit")
var r=confirm("Do you really want to edit?");
else
var r=confirm("Do you really want to delete?");
if (r==true)
{
window.location="<?php echo base_url();?>admin/travels/"+act+"/"+gotoid;
}
}
</script>
<a class="btn btn-sm btn-success" href="<?php echo base_url();?>admin/travels/add">Add Travels</a><br><br>
<div style="display:none;" class="alert alert-info"></div>
<? if(!empty($message)) { ?><div class="alert alert-danger"><?=$message;?></div> <? } ?>
<table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
	<thead>
		<tr>
			<th>Travel Mode</th>
			<th>Travel Cost (Rs. Per Hour)</th>
			<th>For Role(s)</th>
			<th>Status</th>
			<th class="action no-sort no-search">Actions</th>
		</tr>
	</thead>
	<tbody>
	
	<?php foreach ($travels_list as $e_key){ 

			$roles_val = array();
			if(!empty($e_key->role)){
				$exp_role = explode(',',$e_key->role);

				foreach($exp_role as $value)
				{
					$roles_val[] = $travel_roles[$value];	
				}
			}
		?>
		<tr>
			<td><?php echo $e_key->travel_mode; ?></td>
			<td><?php echo (!empty($e_key->is_autocalculated) ? $e_key->travel_cost : 'Enter By Spark'); ?></td>
      <td><?php echo (!empty($roles_val) ? implode(',', $roles_val) : ''); ?></td>
      <td>
        <?php if ($e_key->status=="1") { ?>
          <span class="label-success label">Active</span>
        <?php } else { ?>
          <span class="label-danger label">Inactive</span>
        <?php } ?>  
      </td>
			<td>
        <a class="" href="<?php echo base_url();?>admin/travels/edit/<?php echo $e_key->id;?>">
					<i class="ti-pencil"></i>
				</a>
				<a href="<?php echo base_url();?>admin/travels/changestatus/<?php echo $e_key->id;?>">
					<?php if ($e_key->status==1) { ?>
            <i class="ti-thumb-up text-success"></i>
					<?php } else { ?>
            <i class="ti-thumb-down text-danger"></i>
          <?php } ?>
				</a>
			</td>
		</tr>
	<?php } ?>
		</tbody>
	</table>
	
	
	
