<?
if(isset($travels))
{
	extract($travels);
	$roles = explode(',', $role);
}

$div_cost = "style='display:none'";
if(!empty($is_autocalculated)){
  $div_cost = "style='display:block'";
}
?>

<form action="<?php echo base_url();?>admin/travels/<?=(isset($action)) ? $action : ''?>" method="post" accept-charset="utf-8" name="frmTravel" id="frmTravel">
  <?
  if (isset($error))
  {
    ?><div class="alert alert-danger"><?php echo $error; ?></div><?php
  }
  ?>
  <div class="form-group">
    <label for="travel_mode">Travel Mode</label><?php echo form_error('travel_mode'); ?>
    <span class="err" id="err_travel_mode"></span>
    <input type="text" class="form-control required" id="travel_mode" placeholder="Enter Mode" name="travel_mode" value="<?=(isset($travel_mode)) ? $travel_mode : ''?>"minlength="2" maxlength="20">
  </div>
	
	<div class="form-group">
    <label for="title"><strong>For Role</strong></label>
    <? foreach($travel_roles as $key=>$value) { ?>
			<input name="role[]" type="checkbox" value="<?=$key?>" <?php if(isset($roles) and in_array($key, $roles)) echo "checked"; ?>><?=$value;?>
    <? } ?>
  </div>


  <div class="form-group">
    <label for="title">Is Autocalculated</label><br /><span class="err" id="err_title"></span>
    <input name="is_autocalculated" id="is_autocalculated" class="form-control1" type="checkbox" value="1" <? echo (!empty($is_autocalculated) ? 'checked' : '');?>>
  </div>
  
  <div class="form-group" id="div_cost" <?=$div_cost?>>
    <label for="travel_cost">Travel Cost (Rs. Per Hour)</label><?php echo form_error('travel_cost'); ?>
    <span class="err" id="err_travel_cost"></span>
    <input type="text" class="form-control" id="travel_cost" placeholder="Enter Cost" name="travel_cost" value="<?=(isset($travel_cost)) ? $travel_cost : ''?>">
  </div>
  
	<div class="form-group">
		<?
		if (isset($id)){
      ?><input type="hidden" name="id" value="<?php echo $id; ?>" /><?
		}
		?>
		<button type="submit" class="btn btn-success">Submit</button>
		&nbsp;<a href="<?php echo base_url();?>admin/travels" class="btn btn-default">Cancel</a>
	</div>
</form>

<script>
$(document).ready(function ()
{
  $("#frmTravel").validate();
});
  
$("#is_autocalculated").click(function(){
  if($(this).is(":checked") == true){
    $("#div_cost").show();
    $("#travel_cost").addClass('required');
  }
  else{
    $("#travel_cost").val(0);
    $("#div_cost").hide();
    $("#travel_cost").removeClass('required');
  }
});
</script>
