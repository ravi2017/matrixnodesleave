<script type="text/javascript">
function show_confirm(act,gotoid)
{
if(act=="edit")
var r=confirm("Do you really want to edit?");
else
var r=confirm("Do you really want to delete?");
if (r==true)
{
window.location="<?php echo base_url();?>admin/test_questions/"+act+"/"+gotoid;
}
}
</script>
<form name="disableobservations" id="disableobservations" action="<?php echo base_url();?>admin/observations/disableall" method="post">
  <input name="observations" id="observationids" type="hidden">
</form>
<a class="btn btn-danger btn-sm observationids" href="javascript:void(0)">Disable Selected Questions</a>
<a class="btn btn-success btn-sm pull-right" href="<?php echo base_url();?>admin/test_questions/add/test_question">Add Test Questions</a>
<a class="btn btn-info btn-sm pull-right" href="<?php echo base_url();?>admin/test_questions/sorting_observations">Manage Order</a>
<a class="btn btn-danger btn-sm pull-right" href="<?php echo base_url();?>admin/test_questions/observations_updated">Observations Updated</a>
<br><br>
<div style="display:none;" class="alert alert-info"></div>
<div class="scrollme">
<table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
	<thead>
		<tr>
			<th><input type="checkbox" id="allobservations"></th>
			<th>State</th>
			<th>Concept</th>
			<th>Question</th>
			<th>Class</th>
			<th>Subject</th>
			<th>Test Type</th>
			<th data-sort=0 class="no-sort action">Status</th>
			<th data-priority="1" data-sort=0 class="no-sort action">Actions</th>
		</tr>
	</thead>
	<tbody>
	
	<?php $i=1;
    $activity_arr = unserialize(TEST_LINE_ARRAY);
  
    foreach ($observations_list as $e_key){ 
      
      $stateData = get_state_info($e_key->state_id);
      $classData = get_class_by_id($e_key->class_id);
      $subject_wise = ($e_key->subject_wise == 1 ? 'Yes' : 'No');
  ?>
		<tr>
			<td><input type="checkbox" value="<?php echo $e_key->id; ?>" class="singleobservation" name="observations[]"></td>
			<td><?php echo $stateData[0]->name; ?></td>
			<td><?php echo $e_key->concept; ?></td>
			<td><?php echo $e_key->question; ?></td>
			<td><?php echo $classData['name']; ?></td>
			<td><?php echo $e_key->subject; ?></td>
			<td><?php echo $activity_arr[$e_key->activity_type]; ?></td>
      <td class="action">
        <?php if ($e_key->status=="1") { ?>
          <span class="label-success label">Active</span>
        <?php } else { ?>
          <span class="label-danger label">Inactive</span>
        <?php } ?>  
      </td>
			<td class="action">
        <?php if ($e_key->is_published == 0) { ?>
				<a href="<?php echo base_url();?>admin/test_questions/edit/<?php echo $e_key->id;?>">
					<i class="ti-pencil"></i>
				</a>
        <?php } ?>
				<a href="<?php echo base_url();?>admin/test_questions/changestatus/<?php echo $e_key->id;?>">
					<?php if ($e_key->status==1) { ?>
            <i class="ti-thumb-down text-danger"></i>
					<?php } else { ?>
            <i class="ti-thumb-up text-success"></i>
          <?php } ?>
				</a>
			</td>
		</tr>
	<?php $i++; } ?>
  </tbody>
</table>
</div>
<script>
$("#allobservations").click(function(){
    $(".singleobservation").prop("checked",$(this).prop("checked"))
})
$(".singleobservation").click(function(){
    if ($(".singleobservation:checked").length == $(".singleobservation").length)
      $("#allobservations").prop("checked",true)
    else
      $("#allobservations").prop("checked",false)
})
$(".observationids").click(function(){
  d = confirm("Are you sure to disable all the selected observations. Once disabled they can't be enable again")
  if (d)
  {
    var observationids = []
    $(".singleobservation:checked").each(function(){
      observationids.push($(this).val())
    })
    $("#observationids").val(observationids)
    $("#disableobservations").submit();
  }
})
</script>
