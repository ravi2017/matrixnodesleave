<form name="frmObservation" method="post" action="" class="">
  <div class="row">
    <div class="col-md-3">
      <div class="form-group">
        <select class="form-control" id="state_id" name="state_id">
          <option value="">Select State</option>
          <?php foreach($states as $state) { ?>
          <option value="<?=$state->id?>" <?php echo (isset($state_id) and $state->id == $state_id) ? "selected" : ""?>><?php echo $state->name?></option>
          <?php } ?>
        </select>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <select class="form-control" id="type" name="type">
          <option value="">Select Activity</option>
          <?php foreach($activities as $key=>$value) { ?>
          <option value="<?=$key?>" <?php echo (isset($type) and $key == $type) ? "selected" : ""?>><?php echo $value?></option>
          <?php } ?>
        </select>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <input type="submit" name="submit" class="form-control btn btn-sm btn-success" value="Get Observation">
      </div>
    </div>
  </div>
</form>

<? if(!empty($observations_list)) { ?>
<div class="scrollme">  
<form name="manageSorting" id="manageSorting" method="post">
  
<table class="table table-striped table-bordered responsive">
  <input type="hidden" name="state_id" value="<?=$state_id?>">
  <input type="hidden" name="type" value="<?=$type?>">
  <tr>
    <th>State</th>
    <th>Question Type</th>
    <th>Question</th>
    <th>Class</th>
    <th>Sort Order</th>
  </tr>
	<?php $i=1;
    $activity_arr = unserialize(ACTIVITY_ARRAY);
  
    foreach ($observations_list as $e_key){ 
      
      $stateData = get_state_info($e_key->state_id);
      $classData = get_class_by_id($e_key->class_id);
      $subject_wise = ($e_key->subject_wise == 1 ? 'Yes' : 'No');
  ?>
		<tr>
			<td><?php echo $stateData[0]->name; ?></td>
			<td><?php echo $e_key->question_type; ?></td>
			<td><?php echo $e_key->question; ?></td>
			<td><?php echo $classData['name']; ?></td>
			<td class="action">
				<input type="text" name="sort_order[<?=$e_key->id?>][]" value="<?php echo $e_key->sort_order; ?>" size="3" maxlength="4">
			</td>
		</tr>
	<?php $i++; } ?>
</table>
<div class="col-md-2" align="right" style="padding-top:10px;">
  <input type="submit" name="submit" class="form-control btn btn-sm btn-success" value="Manage Sorting">
</div>
</form>
</div>
<?  }else{ ?>
<div class="col-md-12" align="center" style="padding-top:10px;">
  No Observation Found.
</div>  
<?  } ?>
