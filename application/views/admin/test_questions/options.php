<?
if (isset($observations))
{
	extract($observations);
}
?>
<div class="row">
  <div class="col-md-4"><label>Options </label> <a href="javascript:void(0)" class="icon-list-demo addoption"><i class="ion-plus-circled"></i></a></div>
  <div class="col-md-8">
    <div class="questionoptions">  
      <?php if (isset($observation_options)) {
          $option_count = count($observation_options);
          $o = 0;
        ?>
      <?php foreach($observation_options as $question_option) { 
          $checked_option = (isset($correct_option) && $correct_option == $question_option->id ? 'checked' : '');
        ?>
      <div class="row options">
        <input type="hidden" value="<?php echo $question_option->id; ?>" name="optionids[]">
        <div class="form-group col-md-1">
          <input type="radio" name="correct_option" value="<?=$o?>" <?=$checked_option;?>>
        </div>  
        <div class="form-group col-md-10">  
          <input type="text" name="options[]" value="<?php echo $question_option->option_value; ?>" class="form-control" ></div><div class="col-md-1"><a href="javascript:void(0)" title="Disable" class="icon-list-demo disableoption"><i class="ion-thumbsdown"></i></a>
        </div>
      </div>
      <?php $o++;
      } ?>
      <?php } else { 
         $option_count = 2; 
        ?>
      <div class="row options">
        <div class="form-group col-md-1">
          <input type="radio" name="correct_option" value="1" checked>
        </div>  
        <div class="form-group col-md-10">  
          <input type="text" name="options[1]" value="" class="form-control" ></div><div class="col-md-1"><a href="javascript:void(0)" class="icon-list-demo removeoption"><i class="ion-minus-circled"></i></a></div>
      </div>
      <div class="row options">
        <div class="form-group col-md-1">
          <input type="radio" name="correct_option" value="2">
        </div>  
        <div class="form-group col-md-10">  
          <input type="text" name="options[2]" value="" class="form-control" ></div><div class="col-md-1"><a href="javascript:void(0)" class="icon-list-demo removeoption"><i class="ion-minus-circled"></i></a></div>
      </div>
      <?php } ?>
    </div>
    <input type="hidden" id="option_count" value="<?=$option_count?>">
  </div>
</div>


<script>
$(document).ready(function() {
       
    $(".questionoptions").on('click','.removeoption',function(){
      d = confirm("Are you sure to Permanently Remove the option.")
      if (d)
      {
        var option_count = $("#option_count").val();
        var new_option_count = parseInt(option_count)-1;
        $(this).closest('.row').remove();
        $("#option_count").val(new_option_count);
        if ($(".questionoptions").find(".options").length > 1)
        {
          $(".removeoption").show()
        }
        else
        {
          $(".removeoption").hide()
        }
      }
    })
    
    $(".addoption").click(function(){
      var option_count = $("#option_count").val();
      var new_option_count = parseInt(option_count)+1;
      $(".questionoptions").append('<div class="row options"><div class="form-group col-md-1"><input type="radio" name="correct_option" value="'+new_option_count+'"></div><div class="form-group col-md-10"><input type="text" name="options['+new_option_count+']" value="" class="form-control" ></div><div class="col-md-1"><a href="javascript:void(0)" class="icon-list-demo removeoption"><i class="ion-minus-circled"></i></a></div></div>');
      if ($(".questionoptions").find(".options").length > 1)
      {
        $(".removeoption").show()
      }
      else
      {
        $(".removeoption").hide()
      }
      $("#option_count").val(new_option_count);
    })
    
    $(".questionoptions").on('click','.disableoption',function(){
      d = confirm("Are you sure to Disable the option. Can't be Enable again")
      if (d)
      {
        $(this).closest('.row').remove();
        if ($(".questionoptions").find(".options").length > 1)
        {
          $(".disableoption").show()
        }
        else
        {
          $(".disableoption").hide()
        }
      }
    })
    
    $("#question_type").change(function(){
      if ($(this).val() == "objective")
        $(".questionoptions").closest('.row').show()
      else
        $(".questionoptions").closest('.row').hide()
    })

    if ($("#question_type").val() == "objective")
      $(".questionoptions").closest('.row').show()
    else
      $(".questionoptions").closest('.row').hide()
});
</script>
