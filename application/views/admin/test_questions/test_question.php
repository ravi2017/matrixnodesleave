<?
if (isset($observations))
{
	extract($observations);
}

$activity_arr = unserialize(TEST_LINE_ARRAY);
?>
<form method="post" action="<?php echo base_url();?>admin/test_questions/<?=(isset($action)) ? $action : ''?>" role="form" name="frmobservation" id="frmobservation">
  <div class="row">
    <div class="col-md-8 offset-md-2">  
      <div class="row">    
        <label class="col-md-4" for="question_type">Activity Type <span class="make-me-red">*</span></label><span class="err" id="err_question_type"></span>
        <div class="col-md-8">
          <div class="form-group state_person">
            <select class="form-control required" id="activity_type" name="activity_type">
              <option value="">-Select Type-</option>
              <? foreach($activity_arr as $key=>$value) { ?>
                <option value="<?=$key?>" <? if(isset($activity_type) && $key == $activity_type){ echo 'selected'; } ?> ><?=$value?></option>
              <?  } ?>
            </select>
            <label id="activity_type-error" class="error" for="activity_type" style="display:none;"></label>
          </div>
        </div>
      </div>
      <div class="row">    
        <label class="col-md-4" for="question_type">Question Type <span class="make-me-red">*</span></label><span class="err" id="err_question_type"></span>
        <div class="col-md-8">
          <div class="form-group state_person">
            <select class="form-control required" id="question_type" name="question_type">
              <option value="">-Select Type-</option>
              <option value="objective" <? if(isset($question_type) && $question_type == 'objective'){ echo 'selected'; }?>>Objective</option>
            </select>
            <label id="question_type-error" class="error" for="question_type" style="display:none;"></label>
          </div>
        </div>
      </div>
      <div class="row">
        <label class="col-md-4" for="question">Question <span class="make-me-red">*</span></label><?php echo form_error('question'); ?>
        <div class="col-md-8">
          <div class="form-group">
            <span class="err" id="err_question"></span>
            <input type="text" class="form-control required" id="question" placeholder="Enter Question" name="question" value="<?=(isset($question)) ? $question : ''?>">
          </div>					
        </div>
      </div>
      <div class="row">
        <label class="col-md-4" for="state">Select State <span class="make-me-red">*</span></label><span class="err" id="err_state_id"></span>
        <div class="col-md-8">
          <div class="form-group state_person">
            <select class="form-control basic-multiple select2 required" id="state_id" name="state_id[]" <? if(empty($id)) { ?> multiple <? } ?> >
              <option value="">-Select State-</option>
              <? foreach($states as $state) { ?>
              <option value="<?=$state->id?>" <?=(isset($state_id) and $state->id == $state_id) ? "selected" : ""?>><?=$state->name?></option>
              <? } ?>
            </select>
            <label id="state_id-error" class="error" for="state_id" style="display:none;"></label>
          </div>
        </div>
      </div>
      <div class="row">
        <label class="col-md-4" for="class">Select Class <span class="make-me-red">*</span></label><span class="err" id="err_class_id"></span>
        <div class="col-md-8">
          <div class="form-group state_person">
            <select class="form-control select2 required" id="class_id" name="class_id">
              <option value="">-Select Class-</option>
              <? foreach($classes as $class) { ?>
              <option value="<?=$class->id?>" <?=(isset($class_id) and $class->id == $class_id) ? "selected" : ""?>><?=$class->name?></option>
              <? } ?>
            </select>
            <label id="class_id-error" class="error" for="class_id" style="display:none;"></label>
          </div>
        </div>        
      </div>
      
      <div class="row">
        <label class="col-md-4" for="subject">Select Subject <span class="make-me-red">*</span></label><span class="err" id="err_subject_id"></span>
        <div class="col-md-8">
          <div class="form-group state_person">
            <select class="form-control required select2" id="subject_id" name="subject_id">
              <option value="">-Select Subject-</option>
              <? foreach($subjects as $subject) { ?>
              <option value="<?=$subject->id?>" <?=(isset($subject_id) and $subject->id == $subject_id) ? "selected" : ""?>><?=$subject->name?></option>
              <? } ?>
            </select>
            <label id="subject_id-error" class="error" for="subject_id" style="display:none;"></label>
          </div>
        </div>
      </div>
      
      <div class="row">
        <label class="col-md-4" for="subject">Select Concept <span class="make-me-red">*</span></label><span class="err" id="err_concept_id"></span>
        <div class="col-md-8">
          <div class="form-group state_person">
            <select class="form-control required select2" id="concept_id" name="concept_id">
              <option value="">-Select Concept-</option>
              <? if (isset($concepts)) { ?>
              <? foreach($concepts as $concept) { ?>
              <option value="<?=$concept->id?>" <?=(isset($concept_id) and $concept->id == $concept_id) ? "selected" : ""?>><?=$concept->name?></option>
              <? } ?>
              <? } ?>
            </select>
            <label id="concept_id-error" class="error" for="concept_id" style="display:none;"></label>
          </div>
        </div>        
      </div>
      
      <!--<div class="row">
        <label class="col-md-4" for="weightage">Weightage</label><?php echo form_error('weightage'); ?><br />
        <div class="col-md-8">
          <div class="form-group">
            <span class="err" id="err_weightage"></span>
            <input type="text" class="form-control" name="weightage" id="weightage" value="<? if(!empty($weightage)){ echo $weightage; }?>" placeholder="Enter Weightage">
          </div>
        </div>
      </div>
      <div class="row subjectwiserequired">
        <label class="col-md-4" for="weightage">Subject Wise Responses Required</label><?php echo form_error('weightage'); ?><br />
        <div class="col-md-8">
          <div class="form-group">
            <span class="err" id="err_subject_wise"></span>
            <input type="checkbox" name="subject_wise" id="subject_wise" value="yes" <? if(isset($subject_wise) && $subject_wise == '1'){ echo 'checked'; } ?>>
          </div>
        </div>
      </div>
      <div class="row">
        <label class="col-md-4" for="subject_wise">Used for Productive Report</label><?php echo form_error('productivity_flag'); ?>
        <div class="col-md-8">
          <div class="form-group">
            <span class="err" id="err_productivity_flag"></span>
            <input type="checkbox" name="productivity_flag" id="productivity_flag" value="yes" <? if(isset($productivity_flag) && $productivity_flag == '1'){ echo 'checked'; } ?>>
          </div>  
        </div> 
      </div>
      <div class="row">
        <label class="col-md-4" for="subject_wise">Used for WhatsApp Report</label><?php echo form_error('productivity_flag'); ?>
        <div class="col-md-8">
          <div class="form-group">
            <span class="err" id="err_whatsapp_flag"></span>
            <input type="checkbox" name="whatsapp_flag" id="whatsapp_flag" value="yes" <? if(isset($whatsapp_flag) && $whatsapp_flag == '1'){ echo 'checked'; } ?>>
          </div>  
        </div>  
      </div>-->
      <?php echo $this->load->view('admin/test_questions/options','',true) ?>        
    </div>
  </div>
  
  <div class="row">
    <div class="col-md-12">    
      <div class="form-group text-right">
        <? if (!empty($id)) {	?>
          <input type="hidden" name="id" value="<?php echo $id; ?>" />
        <?  }	?>
        <a href="<?php echo base_url('admin/test_questions'); ?>" class="btn btn-sm btn-default">Cancel</a>       
        <button type="submit" class="btn btn-sm btn-success">Submit</button>
      </div>
    </div>
  </div>
</form>

<script>
$(document).ready(function() {
    $("#frmobservation").validate();
    $('.basic-multiple').select2();
    $("#class_id").change(function(){
        var classes = [];
        $.each($("#class_id option:selected"), function(){
          classes. push($(this). val());
        });
        //alert(classes.length)
        if (classes.length > 0)
        {
          $(".subjectwiserequired").show()
        }
        else
        {
          $("#subject_wise").prop("checked",false)
          $(".subjectwiserequired").hide()
        }
    });
    $("#subject_wise").click(function(){
      var classes = [];
      $.each($("#class_id option:selected"), function(){
        classes. push($(this). val());
      });
      /*if ($(this).prop("checked") && classes.length == 0)
      {
        alert("Select atleast one class to make the question subject wise")
        $(this).prop("checked",false)
      } */     
      
      //if ($(this).prop("checked") && ($("#question_type").val() == "yes-no" || $(this).val() == "objective"))
      if ($(this).prop("checked") && $("#question_type").val() != "yes-no" && $("#question_type").val() != "yes-no-na")
      {
        alert("Subject wise question is only available for Yes/No or Yes/No/NA type question")
        $(this).prop("checked",false)
      }      
    });
    $("#question_type").change(function(){
      if ($(this).val() == "yes-no" || $(this).val() == "yes-no-na")
      {
        $(".subjectwiserequired").show()
      }
      else
      {
        $("#subject_wise").prop("checked",false)
        $(".subjectwiserequired").hide()
      }
    })
});
</script>


<script>

function get_concept(test_type,class_id,subject_id,concept_id)
{
  if (class_id > 0 && subject_id > 0)
  {
    $("#concept_id").html("<option value=''>Select Concept</option>");
    $.ajax({
      url: '<?=base_url()?>admin/concepts/get_list',
      type: 'get',
      data: {
        class_id: class_id,
        subject_id: subject_id,
        test_type: test_type
      },
      dataType: 'json',
      success: function(response) {
        $.each(response, function() {
          if (concept_id == this['id'])
            $("#concept_id").append("<option value='" + this['id'] + "' selected>" + this['value'] + "</option>");
          else
            $("#concept_id").append("<option value='" + this['id'] + "'>" + this['value'] + "</option>");
        });
        $("#concept_id").show();
      },
      error: function(response) {
        window.console.log(response);
      }
    });
  }
}


$("#class_id").change(function() {
  subject = $("#subject_id").val();
  activity_type = $("#activity_type").val();
  get_concept(activity_type,$(this).val(),subject,'')
});

$("#subject_id").change(function() {
  class_id = $("#class_id").val();
  activity_type = $("#activity_type").val();
  get_concept(activity_type,class_id,$(this).val(),'')
});

$("#activity_type").change(function() {
  class_id = $("#class_id").val();
  subject = $("#subject_id").val();
  get_concept($(this).val(),class_id,subject,'')
});

<? if (isset($activity_type) and isset($class_id) and isset($subject_id) and isset($concept_id)) { ?>
  get_concept('<?=$activity_type?>','<?=$class_id?>','<?=$subject_id?>','<?=$concept_id?>')
<? } ?>

</script>	
