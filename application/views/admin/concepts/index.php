	<script type="text/javascript">
function show_confirm(act,gotoid)
{
if(act=="edit")
var r=confirm("Do you really want to edit?");
else
var r=confirm("Do you really want to delete?");
if (r==true)
{
window.location="<?php echo base_url();?>admin/concepts/"+act+"/"+gotoid;
}
}
</script>
<a class="btn btn-success btn-sm" href="<?php echo base_url();?>admin/concepts/add">Add New Concept</a><br><br>
<div style="display:none;" class="alert alert-info"></div>
<table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
	<thead>
		<tr>
			<th>ID</th>
			<th>Class</th>
			<th>Subject</th>
			<th>Concept</th>
			<th>Status</th>
			<th>Actions</th>
		</tr>
	</thead>
	<tbody>
	
	<? $admins_count = 0; ?>
	<?php foreach ($concepts_list as $e_key){ ?>
		<tr>
			<td><?php echo $admins_count = $admins_count + 1; ?></td>
			<td><?php echo $e_key->class_name; ?></td>
			<td><?php echo $e_key->subject_name; ?></td>
			<td><?php echo $e_key->name; ?></td>
			<td>
        <?php if ($e_key->status=="1") { ?>
          <span class="label-success label">Active</span>
        <?php } else { ?>
          <span class="label-danger label">Inactive</span>
        <?php } ?>  
      </td>
			
			<td class="center">
        <a href="<?php echo base_url();?>admin/concepts/edit/<?php echo $e_key->id;?>">
					<i class="ti-pencil"></i>
				</a>
				<a href="<?php echo base_url();?>admin/concepts/changestatus/<?php echo $e_key->id;?>">
					<?php if ($e_key->status==1) { ?>
            <i class="ti-thumb-down text-danger"></i>
					<?php } else { ?>
            <i class="ti-thumb-up text-success"></i>
          <?php } ?>
				</a>			
			</td>
		</tr>
	<?php } ?>
		</tbody>
	</table>
	
	
	
