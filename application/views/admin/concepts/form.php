<?
if (isset($admins))
{
	extract($admins);
}
?>

<form name="Formpage" id="frmconcepts" action="<?php echo base_url();?>admin/concepts/<?=(isset($action)) ? $action : ''?>" method="post" accept-charset="utf-8" enctype="multipart/form-data" >
		
  <div class="row">
    <div class="col-md-8 offset-md-2"> 
      <div class="row">    
        <label class="col-md-4" for="class">Select Class <span class="make-me-red">*</span></label><span class="err" id="err_class_id"></span>
        <div class="col-md-8">
          <div class="form-group class">
            <select class="form-control required" id="class_id" name="class_id">
              <option value="">Select Class</option>
              <? foreach($classes_list as $classes) { ?>
              <option value="<?=$classes->id?>"<?=(isset($admins) and $classes->id == $class_id) ? "selected" : ""?>><?=$classes->name?></option>
              <? } ?> 
            </select>
            <label id="class_id-error" class="error" for="class_id" style="display:none;"></label>
          </div>
        </div>
      </div>
      <div class="row">    
        <label class="col-md-4" for="subject">Select Subject <span class="make-me-red">*</span></label><span class="err" id="err_subject_id"></span>
        <div class="col-md-8">
          <div class="form-group subject">
            <select class="form-control required" id="subject_id" name="subject_id">
              <option value="">Select Subject</option>
              <? foreach($subjects_list as $subject) { ?>
              <option value="<?=$subject->id?>" <?=(isset($admins) and $subject->id == $subject_id) ? "selected" : ""?>><?=$subject->name?></option>
              <? } ?> 
            </select>
            <label id="subject_id-error" class="error" for="subject_id" style="display:none;"></label>
          </div>
        </div>
      </div>
      <div class="row">
        <label class="col-md-4" for="concept">Concept <span class="make-me-red">*</span></label><?php echo form_error('concept'); ?>
        <div class="col-md-8">
          <div class="form-group">
            <span class="err" id="err_concept"></span>
            <input type="text" class="form-control required" id="name" placeholder="Enter Concept" name="name" value="<?=(isset($name)) ? $name : ''?>">
          </div>					
        </div>
      </div>
      <div class="row">
        <label class="col-md-4" for="concept">Available For <span class="make-me-red">*</span></label><?php echo form_error('concept'); ?>
        <div class="col-md-8">
          <div class="form-group">
            <label class="checkbox-inline">
                <input type="checkbox" id="inlineCheckbox2" name="available[]" value="base_line" <?=(isset($admins) and $base_line ==1) ? "checked" : ""?>> Base Line
            </label>
            <label class="checkbox-inline">
                <input type="checkbox" id="inlineCheckbox3" name="available[]" value="end_line" <?=(isset($admins) and $end_line ==1) ? "checked" : ""?>> End Line
            </label><br>
            <label for="available[]" class="error" style="display:none">Please select at least one.</label>		
          </div>			
        </div>
      </div>
  	</div>
  </div>
  <div class="row">
    <div class="col-md-12">    
      <div class="form-group text-right">
        <? if (!empty($id)) {	?>
          <input type="hidden" name="id" value="<?php echo $id; ?>" />
        <?  }	?>
        <a href="<?php echo base_url('admin/concepts'); ?>" class="btn btn-sm btn-default">Cancel</a>       
        <button type="submit" class="btn btn-sm btn-success">Submit</button>
      </div>
    </div>
  </div>
</form>
	

<script>
$(document).ready(function() {
  $("#frmconcepts").validate({
    rules: { 
            "available[]": { 
                    required: true, 
                    minlength: 1 
            } 
    }, 
    messages: { 
            "available[]": "Please select at least one."
    }   
  });
  $('.basic-multiple').select2();
});
</script>
