<a class="btn btn-sm btn-success" href="<?php echo base_url();?>admin/offdays/add">Add Offday</a><br><br>
<div style="display:none;" class="alert alert-info"></div>
<table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
	<thead>
		<tr>
			<th>Holiday Date</th>
			<th>Day</th>
			<th>Holiday Type</th>
			<th class="action no-sort no-search">Actions</th>
		</tr>
	</thead>
	<tbody>
	
	<? $holiday_count = 0; ?>
	<?php foreach ($holidays_list as $e_key){ ?>
		<tr>
			<td><?php echo date("d-m-Y",strtotime($e_key->activity_date)); ?></td>
      <td><?php echo $e_key->type; ?></td>
			<td><?php echo $USER_GROUPS[$e_key->user_group]; ?></td>
			<td class="action">
				<a class="btn btn-danger" onclick="return confirm('Are you sure to remove ?');" href="<?php echo base_url()?>admin/offdays/delete/<?php echo $e_key->id?>">
					<i class="glyphicon glyphicon-trash icon-white"></i>
					Delete
				</a>
			</td>
		</tr>
	<?php } ?>
		</tbody>
	</table>
	
	
	
