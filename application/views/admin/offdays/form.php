<?
if (isset($holidays[0]))
{
	extract($holidays[0]);
  $holiday_date = implode("-",array_reverse(explode("-",$holiday_date)));
}
?>

<form action="<?php echo base_url();?>admin/offdays/<?=(isset($action)) ? $action : ''?>" method="post" accept-charset="utf-8" onsubmit="return campaignvalidate('<?=(isset($action)) ? $action : ''?>')" name="Formcampaign">
  <?
  if (isset($error))
  {
  ?>
  <div class="alert alert-danger"><?php echo $error; ?></div>
  <?php
  }
  ?>
	<div class="form-group">
		<label for="name"><strong>Type</strong></label><?php echo form_error('title'); ?>
		<span class="err" id="err_name"></span>
    <select name="user_group" id="user_group">
			<? foreach($USER_GROUPS as $key=>$value) { ?>
				<option value="<?=$key;?>" <?php if(isset($user_group) and $user_group == $key) { echo 'selected'; } ?> ><?=$value?></option>
      <?	}	?>
    </select>
	</div>
  <div class="form-group">
		<label for="name"><strong>Holiday Date</strong></label><?php echo form_error('title'); ?>
		<span class="err" id="err_name"></span>
		<input type="text" class="form-control" id="holiday_date" placeholder="Enter Holiday Date" name="holiday_date" value="<?=(isset($holiday_date)) ? $holiday_date : ''?>" autocomplete="off">
	</div>
	<div class="form-group">
		<?
		if (isset($id))
		{
		?>
		<input type="hidden" name="id" value="<?php echo $id; ?>" />
		<?
		}
		?>
		<button type="submit" class="btn btn-default">Submit</button>
	</div>
</form>


<script>
$( document ).ready(function() {
  $("#holiday_date").datepicker({
    minDate: +0,
    changeMonth:true,
    dateFormat:"yy-mm-dd",
    changeYear:true
  });
});
</script>

