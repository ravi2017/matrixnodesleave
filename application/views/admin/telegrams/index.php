<script type="text/javascript">
function show_confirm(act,gotoid)
{
if(act=="edit")
var r=confirm("Do you really want to edit?");
else
var r=confirm("Do you really want to delete?");
if (r==true)
{
window.location="<?php echo base_url();?>admin/telegrams/"+act+"/"+gotoid;
}
}
</script>
<a class="btn btn-info btn-sm" href="<?php echo base_url();?>admin/telegrams/add">Add Group</a><br><br>
<div style="display:none;" class="alert alert-info"></div>
<table class="table table-striped table-bordered bootstrap-datatable datatable responsive customdatatable">
	<thead>
		<tr>
			<th>State</th>
			<th>Group Code</th>
			<th class="no-sort action no-search">Actions</th>
		</tr>
	</thead>
	<tbody>
	
	<? $state_count = 0; ?>
	<?php foreach ($group_list as $e_key){ ?>
		<tr>
			<td><?php echo $e_key->state_id; ?></td>
			<td><?php echo $e_key->group_id; ?></td>
			<td class="action">
				<a class="" href="<?php echo base_url();?>admin/telegrams/edit/<?php echo $e_key->id;?>">
					<i class="ti-pencil"></i>
				</a>
        <a class="" href="<?php echo base_url();?>admin/telegrams/delete/<?php echo $e_key->id;?>" onclick="return confirm('Are you sure to delete this record.')">
					<i class="ti-trash"></i>
				</a>
        <a href="<?php echo base_url();?>admin/telegrams/changestatus/<?php echo $e_key->id;?>">
					<?php if ($e_key->status==1) { ?>
            <i class="ti-thumb-up text-success"></i>
					<?php } else { ?>
            <i class="ti-thumb-down text-danger"></i>
          <?php } ?>
				</a>
        <!--<a class="" href="<?php echo base_url();?>admin/telegrams/send_message/<?php echo $e_key->group_id;?>/<?php echo $e_key->state_id;?>">
					<i class="ti-info"></i>
				</a>-->
			</td>
		</tr>
	<?php } ?>
  </tbody>
</table>
	
	
	
