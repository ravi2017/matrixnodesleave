<?
if (isset($state)){
	extract($state);
}
?>
<form action="<?php echo base_url();?>admin/telegrams/<?=(isset($action)) ? $action : ''?>" method="post" accept-charset="utf-8" name="frmState" id="frmTelegrams">
	<div class="row">
    <div class="col-md-6">
      <div class="form-group">
        <label for="state">State</label><?php echo form_error('state_id'); ?>
        <select name="state_id" id="state_id" class="form-control required">
          <option value=''>-Select State-</option>
          <? foreach($state_list as $states) { ?>
            <option value="<?=$states->name;?>" <?php echo (isset($groups) && $groups['state_id'] == $states->name ? 'selected' : ''); ?> ><?=$states->name;?></option>
          <?  } ?>  
        </select>
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        <label for="group_id">Group Code</label><?php echo form_error('group_id'); ?>
        <span class="err" id="err_group_id"></span>
        <input type="text" class="form-control required" id="group_id" placeholder="Enter Group Id" name="group_id" value="<?=(isset($groups)) ? $groups['group_id'] : ''?>" minlength="2" maxlength="30">
      </div>
    </div>
  </div>
	<div class="form-group">
		<?
		if (isset($id))
		{
		?>
		<input type="hidden" name="id" value="<?php echo $id; ?>" />
		<?
		}
		?>
		<button type="submit" class="btn btn-default">Submit</button>
	</div>
</form>
<script>
  $(document).ready(function() {
    $("#frmTelegrams").validate();
  });
</script>
