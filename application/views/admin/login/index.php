
<div class="alert alert-info">
Please login with your Username and Password.
</div>
<?php echo validation_errors(); ?>
<?php echo form_open('admin/verifyLogin'); ?>
  <fieldset>
    <div class="input-group input-group-lg">
      <span class="input-group-addon"><i class="glyphicon glyphicon-user red"></i></span>
      <input type="text" class="form-control" placeholder="Username" name="username">
    </div>
    <div class="clearfix"></div><br>
    <div class="input-group input-group-lg">
      <span class="input-group-addon"><i class="glyphicon glyphicon-lock red"></i></span>
      <input type="password" class="form-control" placeholder="Password" name="password">
    </div>
    <div class="clearfix"></div>
    <p class="center col-md-5">
      <button type="submit" class="btn btn-primary">Login</button>
    </p>
  </fieldset>
</form>
