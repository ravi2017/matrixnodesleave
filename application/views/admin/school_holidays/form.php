  <?
if (isset($holidays[0]))
{
	extract($holidays[0]);
  $holiday_date = implode("-",array_reverse(explode("-",$holiday_date)));
}
?>

<form action="<?php echo base_url();?>admin/school_holidays/<?=(isset($action)) ? $action : ''?>" method="post" accept-charset="utf-8" onsubmit="return campaignvalidate('<?=(isset($action)) ? $action : ''?>')" name="Formcampaign">
  <?
  if (isset($error))
  {
  ?>
  <div class="alert alert-danger"><?php echo $error; ?></div>
  <?php
  }
  ?>
	<div class="form-group">
		<label for="type">Type</label><?php echo form_error('type'); ?>
    <select name="holiday_type" id="holiday_type">
      <option value="GOVT" <?php if(isset($holiday_type) and $holiday_type == "GOVT") { echo 'selected'; } ?> >Govt</option>
      <option value="HR" <?php if(isset($holiday_type) and $holiday_type == "HR") { echo 'selected'; } ?>>HR</option>
    </select>
	</div>
  <div class="form-group">
    <label for="holiday_name">Holiday Name</label><?php echo form_error('holiday_name'); ?>
    <input type="text" class="form-control" id="name" placeholder="Enter Holiday Name" name="name" value="<?=(isset($name)) ? $name : ''?>">
  </div>
	<div class="form-group">
		<label for="start_date">Holiday Start Date</label><?php echo form_error('start_date'); ?>
			<div class='input-group date' id='datetimepicker1'>
				<input type="text" class="form-control" id="start_date" placeholder="Enter Holiday Start Date" name="start_date" value="<?=(isset($start_date)) ? $start_date : ''?>" autocomplete="off">
				<span class="input-group-addon">
					<span class="icon-calendar"></span>
				</span>
			</div>
	</div>
	<div class="form-group">
		<label for="end_date">Holiday End Date</label><?php echo form_error('end_date'); ?>
		<div class='input-group date' id='datetimepicker2'>
			<input type="text" class="form-control endDate" id="end_date" placeholder="Enter Holiday End Date" name="end_date" value="<?=(isset($end_date)) ? $end_date : ''?>" autocomplete="off">
			<span class="input-group-addon">
				<span class="icon-calendar"></span>
			</span>
			<input type="hidden" name="total_days" class="form-control" id="total_days">
		</div>
	</div>
  <div class="form-group states">
    <label for="state">Select State</label>
    <? foreach($states as $state) { ?>
    <input name="states[]" type="checkbox" value="<?=$state->id?>" <?php if(isset($state_ids) and in_array($state->id, $state_ids)) echo "checked"; ?>><?=$state->name?>
    <? } ?>
  </div>
	<div class="form-group">
		<?
		if (isset($id))
		{
		?>
		<input type="hidden" name="id" value="<?php echo $id; ?>" />
		<?
		}
		?>
		<button type="submit" class="btn btn-default">Submit</button>
	</div>
</form>


<script>
$( document ).ready(function() {
  /*$("#holiday_date").datepicker({
    minDate: +0,
    changeMonth:true,
    dateFormat:"yy-mm-dd",
    changeYear:true
  });*/
  
  $(document).ready(function ()
  {
     var date = new Date();
     date.setDate(date.getDate() - 4);

      $('#start_date').datepicker({
         format: "d/m/Y",
         minDate: +0,
         autoclose: true
      });

      $('#end_date').datepicker({
        format: "d/m/Y",
        minDate: +0,
        autoclose: true
      });

      $.validator.addMethod("endDate", function(value, element) {
          var startDate = $('#start_date').val();
          return Date.parse(startDate) <= Date.parse(value) || value == "";
      }, "* End date must be after from date");

      
	  
      $("#end_date").change(function(){
        if($("#start_date").val() != '' &&  $("#end_date").val() != ''){
          var start_date = $('#start_date').val();
          var end_date = $('#end_date').val();
          var timeDiff  = (new Date(end_date)) - (new Date(start_date));
          
          var days      = (timeDiff / (1000 * 60 * 60 * 24))+1;
          $("#total_days").val(parseInt(days));    
          if(days <= 0){
            alert('Please select a valid date');
            $("#total_days").val('');    
            $("#end_date").val('');
          }
          
        }
      });

      $("#start_date").change(function(){
        if($("#start_date").val() != '' &&  $("#end_date").val() != ''){
          var start_date = $('#start_date').val();
          var end_date = $('#end_date').val();
          var timeDiff  = (new Date(end_date)) - (new Date(start_date));
          var days = (timeDiff / (1000 * 60 * 60 * 24))+1;
          $("#total_days").val(parseInt(days));    
          if(days <= 0){
            alert('Please select a valid date');
            $("#total_days").val('');    
            $("#start_date").val('');
          }
        }
        else if($("#start_date").val() != '' &&  $("#end_date").val() == ''){
					$("#end_date").val($("#start_date").val());
					$("#total_days").val('1');
				}
      });
      

      //$("#frmleave").validate();  

  });

  /*$("#is_national").click(function(){
    if ($(this).prop("checked"))
      $(".states").hide()
    else
      $(".states").show()
  });
  
  if ($("#is_national").prop("checked"))
    $(".states").hide()
  else
    $(".states").show()
    */
});
</script>

