<script type="text/javascript">
function show_confirm(act,gotoid)
{
if(act=="edit")
var r=confirm("Do you really want to edit?");
else
var r=confirm("Do you really want to delete?");
if (r==true)
{
window.location="<?php echo base_url();?>admin/users/"+act+"/"+gotoid;
}
}
</script>
<a href="<?php echo base_url();?>admin/users/add">Add New User</a><br><br>
<div style="display:none;" class="alert alert-info"></div>
<table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
	<thead>
		<tr>
			<th>ID</th>
			<th>Login ID</th>
			<th>Name</th>
			<th>Email</th>
			<th>Role</th>
			<th>Status</th>
			<th>Actions</th>
		</tr>
	</thead>
	<tbody>
	
	<? $users_count = 0; ?>
	<?php foreach ($users_list as $e_key){ ?>
		<tr>
			<td><?php echo $users_count = $users_count + 1; ?></td>
			<td><?php echo $e_key->login_id; ?></td>
			<td><?php echo $e_key->name; ?></td>
			<td><?php echo $e_key->email; ?></td>
			<td><?php echo role_name($e_key->role); ?></td>
        <td><?php if($e_key->status=="1"){?>
			 <span class="label-success label label-default">	Active	</span>
				<?php }else{?>
		 <span class="label-danger label label-default"> Inactive</span>
				
				<?php }?>  </td>
			<td class="center">
				<a class="btn btn-info" href="<?php echo base_url();?>admin/users/edit/<?php echo $e_key->id;?>">
					<i class="glyphicon glyphicon-edit icon-white"></i>
					Edit
				</a>
				<a class="btn btn-danger" href="#" onClick="show_confirm('delete',<?php echo $e_key->id;?>)">
					<i class="glyphicon glyphicon-trash icon-white"></i>
					Delete
				</a>
         <?if($e_key->status=="1"){?>
        <a class="btn btn-success" href="<?php echo base_url();?>admin/users/changestatus/<?php echo $e_key->id;?>">
         	Mark Inactive	
          	</a>
          <?}else{?>
            <a class="btn btn-danger" href="<?php echo base_url();?>admin/users/changestatus/<?php echo $e_key->id;?>">
				Mark Active	
				
				<?}?>	</a>
			
			</td>
		</tr>
	<?php } ?>
		</tbody>
	</table>
