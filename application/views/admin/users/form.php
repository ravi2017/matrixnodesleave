<?
if (isset($user))
{
	extract($user);
}
?>
<form method="post" action="<?php echo base_url();?>admin/users/<?=(isset($action)) ? $action : ''?>" role="form" name="frmuser" onsubmit="return validateuser()" id="frmuser">
					
	<div class="form-group">
		<label for="user_name">Login ID</label><?php echo form_error('login_id'); ?>
    <span class="err" id="err_login_id"></span>
		<input type="text" class="form-control" id="login_id" placeholder="Enter Login ID" name="login_id" value="<?=(isset($login_id)) ? $login_id : ''?>">
	</div>					
	<div class="form-group">
		<label for="user_name">Password</label><?php echo form_error('password'); ?>
    <span class="err" id="err_password"></span>
		<input type="password" class="form-control" id="password" placeholder="Enter Password" name="password" value="">
	</div>
	<div class="form-group">
		<label for="user_name">Name</label><?php echo form_error('name'); ?>
    <span class="err" id="err_name"></span>
		<input type="text" class="form-control" id="name" placeholder="Enter Name" name="name" value="<?=(isset($name)) ? $name : ''?>">
	</div>					
	<div class="form-group">
		<label for="user_name">Email</label><?php echo form_error('email'); ?>
    <span class="err" id="err_email"></span>
		<input type="text" class="form-control" id="email" placeholder="Enter EMail ID" name="email" value="<?=(isset($email)) ? $email : ''?>">
	</div>					
	<div class="form-group">
		<label for="user_name">Mobile</label><?php echo form_error('mobile'); ?>
    <span class="err" id="err_mobile"></span>
		<input type="text" class="form-control" id="mobile" placeholder="Enter Mobile" name="mobile" value="<?=(isset($mobile)) ? $mobile : ''?>">
	</div>					
  <div class="form-group">
    <label for="title">Select Role</label><span class="err" id="err_role"></span>
    <select class="form-control" id="role" name="role">
      <option value="super_admin" <?=(isset($role) and $role == "super_admin") ? 'selected' : ''?>>Super Admin</option>
<!--      <option value="sampark_admin" <?=(isset($role) and $role == "sampark_admin") ? 'selected' : ''?>>Sampark Admin</option>
      <option value="inventory_admin" <?=(isset($role) and $role == "inventory_admin") ? 'selected' : ''?>>Inventory Admin</option>
      <option value="report_user" <?=(isset($role) and $role == "report_user") ? 'selected' : ''?>>Full Inventory Report Access</option>
      <option value="state_person" <?=(isset($role) and $role == "state_person") ? 'selected' : ''?>>State Person</option>
      <option value="store_head" <?=(isset($role) and $role == "store_head") ? 'selected' : ''?>>Store Head</option>
      <option value="field_user" <?=(isset($role) and $role == "field_user") ? 'selected' : ''?>>Field User</option>
      <option value="employee" <?=(isset($role) and $role == "employee") ? 'selected' : ''?>>Employee</option>-->
    </select>
  </div>
  <div class="form-group state_person">
    <label for="title">Select State</label><span class="err" id="err_title"></span>
    <select class="form-control" id="state_id" name="state_id">
      <option value="">Select State</option>
      <? foreach($states as $state) { ?>
      <option value="<?=$state->id?>" <?=(isset($state_id) and $state->id == $state_id) ? "selected" : ""?>><?=$state->name?></option>
      <? } ?>
    </select>
  </div>
  <div class="form-group field_user">
    <label for="title">Select District</label><span class="err" id="err_title"></span>
    <select class="form-control" id="district_id" name="district_id">
      <option>Select District</option>
    </select>
  </div>
	<div class="form-group">
		<?
		if (isset($id))
		{
		?>
		<input type="hidden" name="id" value="<?php echo $id; ?>" />
		<?
		}
		?>
		<button type="submit" class="btn btn-default">Submit</button>
	</div>
</form>

<script>

$("#role").change(function(){
    if ($(this).val() == "field_user")
    {
      $(".state_person").show()
      $(".field_user").show()
    }
    else if ($(this).val() == "state_person")
    {
      $(".state_person").show()
      $(".field_user").hide()
    }
    else
    {
      $(".state_person").hide()
      $(".field_user").hide()
    }
})
  
function get_district_list(sid,did)
{
  $.ajax({
    url: '<?=base_url()?>admin/dashboard/get_districts',
    type: 'get',
    data: {
      state: sid
    },
    dataType: 'json',
    success: function(response) {
      $("#district_id").html("<option value=''>Select District</option>");
      $.each(response, function() {
        if (did == this['id'])
          $("#district_id").append("<option value='" + this['id'] + "' selected>" + this['value'] + "</option>");
        else
          $("#district_id").append("<option value='" + this['id'] + "'>" + this['value'] + "</option>");
      });
      $("#district_id").show();
    },
    error: function(response) {
      window.console.log(response);
    }
  });
}

$("#state_id").change(function() {
  get_district_list($(this).val(),'')
});

<? if (isset($state_id) and $state_id > 0) { ?>
  get_district_list(<?=$state_id?>,<?=$district_id?>);
<? } ?>

<? if (isset($role) and $role == "field_user") { ?>
    $(".state_person").show()
    $(".field_user").show()
<? } elseif (isset($role) and $role == "state_person") { ?>
    $(".state_person").show()
    $(".field_user").hide()
<? } else { ?>
    $(".state_person").hide()
    $(".field_user").hide()
<? } ?>

function validateuser()
{
  if ($("#login_id").val() == "")
  {
    alert("Please enter login id")
    return false;
  }
  if ($("#name").val() == "")
  {
    alert("Please enter name")
    return false;
  }
  if ($("#email").val() == "")
  {
    alert("Please enter email")
    return false;
  }
  <?
  if (!isset($id))
  {
  ?>
  if ($("#password").val() == "")
  {
    alert("Please enter password")
    return false;
  }
  <?
  }
  ?>
  if ($("#role").val() == "")
  {
    alert("Please select role")
    return false;
  }
  if ($("#role").val() == "state_person")
  {
    if ($("#state_id").val() == "")
    {
      alert("Please select state")
      return false;
    }
  }
  if ($("#role").val() == "field_user")
  {
    if ($("#state_id").val() == "")
    {
      alert("Please select state")
      return false;
    }
    if ($("#district_id").val() == "")
    {
      alert("Please select district")
      return false;
    }
  }
}
</script>
