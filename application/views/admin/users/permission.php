<script type="text/javascript">
function show_confirm(act,gotoid)
{
  var r=confirm("Do you really want to delete?");
  if (r==true)
  {
    window.location="<?php echo base_url();?>admin/users/"+<?=$users['id']?>+"/deletepermission/"+gotoid;
  }
}
</script>

<?
if (isset($users))
{
	extract($users);
}
?>
<div id="newpermission" style="display:none" class="col-md-8 col-xs-offset-2">
<form action="<?php echo base_url();?>admin/users/<?php echo $users['id']; ?>/permissions" method="post" accept-charset="utf-8" name="Formcampaign">
	
  <div class="form-group permission-on">
    <div class="col-md-6">
		<label for="state">State</label>
		<select class="form-control" id="state_id" name="state_id">
      <option value="">Select State</option>
      <? foreach($states as $state) { ?>
      <option value="<?=$state->id?>"><?=$state->name?></option>
      <? } ?>
    </select>
    </div>
    <div class="col-md-6">
		<label for="district">District</label>
		<select class="form-control" id="district_id" name="district_id">
    </select>
    </div>
	</div>
  <div class="form-group permission-on">
    <div class="col-md-6">
      <label for="block">Block</label>
      <select class="form-control" id="block_id" name="block_id">
      </select>
    </div>
    <div class="col-md-6">
      <label for="cluster">Cluster</label>
      <select class="form-control" id="cluster_id" name="cluster_id">
      </select>
    </div>
	</div>
  <div class="form-group permission-on">
    <div class="col-md-6">
      <label for="village">Village</label>
      <select class="form-control" id="village_id" name="village_id">
      </select>
    </div>
    <div class="col-md-6">
      <label for="school">School</label>
      <select class="form-control" id="school_id" name="school_id">
      </select>
    </div>
	</div>
  <div class="form-group permission-on">
    <div class="col-md-6">
      <label for="school">Permission Type</label>
      <select class="form-control" id="permission" name="permission">
        <option value="read">Read</option>
        <option value="write">Write</option>
      </select>
    </div>
    <div class="col-md-6">
      <label for="school">&nbsp;<input type="hidden" name="userid" value="<?php echo $users['id']; ?>" /></label>
      <div>
        <button type="submit" class="btn btn-default">Submit</button>
        <a id="cancelpermission" class="btn btn-default">Cancel</a>
      </div>
    </div>
	</div>
</form>
</div>
<div id="permissions" class="col-md-12">
<a href="javascript:void(0)" id="addpermission">Add Permission</a><br><br>
<div style="display:none;" class="alert alert-info"></div>
<table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
  <thead>
    <tr>
      <th>State</th>
      <th>District</th>
      <th>Block</th>
      <th>Cluster</th>
      <th>Village</th>
      <th>School</th>
      <th>Permission Type</th>
      <th>Actions</th>
    </tr>
  </thead>
  <tbody>

<? $permission_count = 0; ?>
<?php foreach ($permissions as $e_key){ ?>
    <tr>
      <td>
        <? if ($e_key->state_id > 0) { ?>
          <? $state = $this->states_model->getById($e_key->state_id); ?>
          <? echo $state['name']; ?>
        <? } ?>
      </td>
      <td>
        <? if ($e_key->district_id > 0) { ?>
          <? $district = $this->districts_model->getById($e_key->district_id); ?>
          <? echo $district['name']; ?>
        <? } ?>
      </td>
      <td>
        <? if ($e_key->block_id > 0) { ?>
          <? $block = $this->blocks_model->getById($e_key->block_id); ?>
          <? echo $block['name']; ?>
        <? } ?>
      </td>
      <td>
        <? if ($e_key->cluster_id > 0) { ?>
          <? $cluster = $this->clusters_model->getById($e_key->cluster_id); ?>
          <? echo $cluster['name']; ?>
        <? } ?>
      </td>
      <td>
        <? if ($e_key->village_id > 0) { ?>
          <? $village = $this->villages_model->getById($e_key->village_id); ?>
          <? echo $village['name']; ?>
        <? } ?>
      </td>
      <td>
        <? if ($e_key->school_id > 0) { ?>
          <? $school = $this->schools_model->getById($e_key->school_id); ?>
          <? echo $school['name']; ?>
        <? } ?>
      </td>
      <td><?=$e_key->permission?></td>
      <td class="center">
        
        <a class="btn btn-danger" href="#" onClick="show_confirm('delete',<?php echo $e_key->id;?>)">
          <i class="glyphicon glyphicon-trash icon-white"></i>
          Delete <?php echo $e_key->id;?>
        </a>
        
      </td>
    </tr>
<?php } ?>
  </tbody>
</table>
	

<script>
$(function(){
  $("#addpermission").click(function(){
    $("#permissions").hide()
    $("#newpermission").show()
  });
  $("#cancelpermission").click(function(){
    $("#permissions").show()
    $("#newpermission").hide()
  });
  
  get_state('')
  
  <? if (isset($state_id)) { ?>
    //get_state('<?=$state_id?>')
  <? } ?>

  <? if (isset($district_id)) { ?>
    //get_district('<?=$state_id?>','<?=$district_id?>')
  <? } ?>

  <? if (isset($block_id)) { ?>
    //get_block('<?=$district_id?>','<?=$block_id?>')
  <? } ?>

  <? if (isset($cluster_id)) { ?>
    //get_cluster('<?=$block_id?>','<?=$cluster_id?>')
  <? } ?>

  <? if (isset($village_id)) { ?>
    //get_village('<?=$cluster_id?>','<?=$village_id?>')
  <? } ?>

  <? if (isset($school_id)) { ?>
    //get_school('<?=$village_id?>','<?=$school_id?>')
  <? } ?>
});
</script>
</div>
<div class="clearfix"></div>
