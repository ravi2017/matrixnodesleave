<?
if (isset($survey))
{
	extract($survey);
}
//$state_ids = array('1','2');
?>
<form method="post" action="<?php echo base_url();?>admin/survey/<?=(isset($action)) ? $action : ''?>" role="form" name="frmsurvey" id="frmsurvey">

  <div class="row">
    <div class="col-md-2"><label for="title">Title <span class="make-me-red">*</span></label><?php echo form_error('title'); ?></div>
    <div class="col-md-6">
      <div class="form-group">
        <input type="text" class="form-control required" maxlength="230" id="title" placeholder="Enter Title" name="title" value="<?=(isset($title)) ? $title : ''?>" autocomplete="off">
      </div>					
    </div>
  </div>
  
  <div class="row">
		<div class="col-md-2"><label for="state">Select State <span class="make-me-red">*</span></label></div>
    <div class="col-md-6">
      <div class="form-group state_person">
          <?
						$state_ids = (isset($state_id) ? json_decode($state_id) : array());
           foreach($states as $state) { ?>
						<input name="state_id[]" type="checkbox" value="<?=$state->name?>" <?php if(!empty($state_ids) and in_array($state->name, $state_ids)) echo "checked"; ?>><?=$state->name?>
          <? } ?>
      </div>
    </div>
  </div>
  
  <?
		$display_levels = array('state','district','block','cluster','school');
  ?>
  
  <div class="row">
		<div class="col-md-2"><label for="display_level">Display Level</label><?php echo form_error('display_level'); ?></div>
    <div class="col-md-6">
			<div class="form-group">
				 <select class="form-control select2 required" id="display_level" name="display_level">
				<?
					foreach($display_levels as $displaylevel)
					{
						?><option value="<?=$displaylevel?>" <? echo (isset($display_level) && $display_level == $displaylevel ? 'selected' : '');?>><?=ucfirst($displaylevel);?></option><?
					}
				?>
				</select>
      </div>
    </div>
  </div>
  
  <div class="row">
		<div class="col-md-2"><label for="display_level">Take Location</label><?php echo form_error('take_location'); ?></div>
    <div class="col-md-6">
			<div class="form-group">
				 <select class="form-control select2 required" id="take_location" name="take_location">
            <option value="1" <? echo (isset($take_location) && $take_location == "1" ? 'selected' : '');?>>YES</option>
            <option value="0" <? echo (isset($take_location) && $take_location == "0" ? 'selected' : '');?>>NO</option>
				</select>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">    
      <div class="form-group">
        <? if (!empty($id)) {	?>
          <input type="hidden" name="id" value="<?php echo $id; ?>" />
        <?  }	?>
        <a href="<?php echo base_url('admin/survey'); ?>" class="btn btn-sm btn-default">Cancel</a>
        <button type="submit" class="btn btn-sm btn-success">Submit</button>
      </div>
    </div>
  </div>
</form>

<script>
$(document).ready(function() {
    $("#frmsurvey").validate();
    $('.basic-multiple').select2();
});
</script>
