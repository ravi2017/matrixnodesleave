<a class="btn btn-info btn-sm pull-right" href="<?php echo base_url();?>admin/survey">Survey</a>
<a class="btn btn-success btn-sm pull-right" href="<?php echo base_url();?>admin/survey/survey_observations">Observations</a>
<br />
<br />
<form name="frmObservation" method="post" action="" class="">
  <div class="row">
    <div class="col-md-3">
      <div class="form-group">
        <select class="form-control" id="survey_id" name="survey_id">
          <option value="">Select Survey</option>
          <?php foreach($survey as $e_key) { ?>
          <option value="<?=$e_key->id?>" <? echo (isset($survey_id) && $survey_id == $e_key->id ? 'selected' : '')?>><?php echo $e_key->title?></option>
          <?php } ?>
        </select>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <input type="submit" name="submit" class="form-control btn btn-sm btn-success" value="Get Observation">
      </div>
    </div>
  </div>
</form>

<? if(!empty($observations_list)) { ?>
<div class="scrollme">  
<form name="manageSorting" id="manageSorting" method="post">
  
<table class="table table-striped table-bordered responsive">
  <input type="hidden" name="survey_id" value="<?=$survey_id?>">
  <tr>
    <th>Question Type</th>
    <th>Question</th>
    <th>Sort Order</th>
  </tr>
	<?php $i=1;
    $activity_arr = unserialize(ACTIVITY_ARRAY);
  
    foreach ($observations_list as $e_key){ 
      
  ?>
		<tr>
			<td><?php echo $e_key->question_type; ?></td>
			<td><?php echo $e_key->question; ?></td>
			<td class="action">
				<input type="text" name="sort_order[<?=$e_key->id?>][]" value="<?php echo $e_key->sort_order; ?>" size="1" maxlength="2">
			</td>
		</tr>
	<?php $i++; } ?>
</table>
<div class="col-md-2" align="right" style="padding-top:10px;">
  <input type="submit" name="submit" class="form-control btn btn-sm btn-success" value="Manage Sorting">
</div>
</form>
</div>
<?  }else{ ?>
<div class="col-md-12" align="center" style="padding-top:10px;">
  No Observation Found.
</div>  
<?  } ?>
