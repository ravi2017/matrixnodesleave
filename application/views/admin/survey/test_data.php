<? if(!isset($download)) {?>
<form name="frmactivities" id="frmactivities" method="post" action="" class="">
  <div class="row">
    <div class="col-md-2"><label for="user_name">Start Date</label></div>
    <div class="col-md-3">
      <div class="form-group">
        <input type="text" id="activitydatestart" name="activitydatestart" class="form-control required" value="<? echo (!empty($activitydatestart) ? $activitydatestart : '');?>" autocomplete="off" placeholder="From Date">
      </div>
    </div>
    <div class="col-md-2"><label for="user_name">End Date</label></div>
    <div class="col-md-3">
      <div class="form-group">
        <input type="text" id="activitydateend" name="activitydateend" class="form-control required" value="<? echo (!empty($activitydateend) ? $activitydateend : '');?>" autocomplete="off" placeholder="To Date">
      </div>
    </div>
  </div>  
  <div class="row">
		<div class="col-md-2"><label for="user_name">Survey</label></div>
    <div class="col-md-3">
      <div class="form-group state_person">
				<select name="survey_id" id="survey_id"  class="form-control required">
					<option value="">-Select Survey-</option>
				<? foreach($surveys as $survey) { ?>
					<option value="<?=$survey->id?>"><?=$survey->title?></option>
				<? } ?>
				</select>
      </div>
    </div>
    <div class="col-md-2"><label for="user_name">State</label></div>    
    <div class="col-md-3">
      <div class="form-group state_person">
				<select name="state_id" id="state_id" class="form-control">
					<option value="">-Select State-</option>
				<? foreach($states as $state) { ?>
					<option value="<?=$state->id?>"><?=$state->name?></option>
				<? } ?>
				</select>
      </div>
    </div>
   </div> 
   <div class="row"> 
    <div class="col-md-4">&nbsp;</div>
    <div class="col-md-3">
      <div class="form-group">
        <input type="submit" name="submit" class="form-control btn btn-sm btn-success" value="Get Test Data">
      </div>
    </div>
  </div>
</form>
<script>
$(document).ready(function() {
	$("#frmactivities").validate();
});
</script>
<? }else{ ?>
<table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
	<thead>
		<tr>
			<th>S.No.</th>
			<th>Survey</th>
			<th>Spark</th>
			<th>District</th>
			<th>Block</th>
			<th>Cluster</th>
			<th>School</th>
			<th>Qestion/Answer</th>
		</tr>
	</thead>
	<tbody>
	
	<?php $i=1;
    if(!empty($surveyTests)){
    foreach ($surveyTests as $e_key){ 
  ?>
		<tr>
			<td><?=$i?></td>
			<td><?php echo $survey_names[$e_key->survey_id]; ?></td>
			<td><?php echo $spark_names[$e_key->spark_id]; ?></td>
			<td><?php echo (!empty($e_key->district_id) ? $district_names[$e_key->district_id] : ''); ?></td>
			<td><?php echo (!empty($e_key->block_id) ? $block_names[$e_key->block_id] : ''); ?></td>
			<td><?php echo (!empty($e_key->cluster_id) ? $cluster_names[$e_key->cluster_id] : ''); ?></td>
			<td><?php echo (!empty($e_key->school_id) ? $school_names[$e_key->school_id] : ''); ?></td>
			<td><?php echo $answer_data[$e_key->id]; ?></td>
		</tr>
	<?php $i++; } } ?>
  </tbody>
</table>
<?	}	?>
