<a class="btn btn-success btn-sm pull-right" href="<?php echo base_url();?>admin/survey">Survey</a>
<a class="btn btn-info btn-sm pull-right" href="<?php echo base_url();?>admin/survey/add_observations">Add Observations</a>
<a class="btn btn-danger btn-sm pull-right" href="<?php echo base_url();?>admin/survey/sorting_observations">Manage Order</a>
<br><br>
<div style="display:none;" class="alert alert-info"></div>
<div class="scrollme">
<table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
	<thead>
		<tr>
			<th>S.No.</th>
			<th>Survey</th>
			<th>Question</th>
			<th>Question Type</th>
			<th data-sort=0 class="no-sort action">Status</th>
			<th data-priority="1" data-sort=0 class="no-sort action">Actions</th>
		</tr>
	</thead>
	<tbody>
	
	<?php $i=1;
    
    if(!empty($observations_list)){
    foreach ($observations_list as $e_key){ 
      
      $surveyData = get_survey_data($e_key->survey_id);
  ?>
		<tr>
			<td><?=$i?></td>
			<td><?php echo $surveyData['title']; ?></td>
			<td><?php echo $e_key->question; ?></td>
			<td><?php echo ucfirst($e_key->question_type); ?></td>
			<td>
        <?php if ($e_key->status=="1") { ?>
          <span class="label-success label">Active</span>
        <?php } else { ?>
          <span class="label-danger label">Inactive</span>
        <?php } ?>  
      </td>
			<td class="action">
				<a href="<?php echo base_url();?>admin/survey/edit_observation/<?php echo $e_key->id;?>">
					<i class="ti-pencil"></i>
				</a>
				<a href="<?php echo base_url();?>admin/survey/observationstatus/<?php echo $e_key->id;?>">
					<?php if ($e_key->status==1) { ?>
            <i class="ti-thumb-down text-danger"></i>
					<?php } else { ?>
            <i class="ti-thumb-up text-success"></i>
          <?php } ?>
				</a>
			</td>
		</tr>
	<?php $i++; } } ?>
  </tbody>
</table>
</div>
<script>
$("#allobservations").click(function(){
    $(".singleobservation").prop("checked",$(this).prop("checked"))
})
$(".singleobservation").click(function(){
    if ($(".singleobservation:checked").length == $(".singleobservation").length)
      $("#allobservations").prop("checked",true)
    else
      $("#allobservations").prop("checked",false)
})
$(".observationids").click(function(){
  d = confirm("Are you sure to disable all the selected observations. Once disabled they can't be enable again")
  if (d)
  {
    var observationids = []
    $(".singleobservation:checked").each(function(){
      observationids.push($(this).val())
    })
    $("#observationids").val(observationids)
    $("#disableobservations").submit();
  }
})
</script>
