<?
if (isset($observations))
{
	extract($observations);
}

$question_types = array('yes-no','yes-no-na','objective','subjective','multichoice');
?>
<form method="post" action="<?php echo base_url();?>admin/survey/<?=(isset($action)) ? $action : ''?>" role="form" name="frmobservation" id="frmobservation">
  <div class="row">
    <div class="col-md-8 offset-md-2">
      <div class="row">
        <label class="col-md-4" for="question">Question <span class="make-me-red">*</span></label><?php echo form_error('question'); ?>
        <div class="col-md-8">
          <div class="form-group">
            <span class="err" id="err_question"></span>
            <input type="text" class="form-control required" maxlength="230" id="question" placeholder="Enter Question" name="question" value="<?=(isset($question)) ? $question : ''?>">
          </div>					
        </div>
      </div>
      <div class="row">
        <label class="col-md-4" for="question_type">Question Type <span class="make-me-red">*</span></label><span class="err" id="err_question_type"></span>
        <div class="col-md-8">
          <div class="form-group state_person">
            <?php if ($action == "create_observation") { ?>
            <select class="form-control" id="question_type" name="question_type">
							<? foreach($question_types as $questiontype) { ?>	
              <option value="<?=$questiontype?>" <? if(isset($question_type) && $question_type == $questiontype){ echo 'selected'; }?>><?=ucwords($questiontype)?></option>
              <?	}	?>
            </select>
            <?php } else { ?>
                <?=ucwords($question_type);?>
                <input type="hidden" value="<?=$question_type?>" id="question_type" name="question_type">
            <?php } ?>
          </div>
        </div>
      </div>
      <div class="row">
        <label class="col-md-4" for="survey">Select Survey <span class="make-me-red">*</span></label>
        <div class="col-md-8">
          <div class="form-group state_person">
            <?php if ($action == "create_observation") { ?>
            <select class="form-control basic-multiple select2 required" id="survey_id" name="survey_id[]" <? if(empty($id)) { ?> multiple <? } ?> >
              <? foreach($survey_list as $survey) { ?>
								<option value="<?=$survey->id?>"><?=$survey->title?></option>
              <? } ?>
            </select>
            <?php } else { ?>
              <? foreach($survey_list as $survey) { ?>
                <?=((!isset($survey_id)) or (isset($survey_id) and $survey->id == $survey_id)) ? $survey->title : ""?>
              <? } ?>
              <input type="hidden" value="<?=$survey_id?>" id="survey_id" name="survey_id">
            <?php } ?>
          </div>
        </div>
      </div>
      
      <?php echo $this->load->view('admin/survey/options','',true) ?>
    </div>      
  </div>
  
  <div class="row">
    <div class="col-md-12">    
      <div class="form-group text-right">
        <? if (!empty($id)) {	?>
          <input type="hidden" name="id" value="<?php echo $id; ?>" />
        <?  }	?>
        <a href="<?php echo base_url('admin/survey/survey_observations'); ?>" class="btn btn-sm btn-default">Cancel</a>
        <button type="submit" class="btn btn-sm btn-success">Submit</button>
      </div>
    </div>
  </div>
</form>

<script>
$(document).ready(function() {
    $("#frmobservation").validate();
    $('.basic-multiple').select2();
});
</script>

