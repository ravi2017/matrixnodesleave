<div class="row">
  <div class="col-md-4"><label>Options</label> <a href="javascript:void(0)" class="icon-list-demo addoption"><i class="ion-plus-circled"></i></a></div>
  <div class="col-md-8">
    <div class="questionoptions">  
      <?php if (isset($observation_options)) { ?>
      <?php foreach($observation_options as $question_option) { ?>
      <div class="row options">
        <input type="hidden" value="<?php echo $question_option->id; ?>" name="optionids[]">
        <div class="form-group col-md-11"><input type="text" name="options[]" value="<?php echo $question_option->option_value; ?>" class="form-control" ></div><div class="col-md-1"><a href="javascript:void(0)" title="Disable" class="icon-list-demo disableoption"><i class="ion-thumbsdown"></i></a></div>
      </div>
      <?php } ?>
      <?php } else { ?>
      <div class="row options">
        <div class="form-group col-md-11"><input type="text" name="options[]" value="" class="form-control" ></div><div class="col-md-1"><a href="javascript:void(0)" class="icon-list-demo removeoption"><i class="ion-minus-circled"></i></a></div>
      </div>
      <div class="row options">
        <div class="form-group col-md-11"><input type="text" name="options[]" value="" class="form-control" ></div><div class="col-md-1"><a href="javascript:void(0)" class="icon-list-demo removeoption"><i class="ion-minus-circled"></i></a></div>
      </div>
      <?php } ?>
    </div>
  </div>
</div>


<script>
$(document).ready(function() {
       
    $(".questionoptions").on('click','.removeoption',function(){
      d = confirm("Are you sure to Permanently Remove the option.")
      if (d)
      {
        $(this).closest('.row').remove();
        if ($(".questionoptions").find(".options").length > 1)
        {
          $(".removeoption").show()
        }
        else
        {
          $(".removeoption").hide()
        }
      }
    })
    
    $(".addoption").click(function(){
      $(".questionoptions").append('<div class="row options"><div class="form-group col-md-11"><input type="text" name="options[]" value="" class="form-control" ></div><div class="col-md-1"><a href="javascript:void(0)" class="icon-list-demo removeoption"><i class="ion-minus-circled"></i></a></div></div>');
      if ($(".questionoptions").find(".options").length > 1)
      {
        $(".removeoption").show()
      }
      else
      {
        $(".removeoption").hide()
      }
    })
    
    $(".questionoptions").on('click','.disableoption',function(){
      d = confirm("Are you sure to Disable the option. Can't be Enable again")
      if (d)
      {
        $(this).closest('.row').remove();
        if ($(".questionoptions").find(".options").length > 1)
        {
          $(".disableoption").show()
        }
        else
        {
          $(".disableoption").hide()
        }
      }
    })
    
    $("#question_type").change(function(){
      if ($(this).val() == "objective" || $(this).val() == "multichoice")
        $(".questionoptions").closest('.row').show()
      else
        $(".questionoptions").closest('.row').hide()
    })

    if ($("#question_type").val() == "objective" || $("#question_type").val() == "multichoice")
      $(".questionoptions").closest('.row').show()
    else
      $(".questionoptions").closest('.row').hide()
});
</script>
