<?
if(isset($subjects))
{
	extract($subjects);
}
?>
<form action="<?php echo base_url();?>admin/subjects/<?=(isset($action)) ? $action : ''?>" method="post" accept-charset="utf-8" name="frmSubjects" id="frmSubjects">
  <?
  if (isset($error))
  {
    ?><div class="alert alert-danger"><?php echo $error; ?></div><?php
  }
  ?>
  <div class="row">
    <div class="col-md-6">
      <div class="form-group">
        <label for="name">Subject Name</label><?php echo form_error('name'); ?>
        <span class="err" id="err_name"></span>
        <input type="text" class="form-control required" id="name" placeholder="Enter Subject Name" name="name" value="<?=(isset($name)) ? $name : ''?>"  minlength="2" maxlength="20">
      </div>
    </div>
  </div>

	<div class="form-group">
		<?
		if (isset($id)){
      ?><input type="hidden" name="id" value="<?php echo $id; ?>" /><?
		}
		?>
		<button type="submit" class="btn btn-sm btn-success">Submit</button>
	</div>
</form>
<script>
  $(document).ready(function() {
    $("#frmSubjects").validate();
  });
</script>
