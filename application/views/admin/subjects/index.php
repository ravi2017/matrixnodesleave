<script type="text/javascript">
function show_confirm(act,gotoid)
{
if(act=="edit")
var r=confirm("Do you really want to edit?");
else
var r=confirm("Do you really want to delete?");
if (r==true)
{
window.location="<?php echo base_url();?>admin/subjects/"+act+"/"+gotoid;
}
}
</script>
<a class="btn btn-sm btn-success" href="<?php echo base_url();?>admin/subjects/add">Add Subjects</a><br><br>
<div style="display:none;" class="alert alert-info"></div>
<? if(!empty($message)) { ?><div class="alert alert-danger"><?=$message;?></div> <? } ?>
<table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
	<thead>
		<tr>
			<th>Subject Name</th>
			<th>Status</th>
			<th class="action no-sort no-search">Actions</th>
		</tr>
	</thead>
	<tbody>
	
	<?php foreach ($subjects_list as $e_key){ ?>
		<tr>
			<td><?php echo $e_key->name; ?></td>
      <td>
        <?php if (isset($e_key->status) && $e_key->status=="1") { ?>
          <span class="label-success label">Active</span>
        <?php } else { ?>
          <span class="label-danger label">Inactive</span>
        <?php } ?>  
      </td>
			<td>
        <a class="" href="<?php echo base_url();?>admin/subjects/edit/<?php echo $e_key->id;?>">
					<i class="ti-pencil"></i>
				</a>
				<a href="<?php echo base_url();?>admin/subjects/changestatus/<?php echo $e_key->id;?>">
					<?php if (isset($e_key->status) && $e_key->status==1) { ?>
            <i class="ti-thumb-down text-danger"></i>
					<?php } else { ?>
            <i class="ti-thumb-up text-success"></i>
          <?php } ?>
				</a>
			</td>
		</tr>
	<?php } ?>
  </tbody>
</table>
	
	
	
