	<script type="text/javascript">
function show_confirm(act,gotoid)
{
if(act=="edit")
var r=confirm("Do you really want to edit?");
else
var r=confirm("Do you really want to delete?");
if (r==true)
{
window.location="<?php echo base_url();?>admin/admins/"+act+"/"+gotoid;
}
}
</script>
<a href="<?php echo base_url();?>admin/admins/add">Add User</a><br><br>
<div style="display:none;" class="alert alert-info"></div>
<table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
	<thead>
		<tr>
			<th>ID</th>
			<th>Username</th>
      <th>Password</th>
      <th>Email</th>
			<th>Status</th>
			<th>Actions</th>
		</tr>
	</thead>
	<tbody>
	
	<? $admins_count = 0; ?>
	<?php foreach ($admins_list as $e_key){ ?>
		<tr>
			<td><?php echo $admins_count = $admins_count + 1; ?></td>
			<td><?php echo $e_key->username; ?></td>
      <td><?php echo $e_key->password; ?></td>
      <td><?php echo $e_key->email; ?></td>
			<td> <span class="label-success label label-default"><?if($e_key->status=="1"){?>
				Active	
				<?}else{?>
				Inactive
				
				<?}?> </span> </td>
			<td class="center">
			
				<a class="btn btn-info" href="#" onClick="show_confirm('edit',<?php echo $e_key->id;?>)">
					<i class="glyphicon glyphicon-edit icon-white"></i>
					Edit
				</a>
				<a class="btn btn-danger" href="#" onClick="show_confirm('delete',<?php echo $e_key->id;?>)">
					<i class="glyphicon glyphicon-trash icon-white"></i>
					Delete
				</a>
				<a class="btn btn-success" href="<?php echo base_url();?>admin/admins/changestatus/<?php echo $e_key->id;?>">
				<?if($e_key->status=="1"){?>
				Inactive	
				<?}else{?>
				Active
				
				<?}?>
				</a>
				</a>
				
			</td>
		</tr>
	<?php } ?>
		</tbody>
	</table>
	
	
	
