<?if (isset($code_sites))
{
	extract($code_sites);
	if (isset($id)){
		$code_id=$id;
		
		
	}
		
	else
		$m_id=0;
}
?>
<?
if (isset($admins))
{
	extract($admins);
}
?>

<form name="Formpage" action="<?php echo base_url();?>admin/admins/<?=(isset($action)) ? $action : ''?>" method="post" accept-charset="utf-8" enctype="multipart/form-data" onsubmit="return pagevalidate('<?=(isset($action)) ? $action : ''?>')" >
	
		 
	<div class="form-group">
		<label for="title">Username</label><span class="err" id="err_title"></span><?php echo form_error('username'); ?>
				
		<input type="username" class="form-control" id="username" placeholder="Enter Username" name="username" value="<?=(isset($username)) ? $username : ''?>">
	</div>
  	<div class="form-group">
		<label for="title">Password</label><span class="err" id="err_title"></span><?php echo form_error('password'); ?>
				
		<input type="title" class="form-control" id="password" placeholder="Enter Password" name="password" value="<?=(isset($password)) ? $password : ''?>">
	</div>
  <div class="form-group">
		<label for="title">Email</label><span class="err" id="err_title"></span><?php echo form_error('email'); ?>
				
		<input type="title" class="form-control" id="email" placeholder="Enter Email" name="email" value="<?=(isset($email)) ? $email : ''?>">
	</div>
 
	<div class="form-group">
    <label class="control-label" for="selectError">Select User Role </label><?php echo form_error('code'); ?>
		<span class="err" id="err_code"></span>
		<div class="controls">  
      
        <select class="form-control" name="role" id="select">
          <option value="" <?=(isset($role) and $role == "") ? 'selected' : ''?>>Select Type</option> 
          <option value="master_admin" <?=(isset($role) and $role == "master_admin") ? 'selected' : ''?>>Master admin</option>
          <option value="user" <?=(isset($role) and $role == "user") ? 'selected' : ''?>>User</option>
        
      </select>
       
         </div>
		 </div>
		 <div class="form-group">
				<label for="exampleInputFile">Image</label><?php echo form_error('image'); ?>
				<span class="err" id="err_img"></span>
				
				<input type="file" id="exampleInputFile" name="image" value="<?=(isset($image)) ? $image : ''?>">
				<? if (isset($image) and file_exists("./".$image)) { ?>
					<a href="<?=base_url().$image?>" target="_blank">View File</a>
				<?
				}
				?>
		</div>
	
	<div class="form-group">
		<?
		if (isset($id))
		{
		?>
		<input type="hidden" name="id" id="pageid" value="<?php echo $id; ?>" />
		<?
		}
		?>
		<button type="submit" class="btn btn-default pull-right">Submit</button>
		<div class="clearfix"></div>
	</div>

</form>
	
