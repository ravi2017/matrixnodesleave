<table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
	<thead>
		<tr>
			<th>State Name</th>
			<th>Login ID</th>
			<th>Name</th>
			<th>Leave Type</th>
			<th>Total Days</th>
			<th>Start Date</th>
			<th>End Date</th>
		</tr>
	</thead>
	<tbody>
	
	<?php foreach ($sparks_leave_data as $spark_id=>$data){ 
    
      if(!empty($data['leave_data'])){
        
        foreach($data['leave_data'] as $leave_data){
    ?>
		<tr>
			<td><?php echo $data['state_name']; ?></td>
			<td><?php echo $data['login_id']; ?></td>
			<td><?php echo $data['name']; ?></td>
			<td><?php echo $leave_data->leave_type; ?></td>
			<td><?php echo $leave_data->total_days; ?></td>
			<td><?php echo date('d-m-Y', strtotime($leave_data->leave_from_date)); ?></td>
			<td><?php echo date('d-m-Y', strtotime($leave_data->leave_end_date)); ?></td>
		</tr>
	<?php } 
    }
  }
  ?>
  </tbody>
</table>
