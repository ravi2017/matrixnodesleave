<?
if (isset($user))
{
	extract($user);
}
?>
<form method="post" action="<?php echo base_url();?>admin/sparks/<?=(isset($action)) ? $action : ''?>" role="form" name="frmuser" onsubmit="return validateuser()" id="frmuser">
					
	<div class="form-group">
		<label for="user_name">Login ID</label><?php echo form_error('login_id'); ?>
    <span class="err" id="err_login_id"></span>
		<input type="text" class="form-control" id="login_id" placeholder="Enter Login ID" name="login_id" value="<?=(isset($login_id)) ? $login_id : ''?>">
	</div>					
	<div class="form-group">
		<label for="user_name">Password</label><?php echo form_error('password'); ?>
    <span class="err" id="err_password"></span>
		<input type="password" class="form-control" id="password" placeholder="Enter Password" name="password" value="">
	</div>
	<div class="form-group">
		<label for="user_name">Name</label><?php echo form_error('name'); ?>
    <span class="err" id="err_name"></span>
		<input type="text" class="form-control" id="name" placeholder="Enter Name" name="name" value="<?=(isset($name)) ? $name : ''?>">
	</div>					
	<div class="form-group">
		<label for="user_name">Email</label><?php echo form_error('email'); ?>
    <span class="err" id="err_email"></span>
		<input type="text" class="form-control" id="email" placeholder="Enter EMail ID" name="email" value="<?=(isset($email)) ? $email : ''?>">
	</div>					
	<div class="form-group">
		<label for="user_name">Mobile</label><?php echo form_error('mobile'); ?>
    <span class="err" id="err_mobile"></span>
		<input type="text" class="form-control" id="mobile" placeholder="Enter Mobile" name="mobile" value="<?=(isset($mobile)) ? $mobile : ''?>">
	</div>					
  <div class="form-group">
    <label for="title">Select Role</label><span class="err" id="err_role"></span>
    <select class="form-control" id="role" name="role">
      <option value="state_person" <?=(isset($role) and $role == "state_person") ? 'selected' : ''?>>State Person</option>
      <option value="manager" <?=(isset($role) and $role == "manager") ? 'selected' : ''?>>Manager</option>
      <option value="field_user" <?=(isset($role) and $role == "field_user") ? 'selected' : ''?>>Field User</option>
      <option value="accounts" <?=(isset($role) and $role == "accounts") ? 'selected' : ''?>>Accounts</option>
      <option value="reports" <?=(isset($role) and $role == "reports") ? 'selected' : ''?>>Reports</option>
      <option value="executive" <?=(isset($role) and $role == "executive") ? 'selected' : ''?>>Executive</option>
      <option value="executive" <?=(isset($role) and $role == "HR") ? 'selected' : ''?>>HR</option>
    </select>
  </div>
  <div class="form-group">
    <label for="title">Select State</label><span class="err" id="err_title"></span>
    <select class="form-control" id="state_id" name="state_id">
      <option value="">Select State</option>
      <? foreach($states as $state) { ?>
      <option value="<?=$state->id?>" <?=(isset($state_id) and $state->id == $state_id) ? "selected" : ""?>><?=$state->name?></option>
      <? } ?>
    </select>
  </div>
  <div class="form-group field_user">
    <label for="title">Select Manager</label><span class="err" id="err_role"></span>
    <select class="form-control" id="manager_id" name="manager_id">
      <option value="">Select Manager</option>
    </select>
  </div>
  <div class="form-group field_user">
    <label for="title">Select District</label><span class="err" id="err_title"></span>
    <div class="row" id="district_id" style="padding-left: 20px;"></div>
  </div>
  <div class="form-group block_div field_user" style="display: none;">
    <label for="title">Select Block</label><span class="err" id="err_title"></span>
    <div class="row" id="block" style="padding-left: 20px;">
    </div>
  </div>
	<div class="form-group">
		<?
		if (isset($id))
		{
		?>
		<input type="hidden" name="id" value="<?php echo $id; ?>" />
		<?
		}
		?>
		<button type="submit" class="btn btn-default">Submit</button>
	</div>
</form>

<script>

$("#role").change(function(){
    if ($(this).val() == "field_user" || $(this).val() == "manager")
    {
      $(".state_person").show()
      $(".field_user").show()
    }
    else if ($(this).val() == "state_person")
    {
      $(".state_person").show()
      $(".field_user").hide()
    }
})


if($('#role').val() == 'manager' || $('#role').val() == 'field_user'){
  $(".field_user").show()
}
else if($('#role').val() == 'state_person'){
  $(".state_person").show()
  $(".field_user").hide()
}

  
function get_district_list_sampark(sid,did)
{
  var manager = $("#manager_id").val();
  var did = <?php echo $district_ids;?>;
  $.ajax({
    url: '<?=base_url()?>admin/dashboard/get_districts_new',
    type: 'get',
    data: {
      state: sid,
      manager: manager,
      curuser: '<?php echo $id; ?>'
    },
    dataType: 'json',
    success: function(response) {
      $.each(response, function() {
        if (did.indexOf(this['id']) != '-1')
          $("#district_id").append('<div class="col-md-4"><input type="checkbox" id="'+ this['id'] +'edit" checked did="' + this['id'] +'" dname="' + this['value'] + '"class="district_check" value="' + this['id'] +'"name="district_id[]" check="checked" checked>' + this['value'] + '</div>');
        else
          $("#district_id").append('<div class="col-md-4"><input type="checkbox" did="' + this['id'] +'" dname="' + this['value'] + '"class="district_check" value="' + this['id'] +'"name="district_id[]">' + this['value'] + '</div>');
      });
      $("#district_id").show();
      for(var i = 0; i < did.length; i++){
        $('.block_div').show()
        get_block_list_sampark(did[i], $("#"+did[i]+"edit").attr('dname'));
      }
    },
    error: function(response) {
      window.console.log(response);
    }
  });
}

  $("#manager_id").change(function() {
    $('#district_id').empty()
    $('#block').empty()
  get_district_list_sampark($('#state_id').val(),'')
});

  $(document).on('click', '.district_check', function(){
  $('.block_div').show()
  if ($(this).prop('checked'))
  {
    get_block_list_sampark($(this).val(), $(this).attr('dname'))
  }
  else
  {
    $('#'+$(this).attr('did')).remove()
  }
});

 $(document).on('click', '.select_all', function(){
    var id = $(this).parent().parent().attr('id');
    $( '#'+id+' input[type="checkbox"]' ).prop('checked', this.checked)
  })

function get_manager_list(sid, mid){
  $.ajax({
    url: '<?=base_url()?>admin/sparks/get_manager_list',
    type: 'get',
    data: {
      state: sid,
      manager: mid
    },
    dataType: 'json',
    success: function(response) {
      $("#manager_id").empty();
      $("#manager_id").html("<option value='0'>Select manager</option>");
      $.each(response, function() {
        if (mid == this['id'])
          $("#manager_id").append("<option value='" + this['id'] + "' selected>" + this['value'] + "</option>");
        else
          $("#manager_id").append("<option value='" + this['id'] + "'>" + this['value'] + "</option>");
      });
      //$("#manager_id").show();
      get_district_list_sampark(<?=$state_id?>,<?=$district_id?>);
    },
    error: function(response) {
      window.console.log(response);
    }
  });
}

function get_block_list_sampark(did, dname)
{
  if($("#manager").val() == "")
    var manager = '<?php echo $logged_in_user ?>';
  else
    var manager = $("#manager_id").val();
  var bid = <?php echo $block_ids;?>;
  $.ajax({
    url: '<?=base_url()?>admin/dashboard/get_blocks_new',
    type: 'get',
    data: {
      district: did,
      manager: manager,
      role: '<?php echo $user['role']; ?>',
      user_id: '<?php echo $id; ?>'
    },
    dataType: 'json',
    success: function(response) {
      if (response.length == 0)
      {
        alert("Someone already alloted for all the blocks");
        $("#did"+did).prop("checked",false);
      }else{
        if(dname != undefined){
        $('#block').append('<div class="row" id="'+did+'" style="padding-bottom:20px;"><div class="col-md-2"><b>'+dname+'</b></div><div class="col-md-10"><input type="checkbox" class="select_all"><b>Select all blocks</b></div>')
        $.each(response, function() {
          if (bid.indexOf(this['id']) != '-1')
            $("#"+did).append('<div class="col-md-4"><input type="checkbox" value="' + did + "_" + this['id'] +'"name="block_id[]" checked>' + this['value'] + '</div>');
          else
            $("#"+did).append('<div class="col-md-4"><input type="checkbox" check="unchecked" value="' + did + "_" + this['id'] +'"name="block_id[]">' + this['value'] + '</div>');
        });
        $('#'+did).append('</div><div class="clearfix"></div>')
        $("#district_id").show();
        }
      }
    },
    error: function(response) {
      window.console.log(response);
    }
  });
}

$("#state_id").change(function() {
  get_manager_list($(this).val(), '');
  //get_district_list_sampark($(this).val(),'')
});

<? if (isset($state_id) and $state_id > 0) { ?>
  //get_district_list_sampark(<?=$state_id?>,<?=$district_id?>);
  get_manager_list(<?=$state_id?>, <?=$manager_id?>);
<? } ?>

<? if (isset($manager_id) and $manager_id > 0) { ?>
  //get_manager_list(<?=$state_id?>,<?=$manager_id?>);
<? } ?>

function validateuser()
{
  if ($("#login_id").val() == "")
  {
    alert("Please enter login id")
    return false;
  }
  if ($("#name").val() == "")
  {
    alert("Please enter name")
    return false;
  }
  if ($("#email").val() == "")
  {
    //alert("Please enter email")
    //return false;
  }
  <?
  if (!isset($id))
  {
  ?>
  if ($("#password").val() == "")
  {
    alert("Please enter password")
    return false;
  }
  <?
  }
  ?>
  if ($("#role").val() == "")
  {
    alert("Please select role")
    return false;
  }
  if($(this).val() == "state_person" || $(this).val() == "accounts")
  {
    if ($("#state_id").val() == "")
    {
      alert("Please select state")
      return false;
    }
  }
  if ($("#role").val() == "field_user")
  {
    if ($("#state_id").val() == "")
    {
      alert("Please select state")
      return false;
    }
    /*if ($("#district_id").val() == "")
    {
      alert("Please select district")
      return false;
    }*/ 
  }
}

$(document).on('input', '#login_id', function(){
  var login_id = $(this).val();
  if(login_id == ''){
    $('#err_login_id').empty();
  }
  $.ajax({
    url: '<?=base_url()?>admin/sparks/check_user_id',
    type: 'post',
    data: "user_id="+login_id,
    success: function(response) {
        $('#err_login_id').empty();
        $('#err_login_id').append(response);
    },
    error: function(response) {
      window.console.log(response);
    }
  });
});
</script>
