<script type="text/javascript">
function show_confirm(act,gotoid)
{
if(act=="edit")
var r=confirm("Do you really want to edit?");
else
var r=confirm("Do you really want to delete?");
if (r==true)
{
window.location="<?php echo base_url();?>admin/sparks/"+act+"/"+gotoid;
}
}
</script>
<div style="display:none;" class="alert alert-info"></div>
<table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
	<thead>
		<tr>
			<th>Login ID</th>
			<th>Name</th>
			<th>Email</th>
			<th>Role</th>
			<th>State</th>
			<th class="no-sort no-search action">Actions</th>
		</tr>
	</thead>
	<tbody>
	
	<? $sparks_count = 0; ?>
	<?php foreach ($sparks_list as $e_key){ ?>
		<tr>
			<td><?php echo $e_key->login_id; ?></td>
			<td><?php echo $e_key->name; ?></td>
			<td><?php echo $e_key->email; ?></td>
			<td><?php echo role_name($e_key->role); ?></td>
			<td><?php echo $e_key->state_name; ?></td>
			<td class="action">
          		<button class="btn btn-info" onclick="javascript:set_user_id('<?php echo $e_key->id ?>')" >Change state</button>
			</td>
		</tr>
	<?php } ?>
		</tbody>
	</table>
	<div id="myModal" class="modal fade" role="dialog">
	  <div class="modal-dialog">
	    <!-- Modal content-->
	    <div class="modal-content">
	      <div class="modal-header">
	        <h4 class="modal-title">Change state</h4>
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	      </div>
	      <div class="modal-body">
	        <form action="<?php echo base_url()?>admin/sparks/submit_change_state" method="POST">
	        	<div class="row">
			        <div class="form-group col-md-6">
			          <label for="user_name">Select new state</label><?php echo form_error('login_id'); ?>
			          <span class="err" id="err_login_id"></span>
			          <select name="state_id" class="popupselect form-control">
			          	<option value="">Select</option>
			          	<?php foreach ($states as $state) { ?>
			          		<option value="<?php echo $state->id?>"><?php echo $state->name?></option>
			          	<?php } ?>
			          </select>
			        </div>
			        <div class="form-group col-md-6">
			          <label for="user_name">Select Month</label><?php echo form_error('login_id'); ?>
			          <span class="err" id="err_login_id"></span>
			          <input type="text" name="date" id="change_month" readonly class="form-control" placeholder="Month">
			        </div>					
			     </div>
			     <input type="hidden" value="" name="user_id" id="modal_user_id">
			     <button type="submit" class="btn btn-primary">Submit</button>
	        </form>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      </div>
	    </div>

	  </div>
	</div>

<script type="text/javascript">
	function set_user_id(user_id){
	   $('#myModal').modal('show');
	   $('#modal_user_id').val(user_id)
	}
</script>

<script>
	$(document).ready(function() {
	    $("#change_month").datepicker({
			changeMonth:true,
			changeYear:true,
			showButtonPanel: true,
            dateFormat: 'm-yy',
            onClose: function(dateText, inst) { 
                $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
            }
		});
});
</script>
<style>
    .ui-datepicker-calendar {
        display: none;
    }
</style>

