<script type="text/javascript">
function show_confirm(act,gotoid)
{
if(act=="edit")
var r=confirm("Do you really want to edit?");
else
var r=confirm("Do you really want to delete?");
if (r==true)
{
window.location="<?php echo base_url();?>admin/sparks/"+act+"/"+gotoid;
}
}
</script>
<!--<a class="btn btn-success btn-sm" href="<?php echo base_url();?>admin/sparks/add">Add New Spark</a><br><br>-->
<div style="display:none;" class="alert alert-info"></div>
<table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
	<thead>
		<tr>
			<th>Login ID</th>
			<th>Name</th>
			<th>Manager</th>
			<th>District(s)</th>
			<th>Role</th>
      <th>State</th>
			<th>Status</th>
			<th class="action no-sort no-search">Actions</th>
		</tr>
	</thead>
	<tbody>
	
	<?php foreach ($sparks_list as $e_key){ 
      
      $spark_districts = get_spark_districts($e_key->id);
    
    ?>
		<tr>
			<td><?php echo $e_key->login_id; ?></td>
			<td><?php echo $e_key->name; ?></td>
			<td><?php echo ($e_key->manager_name != '' ? $e_key->manager_name." (".$e_key->manager_login.")" : ''); ?></td>
			<td><?php echo implode(',<br>', $spark_districts); ?></td>
			<td><?php echo ucwords(role_name($e_key->role)); ?></td>
      <td><?php echo $e_key->state_name; ?></td>
      <td>
        <?php if ($e_key->status==1) { ?>
          <span class="label-success label">Active</span>
        <?php } else { ?>
          <span class="label-danger label">Inactive</span>
        <?php } ?>  
      </td>
			<td class="action">
        <input type="hidden" id="hblock_<?php echo $e_key->id;?>" value="<?php echo (!empty($e_key->home_block) ? $e_key->home_block : '');?>">
        <a href="<?php echo base_url();?>admin/sparks/edit/<?php echo $e_key->id;?>">
					<i class="ti-pencil"></i>
				</a>
				<a href="<?php echo base_url();?>admin/sparks/changestatus/<?php echo $e_key->id;?>" onclick="return confirm('Are you sure to change status?')">
					<?php if ($e_key->status==1) { ?>
            <i class="ti-thumb-down text-danger"></i>
					<?php } else { ?>
            <i class="ti-thumb-up text-success"></i>
          <?php } ?>
				</a>			
        <?php if ($e_key->status==1) { ?>
				<a href="javascript:void(0)" class="home_block" alt="<?php echo $e_key->id;?>" onclick="return set_home_click('<?php echo $e_key->id;?>')">
					<i class="ti-home <?php echo (!empty($e_key->home_block) ? 'text-default' : 'text-success');?>" alt="Home Block" title="Home Block"></i>
				</a>
        <?  } ?>
			</td>
		</tr>
	<?php } ?>
		</tbody>
	</table>


<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4>Add Spark Home Block</h4>
        <button type="button" class="close" data-dismiss="modal">×</button>
      </div>
      <form method="post" action="<?php echo base_url();?>admin/sparks/save_home_block" role="form" name="frmhomeblock" id="frmhomeblock">
        <input type="hidden" name="userid" id="userid" value="">
        <div class="modal-body">                      
          <div class="row">
            <div class="form-group col-md-12">
              <label for="brc_name">Home Block</label>
              <select id="block_id" name="block_id" class="popupselect form-control">
                <option value="">-Select Block-</option>
              </select>
            </div>
          </div>
          <div class="row" id="mvblk">  
            <div class="form-group col-md-6">
              <input type="checkbox" name="move_block" id="move_block">Move Block
            </div>
          </div>    
          <div class="row">  
            <div class="form-group col-md-3">&nbsp;</div>
            <div class="form-group col-md-6">
              <label for="brc_name">&nbsp;</label>
              <input type="submit" class="form-control" id="submit" name="submit" value="Save" class="btn btn-success">
            </div>  
            <!--<div class="form-group col-md-6">
              <label for="brc_name">&nbsp;</label>
              <input type="submit" class="form-control" id="submit" name="submit" value="Update" class="btn btn-success">
            </div>-->
          </div>  
        </div>  
      </form>
    </div>
  </div>
</div>

<script>
function set_home_click(uid)  
{
  //var uid = $(this).attr('alt');
    var bid = $("#hblock_"+uid).val();
    
    $.ajax({
      url: '<?=base_url()?>admin/sparks/get_user_blocks',
      type: 'get',
      data: {
        uid: uid
      },
      dataType: 'json',
      success: function(response) {
        $("#block_id").html("<option value=''>Select Block</option>");
        $.each(response, function() {
          if (bid == this['id'])
            $("#block_id").append("<option value='" + this['id'] + "' selected>" + this['name'] + "</option>");
          else
            $("#block_id").append("<option value='" + this['id'] + "'>" + this['name'] + "</option>");
        });
        $("#userid").val(uid);
        $("#block_id").show();
      },
      error: function(response) {
        $("#userid").val('');
        $("#block_id").html("<option value=''>Select Block</option>");
        window.console.log(response);
      }
    });
    
    $("#myModal").modal("show");
}
</script>
