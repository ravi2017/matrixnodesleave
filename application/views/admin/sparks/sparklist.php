<table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
	<thead>
		<tr>
			<th>Login ID</th>
			<th>Name</th>
			<th>Email</th>
			<th>Role</th>
      <th>State</th>
			<th>Status</th>
		</tr>
	</thead>
	<tbody>
	
	<?php foreach ($sparks_list as $e_key){ ?>
		<tr>
			<td><?php echo $e_key->login_id; ?></td>
			<td><?php echo $e_key->name; ?></td>
			<td><?php echo $e_key->email; ?></td>
			<td><?php echo ucwords(role_name($e_key->role)); ?></td>
      <td><?php echo $e_key->state_name; ?></td>
      <td>
        <?php if ($e_key->status=="1") { ?>
          Active
        <?php } else { ?>
          Inactive
        <?php } ?>  
      </td>
		</tr>
	<?php } ?>
  </tbody>
</table>
