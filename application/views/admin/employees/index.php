<script type="text/javascript">
function show_confirm(act,gotoid)
{
if(act=="edit")
var r=confirm("Do you really want to edit?");
else
var r=confirm("Do you really want to delete?");
if (r==true)
{
window.location="<?php echo base_url();?>admin/employees/"+act+"/"+gotoid;
}
}
</script>
<div class="row">
<div class="col-md-3">
<a class="btn btn-success btn-sm" href="<?php echo base_url();?>admin/employees/add">Add New Employee</a><br><br>
</div>
</div>
  
<div style="display:none;" class="alert alert-info"></div>
<table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
	<thead>
		<tr>
			<th>Login ID</th>
			<th>Name</th>
			<th>Role</th>
			<th>Group</th>
      <th>State</th>
			<th>Status</th>
			<th class="action no-sort no-search">Actions</th>
		</tr>
	</thead>
	<tbody>
	<?php foreach ($employees_list as $e_key){ 
      
      $state_name = 'N/A';
			if(!empty($e_key->state_id)){
				$state_info = get_state_info($e_key->state_id);
				$state_name = $state_info[0]->name;
			}
    ?>
		<tr>
			<td><?php echo $e_key->login_id; ?></td>
			<td><?php echo $e_key->name; ?></td>
			<td><?php echo ucwords(role_name($e_key->role)); ?></td>
			<td><?php echo (isset($USER_GROUPS[$e_key->user_group]) ? $USER_GROUPS[$e_key->user_group] : ''); ?></td>
      <td><?php echo $state_name; ?></td>
      <td>
        <?php if ($e_key->status==1) { ?>
          <span class="label-success label">Active</span>
        <?php } else { ?>
          <span class="label-danger label">Inactive</span>
        <?php } ?>  
      </td>
			<td class="action">
        <a href="<?php echo base_url();?>admin/employees/edit/<?php echo $e_key->id;?>">
					<i class="ti-pencil text-success" alt="Update Details" title="Update Details"></i>
				</a>
        <a href="<?php echo base_url();?>admin/employees/reset_password/<?php echo $e_key->id;?>">
					<i class="ti-key text-danger" alt="Reset Password" title="Reset Password"></i>
				</a>
				<a href="<?php echo base_url();?>admin/employees/changestatus/<?php echo $e_key->id;?>" onclick="return confirm('Are you sure to change status?')">
					<?php if ($e_key->status==1) { ?>
            <i class="ti-thumb-down text-danger" alt="Change Status" title="Change Status"></i>
					<?php } else { ?>
            <i class="ti-thumb-up text-success" alt="Change Status" title="Change Status"></i>
          <?php } ?>
				</a>			
        <a href="<?php echo base_url();?>admin/employees/employee_address/<?=$e_key->id?>">
					<i class="ti-home text-success" alt="Employee Address" title="Employee Address"></i>
				</a>
				<a href="<?php echo base_url();?>admin/employees/bank_details/<?=$e_key->id?>">
					<i class="ti-money text-success" alt="Bank Details" title="Bank Details"></i>
				</a>
				<a href="<?php echo base_url();?>admin/employees/employee_salary_details/<?=$e_key->id?>">
					<i class="ti-wallet text-success" alt="Salary Details" title="Salary Details"></i>
				</a>
				<a href="<?php echo base_url();?>admin/employees/employee_exit_details/<?=$e_key->id?>">
					<i class="ti-eraser text-success" alt="Exit Details" title="Exit Details"></i>
				</a>
			</td>
		</tr>
	<?php } ?>
		</tbody>
	</table>
	

