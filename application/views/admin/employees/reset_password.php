<form method="post" action="" role="form" name="frmuser" id="frmuser" class="form-horizontal">
  <div class="row">
		<div class="col-sm-12">
		  <div class="form-group row">
			<label for="new_password" class="col-sm-2 control-label">New Password</label>
			<div class="col-sm-4">
			  <input type="password" class="form-control required" id="new_pwd" placeholder="Enter New Password" name="new_pwd" value="" autocomplete="off">
			</div>					
		  </div>					
		</div>
  </div>			
  
  <div class="row">
		<div class="col-sm-6">
			<div class="form-group text-center">
				<input type="hidden" name="spark_id" value="<?php echo $spark_id; ?>" />
				<input type="submit" name="submit" class="btn btn-info" value="Save">
				&nbsp;<a href="<?php echo base_url();?>admin/employees/" class="btn btn-default">Cancel</a>
			</div>
		</div>
	</div>
</form>

<script>
$(document).ready(function() {
   $("#frmuser").validate();
});
</script>
