<?
if (!empty($salary_data))
{
	//print_r($salary_data);
	$salary_data = (array) $salary_data[0];
	extract($salary_data);
}
?>
<style>
@media (min-width: 768px)
.form-horizontal .control-label {
    padding-top: 7px;
    margin-bottom: 0;
    text-align: right;
}
</style>
<form method="post" action="" role="form" name="frmemployee" id="frmemployee" class="form-horizontal">
	
	<div class="form-group row">
		<label for="joining_date" class="col-sm-3 control-label">Employee Code</label>
		<div class="col-sm-3">
			<div class="input-group date">
				<input type="text" autocomplete="off" readonly class="form-control" id="login_id" value="<?=(!empty($emp_details)) ? $emp_details['login_id'] : '0'?>">
			</div>          
		</div>						
		
		<label for="joining_date" class="col-sm-3 control-label">Employee Name</label>
		<div class="col-sm-3">
			<div class="input-group date">
				<input type="text" autocomplete="off" readonly class="form-control" id="emp_name" value="<?=(!empty($emp_details)) ? $emp_details['name'] : '0'?>">
			</div>          
		</div>						
	</div>
	
	<div class="form-group row">
		<label for="effective_date" class="col-sm-3 control-label">Effective Date</label>
		<div class="col-sm-3">
			<div class="input-group date">
				<div class="input-group-addon">
					<i class="ti ti-calendar"></i>
				</div>
				<input type="text" autocomplete="off" class="form-control pull-right required" id="effective_date" placeholder="dd-mm-yyyy" name="effective_date" value="<?=(isset($effective_date)) ? date('d-m-Y', strtotime($effective_date)) : '0'?>">
			</div>          
		</div>						
	</div>
	
  <div class="row salary_table">

    <div class="col-sm-4">
      <div class="form-group row">
        <label for="basic_salary" class="col-sm-8 control-label">Basic Salary</label>
        <div class="col-sm-4">
					<input type="text" autocomplete="off" class="form-control1 net_earn allownumericwithdecimal" size="4" name="basic_salary" id="basic_salary" maxlength="10" value="<?=(isset($basic_salary)) ? $basic_salary : '0'?>">
        </div>
      </div>	
      <div class="form-group row">
        <label for="hra" class="col-sm-8 control-label">HRA</label>
        <div class="col-sm-4">
					<input type="text" autocomplete="off" class="form-control1 net_earn allownumericwithdecimal" size="4" name="hra" id="hra" maxlength="10" value="<?=(isset($hra)) ? $hra : '0'?>">
        </div>
      </div>	
      <div class="form-group row">
        <label for="conveyance_allowance" class="col-sm-8 control-label">Conveyance Allowance</label>
        <div class="col-sm-4">
					<input type="text" autocomplete="off" class="form-control1 net_earn allownumericwithdecimal" size="4" name="conveyance_allowance" id="conveyance_allowance" maxlength="10" value="<?=(isset($conveyance_allowance)) ? $conveyance_allowance : '0'?>">
        </div>
      </div>	
      <div class="form-group row">
        <label for="special_allowance" class="col-sm-8 control-label">Special Allowance</label>
        <div class="col-sm-4">
					<input type="text" autocomplete="off" class="form-control1 net_earn allownumericwithdecimal" size="4" name="special_allowance" id="special_allowance" maxlength="10" value="<?=(isset($special_allowance)) ? $special_allowance : '0'?>">
        </div>
      </div>	
      <div class="form-group row">
        <label for="children_education_allowance" class="col-sm-8 control-label">Children Education Allowance</label>
        <div class="col-sm-4">
					<input type="text" autocomplete="off" class="form-control1 net_earn allownumericwithdecimal" size="4" name="children_education_allowance" id="children_education_allowance" maxlength="10" value="<?=(isset($children_education_allowance)) ? $children_education_allowance : '0'?>">
        </div>
      </div>	
      <div class="form-group row">
        <label for="children_education_hostel" class="col-sm-8 control-label">Children Education Hostel</label>
        <div class="col-sm-4">
					<input type="text" autocomplete="off" class="form-control1 net_earn allownumericwithdecimal" size="4" name="children_education_hostel" id="children_education_hostel" maxlength="10" value="<?=(isset($children_education_hostel)) ? $children_education_hostel : '0'?>">
        </div>
      </div>	
      <div class="form-group row">
        <label for="medical_allowance" class="col-sm-8 control-label">Medical Allowance</label>
        <div class="col-sm-4">
					<input type="text" autocomplete="off" class="form-control1 net_earn allownumericwithdecimal" size="4" name="medical_allowance" id="medical_allowance" maxlength="10" value="<?=(isset($medical_allowance)) ? $medical_allowance : '0'?>">
        </div>
      </div>	
      <div class="form-group row">
        <label for="bike_allowance" class="col-sm-8 control-label">Bike Allowance</label>
        <div class="col-sm-4">
					<input type="text" autocomplete="off" class="form-control1 net_earn allownumericwithdecimal" size="4" name="bike_allowance" id="bike_allowance" maxlength="10" value="<?=(isset($bike_allowance)) ? $bike_allowance : '0'?>">
        </div>
      </div>	
      <div class="form-group row">
        <label for="consultancy_charges" class="col-sm-8 control-label">Consultancy Charges</label>
        <div class="col-sm-4">
					<input type="text" autocomplete="off" class="form-control1 net_earn allownumericwithdecimal" size="4" name="consultancy_charges" id="consultancy_charges" maxlength="10" value="<?=(isset($consultancy_charges)) ? $consultancy_charges : '0'?>">
        </div>
      </div>	
      <div class="form-group row">
        <label for="ampb" class="col-sm-8 control-label">AMPB</label>
        <div class="col-sm-4">
					<input type="text" autocomplete="off" class="form-control1 net_earn allownumericwithdecimal" size="4" name="ampb" id="ampb" maxlength="10" value="<?=(isset($ampb)) ? $ampb : '0'?>">
        </div>
      </div>	
      <div class="form-group row">
        <label for="performance_incentive" class="col-sm-8 control-label">Performance Incentive</label>
        <div class="col-sm-4">
					<input type="text" autocomplete="off" class="form-control1 net_earn allownumericwithdecimal" size="4" name="performance_incentive" id="performance_incentive" maxlength="10" value="<?=(isset($performance_incentive)) ? $performance_incentive : '0'?>">
        </div>
      </div>	
      <div class="form-group row">
        <label for="pf_allowance" class="col-sm-8 control-label">PF Allowance</label>
        <div class="col-sm-4">
					<input type="text" autocomplete="off" class="form-control1 net_earn allownumericwithdecimal" size="4" name="pf_allowance" id="pf_allowance" maxlength="10" value="<?=(isset($pf_allowance)) ? $pf_allowance : '0'?>">
        </div>
      </div>	
    </div>
		
		<div class="col-sm-4 mycontent-left">
      <div class="form-group row">
        <label for="esic_employer_deductior" class="col-sm-8 control-label">ESIC Employer Deductior</label>
        <div class="col-sm-4">
					<input type="text" autocomplete="off" class="form-control1 col_2 allownumericwithdecimal" size="4" name="esic_employer_deductior" id="esic_employer_deductior" maxlength="10" value="<?=(isset($esic_employer_deductior)) ? $esic_employer_deductior : '0'?>">
        </div>
      </div>	
      <div class="form-group row">
        <label for="provident_fund" class="col-sm-8 control-label">Provident Fund</label>
        <div class="col-sm-4">
					<input type="text" autocomplete="off" class="form-control1 col_2 allownumericwithdecimal" size="4" name="provident_fund" id="provident_fund" maxlength="10" value="<?=(isset($provident_fund)) ? $provident_fund : '0'?>">
        </div>
      </div>	
      <div class="form-group row">
        <label for="esic" class="col-sm-8 control-label">ESIC</label>
        <div class="col-sm-4">
					<input type="text" autocomplete="off" class="form-control1 col_2 allownumericwithdecimal" size="4" name="esic" id="esic" maxlength="10" value="<?=(isset($esic)) ? $esic : '0'?>">
        </div>
      </div>	
      <div class="form-group row">
        <label for="profession_tax" class="col-sm-8 control-label">Profession Tax</label>
        <div class="col-sm-4">
					<input type="text" autocomplete="off" class="form-control1 col_2 allownumericwithdecimal" size="4" name="profession_tax" id="profession_tax" maxlength="10" value="<?=(isset($profession_tax)) ? $profession_tax : '0'?>">
        </div>
      </div>	
      <div class="form-group row">
        <label for="tds" class="col-sm-8 control-label">TDS</label>
        <div class="col-sm-4">
					<input type="text" autocomplete="off" class="form-control1 col_2 allownumericwithdecimal" size="4" name="tds" id="tds" maxlength="10" value="<?=(isset($tds)) ? $tds : '0'?>">
        </div>
      </div>	
      <div class="form-group row">
        <label for="laptop_advance" class="col-sm-8 control-label">Laptop Advance</label>
        <div class="col-sm-4">
					<input type="text" autocomplete="off" class="form-control1 col_2 allownumericwithdecimal" size="4" name="laptop_advance" id="laptop_advance" maxlength="10" value="<?=(isset($laptop_advance)) ? $laptop_advance : '0'?>">
        </div>
      </div>	
      <div class="form-group row">
        <label for="telephone_advance" class="col-sm-8 control-label">Telephone Advance</label>
        <div class="col-sm-4">
					<input type="text" autocomplete="off" class="form-control1 col_2 allownumericwithdecimal" size="4" name="telephone_advance" id="telephone_advance" maxlength="10" value="<?=(isset($telephone_advance)) ? $telephone_advance : '0'?>">
        </div>
      </div>	
      <div class="form-group row">
        <label for="other_advance" class="col-sm-8 control-label">Other Advance</label>
        <div class="col-sm-4">
					<input type="text" autocomplete="off" class="form-control1 col_2 allownumericwithdecimal" size="4" name="other_advance" id="other_advance" maxlength="10" value="<?=(isset($other_advance)) ? $other_advance : '0'?>">
        </div>
      </div>	
      <div class="form-group row">
        <label for="tds_profession_fee" class="col-sm-8 control-label">TDS Profession_fee</label>
        <div class="col-sm-4">
					<input type="text" autocomplete="off" class="form-control1 col_2 allownumericwithdecimal" size="4" name="tds_profession_fee" id="tds_profession_fee" maxlength="10" value="<?=(isset($tds_profession_fee)) ? $tds_profession_fee : '0'?>">
        </div>
      </div>	
      <div class="form-group row">
        <label for="non_emplyoee_dedution" class="col-sm-8 control-label">Non Emplyoee Dedution</label>
        <div class="col-sm-4">
					<input type="text" autocomplete="off" class="form-control1 col_2 allownumericwithdecimal" size="4" name="non_emplyoee_dedution" id="non_emplyoee_dedution" maxlength="10" value="<?=(isset($non_emplyoee_dedution)) ? $non_emplyoee_dedution : '0'?>">
        </div>
      </div>	
      
    </div> 
		
		<div class="col-sm-4 mycontent-left">
      <div class="form-group row">
        <label for="pf_control" class="col-sm-8 control-label">PF Control</label>
        <div class="col-sm-4">
					<input type="text" autocomplete="off" class="form-control1 col_2 allownumericwithdecimal" size="4" name="pf_control" id="pf_control" maxlength="10" value="<?=(isset($pf_control)) ? $pf_control : '0'?>">
        </div>
      </div>	
      <div class="form-group row">
        <label for="telephone_peimbusemer" class="col-sm-8 control-label">Telephone Peimbusemer</label>
        <div class="col-sm-4">
					<input type="text" autocomplete="off" class="form-control1 col_2 allownumericwithdecimal" size="4" name="telephone_peimbusemer" id="telephone_peimbusemer" maxlength="10" value="<?=(isset($telephone_peimbusemer)) ? $telephone_peimbusemer : '0'?>">
        </div>
      </div>	
      <div class="form-group row">
        <label for="car_running_amount" class="col-sm-8 control-label">Car Running Amount</label>
        <div class="col-sm-4">
					<input type="text" autocomplete="off" class="form-control1 col_2 allownumericwithdecimal" size="4" name="car_running_amount" id="car_running_amount" maxlength="10" value="<?=(isset($car_running_amount)) ? $car_running_amount : '0'?>">
        </div>
      </div>	
      <div class="form-group row">
        <label for="car_lease_amount" class="col-sm-8 control-label">Car Lease Amount</label>
        <div class="col-sm-4">
					<input type="text" autocomplete="off" class="form-control1 col_2 allownumericwithdecimal" size="4" name="car_lease_amount" id="car_lease_amount" maxlength="10" value="<?=(isset($car_lease_amount)) ? $car_lease_amount : '0'?>">
        </div>
      </div>	
      <div class="form-group row">
        <label for="air_ticket_amount" class="col-sm-8 control-label">Air Ticket Amount</label>
        <div class="col-sm-4">
					<input type="text" autocomplete="off" class="form-control1 col_2 allownumericwithdecimal" size="4" name="air_ticket_amount" id="air_ticket_amount" maxlength="10" value="<?=(isset($air_ticket_amount)) ? $air_ticket_amount : '0'?>">
        </div>
      </div>	
      <div class="form-group row">
        <label for="flexi_benefit_amount" class="col-sm-8 control-label">Flexi Benefit Amount</label>
        <div class="col-sm-4">
					<input type="text" autocomplete="off" class="form-control1 col_2 allownumericwithdecimal" size="4" name="flexi_benefit_amount" id="flexi_benefit_amount" maxlength="10" value="<?=(isset($flexi_benefit_amount)) ? $flexi_benefit_amount : '0'?>">
        </div>
      </div>	
      <div class="form-group row">
        <label for="pf_wages" class="col-sm-8 control-label">PF Wages</label>
        <div class="col-sm-4">
					<input type="text" autocomplete="off" class="form-control1 col_2 allownumericwithdecimal" size="4" name="pf_wages" id="pf_wages" maxlength="10" value="<?=(isset($pf_wages)) ? $pf_wages : '0'?>">
        </div>
      </div>	
      <div class="form-group row">
        <label for="ampi_entitlement" class="col-sm-8 control-label">AMPI Entitlement</label>
        <div class="col-sm-4">
					<input type="text" autocomplete="off" class="form-control1 col_2 allownumericwithdecimal" size="4" name="ampi_entitlement" id="ampi_entitlement" maxlength="10" value="<?=(isset($ampi_entitlement)) ? $ampi_entitlement : '0'?>">
        </div>
      </div>	
      <div class="form-group row">
        <label for="pension_wage" class="col-sm-8 control-label">Pension Wage</label>
        <div class="col-sm-4">
					<input type="text" autocomplete="off" class="form-control1 col_2 allownumericwithdecimal" size="4" name="pension_wage" id="pension_wage" maxlength="10" value="<?=(isset($pension_wage)) ? $pension_wage : '0'?>">
        </div>
      </div>	
      <div class="form-group row">
        <label for="esic_wage_for_checking" class="col-sm-8 control-label">ESIC Wage for Checking</label>
        <div class="col-sm-4">
					<input type="text" autocomplete="off" class="form-control1 col_2 allownumericwithdecimal" size="4" name="esic_wage_for_checking" id="esic_wage_for_checking" maxlength="10" value="<?=(isset($esic_wage_for_checking)) ? $esic_wage_for_checking : '0'?>">
        </div>
      </div>	
      <div class="form-group row">
        <label for="eps_employer" class="col-sm-8 control-label">EPS Employer</label>
        <div class="col-sm-4">
					<input type="text" autocomplete="off" class="form-control1 col_2 allownumericwithdecimal" size="4" name="eps_employer" id="eps_employer" maxlength="10" value="<?=(isset($eps_employer)) ? $eps_employer : '0'?>">
        </div>
      </div>	
      <div class="form-group row">
        <label for="total_monthly" class="col-sm-8 control-label">Total Monthly</label>
        <div class="col-sm-4">
					<input type="text" autocomplete="off" class="form-control1 col_2 allownumericwithdecimal" size="4" name="total_monthly" id="total_monthly" maxlength="10" value="<?=(isset($total_monthly)) ? $total_monthly : '0'?>">
        </div>
      </div>	
      
    </div> 
  </div>
  
  <hr>
  
  <div class="form-group row">
		<div class="col-sm-4">
			<div class="form-group row">
				<label for="total_earning" class="col-sm-8 control-label">Total Earning</label>
				<div class="col-sm-4">
					<div class="input-group">
						<input type="text" autocomplete="off" size=4 class="form-control1" id="total_earning" name="total_earning" value="<?=(isset($total_earning) ? $total_earning : '0')?>">
					</div>          
				</div>						
			</div>
		</div>
		<div class="col-sm-4 mycontent-left">
			<div class="form-group row">
				<label for="total_deduction" class="col-sm-8 control-label">Total Deduction</label>
				<div class="col-sm-4">
					<div class="input-group">
						<input type="text" autocomplete="off" size=4 class="form-control1" id="total_deduction" name="total_deduction" value="<?=(isset($total_deduction) ? $total_deduction : '0')?>">
					</div>          
				</div>						
			</div>						
			<div class="form-group row">
				<label for="net_pay" class="col-sm-8 control-label">Net Pay</label>
				<div class="col-sm-4">
					<div class="input-group">
						<input type="text" autocomplete="off" size=4 class="form-control1" id="net_pay" name="net_pay" value="<?=(isset($net_pay) ? $net_pay : '0')?>">
					</div>          
				</div>						
			</div>						
		</div>						
	</div>
  
  <div class="form-group">
		<input type="hidden" name="spark_id" value="<?php echo $spark_id; ?>" />
		<input type="submit" class="btn btn-info" name="submit" value="Save">
		&nbsp;<a href="<?php echo base_url();?>admin/employees/" class="btn btn-default">Cancel</a>
	</div>
</form>

<script>

function get_net_earning(){	
	var sum = 0;
	var total_earn = $("#total_earning").val();
	var total_deduction = $("#total_deduction").val();
	$('.net_earn').each(function(){
			sum += parseFloat($(this).val());  // Or this.innerHTML, this.innerText
	});	
	$("#total_earning").val(sum);
	var net_pay = parseFloat(sum) - parseFloat(total_deduction);
	$("#net_pay").val(net_pay);
}

function get_net_deduction(){	
	var sum = 0;
	var total_earn = $("#total_earning").val();
	var total_deduction = $("#total_deduction").val();
	$('.col_2').each(function(){
			sum += parseFloat($(this).val());  // Or this.innerHTML, this.innerText
	});	
	$("#total_deduction").val(sum);
	var net_pay = parseFloat(total_earn) - parseFloat(sum);
	$("#net_pay").val(net_pay);
}


$(".allownumericwithdecimal").on("keypress keyup blur",function (event) {
            //this.value = this.value.replace(/[^0-9\.]/g,'');
		$(this).val($(this).val().replace(/[^0-9\.]/g,''));
			if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
					event.preventDefault();
			}
});

$(document).ready(function() {
	get_net_earning();
	get_net_deduction();
});

$(".net_earn").change(function() {
		get_net_earning();
});

$(".col_2").change(function() {
		get_net_deduction();
});

$("#effective_date" ).datepicker({
	changeYear:true,
	dateFormat: "dd-mm-yy"
});
</script>
<style>
.mycontent-left {
  border-left: 1px solid #E6E6E6;
}

.salary_table label {
    font-size: 13px;
    font-family: "Open Sans", sans-serif;
}
</style>
