<? if(!isset($download) && !isset($pdf)) {?>
<form id="frmattendance" name="frmattendance" action="" method="post">
	<div class="row">
		<div class="form-group col-md-3">
		  <div class="form-group">
				<input type="text" id="attendancestart" name="attendancestart" class="form-control required" value="<?=date('d-m-Y', strtotime($attendancestart));?>">
		  </div>
    </div>
    <div class="form-group col-md-3">
		  <div class="form-group">
				<input type="text" id="attendanceend" name="attendanceend" class="form-control required" value="<?=date('d-m-Y', strtotime($attendanceend));?>">
		  </div>
		</div>
		<div class="form-group col-md-3">
		  <div class="form-group">
				<select name="division" id="division">
					<option value="">All</option>
					<? foreach($STATES_ARR as $key=>$value){ ?>
						<option value="<?=$key?>" <? echo (isset($state_id) && $state_id == $key ? 'selected' : '') ?>><?=$value?></option>
					<?	}	?>	
				</select>
		  </div>
		</div>
	  <div class="form-group col-md-3">
        <input type="submit" name="submit" value="Get List" class="btn btn-sm btn-info">
	  </div>
  </div>
</form>

	<? if(!empty($employees_list)) { ?>
	<div class="row">
		<div class="form-group col-md-3">
			<form name="frmreport" action="<?php echo base_url();?>admin/employees/employee_attendance" method="post">
				<input type="hidden" name="attendancestart" value="<?=$attendancestart?>">
				<input type="hidden" name="attendanceend" value="<?=$attendanceend?>">
				<input type="hidden" name="download" value="1">
				<input type="hidden" name="division" value="<?=$state_id?>">
				<button type="submit" class="btn btn-success">Download Report</button>
			</form>
		</div>
		<!--<div class="form-group col-md-3">
			<form name="frmreport" action="<?php echo base_url();?>admin/employees/employee_attendance" method="post">
				<input type="hidden" name="attendanceend" value="<?=$attendanceend?>">
				<input type="hidden" name="pdf" value="1">
				<button type="submit" class="btn btn-success">Download PDF</button>
			</form>
		</div>-->
	</div>
	<?	} ?>
<?	} ?>

<div class="scrollme">
<table class="table table-striped table-bordered bootstrap-datatable datatable1 responsive nowrap" id="withscrolltable">
	<thead>
		<tr style="background-color:#D5ECF9;">
			<th>Employee Code</th>
			<th>Employee Name</th>
			<th>Date of Joining</th>
			<th>Date of Exit</th>
      <th>Manager Code</th>
			<th>Manager Name</th>
			<th>State</th>
			<!--<th>Division</th>-->
			<th>Unit</th>
			<? foreach($rangeDates as $dates) { ?>
				<th><?=date('M d (D)', strtotime($dates));?></th>
			<?	}	?>	
			<th>PST</th>
			<th>FPCAuto</th>
			<th>ABS</th>
			<th>ABSHD</th>
			<th>LWP</th>
			<th>SL</th>
			<th>EL</th>
			<th>COL</th>
			<th>ML</th>
			<th>PL</th>
			<th>BH</th>
			<th>WEO</th>
			<th>PHY</th>
			<? if(!isset($download) && !isset($pdf)) {?>
				<th>Total Present Days<br />+ Leaves+WEO+PHY</th>
				<th>Total Absent<br />+LWP<br />+Absent By Joining<br />+Absent By Exit</th>
				<th>Total<br /> Days</th>
				<th>Total<br /> Payable<br />Days</th>
				<th>Total<br /> Count</th>
				<th>Total week off<br />+holiday</th>
			<? }else{ ?>
					<th>Total Present Days+Leaves+WEO+PHY</th>
				<th>Total Absent +LWP+Absent By Joining+Absent By Exit</th>
				<th>Total Days</th>
				<th>Total Payable Days</th>
				<th>Total Count</th>
				<th>Total week off+holiday</th>
			<?	}	?>	
		</tr>
	</thead>
	<tbody>
	<?php 
		
	foreach ($employees_list as $e_key){ 
    $PST[$e_key->login_id] = 0;
		$FPCAuto[$e_key->login_id] = 0;
		$ABS[$e_key->login_id] = 0;
		$ABSHD[$e_key->login_id] = 0;
		$LWP[$e_key->login_id] = 0;
		$SL[$e_key->login_id] = 0;
		$EL[$e_key->login_id] = 0;
		$COL[$e_key->login_id] = 0;
		$ML[$e_key->login_id] = 0;
		$PL[$e_key->login_id] = 0;
		$BH[$e_key->login_id] = 0;
		$WEO[$e_key->login_id] = 0;
		$PHY[$e_key->login_id] = 0;  
		
		$total_present[$e_key->login_id] = 0;  
		$total_absent[$e_key->login_id] = 0;  
		$total_days[$e_key->login_id] = 0;  
		$total_payble[$e_key->login_id] = 0;  
		$total_count[$e_key->login_id] = 0;  
		$total_off[$e_key->login_id] = 0;  
    ?>
		<tr>
			<td><?php echo $e_key->login_id; ?></td>
			<td><?php echo $e_key->name; ?></td>
			<td><?php echo $e_key->joining_date; ?></td>
			<td><?php echo $e_key->exit_date; ?></td>
			<td><?php echo $e_key->manager_login; ?></td>
			<td><?php echo $e_key->manager_name; ?></td>
			<td><?php echo $STATES_ARR[$e_key->state_id]; ?></td>
			<!--<td><?php echo $e_key->division; ?></td>-->
			<td><?php echo $e_key->unit_name; ?></td>
			<? 
			foreach($rangeDates as $dates) { 
				
				$mdate = date('Y-m-d', strtotime($dates));
				
				$day_value = 'ABS';
				if(isset($activity_arr[$mdate][$e_key->login_id])){
					$day_value = $activity_arr[$mdate][$e_key->login_id];
				}
				else{
					if($e_key->user_group == 'field_user' && isset($fOffDays[$mdate]))
					{
						$day_value = 'WEO';
					}
					else if($e_key->user_group == 'head_office' && isset($hOffDays[$mdate]))
					{
						$day_value = 'WEO';
					}
					else if($e_key->user_group == 'field_user' && isset($field_holiday[$e_key->state_id][$mdate]))
					{
						$day_value = 'PHY';
					}
					else if($e_key->user_group == 'head_office' && isset($HO_holiday[$e_key->state_id][$mdate]))
					{
						$day_value = 'PHY';
					}
				}
				$PST[$e_key->login_id] = ($day_value == 'PST' ? $PST[$e_key->login_id]+1 : $PST[$e_key->login_id]);
				$FPCAuto[$e_key->login_id] = ($day_value == 'FPCAuto' ? $FPCAuto[$e_key->login_id]+1 : $FPCAuto[$e_key->login_id]);
				$ABS[$e_key->login_id] = ($day_value == 'ABS' ? $ABS[$e_key->login_id]+1 : $ABS[$e_key->login_id]);
				$ABSHD[$e_key->login_id] = ($day_value == 'ABSHD' ? $ABSHD[$e_key->login_id]+1 : $ABSHD[$e_key->login_id]);
				$LWP[$e_key->login_id] = ($day_value == 'LWP' ? $LWP[$e_key->login_id]+1 : $LWP[$e_key->login_id]);
				$SL[$e_key->login_id] = ($day_value == 'SL' ? $SL[$e_key->login_id]+1 : $SL[$e_key->login_id]);
				$EL[$e_key->login_id] = ($day_value == 'EL' ? $EL[$e_key->login_id]+1 : $EL[$e_key->login_id]);
				$COL[$e_key->login_id] = ($day_value == 'COL' ? $COL[$e_key->login_id]+1 : $COL[$e_key->login_id]);
				$ML[$e_key->login_id] = ($day_value == 'ML' ? $ML[$e_key->login_id]+1 : $ML[$e_key->login_id]);
				$PL[$e_key->login_id] = ($day_value == 'PL' ? $PL[$e_key->login_id]+1 : $PL[$e_key->login_id]);
				$BH[$e_key->login_id] = ($day_value == 'BH' ? $BH[$e_key->login_id]+1 : $BH[$e_key->login_id]);
				$WEO[$e_key->login_id] = ($day_value == 'WEO' ? $WEO[$e_key->login_id]+1 : $WEO[$e_key->login_id]);
				$PHY[$e_key->login_id] = ($day_value == 'PHY' ? $PHY[$e_key->login_id]+1 : $PHY[$e_key->login_id]);
				
				$total_present[$e_key->login_id] = $PST[$e_key->login_id] + $WEO[$e_key->login_id] + $PHY[$e_key->login_id] + $SL[$e_key->login_id] + $EL[$e_key->login_id] + $COL[$e_key->login_id] + $ML[$e_key->login_id] + $PL[$e_key->login_id] + $BH[$e_key->login_id];  
				$total_absent[$e_key->login_id] = $ABS[$e_key->login_id] + $LWP[$e_key->login_id];  
				$total_days[$e_key->login_id] = $total_days[$e_key->login_id] + 1;  
				$total_payble[$e_key->login_id] = $total_days[$e_key->login_id]-$total_absent[$e_key->login_id];  
				$total_count[$e_key->login_id] = 0;  
				$total_off[$e_key->login_id] = $WEO[$e_key->login_id] + $PHY[$e_key->login_id];  
				?>
				<td><? echo $day_value;?></td>
			<?	}	?>	
			<td><?=$PST[$e_key->login_id]?></td>
			<td><?=$FPCAuto[$e_key->login_id]?></td>
			<td><?=$ABS[$e_key->login_id]?></td>
			<td><?=$ABSHD[$e_key->login_id]?></td>
			<td><?=$LWP[$e_key->login_id]?></td>
			<td><?=$SL[$e_key->login_id]?></td>
			<td><?=$EL[$e_key->login_id]?></td>
			<td><?=$COL[$e_key->login_id]?></td>
			<td><?=$ML[$e_key->login_id]?></td>
			<td><?=$PL[$e_key->login_id]?></td>
			<td><?=$BH[$e_key->login_id]?></td>
			<td><?=$WEO[$e_key->login_id]?></td>
			<td><?=$PHY[$e_key->login_id]?></td>
			<td><?=$total_present[$e_key->login_id]?></td>
			<td><?=$total_absent[$e_key->login_id]?></td>
			<td><?=$total_days[$e_key->login_id]?></td>
			<td><?=$total_payble[$e_key->login_id]?></td>
			<td><?=$total_count[$e_key->login_id]?></td>
			<td><?=$total_off[$e_key->login_id]?></td>
			
		</tr>
	<?php } ?>
		</tbody>
	</table>
</div>	

<? if(!isset($download) && !isset($pdf)) { ?>
<script>
$('#attendancestart').datepicker({
	//dateFormat: "<?=$attendancestart?>"
	dateFormat: "dd-mm-yy"
});	
$('#attendanceend').datepicker({
	dateFormat: "dd-mm-yy"
});

 $('#attendancestart').change(function() {
  var date2 = $('#attendancestart').datepicker('getDate', '+1d'); 
  date2.setDate(date2.getDate()+30); 
  $('#attendanceend').datepicker('setDate', date2);
});

$(document).ready(function() {
    $("#frmattendance").validate();
});
</script>
<?	}	?>
