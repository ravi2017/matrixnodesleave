<form method="post" action="" role="form" name="frmemployee" id="frmemployee" class="form-horizontal">
  <div class="row">
 
    <div class="col-sm-12">
      <div class="form-group row">
        <label for="joining_date" class="col-sm-4 control-label">Joining Date</label>
        <div class="col-sm-8">
          <div class="input-group date">
            <div class="input-group-addon">
              <i class="ti ti-calendar"></i>
            </div>
            <input type="text" readonly class="form-control pull-right required" id="joining_date" placeholder="dd-mm-yyyy" name="joining_date" value="<?=(!empty($emp_details)) ? date('d-m-Y', strtotime($emp_details['joining_date'])) : ''?>">
          </div>          
        </div>						
      </div>					
    </div>
    <div class="col-sm-12">
      <div class="form-group row">
        <label for="schedule_exit_date" class="col-sm-4 control-label">Scheduled Date of Exit</label>
        <div class="col-sm-8">
          <div class="input-group date">
            <div class="input-group-addon">
              <i class="ti ti-calendar"></i>
            </div>
            <input type="text" class="form-control pull-right required" id="schedule_exit_date" placeholder="dd-mm-yyyy" name="schedule_exit_date" value="<?=(!empty($emp_details))  && !empty($emp_details['schedule_exit_date']) ? date('d-m-Y', strtotime($emp_details['schedule_exit_date'])) : ''?>">
          </div>          
        </div>						
      </div>					
    </div>
    
    <div class="col-sm-12">
      <div class="form-group row">
        <label for="resignation_date" class="col-sm-4 control-label">Resignation Date</label>
        <div class="col-sm-8">
          <div class="input-group date">
            <div class="input-group-addon">
              <i class="ti ti-calendar"></i>
            </div>
            <input type="text" class="form-control pull-right required" id="resignation_date" placeholder="dd-mm-yyyy" name="resignation_date" value="<?=(!empty($emp_details)) && !empty($emp_details['resignation_date']) ? date('d-m-Y', strtotime($emp_details['resignation_date'])) : ''?>">
          </div>          
        </div>						
      </div>					
    </div>
    
    <div class="col-sm-12">
      <div class="form-group row">
        <label for="exit_date" class="col-sm-4 control-label">Date of Exit</label>
        <div class="col-sm-8">
          <div class="input-group date">
            <div class="input-group-addon">
              <i class="ti ti-calendar"></i>
            </div>
            <input type="text" class="form-control pull-right required" id="exit_date" placeholder="dd-mm-yyyy" name="exit_date" value="<?=(!empty($emp_details)) && !empty($emp_details['exit_date']) ? date('d-m-Y', strtotime($emp_details['exit_date'])) : ''?>">
          </div>          
        </div>						
      </div>					
    </div>
    
    <div class="col-sm-12">
      <div class="form-group row">
        <label for="exit_date" class="col-sm-4 control-label">Resign Reason</label>
        <div class="col-sm-8">
          <div class="input-group">
            <input type="text" class="form-control pull-right required" id="resign_reason" placeholder="Enter Reason" name="resign_reason" value="<?=$emp_details['resign_reason'];?>">
          </div>          
        </div>						
      </div>					
    </div>
    <?
			$checked_no = '';
			$checked_yes = 'checked="checked"';
			$readonly_served = 'readonly';
			$notice_served = $emp_details['notice_days'];
			if($emp_details['notice_fully_served'] == 'No')
			{
				$checked_no = 'checked="checked"';
				$checked_yes = '';
				$readonly_served = '';
				$notice_served = $emp_details['notice_served'];
			}
    ?>
    <div class="col-sm-12">
      <div class="form-group row">
        <label for="notice_fully_served" class="col-sm-4 control-label">Notice Fully Served</label>
        <div class="col-sm-8">
          <div class="input-group">
            <input type="radio" class="fully_served" name="notice_fully_served" value="Yes" <? echo $checked_yes; ?>>Yes
						&nbsp;<input type="radio" class="fully_served" name="notice_fully_served" value="No" <? echo $checked_no; ?>>No
          </div>          
        </div>						
      </div>					
    </div>
    
    <div class="col-sm-12">
      <div class="form-group row">
        <label for="notice_days" class="col-sm-4 control-label">Notice Days</label>
        <div class="col-sm-8">
          <div class="input-group">
            <input type="text" class="form-control pull-right required" <?=$readonly_served;?> id="notice_served" placeholder="Enter Notice Days" name="notice_served" value="<?=(!empty($notice_served)) ? $notice_served : ''?>">
            <input type="hidden" id="notice_days" value="<?=$emp_details['notice_days'];?>"
          </div>          
        </div>						
      </div>					
    </div>
    
  </div>

  <div class="form-group">
		<input type="hidden" name="spark_id" value="<?php echo $spark_id; ?>" />
		<input type="submit" class="btn btn-info" name="submit" value="Save">
		&nbsp;<a href="<?php echo base_url();?>admin/employees/" class="btn btn-default">Cancel</a>
	</div>
</form>

<script>

$(document).ready(function() {
   $("#frmemployee").validate();
});

$("#birth_date" ).datepicker({
	changeYear:true,
	maxDate: "D",
	dateFormat: "dd-mm-yy"
});

$("#exit_date" ).datepicker({
	dateFormat: "dd-mm-yy"
});

$("#schedule_exit_date" ).datepicker({
	dateFormat: "dd-mm-yy"
});

$(".fully_served").click(function(){
	if($(this).val() == 'Yes'){
		$("#notice_served").val($("#notice_days").val());
		$("#notice_served").attr('readonly', true);
	}else{
		$("#notice_served").attr('readonly', false);
	}
});

$("#resignation_date" ).datepicker({
	dateFormat: "dd-mm-yy"
});

/*$("#joining_date" ).datepicker({
	dateFormat: "dd-mm-yy"
});*/
</script>
