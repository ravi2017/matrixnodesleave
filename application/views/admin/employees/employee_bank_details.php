<form method="post" action="" role="form" name="frmuser" onsubmit="return validateuser()" id="frmuser" class="form-horizontal">
  <div class="row">
		<!--<div class="col-sm-12">
		  <div class="form-group row">
			<label for="payment_mode" class="col-sm-4 control-label">Payment Mode</label>
			<div class="col-sm-8">
			  <select name="payment_mode" id="payment_mode">
					<option value="">-Select Mode-</option>
					<?php foreach($PAYMENT_MODE as $key=>$value){ ?>
						<option value="<?=$key?>"><?=$value;?></option>
					<?	}	?>	
			  </select>
			</div>
		  </div>	
		</div>-->
		<div class="col-sm-12">	
		  <div class="form-group row">
			<input type="hidden" name="payment_mode" id="payment_mode" value="bank_transfer">	
			<label for="bank_branch" class="col-sm-4 control-label">Bank Branch</label>
			<div class="col-sm-8">
			  <select name="bank_branch" id="bank_branch" class="form-control required">
					<option value="">-Select Branch-</option>
					<?php foreach($branchesData as $branches) { ?>
						<option value="<?=$branches->id?>" <? echo (!empty($bank_details) && $bank_details->bank_branch == $branches->id ? 'selected' : ''); ?>><?=$branches->name;?></option>
					<?	}	?>	
			  </select>
			</div>
		  </div>
		</div>
		<div class="col-sm-12">
		  <div class="form-group row">
			<label for="company_bank" class="col-sm-4 control-label">Company Bank</label>
			<div class="col-sm-8">
			  <select name="company_bank" id="company_bank" class="form-control required">
					<option value="">-Select Bank-</option>
					<?php foreach($banksData as $banks) { ?>
						<option value="<?=$banks->id?>" <? echo (!empty($bank_details) && $bank_details->company_bank == $banks->id ? 'selected' : ''); ?>><?=$banks->name;?></option>
					<?	}	?>	
			  </select>
			</div>		
		  </div>		
		</div>
		<div class="col-sm-12">
		  <div class="form-group row">
			<label for="account_number" class="col-sm-4 control-label">Account Number</label>
			<div class="col-sm-8">
			  <input type="text" autocomplete="off" class="form-control required" id="account_number" placeholder="Enter Account Number" name="account_number" value="<?=(!empty($bank_details)) ? $bank_details->account_number : ''?>">
			</div>					
		  </div>					
		</div>
		<div class="col-sm-12">
		  <div class="form-group row">
			<label for="account_name" class="col-sm-4 control-label">Account Name</label>
			<div class="col-sm-8">
			  <input type="text" autocomplete="off" class="form-control required" id="account_name" placeholder="Enter Account Name" name="account_name" value="<?=(!empty($bank_details)) ? $bank_details->account_name : ''?>">
			</div>		
		  </div>		
		</div>
		<div class="col-sm-12">
		  <div class="form-group row">
			<label for="account_type" class="col-sm-4 control-label">Account Type</label>
			<div class="col-sm-8">
			  <input type="text" autocomplete="off" class="form-control required" id="account_type" placeholder="Enter Account Type" name="account_type" value="<?=(!empty($bank_details)) ? $bank_details->account_type : ''?>">
			</div>					
		  </div>					
		</div>
		<div class="col-sm-12">
		  <div class="form-group row">
			<label for="rtgs_ifsc" class="col-sm-4 control-label">RTGS IFSC</label>
			<div class="col-sm-8">
			  <input type="text" autocomplete="off" class="form-control required" id="rtgs_ifsc" placeholder="Enter RTGS IFSC" name="rtgs_ifsc" value="<?=(!empty($bank_details)) ? $bank_details->rtgs_ifsc : ''?>">
			</div>					
		  </div>					
		</div>
		<div class="col-sm-12">
		  <div class="form-group row">
			<label for="rtgs_location" class="col-sm-4 control-label">RTGS Location</label>
			<div class="col-sm-8">
			  <input type="text" autocomplete="off" class="form-control required" id="rtgs_location" placeholder="Enter RTGS Location" name="rtgs_location" value="<?=(!empty($bank_details)) ? $bank_details->rtgs_location : ''?>">
			</div>					
		  </div>					
		</div>
		<div class="col-sm-12">
		  <div class="form-group row">
			<label for="remarks" class="col-sm-4 control-label">Remarks</label>
			<div class="col-sm-8">
			  <input type="text" autocomplete="off" class="form-control" id="remarks" placeholder="Enter Remarks" name="remarks" value="<?=(!empty($bank_details)) ? $bank_details->remarks : ''?>">
			</div>					
		  </div>					
		</div>
  </div>			
  
  <div class="form-group">
		<?
		if (isset($id))
		{
		?>
		<input type="hidden" name="id" value="<?php echo $id; ?>" />
		<?
		}
		?>
		<input type="submit" name="submit" class="btn btn-info" value="Save">
		&nbsp;<a href="<?php echo base_url();?>admin/employees/" class="btn btn-default">Cancel</a>
	</div>
</form>

<script>
$(document).ready(function() {
   $("#frmuser").validate();
});
</script>
