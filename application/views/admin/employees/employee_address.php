<form method="post" action="<?php echo base_url();?>admin/employees/<?=(isset($action)) ? $action : ''?>" role="form" name="frmuser" onsubmit="return validateuser()" id="frmuser" class="form-horizontal">
  <div class="row">
		<div class="col-sm-6">
		  <div class="form-group row">
			<h5>Current Address</h5>
		  </div>	
		</div>
		<?
			$is_checked =   '';
			$is_disabled =   '';
			$same_as = 0;
			if(isset($currentAddress[0]->same_as) && $currentAddress[0]->same_as == 1){
				$is_checked =  'checked';
				$is_disabled =  "disabled='disabled'";
				$same_as = 1;
			}
		?>
		<div class="col-sm-6">
		  <div class="form-group row">
			<h5>Permanent Address</h5>&nbsp;<label>(Same as current address <input type="checkbox" <?=$is_checked;?> name="same_as" id="same_as">)</label>
		  </div>	
		</div>
	</div>	
			
  <div class="row">
   <div class="col-sm-6">
		<div class="col-sm-12">
		  <div class="form-group row">
			<label for="flat_number" class="col-sm-4 control-label">Flat Number</label>
			<div class="col-sm-8">
			  <input type="text" autocomplete="off" class="form-control" id="flat_number" maxlength="40" placeholder="Enter Flat Number" name="flat_number" value="<?=(isset($currentAddress[0]->flat_number) ? $currentAddress[0]->flat_number : '')?>">
			</div>
		  </div>	
		</div>
		<div class="col-sm-12">	
		  <div class="form-group row">
			<label for="premise" class="col-sm-4 control-label">Premise</label>
			<div class="col-sm-8">
			  <input type="text" autocomplete="off" class="form-control" id="premise" maxlength="100" placeholder="Enter Premise" name="premise" value="<?=(isset($currentAddress[0]->premise) ? $currentAddress[0]->premise : '');?>">
			</div>
		  </div>
		</div>
		<div class="col-sm-12">
		  <div class="form-group row">
			<label for="road" class="col-sm-4 control-label">Road</label>
			<div class="col-sm-8">
			  <input type="text" autocomplete="off" class="form-control" id="road" maxlength="100" placeholder="Enter Road" name="road" value="<?=(isset($currentAddress[0]->road)) ? $currentAddress[0]->road : ''?>">
			</div>		
		  </div>		
		</div>
		<div class="col-sm-12">
		  <div class="form-group row">
			<label for="area" class="col-sm-4 control-label">Area</label>
			<div class="col-sm-8">
			  <input type="text" autocomplete="off" class="form-control" id="area" maxlength="100" placeholder="Enter Area" name="area" value="<?=(isset($currentAddress[0]->area)) ? $currentAddress[0]->area : ''?>">
			</div>					
		  </div>					
		</div>
		<div class="col-sm-12">
		  <div class="form-group row">
			<label for="town" class="col-sm-4 control-label">Town</label>
			<div class="col-sm-8">
			  <input type="text" autocomplete="off" class="form-control" maxlength="100" id="town" placeholder="Enter Town" name="town" value="<?=(isset($currentAddress[0]->town)) ? $currentAddress[0]->town : ''?>">
			</div>		
		  </div>		
		</div>
		<div class="col-sm-12">
		  <div class="form-group row">
			<label for="pincode" class="col-sm-4 control-label">Pincode</label>
			<div class="col-sm-8">
			  <input type="text" autocomplete="off" class="form-control" maxlength="6" id="pincode" placeholder="Enter Pincode" name="pincode" value="<?=(isset($currentAddress[0]->pincode)) ? $currentAddress[0]->pincode : ''?>">
			</div>					
		  </div>					
		</div>
		<div class="col-sm-12">
		  <div class="form-group row">
			<label for="state" class="col-sm-4 control-label">State</label>
			<div class="col-sm-8">
			  <select class="form-control" id="state" name="state">
				<option value="">-Select State-</option>
				<?php foreach($statesData as $states) { ?>
					<option value="<?=$states->id?>" <? echo (isset($currentAddress[0]->state) && $currentAddress[0]->state == $states->id ? 'selected' : '' );?>><?=$states->state_code?></option>
				<?php }?>	
			  </select>
			</div>		
		  </div>		
		</div>
		<div class="col-sm-12">
		  <div class="form-group row">
			<label for="country" class="col-sm-4 control-label">Country</label>
			<div class="col-sm-8">
			  <select class="form-control" id="country" name="country">
				<option value="India">India</option>
			  </select>
			</div>					
		  </div>					
		</div>
	 </div>			
	
	 <div class="col-sm-6">
		<div class="col-sm-12">
		  <div class="form-group row">
			<label for="flat_number" class="col-sm-4 control-label">Flat Number</label>
			<div class="col-sm-8">
			  <input type="text" autocomplete="off" class="form-control paddress" id="p_flat_number" placeholder="Enter Flat Number" name="p_flat_number" value="<?=(isset($permanentAddress[0]->flat_number) && $same_as == 0 ? $permanentAddress[0]->flat_number : '');?>" <?=$is_disabled;?>>
			</div>
		  </div>	
		</div>
		<div class="col-sm-12">	
		  <div class="form-group row">
			<label for="premise" class="col-sm-4 control-label">Premise</label>
			<div class="col-sm-8">
			  <input type="text" autocomplete="off" class="form-control paddress" id="p_premise" placeholder="Enter Premise" name="p_premise" value="<?=(isset($permanentAddress[0]->premise) && $same_as == 0 ? $permanentAddress[0]->premise : '');?>" <?=$is_disabled;?>>
			</div>
		  </div>
		</div>
		<div class="col-sm-12">
		  <div class="form-group row">
			<label for="road" class="col-sm-4 control-label">Road</label>
			<div class="col-sm-8">
			  <input type="text" autocomplete="off" class="form-control paddress" id="p_road" placeholder="Enter Road" name="p_road" value="<?=(isset($permanentAddress[0]->road) && $same_as == 0 ? $permanentAddress[0]->road : '');?>" <?=$is_disabled;?>>
			</div>		
		  </div>		
		</div>
		<div class="col-sm-12">
		  <div class="form-group row">
			<label for="area" class="col-sm-4 control-label">Area</label>
			<div class="col-sm-8">
			  <input type="text" autocomplete="off" class="form-control paddress" id="p_area" placeholder="Enter Area" name="p_area" value="<?=(isset($permanentAddress[0]->area) && $same_as == 0 ? $permanentAddress[0]->area : '');?>" <?=$is_disabled;?>>
			</div>					
		  </div>					
		</div>
		<div class="col-sm-12">
		  <div class="form-group row">
			<label for="town" class="col-sm-4 control-label">Town</label>
			<div class="col-sm-8">
			  <input type="text" autocomplete="off" class="form-control paddress" id="p_town" placeholder="Enter Town" name="p_town" value="<?=(isset($permanentAddress[0]->town) && $same_as == 0 ? $permanentAddress[0]->town : '');?>" <?=$is_disabled;?>>
			</div>		
		  </div>		
		</div>
		<div class="col-sm-12">
		  <div class="form-group row">
			<label for="pincode" class="col-sm-4 control-label">Pincode</label>
			<div class="col-sm-8">
			  <input type="text" autocomplete="off" class="form-control paddress" id="p_pincode" placeholder="Enter Pincode" name="p_pincode" value="<?=(isset($permanentAddress[0]->pincode) && $same_as == 0 ? $permanentAddress[0]->pincode : '');?>" <?=$is_disabled;?>>
			</div>					
		  </div>					
		</div>
		<div class="col-sm-12">
		  <div class="form-group row">
			<label for="state" class="col-sm-4 control-label">State</label>
			<div class="col-sm-8">
			  <select class="form-control paddress" id="p_state" name="p_state" <?=$is_disabled;?>>
				<option value="">-Select State-</option>
				<?php foreach($statesData as $states) { ?>
					<option value="<?=$states->id?>" <? echo (isset($permanentAddress[0]->state) && $permanentAddress[0]->state == $states->id  && $same_as == 0 ? 'selected' : '' );?>><?=$states->state_code?></option>
				<?php }?>	
			  </select>
			</div>		
		  </div>		
		</div>
		<div class="col-sm-12">
		  <div class="form-group row">
			<label for="country" class="col-sm-4 control-label">Country</label>
			<div class="col-sm-8">
			  <select class="form-control paddress" id="p_country" name="p_country" <?=$is_disabled;?>>
				<option value="India">India</option>
			  </select>
			</div>					
		  </div>					
		</div>
	 </div>			
  </div>			
  
  <div class="form-group">
		<input type="hidden" name="spark_id" value="<?php echo $spark_id; ?>" />
		<button type="submit" class="btn btn-info">Submit</button>
		&nbsp;<a href="<?php echo base_url();?>admin/employees/" class="btn btn-default">Cancel</a>
	</div>
</form>

<script>
$("#same_as").click(function(){
	if($(this).is(':checked') == true)
	{
		 //$('.paddress [type="text"]').val('');
		 $('.paddress').val('');
		 $('.paddress').attr('disabled',true);
	}
	else{
		$('.paddress').attr('disabled',false);
	}
});
</script>
