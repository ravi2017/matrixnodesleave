<?
if (isset($user))
{
	extract($user);
}
?>
<style>
@media (min-width: 768px)
.form-horizontal .control-label {
    padding-top: 7px;
    margin-bottom: 0;
    text-align: right;
}
</style>
<form method="post" action="<?php echo base_url();?>admin/employees/<?=(isset($action)) ? $action : ''?>" role="form" name="frmemployee" id="frmemployee" class="form-horizontal" enctype="multipart/form-data">
  <div class="row">
    <div class="col-sm-6">
      <div class="form-group row">
        <label for="login_id" class="col-sm-4 control-label">Employee Code</label><?php echo form_error('login_id'); ?>
        <div class="col-sm-8">
					<input type="text" autocomplete="off" class="form-control required" id="login_id" maxlength="20" minlength="5" placeholder="Enter Employee Code" name="login_id" value="<?=(isset($login_id)) ? $login_id : ''?>">
        </div>
      </div>	
    </div>
    <div class="col-sm-6">	
      <div class="form-group row">
        <label for="name" class="col-sm-4 control-label">Name</label>
        <div class="col-sm-8">
          <input type="text" autocomplete="off" class="form-control required" id="name" maxlength="100" minlength="4" placeholder="Enter Name" name="name" value="<?=(isset($name)) ? $name : ''?>">
        </div>
      </div>
    </div>
    <div class="col-sm-6">
      <div class="form-group row">
        <label for="mobile" class="col-sm-4 control-label">Mobile Number</label>
        <div class="col-sm-8">
          <input type="text" autocomplete="off" class="form-control" id="mobile" minlength="10" maxlength="12" placeholder="Enter Mobile Number" name="mobile" value="<?=(isset($mobile)) ? $mobile : ''?>">
        </div>		
      </div>		
    </div>
    <div class="col-sm-6">
      <div class="form-group row">
        <label for="emergency_number" class="col-sm-4 control-label">Emergency Number</label>
        <div class="col-sm-8">
          <input type="text" autocomplete="off" class="form-control" id="emergency_number" placeholder="Enter Emergency Number" name="emergency_number" value="<?=(isset($emergency_number)) ? $emergency_number : ''?>">
        </div>		
      </div>		
    </div>
    <div class="col-sm-6">
      <div class="form-group row">
        <label for="user_group" class="col-sm-4 control-label">User Group</label>
        <div class="col-sm-8">
          <select class="form-control select2 required" id="user_group" name="user_group">
						<option value="">Select User Group</option>
						<?php foreach($USER_GROUPS as $key=>$groups) { ?>
							<option value="<?=$key?>" <?=(isset($user_group) and $user_group == $key) ? 'selected' : ''?>><?=$groups?></option>
            <?	}	?>
          </select>
        </div>		
      </div>		
    </div>
    <div class="col-sm-6">
      <div class="form-group row">
        <label for="role" class="col-sm-4 control-label">Role</label>
        <div class="col-sm-8">
          <select class="form-control select2 required" id="role" name="role">
            <option value="">Select Role</option>
            <?php foreach($EMP_ROLES as $key=>$value) { 
							 if($key != 'accounts' && $key != 'reports' && $key != 'executive'){
							?>
							<option value="<?=$key?>" <?=(isset($role) and $role == $key) ? 'selected' : ''?>><?=$value?></option>
            <?	}	
						}
            ?>
          </select>
        </div>						
      </div>					
    </div>
    
    <div class="col-sm-6">
      <div class="form-group row">
        <label for="email" class="col-sm-4 control-label">Official Email</label>
        <div class="col-sm-8">
          <input type="text" autocomplete="off" class="form-control email" id="email" maxlength="100" placeholder="Enter Email ID" name="email" value="<?=(isset($email)) ? $email : ''?>">
          <span class="err" id="err_email"><?php echo form_error('email'); ?></span>
        </div>		
      </div>		
    </div>
    <div class="col-sm-6">
      <div class="form-group row">
        <label for="personal_email" class="col-sm-4 control-label">Personal Email</label>
        <div class="col-sm-8">
          <input type="text" autocomplete="off" class="form-control email" id="email" maxlength="100" placeholder="Enter Email ID" name="personal_email" value="<?=(isset($personal_email)) ? $personal_email : ''?>">
          <span class="err" id="err_personal_email"><?php echo form_error('personal_email'); ?></span>
        </div>					
      </div>					
    </div>
    <div class="col-sm-6">
      <div class="form-group row">
        <label for="birth_date" class="col-sm-4 control-label">Birth Date</label>
        <div class="col-sm-8">
          <div class="input-group date">
            <div class="input-group-addon">
              <i class="ti ti-calendar"></i>
            </div>
            <input type="text" autocomplete="off" class="form-control pull-right" id="birth_date" placeholder="dd-mm-yyyy" name="birth_date" value="<?=(isset($birth_date)) ? date('d-m-Y', strtotime($birth_date)) : ''?>">
          </div>
          <span class="err" id="err_birth_date"><?php echo form_error('birth_date'); ?></span>
        </div>		
      </div>		
    </div>
    <div class="col-sm-6">
      <div class="form-group row">
        <label for="joining_date" class="col-sm-4 control-label">Joining Date</label>
        <div class="col-sm-8">
          <div class="input-group date">
            <div class="input-group-addon">
              <i class="ti ti-calendar"></i>
            </div>
            <input type="text" autocomplete="off" class="form-control pull-right" id="joining_date" placeholder="dd-mm-yyyy" name="joining_date" value="<?=(isset($joining_date)) ? date('d-m-Y', strtotime($joining_date)) : ''?>">
          </div>          
          <span class="err" id="err_joining_date"><?php echo form_error('joining_date'); ?></span>
        </div>						
      </div>					
    </div>
    <?
			if(isset($aadhar_number)){
				if($aadhar_number != ''){
				$length = strlen($aadhar_number);
				$aadhar_number = "********".substr($aadhar_number, $length-4, $length); 
				}
				else{
					$aadhar_number = '';
				}
			} 
			else{
				$aadhar_number = '';
			}
    ?>
    <div class="col-sm-6">
      <div class="form-group row">
        <label for="aadhar_number" class="col-sm-4 control-label">Aadhar Number</label>
        <div class="col-sm-8">
          <input type="text" autocomplete="off" class="form-control" id="aadhar_number" maxlength="16" placeholder="Enter Aadhar Number" name="aadhar_number" value="<?=$aadhar_number?>">
          <span class="err" id="err_aadhar_number"><?php echo form_error('aadhar_number'); ?></span>
        </div>		
      </div>		
    </div>
    
    <div class="col-sm-6">
      <div class="form-group row">
        <label for="pan_number" class="col-sm-4 control-label">PAN Number</label>
        <div class="col-sm-8">
          <input type="text" autocomplete="off" class="form-control" id="pan_number" maxlength="16" placeholder="Enter PAN Number" name="pan_number" value="<?=(isset($pan_number)) ? $pan_number : ''?>">
          <span class="err" id="err_pan_number"><?php echo form_error('pan_number'); ?></span>
        </div>		
      </div>		
    </div>
    
    <div class="col-sm-6">
      <div class="form-group row">
        <label for="pf_number" class="col-sm-4 control-label">PF Number</label>
        <div class="col-sm-8">
          <input type="text" autocomplete="off" class="form-control" id="pf_number" maxlength="16" placeholder="Enter PF Number" name="pf_number" value="<?=(isset($pf_number)) ? $pf_number : ''?>">
          <span class="err" id="err_pf_number"><?php echo form_error('pf_number'); ?></span>
        </div>		
      </div>		
    </div>
    
    <div class="col-sm-6">
      <div class="form-group row">
        <label for="uan_number" class="col-sm-4 control-label">UAN Number</label>
        <div class="col-sm-8">
          <input type="text" autocomplete="off" class="form-control" id="uan_number" maxlength="16" placeholder="Enter UAN Number" name="uan_number" value="<?=(isset($uan_number)) ? $uan_number : ''?>">
          <span class="err" id="err_uan_number"><?php echo form_error('uan_number'); ?></span>
        </div>		
      </div>		
    </div>
    
    <div class="col-sm-6">
      <div class="form-group row">
        <label for="gender" class="col-sm-4 control-label">Gender</label>
        <div class="col-sm-8">
          <select class="form-control" id="gender" name="gender">
						<?php foreach($EMP_GENDER as $key=>$value) {	?>
							<option value="<?=$key?>" <?=(isset($gender) and $gender == $key) ? "selected" : ""?>><?=$value;?></option>
            <?	}	?>
          </select>
          <span class="err" id="err_gender"><?php echo form_error('gender'); ?></span>
        </div>					
      </div>					
    </div>
  
    <div class="col-sm-6">
      <div class="form-group row">
        <label for="father_name" class="col-sm-4 control-label">Father Name</label>
        <div class="col-sm-8">
          <input type="text" autocomplete="off" class="form-control" id="father_name" maxlength="100" placeholder="Enter Father Name" name="father_name" value="<?=(isset($father_name)) ? $father_name : ''?>">
          <span class="err" id="err_father_name"><?php echo form_error('father_name'); ?></span>
        </div>		
      </div>		
    </div>
    
    <div class="col-sm-6">
      <div class="form-group row">
        <label for="blood_group" class="col-sm-4 control-label">Blood Group</label>
        <div class="col-sm-8">
					<select name="blood_group" id="blood_group">
						<option value="">Select Blood Group</option>	
						<?php foreach($BLOOD_GROUPS as $key=>$value) { ?>
							<option value="<?=$key;?>" <?=(isset($blood_group) && $blood_group == $key) ? 'selected' : ''?>><?=$value?></option>  
						<? } ?>		
					</select>
        </div>					
      </div>					
    </div>
    <div class="col-sm-6">
      <div class="form-group row">
        <label for="marital_status" class="col-sm-4 control-label">Marital Status</label>
        <div class="col-sm-8">
          <select name="marital_status" id="marital_status">
						<option value="married" <?=(isset($marital_status) && $marital_status == 'married') ? 'selected' : ''?>>Married</option>
						<option value="unmarried" <?=(isset($marital_status) && $marital_status == 'unmarried') ? 'selected' : ''?>>Un-Married</option>
          </select>
        </div>					
      </div>					
    </div>
    <?
			$display_marital_status = "style='display:block'";
			if(isset($marital_status) && $marital_status == 'unmarried'){
				$display_marital_status = "style='display:none'";
			}
    ?>
    <div class="col-sm-6" id="div_marital_status" <?=$display_marital_status?>>
      <div class="form-group row">
        <label for="spouse_name" class="col-sm-4 control-label">Spouse Name</label>
        <div class="col-sm-8">
          <input type="text" autocomplete="off" class="form-control" id="spouse_name" placeholder="Enter Spouse Name" name="spouse_name" value="<?=(isset($spouse_name)) ? $spouse_name : ''?>">
        </div>					
      </div>					
    </div>
    
    <div class="col-sm-6">
      <div class="form-group row">
        <label for="marital_status" class="col-sm-4 control-label">Notice Days</label>
        <div class="col-sm-8">
          <input type="text" autocomplete="off" class="form-control" id="notice_days" maxlength="3" placeholder="Enter Notice Days" name="notice_days" value="<?=(isset($notice_days)) ? $notice_days : ''?>">
        </div>					
      </div>					
    </div>
    
    <?
			$cur_year = date('Y');
    ?>
    <div class="col-sm-6">
      <div class="form-group row">
        <label for="appraisal_due" class="col-sm-4 control-label">Appraisal Due</label>
        <div class="col-sm-4">
          <select name="appraisal_month" id="appraisal_month">
						<option value="">-Select Month-</option>
						<?php foreach($APPRAISAL_MONTHS as $key=>$value) { ?>
							<option value="<?=$key;?>" <?=(isset($appraisal_month) && $appraisal_month == $key) ? 'selected' : ''?>><?=$value?></option>  
						<? } ?>	
          </select>
        </div>					
        <div class="col-sm-4">
          <select name="appraisal_year" id="appraisal_year">
						<option value="">-Select Year-</option>
						<? for($i=0; $i<3; $i++){ ?>
							<option value="<?=$cur_year+$i?>" <?=(isset($appraisal_year) && $appraisal_year == $cur_year+$i) ? 'selected' : ''?>><?=$cur_year+$i?></option>
						<?	}	?>	
          </select>
        </div>					
      </div>					
    </div>
    <?
			$checked_yes = '';
			$checked_no = 'checked="checked"';
			if(isset($bike) && $bike == 'Yes')
			{
				$checked_yes = 'checked="checked"';
				$checked_no = '';
			}
    ?>
    <div class="col-sm-6">
      <div class="form-group row">
        <label for="bike" class="col-sm-4 control-label">Bike</label>
        <div class="col-sm-8">
          <input type="radio" class="form-control1" name="bike" value="Yes" <? echo $checked_yes; ?>>Yes
          &nbsp;<input type="radio" class="form-control2" name="bike" value="No" <? echo $checked_no; ?>>No
        </div>					
      </div>					
    </div>
    
    <!--<div class="col-sm-6">
      <div class="form-group row">
        <label for="photo" class="col-sm-4 control-label">Photo</label>
        <div class="col-sm-8">
          <input type="file" name="emp_pic" id="emp_pic">
        </div>					
      </div>					
    </div>-->
    
  </div>
  				
  <hr>
  <div class="row">
    <div class="col-sm-6">
      <div class="form-group row">
        <label for="state" class="col-sm-4 control-label">State</label>
        <div class="col-sm-8">
          <select id="state_id" name="state_id" class="form-control select2 required">
						<option value="">Select State</option>  
						<?php foreach($statesData as $states_data) { ?>
							<option value="<?=$states_data->id;?>" <?=(isset($state_id) && $states_data->id == $state_id) ? 'selected' : ''?>><?=$states_data->state_code;?></option>  
						<? } ?>	
          </select>
        </div>
      </div>	
    </div>
    <div class="col-sm-6">
      <div class="form-group row">
        <label for="branch" class="col-sm-4 control-label">Branch</label>
        <div class="col-sm-8">
          <select id="branch" name="branch" class="form-control select2 required">
						<option value="">Select Branch</option>  
						<?php foreach($EMP_BRANCH as $key=>$value) { ?>
							<option value="<?=$key;?>" <?=(isset($branch) && $branch == $key) ? 'selected' : ''?>><?=$value?></option>  
						<? } ?>	
          </select>
        </div>
      </div>	
    </div>
    <div class="col-sm-6">
      <div class="form-group row">
        <label for="reporting_to" class="col-sm-4 control-label">Reporting To</label>
        <div class="col-sm-8">
          <select id="manager_id" name="manager_id" class="form-control select2">
						<option value="">Select Manager</option>  
						<?php foreach($sparks as $spark) { ?>
							<option value="<?=$spark->id;?>" <?=(isset($manager_id) && $manager_id == $spark->id) ? 'selected' : ''?>><?=$spark->name?></option>  
						<? } ?>	
          </select>
        </div>
      </div>	
    </div>
    <div class="col-sm-6">	
      <div class="form-group row">
        <label for="department" class="col-sm-4 control-label">Department</label>
        <div class="col-sm-8">
          <select id="department" name="department" class="form-control select2 required">
						<option value="">Select Department</option>  
						<?php foreach($EMP_DEPARTMENT as $key=>$value) { ?>
							<option value="<?=$key;?>" <?=(isset($department) and $department == $key) ? 'selected' : ''?>><?=$value?></option>  
						<? } ?>	
          </select>
        </div>
      </div>
    </div>

    <div class="col-sm-6">
      <div class="form-group row">
        <label for="grade" class="col-sm-4 control-label">Grade</label>
        <div class="col-sm-8">
          <select id="grade" name="grade" class="form-control select2 required">
					<option value="">Select Grade</option>  
					<?php foreach($EMP_GRADE as $key=>$value) { ?>
						<option value="<?=$key;?>" <?=(isset($grade) and $grade == $key) ? 'selected' : ''?>><?=$value?></option>  
					<? } ?>	
          </select>
        </div>
      </div>	
    </div>
    <div class="col-sm-6">	
      <div class="form-group row">
        <label for="designation" class="col-sm-4 control-label">Designation</label>
        <div class="col-sm-8">
          <select id="designation" name="designation" class="form-control select2">
						<option value="">Select Designation</option>  
						<?php foreach($EMP_DESIGNATIONS as $key=>$value) { ?>
							<option value="<?=$key;?>" <?=(isset($designation) and $designation == $key) ? 'selected' : ''?>><?=$value?></option>  
						<? } ?>	
          </select>
        </div>
      </div>
    </div>

    <div class="col-sm-6">	
      <div class="form-group row">
        <label for="unit" class="col-sm-4 control-label">Unit</label>
        <div class="col-sm-8">
          <select id="unit_name" name="unit_name" class="form-control select2">
						<option value="">Select Unit</option>  
						<?php foreach($EMP_UNITS as $key=>$value) { ?>
							<option value="<?=$key;?>" <?=(isset($unit_name) and $unit_name == $key) ? 'selected' : ''?>><?=$value?></option>  
						<? } ?>	
          </select>
        </div>
      </div>
    </div>

    <div class="col-sm-6">	
      <div class="form-group row">
        <label for="unit" class="col-sm-4 control-label">Project</label>
        <div class="col-sm-8">
          <select id="project_name" name="project_name" class="form-control select2">
						<option value="">Select Project</option>  
						<?php foreach($EMP_PROJECTS as $key=>$value) { ?>
							<option value="<?=$key;?>" <?=(isset($project_name) and $project_name == $key) ? 'selected' : ''?>><?=$value?></option>  
						<? } ?>	
          </select>
        </div>
      </div>
    </div>

    <div class="col-sm-6">	
      <div class="form-group row">
        <label for="designation" class="col-sm-4 control-label">Employee Category</label>
        <div class="col-sm-8">
          <select id="emp_category" name="emp_category" class="form-control select2 required">
						<option value="">Select Category</option>  
						<?php foreach($EMP_CATEGORY as $key=>$value) { ?>
							<option value="<?=$key;?>" <?=(isset($emp_category) and $emp_category == $key) ? 'selected' : ''?>><?=$value?></option>  
						<? } ?>	
          </select>
          <span class="err" id="err_emp_category"></span>
        </div>
      </div>
    </div>

    <div class="col-sm-6">
      <div class="form-group row">
        <label for="division" class="col-sm-4 control-label">Division</label>
        <div class="col-sm-8">
          <select id="division" name="division" class="form-control select2 required">
						<option value="">Select Division</option>  
						<?php foreach($EMP_DIVISIONS as $key=>$value) { ?>
							<option value="<?=$key;?>" <?=(isset($division) && $division == $key) ? 'selected' : ''?>><?=$value?></option>  
						<? } ?>	
          </select>
          <span class="err" id="err_division"></span>
        </div>
      </div>	
    </div>
    <div class="col-sm-6">	
      <div class="form-group row">
        <label for="attendance_cycle" class="col-sm-4 control-label">Attendance Cycle</label>
        <div class="col-sm-8">
          <select id="attendance_cycle" name="attendance_cycle" class="form-control select2">
						<option value="field_user">Field</option>  
						<option value="head_office">HO</option>  
          </select>
          <span class="err" id="err_attendance_cycle"></span>
        </div>
      </div>
    </div>
  </div>
  
  
  <div class="form-group">
		<?
		if (isset($id))
		{
		?>
		<input type="hidden" name="id" value="<?php echo $spark_id; ?>" />
		<?
		}
		?>
		<button type="submit" class="btn btn-info">Submit</button>
		&nbsp;<a href="<?php echo base_url();?>admin/employees/" class="btn btn-default">Cancel</a>
	</div>
</form>

<script>

$(document).ready(function() {
   $("#frmemployee").validate({
			highlight: function (element, errorClass, validClass) {
					$(element).parents('.form-control').removeClass('has-success').addClass('has-error');     
			},
			unhighlight: function (element, errorClass, validClass) {
					$(element).parents('.form-control').removeClass('has-error').addClass('has-success');
			},
			errorPlacement: function (error, element) {
					if(element.hasClass('select2') && element.next('.select2-container').length) {
						error.insertAfter(element.next('.select2-container'));
					} else if (element.parent('.input-group').length) {
							error.insertAfter(element.parent());
					}
					else if (element.prop('type') === 'radio' && element.parent('.radio-inline').length) {
							error.insertAfter(element.parent().parent());
					}
					else if (element.prop('type') === 'checkbox' || element.prop('type') === 'radio') {
							error.appendTo(element.parent().parent());
					}
					else {
							error.insertAfter(element);
					}
			}
	});
});

$("#marital_status").change(function(){
	if($(this).val() == 'unmarried'){
		$("#div_marital_status").hide();
		$("#spouse_name").val('');
	}
	else{
		$("#div_marital_status").show();
	}
});


$("#birth_date" ).datepicker({
	changeYear:true,
	maxDate: "D",
	dateFormat: "dd-mm-yy"
});

$("#joining_date" ).datepicker({
	maxDate: "+2M",
	dateFormat: "dd-mm-yy"
});

$(document).on('input', '#login_id', function(){
  var login_id = $(this).val();
  if(login_id == ''){
    $('#err_login_id').empty();
  }
  $.ajax({
    url: '<?=base_url()?>admin/sparks/check_user_id',
    type: 'post',
    data: "user_id="+login_id,
    success: function(response) {
        $('#err_login_id').empty();
        $('#err_login_id').append(response);
        $('#login_id').val();
    },
    error: function(response) {
      window.console.log(response);
    }
  });
});
</script>
