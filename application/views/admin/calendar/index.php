<script type="text/javascript">
function show_confirm(act,gotoid)
{
if(act=="edit")
var r=confirm("Do you really want to edit?");
else
var r=confirm("Do you really want to delete?");
if (r==true)
{
window.location="<?php echo base_url();?>admin/calendar/"+act+"/"+gotoid;
}
}
</script>
<a href="<?php echo base_url();?>admin/calendar/add">Add Calendar</a><br><br>
<div style="display:none;" class="alert alert-info"></div>
<table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
	<thead>
		<tr>
			<th>ID</th>
			<th>Name</th>
			<th>Calendar Date</th>
			<th>Task</th>
			<th>Actions</th>
		</tr>
	</thead>
	<tbody>
	
	<? $calendar_count = 0; ?>
	<?php foreach ($calendar_list as $e_key){ ?>
		<tr>
			<td><?php echo $calendar_count = $calendar_count + 1; ?></td>
			<td><?php echo $e_key->login_id." (".$e_key->name.")"; ?></td>
			<td><?php echo date("d-m-Y",strtotime($e_key->calendar_date)); ?></td>
			<td><?php echo $e_key->task; ?></td>
			<td class="center">
				<a class="btn btn-info" href="#" onClick="show_confirm('edit',<?php echo $e_key->id;?>)">
					<i class="glyphicon glyphicon-edit icon-white"></i>
					Edit
				</a>
				<a class="btn btn-danger" href="#" onClick="show_confirm('delete',<?php echo $e_key->id;?>)">
					<i class="glyphicon glyphicon-trash icon-white"></i>
					Delete
				</a>
			</td>
		</tr>
	<?php } ?>
		</tbody>
	</table>
	
	
	
