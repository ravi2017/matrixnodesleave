<?
if (isset($calendar))
{
	extract($calendar);
  $calendar_date = implode("-",array_reverse(explode("-",$calendar_date)));
}
?>

<form action="<?php echo base_url();?>admin/calendar/<?=(isset($action)) ? $action : ''?>" method="post" accept-charset="utf-8" onsubmit="return campaignvalidate('<?=(isset($action)) ? $action : ''?>')" name="Formcampaign">
  <?
  if (isset($error))
  {
  ?>
  <div class="alert alert-danger"><?php echo $error; ?></div>
  <?php
  }
  ?>
	<div class="form-group">
		<label for="name">User</label><?php echo form_error('title'); ?>
		<span class="err" id="err_name"></span>
		<input type="text" class="form-control" id="login_id" placeholder="Enter Login ID" name="login_id" value="<?=(isset($name)) ? $login_id." (".$name.")" : ''?>">
		<input type="hidden" class="form-control" id="user_id" placeholder="Enter User ID" name="user_id" value="<?=(isset($user_id)) ? $user_id : ''?>">
	</div>
	<div class="form-group">
		<label for="name">Calendar Date</label><?php echo form_error('title'); ?>
		<span class="err" id="err_name"></span>
		<input type="text" class="form-control" id="calendar_date" placeholder="Enter Calendar Date" name="calendar_date" value="<?=(isset($calendar_date)) ? $calendar_date : ''?>">
	</div>
	<div class="form-group">
		<label for="name">Task</label><?php echo form_error('title'); ?>
		<span class="err" id="err_name"></span>
		<textarea class="form-control" id="task" name="task"><?=(isset($task)) ? $task : ''?></textarea>
	</div>
	<div class="form-group">
		<?
		if (isset($id))
		{
		?>
		<input type="hidden" name="id" value="<?php echo $id; ?>" />
		<?
		}
		?>
		<button type="submit" class="btn btn-default">Submit</button>
	</div>
</form>


<script>
$( document ).ready(function() {
  $("#calendar_date").datepicker({
    minDate: +0,
    changeMonth:true,
    dateFormat:"dd-mm-yy",
    changeYear:true
  });
});

$(document).ready(function(){     
  $("#login_id").autocomplete({
    source: "<?=base_url()?>admin/users/get_users", // path to the get_author method
    minLength: 3,
    select: function( event, ui ) {
      $("#user_id").val(ui.item.user_id)
			$("#login_id").val(ui.item.label);
    }
  });
});

</script>

