<div class="row">            
	<form action="<?php echo base_url(); ?>admin/schools/upload_school_list" method="post" enctype="multipart/form-data">
		<div class="modal-body">
			<label>Select State</label>
			<select name="state" id="state" class="form-control" required>
				<?php foreach($states as $state) { ?>
					<option value="<?php echo $state->id; ?>"><?php echo $state->name; ?></option>
				<?php } ?>
			</select>
			<br>
			<label>Upload CSV File</label>
			<input type="file" name="fileToUpload" id="fileToUpload" class="form-control">
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
			<input type="submit" class="btn btn-sm btn-primary" value="Upload" name="subupload">
		</div>
	</form>
</div>
