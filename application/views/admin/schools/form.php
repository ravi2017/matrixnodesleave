<?
if (isset($school))
{
	extract($school);
}
?>

<form action="<?php echo base_url();?>admin/schools/<?=(isset($action)) ? $action : ''?>" method="post" accept-charset="utf-8" name="frmSchools" id="frmSchools">
  <?
  if (isset($error))
  {
  ?>
  <div class="alert alert-danger"><?php echo $error; ?></div>
  <?php
  }
  ?>
  <div class="form-group">
		<label for="state">State</label><?php echo form_error('state'); ?>
		<span class="err" id="err_state"></span>
		<select class="form-control required" id="state_id" name="state_id">
      <option value="">Select State</option>
    <?  ?>
      <? foreach($states as $state) { ?>
      <option value="<?=$state->id?>" <?=(isset($state_id) and $state->id == $state_id) ? "selected" : ""?>><?=$state->name?></option>
      <? } ?>
    </select>
	</div>
  <div class="form-group">
		<label for="district">District</label><?php echo form_error('district'); ?>
		<span class="err" id="err_district"></span>
		<select class="form-control required" id="district_id" name="district_id">
    </select>
	</div>
  <div class="form-group">
		<label for="block">Block</label><?php echo form_error('block'); ?>
		<span class="err" id="err_block"></span>
		<select class="form-control required" id="block_id" name="block_id">
    </select>
	</div>
  <div class="form-group">
		<label for="cluster">Cluster</label><?php echo form_error('cluster'); ?>
		<span class="err" id="err_cluster"></span>
		<select class="form-control required" id="cluster_id" name="cluster_id">
    </select>
	</div>
  <!--<div class="form-group">
		<label for="village">Village</label><?php echo form_error('village'); ?>
		<span class="err" id="err_village"></span>
		<select class="form-control" id="village_id" name="village_id">
    </select>
	</div>-->
	<div class="form-group">
		<label for="name">Name</label><?php echo form_error('name'); ?>
		<span class="err" id="err_name"></span>
		<input type="text" class="form-control required" id="title" placeholder="Enter Name" name="name" value="<?=(isset($name)) ? $name : ''?>">
	</div>
	<div class="form-group">
		<label for="name">DISE Code</label><?php echo form_error('dise_code'); ?>
		<span class="err" id="err_name"></span>
		<input type="text" class="form-control" id="dise_code" placeholder="Enter DISE Code" name="dise_code" value="<?=(isset($dise_code)) ? $dise_code : ''?>">
	</div>
	<div class="form-group">
		<label for="name">SRP/DRP Name</label><?php echo form_error('srp_drp_name'); ?>
		<span class="err" id="err_name"></span>
		<input type="text" class="form-control" id="srp_drp_name" placeholder="Enter SRP/DRP Name" name="srp_drp_name" value="<?=(isset($srp_drp_name)) ? $srp_drp_name : ''?>">
	</div>
	<div class="form-group">
		<?
		if (isset($id))
		{
		?>
		<input type="hidden" name="id" value="<?php echo $id; ?>" />
		<?
		}
		?>
		<button type="submit" class="btn btn-default">Submit</button>
	</div>
</form>


<script>
$(document).ready(function ()
{
  $("#frmSchools").validate();
});

function get_district(sid,did)
{
  $.ajax({
    url: '<?=base_url()?>admin/districts/list',
    type: 'get',
    data: {
      state: sid
    },
    dataType: 'json',
    success: function(response) {
      $("#district_id").html("<option value=''>Select District</option>");
      $.each(response, function() {
        if (did == this['id'])
          $("#district_id").append("<option value='" + this['id'] + "' selected>" + this['value'] + "</option>");
        else
          $("#district_id").append("<option value='" + this['id'] + "'>" + this['value'] + "</option>");
      });
      $("#district_id").show();
    },
    error: function(response) {
      window.console.log(response);
    }
  });
}

$("#state_id").change(function() {
  get_district($(this).val(),'')
});

<? if (isset($district_id)) { ?>
  get_district('<?=$state_id?>','<?=$district_id?>')
<? } ?>

function get_block(did,bid)
{
  $.ajax({
    url: '<?=base_url()?>admin/blocks/list',
    type: 'get',
    data: {
      district: did
    },
    dataType: 'json',
    success: function(response) {
      $("#block_id").html("<option value=''>Select Block</option>");
      $.each(response, function() {
        if (bid == this['id'])
          $("#block_id").append("<option value='" + this['id'] + "' selected>" + this['value'] + "</option>");
        else
          $("#block_id").append("<option value='" + this['id'] + "'>" + this['value'] + "</option>");
      });
      $("#block_id").show();
    },
    error: function(response) {
      window.console.log(response);
    }
  });
}

$("#district_id").change(function() {
  get_block($(this).val(),'')
});

<? if (isset($block_id)) { ?>
  get_block('<?=$district_id?>','<?=$block_id?>')
<? } ?>


function get_cluster(bid,cid)
{
  $.ajax({
    url: '<?=base_url()?>admin/clusters/list',
    type: 'get',
    data: {
      block: bid
    },
    dataType: 'json',
    success: function(response) {
      $("#cluster_id").html("<option value=''>Select Cluster</option>");
      $.each(response, function() {
        if (cid == this['id'])
          $("#cluster_id").append("<option value='" + this['id'] + "' selected>" + this['value'] + "</option>");
        else
          $("#cluster_id").append("<option value='" + this['id'] + "'>" + this['value'] + "</option>");
      });
      $("#cluster_id").show();
    },
    error: function(response) {
      window.console.log(response);
    }
  });
}

$("#block_id").change(function() {
  get_cluster($(this).val(),'')
});

<? if (isset($cluster_id)) { ?>
  get_cluster('<?=$block_id?>','<?=$cluster_id?>')
<? } ?>


function get_village(cid,vid)
{
  $.ajax({
    url: '<?=base_url()?>admin/villages/list',
    type: 'get',
    data: {
      cluster: cid
    },
    dataType: 'json',
    success: function(response) {
      $("#village_id").html("<option value=''>Select Village</option>");
      $.each(response, function() {
        if (vid == this['id'])
          $("#village_id").append("<option value='" + this['id'] + "' selected>" + this['value'] + "</option>");
        else
          $("#village_id").append("<option value='" + this['id'] + "'>" + this['value'] + "</option>");
      });
      $("#village_id").show();
    },
    error: function(response) {
      window.console.log(response);
    }
  });
}

$("#cluster_id").change(function() {
  get_village($(this).val(),'')
});

<? if (isset($village_id)) { ?>
  get_village('<?=$cluster_id?>','<?=$village_id?>')
<? } ?>
</script>
