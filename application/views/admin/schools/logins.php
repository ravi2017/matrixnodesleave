<style>
.num {
  mso-number-format:General;
}
.text{
  mso-number-format:"\@";/*force text*/
}
</style>
<table align="center" class='responses' cellspacing='2' border="1">
  <tbody>
    <tr>
      <th colspan="3"><span style="font-weight: bold;">LOGIN IDs</span></th>
    </tr>
    <tr>
      <th colspan="1"><span style="font-weight: bold;">Name</span></th>
      <th colspan="1"><span style="font-weight: bold;">Role</span></th>
      <th colspan="1"><span style="font-weight: bold;">Login ID</span></th>
    </tr>
    <?php foreach($logins as $login)
    {
    ?>
    <tr>
      <td colspan="1"><?php echo $login->teacher_first_name; ?></th>
      <td colspan="1"><?php echo ucwords(str_replace("_"," ",$login->role)); ?></th>
      <td colspan="1" class="text"><?php echo $login->phone_number; ?></th>
    </tr>
    <?php
    }
    ?>
  </tbody>
</table>
