<div class="row">
  <div class="col-md-3">
  <a class="btn btn-sm btn-success" href="<?php echo base_url();?>admin/schools/add?state_id=<?=isset($state_id) ? $state_id : ''?>&district_id=<?=isset($district_id) ? $district_id : ''?>&block_id=<?=isset($block_id) ? $block_id : ''?>&cluster_id=<?=isset($cluster_id) ? $cluster_id : ''?>"> <i class="glyphicon glyphicon-plus icon-white"></i> Add School</a>
  </div>
  <div class="col-md-3">
  <a class="btn btn-sm btn-primary" href="#" id="upload"> <i class="glyphicon glyphicon-upload icon-white"></i> Upload Schools</a><br>
  <a href="<?php echo base_url(); ?>assets/admin/schools_upload_template.csv" onclick="return confirm('Do not make any changes in the first row of the downloaded file.')">Download School Upload Template</a>
  </div>
  <div class="col-md-3">
  <a class="btn btn-sm btn-danger" href="#" id="disableschools"> <i class="glyphicon glyphicon-trash icon-white"></i> Disable/Enable Schools</a><br>
  <a href="<?php echo base_url(); ?>assets/admin/schools_disable_enable_template.csv" onclick="return confirm('Do not make any changes in the first row of the downloaded file.')">Download School Disable/Enable Template</a>
  </div>
  <div class="col-md-3">
  <a class="btn btn-sm btn-warning" href="<?php echo base_url(); ?>admin/schools/set_school_average"> <i class="glyphicon glyphicon-trash icon-white"></i> Reset High Enroll Schools</a>
  </div>
  <!--
  <div class="col-md-3">
  <a class="btn btn-sm btn-warning" href="#" id="downloadlogins"> <i class="glyphicon glyphicon-download icon-white"></i> Download Logins</a><br>
  </div> -->
  <div class="clearfix"></div>
  <br><br>
</div>
<div style="display:none;" class="alert alert-info"></div>
<br>
<form name="frmreport" id="frmreport" action="" method="get" onsubmit="return validatereport()">
  <div class="row">
    <div class="form-group field_user col-sm-2">
      <select class="form-control" id="state_id" name="state_id">
        <option value="0">Select State</option>
        <? foreach($states as $state) { ?>
				
				<option value="<?=$state->id?>" <?=(isset($state_id) and $state->id == $state_id) ? "selected" : ""?>><?=$state->name?></option>
        <? } ?>
      </select>
    </div>
    <div class="form-group field_user col-sm-3">
      <select class="form-control" id="district_id" name="district_id">
        <option value="0">Select District</option>
      </select>
    </div>
    <div class="form-group field_user col-sm-3">
      <select class="form-control" id="block_id" name="block_id">
        <option value="0">Select Block</option>
      </select>
    </div> 
    <div class="form-group field_user col-sm-3">
      <select class="form-control" id="cluster_id" name="cluster_id">
         <option value="0">Select Cluster</option>
      </select>
    </div>
    <div class="col-sm-1">
      <button type="submit" class="btn btn-sm btn-info pull-right">Fetch</button>
    </div>
  </div>
</form>


<?if(isset($school_list)and count($school_list) >0){?>
<!--
<a class="btn btn-sm btn-danger" id="disableall" href="javascript:void(0)">Disable/Enable Selected</a>
<br>  
<br>  -->
<br>
<table id="schoollisting" class="table table-striped table-bordered bootstrap-datatable datatable responsive">
	<thead>
		<tr>
			<th>Name</th>
			<th>DISE Code</th>
			<th>SRP/DRP Name</th>
			<th>Status</th>
			<th class="no-sort no-search action">Actions</th>
		</tr>
	</thead>
	<tbody>
	
	<? $school_count = 0; ?>
	<?php foreach ($school_list as $e_key){ ?>
    <tr>
      <td><?php echo $e_key->name; ?></td>
			<td><?php echo $e_key->dise_code; ?></td>
			<td><?php echo $e_key->srp_drp_name; ?></td>
      <td>
          <?php if ($e_key->status==1) { ?><label class="label-success label">Enabled</label><?php } else { ?><label class="label-danger label">Disabled</label><?php } ?>
      </td>
			<td class="action">
				<a class="text-info" href="<?php echo base_url();?>admin/states/<?php echo $e_key->id;?>">
					<i class="ti-pencil"></i>
				</a>
				<a href="<?php echo base_url();?>admin/schools/changestatus/<?php echo $e_key->id;?>">
					<?php if ($e_key->status==1) { ?>
            <i class="ti-thumb-down text-danger"></i>
					<?php } else { ?>
            <i class="ti-thumb-up text-success"></i>
          <?php } ?>
				</a>
			</td>
		</tr>
	<?php } ?>
  
  </tbody>
</table>
<?}?>

<script>
<? if ($current_role != "field_user") { ?>	
<? if (isset($state_id) and $state_id > 0  and isset($district_id) and $district_id > 0) { ?>
  get_district_list(<?=$state_id?>,$("#district_id"),<?=$district_id?>);
<? }elseif(isset($state_id) and $state_id > 0){ ?> 
  get_district_list(<?=$state_id?>,$("#district_id"));
<? } ?>
<?}?>
<? if (isset($district_id) and $district_id > 0 and $block_id!="all") { ?>
  get_block_list(<?=$district_id?>,$("#block_id"),<?=$block_id?>);
<? } ?>
<? if (isset($block_id) and $block_id > 0 and isset($cluster_id) and $cluster_id > 0) { ?>
  get_cluster_list(<?=$block_id?>,$("#cluster_id"),<?=$cluster_id?>);
<? }elseif(isset($block_id) and $block_id>0){ ?>
  get_cluster_list(<?=$block_id?>,$("#cluster_id"),'');
<?}?>

</script>
	

<!-- Modal -->
<div class="modal fade" id="downloadModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                 <h4 class="modal-title">Download Logins</h4>
            </div>
            <form action="<?php echo base_url(); ?>admin/schools/download_logins" method="post">
            <div class="modal-body">
              <div class="form-group field_user col-sm-3">
                <label>Select State</label>
                <select name="state_id" id="downloadstate" class="form-control">
                  <option value="0">Select State</option>
                  <?php foreach($states as $state) { ?>
                    <option value="<?php echo $state->id; ?>"><?php echo $state->name; ?></option>
                  <?php } ?>
                </select>
              </div>
              <div class="form-group field_user col-sm-3">
                <label>Select District</label>
                <select class="form-control" id="downloaddistrict" name="district_id">
                  <option value="0">Select District</option>
                </select>
              </div>
              <div class="form-group field_user col-sm-3">
                <label>Select Block</label>
                <select class="form-control" id="downloadblock" name="block_id">
                  <option value="0">Select Block</option>
                </select>
              </div> 
              <div class="form-group field_user col-sm-3">
                <label>Select Cluster</label>
                <select class="form-control" id="downloadcluster" name="cluster_id">
                   <option value="0">Select Cluster</option>
                </select>
              </div>
              <div class="clearfix"></div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
              <input type="submit" class="btn btn-sm btn-primary" value="Download" name="subdownload">
            </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- Modal -->
<div class="modal fade" id="uploadModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Upload Schools</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <form action="<?php echo base_url(); ?>admin/schools/upload" method="post" enctype="multipart/form-data">
            <div class="modal-body">
              <label>Select State</label>
              <select name="state" id="state" class="popupUpload form-control" required>
                <?php foreach($states as $state) { ?>
                  <option value="<?php echo $state->id; ?>"><?php echo $state->name; ?></option>
                <?php } ?>
              </select>
              <br>
              <label>Upload CSV File</label>
              <input type="file" name="fileToUpload" id="fileToUpload" class="form-control">
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
              <input type="submit" class="btn btn-sm btn-primary" value="Upload" name="subupload">
            </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- Modal -->
<div class="modal fade" id="disableModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Disable/Enable Schools</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <form action="<?php echo base_url(); ?>admin/schools/disable" method="post" enctype="multipart/form-data">
            <div class="modal-body">
              <label>Upload CSV File</label>
              <input type="file" name="fileToUpload" id="fileToUpload" class="form-control">
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
              <input type="submit" class="btn btn-sm btn-primary" value="Upload" name="subupload">
            </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
