<script type="text/javascript">
function show_confirm(act,gotoid)
{
if(act=="edit")
var r=confirm("Do you really want to edit?");
else
var r=confirm("Do you really want to delete?");
if (r==true)
{
window.location="<?php echo base_url();?>admin/states/"+act+"/"+gotoid;
}
}
</script>
<a class="btn btn-info btn-sm" href="<?php echo base_url();?>admin/states/add">Add State</a><br><br>
<div style="display:none;" class="alert alert-info"></div>
<table class="table table-striped table-bordered bootstrap-datatable datatable responsive customdatatable">
	<thead>
		<tr>
			<th>Names</th>
			<th>State Code</th>
			<th class="no-sort action no-search">Actions</th>
		</tr>
	</thead>
	<tbody>
	
	<? $state_count = 0; ?>
	<?php foreach ($state_list as $e_key){ ?>
		<tr>
			<td><a class="" href="<?php echo base_url();?>admin/districts?state=<?php echo $e_key->id;?>"><?php echo $e_key->name; ?></a></td>
			<td><?php echo $e_key->dise_code; ?></td>
			<td class="action">
				<a class="" href="<?php echo base_url();?>admin/states/edit/<?php echo $e_key->id;?>">
					<i class="ti-pencil"></i>
				</a>
			</td>
		</tr>
	<?php } ?>
  </tbody>
</table>
	
	
	
