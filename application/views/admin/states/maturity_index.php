<script type="text/javascript">
function show_confirm(act,gotoid)
{
if(act=="edit")
var r=confirm("Do you really want to edit?");
else
var r=confirm("Do you really want to delete?");
if (r==true)
{
window.location="<?php echo base_url();?>admin/states/"+act+"/"+gotoid;
}
}
</script>

<form action="<?php echo base_url();?>admin/states/create_maturity_index" method="post" accept-charset="utf-8" name="frmState" id="frmState">
	<div class="row">
    <div class="col-md-6">
      <div class="form-group">
        <label for="name">State Name</label><?php echo form_error('state'); ?>
        <span class="err" id="err_name"></span>
        <select name="sel_state">
          <option value="">-Select State-</option>
          <?php foreach($state_list as $states) { ?>
            <option value="<?=$states->id."_".$states->name?>"><?php echo $states->name;?></option>
          <?php } ?>  
        </select>
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        <label for="dise_code">Maturity Score</label><?php echo form_error('maturity_score'); ?>
        <span class="err" id="err_name"></span>
        <input type="text" class="form-control required" id="maturity_score" placeholder="Enter Score" name="maturity_score" value="" maxlength="1">
      </div>
    </div>
  </div>
	<div class="form-group">
		<?
		if (isset($id))
		{
		?>
		<input type="hidden" name="id" value="<?php echo $id; ?>" />
		<?
		}
		?>
		<button type="submit" class="btn btn-default">Submit</button>
	</div>
</form>


<div style="display:none;" class="alert alert-info">
  <?
    if(!empty($message)){
        echo $message;
    }
  ?>
</div>
<table class="table table-striped table-bordered bootstrap-datatable datatable responsive customdatatable">
	<thead>
		<tr>
			<th>State Names</th>
			<th>Maturity Score</th>
			<th>Month-Year</th>
			<th class="no-sort action no-search">Actions</th>
		</tr>
	</thead>
	<tbody>
	
	<? 
  $months = array('01'=>'Jan', '02'=>'Feb', '03'=>'Mar', '04'=>'Apr', '05'=>'May', '06'=>'Jun', '07'=>'Jul', '08'=>'Aug', '09'=>'Sep', '10'=>'Oct', '11'=>'Nov', '12'=>'Dec');
  $state_count = 0; ?>
	<?php foreach ($maturity_list as $e_key){ 
    
      $exp_duration = explode('_',$e_key->month_year);
    ?>
		<tr>
			<td><a class="" href="<?php echo base_url();?>admin/districts?state=<?php echo $e_key->id;?>"><?php echo $e_key->state; ?></a></td>
			<td><?php echo $e_key->score; ?></td>
			<td><?php echo $months[$exp_duration[0]]."-".$exp_duration[1]; ?></td>
			<td class="action text-center">
				<a class="" href="<?php echo base_url();?>admin/states/delete_maturity/<?php echo $e_key->id;?>"  onclick="return confirm('Are you sure to remove this record?')">
					<i class="ti-trash"></i>
				</a>
			</td>
		</tr>
	<?php } ?>
  </tbody>
</table>
	
	
	
