<?
if (isset($state)){
	extract($state);
}
?>
<form action="<?php echo base_url();?>admin/states/<?=(isset($action)) ? $action : ''?>" method="post" accept-charset="utf-8" name="frmState" id="frmState">
	<div class="row">
    <div class="col-md-6">
      <div class="form-group">
        <label for="name">Name</label><?php echo form_error('name'); ?>
        <span class="err" id="err_name"></span>
        <input type="text" class="form-control required" id="title" minlength="3" maxlength="20" placeholder="Enter Name" name="name" value="<?=(isset($name)) ? $name : ''?>">
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        <label for="dise_code">State Code</label><?php echo form_error('dise_code'); ?>
        <span class="err" id="err_name"></span>
        <input type="text" class="form-control required" id="dise_code" placeholder="Enter DISE" name="dise_code" value="<?=(isset($dise_code)) ? $dise_code : ''?>" minlength="2" maxlength="4">
      </div>
    </div>
  </div>
	<div class="form-group">
		<?
		if (isset($id))
		{
		?>
		<input type="hidden" name="id" value="<?php echo $id; ?>" />
		<?
		}
		?>
		<button type="submit" class="btn btn-default">Submit</button>
	</div>
</form>
<script>
  $(document).ready(function() {
    $("#frmState").validate();
  });
</script>
