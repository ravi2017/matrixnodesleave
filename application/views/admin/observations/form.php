<?
if (isset($observations))
{
	extract($observations);
}

$activity_arr = unserialize(ACTIVITY_ARRAY);
?>
<form method="post" action="<?php echo base_url();?>admin/observations/<?=(isset($action)) ? $action : ''?>" role="form" name="frmobservation" id="frmobservation">

  <div class="row">
    <div class="col-md-6">
      <div class="form-group">
        <label for="activity_type">Activity Type <span class="make-me-red">*</span></label>
        <select class="form-control required" id="activity_type" name="activity_type">
          <option value="">Select Activity</option>
          <? foreach($activity_arr as $key=>$value) { ?>
            <option value="<?php echo $key; ?>" <?=(isset($activity_type) and $key == $activity_type) ? "selected" : ""?>><?php echo $value; ?></option>
          <? } ?>
        </select>
        <label id="activity_type-error" class="error" for="activity_type" style="display:none"></label>
      </div>
    </div>
    
    <div class="col-md-6">
      <div class="form-group state_person">
        <label for="question_type">Question Type <span class="make-me-red">*</span></label><span class="err" id="err_question_type"></span>
        <select class="form-control" id="question_type" name="question_type">
          <option value="yes-no" <? if(isset($question_type) && $question_type == 'yes-no'){ echo 'selected'; }?>>Yes/No</option>
          <option value="objective" <? if(isset($question_type) && $question_type == 'objective'){ echo 'selected'; }?>>Objective</option>
          <option value="subjective" <? if(isset($question_type) && $question_type == 'subjective'){ echo 'selected'; }?>>Subjective</option>
        </select>
      </div>
    </div>
  </div>
  
  <div class="row">
    <div class="col-md-6">
      <div class="form-group state_person">
        <label for="class">Select Class</label><span class="err" id="err_class_id"></span>
        <select class="form-control basic-multiple select2" id="class_id" name="class_id[]" <? if(empty($id)) { ?> multiple <? } ?> >
          <option value="">-Select Class-</option>
          <? foreach($classes as $class) { ?>
          <option value="<?=$class->id?>" <?=(isset($class_id) and $class->id == $class_id) ? "selected" : ""?>><?=$class->name?></option>
          <? } ?>
        </select>
      </div>
    </div>
    
    <div class="col-md-6">
      <div class="form-group state_person">
        <label for="state">Select State <span class="make-me-red">*</span></label><span class="err" id="err_state_id"></span>
        <select class="form-control basic-multiple select2 required" id="state_id" name="state_id[]" <? if(empty($id)) { ?> multiple <? } ?> >
          <option value="">-Select State-</option>
          <? foreach($states as $state) { ?>
          <option value="<?=$state->id?>" <?=(isset($state_id) and $state->id == $state_id) ? "selected" : ""?>><?=$state->name?></option>
          <? } ?>
        </select>
      </div>
    </div>
  </div>
  
  <div class="row">
    <div class="col-md-6">
      <div class="form-group">
        <label for="question">Question <span class="make-me-red">*</span></label><?php echo form_error('question'); ?>
        <span class="err" id="err_question"></span>
        <input type="text" class="form-control required" id="question" placeholder="Enter Question" name="question" value="<?=(isset($question)) ? $question : ''?>">
      </div>					
    </div>
    <div class="col-md-6">
      <div class="row">
        <div class="col-md-4">
          <div class="form-group">
            <label for="weightage">Weightage</label><?php echo form_error('weightage'); ?><br />
            <span class="err" id="err_weightage"></span>
            <input type="text" class="form-control" name="weightage" id="weightage" value="<? if(!empty($weightage)){ echo $weightage; }?>" placeholder="Enter Weightage">
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-6">
      <div class="row">
         <div class="col-md-8">
          <div class="form-group">
            <label for="subject_wise">Subject Wise Responses Required</label><?php echo form_error('subject_wise'); ?><br />
            <span class="err" id="err_subject_wise"></span>
            <input type="checkbox" name="subject_wise" id="subject_wise" value="yes" <? if(isset($subject_wise) && $subject_wise == '1'){ echo 'checked'; } ?>>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-6">
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label for="subject_wise">For Productive Report</label><?php echo form_error('productivity_flag'); ?><br />
            <span class="err" id="err_productivity_flag"></span>
            <input type="checkbox" name="productivity_flag" id="productivity_flag" value="yes" <? if(isset($productivity_flag) && $productivity_flag == '1'){ echo 'checked'; } ?>>
          </div>  
        </div>  
        <div class="col-md-6">
          <div class="form-group">
            <label for="subject_wise">For Whatsapp Report</label><?php echo form_error('productivity_flag'); ?><br />
            <span class="err" id="err_whatsapp_flag"></span>
            <input type="checkbox" name="whatsapp_flag" id="whatsapp_flag" value="yes" <? if(isset($whatsapp_flag) && $whatsapp_flag == '1'){ echo 'checked'; } ?>>
          </div>  
        </div>
      </div>  
    </div>
  </div>    
  
  <div class="questionoptions">
    <div class="row">
      <div class="col-md-12"><label>Options</label></div>
    </div>
    <div class="row">
      <div class="form-group col-md-6"><input type="text" name="options[]" value="" class="form-control" ></div>
    </div>
    <div class="row">
      <div class="form-group col-md-6"><input type="text" name="options[]" value="" class="form-control" ></div>
    </div>
    <div class="row">
      <div class="form-group col-md-6"><input type="text" name="options[]" value="" class="form-control" ></div>
    </div>
    <div class="row">
      <div class="form-group col-md-6"><input type="text" name="options[]" value="" class="form-control" ></div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">    
      <div class="form-group">
        <? if (!empty($id)) {	?>
          <input type="hidden" name="id" value="<?php echo $id; ?>" />
        <?  }	?>
        <button type="submit" class="btn btn-sm btn-success">Submit</button>
      </div>
    </div>
  </div>
</form>

<script>
$(document).ready(function() {
    $("#frmobservation").validate();
    $('.basic-multiple').select2();
    $("#question_type").change(function(){
      if ($(this).val() == "objective")
        $(".questionoptions").show()
      else
        $(".questionoptions").hide()
    })
    if ($("#question_type").val() == "objective")
      $(".questionoptions").show()
    else
      $(".questionoptions").hide()
});
</script>
