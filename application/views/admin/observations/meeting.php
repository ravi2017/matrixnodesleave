<?
if (isset($observations))
{
	extract($observations);
}

$activity_arr = unserialize(ACTIVITY_ARRAY);
?>
<form method="post" action="<?php echo base_url();?>admin/observations/<?=(isset($action)) ? $action : ''?>" role="form" name="frmobservation" id="frmobservation">
  <input type="hidden" value="meeting" name="activity_type">
  <div class="row">
    <div class="col-md-8 offset-md-2">
      <div class="row">
        <label class="col-md-4" for="question">Question <span class="make-me-red">*</span></label><?php echo form_error('question'); ?>
        <div class="col-md-8">
          <div class="form-group">
            <span class="err" id="err_question"></span>
            <input type="text" class="form-control required" id="question" placeholder="Enter Question" name="question" value="<?=(isset($question)) ? $question : ''?>">
          </div>					
        </div>
      </div>
      <div class="row">
        <label class="col-md-4" for="question_type">Question Type <span class="make-me-red">*</span></label><span class="err" id="err_question_type"></span>
        <div class="col-md-8">
          <div class="form-group state_person">
            <?php if ($action == "create") { ?>
            <select class="form-control" id="question_type" name="question_type">
              <option value="yes-no" <? if(isset($question_type) && $question_type == 'yes-no'){ echo 'selected'; }?>>Yes/No</option>
              <option value="objective" <? if(isset($question_type) && $question_type == 'objective'){ echo 'selected'; }?>>Objective</option>
              <option value="subjective" <? if(isset($question_type) && $question_type == 'subjective'){ echo 'selected'; }?>>Subjective</option>
            </select>
            <?php } else { ?>
                <?=ucwords(str_replace("-"," / ",$question_type))?>
                <input type="hidden" value="<?=$question_type?>" id="question_type">
            <?php } ?>
          </div>
        </div>
      </div>
      <div class="row">
        <label class="col-md-4" for="state">Select State <span class="make-me-red">*</span></label><span class="err" id="err_state_id"></span>
        <div class="col-md-8">
          <div class="form-group state_person">
            <?php if ($action == "create") { ?>
            <select class="form-control basic-multiple select2 required" id="state_id" name="state_id[]" <? if(empty($id)) { ?> multiple <? } ?> >
              <option value="">-Select State-</option>
              <? foreach($states as $state) { ?>
              <option value="<?=$state->id?>" <?=((!isset($state_id)) or (isset($state_id) and $state->id == $state_id)) ? "selected" : ""?>><?=$state->name?></option>
              <? } ?>
            </select>
            <?php } else { ?>
              <? foreach($states as $state) { ?>
                <?=((!isset($state_id)) or (isset($state_id) and $state->id == $state_id)) ? $state->name : ""?>
              <? } ?>
            <?php } ?>
          </div>
        </div>
      </div>
      <div class="row">
        <label class="col-md-4" for="subject_wise">Used for Productive Report</label><?php echo form_error('productivity_flag'); ?>
        <div class="col-md-8">
          <div class="form-group">
            <span class="err" id="err_productivity_flag"></span>
            <input type="checkbox" name="productivity_flag" id="productivity_flag" value="yes" <? if(isset($productivity_flag) && $productivity_flag == '1'){ echo 'checked'; } ?>>
          </div>  
        </div> 
      </div>
      <div class="row">
        <label class="col-md-4" for="subject_wise">Used for WhatsApp Report</label><?php echo form_error('productivity_flag'); ?>
        <div class="col-md-8">
          <div class="form-group">
            <span class="err" id="err_whatsapp_flag"></span>
            <input type="checkbox" name="whatsapp_flag" id="whatsapp_flag" value="yes" <? if(isset($whatsapp_flag) && $whatsapp_flag == '1'){ echo 'checked'; } ?>>
          </div>  
        </div>  
      </div>
    
      <?php echo $this->load->view('admin/observations/options','',true) ?>
    </div>      
  </div>
  
  <div class="row">
    <div class="col-md-12">    
      <div class="form-group text-right">
        <? if (!empty($id)) {	?>
          <input type="hidden" name="id" value="<?php echo $id; ?>" />
        <?  }	?>
        <a href="<?php echo base_url('admin/observations'); ?>" class="btn btn-sm btn-default">Cancel</a>
        <button type="submit" class="btn btn-sm btn-success">Submit</button>
      </div>
    </div>
  </div>
</form>

<script>
$(document).ready(function() {
    $("#frmobservation").validate();
    $('.basic-multiple').select2();
});
</script>

