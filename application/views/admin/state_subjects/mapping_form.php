<?
if (isset($observations))
{
	extract($observations);
}

$activity_arr = unserialize(ACTIVITY_ARRAY);
?>
<form method="post" action="<?php echo base_url();?>admin/state_subjects/<?=(isset($action)) ? $action : ''?>" role="form" name="frmMapping" id="frmMapping">
  <div class="row">
    <div class="col-md-4">
      <div class="form-group state_person">
        <label for="state">Select State <span class="make-me-red">*</span></label><span class="err" id="err_state_id"></span>
        <select class="form-control basic-multiple select2 required" id="state_id" name="state_id[]" <? if(empty($id)) { ?> multiple <? } ?> >
          <option value="">-Select State-</option>
          <? foreach($states as $state) { ?>
          <option value="<?=$state->id?>" <?=(isset($state_id) and $state->id == $state_id) ? "selected" : ""?>><?=$state->name?></option>
          <? } ?>
        </select>
      </div>
    </div>
    
    <div class="col-md-4">
      <div class="form-group state_person">
        <label for="class">Select Class <span class="make-me-red">*</span></label><span class="err" id="err_class_id"></span>
        <select class="form-control basic-multiple select2 required" id="class_id" name="class_id[]" <? if(empty($id)) { ?> multiple <? } ?> >
          <option value="">-Select Class-</option>
          <? foreach($classes as $class) { ?>
          <option value="<?=$class->id?>" <?=(isset($class_id) and $class->id == $class_id) ? "selected" : ""?>><?=$class->name?></option>
          <? } ?>
        </select>
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group state_person">
        <label for="state">Select Subjects <span class="make-me-red">*</span></label><span class="err" id="err_state_id"></span>
        <select class="form-control basic-multiple select2 required" id="subject_id" name="subject_id[]" <? if(empty($id)) { ?> multiple <? } ?> >
          <option value="">-Select Subjects-</option>
          <? foreach($subjects as $subject) { ?>
          <option value="<?=$subject->id?>" <?=(isset($subject_id) and $subject->id == $subject_id) ? "selected" : ""?>><?=$subject->name?></option>
          <? } ?>
        </select>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">    
      <div class="form-group text-right">
        <? if (!empty($id)) {	?>
          <input type="hidden" name="id" value="<?php echo $id; ?>" />
        <?  }	?>
        <button type="submit" class="btn btn-sm btn-success">Submit</button>
      </div>
    </div>
  </div>
</form>

<script>
$(document).ready(function() {
    $("#frmMapping").validate();
    $('.basic-multiple').select2();
});
</script>
