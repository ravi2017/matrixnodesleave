<a class="btn btn-success btn-sm" href="<?php echo base_url();?>admin/state_subjects/add">Add Mapping</a><br><br>
<div style="display:none;" class="alert alert-info"></div>
<table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
	<thead>
		<tr>
			<th>S.No</th>
			<th>State</th>
			<th>Class</th>
			<th>Subject</th>
			<th data-sort=0 class="no-sort action">Status</th>
			<th data-priority="1" data-sort=0 class="no-sort action">Actions</th>
		</tr>
	</thead>
	<tbody>
	
	<?php $i=1;
    foreach ($mapping_list as $e_key){ 
  ?>
		<tr>
      <td><?=$i;?></td>
			<td><?php echo $e_key->state_name; ?></td>
			<td><?php echo $e_key->class_name; ?></td>
			<td><?php echo $e_key->subject_name; ?></td>
      <td>
        <?php if ($e_key->status=="1") { ?>
          <span class="label-success label">Active</span>
        <?php } else { ?>
          <span class="label-danger label">Inactive</span>
        <?php } ?>  
      </td>
			<td class="action">
				<a href="<?php echo base_url();?>admin/state_subjects/changemapstatus/<?php echo $e_key->id;?>">
					<?php if ($e_key->status==1) { ?>
            <i class="ti-thumb-down text-danger"></i>
					<?php } else { ?>
            <i class="ti-thumb-up text-success"></i>
          <?php } ?>
				</a>
        <a href="<?php echo base_url();?>admin/state_subjects/deletemapping/<?php echo $e_key->id;?>"  onclick="return confirm('Are you sure?')">
					<i class="ti-trash"></i>
				</a>			
			</td>
		</tr>
	<?php $i++; } ?>
		</tbody>
	</table>
