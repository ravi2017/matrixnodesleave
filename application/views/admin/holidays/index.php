<script type="text/javascript">
function show_confirm(act,gotoid)
{
if(act=="edit")
var r=confirm("Do you really want to edit?");
else
var r=confirm("Do you really want to delete?");
if (r==true)
{
window.location="<?php echo base_url();?>admin/holidays/"+act+"/"+gotoid;
}
}
</script>
<a class="btn btn-sm btn-success" href="<?php echo base_url();?>admin/holidays/add">Add Holiday</a><br><br>
<div style="display:none;" class="alert alert-info"></div>
<table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
	<thead>
		<tr>
			<th>Holiday Name</th>
			<th>Holiday Type</th>
			<th>Holiday Date</th>
			<th>Is National</th>
			<th>State</th>
			<th class="action no-sort no-search">Actions</th>
		</tr>
	</thead>
	<tbody>
	
	<? $holiday_count = 0; ?>
	<?php foreach ($holidays_list as $e_key){ ?>
		<tr>
			<td><?php echo $e_key->name; ?></td>
			<td><?php echo $USER_GROUPS[$e_key->holiday_type]; ?></td>
			<td><?php echo date("d-m-Y",strtotime($e_key->activity_date)); ?></td>
      <td><?php echo ($e_key->national) ? 'YES' : 'NO'; ?></td>
      <td><?php echo $e_key->statename; ?></td>
			<td class="action">
				<a class="btn btn-danger" onclick="return confirm('Are you sure to remove ?');" href="<?php echo base_url()?>admin/holidays/delete/<?php echo $e_key->id?>">
					<i class="glyphicon glyphicon-trash icon-white"></i>
					Delete
				</a>
			</td>
		</tr>
	<?php } ?>
		</tbody>
	</table>
	
	
	
