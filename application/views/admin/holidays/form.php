  <?
if (isset($holidays[0]))
{
	extract($holidays[0]);
  $holiday_date = implode("-",array_reverse(explode("-",$holiday_date)));
}
?>

<form action="<?php echo base_url();?>admin/holidays/<?=(isset($action)) ? $action : ''?>" method="post" accept-charset="utf-8" onsubmit="return campaignvalidate('<?=(isset($action)) ? $action : ''?>')" name="Formcampaign">
  <?
  if (isset($error))
  {
  ?>
  <div class="alert alert-danger"><?php echo $error; ?></div>
  <?php
  }
  ?>
	<div class="form-group">
		<label for="name"><strong>Type</strong></label><?php echo form_error('title'); ?>
		<span class="err" id="err_name"></span>
    <select name="holiday_type" id="holiday_type">
			<? foreach($USER_GROUPS as $key=>$value) { ?>
				<option value="<?=$key;?>" <?php if(isset($holiday_type) and $holiday_type == $key) { echo 'selected'; } ?> ><?=$value?></option>
      <?	}	?>
    </select>
	</div>
  <div class="form-group">
    <label for="name"><strong>Holiday Name</strong></label><?php echo form_error('title'); ?>
    <span class="err" id="err_name"></span>
    <input type="text" class="form-control" id="name" placeholder="Enter Holiday Name" name="name" value="<?=(isset($name)) ? $name : ''?>" autocomplete="off">
  </div>
	<div class="form-group">
		<label for="name"><strong>Holiday Date</strong></label><?php echo form_error('title'); ?>
		<span class="err" id="err_name"></span>
		<input type="text" class="form-control" id="holiday_date" placeholder="Enter Holiday Date" name="holiday_date" value="<?=(isset($holiday_date)) ? $holiday_date : ''?>" autocomplete="off">
	</div>
	<!--<div class="form-group">
		<label for="name">National Holiday</label><?php echo form_error('title'); ?>
		<span class="err" id="err_name"></span>
		<input type="checkbox" id="is_national" name="is_national" value="1" <?=(isset($national) and $national == 0) ? '' : 'checked'?>>
	</div>-->
  <div class="form-group states">
    <label for="title"><strong>Select State</strong></label>&nbsp;<span class="err" id="err_title"></span>
    <? foreach($states as $state) { ?>
    <input name="states[]" type="checkbox" value="<?=$state->id?>" <?php if(isset($state_ids) and in_array($state->id, $state_ids)) echo "checked"; ?>>&nbsp;<?=$state->name?>&nbsp;&nbsp;
    <? } ?>
  </div>
	<div class="form-group">
		<?
		if (isset($id))
		{
		?>
		<input type="hidden" name="id" value="<?php echo $id; ?>" />
		<?
		}
		?>
		<button type="submit" class="btn btn-default">Submit</button>
	</div>
</form>


<script>
$( document ).ready(function() {
  $("#holiday_date").datepicker({
    minDate: +0,
    changeMonth:true,
    dateFormat:"yy-mm-dd",
    changeYear:true
  });
  $("#is_national").click(function(){
    if ($(this).prop("checked"))
      $(".states").hide()
    else
      $(".states").show()
  });
  
 /* if ($("#is_national").prop("checked"))
    $(".states").hide()
  else
    $(".states").show()*/
});
</script>

