<style>
.num {
  mso-number-format:General;
}
.text{
  mso-number-format:"\@";/*force text*/
}
</style>
<table align="center" class='responses' cellspacing='2' border="1">
  <tbody>
    <tr>
      <th colspan="3"><span style="font-weight: bold;">LOGIN IDs</span></th>
    </tr>
    <tr>
      <th colspan="1"><span style="font-weight: bold;">Holiday Name</span></th>
      <th colspan="1"><span style="font-weight: bold;">Holiday Date</span></th>
    </tr>
    <?php foreach($logins as $login)
    {
    ?>
    <tr>
      <td colspan="1"><?php echo $login->name; ?></th>
      <td colspan="1" class="text"><?php echo $login->holiday_date; ?></th>
    </tr>
    <?php
    }
    ?>
  </tbody>
</table>
