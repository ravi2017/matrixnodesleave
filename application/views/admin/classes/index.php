<script type="text/javascript">
function show_confirm(act,gotoid)
{
if(act=="edit")
var r=confirm("Do you really want to edit?");
else
var r=confirm("Do you really want to delete?");
if (r==true)
{
window.location="<?php echo base_url();?>admin/classes/"+act+"/"+gotoid;
}
}
</script>
<a class="btn btn-sm btn-success" href="<?php echo base_url();?>admin/classes/add">Add Class</a><br><br>
<div style="display:none;" class="alert alert-info"></div>
<? if(!empty($message)) { ?><div class="alert alert-danger"><?=$message;?></div> <? } ?>
<table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
	<thead>
		<tr>
			<th>Class Name</th>
			<th>Subject Name</th>
			<th>Status</th>
			<th class="action no-sort no-search">Actions</th>
		</tr>
	</thead>
	<tbody>
	
	<? $holiday_count = 0; ?>
	<?php foreach ($classes_list as $e_key){ 
      $subjects = get_subjects_info(explode(',',$e_key->subject_id));
      $sub_arr = array();
      foreach($subjects as $key=>$value){
        $sub_arr[] = $value->name;
      }
    ?>
		<tr>
			<td><?php echo $e_key->name; ?></td>
			<td><?php echo implode(', ',$sub_arr); ?></td>
      <td>
        <?php if ($e_key->status=="1") { ?>
          <span class="label-success label">Active</span>
        <?php } else { ?>
          <span class="label-danger label">Inactive</span>
        <?php } ?>  
      </td>
			<td>
        <a class="" href="<?php echo base_url();?>admin/classes/edit/<?php echo $e_key->id;?>">
					<i class="ti-pencil"></i>
				</a>
				<a href="<?php echo base_url();?>admin/classes/changestatus/<?php echo $e_key->id;?>">
					<?php if ($e_key->status==1) { ?>
            <i class="ti-thumb-up text-success"></i>
					<?php } else { ?>
            <i class="ti-thumb-down text-danger"></i>
          <?php } ?>
				</a>
			</td>
		</tr>
	<?php } ?>
		</tbody>
	</table>
	
	
	
