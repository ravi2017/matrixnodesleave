<div class="row">
	<div class="form-group col-md-3">
		<a href="<?php echo base_url();?>admin/leaves/credit_leaves" class="btn btn-success">Add</a>
	</div>	
	<div class="form-group col-md-9" align="right">
		<a href="<?php echo base_url();?>admin/leaves/spark_leave_details" class="btn btn-success">Spark Leave Details</a>
	</div>	
</div>	
<form id="frmattendance" name="frmcreditleave" action="" method="post">
	<div class="row">
	 <? if($current_role == 'super_admin' || $current_role == 'HR') { ?>	
	  <div class="form-group col-md-3">
		<select class="form-control" id="spark_id" name="spark_id">
		  <option value="">Select Spark</option>
		  <?php foreach($sparks as $spark) { ?>
		  <option value="<?=$spark->id?>" <?php echo ((isset($spark_id) && $spark_id == $spark->id) ? 'selected' : '');?>><?php echo $spark->name." (".$spark->login_id.")"?></option>
		  <?php } ?>
		</select>
	  </div>
	  <? }else{	?>
		  <input type="hidden" id="spark_id" name="spark_id" class="form-control" value="<?=$current_user_id?>">
	  <? } ?>	  
		<!--<div class="form-group col-md-3">
		  <div class="form-group">
			<input type="text" id="creditleavestart" name="creditleavestart" class="form-control required" value="<?=$creditleavestart?>">
		  </div>
        </div>
        <div class="form-group col-md-3">
		  <div class="form-group">
			<input type="text" id="creditleaveend" name="creditleaveend" class="form-control required" value="<?=$creditleaveend?>">
		  </div>
    </div>-->
	  <div class="form-group col-md-3">
        <input type="submit" name="submit" value="Get List" class="btn btn-sm btn-info">
	  </div>
  </div>
</form>

<table class="table table-striped table-bordered bootstrap-datatable datatable responsive customdatatable">
	<thead>
		<tr>
			<th>Type</th>
			<th>Leave Earned</th>
			<th>Leave Taken</th>
			<th>Details</th>
		</tr>
	</thead>
	<tbody>
	
	<?php
	  if(!empty($creditleavelist)){	
		foreach ($creditleavelist as $e_key){ ?>
		<tr>
			<td><?php echo ucfirst($e_key->leave_type); ?></td>
			<td><?php echo $e_key->leave_earned; ?></td>
			<td><?php echo $e_key->leave_taken; ?></td>
			<td>
				<a href="javascript:void(0)" class="view_credit_leaves" alt="<?=$e_key->id?>">View</a>
			</td>
		</tr>
	<?php } } ?>
  </tbody>
</table>

<div class="modal fade" id="show_leavecredit" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Credit Leaves Details</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body" id="leavecredit_data">
			</div>
		</div>
	</div>
</div>


<script>
$(document).on('click', '.view_credit_leaves', function(){
		var cur_id = $(this).attr('alt');

		$.ajax({
			url: BASE_URL+'admin/leaves/credit_leave_details',
			type: 'post',
			data: 'cur_id='+cur_id,
			dataType: 'html',
			success: function(response) {
				$("#leavecredit_data").html(response)
				$("#show_leavecredit").modal('show');
			},
			error: function(response, error) {
				window.console.log(error);
			}
		});
});	
	
$(document).ready(function() {
    $("#frmcreditleave").validate();
});
</script>
