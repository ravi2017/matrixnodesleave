<a class="btn btn-info btn-sm" href="<?php echo base_url();?>admin/leaves/add_state_leave">Add State Leaves</a><br><br>
<div style="display:none;" class="alert alert-info"></div>
<table class="table table-striped table-bordered bootstrap-datatable datatable responsive customdatatable">
	<thead>
		<tr>
			<th>State</th>
			<th>Leave Type</th>
			<th>Leave Count</th>
			<th class="no-sort action no-search">Actions</th>
		</tr>
	</thead>
	<tbody>
	
	<?php
		foreach ($stateleaves as $e_key){ ?>
		<tr>
			<td><?php echo $e_key->state_code; ?></td>
			<td><?php echo $e_key->leave_type; ?></td>
			<td><?php echo $e_key->leave_count; ?></td>
			<td class="action">
				<a class="" href="<?php echo base_url();?>admin/leaves/edit_state_leave/<?php echo $e_key->id;?>">
					<i class="ti-pencil"></i>
				</a>
				<a class="" href="<?php echo base_url();?>admin/leaves/delete_state_leave/<?php echo $e_key->id;?>">
					<i class="ti-trash"></i>
				</a>
			</td>
		</tr>
	<?php } ?>
  </tbody>
</table>
