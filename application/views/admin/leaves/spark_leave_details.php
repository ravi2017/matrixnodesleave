<? if(!isset($download) && !isset($pdf)) {?>
<form id="frmattendance" name="frmcreditleave" action="" method="post">
	<div class="row">
	 <? if($current_role == 'super_admin' || $current_role == 'HR') { ?>	
	  <div class="form-group col-md-3">
		<select class="form-control" id="spark_id" name="spark_id">
		  <option value="">Select Spark</option>
		  <?php foreach($sparks as $spark) { ?>
		  <option value="<?=$spark->id?>" <?php echo ((isset($spark_id) && $spark_id == $spark->id) ? 'selected' : '');?>><?php echo $spark->name." (".$spark->login_id.")"?></option>
		  <?php } ?>
		</select>
	  </div>
	  <? }else{	?>
		  <input type="hidden" id="spark_id" name="spark_id" class="form-control" value="<?=$current_user_id?>">
	  <? } ?>	  
		<!--<div class="form-group col-md-3">
		  <div class="form-group">
			<input type="text" id="creditleavestart" name="creditleavestart" class="form-control required" value="<?=date('d-m-Y', strtotime($creditleavestart));?>">
		  </div>
        </div>
        <div class="form-group col-md-3">
		  <div class="form-group">
			<input type="text" id="creditleaveend" name="creditleaveend" class="form-control required" value="<?=date('d-m-Y', strtotime($creditleaveend));?>">
		  </div>
    </div>-->
	  <div class="form-group col-md-3">
        <input type="submit" name="submit" value="Get List" class="btn btn-sm btn-info">
	  </div>
  </div>
</form>

<div class="form-group row">
    <div class="form-group col-md-3">
      <form name="frmreport" action="<?php echo base_url();?>admin/leaves/spark_leave_details" method="post">
          <input type="hidden" name="spark_id" value="<?=$spark_id?>">
          <input type="hidden" name="creditleaveend" value="<?=$creditleaveend?>">
          <input type="hidden" name="creditleaveend" value="<?=$creditleaveend?>">
          <input type="hidden" name="download" value="1">
          <button type="submit" name="submit" value="download" class="btn btn-success">Download Report</button>
      </form>
    </div>
    <div class="form-group col-md-3">
      <form name="frmreport" action="<?php echo base_url();?>admin/leaves/spark_leave_details" method="post">
				<input type="hidden" name="spark_id" value="<?=$spark_id?>">
          <input type="hidden" name="creditleavestart" value="<?=$creditleavestart?>">
          <input type="hidden" name="creditleaveend" value="<?=$creditleaveend?>">
          <input type="hidden" name="pdf" value="1">
          <button type="submit" name="submit" value="download" class="btn btn-success">Download PDF</button>
      </form>
    </div>
</div>

<?	}	?>

<table class="table table-striped table-bordered bootstrap-datatable credittable responsive customdatatable" width="100%" id="credittable">
	<thead>
		<tr style="background-color:#D5ECF9">
			<th align="center">Type</th>
			<th align="center">From Date</th>
			<th align="center">End Date</th>
			<th align="center">Total Days</th>
			<th align="center">Day Type</th>
			<th align="center">Reason</th>
		</tr>
	</thead>
	<tbody>
	
	<?php
	  if(!empty($leaveslist)){	
		foreach ($leaveslist as $e_key){ ?>
		<tr>
			<td align="left"><?php echo ucfirst($e_key->leave_type); ?></td>
			<td align="right"><?php echo date('d-m-Y', strtotime($e_key->leave_from_date)); ?></td>
			<td align="right"><?php echo date('d-m-Y', strtotime($e_key->leave_end_date)); ?></td>
			<td align="right"><?php echo $e_key->total_days; ?></td>
			<td align="right"><?php echo ($e_key->day_type != '' ? $e_key->day_type : 'N/A'); ?></td>
			<td align="right"><?php echo ($e_key->reason != '' ? $e_key->reason : 'N/A'); ?></td>
		</tr>
	<?php } } ?>
  </tbody>
</table>

<? if(!isset($download) && !isset($pdf)) {?>
<script>
$('#creditleavestart').datepicker({
	dateFormat: "dd-mm-yy"
});	
$('#creditleaveend').datepicker({
	dateFormat: "dd-mm-yy"
});
$(document).ready(function() {
    $("#frmcreditleave").validate();
});
</script>
<?	}	?>
