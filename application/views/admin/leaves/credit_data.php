<table class="table table-striped table-bordered bootstrap-datatable responsive">	
<? if(!empty($creditData)){ ?>
		<tr>
			<th>S.No</th>
			<th>Leave Count</th>
			<th>Credit Date</th>
			<th>Description</th>
		</tr>
<? $i=1;
foreach($creditData as $credit_data) { ?>
		<tr>
			<td><?=$i?></td>
			<td><?=$credit_data->leave_count;?></td>
			<td><?=date('d-m-Y', strtotime($credit_data->created_at));?></td>
			<td><?=$credit_data->description;?></td>
		</tr>
<? $i++; } ?>
<? }else{ ?>
	<tr>
			<td>No Record Found.</td>
	</tr>		
<? } ?>  
</table>
          

