<?
if (isset($groupleaves)){
	extract($groupleaves);
}
?>
<form action="<?php echo base_url();?>admin/leaves/<?=(isset($action)) ? $action : ''?>" method="post" accept-charset="utf-8" name="frmUserLeave" id="frmUserLeave">
	<div class="row">
    <div class="col-md-6 form-group">
			<label for="name">User Group</label><?php echo form_error('user_group'); ?>
			<select name="user_group" id="user_group" class="form-control required" readonly disabled>
				<option value="">-Select Group-</option>
				<?
					foreach($USER_GROUPS as $key=>$value)	
					{
						$selected = ((isset($user_group) && $user_group == $key) ? 'selected' : '');
						?><option value="<?=$key?>" <?php echo $selected; ?>><?=$value;?></option><?
					}
				?>
			</select> 
    </div>
   </div>
	<div class="row">
    <div class="col-md-6 form-group">
			<label for="name">Leave Type</label><?php echo form_error('leave_type'); ?>
			<select name="leave_type" id="leave_type" class="form-control required" readonly disabled>
				<option value="">-Select Leave Type-</option>
				<?
					foreach($LEAVE_TYPES as $key=>$value)	
					{
						$selected = ((isset($leave_type) && $leave_type == $key) ? 'selected' : '');
						?><option value="<?=$key?>" <?php echo $selected; ?>><?=$value;?></option><?
					}
				?>
			</select> 
    </div>
   </div>
   <div class="row">
    <div class="col-md-6 form-group">
			<label for="forward_value">Carry Forward Value</label><?php echo form_error('forward_value'); ?>
      <div class="row">
        <div class="col-md-6 form-group">
          <input type="text" class="form-control required" id="forward_value" placeholder="Enter Leave Count" name="forward_value" value="<?=(isset($forward_value)) ? $forward_value : ''?>" minlength="1" maxlength="3">
        </div>
        <div class="col-md-6 form-group">
          <select name="forward_type" id="forward_type" class="form-control required">
            <option value="percent" <?=(isset($forward_type) && $forward_type == "percent") ? 'selected' : ''?>>Percent of Total Earned</option>
            <option value="count" <?=(isset($forward_type) && $forward_type == "count") ? 'selected' : ''?>>Count</option>
          </select> 
        </div>
      </div>
    </div>
   </div>
   <div class="row maxvalue">
    <div class="form-group col-md-6">
			<label for="start_from">Maximum Number of Leave to be Carry Forward</label><?php echo form_error('max_value'); ?>  
			<input type="text" id="max_value" name="max_value" class="form-control required" value="<?=(isset($max_value)) ? $max_value : ''?>">
	  </div>
	</div>
    
  </div>
  <div class="row">
    <div class="form-group col-md-6" style="text-align:center">
			<? if (isset($id)) { ?>
				<input type="hidden" name="id" value="<?php echo $id; ?>" />
			<? } ?>
			<button type="submit" class="btn btn-info">Submit</button>
			<a href="<?php echo base_url();?>admin/leaves/carry_forward_policies" class="btn btn-default">Cancel</a>
		</div>
	</div>
</form>
<script>
  $(document).ready(function() {
    $("#frmUserLeave").validate();
    $("#forward_type").change(function(){
      if ($(this).val() == "percent")
        $(".maxvalue").show();
      else
        $(".maxvalue").hide();
      $("#max_value").val("");
    })
    
  });
  
</script>
