<?
if (isset($stateleaves)){
	extract($stateleaves);
}
?>
<form action="<?php echo base_url();?>admin/leaves/<?=(isset($action)) ? $action : ''?>" method="post" accept-charset="utf-8" name="frmUserLeave" id="frmUserLeave">
	<div class="row">
    <div class="col-md-4">
      <div class="form-group">
        <label for="name">State</label><?php echo form_error('state_code'); ?>
        <span class="err" id="err_name"></span>
        <select name="state_code" id="state_code" class="form-control required">
					<option value="">-Select State-</option>
					<?
						foreach($states as $state)	
						{
							$selected = ((isset($state_code) && $state_code == $state->name) ? 'selected' : '');
							?><option value="<?=$state->name?>" <?php echo $selected; ?>><?=$state->name;?></option><?
						}
					?>
        </select> 
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label for="name">Leave Type</label><?php echo form_error('leave_type'); ?>
        <span class="err" id="err_name"></span>
        <select name="leave_type" id="leave_type" class="form-control required">
					<option value="EL">EL/Paid Leave</option>
        </select> 
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label for="leave_count">Leave Count</label><?php echo form_error('leave_count'); ?>
        <span class="err" id="err_name"></span>
        <input type="text" class="form-control required" id="leave_count" placeholder="Enter Leave Count" name="leave_count" value="<?=(isset($leave_count)) ? $leave_count : ''?>" minlength="1" maxlength="2">
      </div>
    </div>
  </div>
	<div class="form-group">
		<?
		if (isset($id))
		{
		?>
		<input type="hidden" name="id" value="<?php echo $id; ?>" />
		<?
		}
		?>
		<button type="submit" class="btn btn-info">Submit</button>
		<a href="<?php echo base_url();?>admin/leaves" class="btn btn-default">Cancel</a>
	</div>
</form>
<script>
  $(document).ready(function() {
    $("#frmUserLeave").validate();
  });
</script>
