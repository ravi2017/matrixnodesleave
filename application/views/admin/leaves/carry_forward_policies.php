<script type="text/javascript">
function show_confirm(act,gotoid)
{
if(act=="edit")
var r=confirm("Do you really want to edit?");
else
var r=confirm("Do you really want to delete?");
if (r==true)
{
window.location="<?php echo base_url();?>admin/states/"+act+"/"+gotoid;
}
}
</script>
<a class="btn btn-info btn-sm" href="<?php echo base_url();?>admin/leaves/add_carry_forward_policy">Add Carry Forward Policy</a><br><br>
<div style="display:none;" class="alert alert-info"></div>
<table class="table table-striped table-bordered bootstrap-datatable datatable responsive customdatatable">
	<thead>
		<tr>
			<th>User Group</th>
			<th>Leave Type</th>
			<th>Carry Forward Value</th>
			<th class="no-sort action no-search">Actions</th>
		</tr>
	</thead>
	<tbody>
	
	<?php
		foreach ($groupleave_list as $e_key){ ?>
		<tr>
			<td><?php echo $USER_GROUPS[$e_key->user_group]; ?></td>
			<td><?php echo $LEAVE_TYPES[$e_key->leave_type]; ?></td>
			<td><?php echo $e_key->forward_value.(($e_key->forward_type == "percent") ? "% (Maximum ".$e_key->max_value." Leaves)" : ""); ?></td>
			<td class="action">
				<a class="" href="<?php echo base_url();?>admin/leaves/edit_carry_forward_policy/<?php echo $e_key->id;?>">
					<i class="ti-pencil"></i>
				</a>
				<a class="" href="<?php echo base_url();?>admin/leaves/delete_carry_forward_policy/<?php echo $e_key->id;?>">
					<i class="ti-trash"></i>
				</a>
			</td>
		</tr>
	<?php } ?>
  </tbody>
</table>
