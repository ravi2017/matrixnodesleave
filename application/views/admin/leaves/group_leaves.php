<script type="text/javascript">
function show_confirm(act,gotoid)
{
if(act=="edit")
var r=confirm("Do you really want to edit?");
else
var r=confirm("Do you really want to delete?");
if (r==true)
{
window.location="<?php echo base_url();?>admin/states/"+act+"/"+gotoid;
}
}
</script>
<a class="btn btn-info btn-sm" href="<?php echo base_url();?>admin/leaves/add_group_leave">Add Group Leaves</a><br><br>
<div style="display:none;" class="alert alert-info"></div>
<table class="table table-striped table-bordered bootstrap-datatable datatable responsive customdatatable">
	<thead>
		<tr>
			<th>Group Names</th>
			<th>Leave Count</th>
			<th>Start Date</th>
			<th>End Date</th>
			<th class="no-sort action no-search">Actions</th>
		</tr>
	</thead>
	<tbody>
	
	<?php
		foreach ($groupleave_list as $e_key){ ?>
		<tr>
			<td><?php echo $USER_GROUPS[$e_key->user_group]; ?></td>
			<td><?php echo $e_key->leave_count; ?></td>
			<td><?php echo date('d-m-Y', strtotime($e_key->start_date)); ?></td>
			<td><?php echo ($e_key->end_date !='' ? date('d-m-Y', strtotime($e_key->end_date)) : ''); ?></td>
			<td class="action">
				<? if(strtotime($e_key->start_date) > time()) { ?>
				<a class="" href="<?php echo base_url();?>admin/leaves/edit_group_leave/<?php echo $e_key->id;?>">
					<i class="ti-pencil"></i>
				</a>
				<?	} ?>
				<a class="" href="<?php echo base_url();?>admin/leaves/delete_group_leave/<?php echo $e_key->id;?>">
					<i class="ti-trash"></i>
				</a>
			</td>
		</tr>
	<?php } ?>
  </tbody>
</table>
