<form name="frmcreditleave" id="frmcreditleave" action="" method="post">
	<div class="row">
     <?php //$states = get_states(); ?>
	  <!--<div class="form-group col-md-6">
		<select class="form-control" id="spark_state_id" name="spark_state_id">
		  <option value="">Select State</option>
		  <?php foreach($states as $state) { ?>
		  <option value="<?=$state->id?>"><?php echo $state->name?></option>
		  <?php } ?>
		</select>
	  </div> -->
	  <div class="form-group col-md-2">Spark</div>
	  <div class="form-group col-md-6">
		<select class="form-control select2 required" id="spark_id" name="spark_id">
		  <option value="">Select Spark</option>
		  <?php foreach($sparks as $spark) { ?>
		  <option value="<?=$spark->id?>"><?php echo $spark->name." (".$spark->login_id.")"?></option>
		  <?php } ?>
		</select>
	  </div>
     </div>
	 <!--<div class="row">
		 <div class="form-group col-md-2">Leave Date</div>
			<div class="form-group col-md-6">
				<input type="text" id="leavedate" name="leavedate" class="form-control" value="" placeholder="Enter Leave Date">
			</div>
    </div>-->
    <div class="row">
		<div class="form-group col-md-2">Leave Type</div>
		<div class="form-group col-md-6">
          <select class="form-control select2 required" id="leave_type" name="leave_type">
            <option value="">Leave Type</option>
            <?php foreach($LEAVE_TYPES as $key=>$value) { ?>
						<option value="<?=$key;?>"><?=$value?></option>	
						<? } ?>	
          </select>
        </div>
      </div>
    <div class="row">
		<div class="form-group col-md-2">Total Leave</div>  
		<div class="form-group col-md-6">
          <input type="text" id="leave_count" name="leave_count" class="form-control number required" value="" placeholder="Enter Number of leaves">
        </div>
      </div>
      <div class="row">
		<div class="form-group col-md-2">Description</div>  
		<div class="form-group col-md-6">
          <textarea name="description" id="description" class="form-control" placeholder="Enter Description"></textarea>
        </div>
      </div>
	  <div class="row">
		<div class="form-group col-md-8 text-right">
			<input type="submit" name="submit" value="Save" class="btn btn-sm btn-info">
		</div>
	 </div>
</form>

<script>
$(document).ready(function() {
   $("#frmcreditleave").validate({
			highlight: function (element, errorClass, validClass) {
					$(element).parents('.form-control').removeClass('has-success').addClass('has-error');     
			},
			unhighlight: function (element, errorClass, validClass) {
					$(element).parents('.form-control').removeClass('has-error').addClass('has-success');
			},
			errorPlacement: function (error, element) {
					if(element.hasClass('select2') && element.next('.select2-container').length) {
						error.insertAfter(element.next('.select2-container'));
					} else if (element.parent('.input-group').length) {
							error.insertAfter(element.parent());
					}
					else if (element.prop('type') === 'radio' && element.parent('.radio-inline').length) {
							error.insertAfter(element.parent().parent());
					}
					else if (element.prop('type') === 'checkbox' || element.prop('type') === 'radio') {
							error.appendTo(element.parent().parent());
					}
					else {
							error.insertAfter(element);
					}
			}
	});
});
	 	
/*$("#spark_state_id").change(function() {
  get_all_user_list($(this).val())
});

function get_all_user_list(ssid,sid = 0)
{
  $.ajax({
    url: '<?=base_url()?>dashboard/getSparkFromState',
    type: 'get',
    data: {
      state: ssid
    },
    dataType: 'json',
    success: function(response) {
      $("#spark_id").html('');
      $("#spark_id").html("<option value=''>Select Spark</option>");
      $.each(response, function() {
        if (sid > 0 && sid == this['id'])
          $("#spark_id").append("<option selected value='" + this['id']+"-"+ this['name'] +"-" +this['state_id']+"'>" + this['name'] + "</option>");
        else
          $("#spark_id").append("<option value='" + this['id']+"-"+ this['name'] +"-" +this['state_id']+"'>" + this['name'] + "</option>");
      });
      $("#spark_id").show();
    },
    error: function(response) {
      window.console.log(response);
    }
  });
}
	
$("#leavedate" ).datepicker({
	maxDate: "D",
	minDate: "-1M",
  dateFormat: "dd-mm-yy"
});
	
function validatereport()
{
  if ($("#spark_id").val() == "")
  {
    alert("Please select spark")
    return false;
  }
  if ($("#spark_id").val() == "")
  {
    alert("Please select spark")
    return false;
  }
  
  return true
}*/

</script>
