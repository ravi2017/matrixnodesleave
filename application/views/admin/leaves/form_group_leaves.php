<?
if (isset($groupleaves)){
	extract($groupleaves);
}
?>
<form action="<?php echo base_url();?>admin/leaves/<?=(isset($action)) ? $action : ''?>" method="post" accept-charset="utf-8" name="frmUserLeave" id="frmUserLeave">
	<div class="row">
			<div class="col-md-6 form-group">
			<label for="name">User Group</label><?php echo form_error('user_group'); ?>
			<select name="user_group" id="user_group" class="form-control required">
				<option value="">-Select Group-</option>
				<?
					foreach($USER_GROUPS as $key=>$value)	
					{
						$selected = ((isset($user_group) && $user_group == $key) ? 'selected' : '');
						?><option value="<?=$key?>" <?php echo $selected; ?>><?=$value;?></option><?
					}
				?>
			</select> 
			</div>
   </div>
   <div class="row">
    <div class="col-md-6 form-group">
			<label for="leave_count">Leave Count</label><?php echo form_error('leave_count'); ?>
			<input type="text" class="form-control required" id="leave_count" placeholder="Enter Leave Count" name="leave_count" value="<?=(isset($leave_count)) ? $leave_count : ''?>" minlength="1" maxlength="3">
    </div>
   </div>
   <div class="row">
    <div class="form-group col-md-6">
			<label for="start_from">Start From</label><?php echo form_error('start_from'); ?>  
			<input type="text" id="leavestart" name="leavestart" class="form-control required" value="<?=(isset($start_date)) ? date('d-m-Y', strtotime($start_date)) : ''?>">
	  </div>
	</div>
    
  </div>
  <div class="row">
    <div class="form-group col-md-6" style="text-align:center">
			<? if (isset($id)) { ?>
				<input type="hidden" name="id" value="<?php echo $id; ?>" />
			<? } ?>
			<button type="submit" class="btn btn-info">Submit</button>
			<a href="<?php echo base_url();?>admin/leaves/group_leaves" class="btn btn-default">Cancel</a>
		</div>
	</div>
</form>
<script>
  $(document).ready(function() {
    $("#frmUserLeave").validate();
    
    $("#leavestart" ).datepicker({
			minDate: "D",
			maxDate: "+2M",
			dateFormat: "dd-mm-yy"
		});
    
  });
  
</script>
