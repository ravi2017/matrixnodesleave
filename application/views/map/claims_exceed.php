<? if(!isset($download) && !isset($pdf)) {?>
<form name="frmactivities" method="post" action="" class="">
  <div class="row">
    <div class="col-md-4">
      <div class="form-group">
        <span>From Date</span>
        <input type="text" id="activitydatestart" name="activitydatestart" class="form-control" value="<? echo (!empty($activitydatestart) ? $activitydatestart : '');?>" autocomplete="off" placeholder="From Date">
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <span>To Date</span>
        <input type="text" id="activitydateend" name="activitydateend" class="form-control" value="<? echo (!empty($activitydateend) ? $activitydateend : '');?>" autocomplete="off" placeholder="To Date">
      </div>
    </div>
    <?
      $exceed_arr = array('10','15','20','30');
    ?>
    <div class="col-md-4">
      <div class="form-group">
        <span>Exceed Percent</span>
        <select name="exceed_value" id="exceed_value">
					<option value="">None</option>
          <? foreach($exceed_arr as $exceeds) { ?>
            <option value=<?=$exceeds?> <? echo ($exceeds == $exceed_value ? 'selected' : '')?>><?=$exceeds;?></option>
          <? } ?>
        </select>
      </div>
    </div>    
    <?php if ($current_role != "field_user") { ?>
    <div class="col-md-8">
    <?php echo $this->load->view('team_members/select_current_spark','',true) ?>
    </div>
    <?php } ?>
    <div class="col-md-4">
			<div class="form-group">
        <select name="travel_mode" id="travel_mode">
					<option value="">Travel Mode</option>
          <? foreach($travels_mode as $travel_mode) { ?>
            <option value=<?=$travel_mode->id?> <? echo ($travel_mode->id == $travel_mode_id ? 'selected' : '')?>><?=$travel_mode->travel_mode;?></option>
          <? } ?>
        </select>
      </div>
		</div>
		<?
			$claim_types = array(''=>'All', 'processed'=> 'Processed', 'unprocessed'=> 'Unprocessed', 'rejected'=> 'Rejected');
		?>	
		<div class="col-md-4">
			<div class="form-group">
				<select name="claim_type" id="claim_type">
					<?
						foreach($claim_types as $key=>$value){
							?><option value="<?=$key?>" <? echo ($key == $sel_claim_type ? 'selected' : '')?>><?=$value;?></option><?
						}
					?>
        </select>
      </div>
		</div>	
    <div class="col-md-4">
      <div class="form-group">
        <input type="submit" name="submit" class="form-control btn btn-sm btn-success" value="Get Exceed Claims List">
      </div>
    </div>
  </div>
</form>
<?php if(($current_role == "super_admin" || $current_role == "accounts")) { ?>
<div class="row">
    <div class="col-md-8">  
<form name="frmreport" action="<?php echo base_url();?>map/claims_import" method="post">
  <div class="row">
    <!--<div class="col-md-6 input-group date" id='datetimepicker1'>
      <input type="text" id="process_date" name="process_date" class="form-control" placeholder="Select Process Date">
    </div>-->
    <div class="col-md-6">
			<select name="process_date" id="process_date">
				<option value="">-Process Date-</option>
				<?php foreach($process_dates as $process_date) { ?>
					<option value="<?=$process_date->process_date?>"><?=$process_date->process_date?></option>
				<?	}	?>	
			</select>
		</div>	
    <div class="col-md-6">
      <input type="hidden" id="import_spark_state_id" name="spark_state_id" value="<?=$spark_state_id?>">
      <input type="hidden" name="tally_download" value="1">
      <input type="submit" id="stallyButton" name="tallyButton" value="Summary Tally Report" class="tallyButton btn btn-sm btn-info">
      <input type="submit" id="dtallyButton" name="tallyButton" value="Details Tally Report" class="tallyButton btn btn-sm btn-info">
    </div>
  </div>  
</form>
</div>
  <!--<form name="frmreport" action="<?php echo base_url();?>map/claims_import" method="post">
    <div class="col-md-3">
      <input type="hidden" id="import_activitydatestart" name="activitydatestart" value="<? echo (!empty($activitydatestart) ? $activitydatestart : '');?>">
      <input type="hidden" id="import_activitydateend" name="activitydateend" value="<? echo (!empty($activitydateend) ? $activitydateend : '');?>">
      <input type="hidden" id="import_spark_id" name="spark_id" value="<?=$spark_id?>">
      <input type="hidden" id="import_spark_state_id" name="spark_state_id" value="<?=$spark_state_id?>">
      <input type="hidden" name="tally_download" value="1">
      <input type="submit" id="tallyButton" name="tallyButton" value="Download Tally Report" class="btn btn-info">
    </div>
  </form>-->
<div class="col-md-4">   
<form name="frmreport" action="<?php echo base_url();?>map/auto_claims_process" method="post">
  <div class="row">
    <div class="col-md-3 form-group">
      <input type="hidden" id="process_activitydatestart" name="activitydatestart" value="<? echo (!empty($activitydatestart) ? $activitydatestart : '');?>">
      <input type="hidden" id="process_activitydateend" name="activitydateend" value="<? echo (!empty($activitydateend) ? $activitydateend : '');?>">
      <input type="hidden" id="process_spark_id" name="spark_id" value="<?=$spark_id?>">
      <input type="hidden" id="process_spark_state_id" name="spark_state_id" value="<?=$spark_state_id?>">
      <input type="submit" id="claimProcess" name="claimProcess" value="Process Claim" class="btn btn-info">
    </div>
  </div>
</form>
</div>
</div>
  
<?  } ?>

<? } ?>

<? if(!empty($claim_data)) { 
  $activity_date = ''; 
  
  if(!isset($download) && !isset($pdf))  { 
    ?><table class="table table-striped table-bordered bootstrap-datatable datatable1 responsive customdatatable"><?  
  }else{
    ?><table cellspacing=0 cellpadding=2 border=1 width="100%"><?
  }
  ?>
    <tr>
      <? /*if($spark_id > 0) { ?>
      <td colspan=5><strong>Spark Name :</strong> <? echo $spark_name;?></td>
      <?  }*/ ?>
      <td colspan=11><strong>Duration :</strong> <?=$activitydatestart?> To <?=$activitydateend?></td>
    </tr>
  </table>  
<div class="scrollme">  
  <?
  if(!isset($download) && !isset($pdf))  { 
    ?><table class="table table-striped table-bordered bootstrap-datatable datatable1 responsive customdatatable"><?  
  }else{
    ?><table cellspacing=0 cellpadding=2 border=1 width="100%"><?
  }
  ?>
	<thead>
		<tr>
      <th align="center">S.No.</th>
      <? if(!empty($spark_names)) { ?>
        <th align="center">Spark Name</th>
      <? } ?>  
			<th align="center">Activity</th>
			<!--<th align="center">Details</th>-->
			<th align="center">Date<br>(DD:MM:YY)</th>
			<th align="center">Time <br>(HH:MM)</th>
			<th align="center">Duration <br> (HH:MM)</th>
			<th align="center">Distance <br>(KM)</th>
			<th align="center">Manual <br>Distance (KM)</th>
			<th align="center">Travel Mode</th>
			<th align="center">Actual Travel <br>Cost</th>
			<th align="center">Manual Travel <br>Cost</th>
			<th align="center">Remark</th>
			<th align="center"><?php if($current_role == 'accounts') { echo 'Action'; }else{ echo 'Account Status'; } ?></th>
		</tr>
	</thead>
	<tbody>
	<? $state_count = 0; 
     $k=1; 
     $total_travel_distance = 0;
     $total_manual_distance = 0;
     $total_travel_cost = 0;
     $total_actual_travel_cost = 0;
  ?>
	<?php foreach ($claim_data as $e_key){
    
    if($e_key->travel_mode_id != '9999'){ 
      $record_id = $e_key->record_id;
      $sel_travel_mode = '';  
      $actual_travel_cost = 0;
      $is_autocalculated = 0;
      
      foreach($travels_mode as $travel_mode) {
          if($e_key->travel_mode_id == $travel_mode->id){ 
            $sel_travel_mode =  $travel_mode->travel_mode; 
            $actual_travel_cost =  $e_key->travel_distance*$travel_mode->travel_cost;
            //$actual_travel_cost =  ($e_key->travel_distance > 0 ? $e_key->travel_distance*$travel_mode->travel_cost : $travel_mode->travel_cost);
            $is_autocalculated = $travel_mode->is_autocalculated;
          } 
      }  
      
      if($activity_date != date('d-m-Y', strtotime($e_key->tracking_time))){
        $activity_date = date('d-m-Y', strtotime($e_key->tracking_time));
        
        $tacking_data = get_tracking_data($e_key->track_source_id);
        
        $activity_location = trim(strip_tags($tacking_data['tracking_location']));
        $exp_loc = implode(', ', explode(',',$activity_location));
      }
      
      $activity_type = get_activity_type($e_key->activity_type);
      
      $duration = '--';
      if(strtolower($e_key->activity_type) != 'attendance-out' && $e_key->activity_id != 0){
        $activity_data = get_activity_data($e_key->activity_id, $e_key->activity_type);
        $duration = $activity_data['duration'];
      }  
      
      $activity_location = trim(strip_tags($e_key->tracking_location));
      $exp_loc = implode(', ', explode(',',$activity_location));
      
      //$new_cost = $actual_travel_cost + ((CLAIM_EXCEED_PERCENT / 100) * $actual_travel_cost);
      if($exceed_value != '')
				$new_cost = $actual_travel_cost + (($exceed_value / 100) * $actual_travel_cost);
      else
				$new_cost = $e_key->travel_cost;
				
      $exceed = '';
      //echo"<br>".$e_key->travel_cost." >> ".$new_cost." >> ".$exceed_value;
      if($exceed_value != ''){
      if($e_key->travel_cost > $new_cost && $is_autocalculated == 1){
        
        $total_travel_distance = $e_key->travel_distance + $total_travel_distance;
        $total_manual_distance = $e_key->manual_distance + $total_manual_distance;
        $total_travel_cost = $e_key->travel_cost + $total_travel_cost;
        $total_actual_travel_cost = $actual_travel_cost + $total_actual_travel_cost;
        
        ?>
        <tr>
          <td align="center"><?=$k?></td>
          <? if(!empty($spark_names)) { ?>
            <td align="center"><?=$spark_names[$e_key->spark_id]?></td>
          <? } ?>  
          <td><?php echo $activity_type; ?></td>
          <!--<td><?php echo $exp_loc; ?></td>-->
          <td><?php echo date('d-m-Y', strtotime($e_key->tracking_time)); ?></td>
          <td><?php echo date('H:i', strtotime($e_key->tracking_time)); ?></td>
          <td><?=$duration;?></td>
          <td align="right">
            <?php echo round($e_key->travel_distance); ?></td>
          <td align="right">
            <?=(!empty($e_key->manual_distance) ? $e_key->manual_distance : round($e_key->travel_distance));?>
          </td>
          <td><?=$sel_travel_mode;?></td>
          <td align="right"><?=$actual_travel_cost;?></td>
          <td align="right"><?=round($e_key->travel_cost);?></td>
          <td><?=$e_key->remarks;?></td>
          <td>
        <?php 
        if($e_key->is_claimed == 1) {  
						$process_date = (!empty($e_key->process_date) ? date('d-m-Y', strtotime($e_key->process_date)) : '');
						$user_data = get_spark_data($e_key->process_by);
						//"(".$process_date.",By ".$user_data['name'].")";
            ?><span id="rreason_<?=$record_id?>">Processed</span><?
        }
        else{
        if($current_role == 'accounts') {  
          $btn_reject = 'display:block';
          $btn_approve = 'display:none';
          $reject_reason = '';
          if($e_key->claim_discard == 1){ 
            $btn_reject = 'display:none';
            $btn_approve = 'display:block';
            $reject_reason = $e_key->discard_reason;
          } 
          ?>  
          <div id="approved_<?=$record_id?>" style="<?=$btn_approve?>">
            <input type="button" name="approved_claim" id="approved_claim" onClick="return approvedclaim('<?=$record_id?>')" class="btn btn-sm btn-success" value="Approve"> 
            <br /><span id="rreason_<?=$record_id?>"><?=$reject_reason;?></span>
          </div>
          <div id="discard_<?=$record_id?>" style="<?=$btn_reject?>">
            <input type="button" name="reject_claim" id="reject_claim" onClick="return discard_claim('<?=$record_id?>')" class="btn btn-sm btn-danger" value="Reject"> 
          </div>
        <? }else{ ?>  
          <span id="rreason_<?=$record_id?>"><?=$e_key->discard_reason;?></span>
        <?  } ?>  
        <? } ?>  
      </td>
        </tr>
    <?php $k++; 
        } 
       }
       else{

        $total_travel_distance = $e_key->travel_distance + $total_travel_distance;
        $total_manual_distance = $e_key->manual_distance + $total_manual_distance;
        $total_travel_cost = $e_key->travel_cost + $total_travel_cost;
        $total_actual_travel_cost = $actual_travel_cost + $total_actual_travel_cost;
        
        ?>
        <tr>
          <td align="center"><?=$k?></td>
          <? if(!empty($spark_names)) { ?>
            <td align="center"><?=$spark_names[$e_key->spark_id]?></td>
          <? } ?>  
          <td><?php echo $activity_type; ?></td>
         <!-- <td><?php echo $exp_loc; ?></td>-->
          <td><?php echo date('d-m-Y', strtotime($e_key->tracking_time)); ?></td>
          <td><?php echo date('H:i', strtotime($e_key->tracking_time)); ?></td>
          <td><?=$duration;?></td>
          <td align="right">
            <?php echo round($e_key->travel_distance); ?></td>
          <td align="right">
            <?=(!empty($e_key->manual_distance) ? $e_key->manual_distance : round($e_key->travel_distance));?>
          </td>
          <td><?=$sel_travel_mode;?></td>
          <td align="right"><?=$actual_travel_cost;?></td>
          <td align="right"><?=round($e_key->travel_cost);?></td>
          <td><?=$e_key->remarks;?></td>
          <td>
        <?php 
        if($e_key->is_claimed == 1) {  
						$process_date = (!empty($e_key->process_date) ? date('d-m-Y', strtotime($e_key->process_date)) : '');
						$user_data = get_spark_data($e_key->process_by);
						//"(".$process_date.",By ".$user_data['name'].")";
            ?><span id="rreason_<?=$record_id?>">Processed</span><?
        }
        else{
        if($current_role == 'accounts') {  
          $btn_reject = 'display:block';
          $btn_approve = 'display:none';
          $reject_reason = '';
          if($e_key->claim_discard == 1){ 
            $btn_reject = 'display:none';
            $btn_approve = 'display:block';
            $reject_reason = $e_key->discard_reason;
          } 
          ?>  
          <div id="approved_<?=$record_id?>" style="<?=$btn_approve?>">
            <input type="button" name="approved_claim" id="approved_claim" onClick="return approvedclaim('<?=$record_id?>')" class="btn btn-sm btn-success" value="Approve"> 
            <br /><span id="rreason_<?=$record_id?>"><?=$reject_reason;?></span>
          </div>
          <div id="discard_<?=$record_id?>" style="<?=$btn_reject?>">
            <input type="button" name="reject_claim" id="reject_claim" onClick="return discard_claim('<?=$record_id?>')" class="btn btn-sm btn-danger" value="Reject"> 
          </div>
        <? }else{ ?>  
          <span id="rreason_<?=$record_id?>"><?=$e_key->discard_reason;?></span>
        <?  } ?>  
        <? } ?>  
      </td>
        </tr>
			<?php $k++; 
			 } 
        
    }
  
  } ?>
    <tr>
      <td colspan=5></td>
      <td align="right"><?=$total_travel_distance;?></td>
      <td align="right"><?=$total_manual_distance;?></td>
      <td>&nbsp;</td>
      <td align="right"><?=$total_actual_travel_cost;?></td>
      <td align="right"><?=$total_travel_cost;?></td>
      <td>&nbsp;</td>
    </tr>
  </tbody>
</table>  
</div> 
<? } ?>


<? if(!isset($download) && !isset($pdf) && !empty($claim_data))  { ?>
  <br />
    <div class="form-group row">
      <? if($current_role == 'super_admin' || $current_role == 'accounts') { ?>
      <div class="form-group col-md-3">
      <form name="frmreport" action="<?php echo base_url();?>map/claims_exceed" method="post">
          <input type="hidden" name="activitydatestart" value="<? echo (!empty($activitydatestart) ? $activitydatestart : '');?>">
          <input type="hidden" name="activitydateend" value="<? echo (!empty($activitydateend) ? $activitydateend : '');?>">
          <input type="hidden" name="spark_id" value="<?=$sel_spark_id?>">
          <input type="hidden" name="spark_state_id" value="<?=$spark_state_id?>">
          <input type="hidden" name="exceed_value" value="<?=$exceed_value?>">
          <input type="hidden" name="travel_mode" value="<?=$travel_mode_id?>">
          <input type="hidden" name="claim_type" value="<?=$sel_claim_type?>">
          <input type="hidden" name="download" value="1">
          <button type="submit" name="submit" value="Get Claims List" class="btn btn-success">Download Report</button>
      </form>
      </div>
      <?  } ?>
      <div class="form-group col-md-3">
      <form name="frmreport" action="<?php echo base_url();?>map/claims_exceed" method="post">
          <input type="hidden" name="activitydatestart" value="<? echo (!empty($activitydatestart) ? $activitydatestart : '');?>">
          <input type="hidden" name="activitydateend" value="<? echo (!empty($activitydateend) ? $activitydateend : '');?>">
          <input type="hidden" name="spark_id" value="<?=$sel_spark_id?>">
          <input type="hidden" name="spark_state_id" value="<?=$spark_state_id?>">
          <input type="hidden" name="exceed_value" value="<?=$exceed_value?>">
          <input type="hidden" name="travel_mode" value="<?=$travel_mode_id?>">
          <input type="hidden" name="claim_type" value="<?=$sel_claim_type?>">
          <input type="hidden" name="pdf" value="1">
        <button type="submit" name="submit" value="Get Claims List" class="btn btn-success">Download PDF</button>
      </form>
      </div>
    </div>
    
    
<div class="modal fade" id="discardModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4>Reject Claim</h4>
        <button type="button" class="close" data-dismiss="modal">×</button>
      </div>
      <form method="post" action="" role="form" name="frmdiscard" id="frmdiscard">
        <input type="hidden" name="claimid" id="claimid" value="">
        <div class="modal-body">                      
          <div class="row">
            <div class="form-group col-md-12">
              <textarea name="discard_reason" id="discard_reason" placeholder="Reason to reject" class="form-control" maxlength="200"></textarea>
            </div>
          </div>
          <div class="row">  
            <div class="form-group col-md-3">&nbsp;</div>
            <div class="form-group col-md-6">
              <label for="brc_name">&nbsp;</label>
              <input type="button" class="form-control" id="submit" name="submit" value="Save" class="btn btn-success" onClick="return save_discard_claim()">
            </div>  
          </div>  
        </div>  
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="approvedModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4>Approve Claim</h4>
        <button type="button" class="close" data-dismiss="modal">×</button>
      </div>
      <form method="post" action="" role="form" name="frmapproved" id="frmapproved">
        <input type="hidden" name="claimid" id="claimid" value="">
        <div class="modal-body">                      
          <div class="row">
            <div class="form-group col-md-12">
              <textarea name="approved_reason" id="approved_reason" placeholder="Reason to approved" class="form-control" maxlength="200"></textarea>
            </div>
          </div>
          <div class="row">  
            <div class="form-group col-md-3">&nbsp;</div>
            <div class="form-group col-md-6">
              <label for="brc_name">&nbsp;</label>
              <input type="button" class="form-control" id="submit" name="submit" value="Save" class="btn btn-success" onClick="return save_approved_claim()">
            </div>  
          </div>  
        </div>  
      </form>
    </div>
  </div>
</div>

<? } ?>


<? if(!isset($download) && !isset($pdf)) {?>

<script>
$(document).ready(function() {
    //$("#frmClaim").validate();
});

/*$('#datetimepicker1 #process_date').datetimepicker({
  timepicker: false,
  autoclose: true,
  format: "d-m-Y",
  maxDate:new Date()
});*/

$('.tallyButton').click( function() {
  if($('#process_date').val() == '')
  {
    alert('Please select process date');
    return false
  }
  else if($('#spark_state_id').val() == '')
  {
    alert('Please select state');
    return false
  }
  $("#import_spark_state_id").val($('#spark_state_id').val());
});

$('#claimProcess').click( function() {
  
  if($('#activitydatestart').val() == '')
  {
    alert('Please select from date');
    return false
  }
  else if($('#activitydateend').val() == '')
  {
    alert('Please select to date');
    return false
  }
  else if($('#spark_state_id').val() == '')
  {
    alert('Please select state');
    return false
  }
  if(!confirm('Are you sure to process the claim')){
    return false;
  }
  $("#process_activitydatestart").val($('#activitydatestart').val());
  $("#process_activitydateend").val($('#activitydateend').val());
  $("#process_spark_id").val($('#spark_id').val());
  $("#process_spark_state_id").val($('#spark_state_id').val());
});



function discard_claim(cid)
{
  $("#discard_reason").val('');
  $("#claimid").val(cid);
  $("#discardModel").modal("show");
}

function save_discard_claim()
{
  var claim_id = $("#claimid").val();
  var reason = $("#discard_reason").val();
  
  if(reason == ''){
    alert('Please add reason');
    return false;
  }
  
  $.ajax({
      url: '<?=base_url()?>map/discard_claim',
      type: 'post',
      data: {
        claim_id: claim_id,
        reason : reason
      },
      success: function(response) {
        $("#discard_"+claim_id).hide();
        $("#approved_"+claim_id).show();
        $("#discardModel").modal("hide");
        $("#rreason_"+claim_id).html(reason);
        $(".claim_discard_"+claim_id).val('1');
        $("#row_"+claim_id).css("background-color", "#fd6252");
      },
      error: function(response) {
        window.console.log(response);
      }
    });
}


function approvedclaim(cid)
{
  $("#approved_reason").val('');
  $("#claimid").val(cid);
  $("#approvedModel").modal("show");
}

function save_approved_claim()
{
  var claim_id = $("#claimid").val();
  var reason = $("#approved_reason").val();
  if(reason == ''){
    alert('Please add reason');
    return false;
  }
  $.ajax({
      url: '<?=base_url()?>map/approved_claim',
      type: 'post',
      data: {
        claim_id: claim_id,
        reason : reason
      },
      success: function(response) {
        $("#approvedModel").modal("hide");
        $("#discard_"+claim_id).show();
        $("#approved_"+claim_id).hide();
        $(".claim_discard_"+claim_id).val('0');
        $("#row_"+claim_id).css("background-color", "#FFFFFF");
      },
      error: function(response) {
        window.console.log(response);
      }
    });
}


</script>
<? } ?>
