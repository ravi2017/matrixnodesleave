<form name="frmactivities" method="post" action="" class="">
  <div class="row">
    <div class="col-md-3">
      <div class="form-group">
        <input type="text" id="activitydatestart" name="activitydatestart" class="form-control" value="<? echo (!empty($activitydatestart) ? $activitydatestart : '');?>" autocomplete="off">
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <input type="text" id="activitydateend" name="activitydateend" class="form-control" value="<? echo (!empty($activitydateend) ? $activitydateend : '');?>" autocomplete="off">
      </div>
    </div>
    <?php if ($current_role != "field_user") { ?>
    <div class="col-md-6">
    <?php echo $this->load->view('team_members/select_current_spark','',true) ?>
    </div>
    <?php } ?>
    <div class="col-md-3">
      <div class="form-group">
        <input type="submit" name="submit" class="form-control btn btn-sm btn-success" value="Get Claims List">
      </div>
    </div>
  </div>
</form>
