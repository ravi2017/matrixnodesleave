<? if(!isset($download) && !isset($pdf)) {?>
<form name="frmactivities" method="post" action="" class="">
  <div class="row">
    <div class="col-md-4">
      <div class="form-group">
        <span>From Date</span>
        <input type="text" id="activitydatestart" name="activitydatestart" class="form-control" value="<? echo (!empty($activitydatestart) ? $activitydatestart : '');?>" autocomplete="off" placeholder="From Date">
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <span>To Date</span>
        <input type="text" id="activitydateend" name="activitydateend" class="form-control" value="<? echo (!empty($activitydateend) ? $activitydateend : '');?>" autocomplete="off" placeholder="To Date">
      </div>
      <input type="hidden" name="exceed_value" id="exceed_value" value="">
    </div>
     <?
      $exceed_arr = array('10','15','20','30');
    ?>
    <!--<div class="col-md-4">
      <div class="form-group">
        <span>Exceed Percent</span>
        <select name="exceed_value" id="exceed_value">
					<option value="">None</option>
          <? foreach($exceed_arr as $exceeds) { ?>
            <option value=<?=$exceeds?> <? echo ($exceeds == $exceed_value ? 'selected' : '')?>><?=$exceeds;?></option>
          <? } ?>
        </select>
      </div>
    </div>-->
    <?
			$claim_types = array(''=>'All', 'processed'=> 'Processed', 'unprocessed'=> 'Unprocessed', 'rejected'=> 'Rejected', 'referback'=> 'Refer Back');
		?>	
		<div class="col-md-4">
			<span>Claim Status</span>
			<div class="form-group">
				<select name="claim_type" id="claim_type">
					<?
						foreach($claim_types as $key=>$value){
							?><option value="<?=$key?>" <? echo ($key == $sel_claim_type ? 'selected' : '')?>><?=$value;?></option><?
						}
					?>
        </select>
      </div>
		</div>
   
    <?php if ($current_role != "field_user") { ?>
    <div class="col-md-8">
    <?php echo $this->load->view('team_members/select_current_spark','',true) ?>
    </div>
    <?php } ?>
   	
    <div class="col-md-4">
      <div class="form-group">
        <input type="submit" name="submit" class="form-control btn btn-sm btn-success" value="Get Exceed Claims List">
      </div>
    </div>
  </div>
</form>
<?php if(($current_role == "super_admin" || $current_role == "accounts")) { ?>
<div class="row">
    <div class="col-md-8">  
<form name="frmreport" action="<?php echo base_url();?>map/stay_claims_import" method="post">
  <div class="row">
    <div class="col-md-6">
			<select name="process_date" id="process_date">
				<option value="">-Process Date-</option>
				<?php foreach($process_dates as $process_date) { ?>
					<option value="<?=$process_date->process_date?>"><?=$process_date->process_date?></option>
				<?	}	?>	
			</select>
		</div>	
    <div class="col-md-6">
      <input type="hidden" id="import_spark_state_id" name="spark_state_id" value="<?=$spark_state_id?>">
      <input type="hidden" name="tally_download" value="1">
      <input type="submit" id="stallyButton" name="tallyButton" value="Summary Tally Report" class="tallyButton btn btn-sm btn-info">
      <input type="submit" id="dtallyButton" name="tallyButton" value="Details Tally Report" class="tallyButton btn btn-sm btn-info">
    </div>
  </div>  
</form>
</div>

<div class="col-md-4">   
<form name="frmreport" action="<?php echo base_url();?>map/auto_stay_claims_process" method="post">
  <div class="row">
    <div class="col-md-3 form-group">
      <input type="hidden" id="process_activitydatestart" name="activitydatestart" value="<? echo (!empty($activitydatestart) ? $activitydatestart : '');?>">
      <input type="hidden" id="process_activitydateend" name="activitydateend" value="<? echo (!empty($activitydateend) ? $activitydateend : '');?>">
      <input type="hidden" id="process_spark_id" name="spark_id" value="<?=$spark_id?>">
      <input type="hidden" id="process_spark_state_id" name="spark_state_id" value="<?=$spark_state_id?>">
      <input type="submit" id="claimProcess" name="claimProcess" value="Process Claim" class="btn btn-info">
    </div>
  </div>
</form>
</div>
</div>
  
<?  } ?>

<? } ?>

<? if(!empty($claim_data)) { 
  $activity_date = ''; 
  
  if(!isset($download) && !isset($pdf))  { 
    ?><table class="table table-striped table-bordered bootstrap-datatable datatable1 responsive customdatatable"><?  
  }else{
    ?><table cellspacing=0 cellpadding=2 border=1 width="100%"><?
  }
  ?>
    <tr>
      <td colspan=11><strong>Duration :</strong> <?=$activitydatestart?> To <?=$activitydateend?></td>
    </tr>
  </table>  
<div class="scrollme">  
  <?
  if(!isset($download) && !isset($pdf))  { 
    ?><table class="table table-striped table-bordered bootstrap-datatable datatable1 responsive customdatatable"><?  
  }else{
    ?><table cellspacing=0 cellpadding=2 border=1 width="100%"><?
  }
  ?>
	<thead>
		<tr>
      <th align="center">S.No.</th>
      <? if(!empty($spark_names)) { ?>
        <th align="center">Spark Name</th>
      <? } ?>  
			<th align="center">Stay Reason</th>
			<th align="center">Duration</th>
			<th align="center">Bill Number</th>
			<th align="center">Bill Date</th>
			<th align="center">Bill Cost</th>
			<th align="center">Claim Cost</th>
			<th align="center">Remark</th>
			<th align="center"><?php if($current_role == 'accounts') { echo 'Action'; }else{ echo 'Account Status'; } ?></th>
		</tr>
	</thead>
	<tbody>

	<?php 
		$k=1; 
		$net_manual_cost = 0;
    $net_claim_cost = 0;
        
		foreach ($claim_data as $e_key){
    
      $record_id = $e_key->id;
      
      $actual_cost = $e_key->actual_cost;
      if($exceed_value != '')
				$new_cost = $actual_cost + (($exceed_value / 100) * $actual_cost);
      else
				$new_cost = $e_key->manual_cost;
				
			$stay_activity = json_decode($e_key->stay_reason);
			$stay_with = json_decode($e_key->paid_spark_id);
			$stay_spark_names = array();
			if(!empty($stay_with))
			{
				foreach($stay_with as $stays)
				{
					$stay_spark_names[] = $spark_with_names[$stays];	
				}
			}	
			
			if(strtotime($e_key->from_date) != strtotime($e_key->to_date))
				$claim_duration = date('d-m-Y', strtotime($e_key->from_date))." to ".date('d-m-Y', strtotime($e_key->to_date));
			else	
				$claim_duration = date('d-m-Y', strtotime($e_key->from_date));
				
      $exceed = '';
      //echo"<br>".$e_key->travel_cost." >> ".$new_cost." >> ".$exceed_value;
      if($e_key->file_name == '' && $e_key->claim_amount !=0) {  
					// not display
			}
			else{
      if($exceed_value != ''){
      if($e_key->manual_cost > $new_cost){
				$net_manual_cost = $e_key->manual_cost + $net_manual_cost;
        $net_claim_cost = $e_key->claim_amount + $net_claim_cost;
        ?>
        <tr>
          <td align="center"><?=$k?></td>
          <? if(!empty($spark_names)) { ?>
            <td align="center"><?=$spark_names[$e_key->spark_id]?></td>
          <? } ?>  
          <td><?php echo implode(', ', $stay_activity); ?></td>
          <td><?php echo $claim_duration; ?></td>
          <td><?php echo $e_key->bill_number; ?></td>
          <td><?php echo $e_key->bill_date; ?></td>
          <td align="right"><?=round($e_key->actual_cost);?></td>
          <td align="right"><?=round($e_key->manual_cost);?></td>
          <td><?=$e_key->comment;?></td>
          <td>
        <?php 
        if($e_key->claim_status == 'processed') {  
						$process_date = (!empty($e_key->process_date) ? date('d-m-Y', strtotime($e_key->process_date)) : '');
						$user_data = get_spark_data($e_key->updated_by);
						?><span id="rreason_<?=$record_id?>">Processed <?=$process_date." (By ".$user_data['login_id'].")";?></span><?
        }
        else{
        if($current_role == 'accounts') {  
          $btn_reject = 'display:block';
          $btn_referbk = 'display:block';
          
          if($e_key->claim_status == 'rejected' || $e_key->claim_status == 'referback'){ 
            $btn_reject = 'display:none';
            $btn_referbk = 'display:none';
          } 
          ?>  
          <div id="referback_<?=$record_id?>" style="<?=$btn_referbk?>">
            <input type="button" name="referback_claim" id="referback_claim" onClick="return approvedclaim('<?=$record_id?>', 'referback', 'Refer Back', 'stay_claims')" class="btn btn-sm btn-success" value="Refer Back"> 
          </div>
          <div id="discard_<?=$record_id?>" style="<?=$btn_reject?>">
						<br />
            <input type="button" name="reject_claim" id="reject_claim" onClick="return approvedclaim('<?=$record_id?>', 'rejected', 'Reject', 'stay_claims')" class="btn btn-sm btn-danger" value="Reject"> 
          </div>
        <? } ?>  
          <div><span id="rreason_<?=$record_id?>"><?=$e_key->status_remark;?></span></div>
        <? } ?>  
      </td>
        </tr>
    <?php $k++; 
        } 
       }
       else{

        $net_manual_cost = $e_key->manual_cost + $net_manual_cost;
        $net_claim_cost = $e_key->claim_amount + $net_claim_cost;
        ?>
        <tr>
          <td align="center"><?=$k?></td>
          <? if(!empty($spark_names)) { ?>
            <td align="center"><?=$spark_names[$e_key->spark_id]?></td>
          <? } ?>  
          <td><?php echo implode(', ', $stay_activity); ?></td>
          <td><?php echo $claim_duration; ?></td>
          <td><?php echo $e_key->bill_number; ?></td>
          <td><?php echo $e_key->bill_date; ?></td>
          <td align="right"><?=round($e_key->manual_cost);?></td>
          <td align="right"><?=round($e_key->claim_amount);?></td>
          <td><?=$e_key->comment;?></td>
          <td>
        <?php 
        if($e_key->claim_status == 'processed') {  
						$process_date = (!empty($e_key->process_date) ? date('d-m-Y', strtotime($e_key->process_date)) : '');
						$user_data = get_spark_data($e_key->updated_by);
						?><span id="rreason_<?=$record_id?>">Processed <?=$process_date." (By ".$user_data['login_id'].")";?></span><?
        }
        else{
        if($current_role == 'accounts') {  
          $btn_reject = 'display:block';
          $btn_referbk = 'display:block';
          
          if($e_key->claim_status == 'rejected' || $e_key->claim_status == 'referback'){ 
            $btn_reject = 'display:none';
            $btn_referbk = 'display:none';
          } 
          ?>  
          <div id="referback_<?=$record_id?>" style="<?=$btn_referbk?>">
            <input type="button" name="referback_claim" id="referback_claim" onClick="return approvedclaim('<?=$record_id?>', 'referback', 'Refer Back', 'stay_claims')" class="btn btn-sm btn-success" value="Refer Back"> 
          </div>
          <div id="discard_<?=$record_id?>" style="<?=$btn_reject?>">
						<br />
            <input type="button" name="reject_claim" id="reject_claim" onClick="return approvedclaim('<?=$record_id?>', 'rejected', 'Reject', 'stay_claims')" class="btn btn-sm btn-danger" value="Reject"> 
          </div>
        <? } ?>  
          <div><span id="rreason_<?=$record_id?>"><?=$e_key->status_remark;?></span></div>
        <? } ?>  
      </td>
        </tr>
			<?php $k++; 
			 } 
		}
  } ?>
    <tr>
			<? if(!empty($spark_names)) { ?>
      <td colspan=6></td>
      <? }else{ ?>
				<td colspan=5></td>
			<?	}	?>	
      <td align="right"><?=$net_manual_cost;?></td>
      <td align="right"><?=$net_claim_cost;?></td>
      <td>&nbsp;</td>
    </tr>
  </tbody>
</table>  
</div> 
<? } ?>


<? if(!isset($download) && !isset($pdf) && !empty($claim_data))  { ?>
  <br />
    <div class="form-group row">
      <? if($current_role == 'super_admin' || $current_role == 'accounts') { ?>
      <div class="form-group col-md-3">
      <form name="frmreport" action="<?php echo base_url();?>map/stay_claims_exceed" method="post">
          <input type="hidden" name="activitydatestart" value="<? echo (!empty($activitydatestart) ? $activitydatestart : '');?>">
          <input type="hidden" name="activitydateend" value="<? echo (!empty($activitydateend) ? $activitydateend : '');?>">
          <input type="hidden" name="spark_id" value="<?=$sel_spark_id?>">
          <input type="hidden" name="spark_state_id" value="<?=$spark_state_id?>">
          <input type="hidden" name="exceed_value" value="<?=$exceed_value?>">
          <input type="hidden" name="claim_type" value="<?=$sel_claim_type?>">
          <input type="hidden" name="download" value="1">
          <button type="submit" name="submit" value="Get Claims List" class="btn btn-success">Download Report</button>
      </form>
      </div>
      <?  } ?>
      <div class="form-group col-md-3">
      <form name="frmreport" action="<?php echo base_url();?>map/stay_claims_exceed" method="post">
          <input type="hidden" name="activitydatestart" value="<? echo (!empty($activitydatestart) ? $activitydatestart : '');?>">
          <input type="hidden" name="activitydateend" value="<? echo (!empty($activitydateend) ? $activitydateend : '');?>">
          <input type="hidden" name="spark_id" value="<?=$sel_spark_id?>">
          <input type="hidden" name="spark_state_id" value="<?=$spark_state_id?>">
          <input type="hidden" name="exceed_value" value="<?=$exceed_value?>">
          <input type="hidden" name="claim_type" value="<?=$sel_claim_type?>">
          <input type="hidden" name="pdf" value="1">
        <button type="submit" name="submit" value="Get Claims List" class="btn btn-success">Download PDF</button>
      </form>
      </div>
    </div>

<? } ?>


<? if(!isset($download) && !isset($pdf)) {
		
		include_once('claim_action.php');
	
	?>

<script>
$(document).ready(function() {
    //$("#frmClaim").validate();
});

$('.tallyButton').click( function() {
  if($('#process_date').val() == '')
  {
    alert('Please select process date');
    return false
  }
  else if($('#spark_state_id').val() == '')
  {
    alert('Please select state');
    return false
  }
  $("#import_spark_state_id").val($('#spark_state_id').val());
});

$('#claimProcess').click( function() {
  
  if($('#activitydatestart').val() == '')
  {
    alert('Please select from date');
    return false
  }
  else if($('#activitydateend').val() == '')
  {
    alert('Please select to date');
    return false
  }
  else if($('#spark_state_id').val() == '')
  {
    alert('Please select state');
    return false
  }
  if(!confirm('Are you sure to process the claim')){
    return false;
  }
  $("#process_activitydatestart").val($('#activitydatestart').val());
  $("#process_activitydateend").val($('#activitydateend').val());
  $("#process_spark_id").val($('#spark_id').val());
  $("#process_spark_state_id").val($('#spark_state_id').val());
});
</script>
<? } ?>
