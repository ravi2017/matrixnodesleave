<div class="modal fade" id="discard_Model" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4><span id="mainheading"></span> Claim</h4>
        <button type="button" class="close" data-dismiss="modal">×</button>
      </div>
      <form method="post" action="" role="form" name="frmdiscard" id="frmdiscard">
        <input type="hidden" name="claimid" id="claimid" value="">
        <input type="hidden" name="actiontype" id="actiontype" value="">
        <input type="hidden" name="claimtype" id="claimtype" value="">
        <div class="modal-body">                      
          <div class="row">
            <div class="form-group col-md-12">
              <textarea name="discardreason" id="discardreason" placeholder="Add Reason" class="form-control" maxlength="200"></textarea>
            </div>
          </div>
          <div class="row">  
            <div class="form-group col-md-3">&nbsp;</div>
            <div class="form-group col-md-6">
              <label for="brc_name">&nbsp;</label>
              <input type="button" class="form-control" id="submit" name="submit" value="Save" class="btn btn-success" onClick="return save_claim_action()">
            </div>  
          </div>  
        </div>  
      </form>
    </div>
  </div>
</div>
	
<script>
 
function approvedclaim(cid, actiontype, heading, claim_type)
{
  $("#discardreason").val('');
  $("#actiontype").val('');
  $("#mainheading").html('');
  $("#claimtype").val(claim_type);
  $("#actiontype").val(actiontype);
  $("#mainheading").html(heading);
  $("#claimid").val(cid);
  $("#discard_Model").modal("show");
}

function save_claim_action()
{
  var claim_id = $("#claimid").val();
  var reason = $("#discardreason").val();
  var action_type = $("#actiontype").val();
  var claim_type = $("#claimtype").val();
  
  if(reason == ''){
    alert('Please add reason');
    return false;
  }
  
  $.ajax({
      url: '<?=base_url()?>map/action_claim',
      type: 'post',
      data: {
				claim_id: claim_id,
        claim_type : claim_type,
        action_type : action_type,
        reason : reason
      },
      success: function(response) {
				$("#discard_"+claim_id).hide();
        $("#approved_"+claim_id).hide();
        $("#referback_"+claim_id).hide();
        if(claim_type == 'food_claims'){
					$("#cancel_"+claim_id).hide();
				}
        $("#discard_Model").modal("hide");
        $("#rreason_"+claim_id).html(response);
        $(".claim_discard_"+claim_id).val('1');
        if(action_type == 'rejected'){
					$("#row_"+claim_id).css("background-color", "#fd6252");
				}
      },
      error: function(response) {
        window.console.log(response);
      }
   });
}

</script>
