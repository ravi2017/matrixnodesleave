<div class="row">
  <div class="col-md-12">
    <form method="post" action="" role="form" name="frmstayclaims" id="frmstayclaims">
      <div class="row">
        <label class="col-md-2" for="from_date">From Date</label>
        <div class="form-group col-md-3">
          <div class='input-group date' id='datetimepicker1'>
            <span class="input-group-addon">
              <span class="icon-calendar"></span>
            </span>
            <input type="text" name="sdate" value="<? echo (isset($from_date) ? date('d-m-Y', strtotime($from_date)) : ''); ?>" class="form-control required change_date" id="sdate" autocomplete="off" />
          </div>
        </div>
      
        <label class="col-md-2" for="end_date">End Date</label>
        <div class="form-group col-md-3">
          <div class='input-group date' id='datetimepicker2'>
						<span class="input-group-addon">
              <span class="icon-calendar"></span>
            </span>
            <input type="text" name="edate" value="<? echo (isset($to_date) ? date('d-m-Y', strtotime($to_date)) : ''); ?>" class="form-control endDate required change_date" id="edate" autocomplete="off" />
          </div>
        </div>
        <div class="col-md-2">
          <input type="submit" class="btn btn-sm btn-success f-left" name="Submit" value="Get Stay Activity" />
        </div>
      </div>
   </form>   
	</div>
</div>

<? if($error == 2) { ?>
			<div class="row">   
				<div class="col-md-12 text-center text-bold">
					No stay claim found for selected duration.
				</div>
			</div>		
		<?
	}
	else if($error == 1) { 
		?>
			<div class="row">   
				<div class="col-md-12 text-center text-bold">
					Different sparks for stay claim selected duration.
				</div>
			</div>		
		<?
	}
	else{
		if(!empty($stay_list)){
	?>
<div class="row" id="stay_form">   
	<div class="col-md-12">
   <form method="post" action="" role="form" name="frmclaims" id="frmclaims"  enctype="multipart/form-data">
      <div class="row">
				
        <input type="hidden" name="from_date" value="<?=$from_date?>" class="required" />
        <input type="hidden" name="to_date" value="<?=$to_date?>" class="required" />
        
        <?	$s = 0; 
						$bill_amount = 0;
						$claim_amount = 0;
						$stay_with_count = count($stay_with);
					
					foreach($stay_list as $staylist) { 
						
						//$from_address = implode(', <br>', explode(',', $staylist->from_address));
						$from_address = $staylist->from_address;
						
						//$stay_sparks = json_decode($staylist->stay_with_sparks);
						
						if(strpos(strtolower($staylist->accommodation_type), 'friend') !== false){
							//$bill_amount = $bill_amount + STAY_WITH_FRIEND;
							$bill_amount = $bill_amount + STAY_WITH_FRIEND*($stay_with_count+1);
						}
						else{
							$bill_amount = $bill_amount + $staylist->manual_rent;
						}
						$claim_amount_spark = ($bill_amount > 0 ? round($bill_amount/($stay_with_count+1),2) : 0);
						$claim_amount = $claim_amount + $claim_amount_spark;
					?>
					<label class="col-md-2" for="selected_activity">
						<input type="hidden" name="stay_ids[]" value="<?=$staylist->id; ?>">
						<input type="hidden" name="manual_rent[<?=$staylist->id?>]" value="<?=$staylist->manual_rent; ?>">
						<input type="hidden" name="activity_date[<?=$staylist->id?>]" value="<?=$staylist->activity_date; ?>">
						<input type="hidden" name="accommodation_type[<?=$staylist->id?>]" value="<?=$staylist->accommodation_type; ?>">
						<input type="hidden" name="stay_reason[<?=$staylist->id?>]" value="<?=$staylist->stay_reason; ?>">
						<? if($s == 0) { echo	'Selected Activity'; } ?>
					</label>
					<div class="form-group col-md-10">
							<li><?= date('d-m-Y', strtotime($staylist->activity_date))." (".$from_address.") ,".$staylist->accommodation_type.", ".$staylist->stay_reason ; ?></li>  
					</div>
        <? $s++;	}	
					
        ?>
			</div>	
			
      <div class="row">
				<label class="col-md-2" for="bill_no">Bill No<span style="color:red">*</span></label>
        <div class="form-group col-md-3">
					<input type="hidden" class="form-control required" id="actual_claim_amount" value="<?=$claim_amount;?>">
					<input type="text" name="bill_number" value="" class="form-control required" id="bill_number" autocomplete="off" maxlength="40" />
        </div>

        <label class="col-md-2 offset-md-1" for="bill_date">Bill Date<span style="color:red">*</span></label>
        <div class="form-group col-md-3">
          <div class='input-group date' id='datetimepicker2'>
						<span class="input-group-addon">
              <span class="icon-calendar"></span>
            </span>
            <input type="text" name="bill_date" value="" class="form-control required" id="bill_date" autocomplete="off" />
          </div>
        </div>
      </div>
      
      <div class="row">
				<label class="col-md-2" for="phone_no">Phone No<span style="color:red">*</span></label>
        <div class="form-group col-md-3">
            <input type="text" name="phone_number" value="" class="form-control required" id="phone_number" autocomplete="off" maxlength="10" minlength="10" />
        </div>

        <label class="col-md-2 offset-md-1" for="manager_name">Paid To (Manager Name)<span style="color:red">*</span></label>
        <div class="form-group col-md-3">
					<input type="text" name="manager_name" value="" class="form-control required" id="manager_name" autocomplete="off" />
        </div>
      </div>
      
      <div class="row">
        <label class="col-md-2" for="amount">Bill Cost<span style="color:red">*</span></label>
        <div class="form-group col-md-3">
          <input type="text" name="amount" class="form-control required" id="amount" value="<?=$bill_amount;?>" readonly>
        </div>
        
        <label class="col-md-2 offset-md-1" for="amount">Claim Cost<span style="color:red">*</span></label>
        <div class="form-group col-md-3">
          <input type="text" name="claim_amount" class="form-control required" id="claim_amount" value="<?=$claim_amount;?>">
        </div>
      </div>
      
      <div class="row">		
				<label class="col-md-2" for="paid_for_sparks">
					Paid For Sparks
				</label>
				<div class="form-group col-md-3">
					<select name="paid_for_sparks[]" class="form-control basic-multiple select2" id="paid_for_sparks" multiple>
						<? foreach($sparks_list as $value){	?>
							<option value="<?=$value->id?>" <? if(in_array($value->id, $stay_with)){ echo 'selected="selected"'; }?>><?=$value->name; ?></option>  
						<?	} ?>	
					</select>
				</div>

        <label class="col-md-2 offset-md-1" for="upload_bill">Upload Bill</label>
        <div class="form-group col-md-3">
          <input type="file" name="file_name" class="form-control1" id="file_name" >(Allowed only .jpg,.gif,.png,.csv,.xls file type)
        </div>
      </div>
      
      <div class="form-group row">
				<div class="col-md-3">&nbsp;</div>
				<div class="col-md-3">
					<div class="form-group">
						<a href="<?php echo base_url();?>map/stay_claims_list" class="btn btn-sm btn-default f-right">Cancel</a>
					</div>
				</div>
        <!--<div class="col-md-6"><br>
          <a class="btn btn-sm btn-danger f-left" href="<?=base_url()?>activity">Cancel</a>
        </div>-->
        <div class="col-md-3">
          <input type="submit" class="btn btn-sm btn-success f-left" name="Submit" value="Save Claim" />
        </div>
        <div class="col-md-3">&nbsp;</div>
      </div>
      
    </form>
  </div>
</div>
<?	}	}?>
<style>
.error {
    border: 1px solid red !important;
}
.error:focus {
    border: 1px solid red !important;
}
.valid {
    border: 1px solid green !important;
}
select.error {
    border: 1px solid red !important;
}
label.error {
    color: #e74c3c;
    display:none !important;
}
</style>
<script type="text/javascript">
  $(document).ready(function ()
  {
		 $('#frmstayclaims').validate({
				errorPlacement: function (error, element) {
						return false;
				}
		 });
     
    $('#frmclaims').validate();
			 
    $('#sdate').datepicker({
			dateFormat: "dd-mm-yy"
		});	
		
		$('#edate').datepicker({
			dateFormat: "dd-mm-yy"
		});
		
		$('#bill_date').datepicker({
			dateFormat: "dd-mm-yy"
		});
		
		$('#paid_for_sparks').change(function() {
			$('#claim_amount').val('');
			//alert($("#paid_for_sparks :selected").length);
		});
		
		$('#claim_amount').change(function() {
			var spark_count = $("#paid_for_sparks :selected").length;
			var total_spark = spark_count+1;
			var bill_amount = $("#amount").val();
			var cur_amount = $(this).val();
			//alert(total_spark+' >> '+bill_amount);
			var actual_claim_amount = parseFloat(bill_amount/total_spark);
			//alert(cur_amount+' >> '+actual_claim_amount);
			//$("#actual_claim_amount").val()
				if(cur_amount > actual_claim_amount){
					alert('Claim amount exceed, please enter valid amount.');
					$('#claim_amount').val('');
					return false;
				}
		});
		
		
		$('.change_date').change(function() {
				$("#stay_form").hide();
		});

		/*$('.change_date').change(function() {
			var sdate = $('#from_date').val();
			var edate = $('#to_date').val();
			
			if(sdate !='' && edate != '')
			{
				$.ajax({
					url: '<?=base_url()?>map/stay_claim_list',
					type: 'post',
					data: {
						sdate: sdate,
						edate : edate
					},
					success: function(response) {
						//$("#pushDontpush").text("Attendance OUT"); 
						//$("#pushDontpush").removeClass('btn-default');
						//$("#pushDontpush").addClass('btn-info');
						//$(".loader-att").hide();
					},
					error: function(response) {
						window.console.log(response);
					}
				});
			}
		});	*/
		
		$('#sdate').change(function() {
			var date2 = $('#sdate').datepicker('getDate', '+1d'); 
			date2.setDate(date2.getDate()+1); 
			$('#edate').datepicker('setDate', date2);
		}); 

  });
</script>
