<div class="modal fade" id="uploadpodModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4>Upload POD</h4>
				<button type="button" class="close" data-dismiss="modal">×</button>
			</div>
			<form method="post" action="" role="form" name="frmuploadpod" id="frmupload"   enctype="multipart/form-data">
				<input type="hidden" name="fclaimid" id="fclaimid" value="" />
				<input type="hidden" name="fclaimtype" id="fclaimtype" value="" />
				<div class="modal-body">                      
					<span class="err" id="err_up"></span>
					<div class="form-group">
						<label for="pod"><span class='error'>*</span>Upload Bill:</label>
						<input type="file" class="form-control" id="file_name" name="file_name" value="" required>
						<span class="err">(Allowed only .jpg,.gif,.png,.csv,.xls file type)</span>
					</div>
				</div>
				<div class="modal-footer">
					<a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
					<input type="submit" id="uploadfile" name="uploadfile" class="btn btn-primary" value="Submit">
				</div>
			</form>
		</div>
  </div>
</div>

<script type="text/javascript">
function upload_bill(cid, ctype)
{
	$("#file_name").val('');
	$("#err_up").html('');
	$("#claimid").val('');
	$("#uploadpodModal").modal("show");
	$("#fclaimid").val(cid);
	$("#fclaimtype").val(ctype);
}

function remove_file(fileid)
{
	if(confirm('Are you sure to remove this file'))
	{
		alert(fileid);
		$.ajax({
				type: "POST",
				url: BASE_URL+"map/remove_bill",
				data: {
					fileid: fileid
				},
				success: function (response) {
					$("#row_"+fileid).remove();
				},
				error: function (e) {
					//$("#result").text(e.responseText);
					console.log("ERROR : ", e);
				}
		});
		
		
	}
}

$(document).ready(function () {
	
	/*$('.viewfile').click(function(e) {
    e.preventDefault();  //stop the browser from following
    var fname = $(this).attr('href');
    window.location.href = fname;	//'uploads/file.doc';
	});*/
	
	$("#uploadfile").click(function (event) {
		//stop submit the form, we will post it manually.
	  event.preventDefault();
		
		var claim_id = $("#fclaimid").val();
		// Get form
		var form = $('#frmupload')[0];
		// Create an FormData object 
		var data = new FormData(form);
		// If you want to add an extra field for the FormData
		//data.append("CustomField", "This is some extra data, testing");
		//disabled the submit button
		$("#uploadfile").prop("disabled", true);
		$.ajax({
				type: "POST",
				enctype: 'multipart/form-data',
				url: BASE_URL+"map/upload_claim_bill",
				data: data,
				processData: false,
				contentType: false,
				cache: false,
				timeout: 600000,
				success: function (data) {
					var returnedData = JSON.parse(data);
					if(returnedData.error == 0){
						$("#uploadpodModal").modal("hide");
						var fname = BASE_URL+returnedData.msg;
						//var spanval = "<a href="+fname+" target='_blank' download>View Bill</a>";
						var spanval = '<br /><a href="javascript:void(0)" onclick="return view_bills(\''+claim_id+'\', \'Other Claim\')" target="_blank" class="viewfile" download>View Bill</a>';
						if(returnedData.file_count == 1){
							$("#uplodedfile_"+claim_id).append(spanval);
						}
						else{
							//$("#uplodedfile_"+claim_id).append(spanval);
						}
						$("#uploadfile").prop("disabled", false);
					}
					else{
						$("#uploadfile").prop("disabled", false);
						$("#err_up").html(returnedData.msg);
						return false;
					}
					//console.log("SUCCESS : ", data);
				},
				error: function (e) {
					//$("#result").text(e.responseText);
					console.log("ERROR : ", e);
					$("#btnSubmit").prop("disabled", false);
				}
		});
	});
});
</script>
