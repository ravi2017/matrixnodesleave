<div class="row">
  <div class="col-md-12">
    <form method="post" action="" role="form" name="frmclaims" id="frmclaims"  enctype="multipart/form-data">
      <div class="row">
        <label class="col-md-2" for="title"><span style="color:red">*</span>From Date</label>
        <div class="form-group col-md-3">
          <div class='input-group date' id='datetimepicker1'>
            <span class="input-group-addon">
              <span class="icon-calendar"></span>
            </span>
            <input type="text" name="from_date" value="" class="form-control required" id="from_date" autocomplete="off" />
          </div>
        </div>
      
        <label class="col-md-2 offset-md-1" for="title"><span style="color:red">*</span>End Date</label>
        <div class="form-group col-md-3">
          <div class='input-group date' id='datetimepicker2'>
						<span class="input-group-addon">
              <span class="icon-calendar"></span>
            </span>
            <input type="text" name="to_date" value="" class="form-control endDate required" id="to_date" autocomplete="off" />
          </div>
        </div>
      </div>
      
      <div class="row">
				<label class="col-md-2" for="title"><span style="color:red">*</span>Bill No/Receipt No</label>
        <div class="form-group col-md-3">
          <div class='input-group date' id='datetimepicker2'>
            <input type="text" name="bill_number" value="" class="form-control required" id="bill_number" autocomplete="off" maxlength="40" />
          </div>
        </div>

        <label class="col-md-2 offset-md-1" for="title"><span style="color:red">*</span>Bill Date/Receipt Date</label>
        <div class="form-group col-md-3">
          <div class='input-group date' id='datetimepicker2'>
						<span class="input-group-addon">
              <span class="icon-calendar"></span>
            </span>
            <input type="text" name="bill_date" value="" class="form-control required" id="bill_date" autocomplete="off" />
          </div>
        </div>
      </div>
      
      <div class="row">
        <label class="col-md-2" for="title"><span style="color:red">*</span>Claim Type</label>
        <div class="form-group col-md-3">
          <select name="claim_type" id="claim_type" class="form-control required">
            <option value="">-Select Type-</option>  
            <? foreach($OTHER_CLAIM_TYPES as $key=>$value){	?>
							<option value="<?=$key;?>"><?=$value; ?></option>  
						<?	} ?>	
          </select>
        </div>
        <label class="col-md-2 offset-md-1" for="title"><span style="color:red">*</span>Amount</label>
        <div class="form-group col-md-3">
          <input type="text" name="amount" class="form-control required" id="amount"  autocomplete="off" >
        </div>
      </div>
      
      <div class="row">
        <label class="col-md-2" for="title">Comment</label>
        <div class="form-group col-md-3">
          <textarea name="comment" class="form-control" id="comment" ></textarea>
        </div>

        <label class="col-md-2 offset-md-1" for="title">Upload Bill</label>
        <div class="form-group col-md-3">
          <input type="file" name="file_name" class="form-control1" id="file_name" >(Allowed only .jpg,.gif,.png,.csv,.xls file type)
        </div>
      </div>
      
      <div class="form-group row">
				<div class="col-md-3">&nbsp;</div>
				<div class="col-md-3">
					<div class="form-group">
						<a href="<?php echo base_url();?>map/other_claims_list" class="btn btn-sm btn-default f-right">Cancel</a>
					</div>
				</div>
        <!--<div class="col-md-6"><br>
          <a class="btn btn-sm btn-danger f-left" href="<?=base_url()?>activity">Cancel</a>
        </div>-->
        <div class="col-md-3">
          <input type="submit" class="btn btn-sm btn-success f-left" name="Submit" value="Submit" />
        </div>
        <div class="col-md-3">&nbsp;</div>
      </div>
      
    </form>
  </div>
</div>
<style>
.error {
    border: 1px solid red !important;
}
.error:focus {
    border: 1px solid red !important;
}
.valid {
    border: 1px solid green !important;
}
select.error {
    border: 1px solid red !important;
}
</style>
<script type="text/javascript">
  $(document).ready(function ()
  {
		$("#frmclaims").validate({
				rules: {
					claim_type: "required"
				},
				messages: {
					claim_type: "Please select claim type"
				},
				errorPlacement: function (error, element) {
						return false;
				}
		});
		
		var curdate = new Date();
    curdate.setDate(curdate.getDate() - 1);
    var predate = new Date();
    predate.setDate(predate.getDate() - 31);
    
		$('#bill_date').datepicker({
			dateFormat: "dd-mm-yy",
			//minDate:predate,
			//maxDate:curdate
		});

     //$('#frmclaims').validate();
		//var date = new Date('2019-06-01');
    /*var cur_date = new Date();
    cur_date.setDate(cur_date.getDate() - 1);
    var pre_date = new Date();
    pre_date.setDate(pre_date.getDate() - 7);
			 
    $('#from_date').datepicker({
			dateFormat: "dd-mm-yy"
		});	
		
		$('#to_date').datepicker({
			dateFormat: "dd-mm-yy",
			minDate:pre_date,
      maxDate:cur_date
		});
		
		
		$('#from_date').change(function() {
			var date2 = $('#from_date').datepicker('getDate', '+1d'); 
			date2.setDate(date2.getDate()+1); 
			$('#to_date').datepicker('setDate', date2);
		}); */

  });
  
  $( function() {
  //var currentTime = new Date();
  //var startDateTo = new Date(currentTime.getFullYear(),currentTime.getMonth() -2,1);
  
  var cur_date = new Date();
	//cur_date.setDate(cur_date.getDate() - 1);
	var pre_date = new Date();
	//pre_date.setDate(pre_date.getDate() - 31);
  
  var dateFormat = "dd-mm-yy",
    from = $( "#from_date" )
      .datepicker({
        dateFormat: "dd-mm-yy",
        changeMonth: true,
        //minDate: pre_date,
        //maxDate: cur_date
      })
      .on( "change", function() {
        to.datepicker( "option", "minDate", getDate( this ) );
      }),
    to = $( "#to_date" ).datepicker({
      dateFormat: "dd-mm-yy",
      changeMonth: true,
      //minDate: pre_date,
      //maxDate: cur_date
    })
    .on( "change", function() {
      from.datepicker( "option", "maxDate", getDate( this ) );
    });

  function getDate( element ) {
    var date;
    try {
      date = $.datepicker.parseDate( dateFormat, element.value );
    } catch( error ) {
      date = null;
    }
    return date;
  }
});

  
</script>
