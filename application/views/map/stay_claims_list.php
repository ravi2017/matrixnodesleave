<? if(!isset($download) && !isset($pdf)) {	?>
		<? if($current_role != 'HR' && $current_role != 'accounts' && $current_role != 'super_admin'){	?>	
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<a href="<?php echo base_url();?>map/stay_claims" class="btn btn-sm btn-info">Stay Claim Request</a>
				</div>
			</div>
		</div><?	
	 }	
		include_once('top_form.php');
} ?>

<? if(!empty($claim_data)) { ?>
<? if(!isset($download) && !isset($pdf)) {?>
<form name='frmClaim' id='frmClaim' method="post" action="<?php echo base_url();?>map/process_stay_claims">
<div class="scrollme">
<table class="table table-striped table-bordered bootstrap-datatable datatable1 responsive customdatatable" id="withscrolltable">
	<thead>
		<tr style="background-color: #d5ecf9;">
			<th align="center">S.No.</th>
			<th align="center">Claim Duration</th>
			<th align="center">Activity <input type="hidden" name="spark_id" value="<?=$spark_id?>"></th>
			<th align="center">Stay With</th>
			<th align="center">Bill Details</th>
			<th align="center">Bill Cost</th>
			<th align="center">Claim Cost</th>
			<th align="center">Upload Bill</th>
			<th align="center">Remark</th>
      <th align="center">
			<?php if($current_role == 'accounts') { echo 'Action'; }else{ echo 'Account Status'; } ?>
      </th>
		</tr>
	</thead>
	<tbody>
	<?php 
		$r=1;
    $display_buttons = 1;
    $is_processed = 1;
    foreach($claim_data as $e_key){ 

      $bg_color = '';
      $record_id = $e_key->id;
      $comment = ($e_key->comment != '' ? $e_key->comment : '&nbsp;');
      $readonly_fields = '';
      $disable_fields = '';
      if($e_key->claim_status == 'rejected'){
        $readonly_fields = 'readonly';
        $disable_fields = ' disabled="true"';
        $bg_color = 'style="background-color:#fd6252;"';
      }
      
      $created_date = strtotime(date('Y-m-d', strtotime($e_key->created_on)));
      $current_date = strtotime(date('Y-m-d'));
      $days = (($current_date-$created_date)/(24*60*60));
      
      if(strtotime($e_key->from_date) != strtotime($e_key->to_date))
				$claim_duration = date('d-m-Y', strtotime($e_key->from_date))." to ".date('d-m-Y', strtotime($e_key->to_date));
			else	
				$claim_duration = date('d-m-Y', strtotime($e_key->from_date));
      
      $not_update = '';
      if($days > 15 && ($current_role != 'accounts')){
				$readonly_fields = 'readonly';
				$not_update = 'not_update';
			}
			if($e_key->claim_status == 'approved'){ 
				$is_processed = 0;
			}
			
			$stay_activity = json_decode($e_key->stay_reason);
			$stay_with = json_decode($e_key->paid_spark_id);
			$spark_names = array();
			if(!empty($stay_with))
			{
				foreach($stay_with as $stays)
				{
					$spark_names[] = $spark_with_names[$stays];	
				}
			}
			
			if($e_key->file_name == '' && $current_role == 'accounts' && $e_key->claim_amount !=0) {  
					// not display
			}
			else{
  ?>
		<tr id="row_<?php echo $record_id;?>" <?=$bg_color;?>>
      <td><?=$r;?></td>
      <td><?=$claim_duration;?></td>
			<td><?php echo implode(', ', $stay_activity); ?></td>
			<td><?php echo (!empty($spark_names) ? implode(',<br/>',$spark_names) : '');; ?></td>
			<td><?php echo '<strong>Bill No.:</strong> '.$e_key->bill_number.'<br /><strong>Bill Date:</strong> '.date('d-m-Y', strtotime($e_key->bill_date)); ?></td>
			<td><?php echo $e_key->manual_cost; ?></td>
			<td><?php echo $e_key->claim_amount; ?></td>
      <td>
				<? if($e_key->file_name != '') {?>
				<a href="<?php echo base_url().$e_key->file_name; ?>" target="_blank" class="viewfile" download>View Bill</a>
				<?	} else {?>
					<? if(($e_key->spark_id == $current_user_id) && ($e_key->claim_status == 'approved' || $e_key->claim_status == 'referback')){ ?>
							<div id="uplodedfile_<?=$record_id?>"><a href="javascript:void(0)" onclick="return upload_bill('<?=$record_id?>', 'Stay Claim')">Upload Bill</a></div>
					<?	}else{	?>	
						<div id="uplodedfile_<?=$record_id?>">No Bill</div>
					<? } ?>
        <?	}	?>
      </td>
      <td><textarea name="comment[]" id="comment_<?=$record_id?>" cols="6" rows="2" maxlength="200" <?=$readonly_fields?>><?=$e_key->comment;?></textarea>
				<input type="hidden" name="claim_status[<?=$record_id?>]" value="<?=$e_key->claim_status?>">
				<input type="hidden" name="record_id[]" value="<?=$record_id?>">
			</td>
			<td>
        <?php 
        if($e_key->claim_status == 'processed') {  
						$process_date = (!empty($e_key->process_date) ? '('.date('d-m-Y', strtotime($e_key->process_date)).')' : '');
						$user_data = get_spark_data($e_key->updated_by);
            ?><span id="rreason_<?=$record_id?>">Processed <?=$process_date." (By ".$user_data['login_id'].")";?></span><?
        }
        else{
        	if($current_role == 'accounts') {  
          $btn_reject = 'display:block';
          $btn_referbk = 'display:block';
          
          if($e_key->claim_status == 'rejected' || $e_key->claim_status == 'referback'){ 
            $btn_reject = 'display:none';
            $btn_referbk = 'display:none';
          } 
          ?>  
          <div id="referback_<?=$record_id?>" style="<?=$btn_referbk?>">
            <input type="button" name="referback_claim" id="referback_claim" onClick="return approvedclaim('<?=$record_id?>', 'referback', 'Refer Back', 'stay_claims')" class="btn btn-sm btn-success" value="Refer Back"> 
          </div>
          <div id="discard_<?=$record_id?>" style="<?=$btn_reject?>">
						<br />
            <input type="button" name="reject_claim" id="reject_claim" onClick="return approvedclaim('<?=$record_id?>', 'rejected', 'Reject', 'stay_claims')" class="btn btn-sm btn-danger" value="Reject"> 
          </div>
        <? }
					if($current_role == 'state_person' || $current_role == 'manager') {  
						if($e_key->claim_status == 'referback'){ 
							?>
							<div id="approved_<?=$record_id?>">
								<input type="button" name="approved_claim" id="approved_claim" onClick="return approvedclaim('<?=$record_id?>', 'approved', 'Approve', 'stay_claims')" class="btn btn-sm btn-success" value="Approve"> 
							</div>
							<div id="discard_<?=$record_id?>">
								<br />
								<input type="button" name="reject_claim" id="reject_claim" onClick="return approvedclaim('<?=$record_id?>', 'rejected', 'Reject', 'stay_claims')" class="btn btn-sm btn-danger" value="Reject"> 
							</div>
							<?
						}
					}
					?>
         <div><span id="rreason_<?=$record_id?>"><?=$e_key->status_remark;?></span></div> 
        <? } ?>  
      </td>
      
		</tr>
	<?php } 
	$r++; }  ?>
  </tbody>
</table>  
</div>

<? if($current_role == 'accounts' && $is_processed == 0) { ?>
<div class="col-md-12" align="right" style="padding-top:10px;">
  <input type="submit" name="processClaim" value="Process Claim" class="btn btn-sm btn-info">
</div>
<?  } ?>
<? if($sel_spark_id == $current_user_id && $is_processed == 0) { ?>
<div class="col-md-12" align="right" style="padding-top:10px;">
  <input type="submit" name="processClaim" value="Update Claim" class="btn btn-sm btn-info">
</div>
<?  } ?>
</form>  

<? }else{ ?>
  <table cellspacing=0 cellpadding=2 border=1 width="100%">
    <tr>
      <td colspan=4><strong>Spark Name :</strong> <? echo $spark_name?></td>
      <td colspan=4><strong>Duration :</strong> <?=$activitydatestart?> To <?=$activitydateend?></td>
    </tr>
  </table>  
  <table cellspacing=0 cellpadding=2 border=1>
	<thead>
		<tr>
      <th align="center">S.No.</th>
			<th align="center">Claim Duration</th>
			<th align="center">Claim Type</th>
			<th align="center">Stay With</th>
			<th align="center">Bill Number</th>
			<th align="center">Bill Date</th>
			<th align="center">Bill Cost</th>
			<th align="center">Claim Cost</th>
			<th align="center">Remark</th>
			<th align="center">Claim Status</th>
		</tr>
	</thead>
	<tbody>
	<? 
		 $k=1; 
     $total_travel_cost = 0;
     
		foreach ($claim_data as $e_key){
      
      $bg_color = '';
      $record_id = $e_key->id;
      $comment = ($e_key->comment != '' ? $e_key->comment : '&nbsp;');
      $readonly_fields = '';
      $disable_fields = '';
      if($e_key->claim_status == 'rejected'){
        $readonly_fields = 'readonly';
        $disable_fields = ' disabled="true"';
        $bg_color = 'style="background-color:#fd6252;"';
      }
      
      $created_date = strtotime(date('Y-m-d', strtotime($e_key->created_on)));
      $current_date = strtotime(date('Y-m-d'));
      $days = (($current_date-$created_date)/(24*60*60));
      
      $claim_duration = date('d-m-Y', strtotime($e_key->from_date))." to ".date('d-m-Y', strtotime($e_key->to_date));
      
      $not_update = '';
      if($days > 3 && ($current_role != 'accounts')){
				$readonly_fields = 'readonly';
				$not_update = 'not_update';
			}
			
			$stay_activity = json_decode($e_key->stay_reason);
			$stay_with = json_decode($e_key->paid_spark_id);
			$spark_names = array();
			if(!empty($stay_with))
			{
				foreach($stay_with as $stays)
				{
					$spark_names[] = $spark_with_names[$stays];	
				}
			}
			
			if($upload_proofs[$record_id] == '' && $current_role == 'accounts' && $e_key->claim_amount !=0) {
					// not display
			}
			else{
			
			$total_travel_cost = $total_travel_cost + $e_key->manual_cost;
    ?>
		<tr>
      <td><?=$k;?></td>
      <td><?=$claim_duration;?></td>
			<td><?php echo implode(', ', $stay_activity); ?></td>
			<td><?php echo (!empty($spark_names) ? implode(',<br/>',$spark_names) : '');; ?></td>
			<td><?php echo $e_key->bill_number; ?></td>
			<td><?php echo date('d-m-Y', strtotime($e_key->bill_date)); ?></td>
			<td><?php echo $e_key->actual_cost; ?></td>
			<td><?php echo $e_key->claim_amount; ?></td>
      <td><?php echo $e_key->comment; ?></td>
			<td><?php echo $e_key->claim_status; ?></td>
		</tr>
	<?php } $k++; }  ?>
    <!--<tr>
      <td colspan=5></td>
      <td align="right"><?=$total_travel_cost;?></td>
      <td colspan=2>&nbsp;</td>
    </tr>-->
  </tbody>
</table>  
 
<? } ?>
<? } ?>

<? // && !empty($display_buttons)
if(!isset($download) && !isset($pdf) && !empty($claim_data))  { ?>
    <br />
    <div class="row">
      <? if($current_role == 'super_admin' || $current_role == 'accounts') { ?>
      <div class="form-group col-md-3">
      <form name="frmreport" action="<?php echo base_url();?>map/stay_claims_list" method="post">
          <input type="hidden" name="activitydatestart" value="<? echo (!empty($activitydatestart) ? $activitydatestart : '');?>">
          <input type="hidden" name="activitydateend" value="<? echo (!empty($activitydateend) ? $activitydateend : '');?>">
          <input type="hidden" name="spark_id" value="<?=$spark_id?>">
          <input type="hidden" name="spark_state_id" value="<?=$spark_state_id?>">
          <input type="hidden" name="download" value="1">
          <button type="submit" name="submit" value="Get Claims List" class="btn btn-success">Download Report</button>
      </form>
      </div>
      <?  } ?>
      <div class="form-group col-md-3">
      <form name="frmreport" action="<?php echo base_url();?>map/stay_claims_list" method="post">
          <input type="hidden" name="activitydatestart" value="<? echo (!empty($activitydatestart) ? $activitydatestart : '');?>">
          <input type="hidden" name="activitydateend" value="<? echo (!empty($activitydateend) ? $activitydateend : '');?>">
          <input type="hidden" name="spark_id" value="<?=$spark_id?>">
          <input type="hidden" name="spark_state_id" value="<?=$spark_state_id?>">
          <input type="hidden" name="pdf" value="1">
        <button type="submit" name="submit" value="Get Claims List" class="btn btn-success">Download PDF</button>
      </form>
      </div>
    </div>
<? } ?>


<? if(!isset($download) && !isset($pdf)) {
		include_once('claim_action.php');
		include_once('claim_upload.php');
} ?>
