<style>
#right-panel {
  font-family: 'Roboto','sans-serif';
  line-height: 30px;
  padding-left: 10px;
}

#right-panel select, #right-panel input {
  font-size: 15px;
}

#right-panel select {
  width: 100%;
}

#right-panel i {
  font-size: 12px;
}
#map {
  height: 100%;
  float: left;
  width: 70%;
  height: 100%;
}
#right-panel {
  border-width: 2px;
  height: 400px;
  float: left;
  text-align: left;
  padding-top: 0;
}
#directions-panel {
  margin-top: 10px;
  padding: 10px;
  height: 100%;
  overflow-y: auto;
}
.routedetails{display:none;}
</style>
<form name="frmactivities" method="get" action="" class="">
  <div class="row">
    <div class="col-md-3">
      <div class="form-group">
        <input type="text" id="activitydate" name="activitydate" class="form-control" value="<?php echo $map_date; ?>">
      </div>
    </div>
    <?php if ($current_role != "field_user") { ?>
    <div class="col-md-6">
    <?php echo $this->load->view('team_members/select_spark','',true) ?>
    </div>
    <?php } ?>
    <div class="col-md-3">
      <div class="form-group">
        <input type="submit" id="getactivities" class="form-control btn btn-sm btn-success" value="Get Activities">
      </div>
    </div>
  </div>
</form>
<div class="row">
  <div class="col-md-12">
    <div id="map" style="width:100%;height:500px"></div>
  </div>
</div>
<?php 
if (isset($activities) and count($activities) > 0) {
  $locations_arr = array();
  $locations = array();
  //print_r($activities);
  //die;
  $net_lat = 0;
  $net_long = 0;
  $count = count($activities);
  foreach($activities as $activity) { 
    if($activity['latitude'] !='' && $activity['longitude'] !=''){
      $location = array();
      $location1 = array();
      
      $net_lat = $net_lat+$activity['latitude'];
      $net_long = $net_long+$activity['longitude'];
      
      $loc = ($activity['location'] != '' ? $activity['location'] : 'Lat :'.$activity['latitude'].' - Long :'.$activity['longitude']);
      
      array_push($location1, $loc);
      array_push($location1,$activity['latitude']);
      array_push($location1,$activity['longitude']);
      array_push($location1,$activity['activity']);
      array_push($location1,$activity['time']);
      array_push($location1,$activity['distance']);
      array_push($locations_arr,$location1);
      
      //$marker_info = '<strong>'.ucfirst(strtolower($activity['activity'])).'</strong><br>'.$activity['time'].'<br>'.$loc;
      array_push($location,$loc);
      array_push($location,$activity['latitude']);
      array_push($location,$activity['longitude']);
      array_push($location,"assets/v2/images/markers/".$activity['marker']);
      
      array_push($locations,$location);
    } 
  } 
  $count_loc = count($locations_arr);
  
  $avg_loc_count = round($count/2);
  //$avg_lat = round($net_lat/$count_loc, 4);
  //$avg_long = round($net_long/$count_loc, 4);
  $avg_lat = (isset($activities[$avg_loc_count]['latitude']) ? $activities[$avg_loc_count]['latitude'] : '');
  $avg_long =(isset($activities[$avg_loc_count]['longitude']) ? $activities[$avg_loc_count]['longitude'] : '');
  
  $locations = json_encode($locations);
?>
<script>
//var lat_longs = [];
var locations = <?php echo $locations; ?>

    // When the user clicks the marker, an info window opens.
    var i = 0;
    var summaryPanel_HTML = document.getElementById('directions-panel');
    summaryPanel_HTML = '<div class="selector-toggle"><a href="javascript:void(0)" alt="Route Details" title="Route Details"></a></div><ul><li><p class="selector-title main-title">ACTIVITY DETAILS</p></li></ul><div class="card-block accordion-block" style="overflow-y: auto; height: 80%;"><div id="accordion" role="tablist" aria-multiselectable="true">';
    
    summaryPanel_HTML += '<div class="accordion-panel">';
    summaryPanel_HTML += '<div class="accordion-heading" role="tab" id="heading'+i+'">';
    summaryPanel_HTML += '<h3 class="card-title accordion-title">';
    summaryPanel_HTML += '<a class="accordion-msg-map" data-toggle="collapse" style="padding: 14px 0px" data-parent="#accordion" href="#collapse'+i+'" aria-expanded="true" aria-controls="collapse'+i+'">';
    summaryPanel_HTML += '<?=$locations_arr[0][3]?>';
    summaryPanel_HTML += '</a>';
    summaryPanel_HTML += '</h3>';
    summaryPanel_HTML += '</div>';
    summaryPanel_HTML += '<div id="collapse'+i+'" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading'+i+'">';
    summaryPanel_HTML += '<div class="accordion-msg-map accordion-content accordion-desc" style="padding: 0px 0px">';
    summaryPanel_HTML += '<p><?=$locations_arr[0][0]?></p><br>';
    summaryPanel_HTML += '</div>';
    summaryPanel_HTML += '</div>';
    summaryPanel_HTML += '</div>';
    
  <? for($i=1; $i < $count_loc; $i++){ ?>
                                  
        var k = <?=$i?>;
        
        //if(k > 0){
          summaryPanel_HTML += '<div class="accordion-panel">';
          summaryPanel_HTML += '<div class="accordion-heading" role="tab" id="heading'+k+'">';
          summaryPanel_HTML += '<h3 class="card-title accordion-title">';
          summaryPanel_HTML += '<a class="accordion-msg-map" data-toggle="collapse" style="padding: 14px 0px" data-parent="#accordion" href="#collapse'+k+'" aria-expanded="true" aria-controls="collapse'+k+'">';
          summaryPanel_HTML += '<?=$locations_arr[$i][3]?>';
          summaryPanel_HTML += '</a>';
          summaryPanel_HTML += '</h3>';
          summaryPanel_HTML += '</div>';
          summaryPanel_HTML += '<div id="collapse'+k+'" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading'+k+'">';
          summaryPanel_HTML += '<div class="accordion-msg-map accordion-content accordion-desc" style="padding: 0px 0px">';
          summaryPanel_HTML += '<p><?=$locations_arr[$i][0]?></p><br>';
          summaryPanel_HTML += '<p><b>Distance: </b>(<?=$locations_arr[$i][5]?>)</p><br>';
          summaryPanel_HTML += '</div>';
          summaryPanel_HTML += '</div>';
          summaryPanel_HTML += '</div>';
        //}
  <?  } ?>
    
    
    summaryPanel_HTML = summaryPanel_HTML+'</div><div><br /><p><a href="map/claims_list" class="text-normal">Goto Claim List</a></p></div></div>';
  
    
    function initMap() {
        var myLatLng = {lat: <?=$avg_lat?>, lng: <?=$avg_long?>};

        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 14,
            center: myLatLng
            });

        var count=0;


        for (count = 0; count < locations.length; count++) {  

            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(locations[count][1], locations[count][2]),
                map: map,
                icon: locations[count][3]
                });

            marker.info = new google.maps.InfoWindow({
                content: locations [count][0]
                });


            google.maps.event.addListener(marker, 'click', function() {  
                // this = marker
                var marker_map = this.getMap();
                this.info.open(marker_map, this);
                // Note: If you call open() without passing a marker, the InfoWindow will use the position specified upon construction through the InfoWindowOptions object literal.
                });
        }
        (function() {
            $(summaryPanel_HTML).appendTo($('#styleSelector'));
          })();
    }
            
    $('.selector-toggle > a').on("click", function() {
        $('#styleSelector').toggleClass('open')
    });
</script>      
<?php } else { ?>
  <script>      
    $("#map").html("<div class='alert alert-danger icons-alert'>No Activity Available</div>");
  </script>      
<?php } ?>




