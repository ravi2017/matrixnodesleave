<style>
#right-panel {
  font-family: 'Roboto','sans-serif';
  line-height: 30px;
  padding-left: 10px;
}

#right-panel select, #right-panel input {
  font-size: 15px;
}

#right-panel select {
  width: 100%;
}

#right-panel i {
  font-size: 12px;
}
#map {
  height: 100%;
  float: left;
  width: 70%;
  height: 100%;
}
#right-panel {
  border-width: 2px;
  height: 400px;
  float: left;
  text-align: left;
  padding-top: 0;
}
#directions-panel {
  margin-top: 10px;
  padding: 10px;
  height: 100%;
  overflow-y: auto;
}
.routedetails{display:none;}
</style>
<form name="frmactivities" method="get" action="" class="">
  <div class="row">
    <div class="col-md-3">
      <div class="form-group">
        <input type="text" id="activitydate" name="activitydate" class="form-control" value="<?php echo $map_date; ?>">
      </div>
    </div>
    <?php if ($current_role != "field_user") { ?>
    <div class="col-md-6">
    <?php echo $this->load->view('team_members/select_spark','',true) ?>
    </div>
    <?php } ?>
    <div class="col-md-3">
      <div class="form-group">
        <input type="submit" id="getactivities" class="form-control btn btn-sm btn-success" value="Get Activities">
      </div>
    </div>
  </div>
</form>
<div class="row">
  <div class="col-md-12">
    <div id="map" style="width:100%;height:500px"></div>
  </div>
</div>
<script>


$("#activitydate").change(function(){
	var sdate = $("#activitydate").val();
	var edate = $("#activitydate").val();
	$("#activitydate").val(sdate);
	$("#activitydate").val(edate);
	reset_values();
});	
	
var lat_longs = [];
<?php 
if (isset($activities) and count($activities) > 0) {
?>
<?php foreach($activities as $activity) { ?>
lat_longs.push({
  tracking_id: '<?php echo $activity['track_id']; ?>',
  tracking_latitude: '<?php echo $activity['latitude']; ?>',
  tracking_longitude: '<?php echo $activity['longitude']; ?>',
  tracking_location: '<?php echo addslashes($activity['location']); ?>',
  tracking_time: '<?php echo ((isset($activity['time']['in'])) ? $activity['time']['in'] : '').((isset($activity['time']['in']) && isset($activity['time']['out'])) ? ' - ' : '').((isset($activity['time']['out'])) ? $activity['time']['out'] : ''); ?>',
  tracking_activity: '<?php echo $activity['activity']; ?>',
  tracking_place: '<?php echo (isset($activity['place']) and $activity['place'] != "") ? $activity['place'] : ''; ?>',
  tracking_duration: '<?php echo $activity['duration']; ?>',
  created_by: '<?php echo $activity['created_by']; ?>',
  marker: '<?php echo $activity['marker']; ?>'
});
<?php } ?>
<?php } else { ?>
  $("#map").html("<div class='alert alert-danger icons-alert'>No Activity Available</div>")
<?php } ?>

/*$(document).on('click', '#claim_now', function(){
  var source_id = $('input[name="track_source_id[]"]').map(function () {
      return this.value; // $(this).val()
  }).get();
  var destination_id = $('input[name="track_destination_id[]"]').map(function () {
      return this.value; // $(this).val()
  }).get();
  var distance = $('input[name="travel_distance[]"]').map(function () {
      return this.value; // $(this).val()
  }).get();
  
  $.ajax({
      type: "POST",
      data: 'sids='+source_id.toString()+'&dids='+destination_id.toString()+'&distance='+distance.toString(), 
      url: "map/save_trcking",
      success: function (response) {
          alert('Claim records saved successfully.');
      }
  });
  
});*/

</script>
