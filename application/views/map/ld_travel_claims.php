<? if(!isset($download) && !isset($pdf)) {
		include_once('top_form.php');
} ?>

<? if(!empty($claim_data)) { $activity_date = ''; ?>
<? if(!isset($download) && !isset($pdf)) {?>
<form name='frmClaim' id='frmClaim' method="post" action="<?php echo base_url();?>map/process_ld_travel">
<div class="scrollme">
<table class="table table-striped table-bordered bootstrap-datatable datatable1 responsive customdatatable">
	<thead>
		<tr style="background-color: #d5ecf9;">
			<th align="center">S.No.<input type="hidden" name="spark_id" value="<?=$spark_id?>"></th>
			<!--<th align="center">Activity <input type="hidden" name="spark_id" value="<?=$spark_id?>"></th>-->
			<th align="center">Details</th>
			<th align="center">Date<br>(DD:MM:YY)</th>
			<th align="center">Time <br>(HH:MM)</th>
			<th align="center">Duration <br>(HH:MM)</th>
			<th align="center">Distance <br>(KM)</th>
			<th align="center">Manual Distance <br>(KM)</th>
			<th align="center">Upload</th>
			<th align="center">Travel Mode</th>
			<th align="center">Travel Cost</th>
			<th align="center">Remark</th>
      <th align="center">
			<?php if($current_role == 'accounts') { echo 'Action'; }else{ echo 'Account Status'; } ?>
      </th>
		</tr>
	</thead>
	<tbody>
	<?php 
    $r=1;
    $previous_travel_mode_id = '';
    $display_buttons = 1;
    $is_processed = 1;
    foreach($claim_data as $e_key){ 
      $record_id = $e_key->id;
      $bg_color = '';
			$duration = $e_key->duration;
			$travel_mode_id = $e_key->travel_mode_id;
			$travel_cost = $e_key->travel_cost;
			
      $activity_type = 'LD Travel';
      
      $uploadData = get_upload_file(array('claim_type'=>'LD Travel', 'claim_id'=>$e_key->id));
      
      $file_name = '';
      if(!empty($uploadData))
      $file_name = $uploadData[0]->file_name; 
      
      $activity_location = trim(strip_tags($e_key->from_address));
      $exp_loc = '<strong>IN:</strong> '.implode(',<br>', explode(',',$activity_location));
      
      $lat_long_location = trim(strip_tags($e_key->to_address));
        
      //if($activity_location != $lat_long_location)
      //{
        $lat_long_location = implode(',<br>', explode(',',$lat_long_location));
        //if($current_role == "super_admin")
         $exp_loc .= '<hr><strong>OUT:</strong> '.$lat_long_location;
      //}
      
      $status_remark = ($e_key->status_remark != '' ? $e_key->status_remark : '&nbsp;');
      $remarks = ($e_key->remarks != '' ? $e_key->remarks : '&nbsp;');
      
      if($e_key->travel_mode_id == ''){
        $display_buttons = 0;
      }
      
      if($e_key->claim_status == 'approved'){
        $is_processed = 0;
      }
      
      if(empty($e_key->travel_mode_id) && $travel_mode_id !='')
      {
        $e_key->travel_mode_id = $travel_mode_id;
        $e_key->travel_cost = $travel_cost;
      }
      
      $travelCost = '';
      foreach($travels_mode as $travel_mode) {
        if($e_key->travel_mode_id == $travel_mode->id){
          $tdistance = round($e_key->travel_distance);
          $travelCost = $tdistance*$travel_mode->travel_cost;
        }
      }
      
      if(empty($e_key->travel_cost) || $e_key->travel_cost == 0.00)
      {
        $e_key->travel_cost = $travelCost;
      }
      
      $readonly_fields = '';
      $disable_fields = '';
      if($e_key->claim_status == 'rejected'){
        $readonly_fields = 'readonly';
        $disable_fields = ' disabled="true"';
        $bg_color = 'style="background-color:#fd6252;"';
      }
      
      $created_date = strtotime(date('Y-m-d', strtotime($e_key->created_at)));
      $current_date = strtotime(date('Y-m-d'));
      $days = (($current_date-$created_date)/(24*60*60));
      
      $not_update = '';
      if($days > 3 && ($current_role != 'accounts')){
				//$readonly_fields = 'readonly';
				$not_update = 'not_update';
			}
  ?>
		<tr id="row_<?php echo $record_id;?>" <?=$bg_color;?>>
      <td><?=$r;?><input type="hidden" name="record_id[]" value="<?=$record_id?>"></td>
			<!--<td><?php //echo $activity_type; ?></td>-->
			<td><?php echo $exp_loc; ?></td>
			<td><?php echo date('d-m-Y', strtotime($e_key->activity_date)); ?></td>
			<td><?php echo date('H:i', strtotime($e_key->activity_date)); ?></td>
      <td><?=$duration;?></td>
			<td align="right">
        <input type="hidden" name="tracking_time[]" id="distance_<?=$record_id;?>" value="<?=$e_key->activity_date?>">
        <input type="hidden" name="distance[]" id="distance_<?=$record_id;?>" value="<?=$e_key->travel_distance?>">
        <?php echo round($e_key->travel_distance); ?></td>
			<td>
        <input type="text" size="4" name="manualdistance[]" id="manualdistance_<?=$record_id;?>" value="<?=(!empty($e_key->manual_distance) && $e_key->manual_distance > 0 ? $e_key->manual_distance : round($e_key->travel_distance));?>" class="required chg_manual text-right" <?=$readonly_fields?>>
      </td>
      <td>
				<? if($file_name != '') {?>
				<a href="<?php echo base_url().$file_name;?>" target="_blank" class="viewfile" download>View Bill</a>
				<?	} else {?>
					<? if(($e_key->user_id == $current_user_id) && ($e_key->claim_status == 'approved' || $e_key->claim_status == 'referback')){ ?>
							<div id="uplodedfile_<?=$record_id?>"><a href="javascript:void(0)" onclick="return upload_bill('<?=$record_id?>', 'LD Travel')">Upload Bill</a></div>
					<?	}else{	?>	
						<div id="uplodedfile_<?=$record_id?>">No Bill</div>
					<? } ?>
        <?	}	?>
      </td>
			<td>
				<? if($not_update == '') { ?>
        <select name="travel_mode[]" id="travel_mode_<?=$record_id?>" alt="<?=$record_id?>" class="sel_mode form-control required">
          <option value="">-Mode-</option>
          <?php foreach($travels_mode as $travel_mode) { ?>
            <option value="<?=$travel_mode->id."_".$travel_mode->travel_cost;?>" <? if($e_key->travel_mode_id == $travel_mode->id){ echo 'selected'; } ?>>
              <?=$travel_mode->travel_mode;?>
            </option>
          <?php } ?>  
            <option value="9999" <? if(isset($travel_mode->id) && $e_key->travel_mode_id == '9999'){ echo 'selected'; } ?>>With Other</option>
        </select>
        <?	}else{	?>
					<select name="travel_mode[]" id="travel_mode_<?=$record_id?>" alt="<?=$record_id?>" class="sel_mode form-control required">
					  <? if($e_key->travel_mode_id == ''){ ?>
							<option value="">-Mode-</option>
						<?	}else if($e_key->travel_mode_id == '9999'){ ?>
							<option value="9999">With Other</option>
						<? } else { ?>
						<?php foreach($travels_mode as $travel_mode) { 
							if($e_key->travel_mode_id == $travel_mode->id){	?>
								<option value="<?=$travel_mode->id."_".$travel_mode->travel_cost;?>"><?=$travel_mode->travel_mode;?></option>
						<?php } } } ?>  
					</select>
				<?	}	?>	
      </td>
      <td><input type="text" name="travel_cost[]" id="travel_cost_<?=$record_id?>" size="4" value="<?=round($e_key->travel_cost);?>" maxlength="8" class="required text-right" <?=$readonly_fields?>></td>
      <td><textarea name="remarks[]" id="remark_<?=$record_id?>" cols="6" rows="2" maxlength="200" class="required" <?=$readonly_fields?>><?=$remarks;?></textarea>
      <input type="hidden" name="claim_status[<?=$record_id?>]" value="<?=$e_key->claim_status?>">
      <input type="hidden" name="claim_processed[<?=$record_id?>]" value="<?=$e_key->claim_status?>">
      </td>
      <td>
        <?php 
        if($e_key->claim_status == 'processed') {  
						$process_date = (!empty($e_key->process_date) ? '('.date('d-m-Y', strtotime($e_key->process_date)).')' : '');
						$user_data = get_spark_data($e_key->updated_by);
            ?><span id="rreason_<?=$record_id?>">Processed <?=$process_date." (By ".$user_data['login_id'].")";?></span><?
        }
        else{
        if($current_role == 'accounts') {  
          $btn_reject = 'display:block';
          $btn_referbk = 'display:block';
          $status_remark = $e_key->status_remark;
          
          if($e_key->claim_status == 'rejected' || $e_key->claim_status == 'referback'){ 
            $btn_reject = 'display:none';
            $btn_referbk = 'display:none';
            //$reject_reason = $e_key->status_remark." (Rejected)";
          } 
          ?>  
          <div id="referback_<?=$record_id?>" style="<?=$btn_referbk?>">
            <input type="button" name="referback_claim" id="referback_claim" onClick="return approvedclaim('<?=$record_id?>', 'referback', 'Refer Back','other_activities')" class="btn btn-sm btn-success" value="Refer Back"> 
          </div>
          <div id="discard_<?=$record_id?>" style="<?=$btn_reject?>">
						<br />
            <input type="button" name="reject_claim" id="reject_claim" onClick="return approvedclaim('<?=$record_id?>', 'rejected', 'Reject','other_activities')" class="btn btn-sm btn-danger" value="Reject"> 
          </div>
        <? }
        if($current_role == 'state_person' || $current_role == 'manager') {  
					if($e_key->claim_status == 'referback'){ 
						?>
						<div id="approved_<?=$record_id?>">
							<input type="button" name="approved_claim" id="approved_claim" onClick="return approvedclaim('<?=$record_id?>', 'approved', 'Approve','other_activities')" class="btn btn-sm btn-success" value="Approve"> 
						</div>
						<div id="discard_<?=$record_id?>">
							<br />
							<input type="button" name="reject_claim" id="reject_claim" onClick="return approvedclaim('<?=$record_id?>', 'rejected', 'Reject','other_activities')" class="btn btn-sm btn-danger" value="Reject"> 
						</div>
						<?
					}
				}
       ?>  
          <div><span id="rreason_<?=$record_id?>"><?=$e_key->status_remark;?></span></div>
        <? } ?>  
      </td>
		</tr>
	<?php $r++; }  ?>
  </tbody>
</table>  
</div>

<? if($current_role == 'accounts' && $is_processed == 0) { ?>
<div class="col-md-12" align="right" style="padding-top:10px;">
  <input type="submit" name="processClaim" value="Process Claim" class="btn btn-sm btn-info">
</div>
<?  } ?>
<? if($sel_spark_id == $current_user_id && $is_processed == 0) { ?>
<div class="col-md-12" align="right" style="padding-top:10px;">
  <input type="submit" name="processClaim" value="Update Claim" class="btn btn-sm btn-info">
</div>
<?  } ?>
</form>  

<? }else{ ?>
  <table cellspacing=0 cellpadding=2 border=1 width="100%">
    <tr>
      <td colspan=5><strong>Spark Name :</strong> <? echo $spark_name?></td>
      <td colspan=5><strong>Duration :</strong> <?=$activitydatestart?> To <?=$activitydateend?></td>
    </tr>
  </table>  
  <table cellspacing=0 cellpadding=2 border=1>
	<thead>
		<tr>
      <th align="center">S.No.</th>
			<th align="center">Details</th>
			<th align="center">Date<br>(DD:MM:YY)</th>
			<th align="center">Time <br>(HH:MM)</th>
			<th align="center">Duration <br>(HH:MM)</th>
			<th align="center">Distance <br>(KM)</th>
			<th align="center">Manual Distance <br>(KM)</th>
			<th align="center">Travel Mode</th>
			<th align="center">Travel Cost</th>
			<th align="center">Remark</th>
		</tr>
	</thead>
	<tbody>
	<? $state_count = 0; 
     $k=1; 
     $total_travel_distance = 0;
     $total_manual_distance = 0;
     $total_travel_cost = 0;
  ?>
	<?php foreach ($claim_data as $e_key){
    
      
   if($e_key->travel_mode_id != '9999' && $e_key->claim_status != 'rejected'){ 
      $record_id = $e_key->id;
      $sel_travel_mode = '';
      foreach($travels_mode as $travel_mode) {
          if($e_key->travel_mode_id == $travel_mode->id){ 
            $sel_travel_mode =  $travel_mode->travel_mode; 
          } 
      }  
         
      $activity_location = trim(strip_tags($e_key->from_address));
      $exp_loc = 'IN Address: '.implode(', ', explode(',',$activity_location));
      
      $lat_long_location = trim(strip_tags($e_key->to_address));
			$lat_long_location = implode(', ', explode(',',$lat_long_location));
			$exp_loc .= '<br>OUT Address: '.$lat_long_location;
      
      $total_travel_distance = round($e_key->travel_distance) + $total_travel_distance;
      $total_manual_distance = $e_key->manual_distance + $total_manual_distance;
      $total_travel_cost = $e_key->travel_cost + $total_travel_cost;
    ?>
		<tr>
      <td align="center"><?=$k?></td>
			<td><?php echo $exp_loc; ?></td>
			<td><?php echo date('d-m-Y', strtotime($e_key->activity_date)); ?></td>
			<td><?php echo date('H:i', strtotime($e_key->activity_date)); ?></td>
      <td><?=$e_key->duration;?></td>
			<td align="right"><?php echo round($e_key->travel_distance); ?></td>
			<td><?=(!empty($e_key->manual_distance) && $e_key->manual_distance > 0 ? $e_key->manual_distance : round($e_key->travel_distance));?></td>
			<td><?=$sel_travel_mode;?></td>
      <td align="right"><?=round($e_key->travel_cost);?></td>
      <td><?=$e_key->remarks;?></td>
		</tr>
	<?php $k++; } } ?>
    <tr>
      <td colspan=5></td>
      <td align="right"><?=$total_travel_distance;?></td>
      <td align="right"><?=$total_manual_distance;?></td>
      <td>&nbsp;</td>
      <td align="right"><?=$total_travel_cost;?></td>
      <td>&nbsp;</td>
    </tr>
  </tbody>
</table>  
 
<? } ?>
<? } ?>

<? // && !empty($display_buttons)
if(!isset($download) && !isset($pdf) && !empty($claim_data))  { ?>
    <br />
    <div class="row">
      <? if($current_role == 'super_admin' || $current_role == 'accounts') { ?>
      <div class="form-group col-md-3">
      <form name="frmreport" action="<?php echo base_url();?>map/ld_travel_claims" method="post">
          <input type="hidden" name="activitydatestart" value="<? echo (!empty($activitydatestart) ? $activitydatestart : '');?>">
          <input type="hidden" name="activitydateend" value="<? echo (!empty($activitydateend) ? $activitydateend : '');?>">
          <input type="hidden" name="spark_id" value="<?=$spark_id?>">
          <input type="hidden" name="spark_state_id" value="<?=$spark_state_id?>">
          <input type="hidden" name="download" value="1">
          <button type="submit" name="submit" value="Get Claims List" class="btn btn-success">Download Report</button>
      </form>
      </div>
      <?  } ?>
      <div class="form-group col-md-3">
      <form name="frmreport" action="<?php echo base_url();?>map/ld_travel_claims" method="post">
          <input type="hidden" name="activitydatestart" value="<? echo (!empty($activitydatestart) ? $activitydatestart : '');?>">
          <input type="hidden" name="activitydateend" value="<? echo (!empty($activitydateend) ? $activitydateend : '');?>">
          <input type="hidden" name="spark_id" value="<?=$spark_id?>">
          <input type="hidden" name="spark_state_id" value="<?=$spark_state_id?>">
          <input type="hidden" name="pdf" value="1">
        <button type="submit" name="submit" value="Get Claims List" class="btn btn-success">Download PDF</button>
      </form>
      </div>
    </div>
<? } ?>


<? if(!isset($download) && !isset($pdf)) {

		include_once('claim_action.php');
		include_once('claim_upload.php');
?>
<script>
$(document).ready(function() {
    //$("#frmClaim").validate();
});
  
$(document).on('change', '.chg_manual', function(){

  var cur_id = $(this).attr('id');
  var distance = $(this).val();
  var split_id = cur_id.split('manualdistance_');
  var record_id = split_id[1];
  
  if($("#travel_mode_"+record_id).val() == '9999'){
    $("#travel_cost_"+record_id).val(parseFloat(0).toFixed(2));
  }
  else{
    if(distance == ''){
      distance = $("#distance_"+record_id).val();
    }
    
    var travel_mode = $("#travel_mode_"+record_id).val();
    var travel_cost = '';
    if(travel_mode != ''){
      var val_arr = travel_mode.split('_');
      if(val_arr[1] > 0){
          travel_cost = parseFloat(val_arr[1])*parseFloat(distance);
      }
      $("#travel_cost_"+record_id).val(travel_cost);
    }
  }
});

$(document).on('change', '.sel_mode', function(){
  var cur_id = $(this).attr('alt');
  var distance = $("#manualdistance_"+cur_id).val();
  var cur_val = $(this).val();
  
  if(cur_val != '9999'){
    var val_arr = cur_val.split('_');
    var travel_cost = '';
    if(val_arr[1] > 0){
        travel_cost = parseFloat(val_arr[1])*parseFloat(distance);
    }
    $("#travel_cost_"+cur_id).val(travel_cost);
  }
  else{
    $("#travel_cost_"+cur_id).val(parseFloat(0).toFixed(2));
    $("#remark_"+cur_id).val('No Claim');
  }
});

</script>
<? } ?>
