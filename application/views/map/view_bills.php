<div class="modal fade" id="viewBillModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">	
  <div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4>Proof Uploaded</h4>
				<button type="button" class="close" data-dismiss="modal">×</button>
			</div>

				<div class="modal-body">
					<input type="hidden" name="fclaimid" id="fclaimid" value="" />
					<input type="hidden" name="fclaimtype" id="fclaimtype" value="" />
					<table class="table table-striped table-bordered" id="proofdata">
					
					</table>  
				</div>
				
				<div class="modal-footer">
					<a href="#" class="btn btn-default" data-dismiss="modal">Close</a>					
				</div>
		</div>
  </div>
</div>

<script type="text/javascript">
function view_bills(cid, ctype)
{
	$("#viewBillModal").modal("show");
	$("#fclaimid").val(cid);
	$("#fclaimtype").val(ctype);
	
	$.ajax({
			type: "POST",
			url: BASE_URL+"map/view_bill",
			data: {
				cid: cid,
				ctype: ctype
			},
			success: function (response) {
				$("#proofdata").html(response)
			},
			error: function (e) {
				//$("#result").text(e.responseText);
				console.log("ERROR : ", e);
			}
	});
	
}
</script>
