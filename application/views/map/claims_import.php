<? 
  if(!empty($claim_data)) { 
  $activity_date = ''; 
?>
<div class="scrollme">  
  <table cellspacing=0 cellpadding=2 border=1 width="100%">
	<thead>
		<tr>
      <th align="center">Claim Date</th>	
      <th align="center">Salary Head</th>	
      <th align="center">Claim Amount</th>	
      <th align="center">Status</th>	
      <th align="center">Status Date</th>	 
      <th align="center">Sanctioned Amount</th> 	
      <th align="center">Applicant</th>	
      <th align="center">Applicant Code</th>	
      <th align="center">Applicant Branch</th>	
      <th align="center">Remarks</th>	
      <th align="center">LEVEL</th>
      <th align="center">App No.</th>
      <th align="center">Company Code</th>
		</tr>
	</thead>
	<tbody>
	<? 
     $state_count = 0; 
     $k=1; 
     $import_claim_data = array();
     
     $salary_head = array('schoolvisit'=>'School Visit Conveyance', 'review_meeting'=>'Meeting Conveyance', 'training'=>'Training Conveyance', 'attendance-out'=>'Attendance-Out Conveyance');
   
    $activity_dates = array();
    foreach ($claim_data as $e_key){
    
    if($e_key->travel_mode_id != '9999'){ 
      $record_id = $e_key->record_id;
      $sel_travel_mode = '';  
      $actual_travel_cost = 0;
      $is_autocalculated = 0;
      
      foreach($travels_mode as $travel_mode) {
        if($e_key->travel_mode_id == $travel_mode->id){ 
          $sel_travel_mode =  $travel_mode->travel_mode; 
          $actual_travel_cost =  $e_key->travel_distance*$travel_mode->travel_cost;
          $is_autocalculated = $travel_mode->is_autocalculated;
        } 
      }  
      
      $duration = '--';
      $spark_name = '';
      $spark_code = '';
      // && $e_key->activity_id != 0
      if(strtolower($e_key->activity_type) != 'attendance-in'){
        //$activity_data = get_activity_data($e_key->activity_id, $e_key->activity_type);
        //$duration = $activity_data['duration'];
        //$user_id = $activity_data['user_id'];
        $user_id = $e_key->spark_id;
        $user_data = get_spark_data($user_id);
        $spark_name = $user_data['name'];
        $spark_code = $user_data['login_id'];
        //$spark_tally_app_no = $user_data['tally_app_no'];
      
        $activity_date = date('d-m-Y', strtotime($e_key->tracking_time));
        
        $tally_app_no = $e_key->tally_app_no;
        
        if(!in_array($activity_date, $activity_dates))
          array_push($activity_dates, $activity_date);
        
        //$key = $activity_date.'_'.$user_id;
        if($report_type == 'Summary Tally Report'){
					$key = $user_id."_".$e_key->activity_type;
				}
				else{
					$key = $e_key->record_id;
				}
        //$time_period = date('d-m-Y', strtotime($activitydatestart))." To ".date('d-m-Y', strtotime($activitydateend));
        //$remark = $tally_app_no."-".$spark_code."-".$spark_name."-".$time_period;
        $remark = $tally_app_no."-".$spark_code."-".$spark_name;
        
        if(!isset($import_claim_data[$key])){
          
          $tacking_data = get_tracking_data($e_key->track_source_id);
          
          $activity_location = trim(strip_tags($tacking_data['tracking_location']));
          $exp_loc = implode(', ', explode(',',$activity_location));
          
          $import_claim_data[$key]['activity_date'] = $activity_date;
          $import_claim_data[$key]['Salary Head'] = (isset($salary_head[$e_key->activity_type]) ? $salary_head[$e_key->activity_type] : $e_key->activity_type);
          $import_claim_data[$key]['Claim Amount'] = $e_key->travel_cost;
          $import_claim_data[$key]['Status Date'] = (!empty($e_key->process_date) ? date('d-m-Y', strtotime($e_key->process_date)) : date('d-m-Y'));
          $import_claim_data[$key]['Rejected Amount'] = ($e_key->claim_discard == 1 ? $e_key->travel_cost : 0);
          $import_claim_data[$key]['Applicant'] = $spark_name;
          $import_claim_data[$key]['Applicant Code'] = $spark_code;
          $import_claim_data[$key]['App No'] = $tally_app_no;
          $import_claim_data[$key]['Remark'] = $remark;
          
        }
        else{
          $import_claim_data[$key]['activity_date'] = $activity_date;
          $import_claim_data[$key]['Status Date'] = (!empty($e_key->process_date) ? date('d-m-Y', strtotime($e_key->process_date)) : date('d-m-Y'));
          $import_claim_data[$key]['Claim Amount'] = $import_claim_data[$key]['Claim Amount']+$e_key->travel_cost;
          //$import_claim_data[$key]['App No'] = $import_claim_data[$key]['App No']+1;
          if($e_key->claim_discard == 1)
            $import_claim_data[$key]['Rejected Amount'] = $import_claim_data[$key]['Rejected Amount']+$e_key->travel_cost;
        }
      }   
      $k++; 
    }
    } 
     //print_r($import_claim_data); 
     //die;
    $previous_key = '';
     
    function date_sort($a, $b) {
      return strtotime($a) - strtotime($b);
    }
    usort($activity_dates, "date_sort");
    
    $startdate = $activity_dates[0];
    $enddate = $activity_dates[count($activity_dates)-1];
    
    $time_period = $startdate." To ".$enddate;
     
    foreach($import_claim_data as $key=>$value){
        $exp = explode('_',$key);
        if($exp[0] != $previous_key)
          $activity_number = 1;  
        else
          $activity_number++;
      ?>
      <tr>
        <td align="center"><?=$value['activity_date'];?></td>	
        <td align="center"><?=$value['Salary Head'];?></td>	
        <td align="center"><?=$value['Claim Amount']; ?></td>	
        <td align="center">ACCEPT</td>	
        <td align="center"><?=$value['Status Date']; ?></td>	
        <td align="center"><?=($value['Claim Amount']-$value['Rejected Amount']); ?></td>
        <td align="center"><?=$value['Applicant']; ?></td>
        <td align="center"><?=$value['Applicant Code']; ?></td>
        <td align="center">SAMPARK</td>	
        <td align="center"><?=$value['Remark']."-".$time_period; ?></td>
        <td align="center">2</td>
        <td align="center">'<?=$value['App No']."-".$activity_number; ?></td>
        <td align="center">SAMPARK</td>
      </tr>
      <?  
        $previous_key = $exp[0];
     } ?>
  </tbody>
</table>  
</div> 
<? } ?>
