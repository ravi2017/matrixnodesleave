<? if(!isset($download) && !isset($pdf)) {?>
<form name="frmactivities" method="post" action="" class="">
  <div class="row">
    <div class="col-md-3">
      <div class="form-group">
        <input type="text" id="activitydatestart" name="activitydatestart" class="form-control" value="<? echo (!empty($activitydatestart) ? $activitydatestart : '');?>" autocomplete="off">
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <input type="text" id="activitydateend" name="activitydateend" class="form-control" value="<? echo (!empty($activitydateend) ? $activitydateend : '');?>" autocomplete="off">
      </div>
    </div>
    <?php if ($current_role != "field_user") { ?>
    <div class="col-md-6">
    <?php echo $this->load->view('team_members/select_current_spark','',true) ?>
    </div>
    <?php } ?>
    <div class="col-md-3">
      <div class="form-group">
        <input type="submit" name="submit" class="form-control btn btn-sm btn-success" value="Get Claims List">
      </div>
    </div>
  </div>
</form>
<? } ?>

<? if(!empty($claim_data)) { $activity_date = ''; ?>
<? if(!isset($download) && !isset($pdf)) {?>
<form name='frmClaim' id='frmClaim' method="post" action="<?php echo base_url();?>map/process_claim">
<div class="scrollme">
<table class="table table-striped table-bordered bootstrap-datatable datatable1 responsive customdatatable">
	<thead>
		<tr style="background-color: #d5ecf9;">
			<th align="center">S.No.</th>
			<th align="center">Activity <input type="hidden" name="spark_id" value="<?=$spark_id?>"></th>
			<th align="center">Details</th>
			<th align="center">Date<br>(DD:MM:YY)</th>
			<th align="center">Time <br>(HH:MM)</th>
			<th align="center">Duration <br>(HH:MM)</th>
			<th align="center">Distance <br>(KM)</th>
			<th align="center">Manual Distance <br>(KM)</th>
			<th align="center">Travel Mode</th>
			<th align="center">Travel Cost</th>
			<th align="center">Remark</th>
      <th align="center">
			<?php if($current_role == 'accounts') { echo 'Action'; }else{ echo 'Account Status'; } ?>
      </th>
      <? if($current_role == 'accounts') { ?>
      <th>App No.</th>
      <?	}	?>
		</tr>
	</thead>
	<tbody>
	<? $state_count = 0; $r=1; ?>
	<?php 
    
    $previous_travel_mode_id = '';
    $display_buttons = 1;
    $is_processed = 1;
    $previous_tally_no = '';
    foreach($claim_data as $e_key){ 
      $record_id = $e_key->record_id;
      $bg_color = '';
      $travel_mode_id = 0;
      $travel_cost = 0;
      $duration = '--';
      if(strtolower($e_key->activity_type) != 'attendance-out' && $e_key->activity_id != 0){
        $activity_data = get_activity_data($e_key->activity_id, $e_key->activity_type);
        $duration = $activity_data['duration'];
        $travel_mode_id = $activity_data['travel_mode_id'];
        $travel_cost = $activity_data['travel_cost'];
        $previous_travel_mode_id = $travel_mode_id;
      }
      
      if(strtolower($e_key->activity_type) == 'attendance-out'){
				if(empty($e_key->activity_id)){
					$travel_mode_id = $previous_travel_mode_id;
				}
				else{
					$activity_data = get_activity_data($e_key->activity_id, $e_key->activity_type);
					if(!empty($activity_data)){
						$duration = $activity_data['duration'];
						$travel_mode_id = $activity_data['travel_mode_id'];
						$travel_cost = $activity_data['travel_cost'];
					}
					else{
						$travel_mode_id = $previous_travel_mode_id;
					}
				}
      }
      
      if($previous_tally_no != $e_key->tally_app_no){
				$previous_activity = array($e_key->activity_type);
				$previous_tally_no = $e_key->tally_app_no;
			}
			
      
      if($activity_date != date('d-m-Y', strtotime($e_key->tracking_time))){
        $activity_date = date('d-m-Y', strtotime($e_key->tracking_time));
        
        $tacking_data = get_tracking_data($e_key->track_source_id);
        
        if($tacking_data['tracking_location'] != ''){
					$activity_location = trim(strip_tags($tacking_data['tracking_location']));
					$exp_loc = implode(',<br>', explode(',',$activity_location));
				}
				else{
					$tracking_latitude = $tacking_data['tracking_latitude'];
					$tracking_longitude = $tacking_data['tracking_longitude'];
					$exp_loc = 'Latitude : '.round($tracking_latitude, 2).'; Longitude : '.round($tracking_longitude, 2);
				}
        
        ?>
          <tr style="background-color:#A6A6A6;">
            <td><?=$r?></td>
            <td><? echo get_activity_type($tacking_data['activity_type']);?></td>
            <td><? echo $exp_loc;?></td>
            <td><?php echo date('d-m-Y', strtotime($tacking_data['tracking_time'])); ?></td>
            <td><?php echo date('H:i', strtotime($tacking_data['tracking_time'])); ?></td>
            <td>--</td>
            <td>--</td>
            <td>--</td>
            <td>--</td>
            <td>--</td>
            <td>--</td>
            <td>--</td>
            <?	if($current_role == 'accounts') {   ?>
            <td>--</td>
            <?	}	?>
          </tr>
        <?
        $r++;
      }
      $activity_type = get_activity_type($e_key->activity_type);
      
      $activity_location = trim(strip_tags($e_key->tracking_location));
      $exp_loc = implode(',<br>', explode(',',$activity_location));
      if($exp_loc == ''){
				$tracking_latitude = $e_key->tracking_latitude;
				$tracking_longitude = $e_key->tracking_longitude;
				$exp_loc = 'Latitude : '.round($tracking_latitude, 2).'; Longitude : '.round($tracking_longitude, 2);
			}
      
      $lat_long_location = trim(strip_tags($e_key->lat_long_location));
      
      if($activity_location != $lat_long_location)
      {
        $lat_long_location = implode(',<br>', explode(',',$lat_long_location));
        if($current_role == "super_admin")
          $exp_loc .= '<hr><span style="color:red">'.$lat_long_location.'</span>';
      }
      
      $remarks = ($e_key->remarks != '' ? $e_key->remarks : '&nbsp;');
      
      if($e_key->travel_mode_id == ''){
        $display_buttons = 0;
      }
      
      if(empty($e_key->is_claimed)){
        $is_processed = 0;
      }
      
      //if($e_key->travel_mode_id == '' && $travel_mode_id !='')
      if(empty($e_key->travel_mode_id) && $travel_mode_id !='')
      {
        $e_key->travel_mode_id = $travel_mode_id;
        $e_key->travel_cost = $travel_cost;
      }
      
      //if(strtolower($e_key->activity_type) == 'attendance-out'){
        //$e_key->travel_mode_id = $previous_travel_mode_id;
      //}
      
      $travelCost = '';
      foreach($travels_mode as $travel_mode) {
        if($e_key->travel_mode_id == $travel_mode->id){
          $tdistance = round($e_key->travel_distance);
          $travelCost = $tdistance*$travel_mode->travel_cost;
        }
      }
      
      if(empty($e_key->travel_cost) || $e_key->travel_cost == 0.00)
      {
        $e_key->travel_cost = $travelCost;
      }
      
      $readonly_fields = '';
      $disable_fields = '';
      if($e_key->claim_discard == 1){
        $readonly_fields = 'readonly';
        $disable_fields = ' disabled="true"';
        $bg_color = 'style="background-color:#fd6252;"';
      }
      
      $created_date = strtotime(date('Y-m-d', strtotime($e_key->created_at)));
      $current_date = strtotime(date('Y-m-d'));
      $days = (($current_date-$created_date)/(24*60*60));
      
      $not_update = '';
      if($days > 3 && ($current_role != 'accounts')){
				$readonly_fields = 'readonly';
				$not_update = 'not_update';
			}
  ?>
		<tr id="row_<?php echo $record_id;?>" <?=$bg_color;?>>
      <td><?=$r;?></td>
			<td>
        <input type="hidden" name="record_id[]" value="<?=$record_id?>">
        <?php echo $activity_type; ?>
      </td>
			<td><?php echo $exp_loc; ?></td>
			<td><?php echo date('d-m-Y', strtotime($e_key->tracking_time)); ?></td>
			<td><?php echo date('H:i', strtotime($e_key->tracking_time)); ?></td>
      <td><?=$duration;?></td>
			<td align="right">
        <input type="hidden" name="distance[]" id="distance_<?=$record_id;?>" value="<?=$e_key->travel_distance?>">
        <?php echo round($e_key->travel_distance); ?></td>
			<td>
        <input type="text" size="4" name="manualdistance[]" id="manualdistance_<?=$record_id;?>" value="<?=(!empty($e_key->manual_distance) && $e_key->manual_distance > 0 ? $e_key->manual_distance : round($e_key->travel_distance));?>" class="required chg_manual text-right" <?=$readonly_fields?>>
      </td>
			<td>
				<? if($not_update == '') { ?>
        <select name="travel_mode[]" id="travel_mode_<?=$record_id?>" alt="<?=$record_id?>" class="sel_mode form-control required">
          <option value="">-Mode-</option>
          <?php foreach($travels_mode as $travel_mode) { ?>
            <option value="<?=$travel_mode->id."_".$travel_mode->travel_cost;?>" <? if($e_key->travel_mode_id == $travel_mode->id){ echo 'selected'; } ?>>
              <?=$travel_mode->travel_mode;?>
            </option>
          <?php } ?>  
            <option value="9999" <? if(isset($travel_mode->id) && $e_key->travel_mode_id == '9999'){ echo 'selected'; } ?>>With Other</option>
        </select>
        <?	}else{	?>
					<select name="travel_mode[]" id="travel_mode_<?=$record_id?>" alt="<?=$record_id?>" class="sel_mode form-control required">
					  <? if($e_key->travel_mode_id == ''){ ?>
							<option value="">-Mode-</option>
						<?	}else if($e_key->travel_mode_id == '9999'){ ?>
							<option value="9999">With Other</option>
						<? } else { ?>
						<?php foreach($travels_mode as $travel_mode) { 
							if($e_key->travel_mode_id == $travel_mode->id){	?>
								<option value="<?=$travel_mode->id."_".$travel_mode->travel_cost;?>"><?=$travel_mode->travel_mode;?></option>
						<?php } } } ?>  
					</select>
				<?	}	?>	
      </td>
      <td><input type="text" name="travel_cost[]" id="travel_cost_<?=$record_id?>" size="4" value="<?=round($e_key->travel_cost);?>" maxlength="8" class="required text-right" <?=$readonly_fields?>></td>
      <td><textarea name="remarks[]" id="remark_<?=$record_id?>" cols="6" rows="2" maxlength="200" class="required" <?=$readonly_fields?>><?=$remarks;?></textarea>
      <input type="hidden" name="claim_processed[<?=$record_id?>]" value="<?=$e_key->is_claimed?>">
      <input type="hidden" name="claim_discard[<?=$record_id?>]" class="claim_discard_<?=$record_id?>" value="<?=$e_key->claim_discard?>">
      <input type="hidden" name="pre_mode" value="<?=$previous_travel_mode_id?>">
      <input type="hidden" name="tcost" value="<?=$travelCost?>">
      <input type="hidden" name="tcost" value="<?=$e_key->travel_distance?>">
      </td>
      <td>
        <?php 
        if($e_key->is_claimed == 1) {  
						$process_date = (!empty($e_key->process_date) ? '('.date('d-m-Y', strtotime($e_key->process_date)).')' : '');
						$user_data = get_spark_data($e_key->updated_by);
            ?><span id="rreason_<?=$record_id?>">Processed <?=$process_date." (By ".$user_data['name'].")";?></span><?
        }
        else{
        if($current_role == 'accounts') {  
          $btn_reject = 'display:block';
          $btn_approve = 'display:none';
          $reject_reason = '';
          if($e_key->claim_discard == 1){ 
            $btn_reject = 'display:none';
            $btn_approve = 'display:block';
            $reject_reason = $e_key->discard_reason." (Rejected)";
          } 
          ?>  
          <div id="approved_<?=$record_id?>" style="<?=$btn_approve?>">
            <input type="button" name="approved_claim" id="approved_claim" onClick="return approvedclaim('<?=$record_id?>')" class="btn btn-sm btn-success" value="Approve"> 
            <br /><span id="rreason_<?=$record_id?>"><?=$reject_reason;?></span>
          </div>
          <div id="discard_<?=$record_id?>" style="<?=$btn_reject?>">
            <input type="button" name="reject_claim" id="reject_claim" onClick="return discard_claim('<?=$record_id?>')" class="btn btn-sm btn-danger" value="Reject"> 
          </div>
        <? }else{ ?>  
          <span id="rreason_<?=$record_id?>"><?=$e_key->discard_reason;?></span>
        <?  } ?>  
        <? } ?>  
      </td>
      
      <? 
      if($current_role == 'accounts') {  
				$application_no = '';
				if($e_key->is_claimed == 1) {  
						if(!in_array($e_key->activity_type,$previous_activity)){
							array_push($previous_activity, $e_key->activity_type);
						}
						$activity_number = array_search($e_key->activity_type,$previous_activity)+1;
						$application_no = $e_key->tally_app_no."-".$activity_number;
				}
			?>
			<td><?=$application_no;?></td>
			<?	}	?>
		</tr>
	<?php $r++; }  ?>
  </tbody>
</table>  
</div>

<? if($current_role == 'accounts' && $is_processed == 0) { ?>
<div class="col-md-12" align="right" style="padding-top:10px;">
  <input type="submit" name="processClaim" value="Process Claim" class="btn btn-sm btn-info">
</div>
<?  } ?>
<? if($sel_spark_id == $current_user_id && $is_processed == 0) { ?>
<div class="col-md-12" align="right" style="padding-top:10px;">
  <input type="submit" name="processClaim" value="Update Claim" class="btn btn-sm btn-info">
</div>
<?  } ?>
</form>  

<div class="modal fade" id="discardModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4>Reject Claim</h4>
        <button type="button" class="close" data-dismiss="modal">×</button>
      </div>
      <form method="post" action="" role="form" name="frmdiscard" id="frmdiscard">
        <input type="hidden" name="claimid" id="claimid" value="">
        <div class="modal-body">                      
          <div class="row">
            <div class="form-group col-md-12">
              <textarea name="discard_reason" id="discard_reason" placeholder="Reason to reject" class="form-control" maxlength="200"></textarea>
            </div>
          </div>
          <div class="row">  
            <div class="form-group col-md-3">&nbsp;</div>
            <div class="form-group col-md-6">
              <label for="brc_name">&nbsp;</label>
              <input type="button" class="form-control" id="submit" name="submit" value="Save" class="btn btn-success" onClick="return save_discard_claim()">
            </div>  
          </div>  
        </div>  
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="approvedModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4>Approve Claim</h4>
        <button type="button" class="close" data-dismiss="modal">×</button>
      </div>
      <form method="post" action="" role="form" name="frmapproved" id="frmapproved">
        <input type="hidden" name="claimid" id="claimid" value="">
        <div class="modal-body">                      
          <div class="row">
            <div class="form-group col-md-12">
              <textarea name="approved_reason" id="approved_reason" placeholder="Reason to approved" class="form-control" maxlength="200"></textarea>
            </div>
          </div>
          <div class="row">  
            <div class="form-group col-md-3">&nbsp;</div>
            <div class="form-group col-md-6">
              <label for="brc_name">&nbsp;</label>
              <input type="button" class="form-control" id="submit" name="submit" value="Save" class="btn btn-success" onClick="return save_approved_claim()">
            </div>  
          </div>  
        </div>  
      </form>
    </div>
  </div>
</div>

<? }else{ ?>
  <table cellspacing=0 cellpadding=2 border=1 width="100%">
    <tr>
      <td colspan=5><strong>Spark Name :</strong> <? echo $spark_name?></td>
      <td colspan=6><strong>Duration :</strong> <?=$activitydatestart?> To <?=$activitydateend?></td>
    </tr>
  </table>  
  <table cellspacing=0 cellpadding=2 border=1>
	<thead>
		<tr>
      <th align="center">S.No.</th>
			<th align="center">Activity</th>
			<th align="center">Details</th>
			<th align="center">Date<br>(DD:MM:YY)</th>
			<th align="center">Time <br> (HH:MM)</th>
			<th align="center">Duration <br> (HH:MM)</th>
			<th align="center">Distance <br>(KM)</th>
			<th align="center">Manual Distance <br>(KM)</th>
			<th align="center">Travel Mode</th>
			<th align="center">Travel Cost</th>
			<th align="center">Remark</th>
		<?	if($current_role == 'accounts') {  ?>
			<th>App No.</th>
		<? }	?>	
		</tr>
	</thead>
	<tbody>
	<? $state_count = 0; 
     $k=1; 
     $total_travel_distance = 0;
     $total_manual_distance = 0;
     $total_travel_cost = 0;
     $previous_tally_no = '';
     $previous_activity_travel_mode = '';
  ?>
	<?php foreach ($claim_data as $e_key){
    
      
      
      if($previous_tally_no != $e_key->tally_app_no){
				$previous_activity = array($e_key->activity_type);
				$previous_tally_no = $e_key->tally_app_no;
			}
			
      if($activity_date != date('d-m-Y', strtotime($e_key->tracking_time))){
				
				$activity_number = 1;  
        $activity_date = date('d-m-Y', strtotime($e_key->tracking_time));
        
        $tacking_data = get_tracking_data($e_key->track_source_id);
        
        $activity_location = trim(strip_tags($tacking_data['tracking_location']));
        $exp_loc = implode(', ', explode(',',$activity_location));
        ?>
          <tr style="background-color:#A6A6A6;">
            <td align="center"><?=$k?></td>
            <td><? echo get_activity_type($tacking_data['activity_type']);?></td>
            <td><? echo $exp_loc;?></td>
            <td><?php echo date('d-m-Y', strtotime($tacking_data['tracking_time'])); ?></td>
            <td><?php echo date('H:i', strtotime($tacking_data['tracking_time'])); ?></td>
            <td>--</td>
            <td>--</td>
            <td>--</td>
            <td>--</td>
            <td>--</td>
            <td>--</td>
           <? if($current_role == 'accounts') {  ?>
            <td>--</td>
           <?	}	?> 
          </tr>
        <?
        $k++;
      }
   
   if($e_key->travel_mode_id != '9999' && $e_key->claim_discard != 1){ 
      $record_id = $e_key->record_id;
      $sel_travel_mode = '';
         
      $activity_type = get_activity_type($e_key->activity_type);
      
      $duration = '--';
      if(strtolower($e_key->activity_type) != 'attendance-out' && $e_key->activity_id != 0){
        $activity_data = get_activity_data($e_key->activity_id, $e_key->activity_type);
        $duration = $activity_data['duration'];
        $travel_mode_id = $activity_data['travel_mode_id'];
        //$travel_cost = $activity_data['travel_cost'];
        $previous_travel_mode_id = $travel_mode_id;
      } 
      if(strtolower($e_key->activity_type) == 'attendance-out'){
				if(empty($e_key->activity_id)){
					$travel_mode_id = $previous_travel_mode_id;
				}
				else{
					$activity_data = get_activity_data($e_key->activity_id, $e_key->activity_type);
					if(!empty($activity_data)){
						$duration = $activity_data['duration'];
						$travel_mode_id = $activity_data['travel_mode_id'];
						//$travel_cost = $activity_data['travel_cost'];
					}
					else{
						$travel_mode_id = $previous_travel_mode_id;
					}
				}
      }
      
      if(empty($e_key->travel_mode_id) && $travel_mode_id !='')
      {
        $e_key->travel_mode_id = $travel_mode_id;
        //$e_key->travel_cost = $travel_cost;
      }
      
      foreach($travels_mode as $travel_mode) {
          if($e_key->travel_mode_id == $travel_mode->id){ 
            $sel_travel_mode =  $travel_mode->travel_mode; 
          } 
      }   
      //if(strtolower($e_key->activity_type) == 'attendance-out' && $e_key->activity_id != 0){
        //$sel_travel_mode = $previous_activity_travel_mode;
      //}
      if($e_key->travel_mode_id != '9999'){
					$activity_location = trim(strip_tags($e_key->tracking_location));
					$exp_loc = implode(', ', explode(',',$activity_location));
					
					$total_travel_distance = round($e_key->travel_distance) + $total_travel_distance;
					$total_manual_distance = $e_key->manual_distance + $total_manual_distance;
					$total_travel_cost = round($e_key->travel_cost) + $total_travel_cost;
				?>
				<tr>
					<td align="center"><?=$k?></td>
					<td><?php echo $activity_type; ?></td>
					<td><?php echo $exp_loc; ?></td>
					<td><?php echo date('d-m-Y', strtotime($e_key->tracking_time)); ?></td>
					<td><?php echo date('H:i', strtotime($e_key->tracking_time)); ?></td>
					<td><?=$duration;?></td>
					<td align="right">
						<?php echo round($e_key->travel_distance); ?></td>
					<td align="right">
						<?=(!empty($e_key->manual_distance) ? $e_key->manual_distance : round($e_key->travel_distance));?>
					</td>
					<td><?=$sel_travel_mode;?></td>
					<td align="right"><?=round($e_key->travel_cost);?></td>
					<td><?=$e_key->remarks;?></td>
					<?
					if($current_role == 'accounts') {   
						$application_no = '';
						if($e_key->is_claimed == 1) {  
								if(!in_array($e_key->activity_type,$previous_activity)){
									array_push($previous_activity, $e_key->activity_type);
								}
								$activity_number = array_search($e_key->activity_type,$previous_activity)+1;
								$application_no = $e_key->tally_app_no."-".$activity_number;
						}
					?>
					<? if(isset($download)) {?>
						<td>'<?=$application_no;?></td>
					<? }else{ ?>
						<td><?=$application_no;?></td>
					<?	}	?>	
				<?	}	?>	
				</tr>
				<?php $k++; 
				} 
			}	
		} ?>
    <tr>
      <td colspan=6></td>
      <td align="right"><?=$total_travel_distance;?></td>
      <td align="right"><?=$total_manual_distance;?></td>
      <td>&nbsp;</td>
      <td align="right"><?=$total_travel_cost;?></td>
      <td>&nbsp;</td>
    </tr>
  </tbody>
</table>  
 
<? } ?>
<? } ?>

<? // && !empty($display_buttons)
if(!isset($download) && !isset($pdf) && !empty($claim_data))  { ?>
    <br />
    <div class="row">
      <? if($current_role == 'super_admin' || $current_role == 'accounts') { ?>
      <div class="form-group col-md-3">
      <form name="frmreport" action="<?php echo base_url();?>map/claims_list" method="post">
          <input type="hidden" name="activitydatestart" value="<? echo (!empty($activitydatestart) ? $activitydatestart : '');?>">
          <input type="hidden" name="activitydateend" value="<? echo (!empty($activitydateend) ? $activitydateend : '');?>">
          <input type="hidden" name="spark_id" value="<?=$spark_id?>">
          <input type="hidden" name="spark_state_id" value="<?=$spark_state_id?>">
          <input type="hidden" name="download" value="1">
          <button type="submit" name="submit" value="Get Claims List" class="btn btn-success">Download Report</button>
      </form>
      </div>
      <?  } ?>
      <div class="form-group col-md-3">
      <form name="frmreport" action="<?php echo base_url();?>map/claims_list" method="post">
          <input type="hidden" name="activitydatestart" value="<? echo (!empty($activitydatestart) ? $activitydatestart : '');?>">
          <input type="hidden" name="activitydateend" value="<? echo (!empty($activitydateend) ? $activitydateend : '');?>">
          <input type="hidden" name="spark_id" value="<?=$spark_id?>">
          <input type="hidden" name="spark_state_id" value="<?=$spark_state_id?>">
          <input type="hidden" name="pdf" value="1">
        <button type="submit" name="submit" value="Get Claims List" class="btn btn-success">Download PDF</button>
      </form>
      </div>
    </div>
<? } ?>


<? if(!isset($download) && !isset($pdf)) {?>

<script>
$(document).ready(function() {
    //$("#frmClaim").validate();
});
  
$(document).on('change', '.chg_manual', function(){

  var cur_id = $(this).attr('id');
  var distance = $(this).val();
  var split_id = cur_id.split('manualdistance_');
  var record_id = split_id[1];
  
  if($("#travel_mode_"+record_id).val() == '9999'){
    $("#travel_cost_"+record_id).val(parseFloat(0).toFixed(2));
  }
  else{
    if(distance == ''){
      distance = $("#distance_"+record_id).val();
    }
    
    var travel_mode = $("#travel_mode_"+record_id).val();
    var travel_cost = '';
    if(travel_mode != ''){
      var val_arr = travel_mode.split('_');
      if(val_arr[1] > 0){
          travel_cost = parseFloat(val_arr[1])*parseFloat(distance);
      }
      $("#travel_cost_"+record_id).val(travel_cost);
    }
  }
});

$(document).on('change', '.sel_mode', function(){
  var cur_id = $(this).attr('alt');
  var distance = $("#manualdistance_"+cur_id).val();
  var cur_val = $(this).val();
  
  if(cur_val != '9999'){
    var val_arr = cur_val.split('_');
    var travel_cost = '';
    if(val_arr[1] > 0){
        travel_cost = parseFloat(val_arr[1])*parseFloat(distance);
    }
    $("#travel_cost_"+cur_id).val(travel_cost);
  }
  else{
    $("#travel_cost_"+cur_id).val(parseFloat(0).toFixed(2));
    $("#remark_"+cur_id).val('No Claim');
  }
});

function discard_claim(cid)
{
  $("#discard_reason").val('');
  $("#claimid").val(cid);
  $("#discardModel").modal("show");
}

function save_discard_claim()
{
  var claim_id = $("#claimid").val();
  var reason = $("#discard_reason").val();
  
  if(reason == ''){
    alert('Please add reason');
    return false;
  }
  
  $.ajax({
      url: '<?=base_url()?>map/discard_claim',
      type: 'post',
      data: {
        claim_id: claim_id,
        reason : reason
      },
      success: function(response) {
        $("#discard_"+claim_id).hide();
        $("#approved_"+claim_id).show();
        $("#discardModel").modal("hide");
        $("#rreason_"+claim_id).html(reason);
        $(".claim_discard_"+claim_id).val('1');
        $("#row_"+claim_id).css("background-color", "#fd6252");
      },
      error: function(response) {
        window.console.log(response);
      }
    });
}


function approvedclaim(cid)
{
  $("#approved_reason").val('');
  $("#claimid").val(cid);
  $("#approvedModel").modal("show");
}

function save_approved_claim()
{
  var claim_id = $("#claimid").val();
  var reason = $("#approved_reason").val();
  if(reason == ''){
    alert('Please add reason');
    return false;
  }
  $.ajax({
      url: '<?=base_url()?>map/approved_claim',
      type: 'post',
      data: {
        claim_id: claim_id,
        reason : reason
      },
      success: function(response) {
        $("#approvedModel").modal("hide");
        $("#discard_"+claim_id).show();
        $("#approved_"+claim_id).hide();
        $(".claim_discard_"+claim_id).val('0');
        $("#row_"+claim_id).css("background-color", "#FFFFFF");
      },
      error: function(response) {
        window.console.log(response);
      }
    });
}

/*function discard_claim(cid)  
{
  if(confirm('Are you sure to discard this claim')){
    $.ajax({
      url: '<?=base_url()?>map/discard_claim',
      type: 'post',
      data: {
        claim_id: cid
      },
      success: function(response) {
        if(response == 'Discard'){
          $("#row_"+cid).css("background-color", "#FFFFFF");
        }else{
          $("#row_"+cid).css("background-color", "#A6A6A6");
        }
        $("#sp_"+cid).html(response);
      },
      error: function(response) {
        window.console.log(response);
      }
    });
  }
}*/
</script>

<? } ?>
