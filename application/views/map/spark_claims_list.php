<? if(!isset($download) && !isset($pdf)) {
	include_once('top_form.php');
} ?>

<? if(!isset($download) && !isset($pdf) && $count_claim_data > 0) { ?>
<div class="scrollme">

<!-- START LOCAL CONV CLAIM -->	
<?
$display_buttons = 1;
$is_processed = 1;
$net_claim_cost = 0;
$net_local_claim_cost = 0;
$net_ld_claim_cost = 0;
$net_stay_claim_cost = 0;
$net_stay_bill_cost = 0;
$net_food_actual_claim_cost = 0;
$net_food_claim_cost = 0;
$net_other_claim_cost = 0;
?>
<form name='frmClaim' id='frmClaim' method="post" action="<?php echo base_url();?>map/process_all_claim">
<input type="hidden" name="activitydatestart" value="<? echo (!empty($activitydatestart) ? $activitydatestart : '');?>">
<input type="hidden" name="activitydateend" value="<? echo (!empty($activitydateend) ? $activitydateend : '');?>">
<input type="hidden" name="spark_id" value="<?=$spark_id?>">
<input type="hidden" name="spark_state_id" value="<?=$spark_state_id?>">
<div class="row"><div class="col-md-12 text-center"  style="background-color: #A6A6A6;"><h4>Local Convence Claim</h4></div></div>
<table class="table table-striped table-bordered bootstrap-datatable datatable1 responsive customdatatable withscrolltable">
	<thead>
		<tr style="background-color: #d5ecf9;">
			<th align="center">S.No.</th>
			<th align="center">Activity <input type="hidden" name="spark_id" value="<?=$spark_id?>"></th>
			<th align="center">Details</th>
			<th align="center">Date<br>(DD:MM:YY)</th>
			<th align="center">Time <br>(HH:MM)</th>
			<th align="center">Duration <br>(HH:MM)</th>
			<th align="center">Distance <br>(KM)</th>
			<th align="center">Manual Distance <br>(KM)</th>
			<th align="center">Travel Mode</th>
			<th align="center">Travel Cost</th>
			<th align="center">Remark</th>
      <th align="center">
			<?php if($current_role == 'accounts') { echo 'Action'; }else{ echo 'Account Status'; } ?>
      </th>
		</tr>
	</thead>
	<tbody>
	<?php 
    $r=1;
    $activity_date = '';
    $previous_travel_mode_id = '';
    foreach($local_claim_data as $e_key){ 
      $record_id = $e_key->record_id;
      $bg_color = '';
      $travel_mode_id = 0;
      $travel_cost = 0;
      $duration = '--';
      if(strtolower($e_key->activity_type) != 'attendance-out' && $e_key->activity_id != 0){
        $activity_data = get_activity_data($e_key->activity_id, $e_key->activity_type);
        $duration = $activity_data['duration'];
        $travel_mode_id = $activity_data['travel_mode_id'];
        $travel_cost = $activity_data['travel_cost'];
        $previous_travel_mode_id = $travel_mode_id;
      }
      
      if(strtolower($e_key->activity_type) == 'attendance-out'){
				if(empty($e_key->activity_id)){
					$travel_mode_id = $previous_travel_mode_id;
				}
				else{
					$activity_data = get_activity_data($e_key->activity_id, $e_key->activity_type);
					if(!empty($activity_data)){
						$duration = $activity_data['duration'];
						$travel_mode_id = $activity_data['travel_mode_id'];
						$travel_cost = $activity_data['travel_cost'];
					}
					else{
						$travel_mode_id = $previous_travel_mode_id;
					}
				}
      }
      
      if($activity_date != date('d-m-Y', strtotime($e_key->tracking_time))){
        $activity_date = date('d-m-Y', strtotime($e_key->tracking_time));
        
        $tacking_data = get_tracking_data($e_key->track_source_id);
        
        $activity_location = trim(strip_tags($tacking_data['tracking_location']));
        $exp_loc = implode(',<br>', explode(',',$activity_location));
        
        ?>
          <tr style="background-color:#A6A6A6;">
            <td><?=$r?></td>
            <td><? echo get_activity_type($tacking_data['activity_type']);?></td>
            <td><? echo $exp_loc;?></td>
            <td><?php echo date('d-m-Y', strtotime($tacking_data['tracking_time'])); ?></td>
            <td><?php echo date('H:i', strtotime($tacking_data['tracking_time'])); ?></td>
            <td>--</td>
            <td>--</td>
            <td>--</td>
            <td>--</td>
            <td>--</td>
            <td>--</td>
            <td>--</td>
          </tr>
        <?
        $r++;
      }
      $activity_type = get_activity_type($e_key->activity_type);
      
      $activity_location = trim(strip_tags($e_key->tracking_location));
      $exp_loc = implode(',<br>', explode(',',$activity_location));
      
      $lat_long_location = trim(strip_tags($e_key->lat_long_location));
        
      if($activity_location != $lat_long_location)
      {
        $lat_long_location = implode(',<br>', explode(',',$lat_long_location));
        if($current_role == "super_admin")
          $exp_loc .= '<hr><span style="color:red">'.$lat_long_location.'</span>';
      }
      
      $remarks = ($e_key->remarks != '' ? $e_key->remarks : '&nbsp;');
      
      if($e_key->travel_mode_id == ''){
        $display_buttons = 0;
      }
      
      if(empty($e_key->is_claimed)){
        $is_processed = 0;
      }
      
      //if($e_key->travel_mode_id == '' && $travel_mode_id !='')
      if(empty($e_key->travel_mode_id) && $travel_mode_id !='')
      {
        $e_key->travel_mode_id = $travel_mode_id;
        $e_key->travel_cost = $travel_cost;
      }
      
      //if(strtolower($e_key->activity_type) == 'attendance-out'){
        //$e_key->travel_mode_id = $previous_travel_mode_id;
      //}
      
      $travelCost = '';
      foreach($travels_mode as $travel_mode) {
        if($e_key->travel_mode_id == $travel_mode->id){
          $tdistance = round($e_key->travel_distance);
          $travelCost = $tdistance*$travel_mode->travel_cost;
        }
      }
      
      if(empty($e_key->travel_cost) || $e_key->travel_cost == 0.00)
      {
        $e_key->travel_cost = $travelCost;
      }
      
      $readonly_fields = '';
      $disable_fields = '';
      if($e_key->claim_discard == 1){
        $readonly_fields = 'readonly';
        $disable_fields = ' disabled="true"';
        $bg_color = 'style="background-color:#fd6252;"';
      }
      
      $created_date = strtotime(date('Y-m-d', strtotime($e_key->created_at)));
      $current_date = strtotime(date('Y-m-d'));
      $days = (($current_date-$created_date)/(24*60*60));
      
      $not_update = '';
      if($days > 3 && ($current_role != 'accounts')){
				$readonly_fields = 'readonly';
				$not_update = 'not_update';
			}
			
			$net_local_claim_cost = $net_local_claim_cost + round($e_key->travel_cost);
  ?>
		<tr id="row_<?php echo $record_id;?>" <?=$bg_color;?>>
      <td><?=$r;?></td>
			<td>
        <input type="hidden" name="record_id[]" value="<?=$record_id?>">
        <?php echo $activity_type; ?>
      </td>
			<td><?php echo $exp_loc; ?></td>
			<td><?php echo date('d-m-Y', strtotime($e_key->tracking_time)); ?></td>
			<td><?php echo date('H:i', strtotime($e_key->tracking_time)); ?></td>
      <td><?=$duration;?></td>
			<td align="right">
				<?php echo round($e_key->travel_distance); ?>
        <input type="hidden" name="tracking_time[]" id="distance_<?=$record_id;?>" value="<?=$e_key->tracking_time?>">
        <input type="hidden" name="distance[]" id="distance_<?=$record_id;?>" value="<?=$e_key->travel_distance?>">
      </td>
			<td>
				<?=(!empty($e_key->manual_distance) && $e_key->manual_distance > 0 ? $e_key->manual_distance : round($e_key->travel_distance));?>
        <!--<input type="text" size="4" name="manualdistance[]" id="manualdistance_<?=$record_id;?>" value="<?=(!empty($e_key->manual_distance) && $e_key->manual_distance > 0 ? $e_key->manual_distance : round($e_key->travel_distance));?>" class="required chg_manual text-right" <?=$readonly_fields?>>-->
      </td>
			<td>
				<? /*if($not_update == '') { ?>
        <select name="travel_mode[]" id="travel_mode_<?=$record_id?>" alt="<?=$record_id?>" class="sel_mode form-control required">
          <option value="">-Mode-</option>
          <?php foreach($travels_mode as $travel_mode) { ?>
            <option value="<?=$travel_mode->id."_".$travel_mode->travel_cost;?>" <? if($e_key->travel_mode_id == $travel_mode->id){ echo 'selected'; } ?>>
              <?=$travel_mode->travel_mode;?>
            </option>
          <?php } ?>  
            <option value="9999" <? if(isset($travel_mode->id) && $e_key->travel_mode_id == '9999'){ echo 'selected'; } ?>>With Other</option>
        </select>
        <?	}else{ */	?>
					<select name="travel_mode[]" id="travel_mode_<?=$record_id?>" alt="<?=$record_id?>" class="sel_mode form-control required">
					  <? if($e_key->travel_mode_id == ''){ ?>
							<option value="">-Mode-</option>
						<?	}else if($e_key->travel_mode_id == '9999'){ ?>
							<option value="9999">With Other</option>
						<? } else { ?>
						<?php foreach($travels_mode as $travel_mode) { 
							if($e_key->travel_mode_id == $travel_mode->id){	?>
								<option value="<?=$travel_mode->id."_".$travel_mode->travel_cost;?>"><?=$travel_mode->travel_mode;?></option>
						<?php } } } ?>  
					</select>
				<?	//}	?>	
      </td>
      <td><?=round($e_key->travel_cost);?>
				<!--<input type="text" name="travel_cost[]" id="travel_cost_<?=$record_id?>" size="4" value="<?=round($e_key->travel_cost);?>" maxlength="8" class="required text-right" <?=$readonly_fields?>>-->
			</td>
      <td><?=$remarks;?>
				<!--<textarea name="remarks[]" id="remark_<?=$record_id?>" cols="6" rows="2" maxlength="200" class="required" <?=$readonly_fields?>><?=$remarks;?></textarea>-->
      <input type="hidden" name="claim_processed[<?=$record_id?>]" value="<?=$e_key->is_claimed?>">
      <input type="hidden" name="claim_discard[<?=$record_id?>]" class="claim_discard_<?=$record_id?>" value="<?=$e_key->claim_discard?>">
      <input type="hidden" name="pre_mode" value="<?=$previous_travel_mode_id?>">
      <input type="hidden" name="tcost" value="<?=$travelCost?>">
      <input type="hidden" name="tcost" value="<?=$e_key->travel_distance?>">
      </td>
      <td>
        <?php 
        if($e_key->is_claimed == 1) {  
						$process_date = (!empty($e_key->process_date) ? '('.date('d-m-Y', strtotime($e_key->process_date)).')' : '');
						$user_data = get_spark_data($e_key->updated_by);
            ?><span id="rreason_<?=$record_id?>">Processed <?=$process_date." (By ".$user_data['login_id'].")";?></span><?
        }
        else{
        if($current_role == 'accounts') {  
          $btn_reject = 'display:block';
          $btn_approve = 'display:none';
          $reject_reason = '';
          if($e_key->claim_discard == 1){ 
            $btn_reject = 'display:none';
            $btn_approve = 'display:block';
            $reject_reason = $e_key->discard_reason." (Rejected)";
          } 
          ?>  
          <div id="approved_<?=$record_id?>" style="<?=$btn_approve?>">
            <input type="button" name="approved_claim" id="approved_claim" onClick="return approved_lclaim('<?=$record_id?>')" class="btn btn-sm btn-success" value="Approve"> 
            <br /><span id="rreason_<?=$record_id?>"><?=$reject_reason;?></span>
          </div>
          <div id="discard_<?=$record_id?>" style="<?=$btn_reject?>">
            <input type="button" name="reject_claim" id="reject_claim" onClick="return discard_claim('<?=$record_id?>')" class="btn btn-sm btn-danger" value="Reject"> 
          </div>
        <? } ?>  
          <span id="rreason_<?=$record_id?>"><?=$e_key->discard_reason;?></span>
        <? } ?>  
      </td>
      
		</tr>
	<?php $r++; } ?>
  </tbody>
</table>

<? /*if($current_role == 'accounts' && $is_processed == 0) { ?>
<div class="col-md-12" align="right" style="padding-top:10px;">
  <input type="submit" name="processClaim" value="Process Other Claim" class="btn btn-sm btn-info">
</div>
<?  } ?>
<? if($sel_spark_id == $current_user_id && $is_processed == 0) { ?>
<!--<div class="col-md-12" align="right" style="padding-top:10px;">
  <input type="submit" name="processClaim" value="Update Claim" class="btn btn-sm btn-info">
</div>-->
<?  }*/ ?>
<!--<br /></form>-->
<!-- END LOCAL CONV CLAIM -->	

<!-- START LD TRAVEL CLAIM-->
<?
//$display_buttons = 1;
//$is_processed = 1;
?>	
<!--<form name='frmClaim' id='frmClaim' method="post" action="<?php echo base_url();?>map/process_ld_travel">-->
<div class="row"><div class="col-md-12 text-center"  style="background-color: #A6A6A6;"><h4>LD Travel Claim</h4></div></div>
<table class="table table-striped table-bordered bootstrap-datatable datatable1 responsive customdatatable withscrolltable">
	<thead>
		<tr style="background-color: #d5ecf9;">
			<th align="center">S.No.<input type="hidden" name="spark_id" value="<?=$spark_id?>"></th>
			<th align="center">Details</th>
			<th align="center">Date<br>(DD:MM:YY)</th>
			<th align="center">Time <br>(HH:MM)</th>
			<th align="center">Duration <br>(HH:MM)</th>
			<th align="center">Distance <br>(KM)</th>
			<th align="center">Manual Distance <br>(KM)</th>
			<th align="center">View Bill</th>
			<th align="center">Travel Mode</th>
			<th align="center">Travel Cost</th>
			<th align="center">Remark</th>
      <th align="center">
			<?php if($current_role == 'accounts') { echo 'Action'; }else{ echo 'Account Status'; } ?>
      </th>
		</tr>
	</thead>
	<tbody>

	<?php 
    $r=1;
    foreach($ld_claim_data as $e_key){ 
      $record_id = $e_key->id;
      $bg_color = '';
			$duration = $e_key->duration;
			$travel_mode_id = $e_key->travel_mode_id;
			$travel_cost = $e_key->travel_cost;
			
      $activity_type = 'LD Travel';
      
      $uploadData = get_upload_file(array('claim_type'=>'LD Travel', 'claim_id'=>$e_key->id));
      
      $file_name = '';
      if(!empty($uploadData))
      $file_name = $uploadData[0]->file_name; 
      
      $activity_location = trim(strip_tags($e_key->from_address));
      $exp_loc = '<strong>IN:</strong> '.implode(',<br>', explode(',',$activity_location));
      
      $lat_long_location = trim(strip_tags($e_key->to_address));
        
      //if($activity_location != $lat_long_location)
      //{
        $lat_long_location = implode(',<br>', explode(',',$lat_long_location));
        //if($current_role == "super_admin")
         $exp_loc .= '<hr><strong>OUT:</strong> '.$lat_long_location;
      //}
      
      $status_remark = ($e_key->status_remark != '' ? $e_key->status_remark : '&nbsp;');
      $remarks = ($e_key->remarks != '' ? $e_key->remarks : '&nbsp;');
      
      if($e_key->travel_mode_id == ''){
        $display_buttons = 0;
      }
      
      if($e_key->claim_status == 'approved'){
        $is_processed = 0;
      }
      
      if(empty($e_key->travel_mode_id) && $travel_mode_id !='')
      {
        $e_key->travel_mode_id = $travel_mode_id;
        $e_key->travel_cost = $travel_cost;
      }
      
      $travelCost = '';
      foreach($travels_mode as $travel_mode) {
        if($e_key->travel_mode_id == $travel_mode->id){
          $tdistance = round($e_key->travel_distance);
          $travelCost = $tdistance*$travel_mode->travel_cost;
        }
      }
      
      if(empty($e_key->travel_cost) || $e_key->travel_cost == 0.00)
      {
        $e_key->travel_cost = $travelCost;
      }
      
      $readonly_fields = '';
      $disable_fields = '';
      if($e_key->claim_status == 'rejected'){
        $readonly_fields = 'readonly';
        $disable_fields = ' disabled="true"';
        $bg_color = 'style="background-color:#fd6252;"';
      }
      
      $created_date = strtotime(date('Y-m-d', strtotime($e_key->created_at)));
      $current_date = strtotime(date('Y-m-d'));
      $days = (($current_date-$created_date)/(24*60*60));
      
      $not_update = '';
      if($days > 3 && ($current_role != 'accounts')){
				//$readonly_fields = 'readonly';
				$not_update = 'not_update';
			}
			
			$net_ld_claim_cost = $net_ld_claim_cost + round($e_key->travel_cost);
  ?>
		<tr id="other_activities_row_<?php echo $record_id;?>" <?=$bg_color;?>>
      <td><?=$r;?><input type="hidden" name="record_id[]" value="<?=$record_id?>"></td>
			<td><?php echo $exp_loc; ?></td>
			<td><?php echo date('d-m-Y', strtotime($e_key->activity_date)); ?></td>
			<td><?php echo date('H:i', strtotime($e_key->activity_date)); ?></td>
      <td><?=$duration;?></td>
			<td align="right">
				<?php echo round($e_key->travel_distance); ?>
        <input type="hidden" name="tracking_time[]" id="distance_<?=$record_id;?>" value="<?=$e_key->activity_date?>">
        <input type="hidden" name="distance[]" id="distance_<?=$record_id;?>" value="<?=$e_key->travel_distance?>">
      </td>
			<td><? echo (!empty($e_key->manual_distance) && $e_key->manual_distance > 0 ? $e_key->manual_distance : round($e_key->travel_distance));?>
        <!--<input type="text" size="4" name="manualdistance[]" id="manualdistance_<?=$record_id;?>" value="<?=(!empty($e_key->manual_distance) && $e_key->manual_distance > 0 ? $e_key->manual_distance : round($e_key->travel_distance));?>" class="required chg_manual text-right" <?=$readonly_fields?>>-->
      </td>
      <td>
				<? if($file_name != '') {?>
				<a href="<?php echo base_url().$file_name;?>" target="_blank" class="viewfile" download>View Bill</a>
				<?	} else {?>
					<? /*if($e_key->claim_status == 'approved'){ ?>
							<div id="uplodedfile_<?=$record_id?>"><a href="javascript:void(0)" onclick="return upload_bill('<?=$record_id?>', 'LD Travel')">Upload Bill</a></div>
					<?	}else{	?>	
						<div id="uplodedfile_<?=$record_id?>">Upload Bill</div>
					<? }*/ ?>
					<div id="uplodedfile_<?=$record_id?>">No Bill</div>
        <?	}	?>
      </td>
			<td>
				<? /*if($not_update == '') { ?>
        <select name="travel_mode[]" id="travel_mode_<?=$record_id?>" alt="<?=$record_id?>" class="sel_mode form-control required">
          <option value="">-Mode-</option>
          <?php foreach($travels_mode as $travel_mode) { ?>
            <option value="<?=$travel_mode->id."_".$travel_mode->travel_cost;?>" <? if($e_key->travel_mode_id == $travel_mode->id){ echo 'selected'; } ?>>
              <?=$travel_mode->travel_mode;?>
            </option>
          <?php } ?>  
            <option value="9999" <? if(isset($travel_mode->id) && $e_key->travel_mode_id == '9999'){ echo 'selected'; } ?>>With Other</option>
        </select>
        <?	}else{ */	?>
					<select name="travel_mode[]" id="travel_mode_<?=$record_id?>" alt="<?=$record_id?>" class="sel_mode form-control required">
					  <? if($e_key->travel_mode_id == ''){ ?>
							<option value="">-Mode-</option>
						<?	}else if($e_key->travel_mode_id == '9999'){ ?>
							<option value="9999">With Other</option>
						<? } else { ?>
						<?php foreach($travels_mode as $travel_mode) { 
							if($e_key->travel_mode_id == $travel_mode->id){	?>
								<option value="<?=$travel_mode->id."_".$travel_mode->travel_cost;?>"><?=$travel_mode->travel_mode;?></option>
						<?php } } } ?>  
					</select>
				<?	//}	?>	
      </td>
      <td><?=round($e_key->travel_cost);?>
				<!--<input type="text" name="travel_cost[]" id="travel_cost_<?=$record_id?>" size="4" value="<?=round($e_key->travel_cost);?>" maxlength="8" class="required text-right" <?=$readonly_fields?>>--></td>
      <td><?=$remarks;?>
				<!--<textarea name="remarks[]" id="remark_<?=$record_id?>" cols="6" rows="2" maxlength="200" class="required" <?=$readonly_fields?>><?=$remarks;?></textarea>-->
      <input type="hidden" name="claim_status[<?=$record_id?>]" value="<?=$e_key->claim_status?>">
      <input type="hidden" name="claim_processed[<?=$record_id?>]" value="<?=$e_key->claim_status?>">
      </td>
      <td>
        <?php 
        if($e_key->claim_status == 'processed') {  
						$process_date = (!empty($e_key->process_date) ? '('.date('d-m-Y', strtotime($e_key->process_date)).')' : '');
						$user_data = get_spark_data($e_key->updated_by);
            ?><span id="rreason_<?=$record_id?>">Processed <?=$process_date." (By ".$user_data['login_id'].")";?></span><?
        }
        else{
        if($current_role == 'accounts') {  
          $btn_reject = 'display:block';
          $btn_referbk = 'display:block';
          $status_remark = $e_key->status_remark;
          
          if($e_key->claim_status == 'rejected' || $e_key->claim_status == 'referback'){ 
            $btn_reject = 'display:none';
            $btn_referbk = 'display:none';
          } 
          ?>  
          <div id="other_activities_referback_<?=$record_id?>" style="<?=$btn_referbk?>">
            <input type="button" name="referback_claim" id="referback_claim" onClick="return approvedclaim('<?=$record_id?>', 'referback', 'Refer Back','other_activities')" class="btn btn-sm btn-success" value="Refer Back"> 
          </div>
          <div id="other_activities_discard_<?=$record_id?>" style="<?=$btn_reject?>">
						<br />
            <input type="button" name="reject_claim" id="reject_claim" onClick="return approvedclaim('<?=$record_id?>', 'rejected', 'Reject','other_activities')" class="btn btn-sm btn-danger" value="Reject"> 
          </div>
        <? }
        if($current_role == 'state_person' || $current_role == 'manager') {  
					if($e_key->claim_status == 'referback'){ 
						?>
						<div id="other_activities_approved_<?=$record_id?>">
							<input type="button" name="approved_claim" id="approved_claim" onClick="return approvedclaim('<?=$record_id?>', 'approved', 'Approve','other_activities')" class="btn btn-sm btn-success" value="Approve"> 
						</div>
						<div id="other_activities_discard_<?=$record_id?>">
							<br />
							<input type="button" name="reject_claim" id="reject_claim" onClick="return approvedclaim('<?=$record_id?>', 'rejected', 'Reject','other_activities')" class="btn btn-sm btn-danger" value="Reject"> 
						</div>
						<?
					}
				}
       ?>  
          <div><span id="other_activities_rreason_<?=$record_id?>"><?=$e_key->status_remark;?></span></div>
        <? } ?>  
      </td>
		</tr>
	<?php $r++; }  ?>
  </tbody>
  <? if(count($ld_claim_data) > 0){ ?>
	 <tfoot>
		<tr>
			<th colspan="9" style="text-align:right">Total Cost</th>
			<th colspan="3" style="text-align:left"><?=$net_ld_claim_cost;?></th>
		</tr>
	</tfoot>
	<?	}	?>
</table>  

<? /*if($current_role == 'accounts' && $is_processed == 0) { ?>
<div class="col-md-12" align="right" style="padding-top:10px;">
  <input type="submit" name="processClaim" value="Process Other Claim" class="btn btn-sm btn-info">
</div>
<?  } ?>
<? if($sel_spark_id == $current_user_id && $is_processed == 0) { ?>
<!--<div class="col-md-12" align="right" style="padding-top:10px;">
  <input type="submit" name="processClaim" value="Update Claim" class="btn btn-sm btn-info">
</div>-->
<?  }*/ ?>
<!--<br /></form>-->
<!-- END LD TRAVEL CLAIM-->

<!-- START STAY CLAIM -->	
<?
//$display_buttons = 1;
//$is_processed = 1;
?>	
<!--<form name='frmClaim' id='frmClaim' method="post" action="<?php echo base_url();?>map/process_stay_claims">-->
<div class="row"><div class="col-md-12 text-center"  style="background-color: #A6A6A6;"><h4>Stay Claim</h4></div></div>
<table class="table table-striped table-bordered bootstrap-datatable datatable1 responsive customdatatable withscrolltable">
	<thead>
		<tr style="background-color: #d5ecf9;">
			<th align="center">S.No.</th>
			<th align="center">Claim Duration</th>
			<th align="center">Activity <input type="hidden" name="spark_id" value="<?=$spark_id?>"></th>
			<th align="center">Stay With</th>
			<th align="center">Bill Details</th>
			<th align="center">Bill Cost</th>
			<th align="center">Claim Cost</th>
			<th align="center">View Bill</th>
			<th align="center">Remark</th>
      <th align="center">
			<?php if($current_role == 'accounts') { echo 'Action'; }else{ echo 'Account Status'; } ?>
      </th>
		</tr>
	</thead>
	<tbody>
	<?php 
		$r=1;
    foreach($stay_claim_data as $e_key){ 

      $bg_color = '';
      $record_id = $e_key->id;
      $comment = ($e_key->comment != '' ? $e_key->comment : '&nbsp;');
      $readonly_fields = '';
      $disable_fields = '';
      if($e_key->claim_status == 'rejected'){
        $readonly_fields = 'readonly';
        $disable_fields = ' disabled="true"';
        $bg_color = 'style="background-color:#fd6252;"';
      }
      
      $created_date = strtotime(date('Y-m-d', strtotime($e_key->created_on)));
      $current_date = strtotime(date('Y-m-d'));
      $days = (($current_date-$created_date)/(24*60*60));
      
      if(strtotime($e_key->from_date) != strtotime($e_key->to_date))
				$claim_duration = date('d-m-Y', strtotime($e_key->from_date))." to ".date('d-m-Y', strtotime($e_key->to_date));
			else	
				$claim_duration = date('d-m-Y', strtotime($e_key->from_date));
      
      $not_update = '';
      if($days > 15 && ($current_role != 'accounts')){
				$readonly_fields = 'readonly';
				$not_update = 'not_update';
			}
			
			$stay_activity = json_decode($e_key->stay_reason);
			$stay_with = json_decode($e_key->paid_spark_id);
			$spark_names = array();
			if(!empty($stay_with))
			{
				foreach($stay_with as $stays)
				{
					$spark_names[] = $spark_with_names[$stays];	
				}
			}
			if($e_key->file_name == '' && $current_role == 'accounts' && $e_key->claim_amount !=0) {  
					// not display
			}
			else{
			
			if($e_key->claim_status == 'approved'){ 
				$is_processed = 0;
			}
				
			$net_stay_bill_cost = $net_stay_bill_cost + $e_key->manual_cost;	
			$net_stay_claim_cost = $net_stay_claim_cost + $e_key->claim_amount;	
  ?>
		<tr id="stay_claims_row_<?php echo $record_id;?>" <?=$bg_color;?>>
      <td><?=$r;?></td>
      <td><?=$claim_duration;?></td>
			<td><?php echo implode(', ', $stay_activity); ?></td>
			<td><?php echo (!empty($spark_names) ? implode(',<br/>',$spark_names) : '');; ?></td>
			<td><?php echo '<strong>Bill No.:</strong> '.$e_key->bill_number.'<br /><strong>Bill Date:</strong> '.date('d-m-Y', strtotime($e_key->bill_date)); ?></td>
			<td><?php echo $e_key->manual_cost; ?></td>
			<td><?php echo $e_key->claim_amount; ?></td>
      <td>
				<? if($e_key->file_name != '') {?>
				<a href="<?php echo base_url().$e_key->file_name; ?>" target="_blank" class="viewfile" download>View Bill</a>
				<?	} else {?>
					<? /* if($e_key->claim_status == 'approved'){ ?>
							<div id="uplodedfile_<?=$record_id?>"><a href="javascript:void(0)" onclick="return upload_bill('<?=$record_id?>', 'Stay Claim')">Upload Bill</a></div>
					<?	}else{	?>	
						<div id="uplodedfile_<?=$record_id?>">Upload Bill</div>
					<? }*/ ?>
						<div id="uplodedfile_<?=$record_id?>">No Bill</div>
        <?	}	?>
      </td>
      <td><?=$e_key->comment;?>
				<!--<textarea name="comment[]" id="comment_<?=$record_id?>" cols="6" rows="2" maxlength="200" <?=$readonly_fields?>><?=$e_key->comment;?></textarea>-->
				<input type="hidden" name="claim_status[<?=$record_id?>]" value="<?=$e_key->claim_status?>">
				<input type="hidden" name="record_id[]" value="<?=$record_id?>">
			</td>
			<td>
        <?php 
        if($e_key->claim_status == 'processed') {  
						$process_date = (!empty($e_key->process_date) ? '('.date('d-m-Y', strtotime($e_key->process_date)).')' : '');
						$user_data = get_spark_data($e_key->updated_by);
            ?><span id="rreason_<?=$record_id?>">Processed <?=$process_date." (By ".$user_data['login_id'].")";?></span><?
        }
        else{
        	if($current_role == 'accounts') {  
          $btn_reject = 'display:block';
          $btn_referbk = 'display:block';
          
          if($e_key->claim_status == 'rejected' || $e_key->claim_status == 'referback'){ 
            $btn_reject = 'display:none';
            $btn_referbk = 'display:none';
          } 
          ?>  
          <div id="stay_claims_referback_<?=$record_id?>" style="<?=$btn_referbk?>">
            <input type="button" name="referback_claim" id="referback_claim" onClick="return approvedclaim('<?=$record_id?>', 'referback', 'Refer Back', 'stay_claims')" class="btn btn-sm btn-success" value="Refer Back"> 
          </div>
          <div id="stay_claims_discard_<?=$record_id?>" style="<?=$btn_reject?>">
						<br />
            <input type="button" name="reject_claim" id="reject_claim" onClick="return approvedclaim('<?=$record_id?>', 'rejected', 'Reject', 'stay_claims')" class="btn btn-sm btn-danger" value="Reject"> 
          </div>
        <? }
					if($current_role == 'state_person' || $current_role == 'manager') {  
						if($e_key->claim_status == 'referback'){ 
							?>
							<div id="stay_claims_approved_<?=$record_id?>">
								<input type="button" name="approved_claim" id="approved_claim" onClick="return approvedclaim('<?=$record_id?>', 'approved', 'Approve', 'stay_claims')" class="btn btn-sm btn-success" value="Approve"> 
							</div>
							<div id="stay_claims_discard_<?=$record_id?>">
								<br />
								<input type="button" name="reject_claim" id="reject_claim" onClick="return approvedclaim('<?=$record_id?>', 'rejected', 'Reject', 'stay_claims')" class="btn btn-sm btn-danger" value="Reject"> 
							</div>
							<?
						}
					}
					?>
         <div><span id="stay_claims_rreason_<?=$record_id?>"><?=$e_key->status_remark;?></span></div> 
        <? } ?>  
      </td>
		</tr>
	<?php } $r++; }  ?>
  </tbody>
  <? if(count($r) > 1){ ?>
   <tfoot>
		<tr>
			<th colspan="5" style="text-align:right">Total Cost</th>
			<th style="text-align:left"><?=$net_stay_bill_cost?></th>
			<th colspan="4" style="text-align:left"><?=$net_stay_claim_cost;?></th>
		</tr>
	</tfoot>
	<?	} ?>
</table>  

<? /*if($current_role == 'accounts' && $is_processed == 0) { ?>
<div class="col-md-12" align="right" style="padding-top:10px;">
  <input type="submit" name="processClaim" value="Process Other Claim" class="btn btn-sm btn-info">
</div>
<?  } ?>
<? if($sel_spark_id == $current_user_id && $is_processed == 0) { ?>
<!--<div class="col-md-12" align="right" style="padding-top:10px;">
  <input type="submit" name="processClaim" value="Update Claim" class="btn btn-sm btn-info">
</div>-->
<?  }*/ ?>
<!--<br /></form>-->
<!-- END STAY CLAIM -->


<!-- START FOOD CLAIM-->
<?
//$display_buttons = 1;
//$is_processed = 1;
?>	
<!--<form name='frmClaim' id='frmClaim' method="post" action="<?php echo base_url();?>map/process_food_claims">-->
<div class="row"><div class="col-md-12 text-center"  style="background-color: #A6A6A6;"><h4>Food Claim</h4></div></div>
<table class="table table-striped table-bordered bootstrap-datatable datatable1 responsive customdatatable withscrolltable">
	<thead>
		<tr style="background-color: #d5ecf9;">
			<th align="center">S.No.</th>
			<th align="center">Claim Duration</th>
			<th align="center">Activity <input type="hidden" name="spark_id" value="<?=$spark_id?>"></th>
			<th align="center">Stay With</th>
			<th align="center">Actual Cost</th>
			<th align="center">Manual Cost</th>
			<th align="center">Remark</th>
      <th align="center">
			<?php if($current_role == 'accounts') { echo 'Action'; }else{ echo 'Account Status'; } ?>
      </th>
		</tr>
	</thead>
	<tbody>
	<?php 
		$r = 1;
    foreach($food_claim_data as $e_key){ 

      $bg_color = '';
      $record_id = $e_key->id;
      $comment = ($e_key->comment != '' ? $e_key->comment : '&nbsp;');
      $readonly_fields = '';
      $disable_fields = '';
      if($e_key->claim_status == 'rejected'){
        $readonly_fields = 'readonly';
        $disable_fields = ' disabled="true"';
        $bg_color = 'style="background-color:#fd6252;"';
      }
      
      $created_date = strtotime(date('Y-m-d', strtotime($e_key->created_on)));
      $current_date = strtotime(date('Y-m-d'));
      $days = (($current_date-$created_date)/(24*60*60));
      
      if(strtotime($e_key->from_date) != strtotime($e_key->to_date))
				$claim_duration = date('d-m-Y', strtotime($e_key->from_date))." to ".date('d-m-Y', strtotime($e_key->to_date));
			else	
				$claim_duration = date('d-m-Y', strtotime($e_key->from_date));
      
      $not_update = '';
      if($days > 3 && ($current_role != 'accounts')){
				$readonly_fields = 'readonly';
				$not_update = 'not_update';
			}
			if($e_key->claim_status == 'approved'){ 
				$is_processed = 0;
			}
			
			$stay_activity = json_decode($e_key->stay_reason);
			$stay_with = json_decode($e_key->paid_spark_id);
			$spark_names = array();
			if(!empty($stay_with))
			{
				foreach($stay_with as $stays)
				{
					$spark_names[] = $spark_with_names[$stays];	
				}
			}
			$net_food_claim_cost = $net_food_claim_cost + $e_key->manual_cost;	
			$net_food_actual_claim_cost = $net_food_actual_claim_cost + $e_key->actual_cost;	
  ?>
		<tr id="food_claims_row_<?php echo $record_id;?>" <?=$bg_color;?>>
      <td><?=$r;?></td>
      <td><?=$claim_duration;?></td>
			<td><?php echo implode(', ', $stay_activity); ?></td>
			<td><?php echo (!empty($spark_names) ? implode(',<br/>',$spark_names) : '');; ?></td>
			<td><?php echo $e_key->actual_cost; ?></td>
			<td><?php echo $e_key->manual_cost; ?>
				<!--<input type="text" name="manual_cost[<?=$record_id?>]" value="<?php echo $e_key->manual_cost; ?>" size="3" <?=$readonly_fields?> autocomplete="off">-->
			</td>
      <td><?=$e_key->comment;?>
				<!--<textarea name="comment[<?=$record_id?>]" id="comment_<?=$record_id?>" cols="6" rows="2" maxlength="200" <?=$readonly_fields?>><?=$e_key->comment;?></textarea>-->
				<input type="hidden" name="claim_status[<?=$record_id?>]" value="<?=$e_key->claim_status?>">
				<input type="hidden" name="record_id[]" value="<?=$record_id?>">
			</td>
			<td>
        <?php 
        if($e_key->claim_status == 'processed') {  
						$process_date = (!empty($e_key->process_date) ? '('.date('d-m-Y', strtotime($e_key->process_date)).')' : '');
						$user_data = get_spark_data($e_key->updated_by);
            ?><span id="rreason_<?=$record_id?>">Processed <?=$process_date." (By ".$user_data['login_id'].")";?></span><?
        }
        else{
        if($current_role == 'accounts') {  
          $btn_reject = 'display:block';
          $btn_referbk = 'display:block';
          
          if($e_key->claim_status == 'rejected' || $e_key->claim_status == 'referback'){ 
            $btn_reject = 'display:none';
            $btn_referbk = 'display:none';
          } 
          ?>  
          <div id="food_claims_referback_<?=$record_id?>" style="<?=$btn_referbk?>">
            <input type="button" name="referback_claim" id="referback_claim" onClick="return approvedclaim('<?=$record_id?>', 'referback', 'Refer Back', 'food_claims')" class="btn btn-sm btn-success" value="Refer Back"> 
          </div>
          <div id="food_claims_discard_<?=$record_id?>" style="<?=$btn_reject?>">
						<br />
            <input type="button" name="reject_claim" id="reject_claim" onClick="return approvedclaim('<?=$record_id?>', 'rejected', 'Reject', 'food_claims')" class="btn btn-sm btn-danger" value="Reject"> 
          </div>
        <? }
					if($current_role == 'state_person' || $current_role == 'manager') {  
						if($e_key->claim_status == 'referback'){ 
							?>
							<div id="food_claims_approved_<?=$record_id?>">
								<input type="button" name="approved_claim" id="approved_claim" onClick="return approvedclaim('<?=$record_id?>', 'approved', 'Approve', 'food_claims')" class="btn btn-sm btn-success" value="Approve"> 
							</div>
							<div id="food_claims_discard_<?=$record_id?>">
								<br />
								<input type="button" name="reject_claim" id="reject_claim" onClick="return approvedclaim('<?=$record_id?>', 'rejected', 'Reject', 'food_claims')" class="btn btn-sm btn-danger" value="Reject"> 
							</div>
							<?
						}
					}
					?> 
          <span id="food_claims_rreason_<?=$record_id?>"><?=$e_key->status_remark;?></span>
        <? } ?>  
      </td>
		</tr>
	<?php $r++; }  ?>
  </tbody>
  <? if(count($food_claim_data) > 0){ ?>
  <tfoot>
		<tr>
			<th colspan="4" style="text-align:right">Total Cost</th>
			<th style="text-align:left"><?=$net_food_actual_claim_cost?></th>
			<th colspan="3" style="text-align:left"><?=$net_food_claim_cost;?></th>
		</tr>
	</tfoot>
	<?	}	?>
</table>

<? /*if($current_role == 'accounts' && $is_processed == 0) { ?>
<div class="col-md-12" align="right" style="padding-top:10px;">
  <input type="submit" name="processClaim" value="Process Other Claim" class="btn btn-sm btn-info">
</div>
<?  } ?>
<? if($sel_spark_id == $current_user_id && $is_processed == 0) { ?>
<!--<div class="col-md-12" align="right" style="padding-top:10px;">
  <input type="submit" name="processClaim" value="Update Claim" class="btn btn-sm btn-info">
</div>-->
<?  }*/ ?>
<!--<br /></form>-->
<!-- END FOOD CLAIM-->


<!--START OTHER CLAIM -->
<?
//$display_buttons = 1;
//$is_processed = 1;
?>	
<!--<form name='frmClaim' id='frmClaim' method="post" action="<?php echo base_url();?>map/process_other_claims">-->
<div class="row"><div class="col-md-12 text-center"  style="background-color: #A6A6A6;"><h4>Other Claim</h4></div></div>
<table class="table table-striped table-bordered bootstrap-datatable datatable1 responsive customdatatable withscrolltable">
	<thead>
		<tr style="background-color: #d5ecf9;">
			<th align="center">S.No.</th>
			<th align="center">Claim Duration</th>
			<th align="center">Claim Type <input type="hidden" name="spark_id" value="<?=$spark_id?>"></th>
			<th align="center">Bill Number</th>
			<th align="center">Bill Date</th>
			<th align="center">Cost</th>
			<th align="center">View Bill</th>
			<th align="center">Remark</th>
      <th align="center">
			<?php if($current_role == 'accounts') { echo 'Action'; }else{ echo 'Account Status'; } ?>
      </th>
		</tr>
	</thead>
	
	<tbody>
	<?php 
    $r=1;
    foreach($other_claim_data as $e_key){ 

      $bg_color = '';
      $record_id = $e_key->id;
      $comment = ($e_key->comment != '' ? $e_key->comment : '&nbsp;');
      $readonly_fields = '';
      $disable_fields = '';
      if($e_key->claim_status == 'rejected'){
        $readonly_fields = 'readonly';
        $disable_fields = ' disabled="true"';
        $bg_color = 'style="background-color:#fd6252;"';
      }
      
      $created_date = strtotime(date('Y-m-d', strtotime($e_key->created_on)));
      $current_date = strtotime(date('Y-m-d'));
      $days = (($current_date-$created_date)/(24*60*60));
      
      if(strtotime($e_key->from_date) != strtotime($e_key->to_date))
				$claim_duration = date('d-m-Y', strtotime($e_key->from_date))." to ".date('d-m-Y', strtotime($e_key->to_date));
			else	
				$claim_duration = date('d-m-Y', strtotime($e_key->from_date));
      
      $not_update = '';
      if($days > 3 && ($current_role != 'accounts')){
				$readonly_fields = 'readonly';
				$not_update = 'not_update';
			}
			
			if($e_key->claim_status == 'approved'){ 
				$is_processed = 0;
			}
			$net_other_claim_cost = $net_other_claim_cost + $e_key->amount;
  ?>
		<tr id="other_claims_row_<?php echo $record_id;?>" <?=$bg_color;?>>
      <td><?=$r;?></td>
      <td><?=$claim_duration;?></td>
			<td><?php echo ucfirst($e_key->claim_type); ?></td>
			<td><?php echo $e_key->bill_number; ?></td>
			<td><?php echo date('d-m-Y', strtotime($e_key->bill_date)); ?></td>
			<td><?php echo $e_key->amount; ?></td>
      <td>
				<? if($e_key->file_name != '') {?>
				<a href="<?php echo base_url().$e_key->file_name; ?>" target="_blank" class="viewfile" download>View Bill</a>
				<?	} else { ?>
					<? /*if($e_key->claim_status == 'approved'){ ?>
							<div id="uplodedfile_<?=$record_id?>"><a href="javascript:void(0)" onclick="return upload_bill('<?=$record_id?>', 'Other Claim')">Upload Bill</a></div>
					<?	}else{	?>	
						<div id="uplodedfile_<?=$record_id?>">Upload Bill</div>
					<? }*/ ?>
					<div id="uplodedfile_<?=$record_id?>">No Bill</div>
        <?	}	?>
      </td>
      <td><?=$e_key->comment;?>
				<!--<textarea name="comment[]" id="comment_<?=$record_id?>" cols="6" rows="2" maxlength="200" <?=$readonly_fields?>><?=$e_key->comment;?></textarea>-->
				<input type="hidden" name="claim_status[<?=$record_id?>]" value="<?=$e_key->claim_status?>">
				<input type="hidden" name="record_id[]" value="<?=$record_id?>">
			</td>
			<td>
        <?php 
        if($e_key->claim_status == 'processed') {  
						$process_date = (!empty($e_key->process_date) ? '('.date('d-m-Y', strtotime($e_key->process_date)).')' : '');
						$user_data = get_spark_data($e_key->updated_by);
            ?><span id="rreason_<?=$record_id?>">Processed <?=$process_date." (By ".$user_data['login_id'].")";?></span><?
        }
        else{
				if($current_role == 'accounts') {  
          $btn_reject = 'display:block';
          $btn_referbk = 'display:block';
          
          if($e_key->claim_status == 'rejected' || $e_key->claim_status == 'referback'){ 
            $btn_reject = 'display:none';
            $btn_referbk = 'display:none';
          } 
          ?>  
          <div id="other_claims_referback_<?=$record_id?>" style="<?=$btn_referbk?>">
            <input type="button" name="referback_claim" id="referback_claim" onClick="return approvedclaim('<?=$record_id?>', 'referback', 'Refer Back', 'other_claims')" class="btn btn-sm btn-success" value="Refer Back"> 
          </div>
          <div id="other_claims_discard_<?=$record_id?>" style="<?=$btn_reject?>">
						<br />
            <input type="button" name="reject_claim" id="reject_claim" onClick="return approvedclaim('<?=$record_id?>', 'rejected', 'Reject', 'other_claims')" class="btn btn-sm btn-danger" value="Reject"> 
          </div>
        <? }
        if($current_role == 'state_person' || $current_role == 'manager') {  
					if($e_key->claim_status == 'referback'){ 
						?>
						<div id="other_claims_approved_<?=$record_id?>">
							<input type="button" name="approved_claim" id="approved_claim" onClick="return approvedclaim('<?=$record_id?>', 'approved', 'Approve', 'other_claims')" class="btn btn-sm btn-success" value="Approve"> 
						</div>
						<div id="other_claims_discard_<?=$record_id?>">
							<br />
							<input type="button" name="reject_claim" id="reject_claim" onClick="return approvedclaim('<?=$record_id?>', 'rejected', 'Reject', 'other_claims')" class="btn btn-sm btn-danger" value="Reject"> 
						</div>
						<?
					}
				}
       	?>
       	<div><span id="other_claims_rreason_<?=$record_id?>"><?=$e_key->status_remark;?></span></div> 
        <? } ?>  
      </td>
		</tr>
	<?php $r++; }  ?>
	</tbody>
	<? if(count($other_claim_data) > 0) { ?>
	<tfoot>
		<tr>
			<th colspan="5" style="text-align:right">Total Cost</th>
			<th colspan="4" style="text-align:left"><?=$net_other_claim_cost?></th>
		</tr>
	</tfoot>	
	<?	}	?>
</table>	
<?
	$net_claim_cost = $net_ld_claim_cost + $net_stay_claim_cost + $net_food_claim_cost + $net_other_claim_cost;
?>
<div class="col-md-12" align="right" style="padding-top:10px;">
	<strong>Total Cost:</strong> <?=round($net_claim_cost,2);?>
</div>	
<? if($current_role == 'accounts' && $is_processed == 0) { ?>
<div class="col-md-12" align="right" style="padding-top:10px;">
  <input type="submit" name="processClaim" value="Process Claim" class="btn btn-sm btn-info">
</div>
<?  } ?>
<? /*if($sel_spark_id == $current_user_id && $is_processed == 0) { ?>
<!--<div class="col-md-12" align="right" style="padding-top:10px;">
  <input type="submit" name="processClaim" value="Update Claim" class="btn btn-sm btn-info">
</div>-->
<?  }*/ ?>
<br /></form> 
<!--END OTHER CLAIM -->

</div>

<div class="modal fade" id="discard_Model" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4><span id="mainheading"></span> Claim</h4>
        <button type="button" class="close" data-dismiss="modal">×</button>
      </div>
      <form method="post" action="" role="form" name="frmdiscard" id="frmdiscard">
        <input type="hidden" name="claimid" id="claimid" value="">
        <input type="hidden" name="actiontype" id="actiontype" value="">
        <input type="hidden" name="claimtype" id="claimtype" value="">
        <div class="modal-body">                      
          <div class="row">
            <div class="form-group col-md-12">
              <textarea name="discard_reason" id="discardreason" placeholder="Add Reason" class="form-control" maxlength="200"></textarea>
            </div>
          </div>
          <div class="row">  
            <div class="form-group col-md-3">&nbsp;</div>
            <div class="form-group col-md-6">
              <label for="brc_name">&nbsp;</label>
              <input type="button" class="form-control" id="submit" name="submit" value="Save" class="btn btn-success" onClick="return save_claim_action()">
            </div>  
          </div>  
        </div>  
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="discardModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4>Reject Claim</h4>
        <button type="button" class="close" data-dismiss="modal">×</button>
      </div>
      <form method="post" action="" role="form" name="frmdiscard" id="frmdiscard">
        <input type="hidden" name="claimid" id="claimid" value="">
        <div class="modal-body">                      
          <div class="row">
            <div class="form-group col-md-12">
              <textarea name="discard_reason" id="discard_reason" placeholder="Reason to reject" class="form-control" maxlength="200"></textarea>
            </div>
          </div>
          <div class="row">  
            <div class="form-group col-md-3">&nbsp;</div>
            <div class="form-group col-md-6">
              <label for="brc_name">&nbsp;</label>
              <input type="button" class="form-control" id="submit" name="submit" value="Save" class="btn btn-success" onClick="return save_discard_claim()">
            </div>  
          </div>  
        </div>  
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="approvedModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4>Approve Claim</h4>
        <button type="button" class="close" data-dismiss="modal">×</button>
      </div>
      <form method="post" action="" role="form" name="frmapproved" id="frmapproved">
        <input type="hidden" name="claimid" id="claimid" value="">
        <div class="modal-body">                      
          <div class="row">
            <div class="form-group col-md-12">
              <textarea name="approved_reason" id="approved_reason" placeholder="Reason to approved" class="form-control" maxlength="200"></textarea>
            </div>
          </div>
          <div class="row">  
            <div class="form-group col-md-3">&nbsp;</div>
            <div class="form-group col-md-6">
              <label for="brc_name">&nbsp;</label>
              <input type="button" class="form-control" id="submit" name="submit" value="Save" class="btn btn-success" onClick="return save_approved_claim()">
            </div>  
          </div>  
        </div>  
      </form>
    </div>
  </div>
</div>

<? }else{ 
		// code in case of download excel or pdf
} ?>


<? if(!isset($download) && !isset($pdf)) {?>

<script>
$(document).ready(function() {
    //$("#frmClaim").validate();
});
 
function approvedclaim(cid, actiontype, heading, claim_type)
{
  $("#discardreason").val('');
  $("#actiontype").val('');
  $("#mainheading").html('');
  $("#claimtype").val(claim_type);
  $("#actiontype").val(actiontype);
  $("#mainheading").html(heading);
  $("#claimid").val(cid);
  $("#discard_Model").modal("show");
}

function save_claim_action()
{
  var claim_id = $("#claimid").val();
  var reason = $("#discardreason").val();
  var action_type = $("#actiontype").val();
  var claim_type = $("#claimtype").val(); // stay_claims
  
  if(reason == ''){
    alert('Please add reason');
    return false;
  }
  
  $.ajax({
      url: '<?=base_url()?>map/action_claim',
      type: 'post',
      data: {
				claim_id: claim_id,
        claim_type : claim_type,
        action_type : action_type,
        reason : reason
      },
      success: function(response) {
				$("#"+claim_type+"_discard_"+claim_id).hide();
        $("#"+claim_type+"_approved_"+claim_id).hide();
        $("#"+claim_type+"_referback_"+claim_id).hide();
        $("#discard_Model").modal("hide");
        $("#"+claim_type+"_rreason_"+claim_id).html(response);
        if(action_type == 'rejected'){
					$("#"+claim_type+"_row_"+claim_id).css("background-color", "#fd6252");
				}
      },
      error: function(response) {
        window.console.log(response);
      }
   });
}



function discard_claim(cid)
{
  $("#discard_reason").val('');
  $("#claimid").val(cid);
  $("#discardModel").modal("show");
}

function approved_lclaim(cid)
{
  $("#approved_reason").val('');
  $("#claimid").val(cid);
  $("#approvedModel").modal("show");
}

function save_discard_claim()
{
  var claim_id = $("#claimid").val();
  var reason = $("#discard_reason").val();
  if(reason == ''){	alert('Please add reason');	return false;	}
  
  $.ajax({
      url: '<?=base_url()?>map/discard_claim',
      type: 'post',
      data: {
        claim_id: claim_id,
        reason : reason
      },
      success: function(response) {
        $("#discardModel").modal("hide");
        $("#discard_"+claim_id).hide();
        $("#approved_"+claim_id).show();
        $("#rreason_"+claim_id).html(reason);
        $(".claim_discard_"+claim_id).val('1');
        $("#row_"+claim_id).css("background-color", "#fd6252");
      },
      error: function(response) {
        window.console.log(response);
      }
    });
}

function save_approved_claim()
{
  var claim_id = $("#claimid").val();
  var reason = $("#approved_reason").val();
  if(reason == ''){
    alert('Please add reason');
    return false;
  }
  $.ajax({
      url: '<?=base_url()?>map/approved_claim',
      type: 'post',
      data: {
        claim_id: claim_id,
        reason : reason
      },
      success: function(response) {
        $("#approvedModel").modal("hide");
        $("#discard_"+claim_id).show();
        $("#approved_"+claim_id).hide();
        $(".claim_discard_"+claim_id).val('0');
        $("#row_"+claim_id).css("background-color", "#FFFFFF");
      },
      error: function(response) {
        window.console.log(response);
      }
    });
}
</script>
<? } ?>
