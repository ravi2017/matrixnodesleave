<? if(!isset($download) && !isset($pdf)) {	?>
<? if($current_role != 'hr' || $current_role != 'super_admin'){	?>	
<div class="row">
	<div class="col-md-3">
		<div class="form-group">
			<a href="<?php echo base_url();?>map/stay_claims_list" class="btn btn-sm btn-info">Stay Claim List</a>
		</div>
	</div>
</div>    	
<?	}	?>
<form name="frmactivities" method="post" action="" class="">
  <div class="row">
    <div class="col-md-3">
      <div class="form-group">
        <input type="text" id="activitydatestart" name="activitydatestart" class="form-control" value="<? echo (!empty($activitydatestart) ? $activitydatestart : '');?>" autocomplete="off">
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <input type="text" id="activitydateend" name="activitydateend" class="form-control" value="<? echo (!empty($activitydateend) ? $activitydateend : '');?>" autocomplete="off">
      </div>
    </div>
    <?php if ($current_role != "field_user") { ?>
    <div class="col-md-6">
    <?php echo $this->load->view('team_members/select_current_spark','',true) ?>
    </div>
    <?php } ?>
    <div class="col-md-3">
      <div class="form-group">
        <input type="submit" name="submit" class="form-control btn btn-sm btn-success" value="Get Claims List">
      </div>
    </div>
  </div>
</form>
<? } ?>

<? if(!empty($claim_data)) { $activity_date = ''; ?>
<? if(!isset($download) && !isset($pdf)) {?>
<form name='frmClaim' id='frmClaim' method="post" action="<?php echo base_url();?>map/process_food_claims">
<div class="scrollme">
<table class="table table-striped table-bordered bootstrap-datatable datatable1 responsive customdatatable" id="withscrolltable">
	<thead>
		<tr style="background-color: #d5ecf9;">
			<th align="center">S.No.</th>
			<th align="center">Claim Duration</th>
			<th align="center">Claim Type <input type="hidden" name="spark_id" value="<?=$spark_id?>"></th>
			<th align="center">Bill Number</th>
			<th align="center">Bill Date</th>
			<th align="center">Amount</th>
			<th align="center">Upload Bill</th>
			<th align="center">Remark</th>
      <th align="center">
			<?php if($current_role == 'accounts') { echo 'Action'; }else{ echo 'Account Status'; } ?>
      </th>
		</tr>
	</thead>
	<tbody>
	<? $state_count = 0; $r=1; ?>
	<?php 
    $display_buttons = 1;
    $is_processed = 1;
    foreach($claim_data as $e_key){ 

      $bg_color = '';
      $record_id = $e_key->id;
      $comment = ($e_key->comment != '' ? $e_key->comment : '&nbsp;');
      $readonly_fields = '';
      $disable_fields = '';
      if($e_key->claim_status == 'rejected'){
        $readonly_fields = 'readonly';
        $disable_fields = ' disabled="true"';
        $bg_color = 'style="background-color:#fd6252;"';
      }
      
      $created_date = strtotime(date('Y-m-d', strtotime($e_key->created_on)));
      $current_date = strtotime(date('Y-m-d'));
      $days = (($current_date-$created_date)/(24*60*60));
      
      $claim_duration = date('d-m-Y', strtotime($e_key->from_date))." to ".date('d-m-Y', strtotime($e_key->to_date));
      
      $not_update = '';
      if($days > 3 && ($current_role != 'accounts')){
				$readonly_fields = 'readonly';
				$not_update = 'not_update';
			}
  ?>
		<tr id="row_<?php echo $record_id;?>" <?=$bg_color;?>>
      <td><?=$r;?></td>
      <td><?=$claim_duration;?></td>
			<td>
        <input type="hidden" name="record_id[]" value="<?=$record_id?>">
        <?php echo ucfirst($e_key->claim_type); ?>
      </td>
			<td><?php echo $e_key->bill_number; ?></td>
			<td><?php echo date('d-m-Y', strtotime($e_key->bill_date)); ?></td>
			<td><?php echo $e_key->amount; ?></td>
      <td>
				<? if($e_key->file_name != '') {?>
				<a href="<?php echo base_url().$e_key->file_name; ?>" target="_blank" class="viewfile" download>View Bill</a>
				<?	} else {?>
					<? if(($e_key->spark_id == $current_user_id) && $e_key->claim_status == 'approved'){ ?>
							<div id="uplodedfile_<?=$record_id?>"><a href="javascript:void(0)" onclick="return upload_bill('<?=$record_id?>')">Upload Bill</a></div>
					<?	}else{	?>	
						<div id="uplodedfile_<?=$record_id?>">No Bill</div>
					<? } ?>
        <?	}	?>
      </td>
      <td><?php echo $e_key->comment;?><input type="hidden" name="claim_status[<?=$record_id?>]" value="<?=$e_key->claim_status?>"></td>
			<td>
        <?php 
        if($e_key->claim_status == 'processed') {  
						$process_date = (!empty($e_key->process_date) ? '('.date('d-m-Y', strtotime($e_key->process_date)).')' : '');
						$user_data = get_spark_data($e_key->updated_by);
            ?><span id="rreason_<?=$record_id?>">Processed <?=$process_date." (By ".$user_data['name'].")";?></span><?
        }
        else{
        if($current_role == 'accounts') {  
          $btn_reject = 'display:block';
          $btn_approve = 'display:none';
          $reject_reason = '';
          if($e_key->claim_status == 'approved'){ 
						$is_processed = 0;
					}
          if($e_key->claim_status == 'rejected'){ 
            $btn_reject = 'display:none';
            $btn_approve = 'display:block';
            $reject_reason = $e_key->status_remark;
          } 
          ?>  
          <div id="approved_<?=$record_id?>" style="<?=$btn_approve?>">
            <input type="button" name="approved_claim" id="approved_claim" onClick="return approvedclaim('<?=$record_id?>')" class="btn btn-sm btn-success" value="Approve"> 
          </div>
          <div id="discard_<?=$record_id?>" style="<?=$btn_reject?>">
            <input type="button" name="reject_claim" id="reject_claim" onClick="return discard_claim('<?=$record_id?>')" class="btn btn-sm btn-danger" value="Reject"> 
          </div>
          <div><span id="rreason_<?=$record_id?>"><?=$e_key->status_remark;?></span></div>
        <? }else{ ?>  
          <span id="rreason_<?=$record_id?>"><?=$e_key->status_remark;?></span>
        <?  } ?>  
        <? } ?>  
      </td>
      
		</tr>
	<?php $r++; }  ?>
  </tbody>
</table>  
</div>

<? if($current_role == 'accounts' && $is_processed == 0) { ?>
<div class="col-md-12" align="right" style="padding-top:10px;">
  <input type="submit" name="processClaim" value="Process Claim" class="btn btn-sm btn-info">
</div>
<?  } ?>
<? if($sel_spark_id == $current_user_id && $is_processed == 0) { ?>
<div class="col-md-12" align="right" style="padding-top:10px;">
  <input type="submit" name="processClaim" value="Update Claim" class="btn btn-sm btn-info">
</div>
<?  } ?>
</form>  

<div class="modal fade" id="discardModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4>Reject Claim</h4>
        <button type="button" class="close" data-dismiss="modal">×</button>
      </div>
      <form method="post" action="" role="form" name="frmdiscard" id="frmdiscard">
        <input type="hidden" name="claimid" id="claimid" value="">
        <div class="modal-body">                      
          <div class="row">
            <div class="form-group col-md-12">
              <textarea name="discard_reason" id="discard_reason" placeholder="Reason to reject" class="form-control" maxlength="200"></textarea>
            </div>
          </div>
          <div class="row">  
            <div class="form-group col-md-3">&nbsp;</div>
            <div class="form-group col-md-6">
              <label for="brc_name">&nbsp;</label>
              <input type="button" class="form-control" id="submit" name="submit" value="Save" class="btn btn-success" onClick="return save_discard_claim()">
            </div>  
          </div>  
        </div>  
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="approvedModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4>Approve Claim</h4>
        <button type="button" class="close" data-dismiss="modal">×</button>
      </div>
      <form method="post" action="" role="form" name="frmdiscard" id="frmdiscard">
        <input type="hidden" name="claimid" id="claimid" value="">
        <div class="modal-body">                      
          <div class="row">
            <div class="form-group col-md-12">
              <textarea name="approved_reason" id="approved_reason" placeholder="Reason to approve" class="form-control" maxlength="200"></textarea>
            </div>
          </div>
          <div class="row">  
            <div class="form-group col-md-3">&nbsp;</div>
            <div class="form-group col-md-6">
              <label for="brc_name">&nbsp;</label>
              <input type="button" class="form-control" id="submit" name="submit" value="Save" class="btn btn-success" onClick="return save_approved_claim()">
            </div>  
          </div>  
        </div>  
      </form>
    </div>
  </div>
</div>

<? }else{ ?>
  <table cellspacing=0 cellpadding=2 border=1 width="100%">
    <tr>
      <td colspan=4><strong>Spark Name :</strong> <? echo $spark_name?></td>
      <td colspan=4><strong>Duration :</strong> <?=$activitydatestart?> To <?=$activitydateend?></td>
    </tr>
  </table>  
  <table cellspacing=0 cellpadding=2 border=1>
	<thead>
		<tr>
      <th align="center">S.No.</th>
			<th align="center">Claim Duration</th>
			<th align="center">Claim Type</th>
			<th align="center">Bill Number</th>
			<th align="center">Bill Date</th>
			<th align="center">Amount</th>
			<th align="center">Remark</th>
			<th align="center">Claim Status</th>
		</tr>
	</thead>
	<tbody>
	<? 
		 $k=1; 
     $total_travel_cost = 0;
     
		foreach ($claim_data as $e_key){
      
      $bg_color = '';
      $record_id = $e_key->id;
      $comment = ($e_key->comment != '' ? $e_key->comment : '&nbsp;');
      $readonly_fields = '';
      $disable_fields = '';
      if($e_key->claim_status == 'rejected'){
        $readonly_fields = 'readonly';
        $disable_fields = ' disabled="true"';
        $bg_color = 'style="background-color:#fd6252;"';
      }
      
      $created_date = strtotime(date('Y-m-d', strtotime($e_key->created_on)));
      $current_date = strtotime(date('Y-m-d'));
      $days = (($current_date-$created_date)/(24*60*60));
      
      $claim_duration = date('d-m-Y', strtotime($e_key->from_date))." to ".date('d-m-Y', strtotime($e_key->to_date));
      
      $not_update = '';
      if($days > 3 && ($current_role != 'accounts')){
				$readonly_fields = 'readonly';
				$not_update = 'not_update';
			}
			
			$total_travel_cost = $total_travel_cost + $e_key->amount;
    ?>
		<tr>
      <td><?=$k;?></td>
      <td><?=$claim_duration;?></td>
			<td><?php echo ucfirst($e_key->claim_type); ?></td>
			<td><?php echo $e_key->bill_number; ?></td>
			<td><?php echo date('d-m-Y', strtotime($e_key->bill_date)); ?></td>
			<td><?php echo $e_key->amount; ?></td>
      <td><?php echo $e_key->comment; ?></td>
			<td><?php echo $e_key->claim_status; ?></td>
		</tr>
	<?php $k++; }  ?>
    <tr>
      <td colspan=5></td>
      <td align="right"><?=$total_travel_cost;?></td>
      <td colspan=2>&nbsp;</td>
    </tr>
  </tbody>
</table>  
 
<? } ?>
<? } ?>

<? // && !empty($display_buttons)
if(!isset($download) && !isset($pdf) && !empty($claim_data))  { ?>
    <br />
    <div class="row">
      <? if($current_role == 'super_admin' || $current_role == 'accounts') { ?>
      <div class="form-group col-md-3">
      <form name="frmreport" action="<?php echo base_url();?>map/food_claims" method="post">
          <input type="hidden" name="activitydatestart" value="<? echo (!empty($activitydatestart) ? $activitydatestart : '');?>">
          <input type="hidden" name="activitydateend" value="<? echo (!empty($activitydateend) ? $activitydateend : '');?>">
          <input type="hidden" name="spark_id" value="<?=$spark_id?>">
          <input type="hidden" name="spark_state_id" value="<?=$spark_state_id?>">
          <input type="hidden" name="download" value="1">
          <button type="submit" name="submit" value="Get Claims List" class="btn btn-success">Download Report</button>
      </form>
      </div>
      <?  } ?>
      <div class="form-group col-md-3">
      <form name="frmreport" action="<?php echo base_url();?>map/food_claims" method="post">
          <input type="hidden" name="activitydatestart" value="<? echo (!empty($activitydatestart) ? $activitydatestart : '');?>">
          <input type="hidden" name="activitydateend" value="<? echo (!empty($activitydateend) ? $activitydateend : '');?>">
          <input type="hidden" name="spark_id" value="<?=$spark_id?>">
          <input type="hidden" name="spark_state_id" value="<?=$spark_state_id?>">
          <input type="hidden" name="pdf" value="1">
        <button type="submit" name="submit" value="Get Claims List" class="btn btn-success">Download PDF</button>
      </form>
      </div>
    </div>
<? } ?>


<? if(!isset($download) && !isset($pdf)) {?>
<div class="modal fade" id="uploadpodModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4>Upload POD</h4>
				<button type="button" class="close" data-dismiss="modal">×</button>
			</div>
			<form method="post" action="" role="form" name="frmuploadpod" id="frmupload"   enctype="multipart/form-data">
				<input type="hidden" name="claim_id" id="claim_id" value="" />
				<input type="hidden" name="claim_type" id="claim_type" value="Food Claim" />
				<div class="modal-body">                      
					<span class="err" id="err_up"></span>
					<div class="form-group">
						<label for="pod"><span class='error'>*</span>Upload Bill:</label>
						<input type="file" class="form-control" id="file_name" name="file_name" value="" required>
						<span class="err">(Allowed only .jpg,.gif,.png,.csv,.xls file type)</span>
					</div>
				</div>
				<div class="modal-footer">
					<a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
					<input type="submit" id="uploadfile" name="uploadfile" class="btn btn-primary" value="Submit">
				</div>
			</form>
		</div>
  </div>
</div>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/v2/js/uploadfile.js"></script>
<script>
$(document).ready(function() {
    //$("#frmClaim").validate();
});
  
function discard_claim(cid)
{
  $("#discard_reason").val('');
  $("#claimid").val(cid);
  $("#discardModel").modal("show");
}

function save_discard_claim()
{
  var claim_id = $("#claimid").val();
  var reason = $("#discard_reason").val();
  
  if(reason == ''){
    alert('Please add reason');
    return false;
  }
  
  $.ajax({
      url: '<?=base_url()?>map/discard_food_claim',
      type: 'post',
      data: {
        claim_id: claim_id,
        reason : reason
      },
      success: function(response) {
        $("#discard_"+claim_id).hide();
        $("#approved_"+claim_id).show();
        $("#discardModel").modal("hide");
        $("#rreason_"+claim_id).html(reason);
        $(".claim_discard_"+claim_id).val('1');
        $("#row_"+claim_id).css("background-color", "#fd6252");
      },
      error: function(response) {
        window.console.log(response);
      }
    });
}


function approvedclaim(cid)
{
  $("#approved_reason").val('');
  $("#claimid").val(cid);
  $("#approvedModel").modal("show");
}

function save_approved_claim()
{
  var claim_id = $("#claimid").val();
  var reason = $("#approved_reason").val();
  if(reason == ''){
    alert('Please add reason');
    return false;
  }
  $.ajax({
      url: '<?=base_url()?>map/approved_food_claim',
      type: 'post',
      data: {
        claim_id: claim_id,
        reason : reason
      },
      success: function(response) {
        $("#approvedModel").modal("hide");
        $("#discard_"+claim_id).show();
        $("#approved_"+claim_id).hide();
        $(".claim_discard_"+claim_id).val('0');
        $("#row_"+claim_id).css("background-color", "#FFFFFF");
      },
      error: function(response) {
        window.console.log(response);
      }
    });
}
</script>

<? } ?>
