
<style>
    .activeDay {background-color:#ffffff !important;}
    .blind {color:#ffffff;background-color:#ffffff ;border:0px; display:none!important;}
.fc-day:hover, .fc-day:focus{
    background:lightblue;
}
.fc-slats, 
.fc-content-skeleton, 
.fc-bgevent-skeleton{
    pointer-events:none;
}
.fc-bgevent,
.fc-event-container{
    pointer-events:auto;
}

.tooltip{
    position:absolute;
    z-index:10000;
    background:white;
    pointer-events:none;
}   
</style>

<div class="row">
  <div class="box col-md-8 col-md-offset-2">
    <div class="box-inner">
      <div class="box-header well" data-original-title="">
          <h2><i class="glyphicon glyphicon-th"></i> Select Date</h2>
      </div>
        <?$current_month=date('M');?>
        <?$current_month2=date('m');?>
        <?$current_year=date('Y');?>
        <?foreach($todaydate as $date){?>
          <div class="box-content">
              <div class="row">
                  <div class="col-md-offset-4 text-center col-md-4"><a href="<?=base_url()?>dashboard/select_date?activity_date=<?echo $date.'-'.$current_month2.'-'.$current_year?>"><h6><?echo $date?>-<?echo $current_month?>-<?echo $current_year?></h6></a></div>
              </div>
          </div>
        <?}?>
      </div>
    </div>
  </div>
</div>

<script>
    $('#calendar').fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        },
        hiddenDays: [ 0 ],
        defaultDate: '<?=date(DATE_RFC2822)?>',
        editable: false,
        eventLimit: 1,
        dayClick: function(date, jsEvent, view, resourceObj) {
          var cur_date = $.now()
          if (date > cur_date) {
            //TRUE Clicked date smaller than today + daysToadd
            alert("You cannot add activity for future dates!");    
          }
          else
          {
            selected_date = moment(date).format('DD-MM-YYYY')
            var holiday_marked = false
            $('#calendar').fullCalendar('clientEvents', function(event) {
               event_start = moment(event.start).format('DD-MM-YYYY')
                if(event_start == selected_date && event.activity_type == "holiday") {
                //if(event_start == selected_date) {
                  alert('Holiday marked for this date. Cannot add any other activity')
                  holiday_marked = true
                  return true
                }
            });
            if (holiday_marked == false)
            {
              $.ajax({
                url: "<?=base_url()?>select_date/",
                type: 'post',
                dataType: 'json',
                data : {
                  activity_date : date.format()
                },
                success: function (data) {
                  if (data["response"] == "done")
                  {
                    setTimeout(function () {
                      window.location.href = "<?=base_url()?>select_activity/";
                    }, 10);
                  }
                  else
                  {
                    alert(data["response"])
                  }
                }
              });
            }
          }   
        },
        dayRender: function (date, cell) {
          cell.css("background-color", "#ffffff");
        },
        /*events: {
                  url: '<?= base_url() ?>dashboard/activities',
                  type: 'POST'
                },*/
				//event day color change//
		eventRender: function (event, element)
			{
				var dataToFind = moment(event.start).format('YYYY-MM-DD');
				$("td[data-date='"+dataToFind+"']").addClass('activeDay');
			},
        eventClick: function(calEvent, jsEvent, view) {
          if (calEvent.url) {
            $.ajax({
              url: calEvent.url,
              type: 'get',
              dataType: 'html',
              success: function (data) {
                setTimeout(function () {
                  $("#activitydetails").find(".modal-body").each(function(){
                    $(this).html(data)
                  })
                  $("#activitydetails").modal('show');
                }, 10);
              }
            });
            return false;
          }

        }
    });
    
    $('td.fc-day').mouseover(function() {
        var strDate = $(this).data('date');
        $(this).css('background-color', 'red');
    });
    $('td.fc-day-number').mouseover(function() {
        var strDate = $(this).data('date');
        $("td.fc-day").filter("[data-date='" + strDate + "']").css('background-color', 'red');
    });
    $('td.fc-day').mouseout(function() {
        $(this).css('background-color', 'white');
    })
    $('td.fc-day-number').mouseout(function() {
        var strDate = $(this).data('date');
        $("td.fc-day").filter("[data-date='" + strDate + "']").css('background-color', 'white');
    })
</script>


<div class="modal fade" id="activitydetails" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">

    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h3>Activity Detail</h3>
            </div>
            <div class="modal-body">
              
            </div>
            <div class="modal-footer">
                <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
            </div>
        </div>
    </div>
</div>
