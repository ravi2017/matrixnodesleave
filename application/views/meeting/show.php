<div class="row">
  <div class="form-group col-md-8 col-md-offset-2">
    <label for="title"><br>Activity : </label> <?=ucwords($detail['meeting_type'])?> Meeting<br>
    <label for="title"><br>Location : </label> <?=($detail['location'] == "ho") ? strtoupper($detail['location']) : ucwords($detail['location'])?><br>
    
    <? if (isset($state)) { ?>
    <label for="title"><br>State : </label> <?=ucwords($state['name'])?><br>
    <? } ?>
    
    <? if (isset($district)) { ?>
    <label for="title"><br>District : </label> <?=ucwords($district['name'])?><br>
    <? } ?>
    
    <? if (isset($block)) { ?>
    <label for="title"><br>Block : </label> <?=ucwords($block['name'])?><br>
    <? } ?>
    
    <? if (isset($cluster)) { ?>
    <label for="title"><br>Cluster : </label> <?=ucwords($cluster['name'])?><br>
    <? } ?>
    
    <? if (isset($detail) and $detail['meet_with'] != "") { ?>
    <label for="title"><br>Meet With : </label> <?=ucwords($detail['meet_with'])?><br>
    <? } ?>
  </div>
</div>
