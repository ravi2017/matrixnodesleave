<? if(!isset($download) && !isset($pdf)) { ?>
<div class="form-group row">
  <div class="form-group col-md-3">
  <form name="frmreport" action="<?php echo base_url();?>meeting/spark_meeting_report" method="post">
      <input type="hidden" name="state_id" value="<?=$state_id?>">
      <input type="hidden" name="district_id" value="<?=$district_id?>">
      <input type="hidden" name="block_id" value="<?=$block_id?>">
      <input type="hidden" name="start_month" value="<?=$start_month?>">
      <input type="hidden" name="start_year" value="<?=$start_year?>">
      <input type="hidden" name="duration" value="<?=$duration?>">
      <input type="hidden" name="download" value="1">
      <button type="submit" class="btn btn-success">Download Report</button>
  </form>
  </div>
  <div class="form-group col-md-3">
  <form name="frmreport" action="<?php echo base_url();?>meeting/spark_meeting_report" method="post">
      <input type="hidden" name="state_id" value="<?=$state_id?>">
      <input type="hidden" name="district_id" value="<?=$district_id?>">
      <input type="hidden" name="block_id" value="<?=$block_id?>">
      <input type="hidden" name="start_month" value="<?=$start_month?>">
      <input type="hidden" name="start_year" value="<?=$start_year?>">
      <input type="hidden" name="duration" value="<?=$duration?>">
      <input type="hidden" name="pdf" value="1">
      <button type="submit" class="btn btn-success">Download PDF</button>
  </form>
  </div>
  <div class="col-md-6" align="right"><a href="<?=base_url()?>meeting/spark_meeting_report" style="float:right">Back</a></div>
</div>
<? }else{ ?>
<table border="1" cellspacing=0 cellpadding=2 width="100%"> 
  <tr><td colspan="7" align="center"><h2>Spark Meeting Report</h2></td></tr>
</table>
<?  } ?>  

<? if(isset($pdf) || isset($download)) { ?>
  <table border="1" cellspacing=0 cellpadding=2 width="100%">  
<?php }else{ ?>
  <table class="table table-striped table-bordered bootstrap-datatable datatable1 responsive">
<?php } ?>  
    <tr>
      <td colspan=7><strong>MONTH : </strong><?php echo $month_name. ($duration == 'YTD' ? " (".$duration.")" : ''); ?></td>
      <!--<td colspan="6"><strong>Duration : </strong><?php echo date('d-m-Y', strtotime($startdate))." To ".date('d-m-Y', strtotime($enddate)); ?></td>-->
    </tr>  
</table>


<div class="scrollme">
<? if(isset($pdf)) { ?>
  <br />
  <table border="1" cellspacing=0 cellpadding=2 width="100%">  
<?php }else{ ?>
  <table class="table table-striped table-bordered bootstrap-datatable datatable1 responsive">
<?php } ?>  
		<tr>
			<th><?=$column_name; ?></th>
      <th>DM</th>
      <th>DEO/DRC</th>
      <th>BEO/BRC</th>
      <th>CRC</th>
      <!--<th>Others<span style="vertical-align:top;">*</span></th>-->
      <th>Total</th>
      <th>Spark Name</th>
		</tr>
   
    <?php $k=1;
    $total_1 = 0;
    $total_DM = 0;
    $total_DEO_DRC = 0;
    $total_BEO_BRC = 0;
    $total_CRC = 0;
    $total_Others = 0;
    
    if(!empty($type_values)){
    foreach($type_values as $key=>$value) { 
      
      $spark_names = array();
      $sparks = $user_arr[$key];
      foreach($sparks as $spark)
      {
        $spark_names[] = $user_names[$spark];
      }
      
      $meet_with_DM = (isset($meet_with[$key]['DM']) ? $meet_with[$key]['DM'] : '-');
      $meet_with_DEO_DRC = (isset($meet_with[$key]['DEO_DRC']) ? $meet_with[$key]['DEO_DRC'] : '-');
      $meet_with_BEO_BRC = (isset($meet_with[$key]['BEO_BRC']) ? $meet_with[$key]['BEO_BRC'] : '-');
      $meet_with_CRC = (isset($meet_with[$key]['CRC']) ? $meet_with[$key]['CRC'] : '-');
      //$meet_with_Others = (isset($meet_with[$key]['Others']) ? $meet_with[$key]['Others'] : '-');
      $meet_with_Others = 0;
      
      $total_meeting = $meet_with_DM + $meet_with_DEO_DRC + $meet_with_BEO_BRC + $meet_with_CRC;
      
      $total_1 = $total_1 + $total_meeting;
      $total_DM = $total_DM + $meet_with_DM;
      $total_DEO_DRC = $total_DEO_DRC + $meet_with_DEO_DRC;
      $total_BEO_BRC = $total_BEO_BRC + $meet_with_BEO_BRC;
      $total_CRC = $total_CRC + $meet_with_CRC;
      $total_Others = $total_Others + $meet_with_Others;
      ?>
      <tr>
        <td><?=$value;?></td>
        <td><?php echo $meet_with_DM; ?></td>  
        <td><?php echo $meet_with_DEO_DRC; ?></td>  
        <td><?php echo $meet_with_BEO_BRC; ?></td>  
        <td><?php echo $meet_with_CRC; ?></td>  
        <!--<td><?php echo $meet_with_Others; ?></td> -->
        <td><?php echo $total_meeting; ?></td>  
        <td><?php echo implode(', ', $spark_names); ?></td>
      </tr>
    <?php $k++; } ?>
    <tr>
      <td align="center">&nbsp;</td>
      <td><?php echo $total_DM; ?></td>  
      <td><?php echo $total_DEO_DRC; ?></td>  
      <td><?php echo $total_BEO_BRC; ?></td>  
      <td><?php echo $total_CRC; ?></td>  
      <!--<td><?php echo $total_Others; ?></td> -->
      <td><?php echo $total_1; ?></td>  
      <td align="center">&nbsp;</td>
		</tr>
    <?  }else{ ?>
      <tr>
        <? if(isset($download)) { ?>
        <td colspan="7" align="cnter">No Record Found.</td>
        <? }else{ ?>
        <td colspan="7" style="text-align:center">No Record Found.</td>  
        <? } ?>  
      </tr>
    <?  } ?>  
		<tr>
      <? if(isset($download)) { ?>
			<td colspan="7" align="right">Report Generated On: <?=date("d-m-Y H:i");?></td>
      <? }else{ ?>
      <td colspan="7" style="text-align:right">Report Generated On: <?=date("d-m-Y H:i");?></td>  
      <? } ?>  
		</tr>
	</thead>	
</table>
</div>
<? /* if(!empty($meet_others)) { ?>
  <tr>
    <td colspan="7"><strong>Others :</strong> <?php echo implode(', ',$meet_others); ?></td>
  </tr>
<?  }*/ ?>  
