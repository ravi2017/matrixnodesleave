<div class="row">
    <div class="box col-md-6 col-md-offset-3">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-th"></i> Select Meeting - <?=strftime("%d-%b-%Y",strtotime($activity_date))?></h2>
            </div>
            
            <div class="box-content">
                <div class="row">
                    <div class="col-md-offset-4 text-center col-md-4"><a href="<?=base_url()?>meeting/external"><h6>External Meeting</h6></a></div>
                </div>
            </div>
            <div class="box-content">
                <div class="row">
                    <div class="col-md-offset-4 text-center col-md-4"><a href="<?=base_url()?>meeting/internal"><h6>Internal Meeting</h6></a></div>
                </div>
            </div>
            <div class="box-content">
                <div class="row">
                    <div class="col-md-offset-4 text-center col-md-4"><a href="<?=base_url()?>select_activity"><h6>Back</h6></a></div>
                </div>
            </div>
        </div>
    </div>
</div>
