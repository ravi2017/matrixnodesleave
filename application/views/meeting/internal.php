<div class="row">
    <div class="box col-md-6 col-md-offset-3">
        <div class="box-inner">
          <form method="post" action="<?php echo base_url();?>meeting/internal_submit" role="form" name="frmexternalmeeting">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-th"></i> Enter Detail of Internal Meeting</h2>
            </div>
            
            <div class="row">
              <div class="form-group col-md-10 col-md-offset-1">
                <label for="title"><br>Meeting Date : <?=strftime("%d-%b-%Y",strtotime($activity_date))?></label>
              </div>
            </div>
            
            <div class="row">
              <div class="form-group col-md-10 col-md-offset-1">
                <label for="title">Location</label><span class="err" id="err_title"></span>
                <select class="form-control" id="location" name="location">
                  <option value="ho">HO</option>
                  <option value="state">State</option>
                  <option value="district">District</option>
                </select>
              </div>
            </div>
            
            <div class="row" id="selectstate" style="display:none">
              <div class="form-group col-md-10 col-md-offset-1">
                <label for="title">Select State</label><span class="err" id="err_title"></span>
                <select class="form-control" id="state_id" name="state_id">
                  <option value="">Select State</option>
                  <? foreach($states as $state) { ?>
                  <option value="<?=$state->id?>" <?=(isset($state_id) and $state->id == $state_id) ? "selected" : ""?>><?=$state->name?></option>
                  <? } ?>
                </select>
              </div>
            </div>
            
            <div class="row" id="selectdistrict" style="display:none">
              <div class="form-group col-md-10 col-md-offset-1">
                <label for="title">Select District</label><span class="err" id="err_title"></span>
                <select class="form-control" id="district_id" name="district_id">
                  <option>Select District</option>
                </select>
              </div>
            </div>
            
            <div class="row">
              <div class="form-group col-md-10 col-md-offset-1">
                <label for="title">Contact Person Designation</label><span class="err" id="err_title"></span>
                <input type="text" name="designation" id="designation" value="" class="form-control">
              </div>
            </div>
            
            <div class="row">
              <div class="form-group col-md-10 col-md-offset-1">
                <label for="title">Contact Person Name</label><span class="err" id="err_title"></span>
                <input type="text" name="contact_person_name" id="contact_person_name" value="" class="form-control">
              </div>
            </div>
            
            <div class="row">
              <div class="form-group col-md-10 col-md-offset-1">
                <label for="title">Contact Person Number</label><span class="err" id="err_title"></span>
                <input type="text" name="contact_person_number" id="contact_person_number" value="" class="form-control">
              </div>
            </div>

            <div class="row">
              <div class="form-group col-md-10 col-md-offset-1">
                <label for="title">Subject</label><span class="err" id="err_title"></span>
                <input type="text" name="subject" id="subject" value="" class="form-control">
              </div>
            </div>
            
            <div class="row">
              <div class="form-group col-md-10 col-md-offset-1">
                <label for="title">Agenda</label><span class="err" id="err_title"></span>
                <textarea name="agenda" id="agenda" rows="3" cols="20" class="form-control"></textarea>
              </div>
            </div>
            
            <div class="row">
              <div class="form-group col-md-10 col-md-offset-1">
                <label for="title">Remarks</label><span class="err" id="err_title"></span>
                <textarea name="remarks" id="remarks" rows="3" cols="20" class="form-control"></textarea>
              </div>
            </div>
            
            <div class="row">
              <div class="form-group col-md-10 col-md-offset-1 text-center">
                <a href="<?=base_url()?>meeting">Cancel</a>
                <button type="submit" class="btn btn-default">Submit</button>
              </div>
            </div>
          </form>
        </div>
    </div>
</div>

<script type="text/javascript">
  
function get_district_list(sid,did)
{
  $.ajax({
    url: '<?=base_url()?>dashboard/get_districts',
    type: 'get',
    data: {
      state: sid
    },
    dataType: 'json',
    success: function(response) {
      $("#district_id").html("<option value=''>Select District</option>");
      $.each(response, function() {
        if (did == this['id'])
          $("#district_id").append("<option value='" + this['id'] + "' selected>" + this['value'] + " </option>");
        else
          $("#district_id").append("<option value='" + this['id'] + "'>" + this['value'] + " </option>");
      });
      $("#district_id").show();
    },
    error: function(response) {
      window.console.log(response);
    }
  });
}

$("#state_id").change(function() {
  get_district_list($(this).val(),'')
});

$("#location").change(function() {
  if ($(this).val() == "ho")
  {
    $("#selectstate").hide()
    $("#selectdistrict").hide()
  }
  if ($(this).val() == "state")
  {
    $("#selectstate").show()
    $("#selectdistrict").hide()
  }
  if ($(this).val() == "district")
  {
    $("#selectstate").show()
    $("#selectdistrict").show()
  }
});


</script>
