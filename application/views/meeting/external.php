<div class="row">
    <div class="box col-md-6 col-md-offset-3">
        <div class="box-inner">
          <form method="post" action="<?php echo base_url();?>meeting/external_submit" role="form" name="frmexternalmeeting">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-th"></i> Enter Detail of External Meeting</h2>
            </div>
            
            <div class="row">
              <div class="form-group col-md-10 col-md-offset-1">
                <label for="title"><br>Meeting Date : <?=strftime("%d-%b-%Y",strtotime($activity_date))?></label>
              </div>
            </div>
            
            <div class="row">
              <div class="form-group col-md-10 col-md-offset-1">
                <label for="title">Location</label><span class="err" id="err_title"></span>
                <select class="form-control" id="location" name="location">
                  <option value="">Select Location</option>
                  <option value="HO">HO</option>
                  <option value="state">State</option>
                  <option value="district">District</option>
                  <option value="block">Block</option>
                  <option value="cluster">Cluster</option>
                </select>
              </div>
            </div>
            
            <div id="selectmeetwithstate" class="row">
              <div class="form-group col-md-10 col-md-offset-1">
                <label for="title">State Person</label><span class="err" id="err_title"></span>
                <select class="form-control designation" id="state_person" name="state_person">
                  <option value="">Select State Person</option>
                  <option value="SPD">SPD</option>
                  <option value="APD">APD</option>
                  <option value="others">Others</option>
                </select>
              </div>
            </div>
            
            <div id="selectmeetwithdistrict" class="row">
              <div class="form-group col-md-10 col-md-offset-1">
                <label for="title">District Person</label><span class="err" id="err_title"></span>
                <select class="form-control designation" id="district_person" name="district_person">
                  <option value="">Select District Person</option>
                  <option value="DM">DM</option>
                  <option value="CEO">CEO</option>
                  <option value="DEO">DEO</option>
                  <option value="DC">DC</option>
                  <option value="others">Others</option>
                </select>
              </div>
            </div>
            
            <div id="selectmeetwithblock" class="row">
              <div class="form-group col-md-10 col-md-offset-1">
                <label for="title">Block Person</label><span class="err" id="err_title"></span>
                <select class="form-control designation" id="block_person" name="block_person">
                  <option value="">Select Block Person</option>
                  <option value="BRC">BRC</option>
                  <option value="Dy. EO">Dy. EO</option>
                  <option value="others">Others</option>
                </select>
              </div>
            </div>
            
            <div id="selectmeetwithcluster" class="row">
              <div class="form-group col-md-10 col-md-offset-1">
                <label for="title">Cluster Person</label><span class="err" id="err_title"></span>
                <select class="form-control designation" id="cluster_person" name="cluster_person">
                  <option value="">Select Cluster Person</option>
                  <option value="CRC">CRC</option>
                  <option value="others">Others</option>
                </select>
              </div>
            </div>

            <div class="row selectothers" style="display:none">
              <div class="form-group col-md-10 col-md-offset-1">
                <label for="title">Contact Person Designation</label><span class="err" id="err_title"></span>
                <input type="text" name="designation" id="designation" value="" class="form-control">
              </div>
            </div>
            
            <div class="row">
              <div class="form-group col-md-10 col-md-offset-1">
                <label for="title">Contact Person Name</label><span class="err" id="err_title"></span>
                <input type="text" name="contact_person_name" id="contact_person_name" value="" class="form-control">
              </div>
            </div>
            
            <div class="row">
              <div class="form-group col-md-10 col-md-offset-1">
                <label for="title">Contact Person Number</label><span class="err" id="err_title"></span>
                <input type="text" name="contact_person_number" id="contact_person_number" value="" class="form-control">
              </div>
            </div>
            
            <div class="row selectcluster selectplace selectblock selectdistrict selectstate">
              <div class="form-group col-md-10 col-md-offset-1">
                <label for="title">Select State</label><span class="err" id="err_title"></span>
                <select class="form-control" id="state_id" name="state_id">
                  <option value="">Select State</option>
                  <? foreach($states as $state) { ?>
                  <option value="<?=$state->id?>" <?=(isset($state_id) and $state->id == $state_id) ? "selected" : ""?>><?=$state->name?></option>
                  <? } ?>
                </select>
              </div>
            </div>
            
            <div class="row selectcluster selectplace selectblock selectdistrict">
              <div class="form-group col-md-10 col-md-offset-1">
                <label for="title">Select District</label><span class="err" id="err_title"></span>
                <select class="form-control" id="district_id" name="district_id">
                  <option>Select District</option>
                </select>
              </div>
            </div>
            
            <div class="row selectcluster selectplace selectblock">
              <div class="form-group col-md-10 col-md-offset-1">
                <label for="title">Select Block</label><span class="err" id="err_title"></span>
                <select class="form-control" id="block_id" name="block_id">
                  <option>Select Block</option>
                </select>
              </div>
            </div>
            
            <div class="row selectcluster selectplace">
              <div class="form-group col-md-10 col-md-offset-1">
                <label for="title">Select Cluster</label><span class="err" id="err_title"></span>
                <select class="form-control" id="cluster_id" name="cluster_id">
                  <option>Select Cluster</option>
                </select>
              </div>
            </div>
            
            <div class="row">
              <div class="form-group col-md-10 col-md-offset-1">
                <label for="title">Meeting Mode</label><span class="err" id="err_title"></span>
                <select class="form-control" id="meeting_mode" name="meeting_mode">
                  <option value="">Select Meeting Mode</option>
                  <option value="physical">Physical</option>
                  <option value="Telephonic">Telephonic</option>
                </select>
              </div>
            </div>

            <div class="row">
              <div class="form-group col-md-10 col-md-offset-1">
                <label for="title">Meeting Reference</label><span class="err" id="err_title"></span>
                <input type="text" name="subject" id="subject" value="" class="form-control">
              </div>
            </div>
                        
            <div class="row">
              <div class="form-group col-md-10 col-md-offset-1">
                <label for="title">Remarks</label><span class="err" id="err_title"></span>
                <textarea name="remarks" id="remarks" rows="3" cols="20" class="form-control"></textarea>
              </div>
            </div>
            
            <div class="row">
              <div class="form-group col-md-10 col-md-offset-1 text-center">
                <a href="<?=base_url()?>meeting">Cancel</a>
                <button type="submit" class="btn btn-default">Submit</button>
              </div>
            </div>
            
          </form>
        </div>
    </div>
</div>

<script type="text/javascript">
$('[id^="select"]').hide();
$(".selectplace").hide()

$(".designation").change(function(){
  if ($(this).val() == "others")
  {
    $(".selectothers").show()
  }
  else
  {
    $(".selectothers").hide()
  }
})
  
$("#location").change(function() {
  var name = $(this).val();
  if (name == "HO")
  {
    $(".selectothers").show()
  }
  else
  {
    $(".selectothers").hide()
  }
  $('[id^="selectmeetwith"]').hide();
  $(".selectplace").hide()
  $("#selectmeetwith"+name).show()
  $(".select"+name).show()
});

function get_district_list(sid,did)
{
  $.ajax({
    url: '<?=base_url()?>dashboard/get_districts',
    type: 'get',
    data: {
      state: sid
    },
    dataType: 'json',
    success: function(response) {
      $("#district_id").html("<option value=''>Select District</option>");
      $.each(response, function() {
        if (did == this['id'])
          $("#district_id").append("<option value='" + this['id'] + "' selected>" + this['value'] + " </option>");
        else
          $("#district_id").append("<option value='" + this['id'] + "'>" + this['value'] + " </option>");
      });
      $("#district_id").show();
    },
    error: function(response) {
      window.console.log(response);
    }
  });
}

function get_block_list(did,bid)
{
  $.ajax({
    url: '<?=base_url()?>dashboard/get_blocks',
    type: 'get',
    data: {
      district: did
    },
    dataType: 'json',
    success: function(response) {
      $("#block_id").html("<option value=''>Select Block</option>");
      $.each(response, function() {
        if (bid == this['id'])
          $("#block_id").append("<option value='" + this['id'] + "' selected>" + this['value'] + " </option>");
        else
          $("#block_id").append("<option value='" + this['id'] + "'>" + this['value'] + " </option>");
      });
      $("#block_id").show();
    },
    error: function(response) {
      window.console.log(response);
    }
  });
}

function get_cluster_list(bid,cid)
{
  $.ajax({
    url: '<?=base_url()?>dashboard/get_clusters',
    type: 'get',
    data: {
      block: bid
    },
    dataType: 'json',
    success: function(response) {
      $("#cluster_id").html("<option value=''>Select Cluster</option>");
      $.each(response, function() {
        if (cid == this['id'])
          $("#cluster_id").append("<option value='" + this['id'] + "' selected>" + this['value'] + " </option>");
        else
          $("#cluster_id").append("<option value='" + this['id'] + "'>" + this['value'] + " </option>");
      });
      $("#cluster_id").show();
    },
    error: function(response) {
      window.console.log(response);
    }
  });
}

$("#state_id").change(function() {
  get_district_list($(this).val(),'')
});

$("#district_id").change(function() {
  get_block_list($(this).val(),'')
});

$("#block_id").change(function() {
  get_cluster_list($(this).val(),'')
});

$("select").val("")
</script>
