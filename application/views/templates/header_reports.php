													<ul class="pcoded-item pcoded-left-item">
                              <li class="">
                                <a href="<?php echo base_url(); ?>">
                                    <span class="pcoded-micon"><i class="ti-dashboard"></i></span>
                                    <span class="pcoded-mtext">Dashboard</span>
                                    <span class="pcoded-mcaret"></span>
                                </a>
                              </li>
                                <li class="">
                                  <a href="<?php echo base_url().'dashboard/state_dashboard'; ?>">
                                      <span class="pcoded-micon"><i class="ti-dashboard"></i></span>
                                      <span class="pcoded-mtext">State Dashboard</span>
                                      <span class="pcoded-mcaret"></span>
                                  </a>
                                </li>  
                                <li class="">
                                  <a href="<?php echo base_url().'activity/sparks_data'; ?>">
                                      <span class="pcoded-micon"><i class="ti-dashboard"></i></span>
                                      <span class="pcoded-mtext">Dashboard Summary</span>
                                      <span class="pcoded-mcaret"></span>
                                  </a>
                                </li> 
                                <li class="">
                                  <a href="<?php echo base_url().'activity/baithak_summary'; ?>">
                                      <span class="pcoded-micon"><i class="ti-dashboard"></i></span>
                                      <span class="pcoded-mtext">Baithak Summary</span>
                                      <span class="pcoded-mcaret"></span>
                                  </a>
                                </li>   
                            </ul>
                            <ul class="pcoded-item pcoded-left-item">
                                <li class="">
                                    <a href="<?php echo base_url(); ?>map">
                                    <span class="pcoded-micon"><i class="ti-map"></i></span>
                                        <span class="pcoded-mtext">Map</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                </li>
                                <li class="">
                                    <a href="<?php echo base_url(); ?>activity_map">
                                    <span class="pcoded-micon"><i class="ti-map"></i></span>
                                        <span class="pcoded-mtext">Activity Map</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                </li>
                                <!--<li class="">
                                    <a href="<?php echo base_url(); ?>map/claims_list">
                                    <span class="pcoded-micon"><i class="ti-map"></i></span>
                                        <span class="pcoded-mtext">Claims</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                </li>-->
                                
                                <li class="pcoded-hasmenu">
																	<a href="javascript:void(0)">
																		<span class="pcoded-micon"><i class="ti-bar-chart-alt"></i></span>
																		<span class="pcoded-mtext">Claims</span>
																		<span class="pcoded-mcaret"></span>
																	</a>
																	<ul class="pcoded-submenu">
																		<li class="">
																			<a href="<?php echo base_url().'map/claims_list'; ?>">
																				<span class="pcoded-micon"><i class="ti-list"></i></span>
																				<span class="pcoded-mtext">Local Conv. Claim</span>
																				<span class="pcoded-mcaret"></span>
																			</a>
																		</li>  
																		<li class="">
																			<a href="<?php echo base_url().'map/stay_claims_list'; ?>">
																				<span class="pcoded-micon"><i class="ti-list"></i></span>
																				<span class="pcoded-mtext">Stay Claim</span>
																				<span class="pcoded-mcaret"></span>
																			</a>
																		</li>  
																		<li class="">
																			<a href="<?php echo base_url().'map/food_claims_list'; ?>">
																				<span class="pcoded-micon"><i class="ti-list"></i></span>
																				<span class="pcoded-mtext">Food Claim</span>
																				<span class="pcoded-mcaret"></span>
																			</a>
																		</li> 
																		<li class="">
																			<a href="<?php echo base_url().'map/other_claims_list'; ?>">
																				<span class="pcoded-micon"><i class="ti-list"></i></span>
																				<span class="pcoded-mtext">Other Claim</span>
																				<span class="pcoded-mcaret"></span>
																			</a>
																		</li> 
																		<li class="">
																			<a href="<?php echo base_url().'map/ld_travel_claims'; ?>">
																				<span class="pcoded-micon"><i class="ti-list"></i></span>
																				<span class="pcoded-mtext">LD Claim</span>
																				<span class="pcoded-mcaret"></span>
																			</a>
																		</li> 
																	</ul>	
																</li>		
																
                                
                                
                           </ul>
                            <div class="pcoded-navigatio-lavel" menu-title-theme="theme5">Reports</div>
                            <ul class="pcoded-item pcoded-left-item">
																<li class="pcoded-hasmenu">
																	<a href="javascript:void(0)">
																			<span class="pcoded-micon"><i class="ti-bar-chart-alt"></i></span>
																			<span class="pcoded-mtext">Baithak</span>
																			<span class="pcoded-mcaret"></span>
																	</a>
																	<ul class="pcoded-submenu">
																		<li class="">
																			<a href="<?php echo base_url().'reports/sss_app_usage'; ?>">
																					<span class="pcoded-micon"><i class="ti-list"></i></span>
																					<span class="pcoded-mtext">SSS App Usage</span>
																					<span class="pcoded-mcaret"></span>
																			</a>
																		</li>  
																		<li class="">
																			<a href="<?php echo base_url().'reports/teachers_not_registered'; ?>">
																					<span class="pcoded-micon"><i class="ti-list"></i></span>
																					<span class="pcoded-mtext">Teachers Not Registered</span>
																					<span class="pcoded-mcaret"></span>
																			</a>
																		</li>  
																	</ul>	
																</li>		
                                <li class="pcoded-hasmenu">
                                    <a href="javascript:void(0)">
                                        <span class="pcoded-micon"><i class="ti-bar-chart-alt"></i></span>
                                        <span class="pcoded-mtext">Spark</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                    <ul class="pcoded-submenu">
                                        <li class=" ">
																					<a href="<?php echo base_url(); ?>spark_productivity_reports">
																						<span class="pcoded-micon"><i class="ti-angle-right"></i></span>
																						<span class="pcoded-mtext">Productivity</span>
																						<span class="pcoded-mcaret"></span>
																					</a>
                                        </li>
                                        <li class=" ">
																					<a href="<?php echo base_url(); ?>spark_daily_activity_report">
																						<span class="pcoded-micon"><i class="ti-angle-right"></i></span>
																						<span class="pcoded-mtext">Daily Activity</span>
																						<span class="pcoded-mcaret"></span>
																					</a>
                                        </li>
																				
                                    </ul>
                                </li>
                                 

                                <li class="pcoded-hasmenu">
                                    <a href="javascript:void(0)">
                                        <span class="pcoded-micon"><i class="ti-bar-chart-alt"></i></span>
                                        <span class="pcoded-mtext">State</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                    <ul class="pcoded-submenu">
                                        <li class=" ">
                                            <a href="<?php echo base_url(); ?>reports/state">
                                                <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                                <span class="pcoded-mtext">Productivity</span>
                                                <span class="pcoded-mcaret"></span>
                                            </a>
                                        </li>
                                        <li class=" ">
                                            <a href="<?php echo base_url(); ?>state_summary/summary_view">
                                                <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                                <span class="pcoded-mtext">Summary</span>
                                                <span class="pcoded-mcaret"></span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                
                                <li class="pcoded-hasmenu">
                                    <a href="javascript:void(0)">
                                        <span class="pcoded-micon"><i class="ti-bar-chart-alt"></i></span>
                                        <span class="pcoded-mtext">Rating Archive</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                    <ul class="pcoded-submenu">
                                        <li class=" ">
                                            <a href="<?php echo base_url(); ?>reports_archive/spark_school_rating">
                                                <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                                <span class="pcoded-mtext">Spark School Rating</span>
                                                <span class="pcoded-mcaret"></span>
                                            </a>
                                        </li>
                                        <li class=" ">
                                            <a href="<?php echo base_url(); ?>reports_archive/block_school_rating">
                                                <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                                <span class="pcoded-mtext">Block School Rating</span>
                                                <span class="pcoded-mcaret"></span>
                                            </a>
                                        </li>
                                        <li class=" ">
                                            <a href="<?php echo base_url(); ?>reports_archive/district_school_rating">
                                                <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                                <span class="pcoded-mtext">District School Rating</span>
                                                <span class="pcoded-mcaret"></span>
                                            </a>
                                        </li>
                                        <li class=" ">
                                            <a href="<?php echo base_url(); ?>reports_archive/state_school_rating">
                                                <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                                <span class="pcoded-mtext">State School Rating</span>
                                                <span class="pcoded-mcaret"></span>
                                            </a>
                                        </li>
                                        <li class=" ">
                                            <a href="<?php echo base_url(); ?>reports/national_school_rating">
                                                <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                                <span class="pcoded-mtext">National School Rating</span>
                                                <span class="pcoded-mcaret"></span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="pcoded-hasmenu">
                                    <a href="javascript:void(0)">
                                        <span class="pcoded-micon"><i class="ti-bar-chart-alt"></i></span>
                                        <span class="pcoded-mtext">Rating (From Sept 2019)</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                    <ul class="pcoded-submenu">
                                        <li class=" ">
                                            <a href="<?php echo base_url(); ?>reports/spark_school_rating">
                                                <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                                <span class="pcoded-mtext">Spark School Rating</span>
                                                <span class="pcoded-mcaret"></span>
                                            </a>
                                        </li>
                                        <li class=" ">
                                            <a href="<?php echo base_url(); ?>reports/block_school_rating">
                                                <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                                <span class="pcoded-mtext">Block School Rating</span>
                                                <span class="pcoded-mcaret"></span>
                                            </a>
                                        </li>
                                        <li class=" ">
                                            <a href="<?php echo base_url(); ?>reports/district_school_rating">
                                                <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                                <span class="pcoded-mtext">District School Rating</span>
                                                <span class="pcoded-mcaret"></span>
                                            </a>
                                        </li>
                                        <li class=" ">
                                            <a href="<?php echo base_url(); ?>reports/state_school_rating">
                                                <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                                <span class="pcoded-mtext">State School Rating</span>
                                                <span class="pcoded-mcaret"></span>
                                            </a>
                                        </li>
                                        <li class=" ">
                                            <a href="<?php echo base_url(); ?>reports/national_school_rating">
                                                <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                                <span class="pcoded-mtext">National School Rating</span>
                                                <span class="pcoded-mcaret"></span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                  <li class="">
                                    <a href="<?php echo base_url(); ?>reports/school_abc_audit_report">
                                        <span class="pcoded-micon"><i class="ti-bar-chart-alt"></i></span>
                                        <span class="pcoded-mtext">School ABC/Audit Report</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                  </li>
                                  <li class="">
                                    <a href="<?php echo base_url(); ?>meeting/spark_meeting_report">
                                        <span class="pcoded-micon"><i class="ti-bar-chart-alt"></i></span>
                                        <span class="pcoded-mtext">Spark Meeting Report</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                   <li class="pcoded-hasmenu">
                                    <a href="javascript:void(0)">
                                        <span class="pcoded-micon"><i class="ti-bar-chart-alt"></i></span>
                                        <span class="pcoded-mtext">Spark Training Report</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>  
                                    <ul class="pcoded-submenu">
                                        <li class=" ">
                                            <a href="<?php echo base_url(); ?>training/training_quality">
                                                <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                                <span class="pcoded-mtext">Quality Report</span>
                                                <span class="pcoded-mcaret"></span>
                                            </a>
                                        </li>
                                        <li class=" ">
                                            <a href="<?php echo base_url(); ?>training/training_logistic">
                                                <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                                <span class="pcoded-mtext">Logistic Report</span>
                                                <span class="pcoded-mcaret"></span>
                                            </a>
                                        </li>
                                    </ul>
                                  </li>
                                  </li>
                                <li class="pcoded-hasmenu">
                                    <a href="javascript:void(0)">
                                        <span class="pcoded-micon"><i class="ti-bar-chart-alt"></i></span>
                                        <span class="pcoded-mtext">Enrollment</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                    <ul class="pcoded-submenu">
                                        <li class=" ">
                                            <a href="<?php echo base_url(); ?>high_enrol_spark/spark_enrol_display">
                                                <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                                <span class="pcoded-mtext">High Enrollment Spark</span>
                                                <span class="pcoded-mcaret"></span>
                                            </a>
                                        </li>
                                        <li class=" ">
                                            <a href="<?php echo base_url(); ?>high_enrol_state/state_enrol">
                                                <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                                <span class="pcoded-mtext">High Enrollment National</span>
                                                <span class="pcoded-mcaret"></span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="pcoded-hasmenu">
                                    <a href="javascript:void(0)">
                                        <span class="pcoded-micon"><i class="ti-bar-chart-alt"></i></span>
                                        <span class="pcoded-mtext">Manager</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                    <ul class="pcoded-submenu">
                                        <li class=" ">
                                            <a href="<?php echo base_url(); ?>manager_reports/manager_productivity">
                                                <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                                <span class="pcoded-mtext">Manager Productivity</span>
                                                <span class="pcoded-mcaret"></span>
                                            </a>
                                        </li>
                                        <li class=" ">
                                            <a href="<?php echo base_url(); ?>manager_reports/manager_daily_activity">
                                                <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                                <span class="pcoded-mtext">Manager Daily Activity</span>
                                                <span class="pcoded-mcaret"></span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="pcoded-hasmenu">
                                    <a href="javascript:void(0)">
                                        <span class="pcoded-micon"><i class="ti-bar-chart-alt"></i></span>
                                        <span class="pcoded-mtext">Ranking</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                    <ul class="pcoded-submenu">
                                        <li class=" ">
                                            <a href="<?php echo base_url(); ?>report/teacher_feedback/spark_training_ranking">
                                                <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                                <span class="pcoded-mtext">Spark Training Ranking</span>
                                                <span class="pcoded-mcaret"></span>
                                            </a>
                                        </li>
                                        <li class=" ">
                                            <a href="<?php echo base_url(); ?>report/teacher_feedback/spark_feedback_ranking_new">
                                                <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                                <span class="pcoded-mtext">Spark Feedback Ranking</span>
                                                <span class="pcoded-mcaret"></span>
                                            </a>
                                        </li>
                                        <li class=" ">
                                            <a href="<?php echo base_url(); ?>report/teacher_feedback/district_ranking_ack">
                                                <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                                                <span class="pcoded-mtext">District Ranking</span>
                                                <span class="pcoded-mcaret"></span>
                                            </a>
                                        </li>
                                    </ul>
                                 </li>   
                                
                            </ul>                            
                            
                            <div class="pcoded-navigatio-lavel" menu-title-theme="theme5">Attendance</div>
                            <ul class="pcoded-item pcoded-left-item">
                              <li class=" ">
																<a href="<?php echo base_url(); ?>activity/attendance_calendar">
																	<span class="pcoded-micon"><i class="ti-calendar"></i></span>
																	<span class="pcoded-mtext">Attendance Calendar</span>
																	<span class="pcoded-mcaret"></span>
																</a>
															</li>
															<li class="">
																<a href="<?php echo base_url(); ?>activity/attendance_list">
																	<span class="pcoded-micon"><i class="ti-list"></i></span>
																	<span class="pcoded-mtext">Attendance List</span>
																	<span class="pcoded-mcaret"></span>
																</a>
															</li>
															<li class="">
																<a href="<?php echo base_url()?>activity/attendance_regularizations">
																	<span class="pcoded-micon"><i class="ti-list"></i></span>
																	<span class="pcoded-mtext">Attendance Regularizations</span>
																	<span class="pcoded-mcaret"></span>
																</a>
															</li>														
															<li class=" ">
																<a href="<?php echo base_url(); ?>activity/change_password">
																	<span class="pcoded-micon"><i class="ti-key"></i></span>
																	<span class="pcoded-mtext">Change Password</span>
																	<span class="pcoded-mcaret"></span>
																</a>
															</li>
                           </ul>
                           
                           <div class="pcoded-navigatio-lavel" menu-title-theme="theme5">My Leave</div>
														<ul class="pcoded-item pcoded-left-item">
															<li class=" ">
																<a href="<?php echo base_url(); ?>activity/my_leave_request">
																	<span class="pcoded-micon"><i class="ti-list"></i></span>
																	<span class="pcoded-mtext">My Leave Request</span>
																	<span class="pcoded-mcaret"></span>
																</a>
															</li>
															<li class=" ">
																<a href="<?php echo base_url(); ?>activity/my_leave_list">
																	<span class="pcoded-micon"><i class="ti-list"></i></span>
																	<span class="pcoded-mtext">My Leave List</span>
																	<span class="pcoded-mcaret"></span>
																</a>
															</li>
														</ul>
                            
                            <div class="pcoded-navigatio-lavel" menu-title-theme="theme5">Activities</div>
                            <ul class="pcoded-item pcoded-left-item">
                                <li class="">
                                    <a href="<?php echo base_url(); ?>activity">
                                        <span class="pcoded-micon"><i class="ti-search"></i></span>
                                        <span class="pcoded-mtext">Find Activities</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                </li>
                               <li class="">
                                  <a href="<?php echo base_url(); ?>activity/training_list">
                                    <span class="pcoded-micon"><i class="ti-list"></i></span>
                                    <span class="pcoded-mtext">Training List</span>
                                    <span class="pcoded-mcaret"></span>
                                  </a>
                                </li>
                                <li class="">
                                  <a href="<?php echo base_url(); ?>activity/address_mismatch">
                                    <span class="pcoded-micon"><i class="ti-list"></i></span>
                                    <span class="pcoded-mtext">Address Mismatch</span>
                                    <span class="pcoded-mcaret"></span>
                                  </a>
                                </li>
                                <li class="">
                                  <a href="<?php echo base_url(); ?>report/activity_dump">
                                    <span class="pcoded-micon"><i class="ti-list"></i></span>
                                    <span class="pcoded-mtext">Activity Dump</span>
                                    <span class="pcoded-mcaret"></span>
                                  </a>
                                </li>
                                <li class="">
                                  <a href="<?php echo base_url()?>admin/survey">
                                    <span class="pcoded-micon"><i class="ti-list"></i></span>
                                    <span class="pcoded-mtext">Survey</span>
                                    <span class="pcoded-mcaret"></span>
                                  </a>
                                </li>
                                <li class="">
                                  <a href="<?php echo base_url(); ?>reports/testline_responses">
                                    <span class="pcoded-micon"><i class="ti-list"></i></span>
                                    <span class="pcoded-mtext">Baseline Dump</span>
                                    <span class="pcoded-mcaret"></span>
                                  </a>
                                </li>
                                <li class="">
                                  <a href="<?php echo base_url(); ?>reports/survey_report">
                                    <span class="pcoded-micon"><i class="ti-list"></i></span>
                                    <span class="pcoded-mtext">Survey Report</span>
                                    <span class="pcoded-mcaret"></span>
                                  </a>
                                </li>
                            </ul>
                        
