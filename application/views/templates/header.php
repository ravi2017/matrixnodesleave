<!DOCTYPE html>
<html lang="en">
  <head>
    <title>SF Activity</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="Phoenixcoded">
    <meta name="keywords" content=", Flat ui, Admin , Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app">
    <meta name="author" content="Phoenixcoded">
    
    <link rel="icon" href="<?php echo base_url(); ?>assets/v2/images/favicon.ico" type="image/x-icon">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/v2/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/v2/icon/themify-icons/themify-icons.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/v2/icon/icofont/css/icofont.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/v2/pages/flag-icon/flag-icon.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/v2/pages/menu-search/css/component.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/v2/css/style.css?v=1.5">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/v2/css/color/color-1.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/v2/css/linearicons.css" >
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/v2/css/simple-line-icons.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/v2/css/ionicons.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/v2/css/jquery.mCustomScrollbar.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/v2/css/c3.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/v2/css/datatables.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/v2/css/jquery-ui.css">
    
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/v2/css/jquery.datetimepicker.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/v2/css/jquery.timesetter.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/v2/css/select2.min.css"/>
    
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/v2/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/v2/js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/v2/js/tether.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/v2/js/bootstrap.min.js"></script>
    
    <script src="<?php echo base_url(); ?>assets/v2/js/jquery.datetimepicker.js"></script>
    <script src="<?php echo base_url(); ?>assets/v2/js/jquery.timesetter.js"></script>
    
    <?php if (isset($custom_styles) and count($custom_styles) > 0) { ?>
      <?php foreach($custom_styles as $custom_style) { ?>
        <?php if (substr( $custom_style, 0, 4 ) === "http") { ?>
        <link rel="stylesheet" type="text/css" href="<?php echo $custom_style; ?>">
        <?php } else { ?>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url("assets/v2/css/".$custom_style); ?>">
        <?php } ?>
      <?php } ?>
    <?php } ?>
    <script>
    var BASE_URL = '<?php echo base_url(); ?>';
    </script>
    <style>
      ul.header-mtext li:hover a{
        color : #f1592a !important;
      }
    </style>
  </head>
  
  <body>
    <!-- Pre-loader start -->
    <div class="theme-loader">
        <div class="ball-scale">
            <div></div>
        </div>
    </div>
    <!-- Pre-loader end -->


    <div id="pcoded" class="pcoded">
      <div class="pcoded-overlay-box"></div>
      <div class="pcoded-container navbar-wrapper">

        <nav class="navbar header-navbar pcoded-header" header-theme="theme4">
            <div class="navbar-wrapper">
                <div class="navbar-logo">
                    <a class="mobile-menu" id="mobile-collapse" href="#!">
                        <i class="ti-menu"></i>
                    </a>
                    <a class="mobile-search morphsearch-search" href="#">
                        <i class="ti-search"></i>
                    </a>
                    <a href="index.html">
                        <img class="img-fluid" src="<?php echo base_url(); ?>assets/v2/images/sampark-logo.png" alt="Theme-Logo" style="width:125px;background: #eee;" />
                    </a>
                    <a class="mobile-options">
                        <i class="ti-more"></i>
                    </a>
                </div>
                <div class="navbar-container container-fluid">
                    <div>
                        <ul class="nav-left">
                            <li>
                                <div class="sidebar_toggle"><a href="javascript:void(0)"><i class="ti-menu"></i></a></div>
                            </li>
                        </ul>
                        <ul class="nav-right">
                            <?php if($current_role == "HR") { ?> 
															<li class="user-profile header-notification">
                                <a href="#!">
                                    <span>HR</span>
                                    <i class="ti-angle-down"></i>
                                </a>
                                <ul class="show-notification profile-notification header-mtext">
                                    <li class="">
																			<a href="<?php echo base_url()?>admin/employees">
																				<i class="ti-user"></i> Employees
																			</a>
                                    </li>
                                    <li class="">
																			<a href="<?php echo base_url()?>admin/leaves/credit_leave_list">
																				<i class="ti-map"></i> Credit Leaves
																			</a>
                                    </li>
                                    <li>
																			<a href="<?php echo base_url()?>admin/leaves/group_leaves">
																				<i class="ti-map"></i> Group Leaves
																			</a>
                                    </li>
                                    <li class="">
																			<a href="<?php echo base_url()?>admin/leaves/carry_forward_policies">
																				<i class="ti-comment-alt"></i> Carry Forward Policies
																			</a>
                                    </li>
                                    <li>
																			<a href="<?php echo base_url()?>activity/attendance_regularizations">
																				<i class="ti-user"></i> Attendance Regularizations
																			</a>
                                    </li>
                                    <li>
																			<a href="<?php echo base_url()?>admin/employees/employee_attendance">
																				<i class="ti-user"></i> Attendance Details
																			</a>
                                    </li>
                                    <li>
																			<a href="<?php echo base_url()?>admin/holidays">
																					<i class="ti-wand"></i> Holidays
																			</a>
                                    </li>
                                    <li>
																			<a href="<?php echo base_url()?>admin/offdays">
																					<i class="ti-wand"></i> Offdays
																			</a>
                                    </li>
                                    <li class="">
																			<a href="<?php echo base_url()?>admin/school_holidays">
																				<i class="ti-wand"></i> School Holiday
																			</a>
                                    </li>
																</ul>
														 </li>		
														<?	}	?>	
                            
                            <?php if ($current_role == "super_admin") { ?>                          
                            <li class="user-profile header-notification">
                                <a href="#!">
                                    <span>Administration</span>
                                    <i class="ti-angle-down"></i>
                                </a>
                                <ul class="show-notification profile-notification header-mtext">
                                    <li class="">
                                        <a href="<?php echo base_url()?>admin/states">
                                          <i class="ti-map"></i> States
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url()?>admin/schools">
                                            <i class="ti-medall"></i> Schools
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url()?>admin/sparks">
                                            <i class="ti-user"></i> Sparks
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url()?>admin/sparks/attendance_list">
                                            <i class="ti-list"></i> Attendance List
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url()?>admin/travels">
                                            <i class="ti-car"></i> Travel Modes
                                        </a>
                                    </li>
                                    
                                    <li>
                                        <a href="<?php echo base_url()?>admin/sparks/change_state">
                                            <i class="ti-location-pin"></i> Spark Movement
                                        </a>
                                    </li>                                   
                                </ul>
                            </li>                            
                            <?php } ?>
                            <li class="user-profile header-notification">
                                <a href="<?php echo base_url()?>dashboard/logout"><span>Logout <i class="ion-log-out"></i></span></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>

        <div class="pcoded-main-container">
                <div class="pcoded-wrapper">
                    <nav class="pcoded-navbar" pcoded-header-position="relative">
                        <div class="sidebar_toggle"><a href="#"><i class="icon-close icons"></i></a></div>
                        <div class="pcoded-inner-navbar main-menu">
                            <div class="">
                                <div class="main-menu-header">
                                    <img class="img-40" src="<?php echo base_url(); ?>assets/v2/images/logo.png" alt="User-Profile-Image">
                                    <div class="user-details">
                                        <span><?php echo $current_name; ?></span>
                                        <span id="more-details"><?php echo ucwords(str_replace("_"," ",$current_role)); ?><i class="ti-angle-down"></i></span>
                                    </div>
                                </div>

                                <div class="main-menu-content">
                                    <ul>
                                        <li class="more-details">
                                            <a href="user-profile.html"><i class="ti-user"></i>View Profile</a>
                                            <a href="#!"><i class="ti-settings"></i>Settings</a>
                                            <a href="#!"><i class="ti-layout-sidebar-left"></i>Logout</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            
                            <? 
														 if($current_role == "HR") { 
																include_once('header_hr.php');
														 }
														 else if($current_role == "accounts") { 
																include_once('header_accounts.php');
                             }
                             else if($current_role == "reports") { 	
																include_once('header_reports.php');
														 }
														 else if(isset($current_group) && $current_group == "head_office" && $current_role != "hr" && $current_role != "accounts" && $current_role != "reports") { 		
																include_once('header_ho_sparks.php');
                             }
                             else { 
													  ?>	
															
                            <ul class="pcoded-item pcoded-left-item">
                              <li class="">
                                <a href="<?php echo base_url(); ?>">
                                    <span class="pcoded-micon"><i class="ti-dashboard"></i></span>
                                    <span class="pcoded-mtext">Dashboard</span>
                                    <span class="pcoded-mcaret"></span>
                                </a>
                              </li>
                            </ul>
                            <ul class="pcoded-item pcoded-left-item">
                                <?php if ($current_role != "field_user" && $current_role != "super_admin" && $current_role != "accounts" && $current_role != "reports") { ?>
                                <li class="">
                                    <a href="<?php echo base_url(); ?>team_members/view">
                                    <span class="pcoded-micon"><i class="ti-user"></i></span>
                                        <span class="pcoded-mtext">Team Members</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                </li>
                                <?php } ?>
                                <? if($current_role != "reports") { ?>
                                <li class="pcoded-hasmenu">
																		<li class="">
																			<a href="<?php echo base_url().'map/other_claims_list'; ?>">
																					<span class="pcoded-micon"><i class="ti-list"></i></span>
																					<span class="pcoded-mtext">Claim</span>
																					<span class="pcoded-mcaret"></span>
																			</a>
																		</li> 
																</li>		
															
                                <?  } ?>
                            </ul>
                            
                            <div class="pcoded-navigatio-lavel" menu-title-theme="theme5">Attendance</div>
                            <ul class="pcoded-item pcoded-left-item">
                              <li class=" ">
																<a href="<?php echo base_url(); ?>activity/attendance_calendar">
																	<span class="pcoded-micon"><i class="ti-calendar"></i></span>
																	<span class="pcoded-mtext">Attendance Calendar</span>
																	<span class="pcoded-mcaret"></span>
																</a>
															</li>
															<?php if($current_group != 'head_office' && ($current_role == "field_user" || $current_role == "state_person" || $current_role == "manager")) { ?>
																<li class="">
																	<a href="<?php echo base_url()?>activity/add_attendance_regularization">
																		<span class="pcoded-micon"><i class="ti-list"></i></span>
																		<span class="pcoded-mtext">Add Attendance</span>
																		<span class="pcoded-mcaret"></span>
																	</a>
																</li>
															<?php } ?>	
															
															<?php if ($current_role != "super_admin") { ?>
															<li class="">
																<a href="<?php echo base_url(); ?>activity/attendance_list">
																	<span class="pcoded-micon"><i class="ti-list"></i></span>
																	<span class="pcoded-mtext">Attendance List</span>
																	<span class="pcoded-mcaret"></span>
																</a>
															</li>
															<li class=" ">
																<a href="<?php echo base_url(); ?>activity/change_password">
																	<span class="pcoded-micon"><i class="ti-key"></i></span>
																	<span class="pcoded-mtext">Change Password</span>
																	<span class="pcoded-mcaret"></span>
																</a>
															</li>
															<?	}	?>
                           </ul>
                           
                           <?php if ($current_role != "super_admin") { ?>
													 <div class="pcoded-navigatio-lavel" menu-title-theme="theme5">My Leave</div>
														<ul class="pcoded-item pcoded-left-item">
															<li class=" ">
																<a href="<?php echo base_url(); ?>activity/my_leave_request">
																	<span class="pcoded-micon"><i class="ti-list"></i></span>
																	<span class="pcoded-mtext">My Leave Request</span>
																	<span class="pcoded-mcaret"></span>
																</a>
															</li>
															<li class=" ">
																<a href="<?php echo base_url(); ?>activity/my_leave_list">
																	<span class="pcoded-micon"><i class="ti-list"></i></span>
																	<span class="pcoded-mtext">My Leave List</span>
																	<span class="pcoded-mcaret"></span>
																</a>
															</li>
														</ul>
                           <?	}	?>
                            
                              <?php if ($current_role != "field_user") { ?>
																<ul class="pcoded-item pcoded-left-item">
                                <li class="">
                                    <a href="<?php echo base_url(); ?>activity_approval">
                                    <span class="pcoded-micon"><i class="ti-check-box"></i></span>
                                        <span class="pcoded-mtext">Approve Team Leave</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                </li>
                                </ul>
                               <? } ?> 
														<?	} ?>
                        </div>
                    </nav>
                    <div class="pcoded-content">
                        <div class="pcoded-inner-content">
                            <!-- Main-body start -->
                            <div class="main-body">
                                <div class="page-wrapper">
                                    <!-- Page-header start -->
                                    <?php 
                                    if(isset($current_role) && $current_role == 'state_person'){
                                      show_training_notification($user_state_id);
                                    } 
                                    ?>
                                    <?php 
                                    $aroles = array('state_person', 'field_user', 'manager');
                                    $allow_login_ids = array('120');
                                    //echo"<>".$current_role." >> ".$current_user_id;
                                    if(isset($current_role) && in_array($current_role, $aroles)){
                                      //show_360_url($current_role, $current_user_id, $user_state_id);
                                    } 
                                    ?>
                                    <div class="page-header">
                                        <div class="page-header-title">
                                            <?
                                              $method_name = ($this->router->fetch_method() != 'index' ? '> '.ucwords(str_replace("_"," ",$this->router->fetch_method())) : ''); 
                                            ?>
                                            <h5><?php echo ucwords(str_replace("_"," ",$this->router->fetch_class())); ?> <?php echo $method_name; ?></h5>
                                        </div>
                                        <div class="page-header-breadcrumb" style="display:none;">
                                            <ul class="breadcrumb-title">
                                                <li class="breadcrumb-item">
                                                    <a href="index.html">
                                                        <i class="icofont icofont-home"></i>
                                                    </a>
                                                </li>
                                                <li class="breadcrumb-item"><a href="#!">Basic Componenets</a>
                                                </li>
                                                <li class="breadcrumb-item"><a href="#!">Breadcrumb</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <!-- Page-header end -->

                                    <!-- Page-body start -->
                                    <div class="page-body breadcrumb-page">
                                    
