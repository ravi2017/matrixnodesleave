<?php echo $this->load->view('templates/login_header', '', true) ?>
<!-- Authentication card start -->
<div class="login-card card-block auth-body">
   <?php $attributes = array('class' => 'md-float-material'); ?>
   <?php echo form_open('verifylogin', $attributes); ?>
        <div class="text-center">
            <img src="<?php echo base_url(); ?>assets/v2/images/logo-big.png" alt="logo.png" style="width:100px">
        </div>
        <div class="auth-box">
            <div class="row m-b-20">
                <div class="col-md-12">
                    <h3 class="text-left txt-primary">Sign In</h3>
                </div>
            </div>
            <hr/>
            <?php if(validation_errors()) { ?>
            <div class="alert alert-danger"><?php echo validation_errors(); ?></div><?php } ?>
            <div class="form-group">
              <div class="input-group">
                <input type="text" name="username" class="form-control" placeholder="Username">
                <span class="md-line"></span>
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <input type="password" name="password" class="form-control" placeholder="Password">
                <span class="md-line"></span>
              </div>
            </div>
            <div class="row m-t-25 text-left">
                <div class="col-sm-7 col-xs-12">
                    <div class="checkbox-fade fade-in-primary">
                        <label>
                            <input type="checkbox" value="">
                            <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i></span>
                            <span class="text-inverse">Remember me</span>
                        </label>
                    </div>
                </div>
                <div class="col-sm-5 col-xs-12 forgot-phone text-right" style="display:none;">
                    <a href="forgot-password.html" class="text-right f-w-600 text-inverse"> Forgot Your Password?</a>
                </div>
            </div>
            <div class="row m-t-30">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-primary btn-md btn-block waves-effect text-center m-b-20">Sign in</button>
                </div>
            </div>
        </div>
    </form>
    <!-- end of form -->
</div>
<!-- Authentication card end -->
 <?php echo $this->load->view('templates/login_footer','',true) ?>
