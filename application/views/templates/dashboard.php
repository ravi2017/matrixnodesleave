<?php echo $this->load->view('templates/header','',true) ?>
<?php //echo $this->load->view('templates/left_menu') ?>
<?php //echo $this->load->view('templates/breadcrump') ?>
<?php $alert = $this->session->flashdata('alert'); ?>
<?php if (isset($alert) and $alert != "") { ?>
  <div class="alert alert-danger">
  <button class="close" data-dismiss="alert" type="button">×</button>
  <?php echo $alert?>
  </div>
<?php } ?>
<?php $success = $this->session->flashdata('success'); ?>
<?php if (isset($success) and $success != "") { ?>
  <div class="alert alert-success">
  <button class="close" data-dismiss="alert" type="button">×</button>
  <?php echo $success?>
  </div>
<?php } ?>
<?php echo $this->load->view($partial,'',true) ?>
<?php echo $this->load->view('templates/footer','',true) ?>
    
