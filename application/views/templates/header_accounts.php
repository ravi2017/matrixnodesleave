													<ul class="pcoded-item pcoded-left-item">
                              <li class="">
                                <a href="<?php echo base_url(); ?>">
                                    <span class="pcoded-micon"><i class="ti-dashboard"></i></span>
                                    <span class="pcoded-mtext">Dashboard</span>
                                    <span class="pcoded-mcaret"></span>
                                </a>
                              </li>
                              
                                <li class="">
                                  <a href="<?php echo base_url().'dashboard/state_dashboard'; ?>">
                                      <span class="pcoded-micon"><i class="ti-dashboard"></i></span>
                                      <span class="pcoded-mtext">State Dashboard</span>
                                      <span class="pcoded-mcaret"></span>
                                  </a>
                                </li>  
                                <li class="">
                                  <a href="<?php echo base_url().'activity/sparks_data'; ?>">
                                      <span class="pcoded-micon"><i class="ti-dashboard"></i></span>
                                      <span class="pcoded-mtext">Dashboard Summary</span>
                                      <span class="pcoded-mcaret"></span>
                                  </a>
                                </li>  
                                <li class="">
                                  <a href="<?php echo base_url().'activity/baithak_summary'; ?>">
                                      <span class="pcoded-micon"><i class="ti-dashboard"></i></span>
                                      <span class="pcoded-mtext">Baithak Summary</span>
                                      <span class="pcoded-mcaret"></span>
                                  </a>
                                </li>  
                            </ul>
                            <ul class="pcoded-item pcoded-left-item">
                                <li class="">
                                    <a href="<?php echo base_url(); ?>map">
                                    <span class="pcoded-micon"><i class="ti-map"></i></span>
                                        <span class="pcoded-mtext">Map</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                </li>
                                <li class="">
                                    <a href="<?php echo base_url(); ?>activity_map">
																			<span class="pcoded-micon"><i class="ti-map"></i></span>
																			<span class="pcoded-mtext">Activity Map</span>
																			<span class="pcoded-mcaret"></span>
                                    </a>
                                </li>
                                <li class="pcoded-hasmenu">
																	<a href="javascript:void(0)">
																		<span class="pcoded-micon"><i class="ti-bar-chart-alt"></i></span>
																		<span class="pcoded-mtext">Claims</span>
																		<span class="pcoded-mcaret"></span>
																	</a>
																	<ul class="pcoded-submenu">
																		<li class="">
																			<a href="<?php echo base_url().'map/claims_list'; ?>">
																				<span class="pcoded-micon"><i class="ti-list"></i></span>
																				<span class="pcoded-mtext">Local Conv. Claim</span>
																				<span class="pcoded-mcaret"></span>
																			</a>
																		</li>  
																		<li class="">
																			<a href="<?php echo base_url().'map/stay_claims_list'; ?>">
																				<span class="pcoded-micon"><i class="ti-list"></i></span>
																				<span class="pcoded-mtext">Stay Claim</span>
																				<span class="pcoded-mcaret"></span>
																			</a>
																		</li>  
																		<li class="">
																			<a href="<?php echo base_url().'map/food_claims_list'; ?>">
																				<span class="pcoded-micon"><i class="ti-list"></i></span>
																				<span class="pcoded-mtext">Food Claim</span>
																				<span class="pcoded-mcaret"></span>
																			</a>
																		</li> 
																		<li class="">
																			<a href="<?php echo base_url().'map/other_claims_list'; ?>">
																				<span class="pcoded-micon"><i class="ti-list"></i></span>
																				<span class="pcoded-mtext">Other Claim</span>
																				<span class="pcoded-mcaret"></span>
																			</a>
																		</li> 
																		<li class="">
																			<a href="<?php echo base_url().'map/ld_travel_claims'; ?>">
																				<span class="pcoded-micon"><i class="ti-list"></i></span>
																				<span class="pcoded-mtext">LD Claim</span>
																				<span class="pcoded-mcaret"></span>
																			</a>
																		</li> 
																		<li class="">
																			<a href="<?php echo base_url().'map/spark_claims_list'; ?>">
																				<span class="pcoded-micon"><i class="ti-list"></i></span>
																				<span class="pcoded-mtext">Spark All Claim</span>
																				<span class="pcoded-mcaret"></span>
																			</a>
																		</li> 
																	</ul>	
																</li>		
																
                                <li class="pcoded-hasmenu">
																	<a href="javascript:void(0)">
																		<span class="pcoded-micon"><i class="ti-bar-chart-alt"></i></span>
																		<span class="pcoded-mtext">Claims Exceed</span>
																		<span class="pcoded-mcaret"></span>
																	</a>
																	<ul class="pcoded-submenu">
																		<li class="">
																			<a href="<?php echo base_url().'map/claims_exceed'; ?>">
																				<span class="pcoded-micon"><i class="ti-list"></i></span>
																				<span class="pcoded-mtext">Local Conv. Exceed</span>
																				<span class="pcoded-mcaret"></span>
																			</a>
																		</li>  
																		<li class="">
																			<a href="<?php echo base_url().'map/stay_claims_exceed'; ?>">
																				<span class="pcoded-micon"><i class="ti-list"></i></span>
																				<span class="pcoded-mtext">Stay Claim Exceed</span>
																				<span class="pcoded-mcaret"></span>
																			</a>
																		</li>  
																		<li class="">
																			<a href="<?php echo base_url().'map/food_claims_exceed'; ?>">
																				<span class="pcoded-micon"><i class="ti-list"></i></span>
																				<span class="pcoded-mtext">Food Claim Exceed</span>
																				<span class="pcoded-mcaret"></span>
																			</a>
																		</li> 
																		<li class="">
																			<a href="<?php echo base_url().'map/other_claims_exceed'; ?>">
																				<span class="pcoded-micon"><i class="ti-list"></i></span>
																				<span class="pcoded-mtext">Other Claim Exceed</span>
																				<span class="pcoded-mcaret"></span>
																			</a>
																		</li> 
																		<li class="">
																			<a href="<?php echo base_url().'map/ld_claims_exceed'; ?>">
																				<span class="pcoded-micon"><i class="ti-list"></i></span>
																				<span class="pcoded-mtext">LD Claim Exceed</span>
																				<span class="pcoded-mcaret"></span>
																			</a>
																		</li>
																	</ul>	
																</li>		
                            </ul>
                            <div class="pcoded-navigatio-lavel" menu-title-theme="theme5">Reports</div>
                            <ul class="pcoded-item pcoded-left-item">
                                <li class="pcoded-hasmenu">
                                    <a href="javascript:void(0)">
																			<span class="pcoded-micon"><i class="ti-bar-chart-alt"></i></span>
																			<span class="pcoded-mtext">Spark</span>
																			<span class="pcoded-mcaret"></span>
                                    </a>
                                    <ul class="pcoded-submenu">
                                        <li class=" ">
																					<a href="<?php echo base_url(); ?>spark_daily_activity_report">
																						<span class="pcoded-micon"><i class="ti-angle-right"></i></span>
																						<span class="pcoded-mtext">Daily Activity</span>
																						<span class="pcoded-mcaret"></span>
																					</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>                            
                            
                            <div class="pcoded-navigatio-lavel" menu-title-theme="theme5">Attendance</div>
                            <ul class="pcoded-item pcoded-left-item">
                              <li class=" ">
																<a href="<?php echo base_url(); ?>activity/attendance_calendar">
																	<span class="pcoded-micon"><i class="ti-calendar"></i></span>
																	<span class="pcoded-mtext">Attendance Calendar</span>
																	<span class="pcoded-mcaret"></span>
																</a>
															</li>
															<li class="">
																<a href="<?php echo base_url(); ?>activity/attendance_list">
																	<span class="pcoded-micon"><i class="ti-list"></i></span>
																	<span class="pcoded-mtext">Attendance List</span>
																	<span class="pcoded-mcaret"></span>
																</a>
															</li>
															<li class="">
																<a href="<?php echo base_url()?>activity/attendance_regularizations">
																	<span class="pcoded-micon"><i class="ti-list"></i></span>
																	<span class="pcoded-mtext">Attendance Regularizations</span>
																	<span class="pcoded-mcaret"></span>
																</a>
															</li>															
															<li class=" ">
																<a href="<?php echo base_url(); ?>activity/change_password">
																	<span class="pcoded-micon"><i class="ti-key"></i></span>
																	<span class="pcoded-mtext">Change Password</span>
																	<span class="pcoded-mcaret"></span>
																</a>
															</li>
                           </ul>
                           
														<div class="pcoded-navigatio-lavel" menu-title-theme="theme5">My Leave</div>
														<ul class="pcoded-item pcoded-left-item">
															<li class=" ">
																<a href="<?php echo base_url(); ?>activity/my_leave_request">
																	<span class="pcoded-micon"><i class="ti-list"></i></span>
																	<span class="pcoded-mtext">My Leave Request</span>
																	<span class="pcoded-mcaret"></span>
																</a>
															</li>
															<li class=" ">
																<a href="<?php echo base_url(); ?>activity/my_leave_list">
																	<span class="pcoded-micon"><i class="ti-list"></i></span>
																	<span class="pcoded-mtext">My Leave List</span>
																	<span class="pcoded-mcaret"></span>
																</a>
															</li>
														</ul>
                            
                            <div class="pcoded-navigatio-lavel" menu-title-theme="theme5">Activities</div>
                            <ul class="pcoded-item pcoded-left-item">
                                <li class="">
                                    <a href="<?php echo base_url(); ?>activity">
                                        <span class="pcoded-micon"><i class="ti-search"></i></span>
                                        <span class="pcoded-mtext">Find Activities</span>
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                </li>
                            </ul>
                        
