<!DOCTYPE html>
<html lang="en">
<head>
    
    <meta charset="utf-8">
    <title>SF Activity Admin Login</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    

    <!-- The styles -->
    <link id="bs-css" href="<? echo base_url(); ?>assets/admin/css/bootstrap-cerulean.min.css?ver=1.1" rel="stylesheet">

    <link href="<? echo base_url(); ?>assets/admin/css/main-app.css" rel="stylesheet">
    <link href='<? echo base_url(); ?>assets/admin/bower_components/fullcalendar/dist/fullcalendar.css' rel='stylesheet'>
    <link href='<? echo base_url(); ?>assets/admin/bower_components/fullcalendar/dist/fullcalendar.print.css' rel='stylesheet' media='print'>
    <link href='<? echo base_url(); ?>assets/admin/bower_components/chosen/chosen.min.css' rel='stylesheet'>
    <link href='<? echo base_url(); ?>assets/admin/bower_components/colorbox/example3/colorbox.css' rel='stylesheet'>
    <link href='<? echo base_url(); ?>assets/admin/bower_components/responsive-tables/responsive-tables.css' rel='stylesheet'>
    <link href='<? echo base_url(); ?>assets/admin/bower_components/bootstrap-tour/build/css/bootstrap-tour.min.css' rel='stylesheet'>
    <link href='<? echo base_url(); ?>assets/admin/css/jquery.noty.css' rel='stylesheet'>
    <link href='<? echo base_url(); ?>assets/admin/css/noty_theme_default.css' rel='stylesheet'>
    <link href='<? echo base_url(); ?>assets/admin/css/elfinder.min.css' rel='stylesheet'>
    <link href='<? echo base_url(); ?>assets/admin/css/elfinder.theme.css' rel='stylesheet'>
    <link href='<? echo base_url(); ?>assets/admin/css/jquery.iphone.toggle.css' rel='stylesheet'>
    <link href='<? echo base_url(); ?>assets/admin/css/uploadify.css' rel='stylesheet'>
    <link href='<? echo base_url(); ?>assets/admin/css/animate.min.css' rel='stylesheet'>

    <!-- jQuery -->
    <script src="<? echo base_url(); ?>assets/admin/bower_components/jquery/jquery.min.js"></script>
	

    <!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
	<script>
	BASE_URL = '<?=base_url()?>';
	</script>
    <!-- The fav icon -->
    <link rel="shortcut icon" href="<?=base_url()?>assets/admin/img/favicon.ico">

</head>


