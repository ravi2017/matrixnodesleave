<?= $this->load->view('templates/admin/header') ?>
<? 
//if ($current_user['role'] == "admin") { ?>
<?= $this->load->view('templates/admin/left_menu') ?>
<?// } ?>
<?//= $this->load->view('templates/admin/breadcrump') ?>

<div class="row">
    <div class="col-md-12">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-star-empty"></i> <?=(isset($page_title)) ? $page_title : ""?></h2>

                <div class="box-icon" style="display:none;">
                    <a href="#" class="btn btn-setting btn-round btn-default"><i
                            class="glyphicon glyphicon-cog"></i></a>
                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>
                    <a href="#" class="btn btn-close btn-round btn-default"><i
                            class="glyphicon glyphicon-remove"></i></a>
                </div>
            </div>
            <div class="box-content">
              <?php $error = $this->session->flashdata('alert'); ?>
              <?php if (isset($error) and $error != "") { ?>
              <div style="text-align:center;color:red;"> <?= $error ?><br><br></div>
              <?php } ?>
              <?php $success = $this->session->flashdata('success'); ?>
              <?php if (isset($success) and $success != "") { ?>
              <div style="text-align:center;color:green;"> <?= $success ?><br><br></div>
              <?php } ?>
              <?= $this->load->view($partial) ?>
            </div>
        </div>
    </div>
</div><!--/row-->

<?= $this->load->view('templates/admin/footer') ?>
    
