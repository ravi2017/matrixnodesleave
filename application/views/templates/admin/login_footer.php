<script src="<? echo base_url(); ?>assets/admin/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- library for cookie management -->
<script src="<? echo base_url(); ?>assets/admin/js/jquery.cookie.js"></script>
<!-- calender plugin -->
<script src='<? echo base_url(); ?>assets/admin/bower_components/moment/min/moment.min.js'></script>
<script src='<? echo base_url(); ?>assets/admin/bower_components/fullcalendar/dist/fullcalendar.min.js'></script>
<!-- data table plugin -->
<script src='<? echo base_url(); ?>assets/admin/js/jquery.dataTables.min.js'></script>

<!-- select or dropdown enhancer -->
<script src="<? echo base_url(); ?>assets/admin/bower_components/chosen/chosen.jquery.min.js"></script>
<!-- plugin for gallery image view -->
<script src="<? echo base_url(); ?>assets/admin/bower_components/colorbox/jquery.colorbox-min.js"></script>
<!-- notification plugin -->
<script src="<? echo base_url(); ?>assets/admin/js/jquery.noty.js"></script>
<!-- library for making tables responsive -->
<script src="<? echo base_url(); ?>assets/admin/bower_components/responsive-tables/responsive-tables.js"></script>
<!-- tour plugin -->
<script src="<? echo base_url(); ?>assets/admin/bower_components/bootstrap-tour/build/js/bootstrap-tour.min.js"></script>
<!-- star rating plugin -->
<script src="<? echo base_url(); ?>assets/admin/js/jquery.raty.min.js"></script>
<!-- for iOS style toggle switch -->
<script src="<? echo base_url(); ?>assets/admin/js/jquery.iphone.toggle.js"></script>
<!-- autogrowing textarea plugin -->
<script src="<? echo base_url(); ?>assets/admin/js/jquery.autogrow-textarea.js"></script>
<!-- multiple file upload plugin -->
<script src="<? echo base_url(); ?>assets/admin/js/jquery.uploadify-3.1.min.js"></script>
<!-- history.js for cross-browser state change on ajax -->
<script src="<? echo base_url(); ?>assets/admin/js/jquery.history.js"></script>
<script src="<? echo base_url(); ?>assets/admin/js/jquery.uploadify-3.1.min.js"></script>


<script src="<? echo base_url(); ?>assets/admin/js/main.js"></script>

<script type="text/javascript" src="<? echo base_url(); ?>assets/admin/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<? echo base_url(); ?>assets/admin/ckfinder/ckfinder.js"></script>
<script type="text/javascript" src="<? echo base_url(); ?>assets/admin/js/valid.js"></script>

</body>
</html>
