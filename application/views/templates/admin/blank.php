<?= $this->load->view('templates/admin/gallery_header') ?>

<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-star-empty"></i> <?=(isset($page_title)) ? $page_title : ""?></h2>
            </div>
            <div class="box-content">
              <?= $this->load->view($partial) ?>
            </div>
        </div>
    </div>
</div><!--/row-->

    <?= $this->load->view('templates/admin/gallery_footer') ?>
