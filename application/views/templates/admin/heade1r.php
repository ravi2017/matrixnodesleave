<!DOCTYPE html>
<html lang="en">
<head>
    
    <meta charset="utf-8">
    <title>ICSIN</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    

    <!-- The styles -->
    <link id="bs-css" href="<? echo base_url(); ?>assets/admin/css/bootstrap-cerulean.min.css?ver=1.1" rel="stylesheet">

    <link href="<? echo base_url(); ?>assets/admin/css/main-app.css" rel="stylesheet">
    <link href='<? echo base_url(); ?>assets/admin/bower_components/fullcalendar/dist/fullcalendar.css' rel='stylesheet'>
    <link href='<? echo base_url(); ?>assets/admin/bower_components/fullcalendar/dist/fullcalendar.print.css' rel='stylesheet' media='print'>
    <link href='<? echo base_url(); ?>assets/admin/bower_components/chosen/chosen.min.css' rel='stylesheet'>
    <link href='<? echo base_url(); ?>assets/admin/bower_components/colorbox/example3/colorbox.css' rel='stylesheet'>
    <link href='<? echo base_url(); ?>assets/admin/bower_components/responsive-tables/responsive-tables.css' rel='stylesheet'>
    <link href='<? echo base_url(); ?>assets/admin/bower_components/bootstrap-tour/build/css/bootstrap-tour.min.css' rel='stylesheet'>
    <link href='<? echo base_url(); ?>assets/admin/css/jquery.noty.css' rel='stylesheet'>
    <link href='<? echo base_url(); ?>assets/admin/css/noty_theme_default.css' rel='stylesheet'>
    <link href='<? echo base_url(); ?>assets/admin/css/elfinder.min.css' rel='stylesheet'>
    <link href='<? echo base_url(); ?>assets/admin/css/elfinder.theme.css' rel='stylesheet'>
    <link href='<? echo base_url(); ?>assets/admin/css/jquery.iphone.toggle.css' rel='stylesheet'>
    <link href='<? echo base_url(); ?>assets/admin/css/uploadify.css' rel='stylesheet'>
    <link href='<? echo base_url(); ?>assets/admin/css/animate.min.css' rel='stylesheet'>
	    <link href='<? echo base_url(); ?>assets/admin/css/ui.css' rel='stylesheet'>
	
	

	<script src="<? echo base_url(); ?>assets/admin/js/jquery.min.js"></script>

    <!-- jQuery -->
    <script src="<? echo base_url(); ?>assets/admin/bower_components/jquery/jquery.min.js"></script>
	

    <!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
	<script>
	BASE_URL = '<?=base_url()?>';
	</script>
    <!-- The fav icon -->
    <link rel="shortcut icon" href="<?=base_url()?>assets/admin/img/favicon.ico">

</head>

<body>
    <!-- topbar starts -->
    <div class="navbar navbar-default" role="navigation">

        <div class="navbar-inner">
            <button type="button" class="navbar-toggle pull-left animated flip">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?=base_url()?>admin"> <img alt="Admin Logo" src="<?=base_url()?>assets/admin/img/logo20.png" class="hidden-xs"/>
                <span>Admin</span></a>

            <!-- user dropdown starts -->
            <div class="btn-group pull-right">
                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    <i class="glyphicon glyphicon-user"></i><span class="hidden-sm hidden-xs"> admin</span>
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    <li><a href="dashboard/logout">Logout</a></li>
                </ul>
            </div>
            <!-- user dropdown ends -->

            <!-- theme selector starts -->
            <div class="btn-group pull-right theme-container animated tada">
                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    <i class="glyphicon glyphicon-tint"></i><span
                        class="hidden-sm hidden-xs"> Change Theme / Skin</span>
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" id="themes">
                    <li><a data-value="classic" href="#"><i class="whitespace"></i> Classic</a></li>
                    <li><a data-value="cerulean" href="#"><i class="whitespace"></i> Cerulean</a></li>
                    <li><a data-value="cyborg" href="#"><i class="whitespace"></i> Cyborg</a></li>
                    <li><a data-value="simplex" href="#"><i class="whitespace"></i> Simplex</a></li>
                    <li><a data-value="darkly" href="#"><i class="whitespace"></i> Darkly</a></li>
                    <li><a data-value="lumen" href="#"><i class="whitespace"></i> Lumen</a></li>
                    <li><a data-value="slate" href="#"><i class="whitespace"></i> Slate</a></li>
                    <li><a data-value="spacelab" href="#"><i class="whitespace"></i> Spacelab</a></li>
                    <li><a data-value="united" href="#"><i class="whitespace"></i> United</a></li>
                </ul>
            </div>
            <!-- theme selector ends -->

            <ul class="collapse navbar-collapse nav navbar-nav top-menu">
                <li><a href="<?php echo base_url();?>" target="_blank"><i class="glyphicon glyphicon-globe"></i> Visit Site</a></li>
            </ul>

        </div>
    </div>
    <!-- topbar ends -->
