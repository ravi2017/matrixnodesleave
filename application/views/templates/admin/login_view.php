<?= $this->load->view('templates/admin/login_header') ?>
<body>
  <div class="ch-container">
    <div class="row">
      <div class="row">
        <div class="col-md-12 center login-header">
          <img src="<?=base_url()?>/assets/admin/img/logo.png">
          <h2>SF Activity Admin Login</h2>
        </div>
      </div>
      <div class="row">
        <div class="well col-md-5 center login-box">
          <div class="alert alert-info">
            Please login with your Login ID and Password.
          </div>
          <?if(validation_errors()){?>
          <div class="alert alert-danger"><?php echo validation_errors(); ?></div><?}
          ?>
       
          <?php echo form_open('admin/verifyLogin'); ?>
            <fieldset>
              <div class="input-group input-group-lg">
                <span class="input-group-addon"><i class="glyphicon glyphicon-user red"></i></span>
                <input type="text" class="form-control" placeholder="Login ID" name="email">
              </div>
              <div class="clearfix"></div><br>
              <div class="input-group input-group-lg">
                <span class="input-group-addon"><i class="glyphicon glyphicon-lock red"></i></span>
                <input type="password" class="form-control" placeholder="Password" name="password">
              </div>
              <div class="clearfix"></div>
              <p class="center col-md-5">
                <button type="submit" class="btn btn-primary">Login</button>
              </p>
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </div>
<?= $this->load->view('templates/admin/login_footer') ?>
