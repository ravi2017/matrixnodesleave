<div class="ch-container">
    <div class="row">
        
        <!-- left menu starts -->
        <div class="col-sm-2 col-lg-2">
            <div class="sidebar-nav">
                <div class="nav-canvas">
                  <div class="nav-sm nav nav-stacked">

                  </div>
                  <ul class="nav nav-pills nav-stacked main-menu">
                    <!--<li class="nav-header">Masters</li>
                    <li><a class="ajax-link" href="<?=base_url()?>admin/dashboard"><span> Dashboard</span></a>
                    </li>
                    <li><a class="ajax-link" href="<?=base_url()?>admin/states"> <span> States</span></a>
                    </li>
                    <li><a class="ajax-link" href="<?=base_url()?>admin/districts"> <span> Districts</span></a>
                    </li>
                    <li><a class="ajax-link" href="<?=base_url()?>admin/blocks"> <span> Blocks</span></a>
                    </li>
                    <li><a class="ajax-link" href="<?=base_url()?>admin/clusters"> <span> Clusters</span></a>
                    </li>
                    <li><a class="ajax-link" href="<?=base_url()?>admin/schools"> <span> Schools</span></a>
                    </li>
                    <li><a class="ajax-link" href="<?=base_url()?>admin/users"> <span> Users</span></a></li>
                    <li class="nav-header">SSC Modules</li>
                    <li><a class="ajax-link" href="<?=base_url()?>admin/subjects"> <span> Subjects</span></a></li>
                    <li><a class="ajax-link" href="<?=base_url()?>admin/classes"> <span> Classes</span></a></li>
                    <li><a class="ajax-link" href="<?=base_url()?>admin/concepts"> <span> Concepts</span></a></li>
                    <li><a class="ajax-link" href="<?=base_url()?>admin/base_line_test_questions"> <span> Base Line Test Questions</span></a></li>
                    <li><a class="ajax-link" href="<?=base_url()?>admin/end_line_test_questions"> <span> End Line Test Questions</span></a></li>
                    <li><a class="ajax-link" href="<?=base_url()?>admin/performance_parameters"> <span> Performance Parameters</span></a></li>-->
                    <li class="nav-header">Masters</li>
                    <li><a class="ajax-link" href="<?=base_url()?>admin/users"> <span> Admins</span></a></li>
                    <li><a class="ajax-link" href="<?=base_url()?>admin/states"> <span> States</span></a></li>
                    <li><a class="ajax-link" href="<?=base_url()?>admin/schools"> <span> Schools</span></a></li>
                    <li class="nav-header">SF Activity Modules</li>
                    <li><a class="ajax-link" href="<?=base_url()?>admin/sparks"> <span> Sparks</span></a></li>
                    <li><a class="ajax-link" href="<?=base_url()?>admin/holidays"> <span> Holidays</span></a></li>
                    <li><a class="ajax-link" href="<?=base_url()?>admin/sparks/change_state"> <span> Spark Movement</span></a></li>
                    <!--<li><a class="ajax-link" href="<?=base_url()?>admin/calendar"> <span> Calendar</span></a></li>-->
                  </ul>
                </div>
            </div>
        </div>
        <!--/span-->
        <!-- left menu ends -->

        <noscript>
            <div class="alert alert-block col-md-12">
                <h4 class="alert-heading">Warning!</h4>

                <p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a>
                    enabled to use this site.</p>
            </div>
        </noscript>

        <div id="content" class="col-lg-10 col-sm-10">
