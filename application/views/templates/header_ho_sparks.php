													<ul class="pcoded-item pcoded-left-item">
                              <li class="">
                                <a href="<?php echo base_url(); ?>">
                                    <span class="pcoded-micon"><i class="ti-dashboard"></i></span>
                                    <span class="pcoded-mtext">Dashboard</span>
                                    <span class="pcoded-mcaret"></span>
                                </a>
                              </li>
                            </ul>
                            <ul class="pcoded-item pcoded-left-item">
															
															<li class="pcoded-hasmenu">
																<a href="javascript:void(0)">
																	<span class="pcoded-micon"><i class="ti-bar-chart-alt"></i></span>
																	<span class="pcoded-mtext">Claims</span>
																	<span class="pcoded-mcaret"></span>
																</a>
																<ul class="pcoded-submenu">
																	
																	<li class="">
																		<a href="<?php echo base_url().'map/other_claims_list'; ?>">
																			<span class="pcoded-micon"><i class="ti-list"></i></span>
																			<span class="pcoded-mtext">Other Claim</span>
																			<span class="pcoded-mcaret"></span>
																		</a>
																	</li> 
																</ul>	
															</li>																																
                            </ul>

                            <div class="pcoded-navigatio-lavel" menu-title-theme="theme5">Attendance</div>
                            <ul class="pcoded-item pcoded-left-item">
                              <li class=" ">
																<a href="<?php echo base_url(); ?>activity/attendance_calendar">
																	<span class="pcoded-micon"><i class="ti-calendar"></i></span>
																	<span class="pcoded-mtext">Attendance Calendar</span>
																	<span class="pcoded-mcaret"></span>
																</a>
															</li>
															<li class="">
																<a href="<?php echo base_url(); ?>activity/attendance_list">
																	<span class="pcoded-micon"><i class="ti-list"></i></span>
																	<span class="pcoded-mtext">Attendance List</span>
																	<span class="pcoded-mcaret"></span>
																</a>
															</li>
															<li class="">
																<a href="<?php echo base_url()?>activity/attendance_regularizations">
																	<span class="pcoded-micon"><i class="ti-list"></i></span>
																	<span class="pcoded-mtext">Attendance Regularizations</span>
																	<span class="pcoded-mcaret"></span>
																</a>
															</li>															
															<li class=" ">
																<a href="<?php echo base_url(); ?>activity/change_password">
																	<span class="pcoded-micon"><i class="ti-key"></i></span>
																	<span class="pcoded-mtext">Change Password</span>
																	<span class="pcoded-mcaret"></span>
																</a>
															</li>
                           </ul>
                           
                            <div class="pcoded-navigatio-lavel" menu-title-theme="theme5">My Leave</div>
														<ul class="pcoded-item pcoded-left-item">
															<li class=" ">
																<a href="<?php echo base_url(); ?>activity/my_leave_request">
																	<span class="pcoded-micon"><i class="ti-list"></i></span>
																	<span class="pcoded-mtext">My Leave Request</span>
																	<span class="pcoded-mcaret"></span>
																</a>
															</li>
															<li class=" ">
																<a href="<?php echo base_url(); ?>activity/my_leave_list">
																	<span class="pcoded-micon"><i class="ti-list"></i></span>
																	<span class="pcoded-mtext">My Leave List</span>
																	<span class="pcoded-mcaret"></span>
																</a>
															</li>
														</ul>                            
