                                </div>
                            </div>
                            <div id="styleSelector">
                            </div>
                        </div>
                        <!-- Main-body start -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Warning Section Ends -->
    <!-- Required Jquery -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/v2/js/jquery.slimscroll.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/v2/js/modernizr.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/v2/js/css-scrollbars.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/v2/js/classie.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/v2/js/i18next.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/v2/js/i18nextXHRBackend.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/v2/js/i18nextBrowserLanguageDetector.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/v2/js/jquery-i18next.min.js"></script>
    <!-- Custom js
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/v2/js/script.js"></script> -->
        
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/v2/js/chart.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/v2/js/jquery.validate.js"></script>
    
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/v2/js/pcoded.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/v2/js/demo-12.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/v2/js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/v2/js/jquery.mousewheel.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/v2/js/datatables.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/v2/js/select2.full.min.js"></script>
    <!-- Multiselect js -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/v2/js/bootstrap-multiselect.js">
    </script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/v2/js/jquery.multi-select.js"></script>

    <?php if (isset($custom_scripts) and count($custom_scripts) > 0) { ?>
      <?php foreach($custom_scripts as $custom_script) { ?>
        <?php if (substr( $custom_script, 0, 4 ) === "http") { ?>
        <script src="<?php echo $custom_script; ?>"></script>
        <?php } else { ?>
        <script src="<?php echo base_url("assets/v2/js/".$custom_script); ?>"></script>
        <?php } ?>
      <?php } ?>
    <?php } ?>
    <script>
    $("select").select2();
    $("select.popupselect").select2({
        dropdownParent: $('#myModal')
    });
    
    $("select.popupUpload").select2({
        dropdownParent: $('#uploadModal')
    });
    $(".datatable").DataTable({
        "responsive": true,
        "sort": false,
        "pagingType": "simple_numbers",
        "page": true,
        "autoWidth": false,
        "dom": '<"row"<"col-md-6 col-sm-12"l><"col-md-6 col-sm-12"f>>t<"row"<"col-md-6 col-sm-12"i><"col-md-6 col-sm-12"p>>',
        //"dom": '<"row"<"col-md-6 col-sm-12"><"col-md-6 col-sm-12"f>>t<"row"<"col-md-6 col-sm-12"<input type="submit">><"col-md-6 col-sm-12"p>>',
        "select": false,
        "oLanguage": {
            "oPaginate": {
                //"sFirst": "First page", // This is the link to the first page
                //"sPrevious": "«", // This is the link to the previous page
                //"sNext": "»", // This is the link to the next page
                //"sLast": "Last page" // This is the link to the last page
            },
            "sSearch": '',
        },
        columnDefs: [
          { targets: 'no-sort', orderable: false },
          { targets: 'no-search', searchable: false }
        ]
    });    
    
    $('#credittable').DataTable({
			"responsive": true,
			"sort": false,
			"pagingType": "simple_numbers",
			"page": false,
			"paging": false,
			"autoWidth": false,
			"searching": false
		});
		
		$('#withscrolltable').DataTable({
			"responsive": false,
			"sort": false,
			"page": false,
			"paging": false,
			"autoWidth": true,
			"searching": false
		});
		
		$('.withscrolltable').DataTable({
			"responsive": false,
			"sort": false,
			"page": false,
			"paging": false,
			"autoWidth": true,
			"searching": false
		});
		
		
    $('.theme-loader').fadeOut(1000);
    </script>
    <style>
    .select2-container ul li{
    font-size: 15px;
    width: 100%;
    }
    .switch {
  position: relative;
  display: inline-block;
  width: 47px;
  height: 22px;
}

/* Hide default HTML checkbox */
.switch input {display:none;}

/* The slider */
.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 16px;
  width: 16px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 28px;
}

.slider.round:before {
  border-radius: 50%;
}
.scrollme {
    overflow-y: auto;
}
.row_background{
  background-color:#A0A0A0;
}
    </style>
  </body>
</html>
