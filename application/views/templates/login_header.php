<!DOCTYPE html>
<html lang="en">
  <head>
    <title>SF Activity</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="Phoenixcoded">
    <meta name="keywords" content=", Flat ui, Admin , Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app">
    <meta name="author" content="Phoenixcoded">

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/v2/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/v2/icon/themify-icons/themify-icons.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/v2/icon/icofont/css/icofont.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/v2/css/style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/v2/css/color/color-1.css"/>
  </head>

  <body class="fix-menu">
    
    <section class="login p-fixed d-flex text-center bg-primary common-img-bg">
    </section>
    <section class="p-fixed d-flex text-center">
      <div class="container-fluid">
        <div class="row">
          <div class="col-sm-12">
