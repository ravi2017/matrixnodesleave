<?php echo $this->load->view('templates/header','',true) ?>
<?php //echo $this->load->view('templates/left_menu') ?>
<?php //echo $this->load->view('templates/breadcrump') ?>
<?php $success = $this->session->flashdata('success'); ?>
<?php if (isset($success) and $success != "") { ?>
  <div class="alert alert-success">
  <button class="close" data-dismiss="alert" type="button">×</button>
  <?php echo $success?>
  </div>
<?php } ?>
<?php $alert = $this->session->flashdata('alert'); ?>
<?php if (isset($alert) and $alert != "") { ?>
  <div class="alert alert-danger">
  <button class="close" data-dismiss="alert" type="button">×</button>
  <?php echo $alert?>
  </div>
<?php } ?>
<div class="row">
  <div class="col-md-12">
    <div class="card client-blocks">
      <div class="card-block">
        <?php echo $this->load->view($partial,'',true) ?>
      </div>
    </div>
  </div>
</div>
<?php echo $this->load->view('templates/footer','',true) ?>
    
