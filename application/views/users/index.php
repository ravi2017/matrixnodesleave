<script type="text/javascript">
function show_confirm(act,gotoid)
{
if(act=="edit")
var r=confirm("Do you really want to edit?");
else
var r=confirm("Do you really want to delete?");
if (r==true)
{
window.location="<?php echo base_url();?>users/"+act+"/"+gotoid;
}
}
</script>
<a href="<?php echo base_url();?>users/add">Add New User</a><br><br>
<div style="display:none;" class="alert alert-info"></div>
<table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
	<thead>
		<tr>
			<th>ID</th>
			<th>Login ID</th>
			<th>Role</th>
			<th>Actions</th>
		</tr>
	</thead>
	<tbody>
	
	<? $users_count = 0; ?>
	<?php foreach ($users_list as $e_key){ ?>
		<tr>
			<td><?php echo $users_count = $users_count + 1; ?></td>
			<td><?php echo $e_key->login_id; ?></td>
			<td><?php echo $e_key->role; ?></td>
			<td class="center">
				<a class="btn btn-info" href="<?php echo base_url();?>users/edit/<?php echo $e_key->id;?>">
					<i class="glyphicon glyphicon-edit icon-white"></i>
					Edit
				</a>
				<a class="btn btn-danger" href="#" onClick="show_confirm('delete',<?php echo $e_key->id;?>)">
					<i class="glyphicon glyphicon-trash icon-white"></i>
					Delete
				</a>
			</td>
		</tr>
	<?php } ?>
		</tbody>
	</table>
	
  <script>
  
    $('.add-inventory').click(function (e) {
        e.preventDefault();
        var user_id = $(this).attr("user")
        $("#user_id").val(user_id)
        $('#addinventoryModal').modal('show');
    });
    
    $(".saveinventory").click(function(){
      alert('d');
        $("#frmaddinventory").submit();
    })
    
  </script>
	
<div class="modal fade" id="addinventoryModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

  <div class="modal-dialog">
      <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">×</button>
              <h3>Add Inventory</h3>
          </div>
          <form method="post" action="<?php echo base_url();?>users/add_inventory" role="form" name="Formcase_study_group" id="frmaddinventory">
            <div class="modal-body">                      
              <div class="form-group">
                <label for="user_name">Quantity</label><?php echo form_error('quantity'); ?>
                <span class="err" id="err_quantity"></span>
                <input type="text" class="form-control" id="quantity" placeholder="Enter Quantity" name="quantity" value="">
              </div>
              <div class="form-group">
                <input type="hidden" name="id" id="user_id" value="" />
              </div>
            </div>
            <div class="modal-footer">
              <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
              <input type="submit" name="btnsubmit" class="btn btn-primary" value="Save changes">
            </div>
          </form>
      </div>
  </div>
</div>
