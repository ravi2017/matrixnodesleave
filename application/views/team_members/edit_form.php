<?php
if (isset($user))
{
	extract($user);
}
?>
<div class="row">
  <div class="col-md-12">
    <form method="post" action="<?php echo base_url();?>team_members/<?=(isset($action)) ? $action : ''?>" role="form" name="frmuser" onsubmit="return validateuser()" id="frmuser" style="padding: 0 20px 0 20px;">
      <div class="row">
        <div class="form-group col-md-4">
          <label for="user_name">Login ID</label><?php echo form_error('login_id'); ?>
          <span class="err" id="err_login_id"></span>
          <input type="text" class="form-control" id="login_id" readonly placeholder="Enter Login ID" name="login_id" value="<?=(isset($user['login_id'])) ? $user['login_id'] : ''?>">
          <input type="hidden" class="form-control" id="role" value="<?=(isset($user['role'])) ? $user['role'] : ''?>">
        </div>
        <div class="form-group col-md-4">
          <label for="user_name">Name</label><?php echo form_error('name'); ?>
          <span class="err" id="err_name"></span>
          <input type="text" class="form-control"  readonly id="name" placeholder="Enter Name" name="name" value="<?=(isset($user['name'])) ? $user['name'] : ''?>">
        </div>          
        <div class="form-group col-md-4">
          <label for="user_name">Email</label><?php echo form_error('email'); ?>
          <span class="err" id="err_email"></span>
          <input type="text" class="form-control"  readonly id="email" placeholder="Enter Email ID" name="email" value="<?=(isset($user['email'])) ? $user['email'] : ''?>">
        </div>          	
        
      </div>
      <div class="row">
        <div class="form-group col-md-4">
          <label for="title">Select Role</label><span class="err" id="err_role"></span>
          <select class="form-control" id="role" name="role">
            <option value="">Select</option>
            <option value="field_user" <?=(isset($user['role']) and $user['role'] == "field_user") ? 'selected' : ''?>>Field User</option>
            <option value="manager" <?=(isset($user['role']) and $user['role'] == "manager") ? 'selected' : ''?>>Manager</option>
          </select>
        </div>
        <div class="form-group team_member col-md-4">
          <label for="title">Select Manager</label><span class="err" id="err_role"></span>
          <select class="form-control" id="manager" name="manager_id" <?php if($logged_in_user_role == "manager") echo "disabled";?>>
            <option value="">Select</option>
            <?php foreach ($state_persons as $state_person) { ?>
            <option value="<?php echo $state_person->id?>" <?=(isset($user['manager_id']) and $user['manager_id'] == $state_person->id) ? 'selected' : ''?>><?php echo $state_person->name." (".$state_person->login_id.")"?></option>
            <?php } ?>
          </select>
        </div>
        <div class="form-group col-md-4">
          <label for="user_name">Mobile</label><?php echo form_error('mobile'); ?>
          <span class="err" id="err_mobile"></span>
          <input type="text" class="form-control" id="mobile" placeholder="Enter Mobile" name="mobile" value="<?=(isset($user['mobile'])) ? $user['mobile'] : ''?>">
        </div>
      </div>
      
      <div class="form-group team_member">
        <label for="title">Select District</label><span class="err" id="err_title"></span>
        <div class="row" id="district_id" style="padding-left: 20px;"></div>
      </div>
      <div class="form-group block_div" style="display: none;">
        <label for="title">Select Block</label><span class="err" id="err_title"></span>
        <div class="row" id="block" style="padding-left: 20px;">
        </div>
      </div>
      <div class="form-group row">
        <?
        if (isset($user['id']))
        {
        ?>
        <input type="hidden" name="user_id" value="<?php echo $user['id']; ?>" />
        <?
        }
        ?>
        <input type="hidden" value="<?php echo $user_state_id?>" id="user_state_id">
        <div class="col-md-6"><br>
          <a class="btn btn-sm btn-danger f-left" href="<?=base_url()?>team_members/view">Cancel</a>
        </div>
        <div class="col-md-6"><br>
          <button type="submit" class="btn btn-sm btn-success f-right" name="Submit">Submit</button>
        </div>
      </div>
    </form>
  </div>
</div>
<script>

$("#role").change(function(){
    if ($(this).val() == "field_user" || $(this).val() == "manager")
    {
      //$(".team_member").show()
    }
    else
    {
      //$(".team_member").hide()
    }
})

function get_district_list(sid,did)
{
  var manager = $("#manager").val();
  var did = <?php echo $district_ids;?>;
  $.ajax({
    url: '<?=base_url()?>team_members/get_districts',
    type: 'get',
    data: {
      state: '<?php echo $user_state_id; ?>',
      manager: manager,
      curuser: '<?php echo $id; ?>'
    },
    dataType: 'json',
    success: function(response) {
      $.each(response, function() {
        if (did.indexOf(this['id']) != '-1')
          $("#district_id").append('<div class="col-md-4"><input type="checkbox" id="'+ this['id'] +'edit" checked did="' + this['id'] +'" dname="' + this['value'] + '"class="district_check" value="' + this['id'] +'"name="district_id[]" check="checked" checked>' + this['value'] + '</div>');
        else
          $("#district_id").append('<div class="col-md-4"><input type="checkbox" did="' + this['id'] +'" dname="' + this['value'] + '"class="district_check" value="' + this['id'] +'"name="district_id[]">' + this['value'] + '</div>');
      });
      $("#district_id").show();
      for(var i = 0; i < did.length; i++){
        $('.block_div').show()
        get_block_list(did[i], $("#"+did[i]+"edit").attr('dname'));
      }
    },
    error: function(response) {
      window.console.log(response);
    }
  });
}

function get_block_list(did, dname)
{
  if($("#manager").val() == "")
    var manager = '<?php echo $logged_in_user ?>';
  else
    var manager = $("#manager").val();

  var bid = <?php echo $block_ids;?>;
  $.ajax({
    url: '<?=base_url()?>team_members/get_blocks',
    type: 'get',
    data: {
      district: did,
      manager: manager,
      role: '<?php echo $user['role']; ?>',
      user_id: '<?php echo $user['id']; ?>'
    },
    dataType: 'json',
    success: function(response) {
      if (response.length == 0)
      {
        alert("Someone already alloted for all the blocks");
        $("#did"+did).prop("checked",false);
      }else{
        $('#block').append('<div class="row" id="'+did+'" style="padding-bottom:20px;"><div class="col-md-2"><b>'+dname+'</b></div><div class="col-md-10"><input type="checkbox" class="select_all"><b>Select all blocks</b></div>')
        $.each(response, function() {
          if (bid.indexOf(this['id']) != '-1')
            $("#"+did).append('<div class="col-md-4"><input type="checkbox" value="' + did + "_" + this['id'] +'"name="block_id[]" checked>' + this['value'] + '</div>');
          else
            $("#"+did).append('<div class="col-md-4"><input type="checkbox" check="unchecked" value="' + did + "_" + this['id'] +'"name="block_id[]">' + this['value'] + '</div>');
        });
        $('#'+did).append('</div><div class="clearfix"></div>')
        $("#district_id").show();
      }
    },
    error: function(response) {
      window.console.log(response);
    }
  });
}

$(document).on('click', '.district_check', function(){
  $('.block_div').show()
  if ($(this).prop('checked'))
  {
    get_block_list($(this).val(), $(this).attr('dname'))
  }
  else
  {
    $('#'+$(this).attr('did')).remove()
  }
});

 $(document).on('click', '.select_all', function(){
    var id = $(this).parent().parent().attr('id');
    $( '#'+id+' input[type="checkbox"]' ).prop('checked', this.checked)
  })


  //get_district_list($('#user_state_id').val(),'')

  $("#manager").change(function() {
    $('#district_id').empty()
    $('#block').empty()
  get_district_list($('option:selected', this).attr('sid'),'')
});

<? if (isset($state_id) and $state_id > 0) { ?>
  get_district_list(<?=$state_id?>,<?=$district_id?>);
<? } ?>

<? if (isset($role) and $role == "field_user") { ?>
    //$(".team_member").show()
<? } else { ?>
    //$(".team_member").hide()
<? } ?>

function validateuser()
{
  if ($("#login_id").val() == "")
  {
    alert("Please enter login id")
    return false;
  }
  if ($("#password").val() == "")
  {
    alert("Please enter password")
    return false;
  }
  if ($("#role").val() == "")
  {
    alert("Please select role")
    return false;
  }
  if ($("#role").val() == "field_user")
  {
    if ($("#state_id").val() == "")
    {
      alert("Please select state")
      return false;
    }
    /*if ($("#district_id").val() == "")
    {
      alert("Please select district")
      return false;
    }*/
  }
}
</script>
