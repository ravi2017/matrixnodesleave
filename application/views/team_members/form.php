 <?
if (isset($user))
{
	extract($user);
}
?>
<div class="box col-md-8 col-md-offset-2">
  <div class="box-inner">
    <div class="box-header well" data-original-title="">
      <h2><i class="glyphicon glyphicon-th"></i>Create Team Member</h2>
    </div>
    <form method="post" action="<?php echo base_url();?>team_members/<?=(isset($action)) ? $action : ''?>" role="form" name="frmuser" onsubmit="return validateuser()" id="frmuser" style="padding: 0 20px 0 20px;">
      <div class="row">
        <div class="form-group col-md-4">
          <label for="user_name">Login ID</label><?php echo form_error('login_id'); ?>
          <span class="err" id="err_login_id"></span>
          <input type="text" class="form-control" id="login_id" placeholder="Enter Login ID" name="login_id">
        </div>					
        <div class="form-group col-md-4">
          <label for="user_name">Password</label><?php echo form_error('password'); ?>
          <span class="err" id="err_password"></span>
          <input type="password" class="form-control" id="password" placeholder="Enter Password" name="password" value="">
        </div>
        <div class="form-group col-md-4">
          <label for="user_name">Confirm Password</label><?php echo form_error('password'); ?>
          <span class="err" id="err_password"></span>
          <input type="password" class="form-control" id="cpassword" placeholder="Enter Password" name="cpassword" value="">
        </div>
      </div>
      <div class="row">
        <div class="form-group col-md-4">
          <label for="user_name">Name</label><?php echo form_error('name'); ?>
          <span class="err" id="err_name"></span>
          <input type="text" class="form-control" id="name" placeholder="Enter Name" name="name" >
        </div>          
        <div class="form-group col-md-4">
          <label for="user_name">Email</label><?php echo form_error('email'); ?>
          <span class="err" id="err_email"></span>
          <input type="text" class="form-control" id="email" placeholder="Enter Email ID" name="email" value="<?=(isset($email)) ? $email : ''?>">
        </div>          
        <div class="form-group col-md-4">
          <label for="user_name">Mobile</label><?php echo form_error('mobile'); ?>
          <span class="err" id="err_mobile"></span>
          <input type="text" class="form-control" id="mobile" placeholder="Enter Mobile" name="mobile" value="<?=(isset($mobile)) ? $mobile : ''?>">
        </div>
      </div>
      <div class="row">
        <div class="form-group col-md-4">
          <label for="title">Select Role</label><span class="err" id="err_role"></span>
          <select class="form-control" id="role" name="role">
            <option value="field_user" <?=(isset($role) and $role == "field_user") ? 'selected' : ''?>>Field User</option>
            <option value="manager" <?=(isset($role) and $role == "manager") ? 'selected' : ''?>>Manager</option>
          </select>
        </div>
        <div class="form-group team_member col-md-4">
          <label for="title">Select Manager</label><span class="err" id="err_role"></span>
          <select class="form-control" id="manager" name="manager_id">
            <option value="">Select</option>
            <?php foreach ($state_persons as $state_person) { ?>
            <option value="<?php echo $state_person->id?>"><?php echo $state_person->name." (".$state_person->login_id.")"?></option>
            <?php } ?>
          </select>
        </div>
      </div>
      <div class="form-group team_member">
        <label for="title">Select District</label><span class="err" id="err_title"></span>
        <div class="row" id="district_id" style="padding-left: 20px;"></div>
      </div>
      <div class="form-group block_div" style="display: none;">
        <label for="title">Select Block</label><span class="err" id="err_title"></span>
        <div class="row" id="block" style="padding-left: 20px;">
        </div>
      </div>
      <div class="form-group">
        <?
        if (isset($id))
        {
        ?>
        <input type="hidden" name="id" value="<?php echo $id; ?>" />
        <?
        }
        ?>
        <input type="hidden" value="<?php echo $user_state_id?>" id="user_state_id">
        <button type="submit" class="btn btn-default">Submit</button>
        <a href="<?=base_url()?>team_members/view" class="btn btn-primary" style="float: right;">Go back</a>
      </div>
    </form>
  </div>
</div>
<script>

$("#role").change(function(){
    if ($(this).val() == "field_user" || $(this).val() == "manager")
    {
      //$(".team_member").show()
    }
    else
    {
      //$(".team_member").hide()
    }
})

function get_district_list(sid,did)
{
  var manager = $("#manager").val();
  $.ajax({
    url: '<?=base_url()?>team_members/get_districts',
    type: 'get',
    data: {
      state: '<?php echo $user_state_id; ?>',
      manager: manager,
      curuser: '<?php echo $id; ?>'
    },
    dataType: 'json',
    success: function(response) {
      $.each(response, function() {
        if (did == this['id'])
          $("#district_id").append('<div class="col-md-4"><input type="checkbox" did="' + this['id'] +'" id="did' + this['id'] +'" checked dname="' + this['value'] + '"class="district_check" value="' + this['id'] +'"name="district_id[]" checked>' + this['value'] + '</div>');
        else
          $("#district_id").append('<div class="col-md-4"><input type="checkbox" did="' + this['id'] +'" id="did' + this['id'] +'" dname="' + this['value'] + '"class="district_check" value="' + this['id'] +'"name="district_id[]">' + this['value'] + '</div>');
      });
      $("#district_id").show();
    },
    error: function(response) {
      window.console.log(response);
    }
  });
}

function get_block_list(did, dname)
{
  if($("#manager").val() == "")
    var manager = '<?php echo $logged_in_user ?>';
  else
    var manager = $("#manager").val();

  var role = $("#role").val();
  $.ajax({
    url: '<?=base_url()?>team_members/get_blocks',
    type: 'get',
    data: {
      district: did,
      manager: manager,
      role: role
    },
    dataType: 'json',
    success: function(response) {
      if (response.length == 0)
      {
        alert("Someone already alloted for all the blocks");
        $("#did"+did).prop("checked",false);
      }
      else
      {
        $('#block').append('<div class="row" id="'+did+'" style="padding-bottom:20px;"><div class="col-md-2"><b>'+dname+'</b></div><div class="col-md-10"><input type="checkbox" class="select_all"><b>Select all blocks</b></div>')
        $.each(response, function() {
          if (did == this['id'])
            $("#"+did).append('<div class="col-md-4"><input type="checkbox" value="' + did + "_" + this['id'] +'"name="block_id[]" checked>' + this['value'] + '</div>');
          else
            $("#"+did).append('<div class="col-md-4"><input type="checkbox" check="unchecked" value="' + did + "_" + this['id'] +'"name="block_id[]">' + this['value'] + '</div>');
        });
        $('#'+did).append('</div><div class="clearfix"></div>')
        $("#district_id").show();
      }
    },
    error: function(response) {
      window.console.log(response);
    }
  });
}

$(document).on('click', '.district_check', function(){
  $('.block_div').show()
  if ($(this).prop('checked'))
  {
    get_block_list($(this).val(), $(this).attr('dname'))
  }
  else
  {
    $('#'+$(this).attr('did')).remove()
  }
});

 $(document).on('click', '.select_all', function(){
    var id = $(this).parent().parent().attr('id');
    $( '#'+id+' input[type="checkbox"]' ).prop('checked', this.checked)
  })


  get_district_list($('#user_state_id').val(),'')

  $("#manager").change(function() {
    $('#district_id').empty()
    $('#block').empty()
  get_district_list($('option:selected', this).attr('sid'),'')
});

  $("#role").change(function() {
    $('#district_id').empty()
    $('#block').empty()
  get_district_list('','')
});

<? if (isset($state_id) and $state_id > 0) { ?>
  get_district_list(<?=$state_id?>,<?=$district_id?>);
<? } ?>

<? if (isset($role) and $role == "field_user") { ?>
    //$(".team_member").show()
<? } else { ?>
    //$(".team_member").hide()
<? } ?>

function validateuser()
{
  if ($("#login_id").val() == "")
  {
    alert("Please enter login id")
    return false;
  }
  if ($("#password").val() == "")
  {
    alert("Please enter password")
    return false;
  }
  if ($("#role").val() == "")
  {
    alert("Please select role")
    return false;
  }
  if ($("#role").val() == "field_user")
  {
    if ($("#state_id").val() == "")
    {
      alert("Please select state")
      return false;
    }
    /*if ($("#district_id").val() == "")
    {
      alert("Please select district")
      return false;
    }*/
  }
}

$(document).on('input', '#login_id', function(){
  var login_id = $(this).val();
  if(login_id == ''){
    $('#err_login_id').empty();
  }
  $.ajax({
    url: '<?=base_url()?>team_members/check_user_id',
    type: 'post',
    data: "user_id="+login_id,
    success: function(response) {
        $('#err_login_id').empty();
        $('#err_login_id').append(response);
    },
    error: function(response) {
      window.console.log(response);
    }
  });
});
</script>
