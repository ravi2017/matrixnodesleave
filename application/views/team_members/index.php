<div class="row">
    <div class="box col-md-6 col-md-offset-3">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-th"></i> Team members </h2>
            </div>
            <div class="box-content">
                <div class="row">
                    <div class="col-md-offset-4 text-center col-md-4"><a href="<?=base_url()?>team_members/view"><h6>View members</h6></a></div>
                </div>
            </div>
            <div class="box-content">
                <div class="row">
                    <div class="col-md-offset-4 text-center col-md-4"><a href="<?=base_url()?>team_members/add"><h6>Add member</h6></a></div>
                </div>
            </div>
            </div>
        </div>
    </div>
</div>
