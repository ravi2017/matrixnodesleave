<div style="display:none;" class="alert alert-info"></div>
<?php if($current_role == 'state_person'){ ?>
  <!--<a class="btn btn-success" href="<?php echo base_url();?>team_members/add" style="margin-bottom: 10px;">
      <i class="glyphicon glyphicon-add icon-white"></i>
      Add member
  </a>-->
<?php } ?>
<table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
    <thead>
        <tr>
            <th>Name</th>
            <th>Login ID</th>
            <th>Role</th>
            <th>Manager</th>
            <th class="action no-sort no-search">Actions</th>
        </tr>
    </thead>
    <tbody>
    
    <? $users_count = 0; ?>
    <?php foreach ($team_members_list as $e_key){ ?>
        <tr>
            <td><?php echo $e_key->name; ?></td>
            <td><?php echo $e_key->login_id; ?></td>
            <td><?php echo ucwords(str_replace("_", " ", $e_key->role)); ?></td>
            <td><?php echo $e_key->manager; ?></td>
            <td class="action">
              <?php if($current_role == 'state_person' or $current_role == 'manager'){
              if($current_user == $e_key->manager_id){ ?>
                <a class="" href="<?php echo base_url();?>team_members/edit/<?php echo $e_key->id;?>">
                    <i class="icon-pencil icon-white"></i>
                </a>
              <?php }else{ ?>
                <a class="change_manager" href="javascript:void(0)" id="<?php echo $e_key->id;?>">
                    <i class="icon-pencil icon-white"></i>
                </a>
              <?php } } ?>
              <?php 
              $this->load->model('team_members_model');
              $team_check = $this->team_members_model->check_team($e_key->id);
              if($team_check > 0){ ?>
              <a class="text-success" target="_blank" href="<?php echo base_url()?>team_members/view_team/<?php echo $e_key->id?>">
                <i class="icon-eye icon-white"></i>
              </a> 
              <?php } ?>  
              <a class="text-info" target="_blank" href="<?php echo base_url()?>map/index/?spark_id=<?php echo $e_key->id?>-<?php echo $e_key->state_id?>">
                <i class="icon-map icon-white"></i>
              </a> 
            </td>
        </tr>
    <?php } ?>
        </tbody>
</table>
</div>

<script>

  $('.change_manager').click(function (e) {
      e.preventDefault();
      var id = this.id;
      var manager_id;
      $.ajax({
        type:'POST',
        url:'<?php echo base_url()?>team_members/get_managers',
        data: 'id='+id,
        success: function(response) {
            var prePopulate = $.parseJSON(response);
            $("#manager_list").empty();
            jQuery.each(prePopulate, function(index, item) {
              $("#manager_list").append($('<option></option>').val(item.id).html(item.name));
            });
        }
      });
      $('#spark_id').val(id);
      $('#changemanagermodal').modal('show');
  });
  
</script>
  
<div class="modal fade" id="changemanagermodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">×</button>
            <h3>Change Manager</h3>
        </div>
        <form method="post" action="<?php echo base_url();?>team_members/change_manager" role="form">
          <div class="modal-body">                      
            <div class="form-group">
              <label for="user_name">Manager</label>
              <span class="err" id="err_quantity"></span>
              <select id="manager_list" name="manager_id" class="form-control">
              </select>
              <input type="hidden" id="spark_id" name="spark_id">
            </div>
          </div>
          <div class="modal-footer">
            <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
            <input type="submit" name="btnsubmit" class="btn btn-primary" value="Save changes">
          </div>
        </form>
    </div>
  </div>
</div>

