<?
	$sdate = (!empty($activitydatestart) ? date('Y-m-d', strtotime($activitydatestart)) : '');
	$edate = (!empty($activitydateend) ? date('Y-m-d', strtotime($activitydateend)) : '');
?>
<div class="row">
	<input type="hidden" name="spark_start" id="spark_start" value="<?=$sdate?>">
	<input type="hidden" name="spark_end" id="spark_end" value="<?=$edate?>">
<?php if($current_role == "state_person" or $current_role == "manager") { ?>
  <?php $sparks = get_my_team($current_user_id); ?>
  <div class="form-group col-md-6">
    <select class="form-control" id="spark_id" name="spark_id">
      <option value="">Select Spark11</option>
      <option value="<?php echo $current_user_id."-".$current_name."-".$user_state_id?>" <?php echo (isset($spark_id) and $current_user_id == $spark_id) ? "selected" : ""?>><?php echo $current_name." (".$current_username.")"?></option>
      <?php foreach ($sparks as $spark) { ?>
      <option value="<?php echo $spark->id."-".$spark->name."-".$spark->state_id?>" <?php echo (isset($spark_id) and $spark->id == $spark_id) ? "selected" : ""?>><?php echo $spark->name." (".$spark->login_id.")"?></option>
      <?php } ?>
    </select>
  </div>
<?php } elseif($current_role == "super_admin" or $current_role == "admin" or $current_role == "accounts" or $current_role == "reports" or $current_role == "HR") { ?>
  <?php $states = get_states(); ?>
  <div class="form-group col-md-6">
    <select class="form-control" id="spark_state_id" name="spark_state_id">
      <option value="">Select State</option>
      <?php foreach($states as $state) { ?>
      <option value="<?=$state->id?>" <?php echo (isset($spark_state_id) and $state->id == $spark_state_id) ? "selected" : ""?>><?php echo $state->name?></option>
      <?php } ?>
      <option value="HO" <?php echo (isset($spark_state_id) and $spark_state_id == 'HO') ? "selected" : ""?>>HO</option>
    </select>
  </div>
  <div class="form-group col-md-6">
    <select class="form-control" id="spark_id" name="spark_id">
      <option value="">Select Spark</option>
      <?php //if($spark_state_id != ''){	
					$sparks = getCurrentSparkFromState($current_role, $sdate, $edate, $spark_state_id, $current_group, $current_user_id);										
				?>
				<?php foreach ($sparks as $sparkd) { 
							$spark = (object) $sparkd; 
							?><option value="<?php echo $spark->id."-".$spark->name."-".$spark->state_id?>" <?php echo (isset($spark_id) and $spark->id == $spark_id) ? "selected" : ""?>><?php echo $spark->name." (".$spark->login_id.")"?></option>
				<?php } ?>
      <?php //} ?>
    </select>
  </div>
<?php } ?>
</div>
<script>
$("#spark_state_id").change(function() {
  get_all_user_list($(this).val())
});

<?php if ($spark_state_id > 0 && ($current_role == "state_person" or $current_role == "manager")) { ?>
  get_all_user_list('<?php echo $spark_state_id; ?>','<?php echo $spark_id; ?>')
<?php } ?>


$("#activitydatestart").change(function(){
	var sdate = $("#activitydatestart").val();
	var edate = $("#activitydateend").val();
	$("#spark_start").val(sdate);
	$("#spark_end").val(edate);
	reset_values();
});

$("#activitydateend").change(function(){
	var sdate = $("#activitydatestart").val();
	var edate = $("#activitydateend").val();
	$("#spark_start").val(sdate);
	$("#spark_end").val(edate);
	reset_values();
});	
	

function get_all_user_list(ssid,sid = 0)
{
	var sdate = '';
	var edate = '';
	if($('#spark_start').val()){
		var sdate = $('#spark_start').val();
	}
	if($('#spark_end').val()){
		var edate = $('#spark_end').val();
	}
	//alert(ssid+' >> '+sdate+' >> '+edate);
	$.ajax({
    url: '<?=base_url()?>dashboard/getCurrentSparkFromState',
    type: 'get',
    data: {
      state: ssid,
      sdate: sdate,
      edate: edate
    },
    dataType: 'json',
    success: function(response) {
      $("#spark_id").html('');
      $("#spark_id").html("<option value=''>Select Spark</option>");
      $.each(response, function() {
        if (sid > 0 && sid == this['id'])
          $("#spark_id").append("<option selected value='" + this['id']+"-"+ this['name'] +"-" +this['state_id']+"'>" + this['name'] + "</option>");
        else
          $("#spark_id").append("<option value='" + this['id']+"-"+ this['name'] +"-" +this['state_id']+"'>" + this['name'] + "</option>");
      });
      $("#spark_id").show();
      //$("#spark_id").val('').trigger('change');
    },
    error: function(response) {
      window.console.log(response);
    }
  });
}

function reset_values()
{
	<?php if($current_role == "state_person" or $current_role == "manager") { ?>
		get_all_user_list('<?php echo $spark_state_id; ?>','<?php echo $spark_id; ?>');
	<?php }else if($current_role != 'field_user'){	?>
		$("#spark_state_id").val('').trigger('change');
		$("#spark_id").html('');
    $("#spark_id").html("<option value=''>Select Spark</option>");
	<?php	}	?>	
}
</script>
