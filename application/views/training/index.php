<div class="row">
    <div class="box col-md-6 col-md-offset-3">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-th"></i> Select Training - <?=strftime("%d-%b-%Y",strtotime($activity_date))?></h2>
            </div>
            
            <div class="box-content">
                <div class="row">
                    <div class="col-md-offset-4 text-center col-md-4"><a href="<?=base_url()?>training/external"><h6>External Training</h6></a></div>
                </div>
            </div>
            <div class="box-content">
                <div class="row">
                    <div class="col-md-offset-4 text-center col-md-4"><a href="<?=base_url()?>training/internal"><h6>Internal Training</h6></a></div>
                </div>
            </div>
            <div class="box-content">
                <div class="row">
                    <div class="col-md-offset-4 text-center col-md-4"><a href="<?=base_url()?>select_activity"><h6>Back</h6></a></div>
                </div>
            </div>
        </div>
    </div>
</div>
