<? if(!isset($download) && !isset($pdf)) { ?>
<div class="form-group row">
  <div class="form-group col-md-3">
  <form name="frmreport" action="<?php echo base_url();?>training/training_logistic" method="post">
      <input type="hidden" name="state_id" value="<?=$state_id?>">
      <input type="hidden" name="district_id" value="<?=$district_id?>">
      <input type="hidden" name="block_id" value="<?=$block_id?>">
      <input type="hidden" name="start_month" value="<?=$start_month?>">
      <input type="hidden" name="start_year" value="<?=$start_year?>">
      <input type="hidden" name="duration" value="<?=$duration?>">
      <input type="hidden" name="download" value="1">
      <button type="submit" class="btn btn-success">Download Report</button>
  </form>
  </div>
  <div class="form-group col-md-3">
  <form name="frmreport" action="<?php echo base_url();?>training/training_logistic" method="post">
      <input type="hidden" name="state_id" value="<?=$state_id?>">
      <input type="hidden" name="district_id" value="<?=$district_id?>">
      <input type="hidden" name="block_id" value="<?=$block_id?>">
      <input type="hidden" name="start_month" value="<?=$start_month?>">
      <input type="hidden" name="start_year" value="<?=$start_year?>">
      <input type="hidden" name="duration" value="<?=$duration?>">
      <input type="hidden" name="pdf" value="1">
      <button type="submit" class="btn btn-success">Download PDF</button>
  </form>
  </div>
  <div class="col-md-6" align="right"><a href="<?=base_url()?>training/training_logistic" style="float:right">Back</a></div>
</div>
<? }else{ ?>
<table border="1" cellspacing=0 cellpadding=2 width="100%">  
  <tr><td colspan="<?php echo 4+count($observations);?>" align="center"><h2>Training Logistic Report</h2></td></tr>
</table>
<?  } ?>  

<? if(isset($pdf) || isset($download)) { ?>
  <table border="1" cellspacing=0 cellpadding=2 width="100%">  
<?php }else{ ?>
  <table class="table table-striped table-bordered bootstrap-datatable datatable1 responsive">
<?php } ?>  
    <tr>
      <td>&nbsp;</td>
      <td colspan="<?php echo 3+count($observations);?>"><strong>MONTH : </strong><?php echo $month_name. ($duration == 'YTD' ? " (".$duration.")" : ''); ?></td>
      <!--<td colspan="<?php echo 3+count($observations);?>"><strong>Duration : </strong><?php echo date('d-m-Y', strtotime($startdate))." To ".date('d-m-Y', strtotime($enddate)); ?></td>-->
    </tr>  
</table>


<div class="scrollme">
<? if(isset($pdf)) { ?>
  <br />
  <table border="1" cellspacing=0 cellpadding=2 width="100%">  
<?php }else{ ?>
  <table class="table table-striped table-bordered bootstrap-datatable datatable1 responsive">
<?php } ?>  
		<tr>
			<th><?=$column_name; ?></th>
      <th>Subject</th>
      <th>Teacher Trained</th>
      <?php foreach($observations as $observation) { ?>
        <th><?php echo $observation['question_sort']; ?></th>
      <?  } ?>
      <th>Spark Name</th>
		</tr>
   
    <?php $k=1;
    $total_1 = 0;
    
    if(!empty($type_values)){
    foreach($type_values as $key=>$value) { 
      foreach($subjects as $subject_id=>$subject_name) { 
      
      $spark_names = array();
      $sparks = $user_arr[$key];
      foreach($sparks as $spark)
      {
        $spark_names[] = $user_names[$spark];
      }
      
      ?>
      <tr>
        <td><?=$value;?></td>
        <td><?php echo $subject_name; ?></td>
        <td><?php echo (!empty($traineddata[$key][$subject_id]) ? $traineddata[$key][$subject_id] : '-'); ?></th>
        <?php foreach($observations as $ques_id=>$observation) { 
          
            $ques_val = 0;
            $quesval = '-';
            if(isset($observation_data[$key][$subject_id][$ques_id])){
              $ques_val = $observation_data[$key][$subject_id][$ques_id];
              
              if($observation['question_type'] == 'subjective')
              { 
                $quesval1 = (round($ques_val/count($observation_arr[$key][$subject_id][$ques_id]),2));
              }
              else{
                $quesval1 = (round($ques_val/count($observation_arr[$key][$subject_id][$ques_id]),2))*100;  
              }
              $quesval = ($quesval1 > 0 ? $quesval1."%" : '-'); 
            }
          ?>
          <td align="right" vl="<?=$ques_val;?>"><?php echo $quesval; ?></td>
        <?  } ?> 
        <? if(isset($pdf) || isset($download)) { ?>
        <td><?php echo implode(', ', $spark_names); ?></td>
        <?  }else{ ?>
          <td><?php echo implode(', ', $spark_names); ?></td>
        <?  } ?>
      </tr>
    <?php $k++; } }?>
    <?  }else{ ?>
      <tr>
        <? if(isset($download)) { ?>
        <td colspan="<?php echo 4+count($observations);?>" align="cnter">No Record Found.</td>
        <? }else{ ?>
        <td colspan="<?php echo 4+count($observations);?>" style="text-align:center">No Record Found.</td>  
        <? } ?>  
      </tr>
    <?  } ?>  
		<tr>
      <? if(isset($download)) { ?>
			<td colspan="<?php echo 4+count($observations);?>" align="right">Report Generated On: <?=date("d-m-Y H:i");?></td>
      <? }else{ ?>
      <td colspan="<?php echo 4+count($observations);?>" style="text-align:right">Report Generated On: <?=date("d-m-Y H:i");?></td>  
      <? } ?>  
		</tr>
	</thead>	
</table>
</div>
<table>
    <tr>
      <td colspan="<?php echo 4+count($observations);?>">
      <?php foreach($observations as $observation) { ?>
        <?php echo $observation['question_sort']." : ".str_replace(':','',$observation['question']); ?><br />
      <?  } ?>
      </td>
    </tr>
</table>
