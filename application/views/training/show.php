<div class="row">
  <div class="form-group col-md-8 col-md-offset-2">
    <label for="title"><br>Activity : </label> <?=ucwords($detail['training_type'])?> Training<br>
    <label for="title"><br>State : </label> <?=ucwords($state['name'])?><br>
    <label for="title"><br>District : </label> <?=ucwords($district['name'])?><br>
    <label for="title"><br>Block : </label> <?=ucwords($block['name'])?><br>
    <label for="title"><br>Cluster : </label> <?=ucwords($cluster['name'])?><br>
    <label for="title"><br>No Of Days : </label> <?=ucwords($detail['no_of_days'])?><br>
    <label for="title"><br>No Of Teachers Trained : </label> <?=ucwords($detail['no_of_teachers_trained'])?><br>
    <label for="title"><br>Subject Covered : </label> <?=ucwords($subject['name'])?><br>
  </div>
</div>
