 <div class="box col-md-12">
        <div class="box-inner">
<form name="frmreport" action="" method="post" onsubmit="return validatereport()">
    <div class="row">
      <? if($current_role != 'field_user') { ?>
      <div class="form-group col-md-4 field_user">
        <select class="form-control" id="state_id" name="state_id">
          <option value="">Select State</option>
          <? foreach($states as $state) { ?>
          <option value="<?=$state->id?>" <?=(isset($state_id) and $state->id == $state_id) ? "selected" : ""?>><?=$state->name?></option>
          <? } ?>
        </select>
      </div>
      <div class="form-group col-md-4 field_user">
        <select class="form-control" id="district_id" name="district_id">
          <option value="">Select District</option>
        </select>
      </div>
      <?  }else{ ?>
      <div class="form-group col-md-4 field_user">
        <input type="hidden" name="state_id" id="state_id" value="<?=$user_state_id?>">
        <select class="form-control" id="district_id" name="district_id">
          <option value="">Select District</option>
          <? foreach($districts as $key=>$name) { ?>
            <option value="<?=$key;?>"><?=$name;?></option>
          <?  } ?>
        </select>
      </div>
      <?  } ?>
      <div class="form-group col-md-4 field_user">
        <select class="form-control" id="duration" name="duration">
          <option value="monthly">Monthly</option>
          <option value="YTD">YTD</option>
        </select>
      </div>
      <!--<div class="form-group col-md-4 field_user">
        <select class="form-control" id="block_id" name="block_id">
          <option value="">Select Block</option>
        </select>
      </div>
      <div class="form-group col-md-3 field_user">
        <select class="form-control" id="cluster_id" name="cluster_id">
          <option value="">Select Cluster</option>
        </select>
      </div>-->
    </div> 
    
    <div class="row">
      <div class="form-group col-md-4 field_user" id="226">
        <select class="form-control" id="start_month" name="start_month">
          <option value="">Month</option>
          <? foreach($month_arr as $key=>$value) { ?>
            <option value="<?=$key?>"><?=$value?></option>
          <? } ?>
        </select>
      </div>
       <div class="form-group col-md-4 field_user">
        <select class="form-control" id="start_year" name="start_year">
            <option value="">Year</option>
            <? foreach($year_arr as $year) { ?>
                <option value="<?=$year?>"><?=$year?></option>
            <? } ?>
        </select>
       </div>
    <div class="form-group col-md-4 text-center">
      <button type="submit" class="btn btn-sm btn-success">Get Report</button>
    </div>
  </div>
</form>
</div></div>
<script>
  
function get_district_list(sid,did)
{
  $.ajax({
    url: '<?=base_url()?>dashboard/get_districts',
    type: 'get',
    data: {
      state: sid
    },
    dataType: 'json',
    success: function(response) {
      $("#district_id").html("<option value=''>Select District</option>");
      $.each(response, function() {
        if (did == this['id'])
          $("#district_id").append("<option value='" + this['id'] + "' selected>" + this['value'] + "</option>");
        else
          $("#district_id").append("<option value='" + this['id'] + "'>" + this['value'] + "</option>");
      });
      $("#district_id").show();
    },
    error: function(response) {
      window.console.log(response);
    }
  });
}

function get_block_list(did,bid)
{
  $.ajax({
    url: '<?=base_url()?>dashboard/get_blocks',
    type: 'get',
    data: {
      district: did
    },
    dataType: 'json',
    success: function(response) {
      $("#block_id").html("<option value=''>Select Block</option>");
      $.each(response, function() {
        if (bid == this['id'])
          $("#block_id").append("<option value='" + this['id'] + "' selected>" + this['value'] + " (Total Schools - " +  this['school_count'] + ")</option>");
        else
          $("#block_id").append("<option value='" + this['id'] + "'>" + this['value'] + " (Total Schools - " +  this['school_count'] + ")</option>");
      });
      $("#block_id").show();
    },
    error: function(response) {
      window.console.log(response);
    }
  });
}

function get_cluster_list(bid,cid)
{
  $.ajax({
    url: '<?=base_url()?>dashboard/get_clusters',
    type: 'get',
    data: {
      block: bid
    },
    dataType: 'json',
    success: function(response) {
      $("#cluster_id").html("<option value=''>Select Cluster</option>");
      $.each(response, function() {
        if (cid == this['id'])
          $("#cluster_id").append("<option value='" + this['id'] + "' selected>" + this['value'] + " (Total Schools - " +  this['school_count'] + ")</option>");
        else
          $("#cluster_id").append("<option value='" + this['id'] + "'>" + this['value'] + " (Total Schools - " +  this['school_count'] + ")</option>");
      });
      $("#cluster_id").show();
    },
    error: function(response) {
      window.console.log(response);
    }
  });
}

$("#state_id").change(function() {
  get_district_list($(this).val(),'')
});

$("#district_id").change(function() {
  //get_block_list($(this).val(),'')
});

$("#block_id").change(function() {
  //get_cluster_list($(this).val(),'')
});

<? if (isset($state_id) and $state_id > 0) { ?>
  get_district_list(<?=$state_id?>,<?=$district_id?>);
<? } ?>
<? if (isset($district_id) and $district_id > 0) { ?>
  get_block_list(<?=$district_id?>,<?=$block_id?>);
<? } ?>

function validatereport()
{
  if ($("#state_id").val() == "")
  {
    alert("Please select state")
    return false;
  }
  if ($("#start_month").val() == "")
  {
    alert("Please select month")
    return false;
  }
  if ($("#start_year").val() == "")
  {
    alert("Please select year")
    return false;
  }
  return true
}
   
</script>
