<div class="row">
    <div class="box col-md-6 col-md-offset-3">
        <div class="box-inner">
          <form method="post" action="<?php echo base_url();?>training/internal_submit" role="form" name="frminternaltraining">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-th"></i> Enter Detail of Internal Training</h2>
            </div>
            
            <div class="row">
              <div class="form-group col-md-10 col-md-offset-1">
                <label for="title"><br>Training Date : <?=strftime("%d-%b-%Y",strtotime($activity_date))?></label>
              </div>
            </div>
            
            <div class="row">
              <div class="form-group col-md-10 col-md-offset-1">
                <label for="title">Role</label><span class="err" id="err_title"></span>
                <select class="form-control" id="role" name="role">
                  <option value="">Select Role</option>
                  <option value="trainer">Trainer</option>
                  <option value="trainee">Trainee</option>
                </select>
              </div>
            </div>
            
            <div class="row">
              <div class="form-group col-md-10 col-md-offset-1">
                <label for="title">Location</label><span class="err" id="err_title"></span>
                <select class="form-control" id="location" name="location">
                  <option value="">Select Location</option>
                  <option value="HO">HO</option>
                  <option value="state">State</option>
                  <option value="district">District</option>
                  <option value="block">Block</option>
                  <option value="cluster">Cluster</option>
                </select>
              </div>
            </div>
                        
            <div class="row selectcluster selectplace selectblock selectdistrict selectstate">
              <div class="form-group col-md-10 col-md-offset-1">
                <label for="title">Select State</label><span class="err" id="err_title"></span>
                <select class="form-control" id="state_id" name="state_id">
                  <option value="">Select State</option>
                  <? foreach($states as $state) { ?>
                  <option value="<?=$state->id?>" <?=(isset($state_id) and $state->id == $state_id) ? "selected" : ""?>><?=$state->name?></option>
                  <? } ?>
                </select>
              </div>
            </div>
            
            <div class="row selectcluster selectplace selectblock selectdistrict">
              <div class="form-group col-md-10 col-md-offset-1">
                <label for="title">Select District</label><span class="err" id="err_title"></span>
                <select class="form-control" id="district_id" name="district_id">
                  <option>Select District</option>
                </select>
              </div>
            </div>
            
            <div class="row selectcluster selectplace selectblock">
              <div class="form-group col-md-10 col-md-offset-1">
                <label for="title">Select Block</label><span class="err" id="err_title"></span>
                <select class="form-control" id="block_id" name="block_id">
                  <option>Select Block</option>
                </select>
              </div>
            </div>
            
            <div class="row selectcluster selectplace">
              <div class="form-group col-md-10 col-md-offset-1">
                <label for="title">Select Cluster</label><span class="err" id="err_title"></span>
                <select class="form-control" id="cluster_id" name="cluster_id">
                  <option>Select Cluster</option>
                </select>
              </div>
            </div>
            
            <div class="row">
              <div class="form-group col-md-10 col-md-offset-1 training_dates">
                <label for="title">Training Date</label><a href="javascript:void(0)" id="adddate" class="btn btn-info">+</a><span class="err" id="err_title"></span>
                <input readonly type="text" name="training_date[]" value="<?=strftime("%d-%b-%Y",strtotime($activity_date))?>" class="form-control"> 
              </div>
            </div>
            
            <div class="row">
              <div class="form-group col-md-10 col-md-offset-1">
                <label for="title">No Of Teachers Trained</label><span class="err" id="err_title"></span>
                <input type="text" name="no_of_teachers_trained" value="" class="form-control" id="no_of_teachers_trained">
              </div>
            </div>
            
            <div class="row">
              <div class="form-group col-md-10 col-md-offset-1">
                <label for="title">Subject Covered</label><span class="err" id="err_title"></span>
                <select class="form-control" id="subject_id" name="subject_id">
                  <option value="0">Select Subject</option>
                  <? foreach($subjects as $subject){ ?>
                  <option value="<?=$subject->id?>"><?=$subject->name?></option>
                  <? } ?>
                </select>
              </div>
            </div>

            <div class="row">
              <div class="form-group col-md-10 col-md-offset-1">
                <label for="title">Topic</label><span class="err" id="err_title"></span>
                <input type="text" name="topic" id="topic" value="" class="form-control">
              </div>
            </div>
            
            <div class="row">
              <div class="form-group col-md-10 col-md-offset-1">
                <label for="title">Agenda</label><span class="err" id="err_title"></span>
                <textarea name="agenda" id="agenda" rows="3" cols="20" class="form-control"></textarea>
              </div>
            </div>
            
            <div class="row">
              <div class="form-group col-md-10 col-md-offset-1 text-center">
                <a href="<?=base_url()?>training">Cancel</a>
                <button type="submit" class="btn btn-default">Submit</button>
              </div>
            </div>
            
          </form>
        </div>
    </div>
</div>

<script type="text/javascript">
$('[id^="select"]').hide();
$(".selectplace").hide()

  
$("#location").change(function() {
  var name = $(this).val();
  $(".selectplace").hide()
  $(".select"+name).show()
});

function get_district_list(sid,did)
{
  $.ajax({
    url: '<?=base_url()?>dashboard/get_districts',
    type: 'get',
    data: {
      state: sid
    },
    dataType: 'json',
    success: function(response) {
      $("#district_id").html("<option value=''>Select District</option>");
      $.each(response, function() {
        if (did == this['id'])
          $("#district_id").append("<option value='" + this['id'] + "' selected>" + this['value'] + " </option>");
        else
          $("#district_id").append("<option value='" + this['id'] + "'>" + this['value'] + " </option>");
      });
      $("#district_id").show();
    },
    error: function(response) {
      window.console.log(response);
    }
  });
}

function get_block_list(did,bid)
{
  $.ajax({
    url: '<?=base_url()?>dashboard/get_blocks',
    type: 'get',
    data: {
      district: did
    },
    dataType: 'json',
    success: function(response) {
      $("#block_id").html("<option value=''>Select Block</option>");
      $.each(response, function() {
        if (bid == this['id'])
          $("#block_id").append("<option value='" + this['id'] + "' selected>" + this['value'] + " </option>");
        else
          $("#block_id").append("<option value='" + this['id'] + "'>" + this['value'] + " </option>");
      });
      $("#block_id").show();
    },
    error: function(response) {
      window.console.log(response);
    }
  });
}

function get_cluster_list(bid,cid)
{
  $.ajax({
    url: '<?=base_url()?>dashboard/get_clusters',
    type: 'get',
    data: {
      block: bid
    },
    dataType: 'json',
    success: function(response) {
      $("#cluster_id").html("<option value=''>Select Cluster</option>");
      $.each(response, function() {
        if (cid == this['id'])
          $("#cluster_id").append("<option value='" + this['id'] + "' selected>" + this['value'] + " </option>");
        else
          $("#cluster_id").append("<option value='" + this['id'] + "'>" + this['value'] + " </option>");
      });
      $("#cluster_id").show();
    },
    error: function(response) {
      window.console.log(response);
    }
  });
}

$("#state_id").change(function() {
  get_district_list($(this).val(),'')
});

$("#district_id").change(function() {
  get_block_list($(this).val(),'')
});

$("#block_id").change(function() {
  get_cluster_list($(this).val(),'')
});

$("select").val("")

$(".training_dates").on('click','#adddate',function(){
  $(".training_dates").append('<div><input readonly type="text" name="training_date[]" value="" class="form-control training_date" style="width:94%;float:left;"><a href="javascript:void(0)" class="removedate btn btn-info">-</a></div>')
  

  $('.training_date').datepicker({
    dateFormat: 'd-M-yy',
    beforeShowDay: noSunday
  });
  function noSunday(date){ var day = date.getDay(); return [(day > 0), '']; };
});

$(".training_dates").on('click','.removedate',function(){
  $(this).parent().remove()
});
</script>
