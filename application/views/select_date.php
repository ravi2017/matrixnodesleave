
<style>
h6{background: #d8e9f0 none repeat scroll 0% 0%;
    border-radius: 15px;
    border: 1px solid #b6cad1;
    color: #4c4c4c;
    font-size: 15px;
    font-weight: 600;
    text-align: center;
    padding: 5px 17px;
    margin-bottom: 5px;
    margin-top: 5px;}
</style>
<div class="row">
    <div class="box col-md-6 col-md-offset-3">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-th"></i> Select Activity</h2>
            </div>
            
            <div class="box-content">
                <div class="row">
                    <div class="col-md-offset-4 text-center col-md-4"><a href="<?=base_url()?>school_visit"><h6>School Visit</h6></a></div>
                </div>
            </div>
            <div class="box-content">
                <div class="row">
                    <div class="col-md-offset-4 text-center col-md-4"><a href="<?=base_url()?>training"><h6>Training</h6></a></div>
                </div>
            </div>
            <div class="box-content">
                <div class="row">
                    <div class="col-md-offset-4 text-center col-md-4"><a href="<?=base_url()?>meetings"><h6>Meetings</h6></a></div>
                </div>
            </div>
            <div class="box-content">
                <div class="row">
                    <div class="col-md-offset-4 text-center col-md-4"><a href="<?=base_url()?>holidays"><h6>Holidays</h6></a></div>
                </div>
            </div>
            <div class="box-content">
                <div class="row">
                    <div class="col-md-offset-4 text-center col-md-4"><a href="#"><h6>Others</h6></a></div>
                </div>
            </div>
        </div>
    </div>
</div>
