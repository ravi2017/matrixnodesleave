<div class="loader-att" style="display:none;">Loading&#8230;</div>
<div class="row">
	<div class="form-group">
	<?
	  if(!empty($attendanceData) && $attendanceData['activity_type'] == 'attendance-in'){	
	    ?><button id="pushDontpush" class="btn btn-info">Attendance OUT</button><?
	    echo" (<strong>Attendance-In: </strong>".date('d-m-Y H:i:s', strtotime($attendanceData['tracking_time'])).")";
	  }
	  else{
		 ?><button id="pushDontpush" class="btn btn-default">Attendance IN</button><? 
	  } 
	?>
	<p id="demo"></p>
	</div>
</div>


<script>
$("#pushDontpush").click(function() { 
    if ($(this).text() == "Attendance IN") { 
		getLocationON();
    } else { 
        getLocationOFF();
    }; 
});

var x = document.getElementById("demo");

function getLocationON() {
  if (navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(showPositionON, error);
  } else { 
    x.innerHTML = "Geolocation is not supported by this browser.";
  }
}

function error(err) {
  //console.warn(`ERROR(${err.code}): ${err.message}`);
  switch(error.code) {
    case error.PERMISSION_DENIED:
      //x.innerHTML = "User denied the request for Geolocation."
      console.warn("User denied the request for Geolocation.");
      break;
    case error.POSITION_UNAVAILABLE:
      console.warn("Location information is unavailable.");
      break;
    case error.TIMEOUT:
      console.warn("The request to get user location timed out.");
      break;
    case error.UNKNOWN_ERROR:
      console.warn("An unknown error occurred.");
      break;
  }
}

function getLocationOFF() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(showPositionOFF, error);
  } else { 
    x.innerHTML = "Geolocation is not supported by this browser.";
  }
}

function showPositionON(position) {
	
	var latitude_val = position.coords.latitude;
	var longitude_val = position.coords.longitude;
	//$("#latitude").val(latitude_val)	
	//$("#longitude").val(longitude_val)	
	$(".loader-att").show();
	$.ajax({
      url: '<?=base_url()?>dashboard/mark_attendance',
      type: 'post',
      data: {
        activity_type: 'attendance-in',
        vlatitude : latitude_val,
        vlongitude : longitude_val
      },
      success: function(response) {
		$("#pushDontpush").text("Attendance OUT"); 
		$("#pushDontpush").removeClass('btn-default');
		$("#pushDontpush").addClass('btn-info');
		$(".loader-att").hide();
      },
      error: function(response) {
        window.console.log(response);
      }
    });
	//x.innerHTML = "Latitude: " + position.coords.latitude + "<br>Longitude: " + position.coords.longitude;
}

function showPositionOFF(position) {
	
	var latitude_val = position.coords.latitude;
	var longitude_val = position.coords.longitude;
	//$("#latitude_off").val(latitude_val)	
	//$("#longitude_off").val(longitude_val)	
	$(".loader-att").show();
	$.ajax({
      url: '<?=base_url()?>dashboard/mark_attendance',
      type: 'post',
      data: {
        activity_type: 'attendance-out',
        vlatitude : latitude_val,
        vlongitude : longitude_val
      },
      success: function(response) {
        $("#pushDontpush").text("Attendance IN"); 
		$("#pushDontpush").removeClass('btn-info');
		$("#pushDontpush").addClass('btn-default');
		$(".loader-att").hide();
      },
      error: function(response) {
        window.console.log(response);
      }
    });
	
}
</script>	
<style>
/* Absolute Center Spinner */
.loader-att {
  position: fixed;
  z-index: 999;
  height: 2em;
  width: 2em;
  overflow: visible;
  margin: auto;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
}

/* Transparent Overlay */
.loader-att:before {
  content: '';
  display: block;
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background-color: rgba(0,0,0,0.3);
}

/* :not(:required) hides these rules from IE9 and below */
.loader-att:not(:required) {
  /* hide "loader-att..." text */
  font: 0/0 a;
  color: transparent;
  text-shadow: none;
  background-color: transparent;
  border: 0;
}

.loader-att:not(:required):after {
  content: '';
  display: block;
  font-size: 10px;
  width: 1em;
  height: 1em;
  margin-top: -0.5em;
  -webkit-animation: spinner 1500ms infinite linear;
  -moz-animation: spinner 1500ms infinite linear;
  -ms-animation: spinner 1500ms infinite linear;
  -o-animation: spinner 1500ms infinite linear;
  animation: spinner 1500ms infinite linear;
  border-radius: 0.5em;
  -webkit-box-shadow: rgba(0, 0, 0, 0.75) 1.5em 0 0 0, rgba(0, 0, 0, 0.75) 1.1em 1.1em 0 0, rgba(0, 0, 0, 0.75) 0 1.5em 0 0, rgba(0, 0, 0, 0.75) -1.1em 1.1em 0 0, rgba(0, 0, 0, 0.5) -1.5em 0 0 0, rgba(0, 0, 0, 0.5) -1.1em -1.1em 0 0, rgba(0, 0, 0, 0.75) 0 -1.5em 0 0, rgba(0, 0, 0, 0.75) 1.1em -1.1em 0 0;
  box-shadow: rgba(0, 0, 0, 0.75) 1.5em 0 0 0, rgba(0, 0, 0, 0.75) 1.1em 1.1em 0 0, rgba(0, 0, 0, 0.75) 0 1.5em 0 0, rgba(0, 0, 0, 0.75) -1.1em 1.1em 0 0, rgba(0, 0, 0, 0.75) -1.5em 0 0 0, rgba(0, 0, 0, 0.75) -1.1em -1.1em 0 0, rgba(0, 0, 0, 0.75) 0 -1.5em 0 0, rgba(0, 0, 0, 0.75) 1.1em -1.1em 0 0;
}

/* Animation */

@-webkit-keyframes spinner {
  0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
@-moz-keyframes spinner {
  0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
@-o-keyframes spinner {
  0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
@keyframes spinner {
  0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
</style>	
	
