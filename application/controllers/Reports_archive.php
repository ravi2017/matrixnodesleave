<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Reports_archive extends CI_Controller {

	function __construct()	
	{
		parent::__construct();
		$this->load->helper('url');
		$this->data['current_page'] = 'training';
		$this->load->model('trainings_model');
		$this->load->model('Users_model');
		$this->load->model('states_model');
		$this->load->library('form_validation');
		$this->load->library('Common_functions');
		$this->load->helper('date');
		if($this->session->userdata('logged_in'))
		{
		  $session_data = $this->session->userdata('logged_in');
		  $this->data['current_user_id'] = $session_data['id'];
		  $this->data['current_username'] = $session_data['username'];
		  $this->data['current_name'] = $session_data['name'];
		  $this->data['current_role'] = $session_data['role'];
		  $this->data['user_state_id'] = $session_data['state_id'];
		  $this->data['user_district_id'] = $session_data['district_id'];  
      
      $this->data['spark_id'] = $session_data['id'];
      $this->data['spark_state_id'] = $session_data['state_id'];
			$this->data['current_group'] = $session_data['user_group'];
		  /*if($session_data['role'] == 'field_user'){
			$this->session->set_flashdata('alert', "You are not authorized to access this section");
			redirect('dashboard', 'refresh');
		  }*/
		}
		else
		{
		  //If no session, redirect to login page
		  redirect('login', 'refresh');
		}
    $this->data['month_arr'] = unserialize(MONTH_ARRAY);
    $this->data['year_arr'] = unserialize(YEAR_ARRAY);
	}
  
	function index()
	{
		$this->data['partial'] = 'reports_archive/index';
		$this->data['page_title'] = 'Reports';
		$this->load->view('templates/index', $this->data);    
	}
    
  function state()
  {
    if($this->data['current_role']=="field_user")
		{
			$district_id=$this->data['user_district_id'];
			$user_id=$this->data['current_user_id'];
			$state_id=$this->data['user_state_id'];
		}
		if($this->data['current_role']=="state_person")
		{
			$state_id=$this->data['user_state_id'];
			if(isset($_GET['user_id']) and $_GET['user_id'] >0 )
			{
				$user_id=$_GET['user_id'];
				
			}
			if(isset($_GET['district_id']) and $_GET['district_id'] >0 )
			{
				$district_id=$_GET['district_id'];
			}else{
					$district_id=0;
			}
		}
		
    
			$month = 0;
			$year = 0;
      $this->data['year'] = $month;
      $this->data['month'] = $year;
      $this->data['partial'] = 'reports_archive/state';
		
    if($this->data['current_role']=="state_person" || $this->data['current_role']=="manager"){
      $states = $this->states_model->get_by_id($this->data['user_state_id']);
      $this->data['states'] = $states;
      
      $this->load->model('districts_model');
      $districts = $this->districts_model->get_state_districts($this->data['user_state_id']);
      $this->data['districts'] = $districts;
    }
    else{
				
				$states = $this->states_model->get_all();
				$this->data['states'] = $states;
    }
    $this->data['title'] = 'State Productivity Report';
    $this->data['partial'] = 'reports_archive/state';
    $this->load->view('templates/index', $this->data);
  }
  
  function stateProductivityReport()
  {
    
    if(!isset($_POST['monthno']) && !isset($_POST['year']))
    {
        redirect('reports_archive/state');
    }
    $state_id = $this->input->post('state_id');
    $inpmonth = $this->input->post('monthno');
    $inpyear = $this->input->post('year');
    $inpdate			 = '01/'.$inpmonth.'/'.$inpyear;
    $inpdate 		 = date('t/m/Y', strtotime($inpyear.'-'.$inpmonth.'-01'));

    $this->load->model('states_model');
    $this->load->model('No_visit_days_model');
    $this->load->model('school_visits_model');
    $this->load->model('Meetings_model');
    $this->load->model('Leaves_model');
    $this->load->model('Trainings_model');
    $this->load->model('Users_model');
    $this->load->model('Holidays_model');
    $this->load->model('districts_model');
    $this->load->model('Claim_model');
    $this->load->model('Sparks_model');

    $state_data = $this->states_model->getById($state_id);

    $start_date = date('Y-m-d', strtotime($inpyear.'-'.$inpmonth.'-01'));
    $start_date1 = date('Y-m-d', strtotime($inpyear.'-'.($inpmonth-2).'-01'));
    $end_date = date('Y-m-t', strtotime($inpyear.'-'.$inpmonth.'-01'));
    if(strtotime($end_date) > now()){
      $end_date = date('Y-m-d');
    }

    $users_data = $this->Users_model->getSparkByStateDuration($state_id, $start_date1, $end_date);
    //print_r($users_data);

    $current_month = (Int) date("m", strtotime(date("Y-m-d")));
    $current_year = (Int) date("Y", strtotime(date("Y-m-d")));
    $month_name = "";
    $prev_month2_name   = "";
    $prev_month1_name   = "";
    $cm_no 		= 0;
    $pv2_no 	= 0;
    $pv1_no 	= 0;
    $current_year2 = 0;
    $current_year1 = 0;

    switch($inpmonth){

    case "1":
      $month_name = date('F', mktime(0, 0, 0, $inpmonth, 10));
      $cm_no      = $inpmonth;
      $prev_month2_name   = date('F', mktime(0, 0, 0, 12, 10));
      $pv2_no = 12;
      $current_year2 = $inpyear - 1;
      $prev_month1_name   = date('F', mktime(0, 0, 0, 12 - $inpmonth, 10));
      $pv1_no = 12 - $inpmonth;
      $current_year1 = $inpyear - 1;
      break;
    case "2":
      $month_name = date('F', mktime(0, 0, 0, $inpmonth, 10));
      $cm_no      = $inpmonth;
      $prev_month2_name   = date('F', mktime(0, 0, 0, $inpmonth-1, 10));
      $pv2_no = $inpmonth-1;
      $current_year2 = $inpyear;
      $prev_month1_name   = date('F', mktime(0, 0, 0, 12, 10));
      $pv1_no = 12;
      $current_year1 = $inpyear - 1;
      break;
    default:
      $month_name   = date('F', mktime(0, 0, 0, $inpmonth, 10));
      $cm_no      = $inpmonth;
      $prev_month2_name   = date('F', mktime(0, 0, 0, $inpmonth-1, 10));
      $pv2_no = $inpmonth-1;
      $current_year2 = $inpyear;
      $prev_month1_name   = date('F', mktime(0, 0, 0, $inpmonth-2, 10));
      $pv1_no = $inpmonth-2;
      $current_year1 = $inpyear;
    }
    
    $session_start_year = $inpyear;
    if($inpmonth < SESSION_START_MONTH)
    {
      $session_start_year = $inpyear-1;
    }

    $_fromDate1 = date("Y-m-d",strtotime($current_year1."-".$pv1_no."-01"));
    $_toDate1 = date("Y-m-t",strtotime($current_year1."-".$pv1_no."-01"));
    $productive_days_1 = date("t",strtotime($current_year1."-".$pv1_no."-01"));
    //echo"<br>".$_fromDate1." >> ".$_toDate1." >> ".$productive_days_1;
    
    $_fromDate2 = date("Y-m-d",strtotime($current_year2."-".$pv2_no."-01"));
    $_toDate2 = date("Y-m-t",strtotime($current_year2."-".$pv2_no."-01"));
    $productive_days_2 = date("t",strtotime($current_year2."-".$pv2_no."-01"));
    
    $_fromDate_C = date("Y-m-d",strtotime($inpyear."-".$inpmonth."-01"));
    $_toDate_C = date("Y-m-t",strtotime($inpyear."-".$inpmonth."-01"));
    $productive_days_3 = date("t",strtotime($inpyear."-".$inpmonth."-01"));
    if(strtotime($_toDate_C) > now()){
      $_toDate_C = date("Y-m-d");
      $productive_days_3 = date('d');
    }
    
    $_fromDate_yt = date("Y-m-d 00:00:00",strtotime($session_start_year."-".SESSION_START_MONTH."-01"));
    $_fromDate_ytd = new DateTime($_fromDate_yt);
    $_toDate_yt = date("Y-m-t 23:59:59",strtotime($inpyear."-".$inpmonth."-01"));
    $_toDate_ytd = new DateTime($_toDate_yt);
    $interval = $_fromDate_ytd->diff($_toDate_ytd);

    $i = 0;
    $user_ids = array();
    $user_names = array();
    $from_date = array();
    $to_date = array();
    $productive_days_yt = array();
    $months_list = array();
    foreach($users_data as $usersdata){
        $userid = $usersdata['user_id'];
        $user_ids[] = $userid;
        $user_names[$userid]['id'] = $usersdata['user_id'];
        $user_names[$userid]['name'] = $usersdata['name'];
        $user_names[$userid]['login_id'] = $usersdata['login_id'];
        $user_names[$userid]['district'] = ""; //$user_districts[0]->name;
        $user_names[$userid]['district_ids'] = array(); //$user_districts[0]->name;
        $i++;

        $u_start_date = $usersdata['start_date'];
        $u_end_date = $usersdata['end_date'];

        $working_period = array();
        if(!empty($u_end_date) && strtotime($u_end_date) < strtotime($_toDate_yt)){
          $to_date1 = $u_end_date;
          $working_period[] = "<strong>Last Date:</strong>$u_end_date";
        }
        else{
          $to_date1 =   date('Y-m-d', strtotime($_toDate_yt));
        }
        if($u_start_date > $_fromDate_yt){
          $from_date1 = $u_start_date;
          $working_period[] = "<strong>Start Date:</strong>$u_start_date";
        }
        else{
          $from_date1 = date('Y-m-d', strtotime($_fromDate_yt));
        }
        if(!empty($working_period))
          $user_names[$userid]['working_period'] = "(".implode(', ',$working_period).")";
        $from_date[$userid] = date('Y-m-d', strtotime($from_date1));
        $to_date[$userid]  = $to_date1;
        $days_diff = (strtotime($to_date1)- strtotime($from_date1))/(24*60*60)+1;
        $months_values = $this->return_months($from_date1, $to_date1);
        $months_list[$userid] = $months_values;
        $productive_days_yt[$userid] = $days_diff;
    }
    //print_r($months_list);
    //echo "<br>".implode(', ', $user_ids);
    
    $visitproductivity_1 = array();    $visitproductivity_2 = array();    $visitproductivity_3 = array();    $visitproductivity_yt = array();
    $productivedays_1 = array();    $productivedays_2 = array();    $productivedays_3 = array();    $productivedays_yt = array();
    $school_visit_days_1 = array();    $school_visit_days_2 = array();    $school_visit_days_3 = array();    $school_visit_days_yt = array();
    
    $visit_1 = 0;    $visit_2 =0;    $visit_3 = 0;    $visit_ytd = 0;
    $avg_visit_prod_month_1 = 0;    $avg_visit_prod_month_2 = 0;    $avg_visit_prod_month_3 = 0;    $avg_visit_prod_month_yt = 0;
    $school_visit_month_1 = 0;    $school_visit_month_2 = 0;    $school_visit_month_3 = 0;    $school_visit_month_yt = 0;

    $count_users_1 = 0;
    $count_users_2 = 0;
    $count_users_3 = 0;
    
    foreach($user_ids as $login_id){

      $user_blocks = $this->Users_model->user_blocks($login_id);
      $user_block_ids = array();
      foreach($user_blocks as $user_block)
      {
        array_push($user_block_ids,$user_block->id);
      }
      //print_r($user_block_ids);
      $user_district_ids = array();
      //$user_districts = $this->Users_model->get_worked_districts($login_id, $state_id, $start_date1, $end_date);
      $user_districts = $this->Sparks_model->get_member_district_ids($login_id);
      foreach($user_districts as $user_district)
      {
        //array_push($user_district_ids,$user_district['district_id']);
        array_push($user_district_ids,$user_district->district_id);
      }
      $remove_val = array('0');
      $user_district_ids = array_diff($user_district_ids, $remove_val);
      if (count($user_district_ids) > 0)
      {
        $district_names = $this->districts_model->check_district_name_by_State($user_district_ids,$state_id);
        $user_names[$login_id]['district'] = $district_names[0]['districts'];
      }
      
      // Array to store distinct activity dates including state holidays & SAT-SUN offdays
      $activity_dates = array(array(),array(),array(),array()); 
      $internal_meeting_dates = array(array(),array(),array(),array()); 
    
      $refresher_trainings = array(0,0,0,0,0);
      $refresher_trainings = array(0,0,0,0,0);
      $annual_trainings = array(0,0,0,0,0);
        
      $no_visit_days = array(0,0,0,0);
      $auth_dec_holidays = array(0,0,0,0);
      $leaves = array(0,0,0,0,0);
      $home_district_trainings = array(0,0,0,0);
      $outstation_trainings = array(0,0,0,0);
      $district_level_meetings = array(0,0,0,0);
      $block_level_meetings = array(0,0,0,0);
      $internal_meetings = array(0,0,0,0);
      
      //Fetch user group
			$sparkData = $this->Users_model->getById($login_id);
			$current_group = $sparkData['user_group'];
	  
	  /*************************************************************************/	
	  if(in_array(date('Y-m',strtotime($_fromDate1)), $months_list[$login_id])){	
      //echo"<br>cond 1 : ".$login_id." >> ".$_fromDate1." >> ".$_toDate1;
      
      if(strtotime($from_date[$login_id]) > strtotime($_fromDate1)){
        $_fromDate1 = $from_date[$login_id];
      } 
      if(strtotime($to_date[$login_id]) < strtotime($_toDate1)){
        $_toDate1 = $to_date[$login_id];
      } 
      $productive_days_1 = $days_diff = (strtotime($_toDate1)- strtotime($_fromDate1))/(24*60*60)+1;
      
      //echo"<br>cond 1 : ".$login_id." >> ".$_fromDate1." >> ".$_toDate1;
      
		  $no_visit_1 = $this->No_visit_days_model->get_all($login_id, $_fromDate1, $_toDate1);
		  foreach($no_visit_1 as $no_visit)
		  {
			if ($no_visit->no_visit_reason == 'Local Authority Declared Holiday')
			  $auth_dec_holidays[0] = $auth_dec_holidays[0] + 1;
			else
			  $no_visit_days[0] = $no_visit_days[0] + 1;
		  }
		  
      $state_holiday_list_1 = $this->Holidays_model->get_state_holidays_list($state_id, $_fromDate1, $_toDate1, $current_group);
      foreach($state_holiday_list_1 as $stateholiday)
      {
        if(!in_array(date('Y-m-d', strtotime($stateholiday->activity_date)), $activity_dates[0])){
          $activity_dates[0][] = date('Y-m-d', strtotime($stateholiday->activity_date));
        }
      }
      $sun_2_3_list_sat_1 = $this->Holidays_model->get_2_3_saturday_list($_fromDate1, $_toDate1, $current_group);
      foreach($sun_2_3_list_sat_1 as $holiday)
      {
        if(!in_array(date('Y-m-d', strtotime($holiday->activity_date)), $activity_dates[0])){
          $activity_dates[0][] = date('Y-m-d', strtotime($holiday->activity_date));
        }
      }
      
      $leaves_arr_1 = $this->Leaves_model->get_all_leave($login_id, $_fromDate1, $_toDate1);
      $leaves_data_1 = $this->common_functions->return_month_leave_distinct($leaves_arr_1, $_fromDate1, $_toDate1, $state_holiday_list_1, $sun_2_3_list_sat_1);
      $leaves[0] = count($leaves_data_1);
		  
		  $trainings_1 = $this->Trainings_model->get_all($login_id, $_fromDate1, $_toDate1);
		  foreach($trainings_1 as $training)
		  {
        if(!in_array(date('Y-m-d', strtotime($training->activity_date)), $activity_dates[0])){
          $activity_dates[0][] = date('Y-m-d', strtotime($training->activity_date));
        }

        //if ($training->block_id > 0 and in_array($training->block_id,$user_block_ids))
        if ($training->mode == 'internal')
          $home_district_trainings[0] = $home_district_trainings[0] + 1;
        else
          $outstation_trainings[0] = $outstation_trainings[0] + 1;
        
        if ($training->training_type == 'refresher')
          $refresher_trainings[0] = $refresher_trainings[0] + 1;
        else if ($training->training_type == 'annual')
          $annual_trainings[0] = $annual_trainings[0] + 1;    
		  }
		  // MEETINGS STARTED
		  $meetings_1 = $this->Meetings_model->get_all($login_id, $_fromDate1, $_toDate1);
		  foreach($meetings_1 as $meeting)
		  {
        if(!in_array(date('Y-m-d', strtotime($meeting->activity_date)), $activity_dates[0])){
          $activity_dates[0][] = date('Y-m-d', strtotime($meeting->activity_date));
        }
          if ($meeting->block_id == 0 and $meeting->is_internal == false)
          $district_level_meetings[0] = $district_level_meetings[0] + 1;
          else if ($meeting->block_id > 0 and $meeting->is_internal == false)
          $block_level_meetings[0] = $block_level_meetings[0] + 1;
          
        if ($meeting->is_internal == true){
          $internal_meetings[0] = $internal_meetings[0] + 1;
          if(!in_array(date('Y-m-d', strtotime($meeting->activity_date)), $activity_dates[0]) && !in_array(date('Y-m-d', strtotime($meeting->activity_date)), $internal_meeting_dates[0])){
            $internal_meeting_dates[0][] = date('Y-m-d', strtotime($meeting->activity_date));
          }
        }
        
		  }
		  
      $school_visit_count_1 = 0;
      $school_visits_1 = $this->school_visits_model->get_all($login_id, $_fromDate1, $_toDate1);
      foreach($school_visits_1 as $school_visit)
      {
        $school_visit_count_1++;
        if(!in_array(date('Y-m-d', strtotime($school_visit->activity_date)), $activity_dates[0])){
          $activity_dates[0][] = date('Y-m-d', strtotime($school_visit->activity_date));
        }
      }
      
      $LD_activities_date_1 = array();
      $LD_activities_1 = $this->Claim_model->get_distince_ld_activities($login_id, $_fromDate1, $_toDate1);
      foreach($LD_activities_1 as $LD_activitie)
      {
        if(!in_array($LD_activitie->dat, $activity_dates[0])){
          $LD_activities_date_1[] = $LD_activitie->dat;
        }
      }
      $distinct_LD_count_1 = count($LD_activities_date_1);
      
		  $state_holiday_1 = count($state_holiday_list_1);
		  $sun_2_3_sat_1 = count($sun_2_3_list_sat_1);
		  //$he_visit_count_1 = $this->school_visits_model->get_HE_visit_count($login_id, $state_id, $_fromDate1, $_toDate1);
		  //$le_visit_count_1 = $this->school_visits_model->get_LE_visit_count($login_id, $state_id, $_fromDate1, $_toDate1);

		  $visitproductivity_1[$login_id] = round((($school_visit_count_1) + ($refresher_trainings[0] * 2) + ($annual_trainings[0] * 2) + ($district_level_meetings[0] + $block_level_meetings[0])) / ($productive_days_1 - ($state_holiday_1 + $leaves[0] + $sun_2_3_sat_1 + $auth_dec_holidays[0]+ count($internal_meeting_dates[0]) + $distinct_LD_count_1)),1);
		  $avg_visit_prod_month_1 = $avg_visit_prod_month_1 + $visitproductivity_1[$login_id];

		  //$school_visit_days_1[$login_id] = $he_visit_count_1+$le_visit_count_1;
		  $school_visit_days_1[$login_id] = $school_visit_count_1;
		  $school_visit_month_1 = $school_visit_month_1 + $school_visit_days_1[$login_id];
		  
		  $count_users_1++;
	  }
	  else{
		  $visitproductivity_1[$login_id] = 0;
		  $school_visit_days_1[$login_id] = 0;
	  }
      /*************************************************************************/
    if(in_array(date('Y-m',strtotime($_fromDate2)), $months_list[$login_id])){			
      //echo"<br>cond 2 : ".$login_id." >> ".$_fromDate2." >> ".$_toDate2;
      
      if(strtotime($from_date[$login_id]) > strtotime($_fromDate2)){
        $_fromDate2 = $from_date[$login_id];
      } 
      if(strtotime($to_date[$login_id]) < strtotime($_toDate2)){
        $_toDate2 = $to_date[$login_id];
      } 
      $productive_days_2 = $days_diff = (strtotime($_toDate2)- strtotime($_fromDate2))/(24*60*60)+1;
      
      //echo"<br>cond 2 : ".$login_id." >> ".$_fromDate2." >> ".$_toDate2;
      
      
		  $no_visit_2 = $this->No_visit_days_model->get_all($login_id, $_fromDate2, $_toDate2);
		  foreach($no_visit_2 as $no_visit)
		  {
        if ($no_visit->no_visit_reason == 'Local Authority Declared Holiday')
          $auth_dec_holidays[1] = $auth_dec_holidays[1] + 1;
        else
          $no_visit_days[1] = $no_visit_days[1] + 1;
		  }
      
      $state_holiday_list_2 = $this->Holidays_model->get_state_holidays_list($state_id, $_fromDate2, $_toDate2, $current_group);
      foreach($state_holiday_list_2 as $stateholiday)
      {
        if(!in_array(date('Y-m-d', strtotime($stateholiday->activity_date)), $activity_dates[1])){
          $activity_dates[1][] = date('Y-m-d', strtotime($stateholiday->activity_date));
        }
      }
      $sun_2_3_list_sat_2 = $this->Holidays_model->get_2_3_saturday_list($_fromDate2, $_toDate2, $current_group);
      foreach($sun_2_3_list_sat_2 as $holiday)
      {
        if(!in_array(date('Y-m-d', strtotime($holiday->activity_date)), $activity_dates[1])){
          $activity_dates[1][] = date('Y-m-d', strtotime($holiday->activity_date));
        }
      }
        
      $leaves_arr_2 = $this->Leaves_model->get_all_leave($login_id, $_fromDate2, $_toDate2);
      $leaves_data_2 = $this->common_functions->return_month_leave_distinct($leaves_arr_2, $_fromDate2, $_toDate2, $state_holiday_list_2, $sun_2_3_list_sat_2);
      $leaves[1] = count($leaves_data_2);

		  $trainings_2 = $this->Trainings_model->get_all($login_id, $_fromDate2, $_toDate2);
		  foreach($trainings_2 as $training)
		  {
        if(!in_array(date('Y-m-d', strtotime($training->activity_date)), $activity_dates[1])){
          $activity_dates[1][] = date('Y-m-d', strtotime($training->activity_date));
        }
        if ($training->mode == 'internal')
          $home_district_trainings[1] = $home_district_trainings[1] + 1;
        else
          $outstation_trainings[1] = $outstation_trainings[1] + 1;
        
        if ($training->training_type == 'refresher')
          $refresher_trainings[1] = $refresher_trainings[1] + 1;
        else if ($training->training_type == 'annual')
          $annual_trainings[1] = $annual_trainings[1] + 1;      
		  }
		  // MEETINGS STARTED
		  $meetings_2 = $this->Meetings_model->get_all($login_id, $_fromDate2, $_toDate2);
		  foreach($meetings_2 as $meeting)
		  {
        if(!in_array(date('Y-m-d', strtotime($meeting->activity_date)), $activity_dates[1])){
          $activity_dates[1][] = date('Y-m-d', strtotime($meeting->activity_date));
        }
        
        if ($meeting->block_id == 0 and $meeting->is_internal == false)
          $district_level_meetings[1] = $district_level_meetings[1] + 1;
        else if ($meeting->block_id > 0 and $meeting->is_internal == false)
          $block_level_meetings[1] = $block_level_meetings[1] + 1;
          
        if ($meeting->is_internal == true){
          $internal_meetings[1] = $internal_meetings[1] + 1;
          if(!in_array(date('Y-m-d', strtotime($meeting->activity_date)), $activity_dates[1]) && !in_array(date('Y-m-d', strtotime($meeting->activity_date)), $internal_meeting_dates[1])){
            $internal_meeting_dates[1][] = date('Y-m-d', strtotime($meeting->activity_date));
          }
        }  
        
		  }
      
      $school_visit_count_2 = 0;
      $school_visits_2 = $this->school_visits_model->get_all($login_id, $_fromDate2, $_toDate2);
      foreach($school_visits_2 as $school_visit)
      {
        $school_visit_count_2++;
        if(!in_array(date('Y-m-d', strtotime($school_visit->activity_date)), $activity_dates[1])){
          $activity_dates[1][] = date('Y-m-d', strtotime($school_visit->activity_date));
        }
      }
      
      $LD_activities_date_2 = array();
      $LD_activities_2 = $this->Claim_model->get_distince_ld_activities($login_id, $_fromDate2, $_toDate2);
      foreach($LD_activities_2 as $LD_activitie)
      {
        if(!in_array($LD_activitie->dat, $activity_dates[1])){
          $LD_activities_date_2[] = $LD_activitie->dat;
        }
      }
      $distinct_LD_count_2 = count($LD_activities_date_2);
      
      $state_holiday_2 = count($state_holiday_list_2);
		  $sun_2_3_sat_2 = count($sun_2_3_list_sat_2);
      //$he_visit_count_2 = $this->school_visits_model->get_HE_visit_count($login_id, $state_id, $_fromDate2, $_toDate2);
		  //$le_visit_count_2 = $this->school_visits_model->get_LE_visit_count($login_id, $state_id, $_fromDate2, $_toDate2);
		  
      //echo"<br>$login_id 1>> ".$he_visit_count_2." 2>> ".$le_visit_count_2."  3>> ".$refresher_trainings[1]."  4>> ".$annual_trainings[1]."  5>> ".$district_level_meetings[1]."  6>>  ".$block_level_meetings[1]."  7>>  ".$productive_days_2."  8>>  ".$state_holiday_2."  9>>  ".$leaves[1]."  10>>  ".$sun_2_3_sat_2."  11>> ".$auth_dec_holidays[1];
      $visitproductivity_2[$login_id] = round((($school_visit_count_2) + ($refresher_trainings[1] * 2) + ($annual_trainings[1] * 2) + ($district_level_meetings[1] + $block_level_meetings[1])) / ($productive_days_2 - ($state_holiday_2 + $leaves[1] + $sun_2_3_sat_2 + $auth_dec_holidays[1]+ count($internal_meeting_dates[1]) + $distinct_LD_count_2)),1);
		  $avg_visit_prod_month_2 = $avg_visit_prod_month_2 + $visitproductivity_2[$login_id];
		  $school_visit_days_2[$login_id] = $school_visit_count_2;
		  $school_visit_month_2 = $school_visit_month_2 + $school_visit_days_2[$login_id];
		  
		  $count_users_2++;
	  }
	  else{
		  $visitproductivity_2[$login_id] = 0;
		  $school_visit_days_2[$login_id] = 0;
	  }	
      
      /*************************************************************************/	
    if(in_array(date('Y-m',strtotime($_fromDate_C)), $months_list[$login_id])){			
      //echo"<br>cond 3 : ".$login_id." >> ".$_fromDate_C." >> ".$_toDate_C;
      
      if(strtotime($from_date[$login_id]) > strtotime($_fromDate_C)){
        $_fromDate_C = $from_date[$login_id];
      } 
      if(strtotime($to_date[$login_id]) < strtotime($_toDate_C)){
        $_toDate_C = $to_date[$login_id];
      } 
      $productive_days_3 = $days_diff = (strtotime($_toDate_C)- strtotime($_fromDate_C))/(24*60*60)+1;
      
      //echo"<br>cond 3 : ".$login_id." >> ".$_fromDate_C." >> ".$_toDate_C;
      
		  $no_visit_3 = $this->No_visit_days_model->get_all($login_id, $_fromDate_C, $_toDate_C);
		  foreach($no_visit_3 as $no_visit)
		  {
			if ($no_visit->no_visit_reason == 'Local Authority Declared Holiday')
			  $auth_dec_holidays[2] = $auth_dec_holidays[2] + 1;
			else
			  $no_visit_days[2] = $no_visit_days[2] + 1;
		  }
      
      $state_holiday_list_3 = $this->Holidays_model->get_state_holidays_list($state_id, $_fromDate_C, $_toDate_C, $current_group);
      foreach($state_holiday_list_3 as $stateholiday)
      {
        if(!in_array(date('Y-m-d', strtotime($stateholiday->activity_date)), $activity_dates[2])){
          $activity_dates[2][] = date('Y-m-d', strtotime($stateholiday->activity_date));
        }
      }
      $sun_2_3_list_sat_3 = $this->Holidays_model->get_2_3_saturday_list($_fromDate_C, $_toDate_C, $current_group);
      foreach($sun_2_3_list_sat_3 as $holiday)
      {
        if(!in_array(date('Y-m-d', strtotime($holiday->activity_date)), $activity_dates[2])){
          $activity_dates[2][] = date('Y-m-d', strtotime($holiday->activity_date));
        }
      }
        
      $leaves_arr_3 = $this->Leaves_model->get_all_leave($login_id, $_fromDate_C, $_toDate_C);
      $leaves_data_3 = $this->common_functions->return_month_leave_distinct($leaves_arr_3, $_fromDate_C, $_toDate_C, $state_holiday_list_3, $sun_2_3_list_sat_3);
      $leaves[2] = count($leaves_data_3);

		  $trainings_3 = $this->Trainings_model->get_all($login_id, $_fromDate_C, $_toDate_C);
		  foreach($trainings_3 as $training)
		  {
        if(!in_array(date('Y-m-d', strtotime($training->activity_date)), $activity_dates[2])){
          $activity_dates[2][] = date('Y-m-d', strtotime($training->activity_date));
        }
        if ($training->mode == 'internal')
          $home_district_trainings[2] = $home_district_trainings[2] + 1;
        else
          $outstation_trainings[2] = $outstation_trainings[2] + 1;
          
        if ($training->training_type == 'refresher')
          $refresher_trainings[2] = $refresher_trainings[2] + 1;
        else if ($training->training_type == 'annual')
          $annual_trainings[2] = $annual_trainings[2] + 1;      
		  }
		  // MEETINGS STARTED
		  $meetings_3 = $this->Meetings_model->get_all($login_id, $_fromDate_C, $_toDate_C);
		  foreach($meetings_3 as $meeting)
		  {
        if(!in_array(date('Y-m-d', strtotime($meeting->activity_date)), $activity_dates[2])){
          $activity_dates[2][] = date('Y-m-d', strtotime($meeting->activity_date));
        }
          if ($meeting->block_id == 0 and $meeting->is_internal == false)
          $district_level_meetings[2] = $district_level_meetings[2] + 1;
          else if ($meeting->block_id > 0 and $meeting->is_internal == false)
          $block_level_meetings[2] = $block_level_meetings[2] + 1;
        
        if ($meeting->is_internal == true){
          $internal_meetings[2] = $internal_meetings[2] + 1;
          if(!in_array(date('Y-m-d', strtotime($meeting->activity_date)), $activity_dates[2]) && !in_array(date('Y-m-d', strtotime($meeting->activity_date)), $internal_meeting_dates[2])){
            $internal_meeting_dates[2][] = date('Y-m-d', strtotime($meeting->activity_date));
          }
        }
        
		  }
      
      $school_visit_count_3 = 0;
      $school_visits_3 = $this->school_visits_model->get_all($login_id, $_fromDate_C, $_toDate_C);
      foreach($school_visits_3 as $school_visit)
      {
        $school_visit_count_3++;
        if(!in_array(date('Y-m-d', strtotime($school_visit->activity_date)), $activity_dates[2])){
          $activity_dates[2][] = date('Y-m-d', strtotime($school_visit->activity_date));
        }
      }
      
      $LD_activities_date_3 = array();
      $LD_activities_3 = $this->Claim_model->get_distince_ld_activities($login_id, $_fromDate_C, $_toDate_C);
      foreach($LD_activities_3 as $LD_activitie)
      {
        if(!in_array($LD_activitie->dat, $activity_dates[2])){
          $LD_activities_date_3[] = $LD_activitie->dat;
        }
      }
      $distinct_LD_count_3 = count($LD_activities_date_3);
      
      $state_holiday_3 = count($state_holiday_list_3);
		  $sun_2_3_sat_3 = count($sun_2_3_list_sat_3);
      
		  //$he_visit_count_3 = $this->school_visits_model->get_HE_visit_count($login_id, $state_id, $_fromDate_C, $_toDate_C);
		  //$le_visit_count_3 = $this->school_visits_model->get_LE_visit_count($login_id, $state_id, $_fromDate_C, $_toDate_C);
		  
      //echo"<br>$login_id : ".$he_visit_count_3." >>> ".$le_visit_count_3." >>> ".($refresher_trainings[2] * 2)." >>> ".($annual_trainings[2] * 2)." >>> ".$district_level_meetings[2]." >>> ".$block_level_meetings[2]." >>> ".$productive_days_3." >>> ".$state_holiday_3." >>> ".$leaves[2]." >>> ".$sun_2_3_sat_3." >>> ".$auth_dec_holidays[2]." >>> ".$internal_meetings[2]." >>> ".$distinct_LD_count_3;
      
      $visitproductivity_3[$login_id] = round((($school_visit_count_3) + ($refresher_trainings[2] * 2) + ($annual_trainings[2] * 2) + ($district_level_meetings[2] + $block_level_meetings[2])) / ($productive_days_3 - ($state_holiday_3 + $leaves[2] + $sun_2_3_sat_3 + $auth_dec_holidays[2]+ count($internal_meeting_dates[2]) + $distinct_LD_count_3)),1);
		  
      $avg_visit_prod_month_3 = $avg_visit_prod_month_3 + $visitproductivity_3[$login_id];
		  $school_visit_days_3[$login_id] = $school_visit_count_3;
		  $school_visit_month_3 = $school_visit_month_3 + $school_visit_days_3[$login_id];
		  
		  $count_users_3++;
 	  }
	  else{
		  $visitproductivity_3[$login_id] = 0;
		  $school_visit_days_3[$login_id] = 0;
	  }
      
      /*************************************************************************/     	
      //echo"<br>date : ".$from_date[$login_id]." >> ".$to_date[$login_id];
      //echo"<br>cond 4 : ".$login_id." >> ".$from_date[$login_id]." >> ".$to_date[$login_id];
      
      $no_visit_yt = $this->No_visit_days_model->get_all($login_id, $from_date[$login_id], $to_date[$login_id]);
      foreach($no_visit_yt as $no_visit)
      {
        if ($no_visit->no_visit_reason == 'Local Authority Declared Holiday')
          $auth_dec_holidays[3] = $auth_dec_holidays[3] + 1;
        else
          $no_visit_days[3] = $no_visit_days[3] + 1;
      } 
      
      $state_holiday_list_yt = $this->Holidays_model->get_state_holidays_list($state_id, $from_date[$login_id], $to_date[$login_id], $current_group);
      foreach($state_holiday_list_yt as $stateholiday)
      {
        if(!in_array(date('Y-m-d', strtotime($stateholiday->activity_date)), $activity_dates[3])){
          $activity_dates[3][] = date('Y-m-d', strtotime($stateholiday->activity_date));
        }
      }
      $sun_2_3_list_sat_yt = $this->Holidays_model->get_2_3_saturday_list($from_date[$login_id], $to_date[$login_id], $current_group);
      foreach($sun_2_3_list_sat_yt as $holiday)
      {
        if(!in_array(date('Y-m-d', strtotime($holiday->activity_date)), $activity_dates[3])){
          $activity_dates[3][] = date('Y-m-d', strtotime($holiday->activity_date));
        }
      }
        
      $leaves_arr_yt = $this->Leaves_model->get_all_leave($login_id, $from_date[$login_id], $to_date[$login_id]);
      $leaves_data_yt = $this->common_functions->return_month_leave_distinct($leaves_arr_yt, $from_date[$login_id], $to_date[$login_id], $state_holiday_list_yt, $sun_2_3_list_sat_yt);
      $leaves[3] = count($leaves_data_yt);  
      
      $trainings_yt = $this->Trainings_model->get_all($login_id, $from_date[$login_id], $to_date[$login_id]);
      foreach($trainings_yt as $training)
      {
        if(!in_array(date('Y-m-d', strtotime($training->activity_date)), $activity_dates[3])){
          $activity_dates[3][] = date('Y-m-d', strtotime($training->activity_date));
        }
        if ($training->mode == 'internal')
          $home_district_trainings[3] = $home_district_trainings[3] + 1;
        else
          $outstation_trainings[3] = $outstation_trainings[3] + 1;
        
        if ($training->training_type == 'refresher')
          $refresher_trainings[3] = $refresher_trainings[3] + 1;
        else if ($training->training_type == 'annual')
          $annual_trainings[3] = $annual_trainings[3] + 1;      
      }
          
      // MEETINGS STARTED
      $meetings_yt = $this->Meetings_model->get_all($login_id, $from_date[$login_id], $to_date[$login_id]);
      foreach($meetings_yt as $meeting)
      {
        if(!in_array(date('Y-m-d', strtotime($meeting->activity_date)), $activity_dates[3])){
          $activity_dates[3][] = date('Y-m-d', strtotime($meeting->activity_date));
        }
        
        if ($meeting->block_id == 0 and $meeting->is_internal == false)
          $district_level_meetings[3] = $district_level_meetings[3] + 1;
        else if ($meeting->block_id > 0 and $meeting->is_internal == false)
          $block_level_meetings[3] = $block_level_meetings[3] + 1;
      
        if ($meeting->is_internal == true){
          $internal_meetings[3] = $internal_meetings[3] + 1;
          if(!in_array(date('Y-m-d', strtotime($meeting->activity_date)), $activity_dates[3]) && !in_array(date('Y-m-d', strtotime($meeting->activity_date)), $internal_meeting_dates[3])){
            $internal_meeting_dates[3][] = date('Y-m-d', strtotime($meeting->activity_date));
          }
        }  
      }
      
      $school_visit_count_yt = 0;
      $school_visits_yt = $this->school_visits_model->get_all($login_id, $from_date[$login_id], $to_date[$login_id]);
      foreach($school_visits_yt as $school_visit)
      {
        $school_visit_count_yt++;
        if(!in_array(date('Y-m-d', strtotime($school_visit->activity_date)), $activity_dates[3])){
          $activity_dates[3][] = date('Y-m-d', strtotime($school_visit->activity_date));
        }
      }
      
      $LD_activities_date_yt = array();
      $LD_activities_yt = $this->Claim_model->get_distince_ld_activities($login_id, $from_date[$login_id], $to_date[$login_id]);
      foreach($LD_activities_yt as $LD_activitie)
      {
        if(!in_array($LD_activitie->dat, $activity_dates[3])){
          $LD_activities_date_yt[] = $LD_activitie->dat;
        }
      }
      $distinct_LD_count_yt = count($LD_activities_date_yt);
      
      //-----------------------FIND STATE HOLIDAYS
      $state_holiday_yt = count($state_holiday_list_yt);
		  //-----------------------FIND SUN AND 2,3 SAT
      $sun_2_3_sat_yt = count($sun_2_3_list_sat_yt);
      
      //-----------------------FIND HE VISIT COUNT
      //$he_visit_count_yt = $this->school_visits_model->get_HE_visit_count($login_id, $state_id, $from_date[$login_id], $to_date[$login_id]);
      //-----------------------FIND LE VISIT COUNT
      //$le_visit_count_yt = $this->school_visits_model->get_LE_visit_count($login_id, $state_id, $from_date[$login_id], $to_date[$login_id]);
      //echo"<br>".$productive_days_yt[$login_id]." >> ".$state_holiday_yt." >> ".$leaves[3]." >> ".$sun_2_3_sat_yt." >> ".$auth_dec_holidays[3];
      $net_value = $productive_days_yt[$login_id] - ($state_holiday_yt + $leaves[3] + $sun_2_3_sat_yt + $auth_dec_holidays[3]);

      //-----------------------FIND VISIT PRODUCTIVITY 
      $visitproductivity_yt[$login_id] = ($net_value > 0 ? round((($school_visit_count_yt) + ($refresher_trainings[3] * 2) + ($annual_trainings[3] * 2) + ($district_level_meetings[3] + $block_level_meetings[3])) / ($productive_days_yt[$login_id] - ($state_holiday_yt + $leaves[3] + $sun_2_3_sat_yt + $auth_dec_holidays[3] + count($internal_meeting_dates[3]) + $distinct_LD_count_yt)),1) : 0);

      $avg_visit_prod_month_yt = $avg_visit_prod_month_yt + $visitproductivity_yt[$login_id];

      //-----------------------FIND NO. OF SCHOOL VISITED IN THE MONTH
      $school_visit_days_yt[$login_id] = $school_visit_count_yt;
      $school_visit_month_yt = $school_visit_month_yt + $school_visit_days_yt[$login_id];
    }

    $count_record = count($user_ids);

    $this->data['school_visit_month_1'] = $school_visit_month_1;
    $this->data['school_visit_month_2'] = $school_visit_month_2;
    $this->data['school_visit_month_3'] = $school_visit_month_3;
    $this->data['school_visit_month_yt'] = $school_visit_month_yt;

    if($count_record > 0){
      $this->data['visit_1'] = ($count_users_1 > 0 ? round(($avg_visit_prod_month_1/$count_users_1),1) : 0);
      $this->data['visit_2'] = ($count_users_2 > 0 ? round(($avg_visit_prod_month_2/$count_users_2),1) : 0);
      $this->data['visit_3'] = ($count_users_3 > 0 ? round(($avg_visit_prod_month_3/$count_users_3),1) : 0);
      $this->data['visit_yt'] = round(($avg_visit_prod_month_yt/$count_record),1);
    }
    else{
      $this->data['visit_1'] = 0;      $this->data['visit_2'] = 0;      $this->data['visit_3'] = 0;      $this->data['visit_yt'] = 0;
    }

    $this->data['schoolvisit_days_1'] = $school_visit_days_1;
    $this->data['schoolvisit_days_2'] = $school_visit_days_2;
    $this->data['schoolvisit_days_3'] = $school_visit_days_3;
    $this->data['schoolvisit_days_yt'] = $school_visit_days_yt;

    $this->data['visit_productivity_1'] = $visitproductivity_1;
    $this->data['visit_productivity_2'] = $visitproductivity_2;
    $this->data['visit_productivity_3'] = $visitproductivity_3;
    $this->data['visit_productivity_yt'] = $visitproductivity_yt;
    
    $this->data['state_id'] = $state_id;
    $this->data['monthno'] = $inpmonth;
    $this->data['year'] = $inpyear;
    
    $this->data['user_names'] = $user_names;
    $this->data['state_name'] = $state_data['name'];
    $this->data['current_month_name'] = $month_name;
    $this->data['prev_month1_name'] = $prev_month1_name;
    $this->data['prev_month2_name'] = $prev_month2_name;

    $download = (isset($_POST['download']) and $_POST['download'] == 1 ? $_POST['download'] : 0);
    $pdf = (isset($_POST['pdf']) and $_POST['pdf'] == 1 ? $_POST['pdf'] : 0);
    
		if ($download == 1)
    {
      $myFile = "./uploads/reports_archive/state_productivity_report.xls";
      $this->load->library('parser');
			$this->data['download'] = $download;
      //pass retrieved data into template and return as a string
      $stringData = $this->parser->parse('reports_archive/state_productivity_report', $this->data, true);
      //open excel and write string into excel
      $fh = fopen($myFile, 'w') or die("can't open file");
      fwrite($fh, $stringData);
      fclose($fh);
      //download excel file
      $this->common_functions->downloadExcel("state_productivity_report");
      unlink($myFile);
    }
    else if($pdf == 1)
    {
        //pass retrieved data into template and return as a string
        $this->data['pdf'] = 1;
        $summary = $this->load->view('reports_archive/state_productivity_report',$this->data,true);
        $this->common_functions->create_pdf($summary,'state_productivity_report');
    }
    else{
      $this->data['partial'] = 'reports_archive/state_productivity_report';
      $this->data['page_title'] = 'State Productivity Report';
      $this->load->view('templates/index', $this->data);
    }
  }

  function spark_school_rating()
  {
    $this->output->enable_profiler(false);
    $month = 4;
    $year = 2016;
    $this->data['year'] = $month;
    $this->data['month'] = $year;

    if($this->data['current_role']=="state_person" || $this->data['current_role']=="manager"){
				
				$states = $this->states_model->get_by_id($this->data['user_state_id']);
    }
    else{
				
				$states = $this->states_model->get_all();
    }
    $this->data['states'] = $states;
    $this->data['partial'] = 'reports_archive/spark_school_rating';
    $this->data['page_title'] = 'Spark School Rating';
    $this->load->view('templates/index', $this->data); 
  }

  function spark_school_rating_report()
  {
    if(!isset($_POST['start_month']) && !isset($_POST['start_year']))
    {
        redirect('reports_archive/spark_school_rating');
    }
    //$state_id = $_POST['spark_state_id'];
    $posteddata = (isset($_POST['spark_id']) ? $_POST['spark_id'] : $this->data['current_user_id'].'-'.$this->data['current_name'].'-'.$this->data['user_state_id']);
    
    $startmonth  = $_POST['start_month'];
    $endmonth  = $_POST['end_month'];
    $startyear = $_POST['start_year'];
    $endyear = $_POST['end_year'];
    $startdate = '01/'.$startmonth.'/'.$startyear;
    $enddate = '01/'.$endmonth.'/'.$endyear;
    $enddate = date('t/m/Y', strtotime($endyear.'-'.$endmonth.'-01'));

    if(!empty($posteddata))
    {
      //Explode data into separte value 
      $final_posted_data = explode("-",$posteddata);  
      //Check for Empty Login _ID     
      if(count($final_posted_data) > 0){
          //Login ID
          $login_id    = $final_posted_data[0]; 
          $spark_name  = $final_posted_data[1];
          $state_id    = $final_posted_data[2];      
      }
    }
    $period = date('M-y', strtotime('01-'.$startmonth.'-'.$startyear));
    if(!empty($_POST['end_month']) && strtotime($startdate) != strtotime($enddate)){
        $period .= " to ".date('M-y', strtotime('01-'.$endmonth.'-'.$endyear));
    }
    if(empty($state_id) || empty($login_id)){
       redirect('reports_archive/spark_school_rating');
    }
    $state_data = $this->states_model->getById($state_id);

    $spark_data = $this->getSparkInfo($startdate,$enddate,$login_id,$state_id);

    $this->load->Model('Assessment_model');
    $this->load->Model('Observations_model');
    $this->load->Model('Classes_model');
    $this->load->Model('Subjects_model');
    $this->load->Model('districts_model');

    $state_classes = $this->Subjects_model->check_mapping($state_id);
    
    $classes = array();
    $subject_ids = array();
    $class_subjects = array();
    $c = 0;
    foreach($state_classes as $state_class)
    {
        $class_id = $state_class->class_id;
        $subject_id = $state_class->subject_id;
        if(!in_array($class_id, $classes)){
          array_push($classes, $class_id);
        }
        
        if(!in_array($subject_id, $subject_ids)){
          array_push($subject_ids, $subject_id);
        }
        
        $subjectData = $this->Subjects_model->getById($subject_id);

        $class_subjects[$class_id][$c]['id'] = $subjectData['id'];
        $class_subjects[$class_id][$c]['name'] = $subjectData['name'];
        $c++;
    }
    
    $observationsData = $this->Observations_model->getReportsObservation($state_id, $classes);
    
    $class_observation = array();
    $c = 0;
    foreach($observationsData as $observations)
    {
      $class_observation[$observations->class_id][$c]['id'] = $observations->id;
      $class_observation[$observations->class_id][$c]['question'] = $observations->question;
      $c++;
    }
    
    $assessment_data = $this->Assessment_model->getSparkSchoolAssessment($startdate,$enddate,$state_id, $classes, $login_id);

    
    $activity_date_arr = array();
    $school_ids = array();
    $district_ids = array();
    $class_subject_question_ans_counts = array();
    $i = 0;
    //print_r($assessment_data);
    foreach($assessment_data as $assessments)
    {
      $activity_date = date('d-m-Y', strtotime($assessments->activity_date));
      $district_id = $assessments->district_id;
      $school_id = $assessments->school_id;
      $question_id = $assessments->question_id;
      $class_id = $assessments->class_id;
      $subject_id = $assessments->subject_id;
      
      if(!isset($activity_date_arr[$activity_date])){
        $activity_date_arr[$activity_date]['school_id'][$i] = $school_id;
        $activity_date_arr[$activity_date]['district_id'][$i] = $district_id;
        $i++;
      }else{
        if(!in_array($school_id, $activity_date_arr[$activity_date]['school_id'])){
          $activity_date_arr[$activity_date]['school_id'][$i] = $school_id;
          $activity_date_arr[$activity_date]['district_id'][$i] = $district_id;
          $i++;
        }
      }
      
      if(!in_array($school_id, $school_ids))
        array_push($school_ids, $school_id);
      
      if(!in_array($district_id, $district_ids))
        array_push($district_ids, $district_id);
      
      $csq_key = $activity_date.'_'.$school_id.'_'.$class_id.'_'.$subject_id.'_'.$question_id;
        
      $ans_value = $assessments->answer;
      
      if($ans_value == 1)
        $class_subject_question_ans_counts[$csq_key] = 1;
      else  
        $class_subject_question_ans_counts[$csq_key] = 0;
    }
    //print_r($activity_date_arr);
    //die;
    //print_r($class_subject_question_ans_counts);
    
    $class_names = array();
    if (count($classes) > 0)
    {
      $this->load->model('classes_model');
      $classData = $this->classes_model->getByIds($classes);
      foreach($classData as $class)
      {
        $class_names[$class->id] = $class->name;
      }
    }
    
    $school_data = array();
    if (count($school_ids) > 0)
    {
      $this->load->model('schools_model');
      $schoolData = $this->schools_model->get_schools_details($school_ids);
      foreach($schoolData as $schools)
      {
        $school_data[$schools->id]['name'] = $schools->name;
        $school_data[$schools->id]['dise_code'] = $schools->dise_code;
      }
    }
    
    $district_data = array();
    if (count($district_ids) > 0)
    {
      $this->load->model('districts_model');
      $districtData = $this->districts_model->getByIds($district_ids);
      foreach($districtData as $districts)
      {
        $district_data[$districts->id] = $districts->name;
      }
    }
    
    $this->data['classes'] = $classes;
    $this->data['school_data'] = $school_data;
    $this->data['district_data'] = $district_data;
    $this->data['class_names'] = $class_names;
    $this->data['class_subjects'] = $class_subjects;
    $this->data['class_observation'] = $class_observation;
    $this->data['activity_date_arr'] = $activity_date_arr;
    $this->data['question_arr'] = $observationsData;
    $this->data['data_counts'] = $class_subject_question_ans_counts;
    
    $this->data['STATE'] = $state_data['name'];
    $this->data['PERIOD'] = $period;
    
    $this->data['blocks_count'] = $spark_data[0]->blocks_count;
    $this->data['spark'] = $spark_data[0]->name;
    $this->data['district'] = $spark_data[0]->districts;
    $this->data['no_of_teachers_trained'] = $spark_data[0]->no_of_teachers_trained;
    $this->data['annual_training'] = $spark_data[0]->annual_training;
    $this->data['refresher_training'] = $spark_data[0]->refresher_training;

    $this->data['state_id'] = $state_id;
    $this->data['user_login_id'] = $posteddata;
    $this->data['start_month'] = $startmonth;
    $this->data['end_month'] = $endmonth;
    $this->data['start_year'] = $startyear;
    $this->data['end_year'] = $endyear;

    $download = (isset($_POST['download']) and $_POST['download'] == 1 ? $_POST['download'] : 0);
    
    $pdf = (isset($_POST['pdf']) and $_POST['pdf'] == 1 ? $_POST['pdf'] : 0);

		if($download == 1)
    {
        //pass retrieved data into template and return as a string
        $this->data['excel'] = 1;
        $myFile = "./uploads/reports_archive/spark_school_rating_report.xls";
        $this->load->library('parser');
        $this->data['download'] = $download;
        
        $stringData = $this->parser->parse('reports_archive/spark_school_rating_report', $this->data, true);

        //open excel and write string into excel
        $fh = fopen($myFile, 'w') or die("can't open file");
        fwrite($fh, $stringData);
        fclose($fh);
        //download excel file
        $this->common_functions->downloadExcel("spark_school_rating_report");
        unlink($myFile);
    }
    else if($pdf == 1)
    {
        //pass retrieved data into template and return as a string
        $this->data['pdf'] = 1;
        $summary = $this->load->view('reports_archive/spark_school_rating_report',$this->data,true);
        $this->common_functions->create_pdf($summary,'spark_school_rating_report');
    }
    else{
      $this->data['partial'] = 'reports_archive/spark_school_rating_report';
      $this->data['page_title'] = 'Spark School Rating';
      $this->load->view('templates/index', $this->data);
    }
  }

  function block_school_rating()
  {
    $this->output->enable_profiler(false);
    $month = 4;
    $year = 2016;
    $this->data['year'] = $month;
    $this->data['month'] = $year;

    if($this->data['current_role']=="state_person" || $this->data['current_role']=="manager"){
				
				$states = $this->states_model->get_by_id($this->data['user_state_id']);
    }
    else{
				
				$states = $this->states_model->get_all();
    }
    $this->data['states'] = $states;
    $download = 0;
    $this->data['partial'] = 'reports_archive/block_school_rating';
    $this->data['page_title'] = 'Block School Rating';
    $this->load->view('templates/index', $this->data);
  }
    
  function block_school_rating_report()
  {
    if(!isset($_POST['start_month']) && !isset($_POST['start_year']))
    {
        redirect('reports_archive/block_school_rating');
    }
    
    $state_id 	  = $_POST['state_id'];
    $district_id  = $_POST['district_id'];
    $block_id		  = $_POST['block_id'];
    $startmonth = $_POST['start_month'];
    $endmonth   = $_POST['end_month'];
    $startyear  = $_POST['start_year'];
    $endyear    = $_POST['end_year'];
    
   $startdate 	= '01/'.$startmonth.'/'.$startyear;
   $enddate 		= date('t/m/Y', strtotime($endyear.'-'.$endmonth.'-01'));
    
    $this->load->Model('Subjects_model');
    $this->load->Model('Observations_model');
    $this->load->Model('Assessment_model');
    
    $state_classes = $this->Subjects_model->check_mapping($state_id);
    
    $classes = array();
    $subject_ids = array();
    $class_subjects = array();
    $c = 0;
    foreach($state_classes as $state_class)
    {
        $class_id = $state_class->class_id;
        $subject_id = $state_class->subject_id;
        if(!in_array($class_id, $classes)){
          array_push($classes, $class_id);
        }
        
        if(!in_array($subject_id, $subject_ids)){
          array_push($subject_ids, $subject_id);
        }
        
        $subjectData = $this->Subjects_model->getById($subject_id);

        $class_subjects[$class_id][$c]['id'] = $subjectData['id'];
        $class_subjects[$class_id][$c]['name'] = $subjectData['name'];
        $c++;
    }
    
    $observationsData = $this->Observations_model->getReportsObservation($state_id, $classes);
    
    $class_observation = array();
    $c = 0;
    foreach($observationsData as $observations)
    {
      $class_observation[$observations->class_id][$c]['id'] = $observations->id;
      $class_observation[$observations->class_id][$c]['question'] = $observations->question;
      $c++;
    }
    
    $block_data        = $this->getBlockInfo($startdate,$enddate,$state_id,$district_id,$block_id);
	  //$rating_data 			 = $this->getBlockRating($startdate,$enddate,$state_id,$district_id, $block_id);
    $assessment_data = $this->Assessment_model->getSchoolAssessmentList($startdate, $enddate, $classes, $state_id, $district_id, $block_id);
    //print_r($assessment_data);
    
    $activity_date_arr = array();
    $school_ids = array();
    $district_ids = array();
    $class_subject_question_ans_counts = array();
    $i = 0;
    foreach($assessment_data as $assessments)
    {
      $activity_date = date('d-m-Y', strtotime($assessments->activity_date));
      $district_id = $assessments->district_id;
      $school_id = $assessments->school_id;
      $question_id = $assessments->question_id;
      $class_id = $assessments->class_id;
      $subject_id = $assessments->subject_id;
      
      if(!isset($activity_date_arr[$activity_date])){
        $activity_date_arr[$activity_date]['school_id'][$i] = $school_id;
        $activity_date_arr[$activity_date]['district_id'][$i] = $district_id;
        $i++;
      }else{
        if(!in_array($school_id, $activity_date_arr[$activity_date]['school_id'])){
          $activity_date_arr[$activity_date]['school_id'][$i] = $school_id;
          $activity_date_arr[$activity_date]['district_id'][$i] = $district_id;
          $i++;
        }
      }
      
      if(!in_array($school_id, $school_ids))
        array_push($school_ids, $school_id);
      
      if(!in_array($district_id, $district_ids))
        array_push($district_ids, $district_id);
      
      $csq_key = $activity_date.'_'.$school_id.'_'.$class_id.'_'.$subject_id.'_'.$question_id;
        
      $ans_value = $assessments->answer;
      
      if($ans_value == 1)
        $class_subject_question_ans_counts[$csq_key] = 1;
      else  
        $class_subject_question_ans_counts[$csq_key] = 0;
    }
    //print_r($activity_date_arr);
    //die;
    //print_r($class_subject_question_ans_counts);
    
    $class_names = array();
    if (count($classes) > 0)
    {
      $this->load->model('classes_model');
      $classData = $this->classes_model->getByIds($classes);
      foreach($classData as $class)
      {
        $class_names[$class->id] = $class->name;
      }
    }
    
    $school_data = array();
    if (count($school_ids) > 0)
    {
      $this->load->model('schools_model');
      $schoolData = $this->schools_model->get_schools_details($school_ids);
      foreach($schoolData as $schools)
      {
        $school_data[$schools->id]['name'] = $schools->name;
        $school_data[$schools->id]['dise_code'] = $schools->dise_code;
      }
    }
    
    $start_date 	= '01-'.$startmonth.'-'.$startyear;
    $end_date 		= '01-'.$endmonth.'-'.$endyear;
    $period = date('M-y', strtotime($start_date));
    if(!empty($_POST['end_month']) && strtotime($start_date) != strtotime($end_date)){
        $period .= " to ".date('M-y', strtotime($end_date));
    }
    
    $this->data['classes'] = $classes;
    $this->data['school_data'] = $school_data;
    $this->data['class_names'] = $class_names;
    $this->data['class_subjects'] = $class_subjects;
    $this->data['class_observation'] = $class_observation;
    $this->data['activity_date_arr'] = $activity_date_arr;
    $this->data['question_arr'] = $observationsData;
    $this->data['data_counts'] = $class_subject_question_ans_counts;
    
    $this->data['PERIOD'] = $period;
        
    $this->data['schools_count'] = $block_data[0]->no_of_schools;
    $this->data['block'] = $block_data[0]->blocks;
    $this->data['district'] = $block_data[0]->districts;
    $this->data['no_of_teachers_trained'] = $block_data[0]->no_of_teachers_trained;
    $this->data['annual_training'] = $block_data[0]->annual_training;
    $this->data['refresher_training'] = $block_data[0]->refresher_training;
    
    $this->data['rating_data'] = array();
    $this->data['assessment_data'] = $assessment_data;

    $this->data['state_id'] = $state_id;
    $this->data['district_id'] = $district_id;
    $this->data['block_id'] = $block_id;
    $this->data['start_month'] = $startmonth;
    $this->data['end_month'] = $endmonth;
    $this->data['start_year'] = $startyear;
    $this->data['end_year'] = $endyear;

    $download = (isset($_POST['download']) and $_POST['download'] == 1 ? $_POST['download'] : 0);
    $pdf = (isset($_POST['pdf']) and $_POST['pdf'] == 1 ? $_POST['pdf'] : 0);
    
		if ($download == 1)
    {
      $this->data['excel'] = 1;
      $myFile = "./uploads/reports_archive/block_school_rating_report.xls";
      $this->load->library('parser');
      $this->data['download'] = $download;
      //pass retrieved data into template and return as a string
      $stringData = $this->parser->parse('reports_archive/block_school_rating_report', $this->data, true);

      //open excel and write string into excel
      $fh = fopen($myFile, 'w') or die("can't open file");
      fwrite($fh, $stringData);
      fclose($fh);
      //download excel file
      $this->common_functions->downloadExcel("block_school_rating_report");
      unlink($myFile);
    }
    else if($pdf == 1)
    {
        //pass retrieved data into template and return as a string
        $this->data['pdf'] = 1;
        $summary = $this->load->view('reports_archive/block_school_rating_report',$this->data,true);
        $this->common_functions->create_pdf($summary,'block_school_rating_report');
    }
    else{
      $this->data['partial'] = 'reports_archive/block_school_rating_report';
      $this->data['page_title'] = 'Block School Rating';
      $this->data['title'] = 'Block School Rating';
      $this->load->view('templates/index', $this->data);
    }
  }

  function district_school_rating()
  {
    $this->output->enable_profiler(false);
    $month = 4;
    $year = 2016;
    $this->data['year'] = $month;
    $this->data['month'] = $year;
    if($this->data['current_role']=="state_person" || $this->data['current_role']=="manager"){
				
				$states = $this->states_model->get_by_id($this->data['user_state_id']);
    }
    else{
				
				$states = $this->states_model->get_all();
    }
    $this->data['states'] = $states;
    $download = 0;
    $this->data['partial'] = 'reports_archive/district_school_rating'; //'reports_archive/state';
    $this->data['page_title'] = 'District School Rating';
    $this->load->view('templates/index', $this->data);
  }

  function district_school_rating_report()
  {
    if(!isset($_POST['start_month']) && !isset($_POST['start_year']))
    {
        redirect('reports_archive/district_school_rating');
    }
    
    $state_id 		= $_POST['state_id'];
    $district_id	= $_POST['district_id'];
    $startmonth   = $_POST['start_month'];	   
    $startyear    = $_POST['start_year'];
    $startdate    = '01/'.$startmonth.'/'.$startyear;
    $startdate    = date('t/m/Y', strtotime($startyear.'-'.$startmonth.'-01'));

    $results          = $this->getDistrictInfo($state_id,$district_id);
	  $trained          = $this->getDistrictTrained($startmonth,$startyear,$state_id,$district_id);

    $month_name = "";
    $prev_month1_name   = "";
    $prev_month2_name   = "";
		$cm_no 		= 0;
		$pv1_no 	= 0;
		$pv2_no 	= 0;
		$current_year1 = 0;
		$current_year2 = 0;
		
    switch($startmonth){
      case "1":
        $month_name = date('F', mktime(0, 0, 0, $startmonth, 10));
        $cm_no      = $startmonth;
        $prev_month1_name   = date('F', mktime(0, 0, 0, 12, 10));
        $pv1_no = 12;
        $current_year1 = $startyear - 1;
        $prev_month2_name   = date('F', mktime(0, 0, 0, 12 - $startmonth, 10));
        $pv2_no = 12 - $startmonth;
        $current_year2 = $startyear - 1;
        break;
      case "2":
        $month_name = date('F', mktime(0, 0, 0, $startmonth, 10));
        $cm_no      = $startmonth;
        $prev_month1_name   = date('F', mktime(0, 0, 0, $startmonth-1, 10));
        $pv1_no = $startmonth-1;
        $current_year1 = $startyear;
        $prev_month2_name   = date('F', mktime(0, 0, 0, 12, 10));
        $pv2_no = 12;
        $current_year2 = $startyear - 1;
        break;
      default:
        $month_name   = date('F', mktime(0, 0, 0, $startmonth, 10));
        $cm_no      = $startmonth;
        $prev_month1_name   = date('F', mktime(0, 0, 0, $startmonth-1, 10));
        $pv1_no = $startmonth-1;
        $current_year1 = $startyear;
        $prev_month2_name   = date('F', mktime(0, 0, 0, $startmonth-2, 10));
        $pv2_no = $startmonth-2;
        $current_year2 = $startyear;
    }
    
    $cm_result = $this->getDistrictRatingData($cm_no, $startyear, $state_id, $district_id);
    $pv1_result = $this->getDistrictRatingData($pv1_no,$current_year1,$state_id,$district_id);
    $pv2_result = $this->getDistrictRatingData($pv2_no,$current_year2,$state_id,$district_id);
    
		$total_block = $this->getBlockCount($state_id,$district_id);
		$nameblock = $this->getBlockName($state_id,$district_id);
	
    $this->data['trained'] = $trained;
    $this->data['cm_result'] = $cm_result;
    $this->data['pv1_result'] = $pv1_result;
    $this->data['pv2_result'] = $pv2_result;
    $this->data['nameblock'] = $nameblock;
    $this->data['month_name'] = $month_name;
    $this->data['prev_month1_name'] = $prev_month1_name;
    $this->data['prev_month2_name'] = $prev_month2_name;
    $this->data['state_name'] = $results[0]->state_name;
    $this->data['district_name'] = $results[0]->district_name;

    $this->data['state_id'] = $state_id;
    $this->data['district_id'] = $district_id;
    $this->data['start_month'] = $startmonth;
    $this->data['start_year'] = $startyear;

    $download = (isset($_POST['download']) and $_POST['download'] == 1 ? $_POST['download'] : 0);
    $pdf = (isset($_POST['pdf']) and $_POST['pdf'] == 1 ? $_POST['pdf'] : 0);
    
		if ($download == 1)
    {
      $this->data['excel'] = 1;
      $myFile = "./uploads/reports_archive/district_school_rating_report.xls";
      $this->load->library('parser');
      $this->data['download'] = $download;
      //pass retrieved data into template and return as a string
      $stringData = $this->parser->parse('reports_archive/district_school_rating_report', $this->data, true);

      //open excel and write string into excel
      $fh = fopen($myFile, 'w') or die("can't open file");
      fwrite($fh, $stringData);
      fclose($fh);
      //download excel file
      $this->common_functions->downloadExcel("district_school_rating_report");
      unlink($myFile);
    }
    else if($pdf == 1)
    {
        //pass retrieved data into template and return as a string
        $this->data['pdf'] = 1;
        $summary = $this->load->view('reports_archive/district_school_rating_report',$this->data,true);
        $this->common_functions->create_pdf($summary,'district_school_rating_report');
    }
    else{
      $this->data['partial'] = 'reports_archive/district_school_rating_report'; //'reports_archive/state';
      $this->data['page_title'] = 'District School Rating';
      $this->load->view('templates/index', $this->data);
    }
  }

  function getDistrictRatingData($cm_no, $startyear, $state_id, $district_id)
  {
    $this->load->Model('Assessment_model');
    
    $assessment_data1 = $this->Assessment_model->getSparkRatingReport($cm_no, $startyear, $state_id, $district_id);
    
    $blocks_arr = array();
    $report_ids = array();
    foreach($assessment_data1 as $assessments)
    {
      $block_id = $assessments->block_id;
      $report_id = $assessments->id;
      $school_id = $assessments->school_id;
      
      if(!isset($blocks_arr[$block_id]))
      {
        $blocks_arr[$block_id]['school_id'][] =  $school_id;
        $blocks_arr[$block_id]['report_id'][] =  $report_id;
      }
        if(!in_array($school_id, $blocks_arr[$block_id]['school_id']))
          array_push($blocks_arr[$block_id]['school_id'], $school_id);
      
        if(!in_array($report_id, $blocks_arr[$block_id]['report_id']))
          array_push($blocks_arr[$block_id]['report_id'], $report_id);
      
    }
    
    $rating_data = array();
    foreach($blocks_arr as $block_id=>$data)
    {
      $school_data = $data['school_id'];
      $school_count = count($data['school_id']);
      
      $report_id = $data['report_id'];
      
      $ratings_A = 0;
      $ratings_B = 0;
      $ratings_C = 0;
      $rating_count = 0;
      $ratingData = $this->Assessment_model->getSparkRatingCount($report_id);
      foreach($ratingData as $ratings){
        if($ratings['subject_rating'] == 'A'){
          $ratings_A = $ratings['Total'];
        }
        if($ratings['subject_rating'] == 'B'){
          $ratings_B = $ratings['Total'];
        }
        if($ratings['subject_rating'] == 'C'){
          $ratings_C = $ratings['Total'];
        }
        $rating_count = $ratings_A + $ratings_B + $ratings_C;
      }
      $rating_data[$block_id]['school_count'] = $school_count;
      $rating_data[$block_id]['rating_A'] = ($ratings_A > 0 ? ($ratings_A/$rating_count)*100 : 0);
      $rating_data[$block_id]['rating_B'] = ($ratings_B > 0 ? ($ratings_B/$rating_count)*100 : 0);
      $rating_data[$block_id]['rating_C'] = ($ratings_C > 0 ? ($ratings_C/$rating_count)*100 : 0);
    }
    return $rating_data;
  }

  function state_school_rating()
  {
    $this->output->enable_profiler(false);
    $month = 4;
    $year = 2016;
    $this->data['year'] = $month;
    $this->data['month'] = $year;

    if($this->data['current_role']=="state_person"){
				
				$states = $this->states_model->get_by_id($this->data['user_state_id']);
    }
    else{
				
				$states = $this->states_model->get_all();
    }
    
    $this->data['states'] = $states;
    $this->data['partial'] = 'reports_archive/state_school_rating';
    $this->data['page_title'] = 'State School Rating';
    $this->load->view('templates/index', $this->data); 
  }

  function state_school_rating_report()
  {
    if(!isset($_POST['start_month']) && !isset($_POST['start_year']))
    {
        redirect('reports_archive/state_school_rating');
    }
    
    $state_id 		 = $_POST['state_id'];
	  $startmonth       = $_POST['start_month'];	   
	  $startyear        = $_POST['start_year'];
    $startdate        = '01/'.$startmonth.'/'.$startyear;
	  $startdate        = date('t/m/Y', strtotime($startyear.'-'.$startmonth.'-01'));

	  $results          = $this->getStateInfo($state_id);
	  $trained          = $this->getStateTrained($startmonth,$startyear,$state_id);
    $trainedresults = array();
    foreach($trained as $value)
    {
      $trainedresults[$value->district_name]['refresher_training'] = $value->refresher_training;
      $trainedresults[$value->district_name]['annual_training'] = $value->annual_training;
    }
    
    $month_name = "";
    $prev_month1_name   = "";
    $prev_month2_name   = "";
		$cm_no 		= 0;
		$pv1_no 	= 0;
		$pv2_no 	= 0;
		$current_year1 = 0;
		$current_year2 = 0;
		
    switch($startmonth){

      case "1":
        $month_name = date('F', mktime(0, 0, 0, $startmonth, 10));
        $cm_no      = $startmonth;
        $prev_month1_name   = date('F', mktime(0, 0, 0, 12, 10));
        $pv1_no = 12;
        $current_year1 = $startyear - 1;
        $prev_month2_name   = date('F', mktime(0, 0, 0, 12 - $startmonth, 10));
        $pv2_no = 12 - $startmonth;
        $current_year2 = $startyear - 1;
        break;
      case "2":
        $month_name = date('F', mktime(0, 0, 0, $startmonth, 10));
        $cm_no      = $startmonth;
        $prev_month1_name   = date('F', mktime(0, 0, 0, $startmonth-1, 10));
        $pv1_no = $startmonth-1;
        $current_year1 = $startyear;
        $prev_month2_name   = date('F', mktime(0, 0, 0, 12, 10));
        $pv2_no = 12;
        $current_year2 = $startyear - 1;
        break;
      default:
        $month_name   = date('F', mktime(0, 0, 0, $startmonth, 10));
        $cm_no      = $startmonth;
        $prev_month1_name   = date('F', mktime(0, 0, 0, $startmonth-1, 10));
        $pv1_no = $startmonth-1;
        $current_year1 = $startyear;
        $prev_month2_name   = date('F', mktime(0, 0, 0, $startmonth-2, 10));
        $pv2_no = $startmonth-2;
        $current_year2 = $startyear;
    }
		
		$cm_result            = $this->getStateRatingData($cm_no,$startyear,$state_id);
		$pv1_result           = $this->getStateRatingData($pv1_no,$current_year1,$state_id);
		$pv2_result           = $this->getStateRatingData($pv2_no,$current_year2,$state_id);
    
		$total_district = $this->getDistrictCount($state_id);
		$namedistrict = $this->getDistrictName($state_id);
      
    $this->data['trained'] = $trainedresults;
    $this->data['cmresults'] = $cm_result;
    $this->data['pv1results'] = $pv1_result;
    $this->data['pv2results'] = $pv2_result;
    $this->data['namedistrict'] = $namedistrict;
    $this->data['month_name'] = $month_name;
    $this->data['prev_month1_name'] = $prev_month1_name;
    $this->data['prev_month2_name'] = $prev_month2_name;

    $this->data['state_name'] = $results[0]->state_name;

    $this->data['state_id'] = $state_id;
    $this->data['start_month'] = $startmonth;
    $this->data['start_year'] = $startyear;

    $download = (isset($_POST['download']) and $_POST['download'] == 1 ? $_POST['download'] : 0);
    $pdf = (isset($_POST['pdf']) and $_POST['pdf'] == 1 ? $_POST['pdf'] : 0);
    
		if ($download == 1)
    {
      $this->data['excel'] = 1;
      $myFile = "./uploads/reports_archive/state_school_rating_report.xls";
      $this->load->library('parser');
      $this->data['download'] = $download;
      //pass retrieved data into template and return as a string
      $stringData = $this->parser->parse('reports_archive/state_school_rating_report', $this->data, true);

      //open excel and write string into excel
      $fh = fopen($myFile, 'w') or die("can't open file");
      fwrite($fh, $stringData);
      fclose($fh);
      //download excel file
      $this->common_functions->downloadExcel("state_school_rating_report");
      unlink($myFile);
    }
    else if($pdf == 1)
    {
        //pass retrieved data into template and return as a string
        $this->data['pdf'] = 1;
        $summary = $this->load->view('reports_archive/state_school_rating_report',$this->data,true);
        $this->common_functions->create_pdf($summary,'state_school_rating_report');
    }
    else{
      $this->data['partial'] = 'reports_archive/state_school_rating_report'; 
      $this->data['page_title'] = 'State School Rating';
      $this->load->view('templates/index', $this->data);
    }
  }
  
  function getStateRatingData($cm_no, $startyear, $state_id)
  {
    $this->load->Model('Assessment_model');
    
    $assessment_data1 = $this->Assessment_model->getSparkRatingReport($cm_no, $startyear, $state_id);
    
    $districts_arr = array();
    $report_ids = array();
    foreach($assessment_data1 as $assessments)
    {
      $district_id = $assessments->district_id;
      $report_id = $assessments->id;
      $school_id = $assessments->school_id;
      
      if(!isset($districts_arr[$district_id]))
      {
        $districts_arr[$district_id]['school_id'][] =  $school_id;
        $districts_arr[$district_id]['report_id'][] =  $report_id;
      }
        if(!in_array($school_id, $districts_arr[$district_id]['school_id']))
          array_push($districts_arr[$district_id]['school_id'], $school_id);
      
        if(!in_array($report_id, $districts_arr[$district_id]['report_id']))
          array_push($districts_arr[$district_id]['report_id'], $report_id);
      
    }
    
    $rating_data = array();
    foreach($districts_arr as $district_id=>$data)
    {
      $school_data = $data['school_id'];
      $school_count = count($data['school_id']);
      
      $report_id = $data['report_id'];
      
      $ratings_A = 0;
      $ratings_B = 0;
      $ratings_C = 0;
      $rating_count = 0;
      $ratingData = $this->Assessment_model->getSparkRatingCount($report_id);
      foreach($ratingData as $ratings){
        if($ratings['subject_rating'] == 'A'){
          $ratings_A = $ratings['Total'];
        }
        if($ratings['subject_rating'] == 'B'){
          $ratings_B = $ratings['Total'];
        }
        if($ratings['subject_rating'] == 'C'){
          $ratings_C = $ratings['Total'];
        }
        $rating_count = $ratings_A + $ratings_B + $ratings_C;
      }
      $rating_data[$district_id]['school_count'] = $school_count;
      $rating_data[$district_id]['rating_A'] = ($ratings_A > 0 ? ($ratings_A/$rating_count)*100 : 0);
      $rating_data[$district_id]['rating_B'] = ($ratings_B > 0 ? ($ratings_B/$rating_count)*100 : 0);
      $rating_data[$district_id]['rating_C'] = ($ratings_C > 0 ? ($ratings_C/$rating_count)*100 : 0);
    }
    return $rating_data;
  }

  function getSparkInfo($startdate,$enddate,$loginid,$stateid) 
  {
     $select_stmt = "Select (select name from ssc_sparks where id = '$loginid') name,
     (select count(distinct block_id)
        from ssc_school_visits
       where (case when '$loginid' = '-1' then -1 else user_id end) = (case when '$loginid' = '-1' then -1 else '$loginid' end)
         and state_id = '$stateid'
         and date(activity_date) between str_to_date('$startdate','%d/%m/%Y') and  str_to_date('$enddate','%d/%m/%Y')) blocks_count,
     (select GROUP_CONCAT(distinct sd.name)
        from ssc_school_visits ssv,
             ssc_districts sd
       where ssv.district_id = sd.id
         and ssv.state_id = sd.state_id
         and ssv.user_id = '$loginid'
         and ssv.state_id ='$stateid'
         and date(ssv.activity_date) between str_to_date('$startdate','%d/%m/%Y') and  str_to_date('$enddate','%d/%m/%Y')) districts,
     (select IFNULL(sum(no_of_teachers_trained), 0)
        from ssc_trainings
       where (case when '$loginid' = '-1' then -1 else user_id end) = (case when '$loginid' = '-1' then -1 else '$loginid' end)
         and state_id = '$stateid'
     and subject_id IN (1, 2)
         and date(activity_date) between str_to_date('$startdate','%d/%m/%Y') and  str_to_date('$enddate','%d/%m/%Y')) no_of_teachers_trained,
     (SELECT count(training_duration)
        FROM ssc_trainings
       where (case when '$loginid' = '-1' then -1 else user_id end) = (case when '$loginid' = '-1' then -1 else '$loginid' end)
         and state_id = '$stateid'
     and subject_id IN (1, 2)
         and date(activity_date) between str_to_date('$startdate','%d/%m/%Y') and  str_to_date('$enddate','%d/%m/%Y')
         and training_type = 'annual') annual_training,
     (SELECT count(training_duration)
        FROM ssc_trainings
       where (case when '$loginid' = '-1' then -1 else user_id end) = (case when '$loginid' = '-1' then -1 else '$loginid' end)
         and state_id = '$stateid'
     and subject_id IN (1, 2)
         and date(activity_date) between str_to_date('$startdate','%d/%m/%Y') and  str_to_date('$enddate','%d/%m/%Y')
         and training_type = 'refresher') refresher_training";
    
     $select_query = $this->db->query($select_stmt);
     
//Execute query 
      $row = $select_query->result();
      return ($row);
    }
	
  function getBlockInfo($startdate,$enddate,$state_id,$district_id,$block_id){
		 $select_stmt = "Select (select count(distinct ss.id)
          from ssc_school_visits ssv,
               ssc_schools ss
          where ssv.school_id = ss.id
            and ssv.block_id = '$block_id'
            and ssv.district_id = '$district_id'
            and ssv.state_id ='$state_id'
            and ssv.status ='approved'
            and ssv.activity_date between str_to_date('$startdate','%d/%m/%Y') and  str_to_date('$enddate','%d/%m/%Y')) no_of_schools,
	   (select GROUP_CONCAT(distinct sb.name)
          from ssc_school_visits ssv,
               ssc_blocks sb
          where ssv.block_id = sb.id
            and ssv.state_id = sb.state_id
            and ssv.block_id = '$block_id'
            and ssv.district_id = '$district_id'
            and ssv.state_id ='$state_id'
            and ssv.status ='approved'
            and ssv.activity_date between str_to_date('$startdate','%d/%m/%Y') and  str_to_date('$enddate','%d/%m/%Y')) blocks,
       (select GROUP_CONCAT(distinct sd.name)
          from ssc_school_visits ssv,
               ssc_districts sd
          where ssv.district_id = sd.id
            and ssv.state_id = sd.state_id
            and ssv.block_id = '$block_id'
            and ssv.district_id = '$district_id'
            and ssv.state_id ='$state_id'
            and ssv.status ='approved'
            and ssv.activity_date between str_to_date('$startdate','%d/%m/%Y') and  str_to_date('$enddate','%d/%m/%Y')) districts,
       (select IFNULL(sum(no_of_teachers_trained), 0)
          from ssc_trainings
          where block_id = '$block_id'
            and district_id = '$district_id'
            and state_id = '$state_id'
            and status ='approved'
            and subject_id in (1, 2)
            and activity_date between str_to_date('$startdate','%d/%m/%Y') and  str_to_date('$enddate','%d/%m/%Y')) no_of_teachers_trained,
       (SELECT count(training_duration)
          FROM ssc_trainings
          where block_id = '$block_id'
            and district_id = '$district_id'
            and state_id = '$state_id'
            and status ='approved'
            and subject_id in (1, 2)
            and activity_date between str_to_date('$startdate','%d/%m/%Y') and  str_to_date('$enddate','%d/%m/%Y')
            and training_type = 'annual') annual_training,
       (SELECT count(training_duration)
          FROM ssc_trainings
          where block_id = '$block_id'
            and district_id = '$district_id'
            and state_id = '$state_id'
            and status ='approved'
            and subject_id in (1, 2)
            and activity_date between str_to_date('$startdate','%d/%m/%Y') and  str_to_date('$enddate','%d/%m/%Y')
            and training_type = 'refresher') refresher_training";
      
       $select_query = $this->db->query($select_stmt);
       //Execute query 
       $row = $select_query->result();
       return ($row);
	}
	
	function getBlockCount($state_id,$district_id){
		$select_stmt = "SELECT count(*) as dis_count FROM `ssc_blocks` WHERE state_id='$state_id' AND district_id= '$district_id'";
    $select_query = $this->db->query($select_stmt);
    //Execute query 
    $row = $select_query->result();
    return ($row[0]->dis_count);
	}
  
	function getBlockName($state_id,$district_id){
		$select_stmt = "SELECT id,name FROM `ssc_blocks` WHERE state_id='$state_id' AND district_id= '$district_id' order by name";
    $select_query = $this->db->query($select_stmt);
    //Execute query 
    $row = $select_query->result();
    return ($row);
	}

	function getDistrictInfo($stateid,$districtid){
      $select_stmt = "select
                      (select name from ssc_districts where id = '$districtid') district_name,
                      (select name from ssc_states where id = '$stateid') state_name";
      $select_query = $this->db->query($select_stmt);
       
      $row = $select_query->result();
      return ($row);
	 }
   
  function getDistrictTrained($startmonth,$startyear,$state_id,$district_id){
      $select_stmt = "select sb.name block_name,
           q.block_id,
           IFNULL(max(q.no_of_teachers_trained_annual), 0) no_of_teachers_trained_annual,
           IFNULL(max(q.no_of_teachers_trained_refresher), 0) no_of_teachers_trained_refresher,
           IFNULL(max(q.annual_training), 0) annual_training,
           IFNULL(max(q.refresher_training), 0) refresher_training,
           max(q.no_of_schools) no_of_schools
      from
      (Select st.block_id,
             sum((case when st.training_type = 'annual' then st.no_of_teachers_trained else null end)) no_of_teachers_trained_annual,
             sum((case when st.training_type != 'refresher' then st.no_of_teachers_trained else null end)) no_of_teachers_trained_refresher,
             count((case when st.training_type = 'annual' then st.training_type else null end)) annual_training,
             count((case when st.training_type != 'refresher' then st.training_type else null end)) refresher_training,
             null no_of_schools
        from ssc_trainings st
        where st.state_id = '$state_id'
        and st.district_id = '$district_id'
        and st.subject_id in (1, 2)
        and month(st.activity_date) = '$startmonth'
        and year(st.activity_date) = '$startyear'
        and st.status = 'approved'
        group by st.block_id
      union all
      select ssv.block_id,
             null,
             null,
             null,
             null,
             count(distinct ss.id) no_of_schools
        from ssc_school_visits ssv,
             ssc_schools ss
       where ssv.school_id = ss.id
         and ssv.state_id = '$state_id'
         and ssv.district_id = '$district_id'
         and month(ssv.activity_date) = '$startmonth'
         and year(ssv.activity_date) = '$startyear'
         and ssv.status = 'approved'
        group by ssv.block_id) q,
      ssc_blocks sb
      where q.block_id = sb.id
      group by sb.name, q.block_id";
		   $select_query = $this->db->query($select_stmt);
		   
		  //Execute query 
			$row = $select_query->result();

			return ($row);
	}

  function getDistrictCount($state_id){
		$select_stmt = "SELECT count(*) as dis_count FROM `ssc_districts` WHERE 
		state_id='$state_id'";
       $select_query = $this->db->query($select_stmt);
       
	//Execute query 
        $row = $select_query->result();
        return ($row[0]->dis_count);
	}

  function getDistrictName($state_id){
		$select_stmt = "SELECT id, name FROM `ssc_districts` WHERE 
		state_id='$state_id' order by name";
       $select_query = $this->db->query($select_stmt);
       
	//Execute query 
        $row = $select_query->result();
        return ($row);
	}
	

	function getStateRating($startmonth,$startyear,$state_id){
	 $select_query =  "SELECT r.district_name
	,r.total
	,ROUND((r.rating_A_schools / r.total_schools) * 100, 1) rating_A_perct
	,ROUND((r.rating_B_schools / r.total_schools) * 100, 1) rating_B_perct
	,ROUND((r.rating_C_schools / r.total_schools) * 100, 1) rating_C_perct
FROM (
	SELECT i.activity_date
		,i.district_name
		,i.school_id
		,i.state_id
		,i.district_id
		,count(CASE 
				WHEN month(i.activity_date) = '$startmonth'
					AND year(i.activity_date) = '$startyear'
					AND i.state_id = '$state_id'
					THEN i.district_name
				ELSE 0
				END) total
		,count((
				CASE 
					WHEN i.overall_rating = 'A'
						THEN i.school_id
					ELSE NULL
					END
				)) rating_A_schools
		,count((
				CASE 
					WHEN i.overall_rating = 'B'
						THEN i.school_id
					ELSE NULL
					END
				)) rating_B_schools
		,count((
				CASE 
					WHEN i.overall_rating = 'C'
						THEN i.school_id
					ELSE NULL
					END
				)) rating_C_schools
		,count(i.school_id) total_schools
	FROM (
		SELECT k.activity_date
			,sd.name district_name
			,sd.id
			,k.state_id
			,k.district_id
			,k.school_id
			,IFNULL((
					CASE 
						WHEN ((english_rating > 0 AND maths_rating > 0) AND ((maths_rating + english_rating) / 2) >= 76)
              THEN 'A'
            WHEN ((english_rating > 0 AND maths_rating > 0) AND ((maths_rating + english_rating) / 2) <= 75 AND ((maths_rating + english_rating) / 2) >= 51)
              THEN 'B'
            WHEN (english_rating = 0 AND maths_rating > 0 AND maths_rating >= 76)
              THEN 'A'
            WHEN (english_rating = 0 AND maths_rating > 0 AND (maths_rating <= 75 AND maths_rating >= 51))
              THEN 'B'
            WHEN ((maths_rating = 0 AND english_rating > 0) AND english_rating >= 76)
              THEN 'A'
            WHEN ((maths_rating = 0 AND english_rating > 0) AND (english_rating <= 75 AND english_rating >= 51))
              THEN 'B'    
						ELSE 'C'
						END
					), 0) overall_rating
		FROM ssc_districts sd
		INNER JOIN (
			SELECT ssv.activity_date
				,ssv.id
				,ssv.state_id
				,ssv.district_id
				,ssv.block_id
				,ssv.cluster_id
				,ssv.school_id
				,ssv.user_id
				,sa.school_visit_id
				,sa.assessment_id
				,MAX((
						CASE 
							WHEN sa.subject_id = 1
								THEN (((sa.trained_teachers + sa.tlm_usage + sa.sequence_usage + sa.prog_chart_usage) / 4) * 100)
							ELSE - 1
							END
						)) maths_rating
				,MAX((
						CASE 
							WHEN sa.subject_id = 2
								THEN (((sa.trained_teachers + sa.tlm_usage + sa.sequence_usage + sa.prog_chart_usage) / 4) * 100)
							ELSE - 1
							END
						)) english_rating
			FROM ssc_assessment sa
			INNER JOIN ssc_school_visits ssv ON ssv.id = sa.school_visit_id
				AND sa.subject_id IN (
					1
					,2
					)
				AND month(ssv.activity_date) = '$startmonth'
				AND year(ssv.activity_date) = '$startyear'
				AND ssv.state_id = '$state_id'
        AND ssv.status = 'approved'
        and ssv.id = (select max(ssv2.id) from ssc_school_visits ssv2
          where month(ssv2.activity_date) = '$startmonth'
            and year(ssv2.activity_date) = '$startyear'
            and ssv2.school_id = ssv.school_id
            and ssv2.user_id = ssv.user_id
            and ssv2.district_id = ssv.district_id
            and ssv2.block_id = ssv.block_id)
			GROUP BY DATE(ssv.activity_date), ssv.school_id
			) k ON k.district_id = sd.id
		) i
	GROUP BY i.district_name
	) r
GROUP BY r.district_name
ORDER BY r.district_name";
/*WHEN ((maths_rating + english_rating) / 2) >= 76
							THEN 'A'
						WHEN (
								((maths_rating + english_rating) / 2) <= 75
								AND ((maths_rating + english_rating) / 2) >= 51
								)
							THEN 'B'
						ELSE 'C'
						END*/
	$select_query =  $this->db->query($select_query);
     
    //Execute query 
    $row = $select_query->result();
	
	return ($row);
	}

	function getStateInfo($state_id){
		$select_stmt = "select name state_name from ssc_states where id = '$state_id'";
       $select_query = $this->db->query($select_stmt);
       
	//Execute query 
        $row = $select_query->result();
		return ($row);
	 }
	
	function getStateTrained($startmonth,$startyear,$state_id){
		$select_stmt = "select sd.name district_name,
       q.district_id,
       IFNULL(max(q.no_of_teachers_trained_fd), 0) no_of_teachers_trained_fd,
       IFNULL(max(q.no_of_teachers_trained_hd), 0) no_of_teachers_trained_hd,
       IFNULL(max(q.annual_training), 0) annual_training,
       IFNULL(max(q.refresher_training), 0) refresher_training,
       max(q.no_of_schools) no_of_schools
  from
  (Select st.district_id,
         sum((case when st.training_duration = '24:00' then st.no_of_teachers_trained else null end)) no_of_teachers_trained_fd,
         sum((case when st.training_duration != '24:00' then st.no_of_teachers_trained else null end)) no_of_teachers_trained_hd,
         count((case when st.training_duration = '24:00' then st.training_duration else null end)) annual_training,
         count((case when st.training_duration != '24:00' then st.training_duration else null end)) refresher_training,
         null no_of_schools
    from ssc_trainings st
   where st.state_id = '$state_id'
     and st.subject_id in (1, 2)
     and month(st.activity_date) = '$startmonth'
     and year(st.activity_date) = '$startyear'
     and st.status = 'approved'
   group by st.district_id
  union all
  select ssv.district_id,
         null,
         null,
         null,
         null,
         count(distinct ss.id) no_of_schools
    from ssc_school_visits ssv,
         ssc_schools ss
   where ssv.school_id = ss.id
     and ssv.state_id ='$state_id'
     and month(ssv.activity_date) = '$startmonth'
     and year(ssv.activity_date) = '$startyear'
     and ssv.status = 'approved'
    group by ssv.district_id) q,
  ssc_districts sd
where q.district_id = sd.id
 group by sd.name, q.district_id;
";
		   $select_query = $this->db->query($select_stmt);
		   
		  //Execute query 
			$row = $select_query->result();
			 // echo("<pre>");
		     // print_r($row);
		     // die();
			return ($row);
	}
	
  function getStatesName(){
		$select_stmt = "SELECT id, name FROM `ssc_states` order by name";
    $select_query = $this->db->query($select_stmt);
    $row = $select_query->result();
    return ($row);
	}

  function national_school_rating()
  {
    $this->output->enable_profiler(false);
    $month = 4;
    $year = 2016;
    $this->data['year'] = $month;
    $this->data['month'] = $year;

    $this->data['partial'] = 'reports_archive/national_school_rating';
    $this->data['page_title'] = 'National School Rating';
    $this->load->view('templates/index', $this->data); 
  }


  function national_school_rating_report()
  {
    if(!isset($_POST['start_month']) && !isset($_POST['start_year']))
    {
        redirect('reports_archive/national_school_rating');
    }
    $startmonth       = $_POST['start_month'];	   
	  $startyear        = $_POST['start_year'];
    $startdate        = '01/'.$startmonth.'/'.$startyear;
	  $startdate        = date('t/m/Y', strtotime($startyear.'-'.$startmonth.'-01'));

    $month_name = "";
    $prev_month1_name   = "";
    $prev_month2_name   = "";
		$cm_no 		= 0;
		$pv1_no 	= 0;
		$pv2_no 	= 0;
		$current_year1 = 0;
		$current_year2 = 0;
		
    switch($startmonth){

      case "1":
        $month_name = date('F', mktime(0, 0, 0, $startmonth, 10));
        $cm_no      = $startmonth;
        $prev_month1_name   = date('F', mktime(0, 0, 0, 12, 10));
        $pv1_no = 12;
        $current_year1 = $startyear - 1;
        $prev_month2_name   = date('F', mktime(0, 0, 0, 12 - $startmonth, 10));
        $pv2_no = 12 - $startmonth;
        $current_year2 = $startyear - 1;
        break;
      case "2":
        $month_name = date('F', mktime(0, 0, 0, $startmonth, 10));
        $cm_no      = $startmonth;
        $prev_month1_name   = date('F', mktime(0, 0, 0, $startmonth-1, 10));
        $pv1_no = $startmonth-1;
        $current_year1 = $startyear;
        $prev_month2_name   = date('F', mktime(0, 0, 0, 12, 10));
        $pv2_no = 12;
        $current_year2 = $startyear - 1;
        break;
      default:
        $month_name   = date('F', mktime(0, 0, 0, $startmonth, 10));
        $cm_no      = $startmonth;
        $prev_month1_name   = date('F', mktime(0, 0, 0, $startmonth-1, 10));
        $pv1_no = $startmonth-1;
        $current_year1 = $startyear;
        $prev_month2_name   = date('F', mktime(0, 0, 0, $startmonth-2, 10));
        $pv2_no = $startmonth-2;
        $current_year2 = $startyear;
    }
		
		$cm_result            = $this->getNationalRatingData($cm_no,$startyear);
		$pv1_result           = $this->getNationalRatingData($pv1_no,$current_year1);
		$pv2_result           = $this->getNationalRatingData($pv2_no,$current_year2);
    //print_r($pv2_result);die;
    
		$namestate = $this->getStatesName();
      
    //$this->data['trained'] = $trainedresults;
    $this->data['cmresults'] = $cm_result;
    $this->data['pv1results'] = $pv1_result;
    $this->data['pv2results'] = $pv2_result;
    $this->data['namestate'] = $namestate;
    $this->data['month_name'] = $month_name;
    $this->data['prev_month1_name'] = $prev_month1_name;
    $this->data['prev_month2_name'] = $prev_month2_name;
    $this->data['start_month'] = $startmonth;
    $this->data['start_year'] = $startyear;

    $download = (isset($_POST['download']) and $_POST['download'] == 1 ? $_POST['download'] : 0);
    $pdf = (isset($_POST['pdf']) and $_POST['pdf'] == 1 ? $_POST['pdf'] : 0);
    
		if ($download == 1)
    {
      $this->data['excel'] = 1;
      $myFile = "./uploads/reports_archive/national_school_rating_report.xls";
      $this->load->library('parser');
      $this->data['download'] = $download;
      //pass retrieved data into template and return as a string
      $stringData = $this->parser->parse('reports_archive/national_school_rating_report', $this->data, true);

      //open excel and write string into excel
      $fh = fopen($myFile, 'w') or die("can't open file");
      fwrite($fh, $stringData);
      fclose($fh);
      //download excel file
      $this->common_functions->downloadExcel("national_school_rating_report");
      unlink($myFile);
    }
    else if($pdf == 1)
    {
      //pass retrieved data into template and return as a string
      $this->data['pdf'] = 1;
      $summary = $this->load->view('reports_archive/national_school_rating_report',$this->data,true);
      $this->common_functions->create_pdf($summary,'national_school_rating_report');
    }
    else{
      $this->data['partial'] = 'reports_archive/national_school_rating_report'; 
      $this->data['page_title'] = 'National School Rating';
      $this->load->view('templates/index', $this->data);
    }
  }


  function getNationalRatingData($cm_no, $startyear)
  {
    $this->load->Model('Assessment_model');
    
    $assessment_data1 = $this->Assessment_model->getSparkRatingReport($cm_no, $startyear);
    
    $districts_arr = array();
    $report_ids = array();
    foreach($assessment_data1 as $assessments)
    {
      $state_id = $assessments->state_id;
      $report_id = $assessments->id;
      $school_id = $assessments->school_id;
      
      if(!isset($districts_arr[$state_id]))
      {
        $districts_arr[$state_id]['school_id'][] =  $school_id;
        $districts_arr[$state_id]['report_id'][] =  $report_id;
      }
        if(!in_array($school_id, $districts_arr[$state_id]['school_id']))
          array_push($districts_arr[$state_id]['school_id'], $school_id);
      
        if(!in_array($report_id, $districts_arr[$state_id]['report_id']))
          array_push($districts_arr[$state_id]['report_id'], $report_id);
      
    }
    
    $rating_data = array();
    foreach($districts_arr as $state_id=>$data)
    {
      $school_data = $data['school_id'];
      $school_count = count($data['school_id']);
      
      $report_id = $data['report_id'];
      
      $ratings_A = 0;
      $ratings_B = 0;
      $ratings_C = 0;
      $rating_count = 0;
      $ratingData = $this->Assessment_model->getSparkRatingCount($report_id);
      foreach($ratingData as $ratings){
        if($ratings['subject_rating'] == 'A'){
          $ratings_A = $ratings['Total'];
        }
        if($ratings['subject_rating'] == 'B'){
          $ratings_B = $ratings['Total'];
        }
        if($ratings['subject_rating'] == 'C'){
          $ratings_C = $ratings['Total'];
        }
        $rating_count = $ratings_A + $ratings_B + $ratings_C;
      }
      $rating_data[$state_id]['school_count'] = $school_count;
      $rating_data[$state_id]['rating_A'] = ($ratings_A > 0 ? round(($ratings_A/$rating_count)*100,2) : 0);
      $rating_data[$state_id]['rating_B'] = ($ratings_B > 0 ? round(($ratings_B/$rating_count)*100,2) : 0);
      $rating_data[$state_id]['rating_C'] = ($ratings_C > 0 ? round(($ratings_C/$rating_count)*100,2) : 0);
    }
    return $rating_data;
  }


	function return_months($start_date, $end_date){
	   
		$a = $start_date;
		$b = $end_date;

		$i = date("Y-m", strtotime($a));
		$months = array();
		while($i <= date("Y-m", strtotime($b))){
			$months[] = $i;
			if(substr($i, 5, 2) == "12")
				$i = (date("Y", strtotime($i."-01")) + 1)."-01";
			else
				$i++;
		}
		return $months;
	}
 
 
   function state_school_assessment()
  {
    $this->output->enable_profiler(false);
    $month = 4;
    $year = 2016;
    $this->data['year'] = $month;
    $this->data['month'] = $year;

    if($this->data['current_role']=="state_person"){
				
				$states = $this->states_model->get_by_id($this->data['user_state_id']);
    }
    else{
				
				$states = $this->states_model->get_all();
    }
    
    $this->data['states'] = $states;
    $this->data['partial'] = 'reports_archive/state_school_assessment';
    $this->data['page_title'] = 'State School Assessment';
    $this->load->view('templates/index', $this->data); 
  }


  function stateSchoolAssessmentReport()
  {
    if(!isset($_POST['start_month']) && !isset($_POST['start_year']))
    {
        redirect('reports_archive/state_school_assessment');
    }
    $state_id = $_POST['state_id'];
    $display_type  = $_POST['display_type'];
    $startmonth  = $_POST['start_month'];
    $startyear = $_POST['start_year'];
    $startdate = '01-'.$startmonth.'-'.$startyear;
    $enddate = date('t-m-Y', strtotime($startyear.'-'.$startmonth.'-01'));

    $this->load->Model('Assessment_model');
    $this->load->Model('Observations_model');
    $this->load->Model('Classes_model');
    $this->load->Model('Subjects_model');
    $this->load->Model('districts_model');

    $state_classes = $this->Subjects_model->check_mapping($state_id);
    
    $classes = array();
    $subject_ids = array();
    $class_subjects = array();
    $c = 0;
    foreach($state_classes as $state_class)
    {
        $class_id = $state_class->class_id;
        $subject_id = $state_class->subject_id;
        if(!in_array($class_id, $classes)){
          array_push($classes, $class_id);
        }
        
        if(!in_array($subject_id, $subject_ids)){
          array_push($subject_ids, $subject_id);
        }
        
        $subjectData = $this->Subjects_model->getById($subject_id);

        $class_subjects[$class_id][$c]['id'] = $subjectData['id'];
        $class_subjects[$class_id][$c]['name'] = $subjectData['name'];
        $c++;
    }
    
    $district_data = $this->districts_model->get_all_district($state_id);
    
    $observationsData = $this->Observations_model->getReportsObservation($state_id, $classes);
    
    $class_observation = array();
    $c = 0;
    foreach($observationsData as $observations)
    {
      $class_observation[$observations->class_id][$c]['id'] = $observations->id;
      $class_observation[$observations->class_id][$c]['question'] = $observations->question;
      $c++;
    }
    
    //echo"<br>".$startdate." >> ".$enddate." >> ".$state_id;
    //print_r($classes);
    $assessment_data = $this->Assessment_model->getStateClassAssessment($startdate, $enddate, $state_id, $classes);
    
    $district_ids = array();
    $resultData = array();
    $school_visit_counts = array();
    $class_subject_question_ans_counts = array();
    //print_r($assessment_data);
    foreach($assessment_data as $assessments)
    {   
        $district_id = $assessments->district_id;
        $class_id = $assessments->class_id;
        $subject_id = $assessments->subject_id;
        $school_id = $assessments->school_id;
          
        if(!isset($school_visit_counts[$district_id])){
          $school_visit_counts[$district_id] = array();
        }
        if(!in_array($school_id, $school_visit_counts[$district_id]))
            array_push($school_visit_counts[$district_id],$school_id);
        
        if(!in_array($district_id, $district_ids))
          array_push($district_ids, $district_id);
        
        $csq_key = $district_id.'_'.$class_id.'_'.$subject_id.'_'.$assessments->question_id;
        
        $ans_value = $assessments->answer;
        
        if(!isset($class_subject_question_ans_counts[$csq_key]['total']))
          $class_subject_question_ans_counts[$csq_key]['total'] = 1;
        else  
          $class_subject_question_ans_counts[$csq_key]['total'] = $class_subject_question_ans_counts[$csq_key]['total']+1;
        
        if($ans_value == 1){
          if(!isset($class_subject_question_ans_counts[$csq_key]['yes']))
            $class_subject_question_ans_counts[$csq_key]['yes'] = 1;
          else  
            $class_subject_question_ans_counts[$csq_key]['yes'] = $class_subject_question_ans_counts[$csq_key]['yes']+1;
        }
    }
    
    $class_names = array();
    if (count($classes) > 0)
    {
      $this->load->model('classes_model');
      $classData = $this->classes_model->getByIds($classes);
      foreach($classData as $class)
      {
        $class_names[$class->id] = $class->name;
      }
    }
    
    $district_data = array();
    if (count($district_ids) > 0)
    {
      $this->load->model('districts_model');
      $district_data = $this->districts_model->getByIds($district_ids);
      
    }
    
    $state_data = $this->states_model->getById($state_id);
    $period = date('M-y', strtotime($startdate));
    
    $this->data['district_names'] = $district_data;
    $this->data['classes'] = $classes;
    $this->data['class_names'] = $class_names;
    $this->data['class_subjects'] = $class_subjects;
    $this->data['class_observation'] = $class_observation;
    $this->data['district_school_visit'] = $school_visit_counts;
    $this->data['question_arr'] = $observationsData;
    $this->data['data_counts'] = $class_subject_question_ans_counts;
    
    $this->data['STATE'] = $state_data['name'];
    $this->data['PERIOD'] = $period;
    $this->data['state_id'] = $state_id;
    $this->data['start_month'] = $startmonth;
    $this->data['start_year'] = $startyear;
    $this->data['display_type'] = $display_type;
    
    $download = (isset($_POST['download']) and $_POST['download'] == 1 ? $_POST['download'] : 0);
    $pdf = (isset($_POST['pdf']) and $_POST['pdf'] == 1 ? $_POST['pdf'] : 0);

		if ($download == 1)
    {
        $this->data['excel'] = 1;
        $myFile = "./uploads/reports_archive/state_school_assessment_report.xls";
        $this->load->library('parser');
        $this->data['download'] = $download;
        //pass retrieved data into template and return as a string
        $stringData = $this->parser->parse('reports_archive/state_school_assessment_report', $this->data, true);

        //open excel and write string into excel
        $fh = fopen($myFile, 'w') or die("can't open file");
        fwrite($fh, $stringData);
        fclose($fh);
        //download excel file
        $this->common_functions->downloadExcel("state_school_assessment_report");
        unlink($myFile);
    }
    else if($pdf == 1)
    {
        //pass retrieved data into template and return as a string
        $this->data['pdf'] = 1;
        $summary = $this->load->view('reports_archive/state_school_assessment_report',$this->data,true);
        $this->common_functions->create_pdf($summary,'state_school_assessment_report');
    }
    else{
      $this->data['partial'] = 'reports_archive/state_school_assessment_report';
      $this->data['page_title'] = 'Spark School Rating';
      $this->load->view('templates/index', $this->data);
    }
  }
  
  
   function return_state_userids($state_id, $start_date, $end_date)
    {
      $users_data = $this->Users_model->getSparkByStateDuration($state_id, $start_date, $end_date);
      
      $user_ids = array();
      foreach($users_data as $usersdata){
        $user_ids[] = $usersdata['user_id'];
      }
      return $user_ids;  
    } 
  
    
  function state_training()
  {
    $month = 0;
    $year = 0;
    $this->data['year'] = $month;
    $this->data['month'] = $year;
    $this->data['partial'] = 'reports_archive/state';
    
    $this->load->Model('states_model');
    $this->load->Model('districts_model');
    
    if($this->data['current_role']=="state_person" || $this->data['current_role']=="manager"){
      $states = $this->states_model->get_by_id($this->data['user_state_id']);
      $this->data['states'] = $states;
      
      $this->load->model('districts_model');
      $districts = $this->districts_model->get_state_districts($this->data['user_state_id']);
      $this->data['districts'] = $districts;
    }
    else{
				
				$states = $this->states_model->get_all();
				$this->data['states'] = $states;
    }

    $this->data['title'] = 'State Productivity Report';
    $this->data['partial'] = 'reports_archive/state_training';
    $this->load->view('templates/index', $this->data);
  }
  
  function stateTrainingReport()
  {
    $state_id = $this->input->post('state_id');
    $inpmonth = $this->input->post('monthno');
    $inpyear = $this->input->post('year');
    
    if(empty($state_id) && empty($inpmonth) && empty($inpyear))
    {
        redirect('reports_archive/state_training');
    }
    
    $inpdate			 = '01/'.$inpmonth.'/'.$inpyear;
    $inpdate 		 = date('t/m/Y', strtotime($inpyear.'-'.$inpmonth.'-01'));

    $this->load->model('states_model');
    $this->load->model('Trainings_model');
    $this->load->model('Users_model');
    $this->load->model('districts_model');
    $this->load->model('blocks_model');

    $state_data = $this->states_model->getById($state_id);

    $start_date = date('Y-m-d', strtotime($inpyear.'-'.$inpmonth.'-01'));
    //$start_date1 = date('Y-m-d', strtotime($inpyear.'-'.($inpmonth-2).'-01'));
    $end_date = date('Y-m-t', strtotime($inpyear.'-'.$inpmonth.'-01'));
    if(strtotime($end_date) > now()){
      $end_date = date('Y-m-d');
    }
    //$users_data = $this->Users_model->getTrainingSparkByState($state_id, $start_date, $end_date);
    $users_data = $this->Users_model->getSparkByStateDuration($state_id, $start_date, $end_date);
    
    $_fromDate_C = date("Y-m-d",strtotime($inpyear."-".$inpmonth."-01"));
    $_toDate_C = date("Y-m-t",strtotime($inpyear."-".$inpmonth."-01"));
    
    $dates = $this->createRange($_fromDate_C, $_toDate_C);
    
    $i = 0;
    $user_names = array();
    $taining_data = array();
    $new_dates = array();
    $t = 0;
    foreach($users_data as $usersdata){
      
      /*$user_id = $usersdata->user_id;
      $login_id = $usersdata->login_id;
      $name = $usersdata->name;*/
      $user_id = $usersdata['user_id'];
      $login_id = $usersdata['login_id'];
      $name = $usersdata['name'];
      //$user_names[]['name'] = $name;
      
      foreach($dates as $activity_date){
        $user_training = $this->Trainings_model->get_fullday_training($user_id, $activity_date, $activity_date);
        if(empty($user_training)){
          $taining_data[$user_id][$activity_date] = 0;
          $new_dates[$activity_date] = (!empty($new_dates[$activity_date]) ? 0+$new_dates[$activity_date] : 0); 
        }
        else{
            $new_dates[$activity_date] = (!empty($new_dates[$activity_date]) ? 1+$new_dates[$activity_date] : 1); 
            $details = array();  
            $statedata = $this->states_model->getById($user_training[0]->state_id);
            $details[] = $statedata['name'];  
            //$details[] = ($user_training[0]->training_duration == '24:00' ? 'Full Day' : 'Half Day');
            if($user_training[0]->district_id != 0){
              $districtdata = $this->districts_model->getById($user_training[0]->district_id);
              $details[] = $districtdata['name'];  
            }
            if($user_training[0]->block_id != 0){
              $blockdata = $this->blocks_model->getById($user_training[0]->block_id);
              $details[] = $blockdata['name'];  
            }
            $taining_data[$user_id][$activity_date] = implode(', ', $details);  
            $t++;
        }
      }
    }
    //echo"<br>count : ".$t;
    //print_r($users_data);
    //print_r($users_data);
    //die;
    $month_name = date('F', mktime(0, 0, 0, $inpmonth, 10));
    
    $this->data['net_count'] = $t;
    $this->data['new_dates'] = $new_dates;
    $this->data['monthno'] = $inpmonth;
    $this->data['year'] = $inpyear;
    $this->data['state_id'] = $state_id;
    $this->data['sparks'] = $users_data;
    $this->data['dates'] = $dates;
    $this->data['training_data'] = $taining_data;
    $this->data['state_name'] = $state_data['name'];
    $this->data['current_month_name'] = $month_name;
    
    $download = (isset($_POST['download']) and $_POST['download'] == 1 ? $_POST['download'] : 0);
    $pdf = (isset($_POST['pdf']) and $_POST['pdf'] == 1 ? $_POST['pdf'] : 0);
    
		if ($download == 1)
    {
      $myFile = "./uploads/reports_archive/state_training_report.xls";
      $this->load->library('parser');
			$this->data['download'] = $download;
      //pass retrieved data into template and return as a string
      $stringData = $this->parser->parse('reports_archive/state_training_report', $this->data, true);
      //open excel and write string into excel
      $fh = fopen($myFile, 'w') or die("can't open file");
      fwrite($fh, $stringData);
      fclose($fh);
      //download excel file
      $this->common_functions->downloadExcel("state_training_report");
      unlink($myFile);
    }
    else if($pdf == 1)
    {
        //pass retrieved data into template and return as a string
        $this->data['pdf'] = 1;
        $summary = $this->load->view('reports_archive/state_training_report',$this->data,true);
        $this->common_functions->create_pdf($summary,'state_training_report');
    }
    else{
      $this->data['partial'] = 'reports_archive/state_training_report';
      $this->data['page_title'] = 'Sparks State Training Report';
      $this->load->view('templates/index', $this->data);
    }
  }
  
  function createRange($start, $end, $format = 'Y-m-d') {
    $start  = new DateTime($start);
    $end    = new DateTime($end);
    $invert = $start > $end;

    $dates = array();
    $dates[] = $start->format($format);
    while ($start != $end) {
        $start->modify(($invert ? '-' : '+') . '1 day');
        $dates[] = $start->format($format);
    }
    return $dates;
  }
  
  function testline_responses()
  {
    $this->load->Model('Test_questions_model');
    
    $district_id = $this->data['user_district_id'];
    $login_id = $this->data['current_user_id'];
    $state_id = $this->data['user_state_id'];
    $role = $this->data['current_role'];
    
    $spark_ids = array();
    $sel_state = '';
    if($this->data['current_role']=="field_user")
		{
      $spark_ids = array($login_id);
			//$response_data = $this->Test_questions_model->get_all_testline_response_data('', $spark_ids);
		}
    else if($this->data['current_role'] == "manager")
		{
      $this->load->Model('Team_members_model');
      $userData = $this->Team_members_model->get_team($login_id);
      if(count($userData) > 0){
        foreach($userData as $users){
          if(!in_array($users->id,$spark_ids)){
            $spark_ids[] = $users->id;
          }
        }
      }
			//$response_data = $this->Test_questions_model->get_all_testline_response_data('', $spark_ids);
		}
		else if($this->data['current_role']=="state_person")
		{ 
      $sel_state = $state_id; 
      //$response_data = $this->Test_questions_model->get_all_testline_response_data($state_id, '');
    }
    
    $questions_data = $this->Test_questions_model->get_all_testline_questions_data($sel_state, $spark_ids);
    $response_data = $this->Test_questions_model->get_all_testline_response_data($sel_state, $spark_ids);
    
    //print_r($response_data);
    $classes = array();
    $classes_subjects = array();
    $classes_question_ids = array();
    $classes_subjects_question_ids = array();
    $classes_subjects_questions = array();
    foreach($questions_data as $questions)
    {
      if(!in_array($questions->class_name, $classes))
        array_push($classes, $questions->class_name);
      
      if(!isset($classes_subjects[$questions->class_name])){
        $classes_subjects[$questions->class_name] = array();
      }
      if(!in_array($questions->subject_name, $classes_subjects[$questions->class_name]))
       array_push($classes_subjects[$questions->class_name],$questions->subject_name);    
       
      $classes_question_ids[$questions->class_name][] = $questions->question_id;    
      
      $classes_subjects_question_ids[$questions->class_name][$questions->subject_name][] = $questions->question_id;    
      $classes_subjects_questions[$questions->class_name][$questions->subject_name][] = $questions->question;    
    }
    
    $this->data['response_data'] = $response_data;
    $this->data['classes'] = $classes;
    $this->data['classes_subjects'] = $classes_subjects;
    $this->data['classes_question_ids'] = $classes_question_ids;
    $this->data['classes_subjects_question_ids'] = $classes_subjects_question_ids;
    
    $download = 1;
    if ($download == 1)
    {
      $myFile = "./uploads/reports_archive/test_question_responses.xls";
      $this->load->library('parser');
			$this->data['download'] = $download;
      //pass retrieved data into template and return as a string
      $stringData = $this->parser->parse('reports_archive/test_question_response_report', $this->data, true);
      //open excel and write string into excel
      $fh = fopen($myFile, 'w') or die("can't open file");
      fwrite($fh, $stringData);
      fclose($fh);
      //download excel file
      $this->common_functions->downloadExcel("test_question_responses");
      unlink($myFile);
    }
    else{
      $this->data['partial'] = 'reports_archive/test_question_response_report';
      $this->data['page_title'] = 'Sparks State Training Report';
      $this->load->view('templates/index', $this->data);
    }
      
  }
  
}
