<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Meeting extends CI_Controller {

	function __construct()	
	{
		parent::__construct();
		$this->load->helper('url');
		$this->data['current_page'] = 'meeting';
		$this->load->model('meetings_model');
		$this->load->library('form_validation');
		$this->load->library('common_functions');
		$this->load->helper('date');
    if($this->session->userdata('logged_in'))
		{
      $session_data = $this->session->userdata('logged_in');
      $this->data['current_user_id'] = $session_data['id'];
      $this->data['current_username'] = $session_data['username'];
      $this->data['current_name'] = $session_data['name'];
      $this->data['current_role'] = $session_data['role'];
      $this->data['user_state_id'] = $session_data['state_id'];
      $this->data['user_district_id'] = $session_data['district_id'];
      
    }
    else
    {
      //If no session, redirect to login page
      redirect('login', 'refresh');
    }
    
    $method_name = $this->uri->segment(2);
    if ($method_name != "show" && $method_name != "spark_meeting_report")
    {
      $activity_date = $this->session->userdata('activity_date');
      if ($activity_date == "")
      {
        $this->session->set_flashdata('alert', 'Select date first');
        redirect('dashboard', 'refresh');
      }
      else
      {
        $this->data['activity_date'] = $activity_date;
      }
    }
    $this->data['month_arr'] = unserialize(MONTH_ARRAY);
    $this->data['year_arr'] = unserialize(YEAR_ARRAY);
	}
  
  function show()
  {
    $id = $this->uri->segment(3);
    
    $detail = $this->meetings_model->getById($id);
    
		$this->load->model('states_model');
		$this->load->model('districts_model');
		$this->load->model('blocks_model');
		$this->load->model('clusters_model');
    
    if ($detail['location'] == "state" || $detail['location'] == "district" || $detail['location'] == "block" || $detail['location'] == "cluster")
    {
      $state = $this->states_model->getById($detail['state_id']);
      $this->data['state'] = $state;
    }
    
    if ($detail['location'] == "district" || $detail['location'] == "block" || $detail['location'] == "cluster")
    {
      $district = $this->districts_model->getById($detail['district_id']);
      $this->data['district'] = $district;
    }
    
    if ($detail['location'] == "block" || $detail['location'] == "cluster")
    {
      $block = $this->blocks_model->getById($detail['block_id']);
      $this->data['block'] = $block;
    }
    
    if ($detail['location'] == "cluster")
    {
      $cluster = $this->clusters_model->getById($detail['cluster_id']);
      $this->data['cluster'] = $cluster;
    }
    
    $this->data['partial'] = 'meeting/show';
    $this->data['detail'] = $detail;
    
    $this->load->view('templates/blank', $this->data);    
  }
  
  function index()
  {
    $this->data['partial'] = 'meeting/index';
    $this->data['page_title'] = 'Meeting';
    $this->load->view('templates/index', $this->data);    
  }
  
  function internal()
  {    
    $this->load->model('states_model');
    $this->data['states'] = $this->states_model->get_all_states($this->data['user_state_id']);
    $this->data['partial'] = 'meeting/internal';
    $this->data['page_title'] = 'Meeting';
    $this->load->view('templates/index', $this->data);    
  }
  
  function internal_submit()
  {    
    $mdata['activity_date'] = $this->data['activity_date'];
    //$mdata['meeting_type'] = "internal";
    $mdata['user_id'] = $this->data['id'];
    
    $location = $this->input->post('location');
    $mdata['location'] = $location;
    if ($location == "ho")
    {
      $mdata['state_id'] = 0;
      $mdata['district_id'] = 0;
    }
    if ($location == "state")
    {
      $mdata['state_id'] = $this->input->post('state_id');
      $mdata['district_id'] = 0;
    }
    if ($location == "district")
    {
      $mdata['state_id'] = $this->input->post('state_id');
      $mdata['district_id'] = $this->input->post('district_id');
    }
   // $mdata['meet_with'] = $this->input->post('designation');
   // $mdata['contact_person_name'] = $this->input->post('contact_person_name');
   // $mdata['contact_person_number'] = $this->input->post('contact_person_number');
   // $mdata['subject'] = $this->input->post('subject');
   // $mdata['agenda'] = $this->input->post('agenda');
   // $mdata['remarks'] = $this->input->post('remarks');
    
    $this->meetings_model->add($mdata);
        
    $this->session->set_flashdata('alert', 'Internal Meeting has been marked for '.strftime("%d-%b-%Y",strtotime($this->data['activity_date'])));
    redirect('select_activity', 'refresh');
  }
  
  function external_submit()
  {
    $mdata['activity_date'] = $this->data['activity_date'];
    //$mdata['meeting_type'] = "external";
    $mdata['user_id'] = $this->data['id'];
    
    $location = $this->input->post('location');
    $mdata['location'] = $location;
    if ($location == "state")
    {
    //  $mdata['meet_with'] = $this->input->post('state_person');
      $mdata['state_id'] = $this->input->post('state_id');
      $mdata['district_id'] = 0;
      $mdata['block_id'] = 0;
      $mdata['cluster_id'] = 0;
    }
    if ($location == "district")
    {
     // $mdata['meet_with'] = $this->input->post('district_person');
      $mdata['state_id'] = $this->input->post('state_id');
      $mdata['district_id'] = $this->input->post('district_id');
      $mdata['block_id'] = 0;
      $mdata['cluster_id'] = 0;
    }
    if ($location == "block")
    {
      //$mdata['meet_with'] = $this->input->post('block_person');
      $mdata['state_id'] = $this->input->post('state_id');
      $mdata['district_id'] = $this->input->post('district_id');
      $mdata['block_id'] = $this->input->post('block_id');
      $mdata['cluster_id'] = 0;
    }
    if ($location == "cluster")
    {
     // $mdata['meet_with'] = $this->input->post('cluster_person');
      $mdata['state_id'] = $this->input->post('state_id');
      $mdata['district_id'] = $this->input->post('district_id');
      $mdata['block_id'] = $this->input->post('block_id');
      $mdata['cluster_id'] = $this->input->post('cluster_id');
    }
    
//    if (isset($mdata['meet_with']) and $mdata['meet_with'] == "others")
//    {
//      $mdata['meet_with'] = $this->input->post('designation');
//    }
//    if ($location == "HO")
//    {
//      $mdata['meet_with'] = $this->input->post('designation');
//    }
//    $mdata['contact_person_name'] = $this->input->post('contact_person_name');
//    $mdata['meeting_mode'] = $this->input->post('meeting_mode');
//    $mdata['contact_person_number'] = $this->input->post('contact_person_number');
//    $mdata['subject'] = $this->input->post('subject');
//    $mdata['agenda'] = $this->input->post('agenda');
//    $mdata['remarks'] = $this->input->post('remarks');
//    
    $this->meetings_model->add($mdata);
        
    $this->session->set_flashdata('alert', 'External Meeting has been marked for '.strftime("%d-%b-%Y",strtotime($this->data['activity_date'])));
    redirect('select_activity', 'refresh');
  }
  
  function external()
  {
    $this->load->model('states_model');
    $this->data['states'] = $this->states_model->get_all_states($this->data['user_state_id']);
    $this->data['partial'] = 'meeting/external';
    $this->data['page_title'] = 'Meeting';
    $this->load->view('templates/index', $this->data);    
  }

  function spark_meeting_report()
  {
    $this->output->enable_profiler(false);
    $month = date('m');
    $year = date('Y');
    $this->data['start_year'] = $month;
    $this->data['start_month'] = $year;

    if(!empty($_POST))
    {
      if(empty($_POST['start_month']) || empty($_POST['start_year']))
      {
          redirect('meeting/meeting_report');
      }
      $startmonth       = $_POST['start_month'];	   
      $startyear        = $_POST['start_year'];
      $duration        = $_POST['duration'];
      
      $download = (!empty($_POST['download']) ? $_POST['download'] : 0); 
      $pdf = (!empty($_POST['pdf']) ? $_POST['pdf'] : 0); 
       
      if(!empty($_POST['state_id']) && !empty($_POST['district_id'])  && !empty($_POST['block_id']))
      {
        $this->get_meeting_report('block', $duration, $startmonth, $startyear, $download, $pdf, $_POST['state_id'], $_POST['district_id'], $_POST['block_id']);
      }
      if(!empty($_POST['state_id']) && !empty($_POST['district_id'])  && empty($_POST['block_id']))
      {
        $this->get_meeting_report('district', $duration, $startmonth, $startyear,$download, $pdf, $_POST['state_id'], $_POST['district_id']);
      }
      if(!empty($_POST['state_id']) && empty($_POST['district_id'])  && empty($_POST['block_id']))
      {
        $this->get_meeting_report('state', $duration, $startmonth, $startyear, $download, $pdf, $_POST['state_id']);
      }
      if(empty($_POST['state_id']) && empty($_POST['district_id'])  && empty($_POST['block_id']))
      {
        redirect('meeting/meeting_report');
      }
    }
    else{
      $this->load->Model('states_model');
      $states = array();
      if($this->data['current_role']=="field_user"){
        $this->load->Model('Users_model');
        $districts = array();
        $districtData = $this->Users_model->user_districts($this->data['current_user_id']);
        foreach($districtData as $district){
          $districts[$district->id] = $district->name;
        }
        $this->data['districts'] = $districts;
      }
      else if($this->data['current_role']=="state_person" || $this->data['current_role']=="manager"){
          $states = $this->states_model->get_by_id($this->data['user_state_id']);
      }
      else {
          $states = $this->states_model->get_all();
      }
      $this->data['states'] = $states;
      $this->data['partial'] = 'meeting/meeting_report';
      $this->data['page_title'] = 'Block School Observation';
      $this->load->view('templates/index', $this->data);
    }
  }

  function get_meeting_report($report_type, $duration, $startmonth, $startyear, $download, $pdf, $state_id, $district_id='', $block_id='')
  {
    $sess_month = SESSION_START_MONTH;
    $sess_year = date('Y');
    if($startmonth < $sess_month){
      $sess_year = date('Y') - 1;
    }
    else if($startyear < $sess_year)
    {
			$sess_year = $startyear;
		}
		
    $session_start_date = $sess_year."-".$sess_month."-01";
    
    $startdate        = ($duration == 'YTD' ? $session_start_date : $startyear."-".$startmonth."-01");
	  $enddate        = date('Y-m-t', strtotime($startyear.'-'.$startmonth.'-01'));
	  $end_date        = date('t-m-Y', strtotime($startyear.'-'.$startmonth.'-01'));
    
    $month_name   = date('F', mktime(0, 0, 0, $startmonth, 10));
    
    $this->data['startdate'] = $startdate;
    $this->data['enddate'] = $end_date;
    $this->data['duration'] = $duration;
    $this->data['start_month'] = $startmonth;
    $this->data['start_year'] = $startyear;
    $this->data['month_name'] = $month_name;
    
    $this->data['state_id'] = $state_id;
    $this->data['district_id'] = $district_id;
    $this->data['block_id'] = $block_id;
    
    $this->load->Model('Dashboard_model');
    
    $spark_ids = array();
    if($this->data['current_role']  == 'field_user')
    {
      $spark_ids[] = $this->data['current_user_id'];
    }
    else{
      $sparkData = $this->Dashboard_model->getAllSparkByStateDuration($state_id, $startdate, $enddate, array('field_user'));
    
      $spark_ids = array();
      foreach($sparkData as $sparks)
      {
        if(!in_array($sparks->user_id, $spark_ids))
          array_push($spark_ids, $sparks->user_id);
      } 
    }  
    
    $meeting_data = $this->meetings_model->get_internal_meetings($spark_ids, $startdate, $enddate, $state_id, $district_id, $block_id);
    //print_r($meeting_data);
    
    $user_ids = array();
    $district_ids = array();
    $block_ids = array();
    $cluster_ids = array();
    $DM	= array('DM', 'DM&C');
    $DEO_DRC	= array('DEO', 'DMC/DEO/BSA/CEO');	
    $BEO_BRC	= array('BRC', 'BRC/BRP/BEO');
    $CRC	    = array('CRC', 'CRC/CRP/CSE');
    $Others   = 0;
    $meet_with  = array();
    $user_arr  = array();
    $meet_others = array();
    
    foreach($meeting_data as $meetings)
    {
      if(!in_array($meetings->user_id, $user_ids))
        array_push($user_ids, $meetings->user_id);
      
      if($report_type == 'block'){
        $meet_loc = $meetings->cluster_id;
        
        if(!in_array($meetings->cluster_id, $cluster_ids))
          array_push($cluster_ids, $meetings->cluster_id);
      }
      else if($report_type == 'district'){
        $meet_loc = $meetings->block_id;
        
        if(!in_array($meetings->block_id, $block_ids))
          array_push($block_ids, $meetings->block_id);
      }
      else if($report_type == 'state'){
        $meet_loc = $meetings->district_id;
        
        if(!in_array($meetings->district_id, $district_ids))
          array_push($district_ids, $meetings->district_id);
      }
      
      if(!isset($user_arr[$meet_loc])){
        $user_arr[$meet_loc][] = $meetings->user_id;
      }
      else{
        if(!in_array($meetings->user_id, $user_arr[$meet_loc]))
          array_push($user_arr[$meet_loc], $meetings->user_id);  
      }
      
      if(!isset($meet_with[$meet_loc]))
      {
        if(in_array($meetings->meet_with,$DM)) 
          $meet_with[$meet_loc]['DM'] = 1;
        else if(in_array($meetings->meet_with,$DEO_DRC)) 
          $meet_with[$meet_loc]['DEO_DRC'] = 1;
        else if(in_array($meetings->meet_with,$BEO_BRC)) 
          $meet_with[$meet_loc]['BEO_BRC'] = 1;
        else if(in_array($meetings->meet_with,$CRC)) 
          $meet_with[$meet_loc]['CRC'] = 1;
        else{ 
          $meet_with[$meet_loc]['Others'] = 1;
          if(!in_array($meetings->meet_with ,$meet_others))
            array_push($meet_others, $meetings->meet_with);
        }
      }
      else{
        if(in_array($meetings->meet_with,$DM)){ 
          if(!isset($meet_with[$meet_loc]['DM']))
            $meet_with[$meet_loc]['DM'] = 1;
          else
            $meet_with[$meet_loc]['DM']++;
        }
        else if(in_array($meetings->meet_with,$DEO_DRC)) {
          if(!isset($meet_with[$meet_loc]['DEO_DRC']))
            $meet_with[$meet_loc]['DEO_DRC'] = 1;
          else
            $meet_with[$meet_loc]['DEO_DRC']++;
        }
        else if(in_array($meetings->meet_with,$BEO_BRC)) {
          if(!isset($meet_with[$meet_loc]['BEO_BRC']))
            $meet_with[$meet_loc]['BEO_BRC'] = 1;
          else
            $meet_with[$meet_loc]['BEO_BRC']++;
        }
        else if(in_array($meetings->meet_with,$CRC)) {
          if(!isset($meet_with[$meet_loc]['CRC']))
            $meet_with[$meet_loc]['CRC'] = 1;
          else
            $meet_with[$meet_loc]['CRC']++;
        }
        else {
          if(!in_array($meetings->meet_with ,$meet_others))
            array_push($meet_others, $meetings->meet_with);
          if(!isset($meet_with[$meet_loc]['Others']))
            $meet_with[$meet_loc]['Others'] = 1;
          else
            $meet_with[$meet_loc]['Others']++;
        }
      }
    }
    
    //print_r($meet_with);
    
    $data_arr = array();
    $type_values = array();
    if($report_type == 'block'){
      $this->load->Model('clusters_model');
      if(!empty($cluster_ids))
        $data_arr = $this->clusters_model->getByIds($cluster_ids);
      
      $this->data['column_name'] = 'Cluster';
    }
    else if($report_type == 'district'){
      $this->load->Model('blocks_model');
      if(!empty($block_ids))
        $data_arr = $this->blocks_model->getByIds($block_ids);
      
      $this->data['column_name'] = 'Block';
    }
    else if($report_type == 'state'){
      $this->load->Model('districts_model');
      if(!empty($district_ids))
        $data_arr = $this->districts_model->getByIds($district_ids);
      
      $this->data['column_name'] = 'District';
    }
    
    if(!empty($data_arr)){
      foreach($data_arr as $data){
        $type_values[$data->id] = $data->name;
      }
      if(!empty($meet_with[0]))
        $type_values[0] = 'Others';
    }
    
    $this->load->Model('Users_model');
    
    $user_names = array();
    if(!empty($user_ids)){
      $user_data = $this->Users_model->getByIds($user_ids);
      foreach($user_data as $data){
        $user_names[$data->id] = $data->name;
      }
    }
    
    $this->data['meet_others'] = $meet_others;
    $this->data['user_names'] = $user_names;
    $this->data['user_arr'] = $user_arr;
    $this->data['type_values'] = $type_values;
    $this->data['meet_with'] = $meet_with;
    
    if ($download == 1)
    {
      $myFile = "./uploads/reports/meeting_report.xls";
      $this->load->library('parser');
			$this->data['download'] = $download;
      //pass retrieved data into template and return as a string
      $stringData = $this->parser->parse('meeting/meeting_report_data', $this->data, true);
      //open excel and write string into excel
      $fh = fopen($myFile, 'w') or die("can't open file");
      fwrite($fh, $stringData);
      fclose($fh);
      //download excel file
      $this->common_functions->downloadExcel("meeting_report");
      unlink($myFile);
    }
    else if($pdf == 1)
    {
        //pass retrieved data into template and return as a string
        $this->data['pdf'] = 1;
        $summary = $this->load->view('meeting/meeting_report_data',$this->data,true);
        $this->common_functions->create_pdf($summary,'meeting_report');
    }
    else{
      $this->data['partial'] = 'meeting/meeting_report_data';
      $this->data['page_title'] = 'Meeting Data';
      $this->load->view('templates/index', $this->data);
    }
    
  }
  

}
