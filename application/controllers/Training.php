<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Training extends CI_Controller {

	function __construct()	
	{
		parent::__construct();
		$this->load->helper('url');
		$this->data['current_page'] = 'training';
		$this->load->model('trainings_model');
		$this->load->library('form_validation');
		$this->load->library('common_functions');
		$this->load->helper('date');
    if($this->session->userdata('logged_in'))
		{
      $session_data = $this->session->userdata('logged_in');
      $this->data['current_user_id'] = $session_data['id'];
      $this->data['current_username'] = $session_data['username'];
      $this->data['current_name'] = $session_data['name'];
      $this->data['current_role'] = $session_data['role'];
      $this->data['user_state_id'] = $session_data['state_id'];
      $this->data['user_district_id'] = $session_data['district_id'];      
    }
    else
    {
      //If no session, redirect to login page
      redirect('login', 'refresh');
    }
    
    $method_name = $this->uri->segment(2);
    if ($method_name != "show" && $method_name != "training_quality" && $method_name != "training_logistic")
    {
      $activity_date = $this->session->userdata('activity_date');
      if ($activity_date == "")
      {
        $this->session->set_flashdata('alert', 'Select date first');
        redirect('dashboard', 'refresh');
      }
      else
      {
        $this->data['activity_date'] = $activity_date;
      }
    }
    $this->data['month_arr'] = unserialize(MONTH_ARRAY);
    $this->data['year_arr'] = unserialize(YEAR_ARRAY);
	}
  
  function show()
  {
    $id = $this->uri->segment(3);
    
    $detail = $this->trainings_model->getById($id);
    
		$this->load->model('subjects_model');
    $subject = $this->subjects_model->getById($detail['subject_id']);
    
		$this->load->model('states_model');
		$this->load->model('districts_model');
		$this->load->model('blocks_model');
		$this->load->model('clusters_model');
    
    $state = $this->states_model->getById($detail['state_id']);
    $district = $this->districts_model->getById($detail['district_id']);
    $block = $this->blocks_model->getById($detail['block_id']);
    $cluster = $this->clusters_model->getById($detail['cluster_id']);
    
    $this->data['partial'] = 'training/show';
    $this->data['detail'] = $detail;
    $this->data['subject'] = $subject;
    $this->data['state'] = $state;
    $this->data['district'] = $district;
    $this->data['block'] = $block;
    $this->data['cluster'] = $cluster;
    $this->load->view('templates/blank', $this->data);    
  }
  
  function index()
  {
    $this->data['partial'] = 'training/index';
    $this->data['page_title'] = 'Training';
    $this->load->view('templates/index', $this->data);    
  }
  
  function internal()
  {    
    $this->load->model('subjects_model');
    $subjects = $this->subjects_model->get_all();
    $this->data['subjects'] = $subjects;
    
    $this->load->model('states_model');
    $this->data['states'] = $this->states_model->get_all_states($this->data['user_state_id']);
    $this->data['partial'] = 'training/internal';
    $this->data['page_title'] = 'Training';
    $this->load->view('templates/index', $this->data);    
    
    //$mdata['activity_date'] = $this->data['activity_date'];
    //$mdata['training_type'] = "internal";
    //$mdata['user_id'] = $this->data['id'];
    
    //$this->trainings_model->add($mdata);
        
    //$this->session->set_flashdata('alert', 'Internal Training has been marked for '.strftime("%d-%b-%Y",strtotime($this->data['activity_date'])));
    //redirect('select_activity', 'refresh');
  }
  
  function external_submit()
  {
    $date_counts = array_count_values($_POST['training_date']);
    foreach($date_counts as $date_count)
    {
      if ($date_count > 1)
      {
        $this->session->set_flashdata('alert', 'Training Dates should be unique');
        redirect('training/external', 'refresh');
        exit();
      }
    }
    
    foreach($_POST['training_date'] as $training_date)
    {
      $training_date = strftime("%Y-%m-%d",strtotime($training_date));
      
      $mdata['activity_date'] = $training_date;
      $mdata['training_type'] = "external";
      $mdata['user_id'] = $this->data['id'];
      
      $mdata['location'] = $this->input->post('location');
      $mdata['state_id'] = $this->input->post('state_id');
      $mdata['district_id'] = $this->input->post('district_id');
      $mdata['block_id'] = $this->input->post('block_id');
      $mdata['training_duration'] = $this->input->post('training_duration');
     // $mdata['cluster_id'] = $this->input->post('cluster_id');
      //$mdata['no_of_days'] = 1;
      $mdata['no_of_teachers_trained'] = $this->input->post('no_of_teachers_trained');
      $mdata['subject_id'] = $this->input->post('subject_id');
     // $mdata['topic'] = $this->input->post('topic');
     // $mdata['agenda'] = $this->input->post('agenda');
     // $mdata['role'] = $this->input->post('role');
      
      $this->trainings_model->add($mdata);
    }
    
    $this->session->set_flashdata('alert', 'External Training has been marked for '.implode(", ",$_POST['training_date']));
    redirect('select_activity', 'refresh');
  }
  
  function internal_submit()
  {
    $date_counts = array_count_values($_POST['training_date']);
    foreach($date_counts as $date_count)
    {
      if ($date_count > 1)
      {
        $this->session->set_flashdata('alert', 'Training Dates should be unique');
        redirect('training/internal', 'refresh');
        exit();
      }
    }
    
    foreach($_POST['training_date'] as $training_date)
    {
      $training_date = strftime("%Y-%m-%d",strtotime($training_date));
      
      $mdata['activity_date'] = $training_date;
      $mdata['training_type'] = "internal";
      $mdata['duration'] = "duration";
      $mdata['user_id'] = $this->data['id'];
      
      $mdata['location'] = $this->input->post('location');
      $mdata['state_id'] = $this->input->post('state_id');
      $mdata['district_id'] = $this->input->post('district_id');
      $mdata['block_id'] = $this->input->post('block_id');
      $mdata['training_duration'] = $this->input->post('training_duration');
     // $mdata['cluster_id'] = $this->input->post('cluster_id');
     // $mdata['no_of_days'] = 1;
      $mdata['no_of_teachers_trained'] = $this->input->post('no_of_teachers_trained');
      $mdata['subject_id'] = $this->input->post('subject_id');
      //$mdata['topic'] = $this->input->post('topic');
      //$mdata['agenda'] = $this->input->post('agenda');
     // $mdata['role'] = $this->input->post('role');
      
      $this->trainings_model->add($mdata);
    }
    
    $this->session->set_flashdata('alert', 'Internal Training has been marked for '.implode(", ",$_POST['training_date']));
    redirect('select_activity', 'refresh');
  }
  
  function external()
  {    
    $this->load->model('subjects_model');
    $subjects = $this->subjects_model->get_all();
    $this->data['subjects'] = $subjects;
    
    $this->load->model('states_model');
    $this->data['states'] = $this->states_model->get_all_states($this->data['user_state_id']);
    $this->data['partial'] = 'training/external';
    $this->data['page_title'] = 'Training';
    $this->load->view('templates/index', $this->data);    
  }

  
  function training_logistic()
  {
    $this->output->enable_profiler(false);
    $month = date('m');
    $year = date('Y');
    $this->data['start_year'] = $month;
    $this->data['start_month'] = $year;

    if(!empty($_POST))
    {
      if(empty($_POST['start_month']) || empty($_POST['start_year']))
      {
          redirect('training/training_subject_report');
      }
      $startmonth       = $_POST['start_month'];	   
      $startyear        = $_POST['start_year'];
      $duration        = $_POST['duration'];
      
      $download = (!empty($_POST['download']) ? $_POST['download'] : 0); 
      $pdf = (!empty($_POST['pdf']) ? $_POST['pdf'] : 0); 
       
      if(!empty($_POST['state_id']) && !empty($_POST['district_id'])  && !empty($_POST['block_id']))
      {
        $this->get_training_subject_report('block', $duration, $startmonth, $startyear, $download, $pdf, $_POST['state_id'], $_POST['district_id'], $_POST['block_id']);
      }
      if(!empty($_POST['state_id']) && !empty($_POST['district_id'])  && empty($_POST['block_id']))
      {
        $this->get_training_subject_report('district', $duration, $startmonth, $startyear,$download, $pdf, $_POST['state_id'], $_POST['district_id']);
      }
      if(!empty($_POST['state_id']) && empty($_POST['district_id'])  && empty($_POST['block_id']))
      {
        $this->get_training_subject_report('state', $duration, $startmonth, $startyear, $download, $pdf, $_POST['state_id']);
      }
      if(empty($_POST['state_id']) && empty($_POST['district_id'])  && empty($_POST['block_id']))
      {
        redirect('training/training_subject_report');
      }
    }
    else{
      $this->load->Model('states_model');
      $states = array();
      if($this->data['current_role']=="field_user"){
        $this->load->Model('Users_model');
        $districts = array();
        $districtData = $this->Users_model->user_districts($this->data['current_user_id']);
        foreach($districtData as $district){
          $districts[$district->id] = $district->name;
        }
        $this->data['districts'] = $districts;
      }
      else if($this->data['current_role']=="state_person" || $this->data['current_role']=="manager"){
          $states = $this->states_model->get_by_id($this->data['user_state_id']);
      }
      else{
          $states = $this->states_model->get_all();
      }
      $this->data['duration'] = 'monthly';
      $this->data['states'] = $states;
      $this->data['partial'] = 'training/training_subject_report';
      $this->data['page_title'] = 'Training Observation';
      $this->load->view('templates/index', $this->data);
    }
  }

  function get_training_subject_report($report_type, $duration, $startmonth, $startyear, $download, $pdf, $state_id, $district_id='', $block_id='')
  {
    $sess_month = SESSION_START_MONTH;
    $sess_year = date('Y');
    if($startmonth < $sess_month){
      $sess_year = date('Y') - 1;
    }
    else if($startyear < $sess_year)
    {
			$sess_year = $startyear;
		}

    $session_start_date = $sess_year."-".$sess_month."-1";
    
    $startdate        = ($duration == 'YTD' ? $session_start_date : $startyear."-".$startmonth."-1");
	  $enddate        = date('Y-m-t', strtotime($startyear.'-'.$startmonth.'-01'));
    
    $month_name   = date('F', mktime(0, 0, 0, $startmonth, 10));
    
    $this->data['startdate'] = $startdate;
    $this->data['enddate'] = $enddate;
    $this->data['duration'] = $duration;
    $this->data['start_month'] = $startmonth;
    $this->data['start_year'] = $startyear;
    $this->data['month_name'] = $month_name;
    
    $this->data['state_id'] = $state_id;
    $this->data['district_id'] = $district_id;
    $this->data['block_id'] = $block_id;
    
    $report_observations = $this->trainings_model->get_training_observations($state_id, 1);
    
    //print_r($report_observations);
    
    $question_ids = array();
    $observations = array();
    
    $q=1;
    foreach($report_observations as $reportobservations)
    {
      $question_ids[] = $reportobservations->id;
      $observations[$reportobservations->id]['question_sort'] = "Q".$q;
      $observations[$reportobservations->id]['question'] = $reportobservations->question;
      $observations[$reportobservations->id]['question_type'] = $reportobservations->question_type;
      $q++;
    }
    //print_r($observations);
    //echo"<br>".$startdate." >> ".$enddate;
    $challan_data = $this->trainings_model->get_challan_assessment($startdate, $enddate, $state_id, $district_id, $block_id);
    
    $sparkids = array();
    $activity_dates = array();
    foreach($challan_data as $challandata)
    {
      if($this->data['current_role'] == 'field_user'){
        if(!in_array($challandata->spark_id, $sparkids) && $challandata->spark_id == $this->data['current_user_id'])
          array_push($sparkids, $challandata->spark_id);
      }
      else{
        if(!in_array($challandata->spark_id, $sparkids))
          array_push($sparkids, $challandata->spark_id);
      }
      if(!in_array($challandata->issue_date, $activity_dates))
          array_push($activity_dates, $challandata->issue_date);
    }
    
    //$training_data = $this->trainings_model->get_training_report($startdate, $enddate, $question_ids, $state_id, $district_id, $block_id);
    $training_data = array();
    $user_ids = array();
    $district_ids = array();
    $block_ids = array();
    $cluster_ids = array();
    $user_arr  = array();
    $activity_ids = array();
    
    $traineddata = array();
    $trained_data = array();
    
    $observation_data = array();
    $observation_arr = array();
    
    if(!empty($sparkids))
    $training_data = $this->trainings_model->get_trainings_report($sparkids, $activity_dates, $question_ids);
    
    
    foreach($training_data as $trainings)
    {
      if(!in_array($trainings->user_id, $user_ids))
        array_push($user_ids, $trainings->user_id);
      
      if($report_type == 'block'){
        $training_loc = $trainings->cluster_id;
        
        if(!in_array($trainings->cluster_id, $cluster_ids))
          array_push($cluster_ids, $trainings->cluster_id);
      }
      else if($report_type == 'district'){
        $training_loc = $trainings->block_id;
        
        if(!in_array($trainings->block_id, $block_ids))
          array_push($block_ids, $trainings->block_id);
      }
      else if($report_type == 'state'){
        $training_loc = $trainings->district_id;
        
        if(!in_array($trainings->district_id, $district_ids))
          array_push($district_ids, $trainings->district_id);
      }
      
      if(!isset($user_arr[$training_loc])){
        $user_arr[$training_loc][] = $trainings->user_id;
      }
      else{
        if(!in_array($trainings->user_id, $user_arr[$training_loc]))
          array_push($user_arr[$training_loc], $trainings->user_id);  
      }
      
      $new_val = $trainings->answer;
      if($observations[$trainings->question_id]['question_type'] == 'subjective')
      {
        $new_val = trim(str_replace('%','',$trainings->answer));
        $new_val = (int) $new_val;
      }
      
      $observation_arr[$training_loc][$trainings->subject_id][$trainings->question_id][] = $new_val;
      
      if(!in_array($trainings->id, $activity_ids)){
        array_push($activity_ids, $trainings->id);
        if(!isset($traineddata[$training_loc][$trainings->subject_id])){
          $traineddata[$training_loc][$trainings->subject_id] = $trainings->no_of_teachers_trained;
        }  
        else{
          $traineddata[$training_loc][$trainings->subject_id] = $traineddata[$training_loc][$trainings->subject_id]+$trainings->no_of_teachers_trained;
        }
      }
        
      if(!isset($observation_data[$training_loc][$trainings->subject_id][$trainings->question_id])){
        $observation_data[$training_loc][$trainings->subject_id][$trainings->question_id] = $new_val;
      }
      else{
        if($observations[$trainings->question_id]['question_type'] == 'subjective'){
          $observation_data[$training_loc][$trainings->subject_id][$trainings->question_id] = $observation_data[$training_loc][$trainings->subject_id][$trainings->question_id]+$new_val;
        }
        else if($observations[$trainings->question_id]['question_type'] == 'yes-no' && $new_val == 1){
          $observation_data[$training_loc][$trainings->subject_id][$trainings->question_id] = $observation_data[$training_loc][$trainings->subject_id][$trainings->question_id]+1;
        }
      }
      
    }
    //print_r($observation_arr);
    //print_r($observation_data);
    //die;
    
    /*$trained_data = array();
    if($report_type == 'block'){
      if(!empty($cluster_ids))
      $trained_data = $this->trainings_model->get_trained_teacher_data('', '', $cluster_ids);
    }
    else if($report_type == 'district'){
      if(!empty($block_ids))
      $trained_data = $this->trainings_model->get_trained_teacher_data('', $block_ids, '');
    }
    else if($report_type == 'state'){
      if(!empty($district_ids))
      $trained_data = $this->trainings_model->get_trained_teacher_data($district_ids, '', '');
    }
    
    $traineddata = array();
    foreach($trained_data as $trained)
    {
      $traineddata[$trained->filter_type][$trained->subject_id] = $trained->teacher_trained;   
    }*/
    
    $data_arr = array();
    $type_values = array();
    if($report_type == 'block'){
      $this->load->Model('clusters_model');
      if(!empty($cluster_ids))
        $data_arr = $this->clusters_model->getByIds($cluster_ids);
      
      $this->data['column_name'] = 'Cluster';
    }
    else if($report_type == 'district'){
      $this->load->Model('blocks_model');
      if(!empty($block_ids))
        $data_arr = $this->blocks_model->getByIds($block_ids);
      
      $this->data['column_name'] = 'Block';
    }
    else if($report_type == 'state'){
      $this->load->Model('districts_model');
      if(!empty($district_ids))
        $data_arr = $this->districts_model->getByIds($district_ids);
      
      $this->data['column_name'] = 'District';
    }
    
    if(!empty($data_arr)){
      foreach($data_arr as $data){
        $type_values[$data->id] = $data->name;
      }
      if(!empty($meet_with[0]))
        $type_values[0] = 'Others';
    }
    
    $this->load->Model('Subjects_model');
    
    $subjectData = $this->Subjects_model->get_all(1);
    
    $subjects = array();
    foreach($subjectData as $subject_data)
    {
      $subjects[$subject_data->id] = $subject_data->name;
    }
    
    $this->load->Model('Users_model');
    
    $user_names = array();
    if(!empty($user_ids)){
      $user_data = $this->Users_model->getByIds($user_ids);
      foreach($user_data as $data){
        $user_names[$data->id] = $data->name;
      }
    }
    
    $this->data['observation_data'] = $observation_data;
    $this->data['observation_arr'] = $observation_arr;
    $this->data['traineddata'] = $traineddata;
    $this->data['subjects'] = $subjects;
    $this->data['observations'] = $observations;
    $this->data['user_names'] = $user_names;
    $this->data['user_arr'] = $user_arr;
    $this->data['type_values'] = $type_values;
    
    if ($download == 1)
    {
      $myFile = "./uploads/reports/training_logistic.xls";
      $this->load->library('parser');
			$this->data['download'] = $download;
      //pass retrieved data into template and return as a string
      $stringData = $this->parser->parse('training/training_subject_report_data', $this->data, true);
      //open excel and write string into excel
      $fh = fopen($myFile, 'w') or die("can't open file");
      fwrite($fh, $stringData);
      fclose($fh);
      //download excel file
      $this->common_functions->downloadExcel("training_logistic");
      unlink($myFile);
    }
    else if($pdf == 1)
    {
        //pass retrieved data into template and return as a string
        $this->data['pdf'] = 1;
        $summary = $this->load->view('training/training_subject_report_data',$this->data,true);
        $this->common_functions->create_pdf($summary,'training_logistic');
    }
    else{
      $this->data['partial'] = 'training/training_subject_report_data';
      $this->data['page_title'] = 'Training Data';
      $this->load->view('templates/index', $this->data);
    }
    
  }
  
  function training_quality()
  {
    $this->output->enable_profiler(false);
    $month = date('m');
    $year = date('Y');
    $this->data['start_year'] = $month;
    $this->data['start_month'] = $year;

    if(!empty($_POST))
    {
      if(empty($_POST['start_month']) || empty($_POST['start_year']))
      {
          redirect('training/training_report');
      }
      $startmonth       = $_POST['start_month'];	   
      $startyear        = $_POST['start_year'];
      $duration        = $_POST['duration'];
      
      $download = (!empty($_POST['download']) ? $_POST['download'] : 0); 
      $pdf = (!empty($_POST['pdf']) ? $_POST['pdf'] : 0); 
       
      if(!empty($_POST['state_id']) && !empty($_POST['district_id'])  && !empty($_POST['block_id']))
      {
        $this->get_training_report('block', $duration, $startmonth, $startyear, $download, $pdf, $_POST['state_id'], $_POST['district_id'], $_POST['block_id']);
      }
      if(!empty($_POST['state_id']) && !empty($_POST['district_id'])  && empty($_POST['block_id']))
      {
        $this->get_training_report('district', $duration, $startmonth, $startyear,$download, $pdf, $_POST['state_id'], $_POST['district_id']);
      }
      if(!empty($_POST['state_id']) && empty($_POST['district_id'])  && empty($_POST['block_id']))
      {
        $this->get_training_report('state', $duration, $startmonth, $startyear, $download, $pdf, $_POST['state_id']);
      }
      if(empty($_POST['state_id']) && empty($_POST['district_id'])  && empty($_POST['block_id']))
      {
        redirect('training/training_report');
      }
    }
    else{
      $this->load->Model('states_model');
      $states = array();
       if($this->data['current_role']=="field_user"){
        $this->load->Model('Users_model');
        $districts = array();
        $districtData = $this->Users_model->user_districts($this->data['current_user_id']);
        foreach($districtData as $district){
          $districts[$district->id] = $district->name;
        }
        $this->data['districts'] = $districts;
      }
      else if($this->data['current_role']=="state_person" || $this->data['current_role']=="manager"){
          $states = $this->states_model->get_by_id($this->data['user_state_id']);
      }
      else{
          $states = $this->states_model->get_all();
      }
      $this->data['duration'] = 'monthly';
      $this->data['states'] = $states;
      $this->data['partial'] = 'training/training_report';
      $this->data['page_title'] = 'Block School Observation';
      $this->load->view('templates/index', $this->data);
    }
  }

  function get_training_report($report_type, $duration, $startmonth, $startyear, $download, $pdf, $state_id, $district_id='', $block_id='')
  {
    
    $sess_month = SESSION_START_MONTH;
    $sess_year = date('Y');
    
    if($startmonth < $sess_month){
      $sess_year = date('Y') - 1;
    }
    else if($startyear < $sess_year)
    {
			$sess_year = $startyear;
		}
    
    $session_start_date = $sess_year."-".$sess_month."-1";
    
    $startdate        = ($duration == 'YTD' ? $session_start_date : $startyear."-".$startmonth."-1");
	  $enddate        = date('Y-m-t', strtotime($startyear.'-'.$startmonth.'-01'));
    
    $month_name   = date('F', mktime(0, 0, 0, $startmonth, 10));
    
    $this->data['startdate'] = $startdate;
    $this->data['enddate'] = $enddate;
    $this->data['duration'] = $duration;
    $this->data['start_month'] = $startmonth;
    $this->data['start_year'] = $startyear;
    $this->data['month_name'] = $month_name;
    
    $this->data['state_id'] = $state_id;
    $this->data['district_id'] = $district_id;
    $this->data['block_id'] = $block_id;
    
    $report_observations = $this->trainings_model->get_training_observations($state_id, 2);
    
    //print_r($report_observations);
    
    $question_ids = array();
    $observations = array();
    
    $q=1;
    foreach($report_observations as $reportobservations)
    {
      $question_ids[] = $reportobservations->id;
      $observations[$reportobservations->id]['question_sort'] = "Q".$q;
      $observations[$reportobservations->id]['question'] = $reportobservations->question;
      $observations[$reportobservations->id]['question_type'] = $reportobservations->question_type;
      $q++;
    }
    //print_r($observations);
    
    $challan_data = $this->trainings_model->get_challan_assessment($startdate, $enddate, $state_id, $district_id, $block_id);
    //print_r($challan_data);
    
    $sparkids = array();
    $activity_dates = array();
    foreach($challan_data as $challandata)
    {
      if($this->data['current_role'] == 'field_user'){
        if(!in_array($challandata->spark_id, $sparkids) && $challandata->spark_id == $this->data['current_user_id'])
          array_push($sparkids, $challandata->spark_id);
      }
      else{
        if(!in_array($challandata->spark_id, $sparkids))
          array_push($sparkids, $challandata->spark_id);
      }
      if(!in_array($challandata->issue_date, $activity_dates))
        array_push($activity_dates, $challandata->issue_date);
    }
    
    $user_ids = array();
    $district_ids = array();
    $block_ids = array();
    $cluster_ids = array();
    $user_arr  = array();
    
    $observation_data = array();
    $observation_arr = array();
    $training_data = array();
    
    //$training_data = $this->trainings_model->get_training_report($startdate, $enddate, $question_ids, $state_id, $district_id, $block_id);
    if(!empty($sparkids))
      $training_data = $this->trainings_model->get_trainings_report($sparkids, $activity_dates, $question_ids);
    //print_r($training_data);
    
    foreach($training_data as $trainings)
    {
      if(!in_array($trainings->user_id, $user_ids))
        array_push($user_ids, $trainings->user_id);
      
      if($report_type == 'block'){
        $training_loc = $trainings->cluster_id;
        
        if(!in_array($trainings->cluster_id, $cluster_ids))
          array_push($cluster_ids, $trainings->cluster_id);
      }
      else if($report_type == 'district'){
        $training_loc = $trainings->block_id;
        
        if(!in_array($trainings->block_id, $block_ids))
          array_push($block_ids, $trainings->block_id);
      }
      else if($report_type == 'state'){
        $training_loc = $trainings->district_id;
        
        if(!in_array($trainings->district_id, $district_ids))
          array_push($district_ids, $trainings->district_id);
      }
      
      if(!isset($user_arr[$training_loc])){
        $user_arr[$training_loc][] = $trainings->user_id;
      }
      else{
        if(!in_array($trainings->user_id, $user_arr[$training_loc]))
          array_push($user_arr[$training_loc], $trainings->user_id);  
      }
      
      $new_val = $trainings->answer;
      if($observations[$trainings->question_id]['question_type'] == 'subjective')
      {
        $new_val = trim(str_replace('%','',$trainings->answer));
        $new_val = (int) $new_val;
      }
      else if($observations[$trainings->question_id]['question_type'] == 'objective'){
        //echo"<br>".$trainings->question_id." >> ".$trainings->answer;
        $new_val = $this->get_option_order($trainings->question_id, $trainings->answer);
      }
      
      $observation_arr[$training_loc][$trainings->question_id][] = $new_val;
      
      if(!isset($observation_data[$training_loc][$trainings->question_id])){
        $observation_data[$training_loc][$trainings->question_id] = $new_val;
      }
      else{
        if($observations[$trainings->question_id]['question_type'] == 'subjective'){
          $observation_data[$training_loc][$trainings->question_id] = $observation_data[$training_loc][$trainings->question_id]+$new_val;
        }
        else if($observations[$trainings->question_id]['question_type'] == 'objective'){
          $observation_data[$training_loc][$trainings->question_id] = $observation_data[$training_loc][$trainings->question_id]+$new_val;
        }
        /*else if($observations[$trainings->question_id]['question_type'] == 'yes-no' && $new_val == 1){
          $observation_data[$training_loc][$trainings->question_id] = $observation_data[$training_loc][$trainings->question_id]+1;
        }*/
      }
    }
    //print_r($observation_arr);
    //print_r($observation_data);
    //die;
    
    $data_arr = array();
    $type_values = array();
    if($report_type == 'block'){
      $this->load->Model('clusters_model');
      if(!empty($cluster_ids))
        $data_arr = $this->clusters_model->getByIds($cluster_ids);
      
      $this->data['column_name'] = 'Cluster';
    }
    else if($report_type == 'district'){
      $this->load->Model('blocks_model');
      if(!empty($block_ids))
        $data_arr = $this->blocks_model->getByIds($block_ids);
      
      $this->data['column_name'] = 'Block';
    }
    else if($report_type == 'state'){
      $this->load->Model('districts_model');
      if(!empty($district_ids))
        $data_arr = $this->districts_model->getByIds($district_ids);
      
      $this->data['column_name'] = 'District';
    }
    
    if(!empty($data_arr)){
      foreach($data_arr as $data){
        $type_values[$data->id] = $data->name;
      }
      if(!empty($meet_with[0]))
        $type_values[0] = 'Others';
    }
    
    $this->load->Model('Subjects_model');
    
    $subjectData = $this->Subjects_model->get_all(1);
    
    $subjects = array();
    foreach($subjectData as $subject_data)
    {
      $subjects[$subject_data->id] = $subject_data->name;
    }
    
    $this->load->Model('Users_model');
    
    $user_names = array();
    if(!empty($user_ids)){
      $user_data = $this->Users_model->getByIds($user_ids);
      foreach($user_data as $data){
        $user_names[$data->id] = $data->name;
      }
    }
    
    $this->data['observation_data'] = $observation_data;
    $this->data['observation_arr'] = $observation_arr;
    $this->data['subjects'] = $subjects;
    $this->data['observations'] = $observations;
    $this->data['user_names'] = $user_names;
    $this->data['user_arr'] = $user_arr;
    $this->data['type_values'] = $type_values;
    
    if ($download == 1)
    {
      $myFile = "./uploads/reports/training_quality.xls";
      $this->load->library('parser');
			$this->data['download'] = $download;
      //pass retrieved data into template and return as a string
      $stringData = $this->parser->parse('training/training_report_data', $this->data, true);
      //open excel and write string into excel
      $fh = fopen($myFile, 'w') or die("can't open file");
      fwrite($fh, $stringData);
      fclose($fh);
      //download excel file
      $this->common_functions->downloadExcel("training_quality");
      unlink($myFile);
    }
    else if($pdf == 1)
    {
        //pass retrieved data into template and return as a string
        $this->data['pdf'] = 1;
        $summary = $this->load->view('training/training_report_data',$this->data,true);
        $this->common_functions->create_pdf($summary,'training_quality');
    }
    else{
      $this->data['partial'] = 'training/training_report_data';
      $this->data['page_title'] = 'Meeting Data';
      $this->load->view('templates/index', $this->data);
    }
    
  }
  
  function get_option_order($ques_id, $answer)
  {
    $this->load->Model('Observations_model');
    
    $options = $this->Observations_model->getObtionsByObservation($ques_id, '', '1');
    //print_r($options);
    $k = 1;
    foreach($options as $option)
    {
       if($option->id == $answer)
       {
         return $k;
       } 
       $k++;
    }
  }
}
