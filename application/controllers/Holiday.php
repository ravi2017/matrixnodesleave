<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Holiday extends CI_Controller {

	function __construct()	
	{
		parent::__construct();
		$this->load->helper('url');
		$this->data['current_page'] = 'holidays';
		$this->load->model('holidays_model');
		$this->load->library('common_functions');
		$this->load->helper('date');
    if($this->session->userdata('logged_in'))
		{
      $session_data = $this->session->userdata('logged_in');
      $this->data['current_user_id'] = $session_data['id'];
      $this->data['current_username'] = $session_data['username'];
      $this->data['current_name'] = $session_data['name'];
      $this->data['current_role'] = $session_data['role'];
      $this->data['user_state_id'] = $session_data['state_id'];
      $this->data['user_district_id'] = $session_data['district_id'];
      
    }
    else
    {
      //If no session, redirect to login page
      redirect('login', 'refresh');
    }
    
    $activity_date = $this->session->userdata('activity_date');
    if ($activity_date == "")
    {
      $this->session->set_flashdata('alert', 'Select date first');
      redirect('dashboard', 'refresh');
    }
    else
    {
      $this->data['activity_date'] = $activity_date;
    }
    
    $this->load->model("school_visits_model");
    $this->load->model("meetings_model");
    $this->load->model("trainings_model");
    $start_date = $end_date = $activity_date;
    $user_id = $this->data['id'];
    $trainings = $this->trainings_model->get_all($user_id, $start_date, $end_date);
    
    $meetings = $this->meetings_model->get_all($user_id, $start_date, $end_date);
    
    $school_visits = $this->school_visits_model->get_all($user_id, $start_date, $end_date);
    
    if (count($trainings) > 0 or count($school_visits) > 0 or count($meetings) > 0)
    {
      $this->session->set_flashdata('alert', 'Holiday cannot be marked, as another activity already marked');
      redirect('select_activity', 'refresh');
    }
    
	}
  
  function index()
  {
    $this->data['partial'] = 'holiday/index';
    $this->data['page_title'] = 'Holiday';
    $this->load->view('templates/index', $this->data);    
  }
  
  function personal()
  {
    $mdata['activity_date'] = $this->data['activity_date'];
    $mdata['holiday_type'] = "personal";
    $mdata['user_id'] = $this->data['id'];
    
    $this->holidays_model->add($mdata);
        
    $this->session->unset_userdata('activity_date');			
    $this->session->set_flashdata('alert', 'Holiday has been marked for '.strftime("%d-%b-%Y",strtotime($this->data['activity_date'])));
    redirect('dashboard', 'refresh');
  }
  
  function govt()
  {
    $mdata['activity_date'] = $this->data['activity_date'];
    $mdata['holiday_type'] = "govt";
    $mdata['user_id'] = $this->data['id'];
    
    $this->holidays_model->add($mdata);
        
    $this->session->unset_userdata('activity_date');			
    $this->session->set_flashdata('alert', 'Holiday has been marked for '.strftime("%d-%b-%Y",strtotime($this->data['activity_date'])));
    redirect('dashboard', 'refresh');
  }
  
}
