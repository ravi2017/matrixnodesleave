<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Spark_training extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->data['current_page'] = 'training';
		$this->load->model('trainings_model');
		$this->load->library('form_validation');
		$this->load->library('common_functions');
		$this->load->helper('date');
		if($this->session->userdata('logged_in'))
		{
		$session_data = $this->session->userdata('logged_in');
		$this->data['current_user_id'] = $session_data['id'];
		$this->data['current_username'] = $session_data['username'];
		$this->data['current_name'] = $session_data['name'];
		$this->data['current_role'] = $session_data['role'];
		$this->data['user_state_id'] = $session_data['state_id'];
		$this->data['user_district_id'] = $session_data['district_id'];  
     
    }
    else
    {
      //If no session, redirect to login page
      redirect('login', 'refresh');
    }
    
	}
  
	function index(){
    $this->data['partial'] = 'reports/index';
    $this->data['page_title'] = 'Reports';
    $this->load->view('templates/index', $this->data);    
  }
  
	function spark_train(){
     
			$this->output->enable_profiler(false);
			$month = 4;
			$year = 2016;
				$this->data['year'] = $month;
				$this->data['month'] = $year;
			if (isset($_GET['state_id']) and isset($_GET['month']) and isset($_GET['year']))
			{
				$this->load->model('clusters_model');
				$state_id = $_GET['state_id'];
				
			 
				
				 $start_date = date('Y-m-d',mktime(0,0,0,$_GET['month'],1,$_GET['year']));
				$end_date = date('Y-m-t',mktime(0,0,0,$_GET['month'],1,$_GET['year']));
		
				//exit;
				$this->data['state_id'] = $state_id;
				
				$district = new District();
				$district->where("state_id",$state_id)->get();
				$this->data['district'] = $district;
				
				$state = $this->states_model->get_by_id($state_id);
				$this->data['state'] = $state;
				
			 
				
				$district_school = new School();
				$district_school->where("state_id",$state_id);
				$district_school->group_by('district_id');
				$district_school->select('district_id');
				$district_school->select('count(ssc_schools.id) as total_schools');
				$district_school->include_related('district',array('name'));
				$district_school->get();
				$this->data['district'] = $district_school;
				$this->load->model('school_visits_model');
				$this->load->model('meetings_model');
				$this->load->model('trainings_model');
				$school_visit_count = array();
				foreach($district_school as $district_schools)
				{
					$school_visit = $this->school_visits_model->get_visited_school_count_district_tlm($district_schools->district_id,$start_date,$end_date);
				//	print_r($school_visit );
					$tlm_usages = $this->school_visits_model->get_school_wise_average_tlm_district($district_schools->district_id,$start_date,$end_date);
					$tlm_0[$district_schools->district_id] = 0;
					$tlm_1[$district_schools->district_id] = 0;
					$tlm_2[$district_schools->district_id] = 0;
					foreach($tlm_usages as $tlm_usage) {
						if (round($tlm_usage->avg_tlm_usage) == 0)
						{
							$tlm_0[$district_schools->district_id] = $tlm_0[$district_schools->district_id] + 1;
						}
						elseif (round($tlm_usage->avg_tlm_usage) == 0)
						{
							$tlm_1[$district_schools->district_id] = $tlm_1[$district_schools->district_id] + 1;
						}
						else
						{
							$tlm_2[$district_schools->district_id] = $tlm_2[$district_schools->district_id] + 1;
						}
					}
					$meetings = $this->meetings_model->get_cluster_meetings($district_schools->district_id, $start_date, $end_date);
				 
					$district_meetings = $this->meetings_model->get_district_meetings($district_schools->district_id, $start_date, $end_date);
					$physical_meet[$district_schools->district_id] = 0;
					$telephonic_meet[$district_schools->district_id] = 0;
					$meet_brc[$district_schools->district_id]=0;
					$meet_dc[$district_schools->district_id]=0;
					$traning[$district_schools->district_id]=0;
					//$meet_brc =array();
					//$meet_dc = 0;
					foreach($district_meetings as $meeting)
					{
						if (strtoupper($meeting->meet_with) == "CRC" and strtolower($meeting->meeting_mode) == "physical")
						{
							$physical_meet[$district_schools->district_id] = 1;
						}
						if (strtoupper($meeting->meet_with) == "BRC")
						{
							 $meet_brc[$district_schools->district_id] = 1;
							
						}
						elseif (strtoupper($meeting->meet_with) == "CRC" and strtolower($meeting->meeting_mode) == "telephonic")
						{
							$telephonic_meet[$district_schools->district_id] = 1;
						}
					}
				 
					foreach($district_meetings as $meeting)
					{
						if (strtoupper($meeting->meet_with) == "DC")
						{
							$meet_dc[$district_schools->district_id] = 1;
						}
					}
					//$school_visit_count[$district_schools->district_id] = $school_visit['total_school_count'];
					$school_visit_count[$district_schools->district_id] = count($school_visit);
					$district_training = $this->trainings_model->get_district_training($district_schools->district_id, $start_date, $end_date);
					foreach($district_training as $tranings)
					{
						if(isset($traning[$district_schools->district_id]))
						{
													
									$traning[$district_schools->district_id] =	$traning[$district_schools->district_id] + 1;
						}else{
								$traning[$district_schools->district_id] = 1;
						}
					}
				}
				//echo"<pre>";
				//print_r($meet_dc);exit();
				$this->data['school_visit_count'] = $school_visit_count;
				$this->data['tlm_0'] = $tlm_0;
				$this->data['tlm_1'] = $tlm_1;
				$this->data['tlm_2'] = $tlm_2;
				$this->data['physical_meet'] = $physical_meet;
				$this->data['telephonic_meet'] = $telephonic_meet;
				$this->data['meet_brc'] = $meet_brc;
				$this->data['meet_dc'] = $meet_dc;
				$this->data['traning'] = $traning;
				
				$this->data['year'] = $_GET['year'];
				$this->data['month'] = $_GET['month'];
			}
			
			$states = $this->states_model->get_all();
			$this->data['states'] = $states;
			$download = 0;
			if (isset($_GET['download']) and $_GET['download'] == 1)
			{
				$download = $_GET['download'];
				
			}
			if ($download == 1)
			{
				$myFile = "./uploads/state.xls";
				$this->load->library('parser');
				$this->data['download'] = $download;
				//pass retrieved data into template and return as a string
				$stringData = $this->parser->parse('print_reports/state', $this->data, true);
				//open excel and write string into excel
				$fh = fopen($myFile, 'w') or die("can't open file");
				fwrite($fh, $stringData);
				fclose($fh);
				//download excel file
				$this->downloadExcel("state");
				unlink($myFile);
			}
				else{
						$this->data['partial'] = 'spark_training/spark_train';
						$this->data['page_title'] = 'Reports';
						$this->load->view('templates/index', $this->data); 
				}
  }
  	/* download created excel file */
	function downloadExcel($name) {
      $myFile = "./uploads/".$name.".xls";
      header("Content-Length: " . filesize($myFile));
      header('Content-Type: application/vnd.ms-excel');
      header('Content-Disposition: attachment; filename='.$name.'.xls');

      readfile($myFile);
  }
	
	function sparktrainingDetail(){
		$state_id 		 = $_GET['state_id'];	  
		$startmonth       = $_GET['month'];	   
		$startyear        = $_GET['year'];
		$startdate        = '01/'.$startmonth.'/'.$startyear;
		$startdate        = date('t/m/Y', strtotime($startyear.'-'.$startmonth.'-01'));

        $month_name = "";
        $prev_month1_name   = "";
        $prev_month2_name   = "";
		$cm_no 		= 0;
		$pv1_no 	= 0;
		$pv2_no 	= 0;
		
	switch($startmonth){

		case "1":
			$month_name = date('F', mktime(0, 0, 0, $startmonth, 10));
			$cm_no      = $startmonth;
			$prev_month1_name   = date('F', mktime(0, 0, 0, 12, 10));
			$pv1_no = 12;
			$prev_month2_name   = date('F', mktime(0, 0, 0, 12 - $startmonth, 10));
			$pv2_no = 12 - $startmonth;
			break;
		case "2":
			$month_name = date('F', mktime(0, 0, 0, $startmonth, 10));
			$cm_no      = $startmonth;
			$prev_month1_name   = date('F', mktime(0, 0, 0, $startmonth-1, 10));
			$pv1_no = $startmonth-1;
			$prev_month2_name   = date('F', mktime(0, 0, 0, 12, 10));
			$pv2_no = 12;
			break;
		default:
			$month_name   = date('F', mktime(0, 0, 0, $startmonth, 10));
			$cm_no      = $startmonth;
			$prev_month1_name   = date('F', mktime(0, 0, 0, $startmonth-1, 10));
			$pv1_no = $startmonth-1;
			$prev_month2_name   = date('F', mktime(0, 0, 0, $startmonth-2, 10));
			$pv2_no = $startmonth-2;
	}
		
		 $cm_result = $this->sparkQuery($state_id,$startmonth,$startyear);
		 $total_s = count($cm_result);
		 $name_result = $this->districtName($state_id);
		 $count_1 = count($name_result);
        //PHP EXCEL TO DOWNLOD REPORT FILE
        ob_clean();
        ob_start(); # added 
        header('Content-type: application/vnd.ms-excel; charset=UTF-8');
        ob_clean(); # remove this
        //load our new PHPExcel library
        $this->load->library('excel');
        //activate worksheet number 1
        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('Spark Training Detailed Report');

        /* ------------------------Setting Static text and dynamic data B3------ */

        //Header Text size 
        $this->excel->getActiveSheet()->getStyle('B2')->getFont()->setSize(15);
        //HEADER TEXT 
        $this->excel->getActiveSheet()->setCellValue('B2', 'Spark Training Detailed Report');
        //Merge
        $this->excel->getActiveSheet()->mergeCells('B2:N2');
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle('B2')->getFont()->setBold(true);
        //Center text
        $this->excel->getActiveSheet()->getStyle('B2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        //MONTH HEADERS  
        $this->excel->getActiveSheet()->setCellValue('G3', $prev_month2_name);
        $this->excel->getActiveSheet()->setCellValue('K3', $prev_month1_name);
        $this->excel->getActiveSheet()->setCellValue('O3', $month_name);
        //Merge
        $this->excel->getActiveSheet()->mergeCells('G3:J3');
        $this->excel->getActiveSheet()->mergeCells('K3:N3');
        $this->excel->getActiveSheet()->mergeCells('O3:R3');
        //Center text
        $this->excel->getActiveSheet()->getStyle('G3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('K3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('O3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        //make the font become bold
        $this->excel->getActiveSheet()->getStyle('G3')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('K3')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('O3')->getFont()->setBold(true);
		

		
        //Training cell
        $this->excel->getActiveSheet()->setCellValue('G4', 'Half Day Training');
		$this->excel->getActiveSheet()->setCellValue('I4', 'Full Day Training');
		$this->excel->getActiveSheet()->setCellValue('K4', 'Half Day Training');
		$this->excel->getActiveSheet()->setCellValue('M4', 'Full Day Training');
		$this->excel->getActiveSheet()->setCellValue('O4', 'Half Day Training');
		$this->excel->getActiveSheet()->setCellValue('Q4', 'Full Day Training');
       
        //Center text
		$this->excel->getActiveSheet()->getStyle('B5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('C5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('D5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('E5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('F5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('Q4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('G4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('I4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('K4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('M4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('O4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		//make the font become bold
        $this->excel->getActiveSheet()->getStyle('B5')->getFont()->setBold(true);
		$this->excel->getActiveSheet()->getStyle('C5')->getFont()->setBold(true);
		$this->excel->getActiveSheet()->getStyle('D5')->getFont()->setBold(true);
		$this->excel->getActiveSheet()->getStyle('E5')->getFont()->setBold(true);
		$this->excel->getActiveSheet()->getStyle('F5')->getFont()->setBold(true);
		$this->excel->getActiveSheet()->getStyle('E4')->getFont()->setBold(true);
		$this->excel->getActiveSheet()->getStyle('G4')->getFont()->setBold(true);
		$this->excel->getActiveSheet()->getStyle('I4')->getFont()->setBold(true);
		$this->excel->getActiveSheet()->getStyle('K4')->getFont()->setBold(true);
		$this->excel->getActiveSheet()->getStyle('M4')->getFont()->setBold(true);
		$this->excel->getActiveSheet()->getStyle('O4')->getFont()->setBold(true);
		$this->excel->getActiveSheet()->getStyle('Q4')->getFont()->setBold(true);
        //Merge cell 
        $this->excel->getActiveSheet()->mergeCells('Q4:R4');
        $this->excel->getActiveSheet()->mergeCells('G4:H4');
        $this->excel->getActiveSheet()->mergeCells('I4:J4');
        $this->excel->getActiveSheet()->mergeCells('K4:L4');
        $this->excel->getActiveSheet()->mergeCells('M4:N4');
        $this->excel->getActiveSheet()->mergeCells('O4:P4');		
		
        $this->excel->getActiveSheet()->setCellValue('B5', 'Sr. No.');
		$this->excel->getActiveSheet()->setCellValue('C5', 'Spark Name');
		$this->excel->getActiveSheet()->setCellValue('D5', 'District');
		$this->excel->getActiveSheet()->setCellValue('E5', 'Block');
		$this->excel->getActiveSheet()->setCellValue('F5', 'Cluster');
		$this->excel->getActiveSheet()->setCellValue('G5', 'No. of Training');
		$this->excel->getActiveSheet()->setCellValue('H5', 'No. of Participants Attended');
		$this->excel->getActiveSheet()->setCellValue('I5', 'No. of Training');
		$this->excel->getActiveSheet()->setCellValue('J5', 'No. of Participants Attended');
		$this->excel->getActiveSheet()->setCellValue('K5', 'No. of Training');
        $this->excel->getActiveSheet()->setCellValue('L5', 'No. of Participants Attended');
		$this->excel->getActiveSheet()->setCellValue('M5', 'No. of Training');
		$this->excel->getActiveSheet()->setCellValue('N5', 'No. of Participants Attended');
		$this->excel->getActiveSheet()->setCellValue('O5', 'No. of Training');
		$this->excel->getActiveSheet()->setCellValue('P5', 'No. of Participants Attended');
		$this->excel->getActiveSheet()->setCellValue('Q5', 'No. of Training');
		$this->excel->getActiveSheet()->setCellValue('R5', 'No. of Participants Attended');		
		
		//wrapping text
		$this->excel->getActiveSheet()->getStyle('B5')->getAlignment()->setWrapText(true);
		$this->excel->getActiveSheet()->getStyle('C5')->getAlignment()->setWrapText(true);
		$this->excel->getActiveSheet()->getStyle('D5')->getAlignment()->setWrapText(true);
        $this->excel->getActiveSheet()->getStyle('E5')->getAlignment()->setWrapText(true);
		$this->excel->getActiveSheet()->getStyle('F5')->getAlignment()->setWrapText(true);
		$this->excel->getActiveSheet()->getStyle('G5')->getAlignment()->setWrapText(true);
        $this->excel->getActiveSheet()->getStyle('H5')->getAlignment()->setWrapText(true);
		$this->excel->getActiveSheet()->getStyle('I5')->getAlignment()->setWrapText(true);
		$this->excel->getActiveSheet()->getStyle('J5')->getAlignment()->setWrapText(true);
        $this->excel->getActiveSheet()->getStyle('K5')->getAlignment()->setWrapText(true);
		$this->excel->getActiveSheet()->getStyle('L5')->getAlignment()->setWrapText(true);
		$this->excel->getActiveSheet()->getStyle('M5')->getAlignment()->setWrapText(true);
        $this->excel->getActiveSheet()->getStyle('N5')->getAlignment()->setWrapText(true);
		$this->excel->getActiveSheet()->getStyle('O5')->getAlignment()->setWrapText(true);
		$this->excel->getActiveSheet()->getStyle('P5')->getAlignment()->setWrapText(true);
		$this->excel->getActiveSheet()->getStyle('Q5')->getAlignment()->setWrapText(true);
		$this->excel->getActiveSheet()->getStyle('R5')->getAlignment()->setWrapText(true);
        
		
		$this->excel->getActiveSheet()->getRowDimension('5')->setRowHeight(45);
		
		//Overall Border -----------------------------------------------------
        //Format Entire Excel sheet 
        // Set thin black border outline around column
        $styleThickBlackBorderOutline = array(
            'borders' => array(
                'outline' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => 'FF000000'),
                ),
            ),
        );

        $styleThinBlackBorderOutline = array(
            'borders' => array(
                'outline' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => 'FF000000'),
                ),
            ),
        );

        $right_styleArray = array(
            'borders' => array(
                'right' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => 'FF000000'),
                ),
            ),
        );

        $allborder_styleArray = array(
            'borders' => array(
                'allborder' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => 'FF000000'),
                ),
            ),
        );
		
		$border_new_start = sprintf('F3');
		$this->excel->getActiveSheet()->getStyle($border_new_start)->applyFromArray($right_styleArray);
		$border_new_start = sprintf('J3');
		$this->excel->getActiveSheet()->getStyle($border_new_start)->applyFromArray($right_styleArray);
		$border_new_start = sprintf('N3');
		$this->excel->getActiveSheet()->getStyle($border_new_start)->applyFromArray($right_styleArray);		
		$border_new_start = sprintf('B5');
		$this->excel->getActiveSheet()->getStyle($border_new_start)->applyFromArray($styleThickBlackBorderOutline);
		$border_new_start = sprintf('C5');
		$this->excel->getActiveSheet()->getStyle($border_new_start)->applyFromArray($styleThickBlackBorderOutline);
		$border_new_start = sprintf('H4');
		$this->excel->getActiveSheet()->getStyle($border_new_start)->applyFromArray($styleThickBlackBorderOutline);
		$border_new_start = sprintf('J4');
		$this->excel->getActiveSheet()->getStyle($border_new_start)->applyFromArray($styleThickBlackBorderOutline);
		$border_new_start = sprintf('L4');
		$this->excel->getActiveSheet()->getStyle($border_new_start)->applyFromArray($styleThickBlackBorderOutline);
		$border_new_start = sprintf('N4');
		$this->excel->getActiveSheet()->getStyle($border_new_start)->applyFromArray($styleThickBlackBorderOutline);

		$border_new_start = sprintf('D5');
		$this->excel->getActiveSheet()->getStyle($border_new_start)->applyFromArray($styleThickBlackBorderOutline);
		$border_new_start = sprintf('E5');
		$this->excel->getActiveSheet()->getStyle($border_new_start)->applyFromArray($styleThickBlackBorderOutline);

		$border_new_start = sprintf('F5');
		$this->excel->getActiveSheet()->getStyle($border_new_start)->applyFromArray($styleThickBlackBorderOutline);
		$border_new_start = sprintf('G5');
		$this->excel->getActiveSheet()->getStyle($border_new_start)->applyFromArray($styleThickBlackBorderOutline);
		$border_new_start = sprintf('H5');
		$this->excel->getActiveSheet()->getStyle($border_new_start)->applyFromArray($styleThickBlackBorderOutline);
		$border_new_start = sprintf('I5');
		$this->excel->getActiveSheet()->getStyle($border_new_start)->applyFromArray($styleThickBlackBorderOutline);
		$border_new_start = sprintf('J5');
		$this->excel->getActiveSheet()->getStyle($border_new_start)->applyFromArray($styleThickBlackBorderOutline);
		$border_new_start = sprintf('K5');
		$this->excel->getActiveSheet()->getStyle($border_new_start)->applyFromArray($styleThickBlackBorderOutline);
		$border_new_start = sprintf('L5');
		$this->excel->getActiveSheet()->getStyle($border_new_start)->applyFromArray($styleThickBlackBorderOutline);
		$border_new_start = sprintf('M5');
		$this->excel->getActiveSheet()->getStyle($border_new_start)->applyFromArray($styleThickBlackBorderOutline);
		$border_new_start = sprintf('N5');
		$this->excel->getActiveSheet()->getStyle($border_new_start)->applyFromArray($styleThickBlackBorderOutline);
		$border_new_start = sprintf('O5');
		$this->excel->getActiveSheet()->getStyle($border_new_start)->applyFromArray($styleThickBlackBorderOutline);
		$border_new_start = sprintf('P5');
		$this->excel->getActiveSheet()->getStyle($border_new_start)->applyFromArray($styleThickBlackBorderOutline);
		$border_new_start = sprintf('Q5');
		$this->excel->getActiveSheet()->getStyle($border_new_start)->applyFromArray($styleThickBlackBorderOutline);

        //SET BODER FOR  header
        $borderindex = sprintf('B2:R2');
        $this->excel->getActiveSheet()->getStyle($borderindex)->applyFromArray($styleThickBlackBorderOutline);

        //set right border line to close START table
        $right_borderindex = sprintf('A2:A5');
        $this->excel->getActiveSheet()->getStyle($right_borderindex)->applyFromArray($right_styleArray);

		//set right border line to close END table
        $right_borderindex = sprintf('R2:R5');
        $this->excel->getActiveSheet()->getStyle($right_borderindex)->applyFromArray($right_styleArray);
		


        //SET BORDER FOR MONTH1 NAME 
        $monthone_borderindex = sprintf('G4:R4');
        $this->excel->getActiveSheet()->getStyle($monthone_borderindex)->applyFromArray($styleThinBlackBorderOutline);

        //SET BORDER FOR MONTH2 NAME 
        $monthtwo_borderindex = sprintf('G5:R5');
        $this->excel->getActiveSheet()->getStyle($monthtwo_borderindex)->applyFromArray($styleThinBlackBorderOutline);

		//SET BORDER FOR MONTH2 NAME 
        $monthtwo_borderindex = sprintf('B6:R6');
        $this->excel->getActiveSheet()->getStyle($monthtwo_borderindex)->applyFromArray($styleThinBlackBorderOutline);


        //-----------------------------------------Border code END here ---------------------------------------
        //SET BACKGROUND COLOR
        // $this->excel->getActiveSheet()
                // ->getStyle('B2:Q2')
                // ->getFill()
                // ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                // ->getStartColor()
                // ->setARGB('F0F8FF');

        // $this->excel->getActiveSheet()
                // ->getStyle('D8')
                // ->getFill()
                // ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                // ->getStartColor()
                // ->setARGB('#ADD8E6');
		// $this->excel->getActiveSheet()
                // ->getStyle('H8')
                // ->getFill()
                // ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                // ->getStartColor()
                // ->setARGB('#ADD8E6');
        // $this->excel->getActiveSheet()
                // ->getStyle('L8')
                // ->getFill()
                // ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                // ->getStartColor()
                // ->setARGB('#ADD8E6');				
        // $this->excel->getActiveSheet()
                // ->getStyle('B9:Q9')
                // ->getFill()
                // ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                // ->getStartColor()
                // ->setARGB('#ADD8E6');

        //Increase District Name 
        // $this->excel->getActiveSheet()->getColumnDimension('Q')->setWidth(20);
        //-------------------- CALLING STORED PROCEDURE 'sp_fetch_district_school_rating'  -----------------

        $inc = 0;
		$start_index = 6;
		$i = 0;

        //Border
        $total_district_new = $total_s + 6;
        $border_index_value = sprintf('B6:R%d', $total_district_new);
        $this->excel->getActiveSheet()->getStyle($border_index_value)->applyFromArray($styleThinBlackBorderOutline);
        $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
		$this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
		$this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
		$this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
		$this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
		// $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
		$this->excel->getActiveSheet()->getColumnDimension('J')->setWidth(15);
		// $this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
		$this->excel->getActiveSheet()->getColumnDimension('L')->setWidth(15);
		// $this->excel->getActiveSheet()->getColumnDimension('K')->setWidth(20);
		$this->excel->getActiveSheet()->getColumnDimension('N')->setWidth(15);
		// $this->excel->getActiveSheet()->getColumnDimension('M')->setWidth(20);
		$this->excel->getActiveSheet()->getColumnDimension('P')->setWidth(15);
		
		  // $cm_count  = 0;
		  // $st_in = 6;
		  // 
		// while ($cm_count < $count_1){
			// $cm_count++;
			// $st_in++;
		// }
        while ($inc < $total_s) {
		
		$border_index_value = sprintf('B6:R%d', $total_district_new);
        $this->excel->getActiveSheet()->getStyle($border_index_value)->applyFromArray($styleThinBlackBorderOutline);
		$rank_cell = sprintf('B%d', $start_index);
		$this->excel->getActiveSheet()->getStyle($rank_cell)->applyFromArray($styleThinBlackBorderOutline);
		$this->excel->getActiveSheet()->setCellValue($rank_cell, $inc + 1);
		
		// if($i < $total_s){
			// if($name_result[$inc]->{'user_name'} == $cm_result[$i]->{'name'} && $name_result[$inc]->{'district_name'} == $cm_result[$i]->{'district'}){
			$total_cell = sprintf('C%d', $start_index);
            $this->excel->getActiveSheet()->setCellValue($total_cell, $cm_result[$inc]->{'user_name'});
			$this->excel->getActiveSheet()->getStyle($total_cell)->applyFromArray($styleThinBlackBorderOutline);
			$total_cell = sprintf('D%d', $start_index);
            $this->excel->getActiveSheet()->setCellValue($total_cell, $cm_result[$inc]->{'district_name'});
			$this->excel->getActiveSheet()->getStyle($total_cell)->applyFromArray($styleThinBlackBorderOutline);
			$total_cell = sprintf('E%d', $start_index);
            $this->excel->getActiveSheet()->setCellValue($total_cell, $cm_result[$inc]->{'block_name'});
			$this->excel->getActiveSheet()->getStyle($total_cell)->applyFromArray($styleThinBlackBorderOutline);
			$total_cell = sprintf('F%d', $start_index);
            $this->excel->getActiveSheet()->setCellValue($total_cell, $cm_result[$inc]->{'cluster_name'});
			$this->excel->getActiveSheet()->getStyle($total_cell)->applyFromArray($styleThinBlackBorderOutline);			
			
			$total_cell = sprintf('O%d', $start_index);
            $this->excel->getActiveSheet()->setCellValue($total_cell, $cm_result[$inc]->{'F_Half_day_invitees'});
			$this->excel->getActiveSheet()->getStyle($total_cell)->applyFromArray($styleThinBlackBorderOutline);
			$total_cell = sprintf('P%d', $start_index);
            $this->excel->getActiveSheet()->setCellValue($total_cell, $cm_result[$inc]->{'F_Half_day_teachers'});
			$this->excel->getActiveSheet()->getStyle($total_cell)->applyFromArray($styleThinBlackBorderOutline);
			$total_cell = sprintf('Q%d', $start_index);
            $this->excel->getActiveSheet()->setCellValue($total_cell, $cm_result[$inc]->{'F_Full_day_invitees'});
			$this->excel->getActiveSheet()->getStyle($total_cell)->applyFromArray($styleThinBlackBorderOutline);
			$total_cell = sprintf('R%d', $start_index);
            $this->excel->getActiveSheet()->setCellValue($total_cell, $cm_result[$inc]->{'F_Full_day_teachers'});
			$this->excel->getActiveSheet()->getStyle($total_cell)->applyFromArray($styleThinBlackBorderOutline);
			$total_cell = sprintf('K%d', $start_index);
            $this->excel->getActiveSheet()->setCellValue($total_cell, $cm_result[$inc]->{'S_Half_day_invitees'});
			$this->excel->getActiveSheet()->getStyle($total_cell)->applyFromArray($styleThinBlackBorderOutline);
			$total_cell = sprintf('L%d', $start_index);
            $this->excel->getActiveSheet()->setCellValue($total_cell, $cm_result[$inc]->{'S_Half_day_teachers'});
			$this->excel->getActiveSheet()->getStyle($total_cell)->applyFromArray($styleThinBlackBorderOutline);
			$total_cell = sprintf('M%d', $start_index);
            $this->excel->getActiveSheet()->setCellValue($total_cell, $cm_result[$inc]->{'S_Full_day_invitees'});
			$this->excel->getActiveSheet()->getStyle($total_cell)->applyFromArray($styleThinBlackBorderOutline);
			$total_cell = sprintf('N%d', $start_index);
            $this->excel->getActiveSheet()->setCellValue($total_cell, $cm_result[$inc]->{'S_Full_day_teachers'});
			$this->excel->getActiveSheet()->getStyle($total_cell)->applyFromArray($styleThinBlackBorderOutline);
			$total_cell = sprintf('G%d', $start_index);
            $this->excel->getActiveSheet()->setCellValue($total_cell, $cm_result[$inc]->{'T_Half_day_invitees'});
			$this->excel->getActiveSheet()->getStyle($total_cell)->applyFromArray($styleThinBlackBorderOutline);
			$total_cell = sprintf('H%d', $start_index);
            $this->excel->getActiveSheet()->setCellValue($total_cell, $cm_result[$inc]->{'T_Half_day_teachers'});
			$this->excel->getActiveSheet()->getStyle($total_cell)->applyFromArray($styleThinBlackBorderOutline);
			$total_cell = sprintf('I%d', $start_index);
            $this->excel->getActiveSheet()->setCellValue($total_cell, $cm_result[$inc]->{'T_Full_day_invitees'});
			$this->excel->getActiveSheet()->getStyle($total_cell)->applyFromArray($styleThinBlackBorderOutline);
			$total_cell = sprintf('J%d', $start_index);
            $this->excel->getActiveSheet()->setCellValue($total_cell, $cm_result[$inc]->{'T_Full_day_teachers'});
			$this->excel->getActiveSheet()->getStyle($total_cell)->applyFromArray($styleThinBlackBorderOutline);
		
			// }
			// else{
			// $total_cell = sprintf('M%d', $start_index);
            // $this->excel->getActiveSheet()->setCellValue($total_cell, '0');
			// $this->excel->getActiveSheet()->getStyle($total_cell)->applyFromArray($styleThinBlackBorderOutline);
			// $total_cell = sprintf('N%d', $start_index);
            // $this->excel->getActiveSheet()->setCellValue($total_cell, '0');
			// $this->excel->getActiveSheet()->getStyle($total_cell)->applyFromArray($styleThinBlackBorderOutline);
			// $total_cell = sprintf('O%d', $start_index);
            // $this->excel->getActiveSheet()->setCellValue($total_cell, '0');
			// $this->excel->getActiveSheet()->getStyle($total_cell)->applyFromArray($styleThinBlackBorderOutline);
			// $total_cell = sprintf('P%d', $start_index);
            // $this->excel->getActiveSheet()->setCellValue($total_cell, '0');
			// $this->excel->getActiveSheet()->getStyle($total_cell)->applyFromArray($styleThinBlackBorderOutline);
			// $total_cell = sprintf('I%d', $start_index);
            // $this->excel->getActiveSheet()->setCellValue($total_cell,'0');
			// $this->excel->getActiveSheet()->getStyle($total_cell)->applyFromArray($styleThinBlackBorderOutline);
			// $total_cell = sprintf('J%d', $start_index);
            // $this->excel->getActiveSheet()->setCellValue($total_cell, '0');
			// $this->excel->getActiveSheet()->getStyle($total_cell)->applyFromArray($styleThinBlackBorderOutline);
			// $total_cell = sprintf('K%d', $start_index);
            // $this->excel->getActiveSheet()->setCellValue($total_cell, '0');
			// $this->excel->getActiveSheet()->getStyle($total_cell)->applyFromArray($styleThinBlackBorderOutline);
			// $total_cell = sprintf('L%d', $start_index);
            // $this->excel->getActiveSheet()->setCellValue($total_cell, '0');
			// $this->excel->getActiveSheet()->getStyle($total_cell)->applyFromArray($styleThinBlackBorderOutline);
			// $total_cell = sprintf('E%d', $start_index);
            // $this->excel->getActiveSheet()->setCellValue($total_cell, '0');
			// $this->excel->getActiveSheet()->getStyle($total_cell)->applyFromArray($styleThinBlackBorderOutline);
			// $total_cell = sprintf('F%d', $start_index);
            // $this->excel->getActiveSheet()->setCellValue($total_cell, '0');
			// $this->excel->getActiveSheet()->getStyle($total_cell)->applyFromArray($styleThinBlackBorderOutline);
			// $total_cell = sprintf('G%d', $start_index);
            // $this->excel->getActiveSheet()->setCellValue($total_cell, '0');
			// $this->excel->getActiveSheet()->getStyle($total_cell)->applyFromArray($styleThinBlackBorderOutline);
			// $total_cell = sprintf('H%d', $start_index);
            // $this->excel->getActiveSheet()->setCellValue($total_cell, '0');
			// $this->excel->getActiveSheet()->getStyle($total_cell)->applyFromArray($styleThinBlackBorderOutline);				

            // counter increment
            $inc++;
            $start_index++;
        }
		
		
		// $tv1 = round((($pv2_totalvisit)),0);
		// $tv2 = round((($pv2_a/$count_3)),0);
		// $tv3 = round((($pv2_b/$count_3)),0);
		// $tv4 = round((($pv2_c/$count_3)),0);
		// $pv1 = round((($pv1_totalvisit)),0);
		// $pv2 = round((($pv1_a/$count_2)),0);
		// $pv3 = round((($pv1_b/$count_2)),0);
		// $pv4 = round((($pv1_c/$count_2)),0);
		// $cm1 = round((($cm_totalvisit)),0);
		// $cm2 = round((($cm_a/$count_1)),0);
		// $cm3 = round((($cm_b/$count_1)),0);
		// $cm4 = round((($cm_c/$count_1)),0);
		// $hlf_tot = round((($halfd)),0);
		// $full_tot = round((($fulld)),0);
		
		// $GrossTotalRow = $start_index ;
		// $totalvisitCell = 'C' . $GrossTotalRow;
        // $this->excel->getActiveSheet()->setCellValue($totalvisitCell, 'State Total');
        // $this->excel->getActiveSheet()->getStyle($totalvisitCell)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        // $this->excel->getActiveSheet()->getStyle($totalvisitCell)->getFont()->setBold(true);
		
		// $totalvisitCell = 'D' . $GrossTotalRow;
        // $this->excel->getActiveSheet()->setCellValue($totalvisitCell, $tv1);
        // $this->excel->getActiveSheet()->getStyle($totalvisitCell)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        // $this->excel->getActiveSheet()->getStyle($totalvisitCell)->getFont()->setBold(true);

		// $totalvisitCell = 'E' . $GrossTotalRow;
        // $this->excel->getActiveSheet()->setCellValue($totalvisitCell, $tv2);
        // $this->excel->getActiveSheet()->getStyle($totalvisitCell)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        // $this->excel->getActiveSheet()->getStyle($totalvisitCell)->getFont()->setBold(true);
		
		// $totalvisitCell = 'F' . $GrossTotalRow;
        // $this->excel->getActiveSheet()->setCellValue($totalvisitCell, $tv3);
        // $this->excel->getActiveSheet()->getStyle($totalvisitCell)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        // $this->excel->getActiveSheet()->getStyle($totalvisitCell)->getFont()->setBold(true);

		// $totalvisitCell = 'G' . $GrossTotalRow;
        // $this->excel->getActiveSheet()->setCellValue($totalvisitCell, $tv4);
        // $this->excel->getActiveSheet()->getStyle($totalvisitCell)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        // $this->excel->getActiveSheet()->getStyle($totalvisitCell)->getFont()->setBold(true);

		// $totalvisitCell = 'H' . $GrossTotalRow;
        // $this->excel->getActiveSheet()->setCellValue($totalvisitCell, $pv1);
        // $this->excel->getActiveSheet()->getStyle($totalvisitCell)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        // $this->excel->getActiveSheet()->getStyle($totalvisitCell)->getFont()->setBold(true);

		// $totalvisitCell = 'I' . $GrossTotalRow;
        // $this->excel->getActiveSheet()->setCellValue($totalvisitCell, $pv2);
        // $this->excel->getActiveSheet()->getStyle($totalvisitCell)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        // $this->excel->getActiveSheet()->getStyle($totalvisitCell)->getFont()->setBold(true);

		// $totalvisitCell = 'J' . $GrossTotalRow;
        // $this->excel->getActiveSheet()->setCellValue($totalvisitCell, $pv3);
        // $this->excel->getActiveSheet()->getStyle($totalvisitCell)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        // $this->excel->getActiveSheet()->getStyle($totalvisitCell)->getFont()->setBold(true);

		// $totalvisitCell = 'K' . $GrossTotalRow;
        // $this->excel->getActiveSheet()->setCellValue($totalvisitCell, $pv4);
        // $this->excel->getActiveSheet()->getStyle($totalvisitCell)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        // $this->excel->getActiveSheet()->getStyle($totalvisitCell)->getFont()->setBold(true);

		// $totalvisitCell = 'L' . $GrossTotalRow;
        // $this->excel->getActiveSheet()->setCellValue($totalvisitCell, $cm1);
        // $this->excel->getActiveSheet()->getStyle($totalvisitCell)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        // $this->excel->getActiveSheet()->getStyle($totalvisitCell)->getFont()->setBold(true);
		
		// $totalvisitCell = 'M' . $GrossTotalRow;
        // $this->excel->getActiveSheet()->setCellValue($totalvisitCell, $cm2);
        // $this->excel->getActiveSheet()->getStyle($totalvisitCell)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        // $this->excel->getActiveSheet()->getStyle($totalvisitCell)->getFont()->setBold(true);

		// $totalvisitCell = 'N' . $GrossTotalRow;
        // $this->excel->getActiveSheet()->setCellValue($totalvisitCell, $cm3);
        // $this->excel->getActiveSheet()->getStyle($totalvisitCell)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        // $this->excel->getActiveSheet()->getStyle($totalvisitCell)->getFont()->setBold(true);

		// $totalvisitCell = 'O' . $GrossTotalRow;
        // $this->excel->getActiveSheet()->setCellValue($totalvisitCell, $cm4);
        // $this->excel->getActiveSheet()->getStyle($totalvisitCell)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        // $this->excel->getActiveSheet()->getStyle($totalvisitCell)->getFont()->setBold(true);
		
		// $totalvisitCell = 'P' . $GrossTotalRow;
        // $this->excel->getActiveSheet()->setCellValue($totalvisitCell, $hlf_tot);
        // $this->excel->getActiveSheet()->getStyle($totalvisitCell)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        // $this->excel->getActiveSheet()->getStyle($totalvisitCell)->getFont()->setBold(true);

		// $totalvisitCell = 'Q' . $GrossTotalRow;
        // $this->excel->getActiveSheet()->setCellValue($totalvisitCell, $full_tot);
        // $this->excel->getActiveSheet()->getStyle($totalvisitCell)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        // $this->excel->getActiveSheet()->getStyle($totalvisitCell)->getFont()->setBold(true);
		
		
		// $GrossTotalRows = $GrossTotalRow + 1;
        // $time_mergeCell = sprintf('A%d:N%d', $GrossTotalRows, $GrossTotalRows);		
		
        $filename = 'Spark Training Detailed Report.xls'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename=' . $filename);
        header('Cache-Control: max-age=0'); //no cache
        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
    	
	}
 
	function sparkQuery($state_id,$month,$year){
		$select_stmt = "
SELECT ssu.name user_name
	,r.district_name
	,r.block_name
	,r.cluster_name
	,r.F_Full_day_invitees
	,r.F_Full_day_teachers
	,r.F_Half_day_invitees
	,r.F_Half_day_teachers
	,r.S_Full_day_invitees
	,r.S_Full_day_teachers
	,r.S_Half_day_invitees
	,r.S_Half_day_teachers
	,r.T_Full_day_invitees
	,r.T_Full_day_teachers
	,r.T_Half_day_invitees
	,r.T_Half_day_teachers
FROM ssc_users ssu
INNER JOIN (
	SELECT e.user_id
		,e.district_id
		,ssd.id
		,ssd.name district_name
		,e.block_name
		,e.cluster_name
		,e.F_Full_day_invitees
		,e.F_Full_day_teachers
		,e.F_Half_day_invitees
		,e.F_Half_day_teachers
		,e.S_Full_day_invitees
		,e.S_Full_day_teachers
		,e.S_Half_day_invitees
		,e.S_Half_day_teachers
		,e.T_Full_day_invitees
		,e.T_Full_day_teachers
		,e.T_Half_day_invitees
		,e.T_Half_day_teachers
	FROM ssc_districts ssd
	INNER JOIN (
		SELECT w.user_id
			,sscb.id
			,sscb.name block_name
			,w.cluster_name
			,sscb.district_id
			,w.F_Full_day_invitees
			,w.F_Full_day_teachers
			,w.F_Half_day_invitees
			,w.F_Half_day_teachers
			,w.S_Full_day_invitees
			,w.S_Full_day_teachers
			,w.S_Half_day_invitees
			,w.S_Half_day_teachers
			,w.T_Full_day_invitees
			,w.T_Full_day_teachers
			,w.T_Half_day_invitees
			,w.T_Half_day_teachers
		FROM ssc_blocks sscb
		INNER JOIN (
			SELECT i.user_id
				,sscl.id
				,sscl.name cluster_name
				,sscl.district_id
				,sscl.block_id
				,i.F_Full_day_invitees
				,i.F_Full_day_teachers
				,i.F_Half_day_invitees
				,i.F_Half_day_teachers
				,i.S_Full_day_invitees
				,i.S_Full_day_teachers
				,i.S_Half_day_invitees
				,i.S_Half_day_teachers
				,i.T_Full_day_invitees
				,i.T_Full_day_teachers
				,i.T_Half_day_invitees
				,i.T_Half_day_teachers
			FROM ssc_clusters sscl
			INNER JOIN (
				SELECT sstr.district_id
					,sstr.user_id
					,sstr.block_id
					,SUM(CASE 
							WHEN training_duration = '24:00'
								AND month(sstr.activity_date) = '$month'
								AND year(sstr.activity_date) = (
									SELECT year(convert(DATE_SUB('$year-$month-01', INTERVAL 0 month) using utf8))
									)
								THEN no_of_invitees
							ELSE 0
							END) AS F_Full_day_invitees
					,SUM(CASE 
							WHEN training_duration = '24:00'
								AND month(sstr.activity_date) = '$month'
								AND year(sstr.activity_date) = (
									SELECT year(convert(DATE_SUB('$year-$month-01', INTERVAL 0 month) using utf8))
									)
								THEN no_of_teachers_trained
							ELSE 0
							END) AS F_Full_day_teachers
					,SUM(CASE 
							WHEN training_duration <> '24:00'
								AND month(sstr.activity_date) = '$month'
								AND year(sstr.activity_date) = (
									SELECT year(convert(DATE_SUB('$year-$month-01', INTERVAL 0 month) using utf8))
									)
								THEN no_of_invitees
							ELSE 0
							END) AS F_Half_day_invitees
					,SUM(CASE 
							WHEN training_duration <> '24:00'
								AND month(sstr.activity_date) = '$month'
								AND year(sstr.activity_date) = (
									SELECT year(convert(DATE_SUB('$year-$month-01', INTERVAL 0 month) using utf8))
									)
								THEN no_of_teachers_trained
							ELSE 0
							END) AS F_Half_day_teachers
					,SUM(CASE 
							WHEN training_duration = '24:00'
								AND month(sstr.activity_date) = (
									SELECT month(convert(DATE_SUB('$year-$month-01', INTERVAL 1 month) using utf8))
									)
								AND year(sstr.activity_date) = (
									SELECT year(convert(DATE_SUB('$year-$month-01', INTERVAL 1 month) using utf8))
									)
								THEN no_of_invitees
							ELSE 0
							END) AS S_Full_day_invitees
					,SUM(CASE 
							WHEN training_duration = '24:00'
								AND month(sstr.activity_date) = (
									SELECT month(convert(DATE_SUB('$year-$month-01', INTERVAL 1 month) using utf8))
									)
								AND year(sstr.activity_date) = (
									SELECT year(convert(DATE_SUB('$year-$month-01', INTERVAL 1 month) using utf8))
									)
								THEN no_of_teachers_trained
							ELSE 0
							END) AS S_Full_day_teachers
					,SUM(CASE 
							WHEN training_duration <> '24:00'
								AND month(sstr.activity_date) = (
									SELECT month(convert(DATE_SUB('$year-$month-01', INTERVAL 1 month) using utf8))
									)
								AND year(sstr.activity_date) = (
									SELECT year(convert(DATE_SUB('$year-$month-01', INTERVAL 1 month) using utf8))
									)
								THEN no_of_invitees
							ELSE 0
							END) AS S_Half_day_invitees
					,SUM(CASE 
							WHEN training_duration <> '24:00'
								AND month(sstr.activity_date) = (
									SELECT month(convert(DATE_SUB('$year-$month-01', INTERVAL 1 month) using utf8))
									)
								AND year(sstr.activity_date) = (
									SELECT year(convert(DATE_SUB('$year-$month-01', INTERVAL 1 month) using utf8))
									)
								THEN no_of_teachers_trained
							ELSE 0
							END) AS S_Half_day_teachers
					,SUM(CASE 
							WHEN training_duration = '24:00'
								AND month(sstr.activity_date) = (
									SELECT month(convert(DATE_SUB('$year-$month-01', INTERVAL 2 month) using utf8))
									)
								AND year(sstr.activity_date) = (
									SELECT year(convert(DATE_SUB('$year-$month-01', INTERVAL 2 month) using utf8))
									)
								THEN no_of_invitees
							ELSE 0
							END) AS T_Full_day_invitees
					,SUM(CASE 
							WHEN training_duration = '24:00'
								AND month(sstr.activity_date) = (
									SELECT month(convert(DATE_SUB('$year-$month-01', INTERVAL 2 month) using utf8))
									)
								AND year(sstr.activity_date) = (
									SELECT year(convert(DATE_SUB('$year-$month-01', INTERVAL 2 month) using utf8))
									)
								THEN no_of_teachers_trained
							ELSE 0
							END) AS T_Full_day_teachers
					,SUM(CASE 
							WHEN training_duration <> '24:00'
								AND month(sstr.activity_date) = (
									SELECT month(convert(DATE_SUB('$year-$month-01', INTERVAL 2 month) using utf8))
									)
								AND year(sstr.activity_date) = (
									SELECT year(convert(DATE_SUB('$year-$month-01', INTERVAL 2 month) using utf8))
									)
								THEN no_of_invitees
							ELSE 0
							END) AS T_Half_day_invitees
					,SUM(CASE 
							WHEN training_duration <> '24:00'
								AND month(sstr.activity_date) = (
									SELECT month(convert(DATE_SUB('$year-$month-01', INTERVAL 2 month) using utf8))
									)
								AND year(sstr.activity_date) = (
									SELECT year(convert(DATE_SUB('$year-$month-01', INTERVAL 2 month) using utf8))
									)
								THEN no_of_teachers_trained
							ELSE 0
							END) AS T_Half_day_teachers
				FROM ssc_trainings sstr
				INNER JOIN ssc_users ssu ON ssu.district_id = sstr.district_id
					AND ssu.id = sstr.user_id
					AND sstr.state_id = '$state_id'
				GROUP BY sstr.district_id
					,sstr.user_id
				) i ON sscl.block_id = i.block_id
			GROUP BY cluster_name
			) w ON w.block_id = sscb.id
		GROUP BY block_name
		) e ON e.district_id = ssd.id
	GROUP BY district_name
	) r ON r.user_id = ssu.id
GROUP BY user_name";
       $select_query = $this->db->query($select_stmt);
       
	//Execute query 
        $row = $select_query->result();

        return ($row);
 }

	function districtName($state_id){
		$select_stmt = "SELECT ssu.name user_name,ssd.name district_name FROM `ssc_users` ssu
		inner join ssc_districts ssd 
		on ssu.district_id = ssd.id
		where ssu.state_id = '$state_id'";
       $select_query = $this->db->query($select_stmt);
       
	//Execute query 
        $row = $select_query->result();
		// echo("<pre>");
		// print_r($row);
		// die();
        return ($row);
 } 
 }
 ?>
