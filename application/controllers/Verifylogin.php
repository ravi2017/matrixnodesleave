<?php
 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Verifylogin extends CI_Controller {

 function __construct()
 {
   parent::__construct();
   $this->output->enable_profiler(FALSE);
   $this->load->model('users_model');
 }

 function index()
 {
   //This method will have the credentials validation
   $this->load->helper('security');
   $this->load->library('form_validation');

   $this->form_validation->set_rules('username', 'username', 'trim|required|xss_clean');
   $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean|callback_check_database');

   if($this->form_validation->run() == FALSE)
   {
     //Field validation failed.  User redirected to login page
     $this->load->view('templates/login_view');
   }
   else
   {
     //Go to private area
      redirect('', 'refresh');
   }

 }

  function check_database($password)
  {
    //Field validation succeeded.  Validate against database
    $email = $this->input->post('username');
    //query the database
    $result = $this->users_model->login($email, $password);

    if($result)
    {
      $sess_array = array();
      foreach($result as $row)
      {
        $data['sso_token'] = md5(time());
        $sess_array = array(
          'id' => $row->id,
          'username' => $row->login_id,
          'name' => $row->name,
          'role' => $row->role,
          'state_id' => $row->state_id,
          'district_id' => $row->district_id,
          'login_id' => $row->login_id,
          'user_group' => $row->user_group,
          'sso_token' => $data['sso_token']
        );
        
        $this->session->set_userdata('logged_in', $sess_array);
       
        if ($row->role == "super_admin")
        {
          $this->session->set_userdata('admin_logged_in', $sess_array);
        }
        
        $this->users_model->update_info($data, $row->id);
      }
      return TRUE;
    }
    else
    {
      $this->form_validation->set_message('check_database', 'Invalid username or password');
      return false;
    }
  }
}
?>
