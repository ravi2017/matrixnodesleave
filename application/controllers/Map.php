<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Map extends CI_Controller {	 
  
  function __construct()  {    
    parent::__construct();
    $this->output->enable_profiler(FALSE);    
    $this->load->helper('url');    
    $this->load->model('Users_model'); 
    $this->load->model('tracking_model'); 
    $this->load->model('Claim_model'); 
		$this->load->library('Common_functions');
	
    if($this->session->userdata('logged_in'))
		{
      $session_data = $this->session->userdata('logged_in');
      $this->data['current_user_id'] = $session_data['id'];
      $this->data['current_username'] = $session_data['username'];
      $this->data['current_name'] = $session_data['name'];
      $this->data['current_role'] = $session_data['role'];
      $this->data['user_state_id'] = $session_data['state_id'];
      $this->data['user_district_id'] = $session_data['district_id'];
      $this->data['current_group'] = $session_data['user_group'];
      
      $this->data['OTHER_CLAIM_TYPES'] = unserialize(OTHER_CLAIM_TYPES);
      $this->data['STATES_ARR'] = unserialize(STATES_ARR);
    }
    else
    {
      //If no session, redirect to login page
      redirect('login', 'refresh');
    }
    $current_date=date('d');
    $lstedate=$current_date-4;
    $todaydate = Array();
    for($i=$current_date; $i>=$lstedate; $i--){
      //echo $i;
       array_push($todaydate,$i);
    }
    $this->data['todaydate'] = $todaydate;
  }		

  public function index()		
  {				   
    $map_date = date("d-m-Y");
    $spark_id = $this->data['current_user_id'];
    $state_id = 0;
    $spark_state_id = 0;
    if($this->data['current_role'] == 'state_person' || $this->data['current_role'] == 'manager'){
      $spark_state_id = $this->data['user_state_id'];
    }
    if (isset($_GET['spark_id']) and $_GET['spark_id'] > 0)
    {
      $spark_state_id = (isset($_GET['spark_state_id']) ? $_GET['spark_state_id'] : 0);
      $spark_detail = $_GET['spark_id'];
      $spark_details = explode("-",$spark_detail);
      $spark_id = $spark_details[0];
    }
    if (isset($_GET['activitydate']) and $_GET['activitydate'] != '')
    {
      $map_date = (isset($_GET['activitydate']) ? $_GET['activitydate'] : $map_date);
    }
    
    $activities = $this->tracking_model->getAll($spark_id, $map_date);
    
    $attendance = $this->tracking_model->getAttendance($spark_id, $map_date, 'in', 'tracking_time asc', 1);
    
    $trackings = array();
    if (count($attendance) == 1)
    {
      $attendance_in = $attendance[0];

      $tracking = array();
      $string = str_replace("\n", "", $attendance_in->tracking_location);
      $attendance_in->tracking_location = str_replace("\r", "", $string);
      $tracking['track_id'] = $attendance_in->id;
      $tracking['activity'] = ucwords("Attendance-IN");
      $tracking['time']['in'] = date("d-m-Y H:i",strtotime($attendance_in->tracking_time));
      //$tracking['time'] = date("d-m-Y H:i",strtotime($attendance_in->tracking_time));
      $tracking['latitude'] = $attendance_in->tracking_latitude;
      $tracking['longitude'] = $attendance_in->tracking_longitude;
      $tracking['location'] = $attendance_in->tracking_location;
      $tracking['distance'] = $attendance_in->distance_val;
      $tracking['duration'] = '';
      $tracking['place'] =  '';
      $tracking['marker'] = 'attendance_in.png';
      $tracking['created_by'] = '';
      array_push($trackings,$tracking);
    }
    
    foreach($activities as $activity)
    {
      //$type_detail = explode("-",$activity->activity_type);
      $type_detail = $activity->activity_type;
      $string = str_replace("\n", "", $activity->tracking_location);
      $activity->tracking_location = str_replace("\r", "", $string);
      switch($type_detail)
      {
        case 'schoolvisit':
                          $tracking = array();
                          $tracking['activity'] = 'School Visit';
                          $tracking['track_id'] = $activity->id;
                          $tracking['time']['in'] = date("d-m-Y H:i",strtotime($activity->tracking_time));
                          //$tracking['time'][$type_detail] = date("d-m-Y H:i",strtotime($activity->tracking_time));
                          //$tracking['time'] = date("d-m-Y H:i",strtotime($activity->tracking_time));
                          $tracking['latitude'] = $activity->tracking_latitude;
                          $tracking['longitude'] = $activity->tracking_longitude;
                          $tracking['location'] = $activity->tracking_location;
                          $tracking['distance'] = $activity->distance_val;
                          $DataActivity = get_activity_data($activity->activity_id, 'schoolvisit');
                          $tracking['place'] = $DataActivity['name']; 
                          $tracking['duration'] = $DataActivity['duration'];
                          $tracking['marker'] = 'schoolvisit.png';
                          $tracking['created_by'] = '';
                            array_push($trackings,$tracking);
                          break;
        case 'review_meeting':
                           $tracking = array();
                          //$tracking['activity'] = ucwords($type_detail);
                          $tracking['activity'] = 'Meeting';
                          $tracking['track_id'] = $activity->id;
                          $tracking['time']['in'] = date("d-m-Y H:i",strtotime($activity->tracking_time));
                          //$tracking['time'][$type_detail] = date("d-m-Y H:i",strtotime($activity->tracking_time));
                          //$tracking['time'] = date("d-m-Y H:i",strtotime($activity->tracking_time));
                          $tracking['latitude'] = $activity->tracking_latitude;
                          $tracking['longitude'] = $activity->tracking_longitude;
                          $tracking['location'] = $activity->tracking_location;
                          $tracking['distance'] = $activity->distance_val;
                          $tracking['distance'] = $activity->distance_val;
                          $DataActivity = get_activity_data($activity->activity_id, 'review_meeting');
                          $tracking['place'] = "";
                          $tracking['duration'] = $DataActivity['duration'];
                          $tracking['marker'] = 'meeting.png';
                          $tracking['created_by'] = '';
                            array_push($trackings,$tracking);
                          break;
        case 'training':
                          $tracking = array();
                          $tracking['activity'] = 'Training';
                          $tracking['track_id'] = $activity->id;
                          $tracking['time']['in'] = date("d-m-Y H:i",strtotime($activity->tracking_time));
                          //$tracking['time'][$type_detail] = date("d-m-Y H:i",strtotime($activity->tracking_time));
                          //$tracking['time'] = date("d-m-Y H:i",strtotime($activity->tracking_time));
                          $tracking['latitude'] = $activity->tracking_latitude;
                          $tracking['longitude'] = $activity->tracking_longitude;
                          $tracking['location'] = $activity->tracking_location;
                          $tracking['distance'] = $activity->distance_val;
                          $DataActivity = get_activity_data($activity->activity_id, 'training');
                          $tracking['place'] = "";
                          $tracking['duration'] = $DataActivity['duration'];
                          $tracking['marker'] = 'training.png';
                          $tracking['created_by'] = '';
                            array_push($trackings,$tracking);
                          break;
         case 'distance_travel':
                          $tracking = array();
                          $tracking['activity'] = 'Distance-Travel';
                          $tracking['track_id'] = $activity->id;
                          $tracking['time']['in'] = date("d-m-Y H:i",strtotime($activity->tracking_time));
                          $tracking['latitude'] = $activity->tracking_latitude;
                          $tracking['longitude'] = $activity->tracking_longitude;
                          $tracking['location'] = $activity->tracking_location;
                          $tracking['distance'] = $activity->distance_val;
                          $DataActivity = get_activity_data($activity->activity_id, 'distance_travel');
                          $tracking['place'] = "";
                          $tracking['duration'] = $DataActivity['duration'];
                          $tracking['marker'] = 'longdistance.png';
                          $tracking['created_by'] = '';
                          array_push($trackings,$tracking);
                          break;                     
      }
    }
      
    $attendance = $this->tracking_model->getAttendance($spark_id, $map_date, 'out', 'tracking_time desc', 1);
    if (count($attendance) == 1)
    {
      $attendance_out = $attendance[0];

      $string = str_replace("\n", "", $attendance_out->tracking_location);
      $attendance_out->tracking_location = str_replace("\r", "", $string);
      $tracking = array();
      $tracking['activity'] = ucwords("Attendance-OUT");
      $tracking['track_id'] = $attendance_out->id;
      $tracking['time']['out'] = date("d-m-Y H:i",strtotime($attendance_out->tracking_time));
      //$tracking['time'] = date("d-m-Y H:i",strtotime($attendance_out->tracking_time));
      $tracking['latitude'] = $attendance_out->tracking_latitude;
      $tracking['longitude'] = $attendance_out->tracking_longitude;
      $tracking['location'] = $attendance_out->tracking_location;
      $tracking['distance'] = $attendance_out->distance_val;
      $tracking['duration'] = '';
      $tracking['marker'] = 'attendance_out.png';
      $tracking['created_by'] = ($attendance_out->created_by == 'system' ?  'System' : '');
      array_push($trackings,$tracking);
    }
    //print_r($trackings);die;
    $this->data['activities'] = $trackings;
    $google_map_api = "https://maps.googleapis.com/maps/api/js?key=".GOOGLE_API_KEY_MAP."&callback=initMap";
    //$google_map_api = "https://maps.googleapis.com/maps/api/js?key=".GOOGLE_API_KEY;
    $custom_styles = []; 
    //$custom_scripts = [$google_map_api, 'map.js']; 
    $custom_scripts = [$google_map_api,'googlemap1.js?ver=1.2','map.js']; 
    $this->data['current_role'] = $this->data['current_role'];     
    $this->data['custom_scripts'] = $custom_scripts;     
    $this->data['custom_styles'] = $custom_styles;     
    $this->data['map_date'] = $map_date;
    $this->data['spark_id'] = $spark_id;
    $this->data['spark_state_id'] = $spark_state_id;
    $this->data['partial'] = 'map/index2';			
    $this->load->view('templates/index', $this->data);
  }
  

  public function save_trcking()
  {
    $source_ids = explode(',', $_POST['sids']);
    $destination_ids = explode(',', $_POST['dids']);
    $travel_distance = explode(',', $_POST['distance']);
        
    for($i=0; $i<count($source_ids); $i++){    
      
      $check_claims = $this->tracking_model->get_claims($source_ids[$i], $destination_ids[$i]);
      if(count($check_claims) == 0){
        $data['track_source_id'] =  $source_ids[$i];
        $data['track_destination_id'] =  $destination_ids[$i];
        $data['travel_distance'] =  $travel_distance[$i];
        $data['claimed_by'] = $this->data['current_user_id'];
        $data['claimed_on'] = date('Y-m-d');
        $this->tracking_model->add_claims($data);
      }
    }
  }
  
  public function claims_list()
  {
    $spark_id = $this->data['current_user_id'];
    $sel_spark_id = $this->data['current_user_id'];
    $state_id = 0;
    $spark_state_id = 0;
    if($this->data['current_role'] == 'state_person' || $this->data['current_role'] == 'manager'){
      $spark_state_id = $this->data['user_state_id'];
    }
    if (isset($_POST['spark_id']) and $_POST['spark_id'] > 0)
    {
      $spark_state_id = (isset($_POST['spark_state_id']) ? $_POST['spark_state_id'] : 0);
      $spark_detail = $_POST['spark_id'];
      $spark_details = explode("-",$spark_detail);
      $spark_id = $spark_details[0];
      $sel_spark_id = $spark_id;
    }
    $this->load->Model('Sparks_model');
    $spark_data = $this->Sparks_model->getById($spark_id);
    $spark_name = $spark_data['name'];
    $this->data['current_group'] = $spark_data['user_group'];
    
    $claim_data = array();
    $activitydatestart = date('Y-m-d');
    $activitydateend = date('Y-m-d');
    if(isset($_POST) && !empty($_POST['submit']))
    {
      $activitydatestart = (!empty($_POST['activitydatestart']) ? date('Y-m-d',strtotime($_POST['activitydatestart'])) : date('Y-m-d'));
      $activitydateend = (!empty($_POST['activitydateend']) ? date('Y-m-d',strtotime($_POST['activitydateend'])) : date('Y-m-d'));
    }
    
    $claim_data = $this->tracking_model->get_track_claims_list($spark_id, $activitydatestart, $activitydateend);
    
    $this->load->model('travels_model');   
    $travels_mode = $this->travels_model->get_all();

    $custom_scripts = ['map.js']; 
    $this->data['activitydatestart'] = date('d-m-Y',strtotime($activitydatestart)); 
    $this->data['activitydateend'] = date('d-m-Y',strtotime($activitydateend)); 
    $this->data['custom_scripts'] = $custom_scripts; 
    $this->data['travels_mode'] = $travels_mode;
    $this->data['claim_data'] = $claim_data;
    $this->data['sel_spark_id'] = $sel_spark_id;
    $this->data['spark_id'] = $spark_id;
    $this->data['spark_name'] = $spark_name;
    $this->data['spark_state_id'] = $spark_state_id;
    
    $download = (isset($_POST['download']) and $_POST['download'] == 1 ? $_POST['download'] : 0);
    $pdf = (isset($_POST['pdf']) and $_POST['pdf'] == 1 ? $_POST['pdf'] : 0);
    
		if ($download == 1)
    {
      $myFile = "./uploads/reports/claim_list.xls";
      $this->load->library('parser');
			$this->data['download'] = $download;
      //pass retrieved data into template and return as a string
      $stringData = $this->parser->parse('map/claims_list', $this->data, true);
      //open excel and write string into excel
      $fh = fopen($myFile, 'w') or die("can't open file");
      fwrite($fh, $stringData);
      fclose($fh);
      //download excel file
      $this->common_functions->downloadExcel("claim_list");
      unlink($myFile);
    }
    else if($pdf == 1)
    {
        //pass retrieved data into template and return as a string
        $this->data['pdf'] = 1;
        $summary = $this->load->view('map/claims_list',$this->data,true);
        $this->common_functions->create_pdf($summary,'claim_list');
    }
    else{
      $this->data['partial'] = 'map/claims_list';			
      $this->load->view('templates/index', $this->data);
    }
  }
  
  public function process_claim()
  {
    if(isset($_POST) && $_POST['processClaim'] == 'Process Claim'){
      
      $spark_id = $_POST['spark_id'];
      $record_ids = $_POST['record_id'];
      
      $sparkData = $this->Users_model->getById($spark_id);
      $tally_app_no = $sparkData['tally_app_no'];
      
      $count = count($record_ids);
      for($i=0; $i<$count; $i++)
      {
        $rid = $record_ids[$i];
        //echo"<br>".$_POST['claim_discard'][$rid];
        $sdata = array();
        if($_POST['claim_discard'][$rid] != 1 && $_POST['claim_processed'][$rid] != 1){
					
					$claim_exceed = 0;
					$tracking_date = date('Y-m-d', strtotime($_POST['tracking_time'][$i]));
					$tracking_month_yr = date('Y-m', strtotime($_POST['tracking_time'][$i]));
					
					$spark_month_expense = $this->tracking_model->check_month_expense($spark_id, $tracking_month_yr); 
          
          if(!empty($spark_month_expense))
          {
						$month_total_expense = $spark_month_expense['total_expense'];
						
						if($month_total_expense > CLAIM_MONTHLY_LIMIT)
						{
							$sdata['discard_reason'] = 'Montly claim limit exceed.';
							$sdata['claim_discard'] = 1;
							$sdata['updated_on'] = date('Y-m-d H:i:s');
							$sdata['updated_by'] = $this->data['current_user_id'];
							$sdata['process_date'] = date('Y-m-d');
							$res = $this->tracking_model->update_claims($rid, $sdata);
							
							$claim_exceed = 1;
						}
						else{
							$date_expense_data = $this->tracking_model->check_date_expense($spark_id, $tracking_date);		
							
							if(!empty($date_expense_data))
							{
								$date_expense_id = $date_expense_data['id'];
								$date_expense = $date_expense_data['date_expense'];
								
								$udata['date_expense'] = $date_expense+$_POST['travel_cost'][$i];
								
								$this->tracking_model->update_date_expense($udata, $date_expense_id);
							}
							else{
								$sdata['spark_id'] = $spark_id;
								$sdata['tracking_date'] = $tracking_date;
								$sdata['tracking_month'] = $tracking_month_yr;
								$sdata['date_expense'] = $_POST['travel_cost'][$i];
								$sdata['date_km'] = $_POST['manualdistance'][$i];
								
								$this->tracking_model->insert_date_expense($sdata);
							}
						}
					}
					else{
						$sdata['spark_id'] = $spark_id;
						$sdata['tracking_date'] = $tracking_date;
						$sdata['tracking_month'] = $tracking_month_yr;
						$sdata['date_expense'] = $_POST['travel_cost'][$i];
						$sdata['date_km'] = $_POST['manualdistance'][$i];
						
						$this->tracking_model->insert_date_expense($sdata);
					}
					
					if($claim_exceed == 0){
						$tmode_arr = explode('_', $_POST['travel_mode'][$i]);
						$travel_mode_id = $tmode_arr[0];
						$data['travel_mode_id'] = $travel_mode_id;
						$data['manual_distance'] = $_POST['manualdistance'][$i];
						$data['travel_cost'] = $_POST['travel_cost'][$i];
						$data['remarks'] = $_POST['remarks'][$i];
						$data['claimed_by'] = $this->data['current_user_id'];
						$data['updated_by'] = $this->data['current_user_id'];
						$data['updated_on'] = date('Y-m-d');
						$data['is_claimed'] = 1;
						$data['tally_app_no'] = $tally_app_no+1;
						$data['process_date'] = date('Y-m-d');
						//print_r($data);
						
						$this->tracking_model->update_claims($rid, $data);
					}
        }
      }
      $this->Users_model->update_spark_tally_app_no($spark_id);
    }
    else if(isset($_POST) && $_POST['processClaim'] == 'Update Claim'){
			
			//print_r($_POST);	
			//die;
      $record_ids = $_POST['record_id'];
      
      $count = count($record_ids);
      for($i=0; $i<$count; $i++)
      {
        $rid = $record_ids[$i];
        if($_POST['claim_discard'][$rid] != 1 && $_POST['claim_processed'][$rid] != 1){
          $tmode_arr = explode('_', $_POST['travel_mode'][$i]);
          $travel_mode_id = $tmode_arr[0];
          $data['travel_mode_id'] = $travel_mode_id;
          $data['manual_distance'] = $_POST['manualdistance'][$i];
          $data['travel_cost'] = $_POST['travel_cost'][$i];
          $data['remarks'] = $_POST['remarks'][$i];
          $data['updated_by'] = $this->data['current_user_id'];
          $data['updated_on'] = date('Y-m-d');
          
          $this->tracking_model->update_claims($rid, $data);
        }
      }
    }
    
    $this->session->set_flashdata('success', 'Claim process successfully.');
    header('location:'.base_url()."map/claims_list");
  }
  
  public function discard_claim()
  {
    $id = $_POST['claim_id'];
    $claims = $this->tracking_model->get_claims('', '', $id);
    extract ($claims);
    $mdata['discard_reason'] = $_POST['reason'];
    $mdata['claim_discard'] = 1;
    $mdata['updated_on'] = date('Y-m-d H:i:s');
    $mdata['updated_by'] = $this->data['current_user_id'];
    $mdata['process_date'] = date('Y-m-d');
    $res = $this->tracking_model->update_claims($id, $mdata);
    //echo $return_status;
  }
  
  public function approved_claim()
  {
    $id = $_POST['claim_id'];
    $claims = $this->tracking_model->get_claims('', '', $id);
    extract ($claims);
    $mdata['approved_reason'] = $_POST['reason'];
    $mdata['claim_discard'] = 0;
    $mdata['updated_on'] = date('Y-m-d H:i:s');
    $mdata['updated_by'] = $this->data['current_user_id'];
    $res = $this->tracking_model->update_claims($id, $mdata);
    //echo $return_status;
  }
  
  public function claims_exceed()
  {
    $spark_id = $this->data['current_user_id'];
    $sel_spark_id = $this->data['current_user_id'];
    $spark_ids = array($spark_id);
    $state_id = 0;
    $spark_state_id = 0;
    $spark_name = '';
    if($this->data['current_role'] == 'state_person' || $this->data['current_role'] == 'manager'){
      $spark_state_id = $this->data['user_state_id'];
    }
    
    $spark_name = 0;
    $exceed_value = 0;
    $claim_data = array();
    $spark_names = array();
    $activitydatestart = date('Y-m-d');
    $activitydateend = date('Y-m-d');
    $spark_names = array();
    $data_submit = '';
    $travel_mode_id = '';
    $sel_claim_type = '';
    if(isset($_POST) && !empty($_POST['submit']))
    {
      $activitydatestart = (!empty($_POST['activitydatestart']) ? date('Y-m-d',strtotime($_POST['activitydatestart'])) : date('Y-m-d'));
      $activitydateend = (!empty($_POST['activitydateend']) ? date('Y-m-d',strtotime($_POST['activitydateend'])) : date('Y-m-d'));
      
      $spark_state_id = (isset($_POST['spark_state_id']) ? $_POST['spark_state_id'] : 0);
      $travel_mode_id = (isset($_POST['travel_mode']) ? $_POST['travel_mode'] : '');
      $sel_claim_type = (isset($_POST['claim_type']) ? $_POST['claim_type'] : '');
      $exceed_value = $_POST['exceed_value'];
      if (isset($_POST['spark_id']) and $_POST['spark_id'] > 0)
      {
        $spark_detail = $_POST['spark_id'];
        $spark_details = explode("-",$spark_detail);
        $spark_id = $spark_details[0];
        $sel_spark_id = $spark_id;
        $spark_ids = array($spark_id);
        $this->load->Model('Sparks_model');
        $spark_data = $this->Sparks_model->getById($spark_id);
        $spark_name = $spark_data['name'];
        $this->data['current_group'] = $spark_data['user_group'];
      }  
      else if($spark_state_id > 0 || $spark_state_id == 'HO' ) 
      {
        $this->load->Model('Users_model');
        
        if($_POST['spark_state_id'] == 'HO'){
					$state = 0;
					$spark_role = array('manager', 'accounts', 'field_user');
					$user_group = array('head_office');  
					$users_data = $this->Users_model->getAllSparksByState($state, $activitydatestart, $activitydateend, '', $spark_role, $user_group);
				}
				else{
					$users_data = $this->Users_model->getSparkByStateDuration($_POST['spark_state_id'], $activitydatestart, $activitydateend,'', array('field_user','manager','state_person'));
					$this->data['current_group'] = 'field_user';
				}
        $spark_id = '';
        $sel_spark_id = '';
        $spark_ids = array();
        foreach($users_data as $users){
          if(!in_array($users['user_id'], $spark_ids)){
            array_push($spark_ids, $users['user_id']);
            $spark_names[$users['user_id']]= $users['name'];
          }
        }
      }
      $data_submit = 'submitted';
    }
    $claim_data = $this->tracking_model->get_track_exceed_claims_list($spark_ids, $activitydatestart, $activitydateend, $travel_mode_id, $sel_claim_type);
    //print_r($claim_data);
    
    $this->load->model('travels_model');   

    $travels_mode = $this->travels_model->get_all();
    
    $process_dates = $this->Claim_model->get_process_dates();

    $custom_scripts = ['map.js']; 
    $this->data['travel_mode_id'] = $travel_mode_id;
    $this->data['sel_claim_type'] = $sel_claim_type;
    $this->data['process_dates'] = $process_dates; 
    $this->data['data_submit'] = $data_submit; 
    $this->data['exceed_value'] = $exceed_value; 
    $this->data['activitydatestart'] = date('d-m-Y',strtotime($activitydatestart)); 
    $this->data['activitydateend'] = date('d-m-Y',strtotime($activitydateend)); 
    $this->data['custom_scripts'] = $custom_scripts; 
    $this->data['travels_mode'] = $travels_mode;
    $this->data['claim_data'] = $claim_data;
    $this->data['spark_id'] = $spark_id;
    $this->data['sel_spark_id'] = $sel_spark_id;
    $this->data['spark_name'] = $spark_name;
    $this->data['spark_names'] = $spark_names;
    $this->data['spark_state_id'] = $spark_state_id;
    
    $download = (isset($_POST['download']) and $_POST['download'] == 1 ? $_POST['download'] : 0);
    $pdf = (isset($_POST['pdf']) and $_POST['pdf'] == 1 ? $_POST['pdf'] : 0);
    
		if ($download == 1)
    {
      $myFile = "./uploads/reports/claims_exceed.xls";
      $this->load->library('parser');
			$this->data['download'] = $download;
      //pass retrieved data into template and return as a string
      $stringData = $this->parser->parse('map/claims_exceed', $this->data, true);
      //open excel and write string into excel
      $fh = fopen($myFile, 'w') or die("can't open file");
      fwrite($fh, $stringData);
      fclose($fh);
      //download excel file
      $this->common_functions->downloadExcel("claims_exceed");
      unlink($myFile);
    }
    else if($pdf == 1)
    {
        //pass retrieved data into template and return as a string
        $this->data['pdf'] = 1;
        $summary = $this->load->view('map/claims_exceed',$this->data,true);
        $this->common_functions->create_pdf($summary,'claims_exceed');
    }
    else{
      $this->data['partial'] = 'map/claims_exceed';			
      $this->load->view('templates/index', $this->data);
    }
  }
  
  public function claims_import()
  {
    $spark_id = $this->data['current_user_id'];
    $state_id = 0;
    $spark_state_id = 0;
    if($this->data['current_role'] == 'state_person' || $this->data['current_role'] == 'manager'){
      $spark_state_id = $this->data['user_state_id'];
    }

    $spark_ids = array();
    $claim_data = array();
    $activitydatestart = date('Y-m-d');
    $activitydateend = date('Y-m-d');
    
    if(isset($_POST) && !empty($_POST['tallyButton']))
    {
      $process_date = (!empty($_POST['process_date']) ? date('Y-m-d',strtotime($_POST['process_date'])) : date('Y-m-d'));
      $spark_state_id = (isset($_POST['spark_state_id']) ? $_POST['spark_state_id'] : 0);
      
      $this->load->Model('Users_model');
      if($_POST['spark_state_id'] == 'HO'){
				$state = 0;
				$spark_role = array('manager', 'accounts', 'field_user');
				$user_group = array('head_office');  
				$users_data = $this->Users_model->getSparksByCurrentState($state, $spark_role, $user_group);
			}
			else{
				$users_data = $this->Users_model->getSparksByCurrentState($_POST['spark_state_id'], array('field_user','manager','state_person'), array('field_user'));
			}
      
      $sel_spark_id = '';
      $spark_ids = array();
      foreach($users_data as $users){
        if(!in_array($users->id, $spark_ids)){
          array_push($spark_ids, $users->id);
          $spark_names[$users->id]= $users->name;
        }
      }
      
      $spark_data = $this->Users_model->getById($spark_id);
      $spark_name = $spark_data['name'];
      
      $claim_data = $this->tracking_model->get_track_tally_list($spark_ids, $process_date);
      
      if(!empty($claim_data)){
        $this->data['claim_data'] = $claim_data;
        
        $this->load->model('travels_model');   
        $travels_mode = $this->travels_model->get_all();
        
        $this->data['report_type'] = $_POST['tallyButton'];
        $this->data['travels_mode'] = $travels_mode;
        $this->data['claim_data'] = $claim_data;
        $this->data['spark_name'] = $spark_name;
        $this->data['spark_state_id'] = $spark_state_id;
        
        $myFile = "./uploads/reports/claim_import.xls";
        $this->load->library('parser');
        //pass retrieved data into template and return as a string
        $stringData = $this->parser->parse('map/claims_import', $this->data, true);
        //open excel and write string into excel
        $fh = fopen($myFile, 'w') or die("can't open file");
        fwrite($fh, $stringData);
        fclose($fh);
        //download excel file
        $this->common_functions->downloadExcel("claim_import");
        unlink($myFile);
      }
      else{
        $this->session->set_flashdata('alert', "No claim found for selected process date.");
        redirect('map/claims_exceed');
      }
    }
  }
  
  
  public function claims_import_bk()
  {
    $spark_id = $this->data['current_user_id'];
    $state_id = 0;
    $spark_state_id = 0;
    if($this->data['current_role'] == 'state_person' || $this->data['current_role'] == 'manager'){
      $spark_state_id = $this->data['user_state_id'];
    }
    /*if (isset($_POST['spark_id']) and $_POST['spark_id'] > 0)
    {
      $spark_state_id = (isset($_POST['spark_state_id']) ? $_POST['spark_state_id'] : 0);
      $spark_detail = $_POST['spark_id'];
      $spark_details = explode("-",$spark_detail);
      $spark_id = $spark_details[0];
    }*/
    
    $spark_ids = array();
    $claim_data = array();
    $activitydatestart = date('Y-m-d');
    $activitydateend = date('Y-m-d');
    
    if(isset($_POST) && !empty($_POST['tallyButton']))
    {
      
      $activitydatestart = (!empty($_POST['activitydatestart']) ? date('Y-m-d',strtotime($_POST['activitydatestart'])) : date('Y-m-d'));
      $activitydateend = (!empty($_POST['activitydateend']) ? date('Y-m-d',strtotime($_POST['activitydateend'])) : date('Y-m-d'));
      
      $spark_state_id = (isset($_POST['spark_state_id']) ? $_POST['spark_state_id'] : 0);
      
      if (isset($_POST['spark_id']) and $_POST['spark_id'] > 0)
      {
        $spark_detail = $_POST['spark_id'];
        $spark_details = explode("-",$spark_detail);
        $spark_id = $spark_details[0];
        $sel_spark_id = $spark_id;
        $spark_ids = array($spark_id);
      }  
      else if($spark_state_id > 0)
      {
        $this->load->Model('Users_model');
        $users_data = $this->Users_model->getSparkByStateDuration($_POST['spark_state_id'], $activitydatestart, $activitydateend,'', array('field_user','manager','state_person'));
        $sel_spark_id = '';
        $spark_ids = array();
        foreach($users_data as $users){
          if(!in_array($users['user_id'], $spark_ids)){
            array_push($spark_ids, $users['user_id']);
            $spark_names[$users['user_id']]= $users['name'];
          }
        }
      }
    }
    $this->load->Model('Sparks_model');
    $spark_data = $this->Sparks_model->getById($spark_id);
    $spark_name = $spark_data['name'];
    
    $claim_data = $this->tracking_model->get_track_tally_list($spark_ids, $activitydatestart, $activitydateend);
    //print_r($claim_data);
    //die;
    $this->load->model('travels_model');   
    $travels_mode = $this->travels_model->get_all();

    $custom_scripts = ['map.js']; 
    $this->data['activitydatestart'] = date('d-m-Y',strtotime($activitydatestart)); 
    $this->data['activitydateend'] = date('d-m-Y',strtotime($activitydateend)); 
    $this->data['custom_scripts'] = $custom_scripts; 
    $this->data['travels_mode'] = $travels_mode;
    $this->data['claim_data'] = $claim_data;
    $this->data['spark_id'] = $spark_id;
    $this->data['spark_name'] = $spark_name;
    $this->data['spark_state_id'] = $spark_state_id;
    
    $download = (isset($_POST['tally_download']) and $_POST['tally_download'] == 1 ? $_POST['tally_download'] : 0);
    
		if ($download == 1)
    {
      $myFile = "./uploads/reports/claim_import.xls";
      $this->load->library('parser');
			$this->data['download'] = $download;
      //pass retrieved data into template and return as a string
      $stringData = $this->parser->parse('map/claims_import', $this->data, true);
      //open excel and write string into excel
      $fh = fopen($myFile, 'w') or die("can't open file");
      fwrite($fh, $stringData);
      fclose($fh);
      //download excel file
      $this->common_functions->downloadExcel("claim_import");
      unlink($myFile);
    }
    else{
      $this->data['partial'] = 'map/claims_list';			
      $this->load->view('templates/index', $this->data);
    }
  }
  
  
  public function auto_claims_process()
  {
    $spark_id = $this->data['current_user_id'];
    $state_id = 0;
    $spark_state_id = 0;
    if($this->data['current_role'] == 'state_person' || $this->data['current_role'] == 'manager'){
      $spark_state_id = $this->data['user_state_id'];
    }
    
    $spark_ids = array();
    $claim_data = array();
    $activitydatestart = date('Y-m-d');
    $activitydateend = date('Y-m-d');
    
    $this->load->model('travels_model');   
    $travels_mode = $this->travels_model->get_all();
    
    if(isset($_POST) && !empty($_POST['claimProcess']))
    {
      $activitydatestart = (!empty($_POST['activitydatestart']) ? date('Y-m-d',strtotime($_POST['activitydatestart'])) : date('Y-m-d'));
      $activitydateend = (!empty($_POST['activitydateend']) ? date('Y-m-d',strtotime($_POST['activitydateend'])) : date('Y-m-d'));
      
      $spark_state_id = (isset($_POST['spark_state_id']) ? $_POST['spark_state_id'] : 0);
      
      if (isset($_POST['spark_id']) and $_POST['spark_id'] > 0)
      {
        $spark_detail = $_POST['spark_id'];
        $spark_details = explode("-",$spark_detail);
        $spark_id = $spark_details[0];
        $sel_spark_id = $spark_id;
        $spark_ids = array($spark_id);
        $this->load->Model('Sparks_model');
        $spark_data = $this->Sparks_model->getById($spark_id);
        $spark_name = $spark_data['name'];
      }  
      else if($spark_state_id > 0 || $spark_state_id == 'HO')
      {
        $this->load->Model('Users_model');
        if($_POST['spark_state_id'] == 'HO'){
					$state = 0;
					$spark_role = array('manager', 'accounts', 'field_user');
					$user_group = array('head_office');  
					$users_data = $this->Users_model->getAllSparksByState($state, $activitydatestart, $activitydateend, '', $spark_role, $user_group);
				}
				else{
					$users_data = $this->Users_model->getSparkByStateDuration($_POST['spark_state_id'], $activitydatestart, $activitydateend,'', array('field_user','manager','state_person'));
				}
        $sel_spark_id = '';
        $spark_id = '';
        $spark_ids = array();
        foreach($users_data as $users){
          if(!in_array($users['user_id'], $spark_ids)){
            array_push($spark_ids, $users['user_id']);
            $spark_names[$users['user_id']]= $users['name'];
          }
        }
      }
    }
    
    if(!empty($spark_ids)){
      
      $this->post_local_claim_process($spark_ids, $activitydatestart, $activitydateend);
      $this->session->set_flashdata('success', "Claim processed successfully");
    }
    else{
      $this->session->set_flashdata('alert', "No spark to process claim");
    }
    redirect('map/claims_exceed');
  }
  
  function post_local_claim_process($spark_ids, $activitydatestart, $activitydateend)
  {
			$this->load->model('travels_model');   
			$travels_mode = $this->travels_model->get_all();
    
			$claim_data = $this->tracking_model->get_claim_unprocess_list($spark_ids, $activitydatestart, $activitydateend);
      
      $sparkIds = array();
      foreach($claim_data as $e_key){ 
        
        $tracking_date = date('Y-m-d', strtotime($e_key->tracking_time));
        $tracking_month_yr = date('Y-m', strtotime($e_key->tracking_time));
        
        $record_id = $e_key->record_id;
        $travel_mode_id = 0;
        $travel_cost = 0;
        $duration = '--';
        $previous_travel_mode_id = '';
        if($e_key->activity_type != 'attendance-out' && $e_key->activity_id != 0){
          $activity_data = get_activity_data($e_key->activity_id, $e_key->activity_type);
          $duration = $activity_data['duration'];
          $travel_mode_id = $activity_data['travel_mode_id'];
          $travel_cost = $activity_data['travel_cost'];
          $previous_travel_mode_id = $travel_mode_id;
        }
        if(strtolower($e_key->activity_type) == 'attendance-out'){
          //$travel_mode_id = $e_key->travel_mode_id;
          if(empty($e_key->activity_id)){
						$travel_mode_id = $previous_travel_mode_id;
					}
					else{
						$activity_data = get_activity_data($e_key->activity_id, $e_key->activity_type);
						if(!empty($activity_data)){
							$duration = $activity_data['duration'];
							$travel_mode_id = $activity_data['travel_mode_id'];
							$travel_cost = $activity_data['travel_cost'];
						}
						else{
							$travel_mode_id = $previous_travel_mode_id;
						}
					}
        }
          
        if($e_key->travel_mode_id != '' && $e_key->travel_mode_id != 0) 
        {
          $travel_mode_id = $e_key->travel_mode_id;
        }
        
        if((empty($e_key->travel_cost) || $e_key->travel_cost == 0.00)){
          $travelCost = '';
          if($travel_cost > 0 && $travel_mode_id > 0){
						$travelCost = $travel_cost;
					}
					else{
						foreach($travels_mode as $travel_mode) {
							if($travel_mode_id == $travel_mode->id){
								$tdistance = ($e_key->manual_distance !='' ?  round($e_key->manual_distance,2) : round($e_key->travel_distance,2));
								$travelCost = $tdistance*$travel_mode->travel_cost;
							}
						}
					}
        }
        else{
          $travelCost = $e_key->travel_cost;
        }
        
        $sparkData = $this->Users_model->getById($e_key->spark_id);
        $tally_app_no = $sparkData['tally_app_no'];
      
        //echo "<br> : ".$e_key->record_id." >> ".$e_key->activity_id." >> ".$e_key->activity_type." >> ".$travel_mode_id." >> ".$travelCost;
        if($travelCost > 0){
					
					$claim_exceed = 0;
					
					$spark_month_expense = $this->tracking_model->check_month_expense($e_key->spark_id, $tracking_month_yr); 
          
          if(!empty($spark_month_expense))
          {
						$month_total_expense = $spark_month_expense['total_expense'];
						
						if($month_total_expense > CLAIM_MONTHLY_LIMIT)
						{
							$mdata = array();
							$mdata['discard_reason'] = 'Montly claim limit exceed.';
							$mdata['claim_discard'] = 1;
							$mdata['updated_on'] = date('Y-m-d H:i:s');
							$mdata['updated_by'] = $this->data['current_user_id'];
							$mdata['process_date'] = date('Y-m-d');
							$res = $this->tracking_model->update_claims($record_id, $mdata);
							
							$claim_exceed = 1;
						}
						else{
							$date_expense_data = $this->tracking_model->check_date_expense($e_key->spark_id, $tracking_date);		
							
							if(!empty($date_expense_data))
							{
								$date_expense_id = $date_expense_data['id'];
								$date_expense = $date_expense_data['date_expense'];
								
								$udata['date_expense'] = $date_expense+$travelCost;
								$this->tracking_model->update_date_expense($udata, $date_expense_id);
							}
							else{
								$sdata = array();
								$sdata['spark_id'] = $e_key->spark_id;
								$sdata['tracking_date'] = $tracking_date;
								$sdata['tracking_month'] = $tracking_month_yr;
								$sdata['date_expense'] = $travelCost;
								$sdata['date_km'] = ($e_key->manual_distance != '' ? $e_key->manual_distance : $e_key->travel_distance);
								
								$this->tracking_model->insert_date_expense($sdata);
							}
						}
					}
					else{
						$sdata = array();
						$sdata['spark_id'] = $e_key->spark_id;
						$sdata['tracking_date'] = $tracking_date;
						$sdata['tracking_month'] = $tracking_month_yr;
						$sdata['date_expense'] = $travelCost;
						$sdata['date_km'] = ($e_key->manual_distance != '' ? $e_key->manual_distance : $e_key->travel_distance);
						
						$this->tracking_model->insert_date_expense($sdata);
					}
					
					if($claim_exceed == 0){
						if(!in_array($e_key->spark_id, $sparkIds))
							array_push($sparkIds, $e_key->spark_id);
						
						$data['claimed_by'] = $this->data['current_user_id'];
						$data['travel_mode_id'] = $travel_mode_id;
						$data['manual_distance'] = ($e_key->manual_distance != '' ? $e_key->manual_distance : $e_key->travel_distance);
						$data['travel_cost'] = $travelCost;
						$data['updated_by'] = $this->data['current_user_id'];
						$data['updated_on'] = date('Y-m-d');
						$data['is_claimed'] = 1;
						$data['tally_app_no'] = $tally_app_no+1;
						$data['process_date'] = date('Y-m-d');
						$this->tracking_model->update_claims($record_id, $data);
					}
        }
      }
      
      foreach($sparkIds as $sparks)
      {
        $this->Users_model->update_spark_tally_app_no($sparks);
      }
	}
  
  function update_claim_data_temp()
  {
		
		$claimData = $this->tracking_model->get_claim_data_no_travel_cost();
		
		foreach($claimData as $e_key)
		{
			if($e_key->activity_id != 0){
				$activity_data = get_activity_data($e_key->activity_id, $e_key->activity_type);
				$duration = $activity_data['duration'];
				$travel_mode_id = $activity_data['travel_mode_id'];
				$travel_cost = $activity_data['travel_cost'];
				$previous_travel_mode_id = $travel_mode_id;
      
				$record_id = $e_key->record_id;
				
				if($travel_mode_id > 0){
					echo"<br>type : ".$e_key->record_id." >> ".$e_key->activity_type." >> ".$e_key->activity_id;
					$data['travel_mode_id'] = $travel_mode_id;
					$data['travel_cost'] = $travel_cost;
					//print_r($data);
					$this->tracking_model->update_claims($record_id, $data);
				}
      }	
		}
		
	}     
  
  
  //START OTHER CLAIM FUNCTIONS
	function other_claim_request()
	{
		if(isset($_POST['Submit']) && $_POST['Submit'] == 'Submit'){
			
			$claim_type = $_POST['claim_type'];
			$from_date = date('Y-m-d', strtotime($_POST['from_date']));
			$to_date = date('Y-m-d', strtotime($_POST['to_date']));
			
			$spark_id = $this->data['current_user_id'];
			$data['spark_id'] = $spark_id;
			$data['claim_type'] = $claim_type;
			$data['from_date'] = $from_date;
			$data['to_date'] = $to_date;
			$data['bill_date'] = date('Y-m-d', strtotime($_POST['bill_date']));
			$data['bill_number'] = $_POST['bill_number'];
			$data['amount'] = $_POST['amount'];
			$data['claim_status'] = 'approved';//'applied';
			$data['comment'] = $_POST['comment'];
			$data['created_on'] = date('Y-m-d H:i:s');
			$data['created_by'] = $this->data['current_user_id'];
			
			/*$rangeDates = $this->Common_functions->createRange($from_date, $to_date);
			
			$claim_exist = 0;
			if($claim_type == 'Travel')
			{
				
			}
			else if($claim_type == 'Local Con')
			{
				
			}
			else if($claim_type == 'Stay' || $claim_type == 'Food')
			{
				$stayData = $this->Claim_model->get_stay_claims_list($spark_id, $from_date, $to_date);
				
				if($claim_type == 'Stay' && count($stayData) > 0)
				{
					$claim_exist = 1;
				}
				
				if($claim_type == 'Food' && count($stayData) > 0)
				foreach($stayData as $stay_data)
				{
					$is_processed = $stay_data->is_processed;
				}
			}*/

			$other_claim_id = $this->Claim_model->add_other_claims($data);
			$fdata['claim_type'] = 'Other Claim';
			$fdata['claim_id'] = $other_claim_id;
			$fdata['created_on'] = date('Y-m-d H:i:s');
			$fdata['created_by'] = $this->data['current_user_id'];
			
			if(!empty($_FILES)){
				$request_folder = "./uploads/claims";
				$request_folder_db = "uploads/claims";
				if (!file_exists($request_folder))
					mkdir($request_folder);
				//$file_name = $_FILES['file_name'];
				$config['upload_path'] = $request_folder;
				$config['allowed_types'] = 'jpg|gif|png|jpeg|JPG|PNG|csv|CSV|xls|xlsx|pdf|PDF';
				$this->load->library('upload', $config);  
				
				$error = '';
				if (isset($_FILES['file_name']) and isset($_FILES['file_name']['name']) and $_FILES['file_name']['name'] != "")
				{
					$file_ext = $this->upload->get_extension($_FILES['file_name']['name']);
					
					$original_name = $_FILES['file_name']['name'];
					$file_name = "oclaim_".$other_claim_id.$file_ext;
					$_FILES['file_name']['name'] = $file_name;
					if(file_exists($request_folder."/".$file_name))
					{
						 unlink($request_folder."/".$file_name);
					}
					if(!$this->upload->do_upload('file_name'))
					{
						$error = $this->upload->display_errors();
					}
					else
					{
						$upload_data = $this->upload->data();
						$file = $upload_data['file_name'];
						$fdata['original_file'] = $original_name;
						$fdata['file_name'] = $request_folder_db."/".$file;
					}
				}
			}
			$this->Claim_model->add_claims_proof($fdata);
			$this->session->set_flashdata('success', 'Claim posted successfully.');
			if($error != '')
				$this->session->set_flashdata('alert', $error);
				redirect('map/other_claims_list', 'refresh');
		}
		$this->data['partial'] = 'map/other_claim';
		$this->data['page_title'] = 'Other Claim Request';
		$this->load->view('templates/index', $this->data);
	}

  public function other_claims_list()
  {
		
    $spark_id = $this->data['current_user_id'];
    $sel_spark_id = $this->data['current_user_id'];
    $state_id = 0;
    $spark_state_id = 0;
    
    if (isset($_POST['spark_id']) and $_POST['spark_id'] > 0)
    {
      $spark_state_id = (isset($_POST['spark_state_id']) ? $_POST['spark_state_id'] : 0);
      $spark_detail = $_POST['spark_id'];
      $spark_details = explode("-",$spark_detail);
      $spark_id = $spark_details[0];
      $sel_spark_id = $spark_id;
    }
    if($this->data['current_role'] == 'state_person' || $this->data['current_role'] == 'manager'){
      $spark_state_id = $this->data['user_state_id'];
    }
    
    $this->load->Model('Sparks_model');
    $spark_data = $this->Sparks_model->getById($spark_id);
    $spark_name = $spark_data['name'];
    $this->data['current_group'] = $spark_data['user_group'];
    
    $claim_data = array();
    $activitydatestart = date('Y-m-d');
    $activitydateend = date('Y-m-d');
    if(isset($_POST) && !empty($_POST['submit']))
    {
      $activitydatestart = (!empty($_POST['activitydatestart']) ? date('Y-m-d',strtotime($_POST['activitydatestart'])) : $activitydatestart);
      $activitydateend = (!empty($_POST['activitydateend']) ? date('Y-m-d',strtotime($_POST['activitydateend'])) : $activitydateend);
    }
    
    $claim_data = $this->Claim_model->get_other_claims_list($spark_id, $activitydatestart, $activitydateend);
    
    $custom_scripts = ['map.js']; 
    $this->data['activitydatestart'] = date('d-m-Y',strtotime($activitydatestart)); 
    $this->data['activitydateend'] = date('d-m-Y',strtotime($activitydateend)); 
    $this->data['custom_scripts'] = $custom_scripts; 
    $this->data['claim_data'] = $claim_data;
    $this->data['sel_spark_id'] = $sel_spark_id;
    $this->data['spark_id'] = $spark_id;
    $this->data['spark_name'] = $spark_name;
    $this->data['spark_state_id'] = $spark_state_id;
    
    $download = (isset($_POST['download']) and $_POST['download'] == 1 ? $_POST['download'] : 0);
    $pdf = (isset($_POST['pdf']) and $_POST['pdf'] == 1 ? $_POST['pdf'] : 0);
    
		if ($download == 1)
    {
      $myFile = "./uploads/reports/other_claims_list.xls";
      $this->load->library('parser');
			$this->data['download'] = $download;
      //pass retrieved data into template and return as a string
      $stringData = $this->parser->parse('map/other_claims_list', $this->data, true);
      //open excel and write string into excel
      $fh = fopen($myFile, 'w') or die("can't open file");
      fwrite($fh, $stringData);
      fclose($fh);
      //download excel file
      $this->common_functions->downloadExcel("other_claims_list");
      unlink($myFile);
    }
    else if($pdf == 1)
    {
        //pass retrieved data into template and return as a string
        $this->data['pdf'] = 1;
        $summary = $this->load->view('map/other_claims_list',$this->data,true);
        $this->common_functions->create_pdf($summary,'other_claims_list');
    }
    else{
      $this->data['partial'] = 'map/other_claims_list';			
      $this->load->view('templates/index', $this->data);
    }
  }
  
	public function other_claims_exceed()
  {
    $spark_id = $this->data['current_user_id'];
    $sel_spark_id = $this->data['current_user_id'];
    $spark_ids = array($spark_id);
    $state_id = 0;
    $spark_state_id = 0;
    $spark_name = '';
    if($this->data['current_role'] == 'state_person' || $this->data['current_role'] == 'manager'){
      $spark_state_id = $this->data['user_state_id'];
    }
    
    $spark_name = 0;
    $exceed_value = 0;
    $claim_data = array();
    $spark_names = array();
    $activitydatestart = date('Y-m-d');
    $activitydateend = date('Y-m-d');
    $spark_names = array();
    $data_submit = '';
    $sel_claim_type = '';
    if(isset($_POST) && !empty($_POST['submit']))
    {
      $activitydatestart = (!empty($_POST['activitydatestart']) ? date('Y-m-d',strtotime($_POST['activitydatestart'])) : date('Y-m-d'));
      $activitydateend = (!empty($_POST['activitydateend']) ? date('Y-m-d',strtotime($_POST['activitydateend'])) : date('Y-m-d'));
      
      $spark_state_id = (isset($_POST['spark_state_id']) ? $_POST['spark_state_id'] : 0);
      $sel_claim_type = (isset($_POST['claim_type']) ? $_POST['claim_type'] : '');
      $exceed_value = $_POST['exceed_value'];
      
      if (isset($_POST['spark_id']) and $_POST['spark_id'] > 0)
      {
        $spark_detail = $_POST['spark_id'];
        $spark_details = explode("-",$spark_detail);
        $spark_id = $spark_details[0];
        $sel_spark_id = $spark_id;
        $spark_ids = array($spark_id);
        $this->load->Model('Sparks_model');
        $spark_data = $this->Sparks_model->getById($spark_id);
        $spark_name = $spark_data['name'];
        $this->data['current_group'] = $spark_data['user_group'];
      }  
      else if($spark_state_id > 0 || $spark_state_id == 'HO')
      {
        $this->load->Model('Users_model');
        
        if($_POST['spark_state_id'] == 'HO'){
					$state = 0;
					$spark_role = array('manager', 'accounts', 'field_user');
					$user_group = array('head_office');  
					$users_data = $this->Users_model->getAllSparksByState($state, $activitydatestart, $activitydateend, '', $spark_role, $user_group);
				}
				else{
					$users_data = $this->Users_model->getSparkByStateDuration($_POST['spark_state_id'], $activitydatestart, $activitydateend,'', array('field_user','manager','state_person'));
					$this->data['current_group'] = 'field_user';
				}
				$spark_id = '';
        $sel_spark_id = '';
        $spark_ids = array();
        foreach($users_data as $users){
          if(!in_array($users['user_id'], $spark_ids)){
            array_push($spark_ids, $users['user_id']);
            $spark_names[$users['user_id']]= $users['name'];
          }
        }
      }
      $data_submit = 'submitted';
    }
    
    $claim_data = $this->tracking_model->get_other_exceed_claims_list($spark_ids, $activitydatestart, $activitydateend, $sel_claim_type);
    
    $process_dates = $this->Claim_model->get_other_process_dates();

    $custom_scripts = ['map.js']; 
    $this->data['sel_claim_type'] = $sel_claim_type;
    $this->data['process_dates'] = $process_dates; 
    $this->data['data_submit'] = $data_submit; 
    $this->data['exceed_value'] = $exceed_value; 
    $this->data['activitydatestart'] = date('d-m-Y',strtotime($activitydatestart)); 
    $this->data['activitydateend'] = date('d-m-Y',strtotime($activitydateend)); 
    $this->data['custom_scripts'] = $custom_scripts; 
    $this->data['claim_data'] = $claim_data;
    $this->data['spark_id'] = $spark_id;
    $this->data['sel_spark_id'] = $sel_spark_id;
    $this->data['spark_name'] = $spark_name;
    $this->data['spark_names'] = $spark_names;
    $this->data['spark_state_id'] = $spark_state_id;
    
    $download = (isset($_POST['download']) and $_POST['download'] == 1 ? $_POST['download'] : 0);
    $pdf = (isset($_POST['pdf']) and $_POST['pdf'] == 1 ? $_POST['pdf'] : 0);
    
		if ($download == 1)
    {
      $myFile = "./uploads/reports/other_claims_exceed.xls";
      $this->load->library('parser');
			$this->data['download'] = $download;
      //pass retrieved data into template and return as a string
      $stringData = $this->parser->parse('map/other_claims_exceed', $this->data, true);
      //open excel and write string into excel
      $fh = fopen($myFile, 'w') or die("can't open file");
      fwrite($fh, $stringData);
      fclose($fh);
      //download excel file
      $this->common_functions->downloadExcel("other_claims_exceed");
      unlink($myFile);
    }
    else if($pdf == 1)
    {
        //pass retrieved data into template and return as a string
        $this->data['pdf'] = 1;
        $summary = $this->load->view('map/other_claims_exceed',$this->data,true);
        $this->common_functions->create_pdf($summary,'other_claims_exceed');
    }
    else{
      $this->data['partial'] = 'map/other_claims_exceed';			
      $this->load->view('templates/index', $this->data);
    }
  }

  public function process_other_claims()
  {
    if(isset($_POST) && $_POST['processClaim'] == 'Process Claim'){
      
      $spark_id = $_POST['spark_id'];
      $record_ids = $_POST['record_id'];
      
      $count = count($record_ids);
      for($i=0; $i<$count; $i++)
      {
        $rid = $record_ids[$i];
        //echo"<br>".$_POST['claim_discard'][$rid];
        $sdata = array();
        if($_POST['claim_status'][$rid] != 'rejected' && $_POST['claim_status'][$rid] != 'processed' && $_POST['claim_status'][$rid] != 'referback'){
					
					$this->post_process_other_claim($spark_id, $rid);
        }
      }
      $this->Users_model->update_spark_tally_app_no($spark_id, 'other');
      $msg = 'Claim processed successfully';
    }
    else if(isset($_POST) && $_POST['processClaim'] == 'Update Claim'){
			
      $record_ids = $_POST['record_id'];
      
      $count = count($record_ids);
      for($i=0; $i<$count; $i++)
      {
        $rid = $record_ids[$i];
        if($_POST['claim_status'][$rid] != 'rejected' && $_POST['claim_status'][$rid] != 'processed' && $_POST['claim_status'][$rid] != 'referback'){

          $data['comment'] = $_POST['comment'][$i];
          $data['updated_by'] = $this->data['current_user_id'];
          $data['updated_on'] = date('Y-m-d');
          $this->Claim_model->update_data('other_claims', $data, array('id'=>$rid));
        }
      }
      $msg = 'Claim updated successfully';
    }
    $this->session->set_flashdata('success', $msg);
    header('location:'.base_url()."map/other_claims_list");
  }
  
	public function auto_other_claims_process()
  {
    $spark_id = $this->data['current_user_id'];
    $state_id = 0;
    $spark_state_id = 0;
    if($this->data['current_role'] == 'state_person' || $this->data['current_role'] == 'manager'){
      $spark_state_id = $this->data['user_state_id'];
    }
    
    $spark_ids = array();
    $claim_data = array();
    $activitydatestart = date('Y-m-d');
    $activitydateend = date('Y-m-d');
    
    if(isset($_POST) && !empty($_POST['claimProcess']))
    {
      $activitydatestart = (!empty($_POST['activitydatestart']) ? date('Y-m-d',strtotime($_POST['activitydatestart'])) : date('Y-m-d'));
      $activitydateend = (!empty($_POST['activitydateend']) ? date('Y-m-d',strtotime($_POST['activitydateend'])) : date('Y-m-d'));
      
      $spark_state_id = (isset($_POST['spark_state_id']) ? $_POST['spark_state_id'] : 0);
      
      if (isset($_POST['spark_id']) and $_POST['spark_id'] > 0)
      {
        $spark_detail = $_POST['spark_id'];
        $spark_details = explode("-",$spark_detail);
        $spark_id = $spark_details[0];
        $sel_spark_id = $spark_id;
        $spark_ids = array($spark_id);
        $this->load->Model('Sparks_model');
        $spark_data = $this->Sparks_model->getById($spark_id);
        $spark_name = $spark_data['name'];
      }  
      else if($spark_state_id > 0 || $spark_state_id == 'HO')
      {
        $this->load->Model('Users_model');
        if($_POST['spark_state_id'] == 'HO'){
					$state = 0;
					$spark_role = array('manager', 'accounts', 'field_user');
					$user_group = array('head_office');  
					$users_data = $this->Users_model->getAllSparksByState($state, $activitydatestart, $activitydateend, '', $spark_role, $user_group);
				}
				else{
					$users_data = $this->Users_model->getSparkByStateDuration($_POST['spark_state_id'], $activitydatestart, $activitydateend,'', array('field_user','manager','state_person'));
				}
        
        $sel_spark_id = '';
        $spark_id = '';
        $spark_ids = array();
        foreach($users_data as $users){
          if(!in_array($users['user_id'], $spark_ids)){
            array_push($spark_ids, $users['user_id']);
            $spark_names[$users['user_id']]= $users['name'];
          }
        }
      }
    }
    
    if(!empty($spark_ids)){
      
      $claim_data = $this->tracking_model->get_other_unprocessed_list($spark_ids, $activitydatestart, $activitydateend);
      
      $sparkIds = array();
      foreach($claim_data as $e_key){ 
        
        $rid = $e_key->id;
        $spark_id = $e_key->spark_id;
				
        if($e_key->amount > 0){
					
					if(!in_array($spark_id, $sparkIds))
						array_push($sparkIds, $spark_id);
						
					$this->post_process_other_claim($spark_id, $rid);
        }
      }
      
      foreach($sparkIds as $sparks)
      {
        $this->Users_model->update_spark_tally_app_no($sparks, 'other');
      }
      $this->session->set_flashdata('success', "Claim processed successfully");
    }
    else{
      $this->session->set_flashdata('alert', "No spark to process claim");
    }
    redirect('map/other_claims_exceed');
  }
  
  function post_process_other_claim($spark_id, $rid)
  {
			$sparkData = $this->Users_model->getById($spark_id);
      $tally_app_no = $sparkData['other_tally_app_no'];
      
			$claim_exceed = 0;
			$claimData = $this->Claim_model->get_other_claims($rid);
			
			$status_remark = $claimData['status_remark'];
			$claim_date = $claimData['from_date'];
			$claim_month_yr = date('Y-m', strtotime($claim_date));
			$claim_date_expense = $claimData['amount'];;
			
			$spark_month_expense = $this->Claim_model->check_month_claim('Other Claim', $spark_id, $claim_month_yr); 
			
			if(!empty($spark_month_expense))
			{
				$month_total_expense = $spark_month_expense['total_expense'];
				
				if($month_total_expense > OTHER_CLAIM_MONTHLY_LIMIT)
				{
					$sdata['status_remark'] = ($status_remark != '' ? $status_remark.' <br/ >Rejected: montly claim limit exceed.' : 'Rejected: montly claim limit exceed.');
					$sdata['claim_status'] = 'rejected';
					$sdata['updated_on'] = date('Y-m-d H:i:s');
					$sdata['updated_by'] = $this->data['current_user_id'];
					$sdata['process_date'] = date('Y-m-d');
					$res = $this->Claim_model->update_data('other_claims', $sdata, array('id'=>$rid));
					$claim_exceed = 1;
				}
				else{
					$date_expense_data = $this->Claim_model->check_date_claim('Other Claim', $spark_id, $claim_date);		
					if(!empty($date_expense_data))
					{
						$udata['date_expense'] = $date_expense_data['date_expense'] + $claim_date_expense;
						$this->Claim_model->update_data('spark_month_claims', $udata, array('id'=>$date_expense_data['id']));
					}
					else{
						$sdata['claim_type'] = 'Other Claim';
						$sdata['spark_id'] = $spark_id;
						$sdata['activity_date'] = $claim_date;
						$sdata['activity_month'] = $claim_month_yr;
						$sdata['date_expense'] = $claim_date_expense;
						$this->Claim_model->addStayActivity($sdata, 'spark_month_claims');
					}
				}
			}
			else{
				$sdata['claim_type'] = 'Other Claim';
				$sdata['spark_id'] = $spark_id;
				$sdata['activity_date'] = $claim_date;
				$sdata['activity_month'] = $claim_month_yr;
				$sdata['date_expense'] = $claim_date_expense;
				$this->Claim_model->addStayActivity($sdata, 'spark_month_claims');
			}
			
			if($claim_exceed == 0){
				$data['claimed_by'] = $this->data['current_user_id'];
				$data['updated_by'] = $this->data['current_user_id'];
				$data['updated_on'] = date('Y-m-d');
				$data['claim_status'] = 'processed';
				$data['tally_app_no'] = $tally_app_no+1;
				$data['process_date'] = date('Y-m-d');
				$this->Claim_model->update_data('other_claims', $data, array('id'=>$rid));
			}
			
		
	}
  
  public function other_claims_import()
  {
    $spark_id = $this->data['current_user_id'];
    $state_id = 0;
    $spark_state_id = 0;
    if($this->data['current_role'] == 'state_person' || $this->data['current_role'] == 'manager'){
      $spark_state_id = $this->data['user_state_id'];
    }

    $spark_ids = array();
    $claim_data = array();
    $activitydatestart = date('Y-m-d');
    $activitydateend = date('Y-m-d');
    
    if(isset($_POST) && !empty($_POST['tallyButton']))
    {
      $process_date = (!empty($_POST['process_date']) ? date('Y-m-d',strtotime($_POST['process_date'])) : date('Y-m-d'));
      $spark_state_id = (isset($_POST['spark_state_id']) ? $_POST['spark_state_id'] : 0);
      
      $this->load->Model('Users_model');
      if($_POST['spark_state_id'] == 'HO'){
				$state = 0;
				$spark_role = array('manager', 'accounts', 'field_user');
				$user_group = array('head_office');  
				$users_data = $this->Users_model->getSparksByCurrentState($state, $spark_role, $user_group);
			}
			else{
				$users_data = $this->Users_model->getSparksByCurrentState($_POST['spark_state_id'], array('field_user','manager','state_person'));
      }
      
      $sel_spark_id = '';
      $spark_ids = array();
      foreach($users_data as $users){
        if(!in_array($users->id, $spark_ids)){
          array_push($spark_ids, $users->id);
          $spark_names[$users->id]= $users->name;
        }
      }
      
      $spark_data = $this->Users_model->getById($spark_id);
      $spark_name = $spark_data['name'];
      
      $claim_data = $this->tracking_model->get_track_tally_other_list($spark_ids, $process_date);
      //print_r($claim_data);
      
      if(!empty($claim_data)){
        $this->data['claim_data'] = $claim_data;
        
        $this->data['report_type'] = $_POST['tallyButton'];
        $this->data['claim_data'] = $claim_data;
        $this->data['spark_name'] = $spark_name;
        $this->data['spark_state_id'] = $spark_state_id;
        
        $myFile = "./uploads/reports/other_claims_import.xls";
        $this->load->library('parser');
        //pass retrieved data into template and return as a string
        $stringData = $this->parser->parse('map/other_claims_import', $this->data, true);
        //open excel and write string into excel
        $fh = fopen($myFile, 'w') or die("can't open file");
        fwrite($fh, $stringData);
        fclose($fh);
        //download excel file
        $this->common_functions->downloadExcel("other_claims_import");
        unlink($myFile);
      }
      else{
        $this->session->set_flashdata('alert', "No claim found for selected process date.");
        redirect('map/other_claims_exceed');
      }
    }
  }
  //END OTHER CLAIM FUNCTIONS

	
	//START LD Travel CLAIM FUNCTIONS
  function ld_travel_claims()
  {
		$spark_id = $this->data['current_user_id'];
    $sel_spark_id = $this->data['current_user_id'];
    $state_id = 0;
    $spark_state_id = 0;
    if($this->data['current_role'] == 'state_person' || $this->data['current_role'] == 'manager'){
      $spark_state_id = $this->data['user_state_id'];
    }
    if (isset($_POST['spark_id']) and $_POST['spark_id'] > 0)
    {
      $spark_state_id = (isset($_POST['spark_state_id']) ? $_POST['spark_state_id'] : 0);
      $spark_detail = $_POST['spark_id'];
      $spark_details = explode("-",$spark_detail);
      $spark_id = $spark_details[0];
      $sel_spark_id = $spark_id;
    }
    $this->load->Model('Sparks_model');
    $spark_data = $this->Sparks_model->getById($spark_id);
    $spark_name = $spark_data['name'];
    $this->data['current_group'] = $spark_data['user_group'];
    
    $claim_data = array();
    $activitydatestart = date('Y-m-d');
    $activitydateend = date('Y-m-d');
    if(isset($_POST) && !empty($_POST['submit']))
    {
      $activitydatestart = (!empty($_POST['activitydatestart']) ? date('Y-m-d',strtotime($_POST['activitydatestart'])) : date('Y-m-d'));
      $activitydateend = (!empty($_POST['activitydateend']) ? date('Y-m-d',strtotime($_POST['activitydateend'])) : date('Y-m-d'));
    }
    
    $claim_data = $this->Claim_model->get_longdistance_activities($spark_id, $activitydatestart, $activitydateend, '');
    
    $this->load->model('travels_model');   
    $travels_mode = $this->travels_model->get_all();

    $custom_scripts = ['map.js']; 
    $this->data['activitydatestart'] = date('d-m-Y',strtotime($activitydatestart)); 
    $this->data['activitydateend'] = date('d-m-Y',strtotime($activitydateend)); 
    $this->data['custom_scripts'] = $custom_scripts; 
    $this->data['travels_mode'] = $travels_mode;
    $this->data['claim_data'] = $claim_data;
    $this->data['sel_spark_id'] = $sel_spark_id;
    $this->data['spark_id'] = $spark_id;
    $this->data['spark_name'] = $spark_name;
    $this->data['spark_state_id'] = $spark_state_id;
    
    $download = (isset($_POST['download']) and $_POST['download'] == 1 ? $_POST['download'] : 0);
    $pdf = (isset($_POST['pdf']) and $_POST['pdf'] == 1 ? $_POST['pdf'] : 0);
    
		if ($download == 1)
    {
      $myFile = "./uploads/reports/ld_travel_claims.xls";
      $this->load->library('parser');
			$this->data['download'] = $download;
      //pass retrieved data into template and return as a string
      $stringData = $this->parser->parse('map/ld_travel_claims', $this->data, true);
      //open excel and write string into excel
      $fh = fopen($myFile, 'w') or die("can't open file");
      fwrite($fh, $stringData);
      fclose($fh);
      //download excel file
      $this->common_functions->downloadExcel("ld_travel_claims");
      unlink($myFile);
    }
    else if($pdf == 1)
    {
        //pass retrieved data into template and return as a string
        $this->data['pdf'] = 1;
        $summary = $this->load->view('map/ld_travel_claims',$this->data,true);
        $this->common_functions->create_pdf($summary,'ld_travel_claims');
    }
    else{
      $this->data['partial'] = 'map/ld_travel_claims';			
      $this->load->view('templates/index', $this->data);
    }
		 
	}
	
	public function ld_claims_exceed()
  {
    $spark_id = $this->data['current_user_id'];
    $sel_spark_id = $this->data['current_user_id'];
    $spark_ids = array($spark_id);
    $state_id = 0;
    $spark_state_id = 0;
    $spark_name = '';
    if($this->data['current_role'] == 'state_person' || $this->data['current_role'] == 'manager'){
      $spark_state_id = $this->data['user_state_id'];
    }
    
    $spark_name = 0;
    $exceed_value = 0;
    $claim_data = array();
    $spark_names = array();
    $activitydatestart = date('Y-m-d');
    $activitydateend = date('Y-m-d');
    $spark_names = array();
    $data_submit = '';
    $travel_mode_id = '';
    $sel_claim_type = '';
    if(isset($_POST) && !empty($_POST['submit']))
    {
      $activitydatestart = (!empty($_POST['activitydatestart']) ? date('Y-m-d',strtotime($_POST['activitydatestart'])) : date('Y-m-d'));
      $activitydateend = (!empty($_POST['activitydateend']) ? date('Y-m-d',strtotime($_POST['activitydateend'])) : date('Y-m-d'));
      
      $spark_state_id = (isset($_POST['spark_state_id']) ? $_POST['spark_state_id'] : 0);
      $travel_mode_id = (isset($_POST['travel_mode']) ? $_POST['travel_mode'] : '');
      $sel_claim_type = (isset($_POST['claim_type']) ? $_POST['claim_type'] : '');
      $exceed_value = $_POST['exceed_value'];
      
      if (isset($_POST['spark_id']) and $_POST['spark_id'] > 0)
      {
        $spark_detail = $_POST['spark_id'];
        $spark_details = explode("-",$spark_detail);
        $spark_id = $spark_details[0];
        $sel_spark_id = $spark_id;
        $spark_ids = array($spark_id);
        $this->load->Model('Sparks_model');
        $spark_data = $this->Sparks_model->getById($spark_id);
        $spark_name = $spark_data['name'];
        $this->data['current_group'] = $spark_data['user_group'];
      }  
      else if($spark_state_id > 0 || $spark_state_id == 'HO')
      {
        $this->load->Model('Users_model');
        if($_POST['spark_state_id'] == 'HO'){
					$state = 0;
					$spark_role = array('manager', 'accounts', 'field_user');
					$user_group = array('head_office');  
					$users_data = $this->Users_model->getAllSparksByState($state, $activitydatestart, $activitydateend, '', $spark_role, $user_group);
				}
				else{
					$users_data = $this->Users_model->getSparkByStateDuration($_POST['spark_state_id'], $activitydatestart, $activitydateend,'', array('field_user','manager','state_person'));
					$this->data['current_group'] = 'field_user';
				}
					
        $spark_id = '';
        $sel_spark_id = '';
        $spark_ids = array();
        foreach($users_data as $users){
          if(!in_array($users['user_id'], $spark_ids)){
            array_push($spark_ids, $users['user_id']);
            $spark_names[$users['user_id']]= $users['name'];
          }
        }
      }
      $data_submit = 'submitted';
    }
    
    $claim_data = $this->tracking_model->get_ld_exceed_claims_list($spark_ids, $activitydatestart, $activitydateend, $travel_mode_id, $sel_claim_type);
    //print_r($claim_data);
    
    $this->load->model('travels_model');   

    $travels_mode = $this->travels_model->get_all();
    
    $process_dates = $this->Claim_model->get_ld_process_dates();

    $custom_scripts = ['map.js']; 
    $this->data['travel_mode_id'] = $travel_mode_id;
    $this->data['sel_claim_type'] = $sel_claim_type;
    $this->data['process_dates'] = $process_dates; 
    $this->data['data_submit'] = $data_submit; 
    $this->data['exceed_value'] = $exceed_value; 
    $this->data['activitydatestart'] = date('d-m-Y',strtotime($activitydatestart)); 
    $this->data['activitydateend'] = date('d-m-Y',strtotime($activitydateend)); 
    $this->data['custom_scripts'] = $custom_scripts; 
    $this->data['travels_mode'] = $travels_mode;
    $this->data['claim_data'] = $claim_data;
    $this->data['spark_id'] = $spark_id;
    $this->data['sel_spark_id'] = $sel_spark_id;
    $this->data['spark_name'] = $spark_name;
    $this->data['spark_names'] = $spark_names;
    $this->data['spark_state_id'] = $spark_state_id;
    
    $download = (isset($_POST['download']) and $_POST['download'] == 1 ? $_POST['download'] : 0);
    $pdf = (isset($_POST['pdf']) and $_POST['pdf'] == 1 ? $_POST['pdf'] : 0);
    
		if ($download == 1)
    {
      $myFile = "./uploads/reports/ld_claims_exceed.xls";
      $this->load->library('parser');
			$this->data['download'] = $download;
      //pass retrieved data into template and return as a string
      $stringData = $this->parser->parse('map/ld_claims_exceed', $this->data, true);
      //open excel and write string into excel
      $fh = fopen($myFile, 'w') or die("can't open file");
      fwrite($fh, $stringData);
      fclose($fh);
      //download excel file
      $this->common_functions->downloadExcel("ld_claims_exceed");
      unlink($myFile);
    }
    else if($pdf == 1)
    {
        //pass retrieved data into template and return as a string
        $this->data['pdf'] = 1;
        $summary = $this->load->view('map/ld_claims_exceed',$this->data,true);
        $this->common_functions->create_pdf($summary,'ld_claims_exceed');
    }
    else{
      $this->data['partial'] = 'map/ld_claims_exceed';			
      $this->load->view('templates/index', $this->data);
    }
  }
  
	public function process_ld_travel()
  {
		
    if(isset($_POST) && $_POST['processClaim'] == 'Process Claim'){
      
      $spark_id = $_POST['spark_id'];
      $record_ids = $_POST['record_id'];
      
      $sparkData = $this->Users_model->getById($spark_id);
      $tally_app_no = $sparkData['ld_tally_app_no'];
      
      $count = count($record_ids);
      for($i=0; $i<$count; $i++)
      {
        $rid = $record_ids[$i];
        //echo"<br>".$_POST['claim_discard'][$rid];
        $sdata = array();
        if($_POST['claim_status'][$rid] != 'rejected' && $_POST['claim_status'][$rid] != 'processed' && $_POST['claim_status'][$rid] != 'referback'){
					
					$claim_exceed = 0;
					$tracking_date = date('Y-m-d', strtotime($_POST['tracking_time'][$i]));
					$tracking_month_yr = date('Y-m', strtotime($_POST['tracking_time'][$i]));
					
					$claimData = $this->Claim_model->getTravelById($rid);
					
					$status_remark = $claimData['status_remark'];
					
					$spark_month_expense = $this->tracking_model->check_month_expense($spark_id, $tracking_month_yr); 
          
          if(!empty($spark_month_expense))
          {
						$month_total_expense = $spark_month_expense['total_expense'];
						
						if($month_total_expense > LD_MONTHLY_LIMIT)
						{
							$sdata['status_remark'] = ($status_remark != '' ? $status_remark.' <br/ > Montly claim limit exceed.' : 'Rejected: Montly claim limit exceed.');
							$sdata['claim_status'] = 'rejected';
							$sdata['updated_on'] = date('Y-m-d H:i:s');
							$sdata['updated_by'] = $this->data['current_user_id'];
							$sdata['process_date'] = date('Y-m-d');
							$res = $this->tracking_model->update_claims($rid, $sdata);
							
							$claim_exceed = 1;
						}
						else{
							$date_expense_data = $this->tracking_model->check_date_expense($spark_id, $tracking_date);		
							
							if(!empty($date_expense_data))
							{
								$date_expense_id = $date_expense_data['id'];
								$date_expense = $date_expense_data['date_expense'];
								
								$udata['date_expense'] = $date_expense+$_POST['travel_cost'][$i];
								
								$this->tracking_model->update_date_expense($udata, $date_expense_id);
							}
							else{
								$sdata['spark_id'] = $spark_id;
								$sdata['tracking_date'] = $tracking_date;
								$sdata['tracking_month'] = $tracking_month_yr;
								$sdata['date_expense'] = $_POST['travel_cost'][$i];
								$sdata['date_km'] = $_POST['manualdistance'][$i];
								
								$this->tracking_model->insert_date_expense($sdata);
							}
						}
					}
					else{
						$sdata['spark_id'] = $spark_id;
						$sdata['tracking_date'] = $tracking_date;
						$sdata['tracking_month'] = $tracking_month_yr;
						$sdata['date_expense'] = $_POST['travel_cost'][$i];
						$sdata['date_km'] = $_POST['manualdistance'][$i];
						
						$this->tracking_model->insert_date_expense($sdata);
					}
					
					if($claim_exceed == 0){
						$tmode_arr = explode('_', $_POST['travel_mode'][$i]);
						$travel_mode_id = $tmode_arr[0];
						$data['remarks'] = $_POST['remarks'][$i];
						$data['travel_mode_id'] = $travel_mode_id;
						$data['manual_distance'] = $_POST['manualdistance'][$i];
						$data['travel_cost'] = $_POST['travel_cost'][$i];
						$data['claimed_by'] = $this->data['current_user_id'];
						$data['updated_by'] = $this->data['current_user_id'];
						$data['updated_on'] = date('Y-m-d');
						$data['claim_status'] = 'processed';
						$data['tally_app_no'] = $tally_app_no+1;
						$data['process_date'] = date('Y-m-d');
						//print_r($data);
						
						$this->Claim_model->update_ld_claim($data, $rid);
					}
        }
      }
      $this->Users_model->update_spark_tally_app_no($spark_id, 'ld');
      
      $msg = 'Claim processed successfully';
    }
    else if(isset($_POST) && $_POST['processClaim'] == 'Update Claim'){
			
			//print_r($_POST);	
			//die;
      $record_ids = $_POST['record_id'];
      
      $count = count($record_ids);
      for($i=0; $i<$count; $i++)
      {
        $rid = $record_ids[$i];
        if($_POST['claim_processed'][$rid] != 'rejected' && $_POST['claim_processed'][$rid] != 'processed' && $_POST['claim_processed'][$rid] != 'referback'){
          $tmode_arr = explode('_', $_POST['travel_mode'][$i]);
          $travel_mode_id = $tmode_arr[0];
          $data['travel_mode_id'] = $travel_mode_id;
          $data['manual_distance'] = $_POST['manualdistance'][$i];
          $data['travel_cost'] = $_POST['travel_cost'][$i];
          $data['remarks'] = $_POST['remarks'][$i];
          $data['updated_by'] = $this->data['current_user_id'];
          $data['updated_on'] = date('Y-m-d');
          $this->Claim_model->update_ld_claim($data, $rid);
        }
      }
      $msg = 'Claim updated successfully';
    }
    
    $this->session->set_flashdata('success', $msg);
    header('location:'.base_url()."map/ld_travel_claims");
  }
  
	public function auto_ld_claims_process()
  {
    $spark_id = $this->data['current_user_id'];
    $state_id = 0;
    $spark_state_id = 0;
    if($this->data['current_role'] == 'state_person' || $this->data['current_role'] == 'manager'){
      $spark_state_id = $this->data['user_state_id'];
    }
    
    $spark_ids = array();
    $claim_data = array();
    $activitydatestart = date('Y-m-d');
    $activitydateend = date('Y-m-d');
    
    if(isset($_POST) && !empty($_POST['claimProcess']))
    {
      $activitydatestart = (!empty($_POST['activitydatestart']) ? date('Y-m-d',strtotime($_POST['activitydatestart'])) : date('Y-m-d'));
      $activitydateend = (!empty($_POST['activitydateend']) ? date('Y-m-d',strtotime($_POST['activitydateend'])) : date('Y-m-d'));
      
      $spark_state_id = (isset($_POST['spark_state_id']) ? $_POST['spark_state_id'] : 0);
      
      if (isset($_POST['spark_id']) and $_POST['spark_id'] > 0)
      {
        $spark_detail = $_POST['spark_id'];
        $spark_details = explode("-",$spark_detail);
        $spark_id = $spark_details[0];
        $sel_spark_id = $spark_id;
        $spark_ids = array($spark_id);
        $this->load->Model('Sparks_model');
        $spark_data = $this->Sparks_model->getById($spark_id);
        $spark_name = $spark_data['name'];
      }  
      else if($spark_state_id > 0 || $spark_state_id == 'HO')
      {
        $this->load->Model('Users_model');
        if($_POST['spark_state_id'] == 'HO'){
					$state = 0;
					$spark_role = array('manager', 'accounts', 'field_user');
					$user_group = array('head_office');  
					$users_data = $this->Users_model->getAllSparksByState($state, $activitydatestart, $activitydateend, '', $spark_role, $user_group);
				}
				else{
					$users_data = $this->Users_model->getSparkByStateDuration($_POST['spark_state_id'], $activitydatestart, $activitydateend,'', array('field_user','manager','state_person'));
				}

        $sel_spark_id = '';
        $spark_id = '';
        $spark_ids = array();
        foreach($users_data as $users){
          if(!in_array($users['user_id'], $spark_ids)){
            array_push($spark_ids, $users['user_id']);
            $spark_names[$users['user_id']]= $users['name'];
          }
        }
      }
    }
    
    if(!empty($spark_ids)){
			$this->post_ld_claim_process($spark_ids, $activitydatestart, $activitydateend);
      $this->session->set_flashdata('success', "Claim processed successfully");
    }
    else{
      $this->session->set_flashdata('alert', "No spark to process claim");
    }
    redirect('map/ld_claims_exceed');
  }
	
	function post_ld_claim_process($spark_ids, $activitydatestart, $activitydateend)
	{
			$this->load->model('travels_model');   
			$travels_mode = $this->travels_model->get_all();
    
		  $claim_data = $this->tracking_model->get_ld_unprocessed_list($spark_ids, $activitydatestart, $activitydateend);
      
      $sparkIds = array();
      foreach($claim_data as $e_key){ 
        
        $tracking_date = date('Y-m-d', strtotime($e_key->activity_date));
        $tracking_month_yr = date('Y-m', strtotime($e_key->activity_date));
        
        $record_id = $e_key->id;
				$duration = $e_key->duration;
				$travel_mode_id = $e_key->travel_mode_id;
				$travel_cost = $e_key->travel_cost;
				$previous_travel_mode_id = $travel_mode_id;
			
        if((empty($e_key->travel_cost) || $e_key->travel_cost == 0.00)){
          $travelCost = '';
          if($travel_cost > 0 && $travel_mode_id > 0){
						$travelCost = $travel_cost;
					}
					else{
						foreach($travels_mode as $travel_mode) {
							if($travel_mode_id == $travel_mode->id){
								$tdistance = ($e_key->manual_distance !='' ?  round($e_key->manual_distance,2) : round($e_key->travel_distance,2));
								$travelCost = $tdistance*$travel_mode->travel_cost;
							}
						}
					}
        }
        else{
          $travelCost = $e_key->travel_cost;
        }
        
        $sparkData = $this->Users_model->getById($e_key->user_id);
        $tally_app_no = $sparkData['ld_tally_app_no'];
      
        //echo "<br> : ".$e_key->record_id." >> ".$e_key->activity_id." >> ".$e_key->activity_type." >> ".$travel_mode_id." >> ".$travelCost;
        if($travelCost > 0){
					
					$claim_exceed = 0;
					$status_remark = $e_key->status_remark;
					$spark_month_expense = $this->tracking_model->check_month_expense($e_key->user_id, $tracking_month_yr); 
          
          if(!empty($spark_month_expense))
          {
						$month_total_expense = $spark_month_expense['total_expense'];
						
						if($month_total_expense > LD_MONTHLY_LIMIT)
						{
							$mdata = array();
							$mdata['status_remark'] = ($status_remark != '' ? $status_remark.'Montly claim limit exceed.' : 'Montly claim limit exceed.');
							$sdata['claim_status'] = 'rejected';
							$sdata['updated_on'] = date('Y-m-d H:i:s');
							$sdata['updated_by'] = $this->data['current_user_id'];
							$sdata['process_date'] = date('Y-m-d');
							$res = $this->tracking_model->update_claims($rid, $sdata);
							
							$claim_exceed = 1;
						}
						else{
							$date_expense_data = $this->tracking_model->check_date_expense($e_key->user_id, $tracking_date);		
							
							if(!empty($date_expense_data))
							{
								$date_expense_id = $date_expense_data['id'];
								$date_expense = $date_expense_data['date_expense'];
								
								$udata['date_expense'] = $date_expense+$travelCost;
								$this->tracking_model->update_date_expense($udata, $date_expense_id);
							}
							else{
								$sdata = array();
								$sdata['spark_id'] = $e_key->user_id;
								$sdata['tracking_date'] = $tracking_date;
								$sdata['tracking_month'] = $tracking_month_yr;
								$sdata['date_expense'] = $travelCost;
								$sdata['date_km'] = ($e_key->manual_distance != '' ? $e_key->manual_distance : $e_key->travel_distance);
								
								$this->tracking_model->insert_date_expense($sdata);
							}
						}
					}
					else{
						$sdata = array();
						$sdata['spark_id'] = $e_key->user_id;
						$sdata['tracking_date'] = $tracking_date;
						$sdata['tracking_month'] = $tracking_month_yr;
						$sdata['date_expense'] = $travelCost;
						$sdata['date_km'] = ($e_key->manual_distance != '' ? $e_key->manual_distance : $e_key->travel_distance);
						
						$this->tracking_model->insert_date_expense($sdata);
					}
					
					if($claim_exceed == 0){
						if(!in_array($e_key->user_id, $sparkIds))
							array_push($sparkIds, $e_key->user_id);
						
						$data['claimed_by'] = $this->data['current_user_id'];
						$data['travel_mode_id'] = $travel_mode_id;
						$data['manual_distance'] = ($e_key->manual_distance != '' ? $e_key->manual_distance : $e_key->travel_distance);
						$data['travel_cost'] = $travelCost;
						$data['updated_by'] = $this->data['current_user_id'];
						$data['updated_on'] = date('Y-m-d');
						$data['claim_status'] = 'processed';
						$data['tally_app_no'] = $tally_app_no+1;
						$data['process_date'] = date('Y-m-d');
						//print_r($data);
						
						$this->Claim_model->update_ld_claim($data, $record_id);
					}
        }
      }
      
      foreach($sparkIds as $sparks)
      {
        $this->Users_model->update_spark_tally_app_no($sparks, 'ld');
      }
	}
		
  public function ld_claims_import()
  {
    $spark_id = $this->data['current_user_id'];
    $state_id = 0;
    $spark_state_id = 0;
    if($this->data['current_role'] == 'state_person' || $this->data['current_role'] == 'manager'){
      $spark_state_id = $this->data['user_state_id'];
    }

    $spark_ids = array();
    $claim_data = array();
    $activitydatestart = date('Y-m-d');
    $activitydateend = date('Y-m-d');
    
    if(isset($_POST) && !empty($_POST['tallyButton']))
    {
      $process_date = (!empty($_POST['process_date']) ? date('Y-m-d',strtotime($_POST['process_date'])) : date('Y-m-d'));
      $spark_state_id = (isset($_POST['spark_state_id']) ? $_POST['spark_state_id'] : 0);
      
      $this->load->Model('Users_model');
      if($_POST['spark_state_id'] == 'HO'){
				$state = 0;
				$spark_role = array('manager', 'accounts', 'field_user');
				$user_group = array('head_office');  
				$users_data = $this->Users_model->getSparksByCurrentState($state, $spark_role, $user_group);
			}
			else{
				$users_data = $this->Users_model->getSparksByCurrentState($_POST['spark_state_id'], array('field_user','manager','state_person'));
			}
      
      $sel_spark_id = '';
      $spark_ids = array();
      foreach($users_data as $users){
        if(!in_array($users->id, $spark_ids)){
          array_push($spark_ids, $users->id);
          $spark_names[$users->id]= $users->name;
        }
      }
      
      $spark_data = $this->Users_model->getById($spark_id);
      $spark_name = $spark_data['name'];
      
      $claim_data = $this->tracking_model->get_track_tally_ld_list($spark_ids, $process_date);
      //print_r($claim_data);
      
      if(!empty($claim_data)){
        $this->data['claim_data'] = $claim_data;
        
        $this->load->model('travels_model');   
        $travels_mode = $this->travels_model->get_all();
        
        $this->data['report_type'] = $_POST['tallyButton'];
        $this->data['travels_mode'] = $travels_mode;
        $this->data['claim_data'] = $claim_data;
        $this->data['spark_name'] = $spark_name;
        $this->data['spark_state_id'] = $spark_state_id;
        
        $myFile = "./uploads/reports/ld_claims_import.xls";
        $this->load->library('parser');
        //pass retrieved data into template and return as a string
        $stringData = $this->parser->parse('map/ld_claims_import', $this->data, true);
        //open excel and write string into excel
        $fh = fopen($myFile, 'w') or die("can't open file");
        fwrite($fh, $stringData);
        fclose($fh);
        //download excel file
        $this->common_functions->downloadExcel("ld_claims_import");
        unlink($myFile);
      }
      else{
        $this->session->set_flashdata('alert', "No claim found for selected process date.");
        redirect('map/claims_exceed');
      }
    }
  }
  //END LD Travel CLAIM FUNCTIONS
  
  
  //START STAY CLAIM FUNCTIONS
  public function stay_claims()
  {
		$this->load->Model('Users_model');
		
    $sparks_list = array();
    
    $sparks_list = $this->Users_model->get_all_users($this->data['current_user_id'], 1, array('field_user', 'state_person', 'manager'));
    $error = 0;
    
    $stay_list = array();
    if(isset($_POST['Submit']) && $_POST['Submit'] == 'Save Claim'){
				
				$stay_ids = $_POST['stay_ids'];
				$total_cost = 0;
				$food_cost = 0;
				$paid_sparks = array();
				$activity_date = array();
				$accommodation_type = array();
				$stay_reason = array();
				foreach($stay_ids as $stay_id)
				{
					$manual_rent = $_POST['manual_rent'][$stay_id];
					$total_cost = $total_cost + $manual_rent;
					$paid_for_sparks = (isset($_POST['paid_for_sparks']) ? $_POST['paid_for_sparks'] : array());
				
					$get_stay_sparks = $this->Claim_model->get_data('stay_activity_sparks', array('stay_activity_id'=>$stay_id));
					$staySparks = array();
					foreach($get_stay_sparks as $get_stay_spark)
					{
						if(!in_array($get_stay_spark->spark_id, $staySparks))
							array_push($staySparks, $get_stay_spark->spark_id);
					}
					
					$spark_to_add = array_diff($paid_for_sparks, $staySparks);
					$spark_to_inactive = array_diff($staySparks, $paid_for_sparks);
					
					if(!empty($spark_to_add)){
						foreach($spark_to_add as $spark_add)
						{
							$sdata = array();
							$sdata['stay_activity_id'] = $stay_id;
							$sdata['spark_id'] = $spark_add;
							$sdata['status'] = 1;
							$this->Claim_model->addStayActivity($sdata, 'stay_activity_sparks');
						}
					}
					
					if(!empty($spark_to_inactive)){
						foreach($spark_to_inactive as $sparkid)
						{
							$sdata = array();
							$sdata['status'] = 0;
							$this->Claim_model->update_data('stay_activity_sparks', $sdata, array('stay_activity_id'=>$stay_id, 'spark_id'=> $sparkid));
						}
					}
					
					if(!in_array($_POST['activity_date'][$stay_id], $activity_date))
						 array_push($activity_date, $_POST['activity_date'][$stay_id]);
					if(!in_array($_POST['stay_reason'][$stay_id], $stay_reason))
						 array_push($stay_reason, $_POST['stay_reason'][$stay_id]);
					if(!in_array($_POST['accommodation_type'][$stay_id], $accommodation_type))
						 array_push($accommodation_type, $_POST['accommodation_type'][$stay_id]);
					
					foreach($paid_for_sparks as $paidspark){
						if(!in_array($paidspark, $paid_sparks))
							array_push($paid_sparks, $paidspark);
					}
					$mdata['is_processed'] = 1;
					$this->Claim_model->update_data('stay_activities', $mdata, array('id'=>$stay_id));
				}
				asort($activity_date);
				
				$count_days = count($activity_date);
				
				$food_cost = $count_days * PERDAY_FOOD_CLAIM;
				
				$data['spark_id'] = $this->data['current_user_id'];
				$data['from_date'] = date('Y-m-d', strtotime($activity_date[0]));
				$data['to_date'] = date('Y-m-d', strtotime($activity_date[$count_days-1]));
				$data['total_days'] = $count_days;
				$data['paid_spark_id'] = json_encode($paid_sparks);
				$data['stay_reason'] = json_encode($stay_reason);
				$data['accommodation_type'] = json_encode($accommodation_type);
				$data['bill_number'] = $_POST['bill_number'];
				$data['bill_date'] = date('Y-m-d', strtotime($_POST['bill_date']));
				$data['claim_amount'] = $_POST['claim_amount'];
				$data['actual_cost'] = $total_cost;
				$data['manual_cost'] = $_POST['amount'];
				$data['phone_number'] = $_POST['phone_number'];
				$data['claimed_by'] = $this->data['current_user_id'];
				$data['created_on'] = date('Y-m-d');
				$data['created_by'] = $this->data['current_user_id'];
				$data['paid_to_manager'] = $_POST['manager_name'];
				$data['claim_status'] = 'approved';
				
				//print_r($data);exit;
				$stay_claim_id = $this->Claim_model->addStayActivity($data, 'stay_claims');
				
				//START INSERT DATA INTO FOOD CLAIM TABLE
				$fdata['stay_claim_id'] = $stay_claim_id;
				$fdata['spark_id'] = $this->data['current_user_id'];
				$fdata['from_date'] = date('Y-m-d', strtotime($activity_date[0]));
				$fdata['to_date'] = date('Y-m-d', strtotime($activity_date[$count_days-1]));
				$fdata['total_days'] = $count_days;
				$fdata['paid_spark_id'] = json_encode($paid_sparks);
				$fdata['actual_cost'] = $food_cost;
				$fdata['manual_cost'] = $food_cost;
				$fdata['claimed_by'] = $this->data['current_user_id'];
				$fdata['created_on'] = date('Y-m-d');
				$fdata['created_by'] = $this->data['current_user_id'];
				$fdata['claim_status'] = 'approved';
				
				$stay_claim_id = $this->Claim_model->addStayActivity($fdata, 'food_claims');
				//END INSERT DATA INTO FOOD CLAIM TABLE
					
				//UPLOAD STAY CLAIM PROOF DOCUMENTS	
				$uploadData = $this->upload_bill('Stay Claim', $stay_claim_id, $_FILES);
				
				$this->session->set_flashdata('success', 'Claim posted successfully.');
				redirect('map/stay_claims_list', 'refresh');
		}
    if(isset($_POST['Submit']) && $_POST['Submit'] == 'Get Stay Activity')
    {
				$spark_id = $this->data['current_user_id'];
				$start_date = date('Y-m-d', strtotime($_POST['sdate']));
				$end_date = date('Y-m-d', strtotime($_POST['edate']));
			 
				$stay_list = $this->Claim_model->get_stay_activity_list($spark_id, $start_date, $end_date);
				$stay_with = array();

				if(count($stay_list) == 0){
					$error = 2;
				}
				else{
					$i=0;
					foreach($stay_list as $staylist)
					{
						if($i == 0){
							$stay_with = json_decode($staylist->stay_with_sparks);
						}
						else{
							$stay_with_new = json_decode($staylist->stay_with_sparks);
							if(count($stay_with_new) != count($stay_with)){
								$error = 1;
								break;
							}
							else
							{
								foreach($stay_with_new as $swith)
								{
									if(!in_array($swith, $stay_with)){
										$error = 1;
										break;
									}
								} 
							}
						}
						$i++;
					}
				}
				$this->data['stay_with'] = $stay_with;
				$this->data['from_date'] = $start_date;
				$this->data['to_date'] = $end_date;
		}
    $this->data['error'] = $error;
				
    $stay_claim_activity = array();
    $this->data['stay_claim_activity'] = $stay_claim_activity;
    $this->data['sparks_list'] = $sparks_list;
    $this->data['stay_list'] = $stay_list;
    
		$this->data['partial'] = 'map/stay_claim';
    $this->data['page_title'] = 'Stay Claim';
    $this->load->view('templates/index', $this->data);
  }
  
  public function stay_claims_list()
  {
    $spark_id = $this->data['current_user_id'];
    $sel_spark_id = $this->data['current_user_id'];
    $state_id = 0;
    $spark_state_id = 0;
    if (isset($_POST['spark_id']) and $_POST['spark_id'] > 0)
    {
      $spark_state_id = (isset($_POST['spark_state_id']) ? $_POST['spark_state_id'] : 0);
      $spark_detail = $_POST['spark_id'];
      $spark_details = explode("-",$spark_detail);
      $spark_id = $spark_details[0];
      $sel_spark_id = $spark_id;
    }
    if($this->data['current_role'] == 'state_person' || $this->data['current_role'] == 'manager'){
      $spark_state_id = $this->data['user_state_id'];
    }
    
    $this->load->Model('Users_model');
    $spark_data = $this->Users_model->getById($spark_id);
    $spark_name = $spark_data['name'];
    $this->data['current_group'] = $spark_data['user_group'];
    
    $claim_data = array();
    $activitydatestart = date('Y-m-d');
    $activitydateend = date('Y-m-d');
    if(isset($_POST) && !empty($_POST['submit']))
    {
      $activitydatestart = (!empty($_POST['activitydatestart']) ? date('Y-m-d',strtotime($_POST['activitydatestart'])) : $activitydatestart);
      $activitydateend = (!empty($_POST['activitydateend']) ? date('Y-m-d',strtotime($_POST['activitydateend'])) : $activitydateend);
    }
    
    $claim_data = $this->Claim_model->get_stay_claims_list($spark_id, $activitydatestart, $activitydateend);
    
    $spark_ids = array();
    foreach($claim_data as $claims)
    {
			$stay_with = json_decode($claims->paid_spark_id);
			if(!empty($stay_with))
			{
				foreach($stay_with as $sparkid){
					if(!in_array($sparkid, $spark_ids))
						array_push($spark_ids, $sparkid);
				}
			}
		}
		
		$spark_with_names = array();
		if(!empty($spark_ids))
		{
			$spark_data = $this->Users_model->getByIds($spark_ids);
			foreach($spark_data as $sparkdata)
			{
				$spark_with_names[$sparkdata->id] = $sparkdata->name;	 
			}
		}
		
    
    $custom_scripts = ['map.js']; 
    $this->data['activitydatestart'] = date('d-m-Y',strtotime($activitydatestart)); 
    $this->data['activitydateend'] = date('d-m-Y',strtotime($activitydateend)); 
    $this->data['custom_scripts'] = $custom_scripts; 
    $this->data['claim_data'] = $claim_data;
    $this->data['sel_spark_id'] = $sel_spark_id;
    $this->data['spark_id'] = $spark_id;
    $this->data['spark_name'] = $spark_name;
    $this->data['spark_with_names'] = $spark_with_names;
    $this->data['spark_state_id'] = $spark_state_id;
    //$this->data['upload_proofs'] = $upload_proofs;
    
    $download = (isset($_POST['download']) and $_POST['download'] == 1 ? $_POST['download'] : 0);
    $pdf = (isset($_POST['pdf']) and $_POST['pdf'] == 1 ? $_POST['pdf'] : 0);
    
		if ($download == 1)
    {
      $myFile = "./uploads/reports/stay_claims_list.xls";
      $this->load->library('parser');
			$this->data['download'] = $download;
      //pass retrieved data into template and return as a string
      $stringData = $this->parser->parse('map/stay_claims_list', $this->data, true);
      //open excel and write string into excel
      $fh = fopen($myFile, 'w') or die("can't open file");
      fwrite($fh, $stringData);
      fclose($fh);
      //download excel file
      $this->common_functions->downloadExcel("stay_claims_list");
      unlink($myFile);
    }
    else if($pdf == 1)
    {
        //pass retrieved data into template and return as a string
        $this->data['pdf'] = 1;
        $summary = $this->load->view('map/stay_claims_list',$this->data,true);
        $this->common_functions->create_pdf($summary,'stay_claims_list');
    }
    else{
      $this->data['partial'] = 'map/stay_claims_list';			
      $this->load->view('templates/index', $this->data);
    }
  }
  
  public function process_stay_claims()
  {
    if(isset($_POST) && $_POST['processClaim'] == 'Process Claim'){
      
      $spark_id = $_POST['spark_id'];
      $record_ids = $_POST['record_id'];
      
      $count = count($record_ids);
      for($i=0; $i<$count; $i++)
      {
        $rid = $record_ids[$i];
        //echo"<br>".$_POST['claim_discard'][$rid];
        $sdata = array();
        if($_POST['claim_status'][$rid] != 'rejected' && $_POST['claim_status'][$rid] != 'processed' && $_POST['claim_status'][$rid] != 'referback'){
					
					$this->post_stay_claim_process($rid, $spark_id);
        }
      }
      $this->Users_model->update_spark_tally_app_no($spark_id, 'stay');
      
      $msg = 'Claim processed successfully';
    }
    else if(isset($_POST) && $_POST['processClaim'] == 'Update Claim'){
			
      $record_ids = $_POST['record_id'];
      $count = count($record_ids);
      for($i=0; $i<$count; $i++)
      {
        $rid = $record_ids[$i];
        if($_POST['claim_status'][$rid] != 'rejected' && $_POST['claim_status'][$rid] != 'processed'){
          $data['comment'] = $_POST['comment'][$i];
          $data['updated_by'] = $this->data['current_user_id'];
          $data['updated_on'] = date('Y-m-d');
          $this->Claim_model->update_data('stay_claims', $data, array('id'=>$rid));
        }
      }
      $msg = 'Claim updated successfully';
    }
    
    $this->session->set_flashdata('success', $msg);
    header('location:'.base_url()."map/stay_claims_list");
  }
	
	public function stay_claims_exceed()
  {
    $spark_id = $this->data['current_user_id'];
    $sel_spark_id = $this->data['current_user_id'];
    $spark_ids = array($spark_id);
    $state_id = 0;
    $spark_state_id = 0;
    $spark_name = '';
    if($this->data['current_role'] == 'state_person' || $this->data['current_role'] == 'manager'){
      $spark_state_id = $this->data['user_state_id'];
    }
    
    $spark_name = 0;
    $exceed_value = 0;
    $claim_data = array();
    $spark_names = array();
    $activitydatestart = date('Y-m-d');
    $activitydateend = date('Y-m-d');
    $spark_names = array();
    $data_submit = '';
    $sel_claim_type = '';
    if(isset($_POST) && !empty($_POST['submit']))
    {
      $activitydatestart = (!empty($_POST['activitydatestart']) ? date('Y-m-d',strtotime($_POST['activitydatestart'])) : date('Y-m-d'));
      $activitydateend = (!empty($_POST['activitydateend']) ? date('Y-m-d',strtotime($_POST['activitydateend'])) : date('Y-m-d'));
      
      $spark_state_id = (isset($_POST['spark_state_id']) ? $_POST['spark_state_id'] : 0);
      $sel_claim_type = (isset($_POST['claim_type']) ? $_POST['claim_type'] : '');
      $exceed_value = $_POST['exceed_value'];
      
      if (isset($_POST['spark_id']) and $_POST['spark_id'] > 0)
      {
        $spark_detail = $_POST['spark_id'];
        $spark_details = explode("-",$spark_detail);
        $spark_id = $spark_details[0];
        $sel_spark_id = $spark_id;
        $spark_ids = array($spark_id);
        $this->load->Model('Sparks_model');
        $spark_data = $this->Sparks_model->getById($spark_id);
        $spark_name = $spark_data['name'];
        $this->data['current_group'] = $spark_data['user_group'];
      }  
      else if($spark_state_id > 0 || $spark_state_id == 'HO')
      {
        $this->load->Model('Users_model');
        
        if($_POST['spark_state_id'] == 'HO'){
					$state = 0;
					$spark_role = array('manager', 'accounts', 'field_user');
					$user_group = array('head_office');  
					$users_data = $this->Users_model->getAllSparksByState($state, $activitydatestart, $activitydateend, '', $spark_role, $user_group);
				}
				else{
					$users_data = $this->Users_model->getSparkByStateDuration($_POST['spark_state_id'], $activitydatestart, $activitydateend,'', array('field_user','manager','state_person'));
					$this->data['current_group'] = 'field_user';
				}
					
        $spark_id = '';
        $sel_spark_id = '';
        $spark_ids = array();
        foreach($users_data as $users){
          if(!in_array($users['user_id'], $spark_ids)){
            array_push($spark_ids, $users['user_id']);
            $spark_names[$users['user_id']]= $users['name'];
          }
        }
      }
      $data_submit = 'submitted';
    }
    
    $claim_data = $this->Claim_model->get_stay_exceed_claims_list($spark_ids, $activitydatestart, $activitydateend, $sel_claim_type);

    $process_dates = $this->Claim_model->get_stay_process_dates();
    
    $spark_ids = array();
    //$upload_proofs = array();
    foreach($claim_data as $claims)
    {
			$stay_with = json_decode($claims->paid_spark_id);
			if(!empty($stay_with))
			{
				foreach($stay_with as $sparkid){
					if(!in_array($sparkid, $spark_ids))
						array_push($spark_ids, $sparkid);
				}
			}
			
			/*$uploadData = $this->Claim_model->get_data('claim_proofs', array('claim_type'=>'Stay Claim', 'claim_id'=>$claims->id));
			
			if(!empty($uploadData))
				$upload_proofs[$claims->id] = $uploadData[0]->file_name;
			else
				$upload_proofs[$claims->id] = '';*/
		}
		
		$spark_with_names = array();
		if(!empty($spark_ids))
		{
			$spark_data = $this->Users_model->getByIds($spark_ids);
			foreach($spark_data as $sparkdata)
			{
				$spark_with_names[$sparkdata->id] = $sparkdata->name;	 
			}
		}
		

    $custom_scripts = ['map.js']; 
    $this->data['sel_claim_type'] = $sel_claim_type;
    $this->data['process_dates'] = $process_dates; 
    $this->data['data_submit'] = $data_submit; 
    $this->data['exceed_value'] = $exceed_value; 
    $this->data['activitydatestart'] = date('d-m-Y',strtotime($activitydatestart)); 
    $this->data['activitydateend'] = date('d-m-Y',strtotime($activitydateend)); 
    $this->data['custom_scripts'] = $custom_scripts; 
    $this->data['claim_data'] = $claim_data;
    $this->data['spark_id'] = $spark_id;
    $this->data['sel_spark_id'] = $sel_spark_id;
    $this->data['spark_name'] = $spark_name;
    $this->data['spark_names'] = $spark_names;
    $this->data['spark_state_id'] = $spark_state_id;
    $this->data['spark_with_names'] = $spark_with_names;
    //$this->data['upload_proofs'] = $upload_proofs;
    
    $download = (isset($_POST['download']) and $_POST['download'] == 1 ? $_POST['download'] : 0);
    $pdf = (isset($_POST['pdf']) and $_POST['pdf'] == 1 ? $_POST['pdf'] : 0);
    
		if ($download == 1)
    {
      $myFile = "./uploads/reports/stay_claims_exceed.xls";
      $this->load->library('parser');
			$this->data['download'] = $download;
      //pass retrieved data into template and return as a string
      $stringData = $this->parser->parse('map/stay_claims_exceed', $this->data, true);
      //open excel and write string into excel
      $fh = fopen($myFile, 'w') or die("can't open file");
      fwrite($fh, $stringData);
      fclose($fh);
      //download excel file
      $this->common_functions->downloadExcel("stay_claims_exceed");
      unlink($myFile);
    }
    else if($pdf == 1)
    {
        //pass retrieved data into template and return as a string
        $this->data['pdf'] = 1;
        $summary = $this->load->view('map/stay_claims_exceed',$this->data,true);
        $this->common_functions->create_pdf($summary,'stay_claims_exceed');
    }
    else{
      $this->data['partial'] = 'map/stay_claims_exceed';			
      $this->load->view('templates/index', $this->data);
    }
  }
  
	public function auto_stay_claims_process()
  {
    $spark_id = $this->data['current_user_id'];
    $state_id = 0;
    $spark_state_id = 0;
    if($this->data['current_role'] == 'state_person' || $this->data['current_role'] == 'manager'){
      $spark_state_id = $this->data['user_state_id'];
    }
    
    $spark_ids = array();
    $claim_data = array();
    $activitydatestart = date('Y-m-d');
    $activitydateend = date('Y-m-d');
    
    if(isset($_POST) && !empty($_POST['claimProcess']))
    {
      $activitydatestart = (!empty($_POST['activitydatestart']) ? date('Y-m-d',strtotime($_POST['activitydatestart'])) : date('Y-m-d'));
      $activitydateend = (!empty($_POST['activitydateend']) ? date('Y-m-d',strtotime($_POST['activitydateend'])) : date('Y-m-d'));
      
      $spark_state_id = (isset($_POST['spark_state_id']) ? $_POST['spark_state_id'] : 0);
      
      if (isset($_POST['spark_id']) and $_POST['spark_id'] > 0)
      {
        $spark_detail = $_POST['spark_id'];
        $spark_details = explode("-",$spark_detail);
        $spark_id = $spark_details[0];
        $sel_spark_id = $spark_id;
        $spark_ids = array($spark_id);
        $this->load->Model('Sparks_model');
        $spark_data = $this->Sparks_model->getById($spark_id);
        $spark_name = $spark_data['name'];
      }  
      else if($spark_state_id > 0 || $spark_state_id == 'HO')
      {
        $this->load->Model('Users_model');
        if($_POST['spark_state_id'] == 'HO'){
					$state = 0;
					$spark_role = array('manager', 'accounts', 'field_user');
					$user_group = array('head_office');  
					$users_data = $this->Users_model->getAllSparksByState($state, $activitydatestart, $activitydateend, '', $spark_role, $user_group);
				}
				else{
					$users_data = $this->Users_model->getSparkByStateDuration($_POST['spark_state_id'], $activitydatestart, $activitydateend,'', array('field_user','manager','state_person'));
				}
					
        $sel_spark_id = '';
        $spark_id = '';
        $spark_ids = array();
        foreach($users_data as $users){
          if(!in_array($users['user_id'], $spark_ids)){
            array_push($spark_ids, $users['user_id']);
            $spark_names[$users['user_id']]= $users['name'];
          }
        }
      }
    }
    
    if(!empty($spark_ids)){
      
      $claim_data = $this->Claim_model->get_stay_exceed_claims_list($spark_ids, $activitydatestart, $activitydateend, 'approved');
      
      $sparkIds = array();
      foreach($claim_data as $e_key){ 
        
        $rid = $e_key->id;
        $spark_id = $e_key->spark_id;
				
        if($e_key->claim_amount > 0 && $e_key->file_name == '' ){
					// do nothing
				}
				else{	
					if(!in_array($spark_id, $sparkIds))
						array_push($sparkIds, $spark_id);
					
					$this->post_stay_claim_process($rid, $spark_id);
        }
      }
      
      foreach($sparkIds as $sparks)
      {
        $this->Users_model->update_spark_tally_app_no($sparks, 'stay');
      }
      $this->session->set_flashdata('success', "Claim processed successfully");
    }
    else{
      $this->session->set_flashdata('alert', "No spark to process claim");
    }
    redirect('map/stay_claims_exceed');
  }
  
  function post_stay_claim_process($rid, $spark_id)
  {
		$sparkData = $this->Users_model->getById($spark_id);
    $tally_app_no = $sparkData['stay_tally_app_no'];
      
		$claim_exceed = 0;
		$claimData = $this->Claim_model->get_stay_claims($rid);
		
		$status_remark = $claimData['status_remark'];
		$claim_date = $claimData['from_date'];
		$claim_month_yr = date('Y-m', strtotime($claim_date));
		$claim_date_expense = $claimData['claim_amount'];
		
		$spark_month_expense = $this->Claim_model->check_month_claim('Stay Claim', $spark_id, $claim_month_yr); 
		
		if(!empty($spark_month_expense))
		{
			$month_total_expense = $spark_month_expense['total_expense'];
			
			if($month_total_expense > STAY_CLAIM_MONTHLY_LIMIT)
			{
				$sdata['status_remark'] = ($status_remark != '' ? $status_remark.' <br/ >Rejected: montly claim limit exceed.' : 'Rejected: montly claim limit exceed.');
				$sdata['claim_status'] = 'rejected';
				$sdata['updated_on'] = date('Y-m-d H:i:s');
				$sdata['updated_by'] = $this->data['current_user_id'];
				$sdata['process_date'] = date('Y-m-d');
				$res = $this->Claim_model->update_data('stay_claims', $sdata, array('id'=>$rid));
				$claim_exceed = 1;
			}
			else{
				$date_expense_data = $this->Claim_model->check_date_claim('Stay Claim', $spark_id, $claim_date);		
				if(!empty($date_expense_data))
				{
					$udata['date_expense'] = $date_expense_data['date_expense'] + $claim_date_expense;
					$this->Claim_model->update_data('spark_month_claims', $udata, array('id'=>$date_expense_data['id']));
				}
				else{
					$sdata['claim_type'] = 'Stay Claim';
					$sdata['spark_id'] = $spark_id;
					$sdata['activity_date'] = $claim_date;
					$sdata['activity_month'] = $claim_month_yr;
					$sdata['date_expense'] = $claim_date_expense;
					$this->Claim_model->addStayActivity($sdata, 'spark_month_claims');
				}
			}
		}
		else{
			$sdata['claim_type'] = 'Stay Claim';
			$sdata['spark_id'] = $spark_id;
			$sdata['activity_date'] = $claim_date;
			$sdata['activity_month'] = $claim_month_yr;
			$sdata['date_expense'] = $claim_date_expense;
			$this->Claim_model->addStayActivity($sdata, 'spark_month_claims');
		}
		
		if($claim_exceed == 0){
			$data['claimed_by'] = $this->data['current_user_id'];
			$data['updated_by'] = $this->data['current_user_id'];
			$data['updated_on'] = date('Y-m-d');
			$data['claim_status'] = 'processed';
			$data['tally_app_no'] = $tally_app_no+1;
			$data['process_date'] = date('Y-m-d');
			$this->Claim_model->update_data('stay_claims', $data, array('id'=>$rid));
		}
	
	}
  
  public function stay_claims_import()
  {
    $spark_id = $this->data['current_user_id'];
    $state_id = 0;
    $spark_state_id = 0;
    if($this->data['current_role'] == 'state_person' || $this->data['current_role'] == 'manager'){
      $spark_state_id = $this->data['user_state_id'];
    }

    $spark_ids = array();
    $claim_data = array();
    $activitydatestart = date('Y-m-d');
    $activitydateend = date('Y-m-d');
    
    if(isset($_POST) && !empty($_POST['tallyButton']))
    {
      $process_date = (!empty($_POST['process_date']) ? date('Y-m-d',strtotime($_POST['process_date'])) : date('Y-m-d'));
      $spark_state_id = (isset($_POST['spark_state_id']) ? $_POST['spark_state_id'] : 0);
      
      $this->load->Model('Users_model');
      if($_POST['spark_state_id'] == 'HO'){
				$state = 0;
				$spark_role = array('manager', 'accounts', 'field_user');
				$user_group = array('head_office');  
				$users_data = $this->Users_model->getSparksByCurrentState($state, $spark_role, $user_group);
			}
			else{
				$users_data = $this->Users_model->getSparksByCurrentState($_POST['spark_state_id'], array('field_user','manager','state_person'));
			}
      
      $sel_spark_id = '';
      $spark_ids = array();
      foreach($users_data as $users){
        if(!in_array($users->id, $spark_ids)){
          array_push($spark_ids, $users->id);
          $spark_names[$users->id]= $users->name;
        }
      }
      
      $spark_data = $this->Users_model->getById($spark_id);
      $spark_name = $spark_data['name'];
      
      $claim_data = $this->Claim_model->get_track_tally_stay_list($spark_ids, $process_date);
      //print_r($claim_data);
      
      if(!empty($claim_data)){
        $this->data['claim_data'] = $claim_data;
        
        $this->data['report_type'] = $_POST['tallyButton'];
        $this->data['claim_data'] = $claim_data;
        $this->data['spark_name'] = $spark_name;
        $this->data['spark_state_id'] = $spark_state_id;
        
        $myFile = "./uploads/reports/stay_claims_import.xls";
        $this->load->library('parser');
        //pass retrieved data into template and return as a string
        $stringData = $this->parser->parse('map/stay_claims_import', $this->data, true);
        //open excel and write string into excel
        $fh = fopen($myFile, 'w') or die("can't open file");
        fwrite($fh, $stringData);
        fclose($fh);
        //download excel file
        $this->common_functions->downloadExcel("stay_claims_import");
        unlink($myFile);
      }
      else{
        $this->session->set_flashdata('alert', "No claim found for selected process date.");
        redirect('map/other_claims_exceed');
      }
    }
  }
  
  //END STAY CLAIM FUNCTIONS	
	
	
	//START FOOD CLAIM FUNCTIONS	
  public function food_claims_list()
  {
    $spark_id = $this->data['current_user_id'];
    $sel_spark_id = $this->data['current_user_id'];
    $state_id = 0;
    $spark_state_id = 0;
    if($this->data['current_role'] == 'state_person' || $this->data['current_role'] == 'manager'){
      $spark_state_id = $this->data['user_state_id'];
    }
    if (isset($_POST['spark_id']) and $_POST['spark_id'] > 0)
    {
      $spark_state_id = (isset($_POST['spark_state_id']) ? $_POST['spark_state_id'] : 0);
      $spark_detail = $_POST['spark_id'];
      $spark_details = explode("-",$spark_detail);
      $spark_id = $spark_details[0];
      $sel_spark_id = $spark_id;
    }
    $this->load->Model('Users_model');
    $spark_data = $this->Users_model->getById($spark_id);
    $spark_name = $spark_data['name'];
    $this->data['current_group'] = $spark_data['user_group'];
    
    $claim_data = array();
    $activitydatestart = date('Y-m-d');
    $activitydateend = date('Y-m-d');
    if(isset($_POST) && !empty($_POST['submit']))
    {
      $activitydatestart = (!empty($_POST['activitydatestart']) ? date('Y-m-d',strtotime($_POST['activitydatestart'])) : $activitydatestart);
      $activitydateend = (!empty($_POST['activitydateend']) ? date('Y-m-d',strtotime($_POST['activitydateend'])) : $activitydateend);
    }
    
    $claim_data = $this->Claim_model->get_food_claims_list($spark_id, $activitydatestart, $activitydateend);
    
    $spark_ids = array();
    foreach($claim_data as $claims)
    {
			$stay_with = json_decode($claims->paid_spark_id);
			if(!empty($stay_with))
			{
				foreach($stay_with as $sparkid){
					if(!in_array($sparkid, $spark_ids))
						array_push($spark_ids, $sparkid);
				}
			}
		}
		
		$spark_with_names = array();
		if(!empty($spark_ids))
		{
			$spark_data = $this->Users_model->getByIds($spark_ids);
			foreach($spark_data as $sparkdata)
			{
				$spark_with_names[$sparkdata->id] = $sparkdata->name;	 
			}
		}
    
    $custom_scripts = ['map.js']; 
    $this->data['activitydatestart'] = date('d-m-Y',strtotime($activitydatestart)); 
    $this->data['activitydateend'] = date('d-m-Y',strtotime($activitydateend)); 
    $this->data['custom_scripts'] = $custom_scripts; 
    $this->data['claim_data'] = $claim_data;
    $this->data['sel_spark_id'] = $sel_spark_id;
    $this->data['spark_id'] = $spark_id;
    $this->data['spark_name'] = $spark_name;
    $this->data['spark_with_names'] = $spark_with_names;
    $this->data['spark_state_id'] = $spark_state_id;
    
    $download = (isset($_POST['download']) and $_POST['download'] == 1 ? $_POST['download'] : 0);
    $pdf = (isset($_POST['pdf']) and $_POST['pdf'] == 1 ? $_POST['pdf'] : 0);
    
		if ($download == 1)
    {
      $myFile = "./uploads/reports/food_claims_list.xls";
      $this->load->library('parser');
			$this->data['download'] = $download;
      //pass retrieved data into template and return as a string
      $stringData = $this->parser->parse('map/food_claims_list', $this->data, true);
      //open excel and write string into excel
      $fh = fopen($myFile, 'w') or die("can't open file");
      fwrite($fh, $stringData);
      fclose($fh);
      //download excel file
      $this->common_functions->downloadExcel("food_claims_list");
      unlink($myFile);
    }
    else if($pdf == 1)
    {
        //pass retrieved data into template and return as a string
        $this->data['pdf'] = 1;
        $summary = $this->load->view('map/food_claims_list',$this->data,true);
        $this->common_functions->create_pdf($summary,'food_claims_list');
    }
    else{
      $this->data['partial'] = 'map/food_claims_list';			
      $this->load->view('templates/index', $this->data);
    }
  }
  
  public function process_food_claims()
  {
    if(isset($_POST) && $_POST['processClaim'] == 'Process Claim'){
      
      $spark_id = $_POST['spark_id'];
      $record_ids = $_POST['record_id'];
      
      $count = count($record_ids);
      for($i=0; $i<$count; $i++)
      {
        $rid = $record_ids[$i];
        //echo"<br>".$_POST['claim_discard'][$rid];
        $sdata = array();
        if($_POST['claim_status'][$rid] != 'rejected' && $_POST['claim_status'][$rid] != 'processed' && $_POST['claim_status'][$rid] != 'referback'){
					
					$this->post_food_claim_process($rid, $spark_id);
        }
      }
      $this->Users_model->update_spark_tally_app_no($spark_id, 'food');
      
      $msg = 'Claim processed successfully';
    }
    else if(isset($_POST) && $_POST['processClaim'] == 'Update Claim'){
			
      $record_ids = $_POST['record_id'];
      $count = count($record_ids);
      
      for($i=0; $i<$count; $i++)
      {
        $rid = $record_ids[$i];
        if($_POST['claim_status'][$rid] != 'rejected' && $_POST['claim_status'][$rid] != 'processed' && $_POST['claim_status'][$rid] != 'referback'){
          $data['manual_cost'] = $_POST['manual_cost'][$rid];
          $data['comment'] = $_POST['comment'][$rid];
          $data['updated_by'] = $this->data['current_user_id'];
          $data['updated_on'] = date('Y-m-d');
          print_r($data);
          $this->Claim_model->update_data('food_claims', $data, array('id'=>$rid));
        }
      }
      $msg = 'Claim updated successfully';
    }
    
    $this->session->set_flashdata('success', $msg);
    header('location:'.base_url()."map/food_claims_list");
  }
  
	public function food_claims_exceed()
  {
    $spark_id = $this->data['current_user_id'];
    $sel_spark_id = $this->data['current_user_id'];
    $spark_ids = array($spark_id);
    $state_id = 0;
    $spark_state_id = 0;
    $spark_name = '';
    if($this->data['current_role'] == 'state_person' || $this->data['current_role'] == 'manager'){
      $spark_state_id = $this->data['user_state_id'];
    }
    
    $spark_name = 0;
    $exceed_value = 0;
    $claim_data = array();
    $spark_names = array();
    $activitydatestart = date('Y-m-d');
    $activitydateend = date('Y-m-d');
    $spark_names = array();
    $data_submit = '';
    $sel_claim_type = '';
    if(isset($_POST) && !empty($_POST['submit']))
    {
      $activitydatestart = (!empty($_POST['activitydatestart']) ? date('Y-m-d',strtotime($_POST['activitydatestart'])) : date('Y-m-d'));
      $activitydateend = (!empty($_POST['activitydateend']) ? date('Y-m-d',strtotime($_POST['activitydateend'])) : date('Y-m-d'));
      
      $spark_state_id = (isset($_POST['spark_state_id']) ? $_POST['spark_state_id'] : 0);
      $sel_claim_type = (isset($_POST['claim_type']) ? $_POST['claim_type'] : '');
      $exceed_value = $_POST['exceed_value'];
      
      if (isset($_POST['spark_id']) and $_POST['spark_id'] > 0)
      {
        $spark_detail = $_POST['spark_id'];
        $spark_details = explode("-",$spark_detail);
        $spark_id = $spark_details[0];
        $sel_spark_id = $spark_id;
        $spark_ids = array($spark_id);
        $this->load->Model('Sparks_model');
        $spark_data = $this->Sparks_model->getById($spark_id);
        $spark_name = $spark_data['name'];
        $this->data['current_group'] = $spark_data['user_group'];
      }  
      else if($spark_state_id > 0 || $spark_state_id == 'HO')
      {
        $this->load->Model('Users_model');
        
        if($_POST['spark_state_id'] == 'HO'){
					$state = 0;
					$spark_role = array('manager', 'accounts', 'field_user');
					$user_group = array('head_office');  
					$users_data = $this->Users_model->getAllSparksByState($state, $activitydatestart, $activitydateend, '', $spark_role, $user_group);
				}
				else{
					$users_data = $this->Users_model->getSparkByStateDuration($_POST['spark_state_id'], $activitydatestart, $activitydateend,'', array('field_user','manager','state_person'));
					$this->data['current_group'] = 'field_user';
				}

        $spark_id = '';
        $sel_spark_id = '';
        $spark_ids = array();
        foreach($users_data as $users){
          if(!in_array($users['user_id'], $spark_ids)){
            array_push($spark_ids, $users['user_id']);
            $spark_names[$users['user_id']]= $users['name'];
          }
        }
      }
      $data_submit = 'submitted';
    }
    
    $claim_data = $this->Claim_model->get_food_exceed_claims_list($spark_ids, $activitydatestart, $activitydateend, $sel_claim_type, 'ssc_food_claims');

    $process_dates = $this->Claim_model->get_stay_process_dates('ssc_food_claims');
    
    $spark_ids = array();
    foreach($claim_data as $claims)
    {
			$stay_with = json_decode($claims->paid_spark_id);
			if(!empty($stay_with))
			{
				foreach($stay_with as $sparkid){
					if(!in_array($sparkid, $spark_ids))
						array_push($spark_ids, $sparkid);
				}
			}
		}
		
		$spark_with_names = array();
		if(!empty($spark_ids))
		{
			$spark_data = $this->Users_model->getByIds($spark_ids);
			foreach($spark_data as $sparkdata)
			{
				$spark_with_names[$sparkdata->id] = $sparkdata->name;	 
			}
		}
		

    $custom_scripts = ['map.js']; 
    $this->data['sel_claim_type'] = $sel_claim_type;
    $this->data['process_dates'] = $process_dates; 
    $this->data['data_submit'] = $data_submit; 
    $this->data['exceed_value'] = $exceed_value; 
    $this->data['activitydatestart'] = date('d-m-Y',strtotime($activitydatestart)); 
    $this->data['activitydateend'] = date('d-m-Y',strtotime($activitydateend)); 
    $this->data['custom_scripts'] = $custom_scripts; 
    $this->data['claim_data'] = $claim_data;
    $this->data['spark_id'] = $spark_id;
    $this->data['sel_spark_id'] = $sel_spark_id;
    $this->data['spark_name'] = $spark_name;
    $this->data['spark_names'] = $spark_names;
    $this->data['spark_state_id'] = $spark_state_id;
    $this->data['spark_with_names'] = $spark_with_names;
    
    $download = (isset($_POST['download']) and $_POST['download'] == 1 ? $_POST['download'] : 0);
    $pdf = (isset($_POST['pdf']) and $_POST['pdf'] == 1 ? $_POST['pdf'] : 0);
    
		if ($download == 1)
    {
      $myFile = "./uploads/reports/food_claims_exceed.xls";
      $this->load->library('parser');
			$this->data['download'] = $download;
      //pass retrieved data into template and return as a string
      $stringData = $this->parser->parse('map/food_claims_exceed', $this->data, true);
      //open excel and write string into excel
      $fh = fopen($myFile, 'w') or die("can't open file");
      fwrite($fh, $stringData);
      fclose($fh);
      //download excel file
      $this->common_functions->downloadExcel("food_claims_exceed");
      unlink($myFile);
    }
    else if($pdf == 1)
    {
        //pass retrieved data into template and return as a string
        $this->data['pdf'] = 1;
        $summary = $this->load->view('map/food_claims_exceed',$this->data,true);
        $this->common_functions->create_pdf($summary,'food_claims_exceed');
    }
    else{
      $this->data['partial'] = 'map/food_claims_exceed';			
      $this->load->view('templates/index', $this->data);
    }
  }
  
	public function auto_food_claims_process()
  {
    $spark_id = $this->data['current_user_id'];
    $state_id = 0;
    $spark_state_id = 0;
    if($this->data['current_role'] == 'state_person' || $this->data['current_role'] == 'manager'){
      $spark_state_id = $this->data['user_state_id'];
    }
    
    $spark_ids = array();
    $claim_data = array();
    $activitydatestart = date('Y-m-d');
    $activitydateend = date('Y-m-d');
    
    if(isset($_POST) && !empty($_POST['claimProcess']))
    {
      $activitydatestart = (!empty($_POST['activitydatestart']) ? date('Y-m-d',strtotime($_POST['activitydatestart'])) : date('Y-m-d'));
      $activitydateend = (!empty($_POST['activitydateend']) ? date('Y-m-d',strtotime($_POST['activitydateend'])) : date('Y-m-d'));
      
      $spark_state_id = (isset($_POST['spark_state_id']) ? $_POST['spark_state_id'] : 0);
      
      if (isset($_POST['spark_id']) and $_POST['spark_id'] > 0)
      {
        $spark_detail = $_POST['spark_id'];
        $spark_details = explode("-",$spark_detail);
        $spark_id = $spark_details[0];
        $sel_spark_id = $spark_id;
        $spark_ids = array($spark_id);
        $this->load->Model('Sparks_model');
        $spark_data = $this->Sparks_model->getById($spark_id);
        $spark_name = $spark_data['name'];
      }  
      else if($spark_state_id > 0 || $spark_state_id == 'HO')
      {
        $this->load->Model('Users_model');
        
        if($_POST['spark_state_id'] == 'HO'){
					$state = 0;
					$spark_role = array('manager', 'accounts', 'field_user');
					$user_group = array('head_office');  
					$users_data = $this->Users_model->getAllSparksByState($state, $activitydatestart, $activitydateend, '', $spark_role, $user_group);
				}
				else{
					$users_data = $this->Users_model->getSparkByStateDuration($_POST['spark_state_id'], $activitydatestart, $activitydateend,'', array('field_user','manager','state_person'));
				}
        
        $sel_spark_id = '';
        $spark_id = '';
        $spark_ids = array();
        foreach($users_data as $users){
          if(!in_array($users['user_id'], $spark_ids)){
            array_push($spark_ids, $users['user_id']);
            $spark_names[$users['user_id']]= $users['name'];
          }
        }
      }
    }
    
    if(!empty($spark_ids)){
      
      $claim_data = $this->Claim_model->get_stay_unprocessed_list($spark_ids, $activitydatestart, $activitydateend, 'food_claims');
      
      $sparkIds = array();
      foreach($claim_data as $e_key){ 
        
        $rid = $e_key->id;
        $spark_id = $e_key->spark_id;
				
        if($e_key->manual_cost > 0){
					
					if(!in_array($spark_id, $sparkIds))
						array_push($sparkIds, $spark_id);
						
					$this->post_food_claim_process($rid, $spark_id);
        }
      }
      
      foreach($sparkIds as $sparks)
      {
        $this->Users_model->update_spark_tally_app_no($sparks, 'food');
      }
      $this->session->set_flashdata('success', "Claim processed successfully");
    }
    else{
      $this->session->set_flashdata('alert', "No spark to process claim");
    }
    redirect('map/food_claims_exceed');
  }
  
  function post_food_claim_process($rid, $spark_id)
  {
      $sparkData = $this->Users_model->getById($spark_id);
      $tally_app_no = $sparkData['food_tally_app_no'];
      
			$claim_exceed = 0;
			$claimData = $this->Claim_model->get_food_claims($rid);
			
			$status_remark = $claimData['status_remark'];
			$claim_date = $claimData['from_date'];
			$claim_month_yr = date('Y-m', strtotime($claim_date));
			$claim_date_expense = $claimData['manual_cost'];;
			
			$spark_month_expense = $this->Claim_model->check_month_claim('Food Claim', $spark_id, $claim_month_yr); 
			
			if(!empty($spark_month_expense))
			{
				$month_total_expense = $spark_month_expense['total_expense'];
				
				if($month_total_expense > FOOD_CLAIM_MONTHLY_LIMIT)
				{
					$sdata['status_remark'] = ($status_remark != '' ? $status_remark.' <br/ >Rejected: montly claim limit exceed.' : 'Rejected: montly claim limit exceed.');
					$sdata['claim_status'] = 'rejected';
					$sdata['updated_on'] = date('Y-m-d H:i:s');
					$sdata['updated_by'] = $this->data['current_user_id'];
					$sdata['process_date'] = date('Y-m-d');
					$res = $this->Claim_model->update_data('food_claims', $sdata, array('id'=>$rid));
					$claim_exceed = 1;
				}
				else{
					$date_expense_data = $this->Claim_model->check_date_claim('Food Claim', $spark_id, $claim_date);		
					if(!empty($date_expense_data))
					{
						$udata['date_expense'] = $date_expense_data['date_expense'] + $claim_date_expense;
						$this->Claim_model->update_data('spark_month_claims', $udata, array('id'=>$date_expense_data['id']));
					}
					else{
						$sdata['claim_type'] = 'Food Claim';
						$sdata['spark_id'] = $spark_id;
						$sdata['activity_date'] = $claim_date;
						$sdata['activity_month'] = $claim_month_yr;
						$sdata['date_expense'] = $claim_date_expense;
						$this->Claim_model->addStayActivity($sdata, 'spark_month_claims');
					}
				}
			}
			else{
				$sdata['claim_type'] = 'Food Claim';
				$sdata['spark_id'] = $spark_id;
				$sdata['activity_date'] = $claim_date;
				$sdata['activity_month'] = $claim_month_yr;
				$sdata['date_expense'] = $claim_date_expense;
				$this->Claim_model->addStayActivity($sdata, 'spark_month_claims');
			}
			
			if($claim_exceed == 0){
				$data['claimed_by'] = $this->data['current_user_id'];
				$data['updated_by'] = $this->data['current_user_id'];
				$data['updated_on'] = date('Y-m-d');
				$data['claim_status'] = 'processed';
				$data['tally_app_no'] = $tally_app_no+1;
				$data['process_date'] = date('Y-m-d');
				$this->Claim_model->update_data('food_claims', $data, array('id'=>$rid));
			}
	}
	
  public function food_claims_import()
  {
    $spark_id = $this->data['current_user_id'];
    $state_id = 0;
    $spark_state_id = 0;
    if($this->data['current_role'] == 'state_person' || $this->data['current_role'] == 'manager'){
      $spark_state_id = $this->data['user_state_id'];
    }

    $spark_ids = array();
    $claim_data = array();
    $activitydatestart = date('Y-m-d');
    $activitydateend = date('Y-m-d');
    
    if(isset($_POST) && !empty($_POST['tallyButton']))
    {
      $process_date = (!empty($_POST['process_date']) ? date('Y-m-d',strtotime($_POST['process_date'])) : date('Y-m-d'));
      $spark_state_id = (isset($_POST['spark_state_id']) ? $_POST['spark_state_id'] : 0);
      
      $this->load->Model('Users_model');
      if($_POST['spark_state_id'] == 'HO'){
				$state = 0;
				$spark_role = array('manager', 'accounts', 'field_user');
				$user_group = array('head_office');  
				$users_data = $this->Users_model->getSparksByCurrentState($state, $spark_role, $user_group);
			}
			else{
				$users_data = $this->Users_model->getSparksByCurrentState($_POST['spark_state_id'], array('field_user','manager','state_person'));
			}
      
      $sel_spark_id = '';
      $spark_ids = array();
      foreach($users_data as $users){
        if(!in_array($users->id, $spark_ids)){
          array_push($spark_ids, $users->id);
          $spark_names[$users->id]= $users->name;
        }
      }
      
      $spark_data = $this->Users_model->getById($spark_id);
      $spark_name = $spark_data['name'];
      
      $claim_data = $this->Claim_model->get_track_tally_stay_list($spark_ids, $process_date, 'food_claims');
      //print_r($claim_data);
      
      if(!empty($claim_data)){
        $this->data['claim_data'] = $claim_data;
        
        $this->data['report_type'] = $_POST['tallyButton'];
        $this->data['claim_data'] = $claim_data;
        $this->data['spark_name'] = $spark_name;
        $this->data['spark_state_id'] = $spark_state_id;
        
        $myFile = "./uploads/reports/food_claims_import.xls";
        $this->load->library('parser');
        //pass retrieved data into template and return as a string
        $stringData = $this->parser->parse('map/food_claims_import', $this->data, true);
        //open excel and write string into excel
        $fh = fopen($myFile, 'w') or die("can't open file");
        fwrite($fh, $stringData);
        fclose($fh);
        //download excel file
        $this->common_functions->downloadExcel("food_claims_import");
        unlink($myFile);
      }
      else{
        $this->session->set_flashdata('alert', "No claim found for selected process date.");
        redirect('map/other_claims_exceed');
      }
    }
  }
  //END FOOD CLAIM FUNCTIONS	

  public function spark_claims_list()
  {
    $spark_id = $this->data['current_user_id'];
    $sel_spark_id = $this->data['current_user_id'];
    $state_id = 0;
    $spark_state_id = 0;
    if (isset($_POST['spark_id']) and $_POST['spark_id'] > 0)
    {
      $spark_state_id = (isset($_POST['spark_state_id']) ? $_POST['spark_state_id'] : 0);
      $spark_detail = $_POST['spark_id'];
      $spark_details = explode("-",$spark_detail);
      $spark_id = $spark_details[0];
      $sel_spark_id = $spark_id;
    }
    if($this->data['current_role'] == 'state_person' || $this->data['current_role'] == 'manager'){
      $spark_state_id = $this->data['user_state_id'];
    }
    
    $this->load->Model('Users_model');
    $spark_data = $this->Users_model->getById($spark_id);
    $spark_name = $spark_data['name'];
    $this->data['current_group'] = $spark_data['user_group'];
    
    $claim_data = array();
    $activitydatestart = date('Y-m-d');
    $activitydateend = date('Y-m-d');
    if(isset($_POST) && !empty($_POST['submit']))
    {
      $activitydatestart = (!empty($_POST['activitydatestart']) ? date('Y-m-d',strtotime($_POST['activitydatestart'])) : $activitydatestart);
      $activitydateend = (!empty($_POST['activitydateend']) ? date('Y-m-d',strtotime($_POST['activitydateend'])) : $activitydateend);
    }
    
		$local_claim_data = $this->tracking_model->get_track_claims_list($spark_id, $activitydatestart, $activitydateend);
    $local_claim_count = count($local_claim_data);
    
    $ld_claim_data = $this->Claim_model->get_longdistance_activities($spark_id, $activitydatestart, $activitydateend, '');
    $ld_claim_count = count($ld_claim_data);
    
    $stay_claim_data = $this->Claim_model->get_stay_claims_list($spark_id, $activitydatestart, $activitydateend);
    $stay_claim_count = count($stay_claim_data);
    
    $spark_ids = array();
    foreach($stay_claim_data as $claims)
    {
			$stay_with = json_decode($claims->paid_spark_id);
			if(!empty($stay_with))
			{
				foreach($stay_with as $sparkid){
					if(!in_array($sparkid, $spark_ids))
						array_push($spark_ids, $sparkid);
				}
			}
		}
		
		$food_claim_data = $this->Claim_model->get_food_claims_list($spark_id, $activitydatestart, $activitydateend);
		$food_claim_count = count($food_claim_data);
		
    foreach($food_claim_data as $claims)
    {
			$stay_with = json_decode($claims->paid_spark_id);
			if(!empty($stay_with))
			{
				foreach($stay_with as $sparkid){
					if(!in_array($sparkid, $spark_ids))
						array_push($spark_ids, $sparkid);
				}
			}
		}
		
		$other_claim_data = $this->Claim_model->get_other_claims_list($spark_id, $activitydatestart, $activitydateend);
		$other_claim_count = count($other_claim_data);
		
    $spark_with_names = array();
		if(!empty($spark_ids))
		{
			$spark_data = $this->Users_model->getByIds($spark_ids);
			foreach($spark_data as $sparkdata)
			{
				$spark_with_names[$sparkdata->id] = $sparkdata->name;	 
			}
		}
		
		$this->load->model('travels_model');   
    $travels_mode = $this->travels_model->get_all();
    
    $custom_scripts = ['map.js']; 
    $this->data['activitydatestart'] = date('d-m-Y',strtotime($activitydatestart)); 
    $this->data['activitydateend'] = date('d-m-Y',strtotime($activitydateend)); 
    $this->data['custom_scripts'] = $custom_scripts; 
    $this->data['stay_claim_data'] = $stay_claim_data;
    $this->data['sel_spark_id'] = $sel_spark_id;
    $this->data['spark_id'] = $spark_id;
    $this->data['spark_name'] = $spark_name;
    $this->data['spark_with_names'] = $spark_with_names;
    $this->data['spark_state_id'] = $spark_state_id;
    
    $this->data['ld_claim_data'] = $ld_claim_data;
    $this->data['travels_mode'] = $travels_mode;
    
    $this->data['food_claim_data'] = $food_claim_data;
    
    $this->data['other_claim_data'] = $other_claim_data;
    $this->data['local_claim_data'] = $local_claim_data;
    
    $this->data['count_claim_data'] = $local_claim_count + $ld_claim_count + $stay_claim_count + $food_claim_count + $other_claim_count;
    
    $download = (isset($_POST['download']) and $_POST['download'] == 1 ? $_POST['download'] : 0);
    $pdf = (isset($_POST['pdf']) and $_POST['pdf'] == 1 ? $_POST['pdf'] : 0);
    
		if ($download == 1)
    {
      $myFile = "./uploads/reports/spark_claims_list.xls";
      $this->load->library('parser');
			$this->data['download'] = $download;
      //pass retrieved data into template and return as a string
      $stringData = $this->parser->parse('map/spark_claims_list', $this->data, true);
      //open excel and write string into excel
      $fh = fopen($myFile, 'w') or die("can't open file");
      fwrite($fh, $stringData);
      fclose($fh);
      //download excel file
      $this->common_functions->downloadExcel("spark_claims_list");
      unlink($myFile);
    }
    else if($pdf == 1)
    {
        //pass retrieved data into template and return as a string
        $this->data['pdf'] = 1;
        $summary = $this->load->view('map/spark_claims_list',$this->data,true);
        $this->common_functions->create_pdf($summary,'spark_claims_list');
    }
    else{
      $this->data['partial'] = 'map/spark_claims_list';			
      $this->load->view('templates/index', $this->data);
    }
  }
  
  function process_all_claim()
  {
		$activitydatestart = (!empty($_POST['activitydatestart']) ? date('Y-m-d',strtotime($_POST['activitydatestart'])) : date('Y-m-d'));
    $activitydateend = (!empty($_POST['activitydateend']) ? date('Y-m-d',strtotime($_POST['activitydateend'])) : date('Y-m-d'));
    $spark_id = (isset($_POST['spark_id']) ? $_POST['spark_id'] : 0);
    
    $spark_ids = array($spark_id);
    
    //PROCESS LOCAL CONVENCE 
    $this->post_local_claim_process($spark_ids, $activitydatestart, $activitydateend);
    
    //PROCESS LD TRAVEL 
    $this->post_ld_claim_process($spark_ids, $activitydatestart, $activitydateend);
    
    //PROCESS STAY
    $stay_claim_data = $this->Claim_model->get_stay_exceed_claims_list($spark_ids, $activitydatestart, $activitydateend, 'approved');
      
		foreach($stay_claim_data as $e_key){ 
			$rid = $e_key->id;
			$spark_id = $e_key->spark_id;
			if($e_key->claim_amount > 0 && $e_key->file_name == '' ){
				// do nothing
			}
			else{	
				$this->post_stay_claim_process($rid, $spark_id);
				$this->Users_model->update_spark_tally_app_no($spark_id, 'stay');
			}
		}
		
		//PROCESS FOOD
    $food_claim_data = $this->Claim_model->get_stay_unprocessed_list($spark_ids, $activitydatestart, $activitydateend, 'food_claims');
		
		foreach($food_claim_data as $e_key){ 
			$rid = $e_key->id;
			$spark_id = $e_key->spark_id;
			if($e_key->manual_cost > 0){
				$this->post_food_claim_process($rid, $spark_id);
				$this->Users_model->update_spark_tally_app_no($spark_id, 'food');
			}
		}
		
		//PROCESS OTHER
		$other_claim_data = $this->tracking_model->get_other_unprocessed_list($spark_ids, $activitydatestart, $activitydateend);
      
		foreach($other_claim_data as $e_key){ 
			$rid = $e_key->id;
			$spark_id = $e_key->spark_id;
			if($e_key->amount > 0){
				$this->post_process_other_claim($spark_id, $rid);
				$this->Users_model->update_spark_tally_app_no($spark_id, 'other');
			}
		}
		//die;
		$this->session->set_flashdata('success', 'Claim processed successfully.');
		redirect('map/spark_claims_list', 'refresh');
	}
  
	function upload_claim_bill()
  {
			$claim_id = $_POST['fclaimid'];
			$uploadData = $this->upload_bill($_POST['fclaimtype'], $claim_id, $_FILES);
			echo json_encode($uploadData);
	}
	
	function upload_bill($claim_type, $claim_id, $FILES)
  {
		if(!empty($FILES)){
        $request_folder = "./uploads/claims";
        $request_folder_db = "uploads/claims";
        if (!file_exists($request_folder))
          mkdir($request_folder);
        //$file_name = $_FILES['file_name'];
        $config['upload_path'] = $request_folder;
        $config['allowed_types'] = 'jpg|gif|png|jpeg|JPG|PNG|csv|CSV|pdf|PDF';
        $this->load->library('upload', $config);  
        
        $error = '';
        if (isset($FILES['file_name']) and isset($FILES['file_name']['name']) and $FILES['file_name']['name'] != "")
        {
          $file_ext = $this->upload->get_extension($FILES['file_name']['name']);
          $original_name = $FILES['file_name']['name'];
          
          $file_name = $claim_type."_claim_".time()."_".$claim_id.$file_ext;
          $FILES['file_name']['name'] = $file_name;
          if(file_exists($request_folder."/".$file_name))
          {
             unlink($request_folder."/".$file_name);
          }
          if(!$this->upload->do_upload('file_name'))
          {
            $msg = $this->upload->display_errors();
            $returnData = array('error'=>1, 'msg'=> $msg);
            //$this->session->set_flashdata('alert', $error);
          }
          else
          {
            $upload_data = $this->upload->data();
            $file = $upload_data['file_name'];
            $file_name = $request_folder_db."/".$file;
            
            if($claim_type == 'Other Claim'){
							$chkData = $this->Claim_model->get_claim_proof(array('claim_type'=>$claim_type, 'claim_id'=>$claim_id));
							if(count($chkData) == 1 && $chkData[0]->file_name == ''){
								$cond['claim_id'] = $claim_id;
								$cond['claim_type'] = $claim_type;
								$fdata['updated_on'] = date('Y-m-d H:i:s');
								$fdata['updated_by'] = $this->data['current_user_id'];
								$fdata['file_name'] = $file_name;
								$fdata['original_file'] = $original_name;
								$this->Claim_model->update_claim_proof($fdata, $cond);
							}
							else{
								$udata['claim_id'] = $claim_id;
								$udata['claim_type'] = $claim_type;
								$udata['created_on'] = date('Y-m-d H:i:s');
								$udata['created_by'] = $this->data['current_user_id'];
								$udata['file_name'] = $file_name;
								$udata['original_file'] = $original_name;
								$this->Claim_model->add_claims_proof($udata);
							}
						}
						else{
							$chkData = $this->Claim_model->get_claim_proof(array('claim_type'=>$claim_type, 'claim_id'=>$claim_id));
							if(!empty($chkData)){
								$cond['claim_id'] = $claim_id;
								$cond['claim_type'] = $claim_type;
								$fdata['updated_on'] = date('Y-m-d H:i:s');
								$fdata['updated_by'] = $this->data['current_user_id'];
								$fdata['file_name'] = $file_name;
								$fdata['original_file'] = $original_name;
								$this->Claim_model->update_claim_proof($fdata, $cond);
							}
							else{
								$udata['claim_id'] = $claim_id;
								$udata['claim_type'] = $claim_type;
								$udata['created_on'] = date('Y-m-d H:i:s');
								$udata['created_by'] = $this->data['current_user_id'];
								$udata['file_name'] = $file_name;
								$udata['original_file'] = $original_name;
								$this->Claim_model->add_claims_proof($udata);
							}
						}
						$this->load->Model('schools_model');
						$proofData = $this->schools_model->get_data('claim_proofs', array('claim_type'=>$claim_type, 'claim_id'=>$claim_id));
						
            $returnData = array('error'=>0, 'msg'=> $file_name, 'file_count'=>count($proofData));
            //$this->session->set_flashdata('success', 'Claim posted successfully.');
          }
        }
        else{
					$udata['claim_id'] = $claim_id;
					$udata['claim_type'] = $claim_type;
					$udata['created_on'] = date('Y-m-d H:i:s');
					$udata['created_by'] = $this->data['current_user_id'];
					$this->Claim_model->add_claims_proof($udata);
					
					$this->load->Model('schools_model');
					$proofData = $this->schools_model->get_data('claim_proofs', array('claim_type'=>$claim_type, 'claim_id'=>$claim_id));
					
					$returnData = array('error'=>0, 'msg'=> 'Data saved.', 'file_count'=>count($proofData));
				}
      }
      else{
				$returnData = array('error'=>1, 'msg'=> 'File not found.');
				//$this->session->set_flashdata('alert', 'File not found.');
			}
			return $returnData;
	}
	
	//FUNCTION TO APPROVE OR DISAPPROVE CLAIMS
	public function action_claim()
  {
    $id = $_POST['claim_id'];
    $claim_type = $_POST['claim_type'];
    $action_type = $_POST['action_type'];
    $reason = $_POST['reason'];
    
    $claims = $this->Claim_model->get_data($claim_type, array('id'=>$id));
		
		if($action_type == 'rejected'){
			if($claims[0]->status_remark != '')
				$status_remark = $claims[0]->status_remark.",<br> Rejected: ".$reason." (By ".$this->data['current_username'].")";
			else	
				$status_remark = "Rejected: ".$reason." (By ".$this->data['current_username'].")";
			$mdata['process_date'] = date('Y-m-d');	
		}
		else if($action_type == 'referback'){
			if($claims[0]->status_remark != '')
				$status_remark = $claims[0]->status_remark.",<br> ReferBack: ".$reason." (By ".$this->data['current_username'].")";
			else	
				$status_remark = "ReferBack: ".$reason." (By ".$this->data['current_username'].")";
		}
		else if($action_type == 'canceled'){
			if($claims[0]->status_remark != '')
				$status_remark = $claims[0]->status_remark.",<br> Canceled: ".$reason." (By ".$this->data['current_username'].")";
			else	
				$status_remark = "Canceled: ".$reason." (By ".$this->data['current_username'].")";
		}
		else{
			if($claims[0]->status_remark != '')
				$status_remark = $claims[0]->status_remark.",<br> Approved: ".$reason." (By ".$this->data['current_username'].")";
			else	
				$status_remark = "Approved: ".$reason." (By ".$this->data['current_username'].")";
		}
    $mdata['status_remark'] = $status_remark;
    $mdata['claim_status'] = $action_type;
    $mdata['updated_on'] = date('Y-m-d H:i:s');
    $mdata['updated_by'] = $this->data['current_user_id'];
    $res = $this->Claim_model->update_data($claim_type, $mdata, array('id'=>$id));
    echo $status_remark;
  }
  	
		
	 function get_dates_stay()
	 {
		 $spark_id = $this->data['current_user_id'];
		 $start_date = date('Y-m-d', strtotime($_POST['sdate']));
		 $end_date = date('Y-m-d', strtotime($_POST['edate']));
		 
		 $claim_list = $this->Claim_model->get_stay_claims_list($spark_id, $start_date, $end_date);
		 
	 }
	 
	 
	 function view_bill()
	 {	
			$claim_id = $_POST['cid'];
			$claim_type = $_POST['ctype'];
			
			$this->load->Model('schools_model');
			
			$proofData = $this->schools_model->get_data('claim_proofs', array('claim_type'=>$claim_type, 'claim_id'=>$claim_id));
			
			$content = '';
			$p = 0;
			foreach($proofData as $proofs)
			{
				//print_r($proofs);
				if($proofs->file_name != ''){
					$row_id = $proofs->id;
					
					$file_name = $proofs->original_file;
					if($proofs->original_file == '')
					{
						$exp = explode('/', $proofs->file_name);
						$file_name = $exp[count($exp)-1];
					}					
					$content .= '<tr id="row_'.$row_id.'"><td>';
					$content .= '<a href="'.base_url().$proofs->file_name.'" target="_blank" class="viewfile" download>'.$file_name.'</a>';
					$content .= '</td></tr>';
					//$content .= '</td><td><i class="ti-trash text-success" title="Remove File" onclick="remove_file('.$row_id.')"></i></td></tr>';
					$p++;
				}
			}
			if($p == 0)
			{
				$content .= '<tr><td>No File Uploaded</td></tr>';
			}
			echo $content;
	 }
	 
	 function remove_bill()
	 {
		 $this->load->Model('schools_model');
		 
		 $fileid = $_POST['fileid'];		 
		 $proofData = $this->schools_model->get_data('claim_proofs', array('id'=>$fileid));
		 		 
		 $file_name = $proofData[0]->file_name;		 
		 unlink($file_name);		 
		 $this->schools_model->remove_date('claim_proofs', array('id'=>$fileid));		 
	 }
	 
}

