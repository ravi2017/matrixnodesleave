<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Activity_approval extends CI_Controller {

	function __construct()	
	{
		parent::__construct();
		$this->load->model('activity_approval_model');
    
    if($this->session->userdata('logged_in'))
		{
      $session_data = $this->session->userdata('logged_in');
      $this->data['current_user_id'] = $session_data['id'];
      $this->data['current_username'] = $session_data['username'];
      $this->data['current_name'] = $session_data['name'];
      $this->data['current_role'] = $session_data['role'];
      $this->data['user_state_id'] = $session_data['state_id'];
      $this->data['user_district_id'] = $session_data['district_id'];
      $this->data['login_id'] = $session_data['login_id'];
      $this->data['current_group'] = $session_data['user_group'];

      $method = $this->router->fetch_method();

      if ($this->data['current_role'] == "field_user" and $method != 'rejected_activities')
      {
        //If no session, redirect to login page
        $this->session->set_flashdata('alert', "You are not authorized to access this section");
        redirect('dashboard', 'refresh');
      }
    }
    else
    {
      //If no session, redirect to login page
      redirect('login', 'refresh');
    }
      $this->load->model('states_model');
      $this->load->model('districts_model');
      $this->load->model('blocks_model');
      $this->load->model('clusters_model');
      $this->load->model('schools_model');
	}

  public function index(){
    $sparks = $this->activity_approval_model->get_sparks($this->data['current_user_id']);
    $this->data['activitystartdate'] = '';
    $this->data['activityenddate'] = '';
    $this->data['sparks'] = $sparks;
    $this->data['partial'] = 'activity_approval/index';
    $this->data['page_title'] = 'Activity approval';
    $this->load->view('templates/index', $this->data);
  }

  public function submit_activity_approval(){

      $this->load->model('No_visit_days_model');
      $this->load->model('school_visits_model');
      $this->load->model('Meetings_model');
      $this->load->model('Leaves_model');
      $this->load->model('Trainings_model');
      $this->load->model('Users_model');

      $user_id = $_POST['user_login_id'];

      if(isset($_POST['activitystartdate']) and isset($_POST['activityenddate'])){
        $start_month = $_POST['activitystartdate'];
        $_fromDate = date('Y-m-d', strtotime($start_month));
       
        $end_month = $_POST['activityenddate'];
        $_toDate = date('Y-m-d', strtotime($end_month));
      }
      else{
        redirect('activity_approval');
      }

      $this->data['spark_name'] = $this->Users_model->getById($user_id);
      $this->data['report_date'] = $_fromDate;
      $this->data['report_end_date'] = $_toDate;

      $activities = array();
      $date = $_fromDate;
      $i = 1;

      while($date <= $_toDate)
      {
        $activities[$date] = array();
        $date = date('Y-m-d', strtotime('+'.$i.' day', strtotime($_fromDate)));
        $i++;
      }

      $no_visit_days = $this->No_visit_days_model->get_approval_activities($user_id, $_fromDate, $_toDate, 'all');
      //echo "<br>".$this->No_visit_days_model->defaultdb->last_query();

      $school_visits = $this->school_visits_model->get_approval_activities($user_id, $_fromDate, $_toDate, 'all');
      //echo "<br>".$this->school_visits_model->defaultdb->last_query();

      $meetings = $this->Meetings_model->get_approval_activities($user_id, $_fromDate, $_toDate, 'all');
      //echo "<br>".$this->Meetings_model->defaultdb->last_query();

      $leaves = $this->Leaves_model->get_approval_activities($user_id, $_fromDate, '', 'all');
      //echo "<br>".$this->Leaves_model->defaultdb->last_query();

      $trainings = $this->Trainings_model->get_approval_activities($user_id, $_fromDate, $_toDate, 'all');
      //echo "<br>".$this->Trainings_model->defaultdb->last_query();

      
      $state_ids = array();
      $district_ids = array();
      $block_ids = array();
      $cluster_ids = array();
      $school_ids = array();
      //echo '<pre>';
      //echo '<br>NO VISIT DAYS';

      foreach($no_visit_days as $key=>$no_visit_day) { 

        //print_r($no_visit_day);
        $activity = array();
        $activity['activity_type'] = 'No Visit Day';
        $activity['type'] = 'no_visit_days';
        $activity['state'] = $no_visit_day->state_id;
        $activity['id'] = $no_visit_day->id;
        $activity['district'] = $no_visit_day->district_id;
        $activity['block'] = "";//$no_visit_day->block_id;
        $activity['cluster'] = "";//$no_visit_day->cluster_id;
        $activity['status'] = $no_visit_day->status;
        $activity['school'] = 0;
        $activity['details'] = '<strong>Reason : </strong>'.$no_visit_day->no_visit_reason;
        
        array_push($activities[date("Y-m-d",strtotime($no_visit_day->activity_date))],$activity);
        array_push($state_ids,$no_visit_day->state_id);
        array_push($district_ids,$no_visit_day->district_id);
        array_push($block_ids,"");
        array_push($cluster_ids,"");
      }

      //echo '<br>SCHOOL VISIT';

      foreach($school_visits as $key=>$school_visit) {

        //print_r($school_visit);

        $activity = array();
        $activity['activity_type'] = 'School Visit';
        $activity['type'] = 'school_visits';
        $activity['state'] = $school_visit->state_id;
        $activity['id'] = $school_visit->id;
        $activity['district'] = $school_visit->district_id;
        $activity['block'] = $school_visit->block_id;
        $activity['cluster'] = $school_visit->cluster_id;
        $activity['school'] = $school_visit->school_id;
        $activity['status'] = $school_visit->status;
        $activity['details'] = '<strong>Duration : </strong>'.$school_visit->duration;

        array_push($activities[date("Y-m-d",strtotime($school_visit->activity_date))],$activity);
        array_push($state_ids,$school_visit->state_id);
        array_push($district_ids,$school_visit->district_id);
        array_push($block_ids,$school_visit->block_id);
        array_push($cluster_ids,$school_visit->cluster_id);
        array_push($school_ids,$school_visit->school_id);
      }

      //echo '<br>MEETINGS';

      foreach($meetings as $key=>$meeting) { 

        //print_r($meeting);
        $activity = array();
        $activity['activity_type'] = 'Meeting';
        $activity['type'] = 'meetings';
        $activity['state'] = $meeting->state_id;
        $activity['id'] = $meeting->id;
        $activity['district'] = $meeting->district_id;
        $activity['block'] = $meeting->block_id;
        $activity['cluster'] = $meeting->cluster_id;
        $activity['status'] = $meeting->status;
        $activity['school'] = 0;

        $activity['details'] = '<strong>Duration : </strong>'.$meeting->duration;
        $activity['details'].= '<br><strong>Time : </strong>'.date("H:i",strtotime($meeting->activity_date));
        $activity['details'].= '<br><strong>No Of Invites : </strong>'.$meeting->no_of_invites;
        $activity['details'].= '<br><strong>No Of Attendees : </strong>'.$meeting->no_of_attendees;

        array_push($activities[date("Y-m-d",strtotime($meeting->activity_date))],$activity);
        array_push($state_ids,$meeting->state_id);
        array_push($district_ids,$meeting->district_id);
        array_push($block_ids,$meeting->block_id);
        array_push($cluster_ids,$meeting->cluster_id);
      }

      //echo 'LEAVES';

      foreach($leaves as $key=>$leave) { 

        $activity = array();
        $activity['activity_type'] = 'Leave';
        $activity['type'] = 'leaves';
        $activity['state'] = $leave->state_id;
        $activity['id'] = $leave->id;
        $activity['district'] = $leave->district_id;
        $activity['status'] = $leave->status;
        $activity['block'] = 0;
        $activity['cluster'] = 0;
        $activity['school'] = 0;
        $activity['details'] = '<strong>Leave Type : </strong>'.$leave->leave_type.'<br><strong>Leave From : </strong>'.date("d-m-Y",strtotime($leave->leave_from_date)).'<br><strong>Leave Till : </strong>'.date("d-m-Y",strtotime($leave->leave_end_date)).'<br><strong>No Of Days : </strong>'.$leave->total_days;

        array_push($activities[date("Y-m-d",strtotime($leave->leave_from_date))],$activity);      
        array_push($state_ids,$leave->state_id);
        array_push($district_ids,$leave->district_id);

      }

      //echo 'TRAININGS';

      foreach($trainings as $key=>$training) { 

        //print_r($training);
        $activity = array();
        $activity['activity_type'] = 'Training';
        $activity['type'] = 'trainings';
        $activity['state'] = $training->state_id;
        $activity['id'] = $training->id;
        $activity['district'] = $training->district_id;
        $activity['block'] = $training->block_id;
        $activity['cluster'] = $training->cluster_id;
        $activity['status'] = $training->status;
        $activity['school'] = 0;
        $activity['details'] = '<strong>Duration : </strong>'.$training->training_duration;
        $activity['details'].= '<br><strong>Time : </strong>'.date("H:i",strtotime($training->activity_date));
        $activity['details'].= '<br><strong>No Of Teachers Trained : </strong>'.$training->no_of_teachers_trained;

        array_push($activities[date("Y-m-d",strtotime($training->activity_date))],$activity);
        array_push($state_ids,$training->state_id);
        array_push($district_ids,$training->district_id);
        array_push($block_ids,$training->block_id);
        array_push($cluster_ids,$training->cluster_id);
      }
      //print_r($activities);
      //exit;
     

      $state_names = array();
      $state_names[0] = '';
      if (count($state_ids) > 0)
      {
        $states = $this->states_model->get_by_ids($state_ids);
        foreach($states as $state)
        {
          $state_names[$state->id] = $state->name;
        }
      }

      $district_names = array();
      $district_names[0] = '';
      if (count($district_ids) > 0)
      {
        $districts = $this->districts_model->getByIds($district_ids);
        foreach($districts as $district)
        {
          $district_names[$district->id] = $district->name;
        }
      }

      $block_names = array();
      $block_names[0] = '';
      if (count($block_ids) > 0)
      {
        $blocks = $this->blocks_model->getByIds($block_ids);
        $block_names = array();
        $block_names[0] = '';
        foreach($blocks as $block)
        {
          $block_names[$block->id] = $block->name;
        }
      }

      $cluster_names = array();
      $cluster_names[0] = '';
      if (count($cluster_ids) > 0)
      {
        $clusters = $this->clusters_model->getByIds($cluster_ids);
        foreach($clusters as $cluster)
        {
          $cluster_names[$cluster->id] = $cluster->name;
        }
      }

      $school_names = array();
      $school_names[0] = '';
      if (count($school_ids) > 0)
      {
        $schools = $this->schools_model->getByIds($school_ids); 
        foreach($schools as $school)
        {
          $school_names[$school->id] = $school->name." (".$school->dise_code.")";
        }
      }

      $this->data['school_names'] = $school_names;
      $this->data['cluster_names'] = $cluster_names;
      $this->data['block_names'] = $block_names;
      $this->data['district_names'] = $district_names;
      $this->data['state_names'] = $state_names;
      $this->data['activities'] = $activities;
      $this->data['post_data'] = $_POST;
      $this->data['pdf'] = 0;
      $this->data['excel'] = 0;

      /*if(isset($_POST['download_type']) and $_POST['download_type'] == 'pdf'){
        $this->data['pdf'] = 1;
        $summary = $this->load->view('reports/spark_daily_activity_report',$this->data,true);
        $this->common_functions->create_pdf($summary,'spark_daily_activity_report');
      }
      elseif(isset($_POST['download_type']) and $_POST['download_type'] == 'excel'){
          $this->data['excel'] = 1;
          $myFile = "./uploads/spark_daily_activity_report.xls";
          $this->load->library('parser');

          //pass retrieved data into template and return as a string
          $stringData = $this->parser->parse('reports/spark_daily_activity_report', $this->data, true);

          //open excel and write string into excel
          $fh = fopen($myFile, 'w') or die("can't open file");
          fwrite($fh, $stringData);

          fclose($fh);
          //download excel file
          $this->downloadExcelNew("spark_daily_activity_report");
          unlink($myFile);
      }
      else{*/
        $this->data['partial'] = 'activity_approval/detail';
        $this->load->view('templates/index', $this->data);
      //}
  }

  public function approve_activity($activity_type, $id){
    $res = $this->activity_approval_model->approve_activity($activity_type, $id, $this->data['current_user_id']);
    echo $res;
  }

  public function approve_multiple_activities(){
    for($i = 0; $i < count($_GET['ids']); $i++){
      $res = $this->activity_approval_model->approve_activity($_GET['types'][$i], $_GET['ids'][$i]);
    }
    echo json_encode(array("status"=>"done","ids"=>$_GET['ids']));
    //echo "done";
  }

  public function reject_activity(){
		
		if($_POST['activity_type'] == 'leaves')
		{
			$activity_id = $_POST['activity_id'];
			
			$this->load->model('Leaves_model');
			$leaveData = $this->Leaves_model->getById($activity_id);
			
			$user_id = $leaveData['user_id'];
			$leave_type = $leaveData['leave_type'];
			$total_days = $leaveData['total_days'];
			
			$creditData = $this->Leaves_model->get_leave_credits($user_id, $leave_type);
			if(!empty($creditData)){
				$mdata['leave_taken'] = $creditData[0]->leave_taken-$total_days;  
				$this->Leaves_model->update_leave_credit($mdata, $creditData[0]->id);    
			}
		}
    $res = $this->activity_approval_model->reject_activity($_POST['activity_type'], $_POST['activity_id'], $_POST['reason'],$this->data['current_user_id']);
    echo $res;
  }

  public function rejected_activities(){
    $acts = $this->activity_approval_model->get_rejected_activities($this->data['current_user_id']);
    //print_r($acts);
    $activities = array();
    foreach ($acts as $act) {
      $ad = (array)json_decode($act->additional_details);
      if($act->activity_type == 'meetings')
        $ad['activity_type'] = "Meeting";
      if($act->activity_type == 'school_visits')
        $ad['activity_type'] = "School Visit";
      if($act->activity_type == 'no_visit_days')
        $ad['activity_type'] = "No Visit Day";
      if($act->activity_type == 'trainings')
        $ad['activity_type'] = "Training";
      if($act->activity_type == 'leaves')
        $ad['activity_type'] = "Leave";
      $ad['reason'] = $act->reason;
      $ad['rejected_on'] = $act->rejected_on;
      $ad['activity_id'] = $act->id;
      array_push($activities, $ad);
    }
    //echo "<pre>";print_r($activities);exit;
    $this->data['activities'] = $activities;
    $this->data['partial'] = 'activity_approval/rejected_activities';
    $this->load->view('templates/index', $this->data);
  }
  
}
