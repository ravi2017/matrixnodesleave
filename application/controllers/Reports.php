<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Reports extends CI_Controller {

	function __construct()	
	{
		parent::__construct();
		$this->load->helper('url');
		$this->data['current_page'] = 'training';
		$this->load->model('trainings_model');
		$this->load->model('Users_model');
		$this->load->model('states_model');
		$this->load->library('form_validation');
		$this->load->library('Common_functions');
		$this->load->helper('date');
		if($this->session->userdata('logged_in'))
		{
		  $session_data = $this->session->userdata('logged_in');
		  $this->data['current_user_id'] = $session_data['id'];
		  $this->data['current_username'] = $session_data['username'];
		  $this->data['current_name'] = $session_data['name'];
		  $this->data['current_role'] = $session_data['role'];
		  $this->data['user_state_id'] = $session_data['state_id'];
		  $this->data['user_district_id'] = $session_data['district_id'];  
      
      $this->data['spark_id'] = $session_data['id'];
      $this->data['spark_state_id'] = $session_data['state_id'];
      $this->data['current_group'] = $session_data['user_group'];

		  /*if($session_data['role'] == 'field_user'){
			$this->session->set_flashdata('alert', "You are not authorized to access this section");
			redirect('dashboard', 'refresh');
		  }*/
		}
		else
		{
		  //If no session, redirect to login page
		  redirect('login', 'refresh');
		}
    $this->data['SSS_APP_CALLING_STATE'] = unserialize(SSS_APP_CALLING_STATE);
    $this->data['month_arr'] = unserialize(MONTH_ARRAY);
    $this->data['year_arr'] = unserialize(YEAR_ARRAY);
    $new_year_arr = unserialize(YEAR_ARRAY);
//    array_shift($new_year_arr);
//    array_shift($new_year_arr);
    $this->data['new_year_arr'] = $new_year_arr;
	}
  
	function index()
	{
		$this->data['partial'] = 'reports/index';
		$this->data['page_title'] = 'Reports';
		$this->load->view('templates/index', $this->data);    
	}
    
  function state()
  {
    if($this->data['current_role']=="field_user")
		{
			$district_id=$this->data['user_district_id'];
			$user_id=$this->data['current_user_id'];
			$state_id=$this->data['user_state_id'];
		}
		if($this->data['current_role']=="state_person")
		{
			$state_id=$this->data['user_state_id'];
			if(isset($_GET['user_id']) and $_GET['user_id'] >0 )
			{
				$user_id=$_GET['user_id'];
				
			}
			if(isset($_GET['district_id']) and $_GET['district_id'] >0 )
			{
				$district_id=$_GET['district_id'];
			}else{
					$district_id=0;
			}
		}
		
    
			$month = 0;
			$year = 0;
      $this->data['year'] = $month;
      $this->data['month'] = $year;
      $this->data['partial'] = 'reports/state';
		
    if($this->data['current_role']=="state_person" || $this->data['current_role']=="manager"){
      $states = $this->states_model->get_by_id($this->data['user_state_id']);
      $this->data['states'] = $states;
      
      $this->load->model('districts_model');
      $districts = $this->districts_model->get_state_districts($this->data['user_state_id']);
      $this->data['districts'] = $districts;
    }
    else{
				
				$states = $this->states_model->get_all();
				$this->data['states'] = $states;
    }
    $this->data['title'] = 'State Productivity Report';
    $this->data['partial'] = 'reports/state';
    $this->load->view('templates/index', $this->data);
  }
  
  function stateProductivityReport()
  {
    
    if(!isset($_POST['monthno']) && !isset($_POST['year']))
    {
        redirect('reports/state');
    }
    $state_id = $this->input->post('state_id');
    $inpmonth = $this->input->post('monthno');
    $inpyear = $this->input->post('year');
    $inpdate			 = '01/'.$inpmonth.'/'.$inpyear;
    $inpdate 		 = date('t/m/Y', strtotime($inpyear.'-'.$inpmonth.'-01'));

    $this->load->model('states_model');
    $this->load->model('No_visit_days_model');
    $this->load->model('school_visits_model');
    $this->load->model('Meetings_model');
    $this->load->model('Leaves_model');
    $this->load->model('Trainings_model');
    $this->load->model('Users_model');
    $this->load->model('Holidays_model');
    $this->load->model('districts_model');
    $this->load->model('Claim_model');
    $this->load->model('Sparks_model');

    $state_data = $this->states_model->getById($state_id);

    $start_date = date('Y-m-d', strtotime($inpyear.'-'.$inpmonth.'-01'));
    $start_date1 = date('Y-m-d', strtotime($inpyear.'-'.($inpmonth-2).'-01'));
    $end_date = date('Y-m-t', strtotime($inpyear.'-'.$inpmonth.'-01'));
    if(strtotime($end_date) > now()){
      $end_date = date('Y-m-d');
    }

    $users_data = $this->Users_model->getSparkByStateDuration($state_id, $start_date1, $end_date);
    //print_r($users_data);

    $current_month = (Int) date("m", strtotime(date("Y-m-d")));
    $current_year = (Int) date("Y", strtotime(date("Y-m-d")));
    $month_name = "";
    $prev_month2_name   = "";
    $prev_month1_name   = "";
    $cm_no 		= 0;
    $pv2_no 	= 0;
    $pv1_no 	= 0;
    $current_year2 = 0;
    $current_year1 = 0;

    switch($inpmonth){

    case "1":
      $month_name = date('F', mktime(0, 0, 0, $inpmonth, 10));
      $cm_no      = $inpmonth;
      $prev_month2_name   = date('F', mktime(0, 0, 0, 12, 10));
      $pv2_no = 12;
      $current_year2 = $inpyear - 1;
      $prev_month1_name   = date('F', mktime(0, 0, 0, 12 - $inpmonth, 10));
      $pv1_no = 12 - $inpmonth;
      $current_year1 = $inpyear - 1;
      break;
    case "2":
      $month_name = date('F', mktime(0, 0, 0, $inpmonth, 10));
      $cm_no      = $inpmonth;
      $prev_month2_name   = date('F', mktime(0, 0, 0, $inpmonth-1, 10));
      $pv2_no = $inpmonth-1;
      $current_year2 = $inpyear;
      $prev_month1_name   = date('F', mktime(0, 0, 0, 12, 10));
      $pv1_no = 12;
      $current_year1 = $inpyear - 1;
      break;
    default:
      $month_name   = date('F', mktime(0, 0, 0, $inpmonth, 10));
      $cm_no      = $inpmonth;
      $prev_month2_name   = date('F', mktime(0, 0, 0, $inpmonth-1, 10));
      $pv2_no = $inpmonth-1;
      $current_year2 = $inpyear;
      $prev_month1_name   = date('F', mktime(0, 0, 0, $inpmonth-2, 10));
      $pv1_no = $inpmonth-2;
      $current_year1 = $inpyear;
    }
    
    $session_start_year = $inpyear;
    if($inpmonth < SESSION_START_MONTH)
    {
      $session_start_year = $inpyear-1;
    }

    /*$_fromDate1 = date("Y-m-d",strtotime($current_year1."-".$pv1_no."-01"));
    $_toDate1 = date("Y-m-t",strtotime($current_year1."-".$pv1_no."-01"));
    $productive_days_1 = date("t",strtotime($current_year1."-".$pv1_no."-01"));
    //echo"<br>".$_fromDate1." >> ".$_toDate1." >> ".$productive_days_1;
    
    $_fromDate2 = date("Y-m-d",strtotime($current_year2."-".$pv2_no."-01"));
    $_toDate2 = date("Y-m-t",strtotime($current_year2."-".$pv2_no."-01"));
    $productive_days_2 = date("t",strtotime($current_year2."-".$pv2_no."-01"));
    
    $_fromDate_C = date("Y-m-d",strtotime($inpyear."-".$inpmonth."-01"));
    $_toDate_C = date("Y-m-t",strtotime($inpyear."-".$inpmonth."-01"));
    $productive_days_3 = date("t",strtotime($inpyear."-".$inpmonth."-01"));
    if(strtotime($_toDate_C) > now()){
      $_toDate_C = date("Y-m-d");
      $productive_days_3 = date('d');
    }*/
    
    $_fromDate_yt = date("Y-m-d 00:00:00",strtotime($session_start_year."-".SESSION_START_MONTH."-01"));
    $_fromDate_ytd = new DateTime($_fromDate_yt);
    $_toDate_yt = date("Y-m-t 23:59:59",strtotime($inpyear."-".$inpmonth."-01"));
    $_toDate_ytd = new DateTime($_toDate_yt);
    $interval = $_fromDate_ytd->diff($_toDate_ytd);

    $i = 0;
    $user_ids = array();
    $user_names = array();
    $from_date = array();
    $to_date = array();
    $productive_days_yt = array();
    $months_list = array();
    foreach($users_data as $usersdata){
        $userid = $usersdata['user_id'];
        $user_ids[] = $userid;
        $user_names[$userid]['id'] = $usersdata['user_id'];
        $user_names[$userid]['name'] = $usersdata['name'];
        $user_names[$userid]['login_id'] = $usersdata['login_id'];
        $user_names[$userid]['exit_date'] = $usersdata['exit_date'];
        $user_names[$userid]['district'] = ""; //$user_districts[0]->name;
        $user_names[$userid]['district_ids'] = array(); //$user_districts[0]->name;
        $i++;

        $u_start_date = $usersdata['start_date'];
        $u_end_date = $usersdata['end_date'];
        $exit_date = $usersdata['exit_date'];

        $working_period = array();
        if(!empty($u_end_date) && strtotime($u_end_date) < strtotime($_toDate_yt)){
          $to_date1 = $u_end_date;
          $working_period[] = "<strong>Last Date:</strong>$u_end_date";
        }
        else{
          $to_date1 =   date('Y-m-d', strtotime($_toDate_yt));
        }
        if($u_start_date > $_fromDate_yt){
          $from_date1 = $u_start_date;
          $working_period[] = "<strong>Start Date:</strong>$u_start_date";
        }
        else{
          $from_date1 = date('Y-m-d', strtotime($_fromDate_yt));
        }
        
        if(!empty($exit_date) && strtotime($exit_date) < strtotime($to_date1)){
					$to_date1 = $exit_date;
				}
        
        if(!empty($working_period))
          $user_names[$userid]['working_period'] = "(".implode(', ',$working_period).")";
        $from_date[$userid] = date('Y-m-d', strtotime($from_date1));
        $to_date[$userid]  = $to_date1;
        $days_diff = (strtotime($to_date1)- strtotime($from_date1))/(24*60*60)+1;
        $months_values = $this->return_months($from_date1, $to_date1);
        $months_list[$userid] = $months_values;
        $productive_days_yt[$userid] = $days_diff;
    }
    //print_r($months_list);
    //echo "<br>".implode(', ', $user_ids);
    
    $visitproductivity_1 = array();    $visitproductivity_2 = array();    $visitproductivity_3 = array();    $visitproductivity_yt = array();
    $productivedays_1 = array();    $productivedays_2 = array();    $productivedays_3 = array();    $productivedays_yt = array();
    $school_visit_days_1 = array();    $school_visit_days_2 = array();    $school_visit_days_3 = array();    $school_visit_days_yt = array();
    
    $visit_1 = 0;    $visit_2 =0;    $visit_3 = 0;    $visit_ytd = 0;
    $avg_visit_prod_month_1 = 0;    $avg_visit_prod_month_2 = 0;    $avg_visit_prod_month_3 = 0;    $avg_visit_prod_month_yt = 0;
    $school_visit_month_1 = 0;    $school_visit_month_2 = 0;    $school_visit_month_3 = 0;    $school_visit_month_yt = 0;

    $count_users_1 = 0;
    $count_users_2 = 0;
    $count_users_3 = 0;
    
    foreach($user_ids as $login_id){
      
      $schedule_exit_date = $user_names[$login_id]['exit_date'];
      $_fromDate1 = date("Y-m-d",strtotime($current_year1."-".$pv1_no."-01"));
      $_toDate1 = date("Y-m-t",strtotime($current_year1."-".$pv1_no."-01"));
      if(!empty($schedule_exit_date) && strtotime($schedule_exit_date) < strtotime($_toDate1) && strtotime($schedule_exit_date) > strtotime($_fromDate1)){
				$_toDate1 = $schedule_exit_date;
			}
      $productive_days_1 = date("t",strtotime($_fromDate1));
      //echo"<br>".$_fromDate1." >> ".$_toDate1." >> ".$productive_days_1;
      
      $_fromDate2 = date("Y-m-d",strtotime($current_year2."-".$pv2_no."-01"));
      $_toDate2 = date("Y-m-t",strtotime($current_year2."-".$pv2_no."-01"));
      if(!empty($schedule_exit_date) && strtotime($schedule_exit_date) < strtotime($_toDate2) && strtotime($schedule_exit_date) > strtotime($_fromDate2)){
				$_toDate2 = $schedule_exit_date;
			}
      $productive_days_2 = date("t",strtotime($_fromDate2));
      
      $_fromDate_C = date("Y-m-d",strtotime($inpyear."-".$inpmonth."-01"));
      $_toDate_C = date("Y-m-t",strtotime($inpyear."-".$inpmonth."-01"));
      if(!empty($schedule_exit_date) && strtotime($schedule_exit_date) < strtotime($_toDate_C) && strtotime($schedule_exit_date) > strtotime($_fromDate_C)){
				$_toDate_C = $schedule_exit_date;
			}
      $productive_days_3 = date("t",strtotime($_fromDate_C));
      if(strtotime($_toDate_C) > now()){
        $_toDate_C = date("Y-m-d");
        $productive_days_3 = date('d');
      }

      $user_blocks = $this->Users_model->user_blocks($login_id);
      $user_block_ids = array();
      foreach($user_blocks as $user_block)
      {
        array_push($user_block_ids,$user_block->id);
      }
      //print_r($user_block_ids);
      $user_district_ids = array();
      //$user_districts = $this->Users_model->get_worked_districts($login_id, $state_id, $start_date1, $end_date);
      $user_districts = $this->Sparks_model->get_member_district_ids($login_id);
      foreach($user_districts as $user_district)
      {
        //array_push($user_district_ids,$user_district['district_id']);
        array_push($user_district_ids,$user_district->district_id);
      }
      $remove_val = array('0');
      $user_district_ids = array_diff($user_district_ids, $remove_val);
      if (count($user_district_ids) > 0)
      {
        $district_names = $this->districts_model->check_district_name_by_State($user_district_ids,$state_id);
        $user_names[$login_id]['district'] = $district_names[0]['districts'];
      }
      
      // Array to store distinct activity dates including state holidays & SAT-SUN offdays
      //$spark_meet_with = array('CS', 'Secretary', 'Director', 'SPD', 'Quality head/APD', 'DM', 'DM&C', 'DEO', 'DMC/DEO/BSA/CEO', 'BRC/BRP/BEO');
      $spark_meet_with = unserialize(MEET_WITH_FUSER);
      
      $activity_dates = array(array(),array(),array(),array()); 
      $internal_meeting_dates = array(array(),array(),array(),array()); 
    
      $refresher_trainings = array(0,0,0,0,0);
      $refresher_trainings = array(0,0,0,0,0);
      $annual_trainings = array(0,0,0,0,0);
        
      $no_visit_days = array(0,0,0,0);
      $auth_dec_holidays = array(0,0,0,0);
      $leaves = array(0,0,0,0,0);
      $home_district_trainings = array(0,0,0,0);
      $outstation_trainings = array(0,0,0,0);
      $district_level_meetings = array(0,0,0,0);
      $district_level_distinct_meetings = array(0,0,0,0);
      $block_level_meetings = array(0,0,0,0);
      $block_level_distinct_meetings = array(0,0,0,0);
      $internal_meetings = array(0,0,0,0);
      
      //Fetch user group
			$sparkData = $this->Users_model->getById($login_id);
			$current_group = $sparkData['user_group'];
	  
	  /*************************************************************************/	
	  if(in_array(date('Y-m',strtotime($_fromDate1)), $months_list[$login_id])){	
      //echo"<br>cond 1 : ".$login_id." >> ".$_fromDate1." >> ".$_toDate1;
      
      if(strtotime($from_date[$login_id]) > strtotime($_fromDate1)){
        $_fromDate1 = $from_date[$login_id];
      } 
      if(strtotime($to_date[$login_id]) < strtotime($_toDate1)){
        $_toDate1 = $to_date[$login_id];
      } 
      $productive_days_1 = $days_diff = (strtotime($_toDate1)- strtotime($_fromDate1))/(24*60*60)+1;
      
      //echo"<br>cond 1 : ".$login_id." >> ".$_fromDate1." >> ".$_toDate1;
      
		  $no_visit_1 = $this->No_visit_days_model->get_all($login_id, $_fromDate1, $_toDate1);
		  foreach($no_visit_1 as $no_visit)
		  {
			if ($no_visit->no_visit_reason == 'Local Authority Declared Holiday')
			  $auth_dec_holidays[0] = $auth_dec_holidays[0] + 1;
			else
			  $no_visit_days[0] = $no_visit_days[0] + 1;
		  }
		  
      $state_holiday_list_1 = $this->Holidays_model->get_state_holidays_list($state_id, $_fromDate1, $_toDate1, $current_group);
      foreach($state_holiday_list_1 as $stateholiday)
      {
        if(!in_array(date('Y-m-d', strtotime($stateholiday->activity_date)), $activity_dates[0])){
          $activity_dates[0][] = date('Y-m-d', strtotime($stateholiday->activity_date));
        }
      }
      $sun_2_3_list_sat_1 = $this->Holidays_model->get_2_3_saturday_list($_fromDate1, $_toDate1, $current_group);
      foreach($sun_2_3_list_sat_1 as $holiday)
      {
        if(!in_array(date('Y-m-d', strtotime($holiday->activity_date)), $activity_dates[0])){
          $activity_dates[0][] = date('Y-m-d', strtotime($holiday->activity_date));
        }
      }
      
      $leaves_arr_1 = $this->Leaves_model->get_all_leave($login_id, $_fromDate1, $_toDate1);
      $leaves_data_1 = $this->common_functions->return_month_leave_distinct($leaves_arr_1, $_fromDate1, $_toDate1, $state_holiday_list_1, $sun_2_3_list_sat_1);
      $leaves[0] = count($leaves_data_1);
		  
		  $trainings_1 = $this->Trainings_model->get_all($login_id, $_fromDate1, $_toDate1);
		  foreach($trainings_1 as $training)
		  {
        if(!in_array(date('Y-m-d', strtotime($training->activity_date)), $activity_dates[0])){
          $activity_dates[0][] = date('Y-m-d', strtotime($training->activity_date));
        }

        //if ($training->block_id > 0 and in_array($training->block_id,$user_block_ids))
        if ($training->mode == 'internal')
          $home_district_trainings[0] = $home_district_trainings[0] + 1;
        else
          $outstation_trainings[0] = $outstation_trainings[0] + 1;
        
        if ($training->training_type == 'refresher')
          $refresher_trainings[0] = $refresher_trainings[0] + 1;
        else if ($training->training_type == 'annual')
          $annual_trainings[0] = $annual_trainings[0] + 1;    
		  }
		  
      $school_visit_count_1 = 0;
      $school_visits_1 = $this->school_visits_model->get_all($login_id, $_fromDate1, $_toDate1);
      foreach($school_visits_1 as $school_visit)
      {
        $school_visit_count_1++;
        if(!in_array(date('Y-m-d', strtotime($school_visit->activity_date)), $activity_dates[0])){
          $activity_dates[0][] = date('Y-m-d', strtotime($school_visit->activity_date));
        }
      }
      
      // MEETINGS STARTED
      $meeting_blocks = array();
		  $meetings_1 = $this->Meetings_model->get_all($login_id, $_fromDate1, $_toDate1);
		  foreach($meetings_1 as $meeting)
		  {
        if(!in_array(date('Y-m-d', strtotime($meeting->activity_date)), $activity_dates[0])){
          $activity_dates[0][] = date('Y-m-d', strtotime($meeting->activity_date));
        }
        
        $meeting_date = date('Y-m-d', strtotime($meeting->activity_date));
        if (!array_key_exists($meeting_date,$meeting_blocks))
        {
          $meeting_blocks[$meeting_date] = array();
        }
        if ($meeting->block_id == 0 and $meeting->is_internal == false){
          $district_level_meetings[0] = $district_level_meetings[0] + 1;
          if (in_array($meeting->meet_with,$spark_meet_with))
              $district_level_distinct_meetings[0]++;
				}
        else if ($meeting->block_id > 0 and $meeting->is_internal == false){
          $block_level_meetings[0] = $block_level_meetings[0] + 1;
          if (!in_array($meeting->block_id,$meeting_blocks[$meeting_date])) {
            if (in_array($meeting->meet_with,$spark_meet_with)) {
              array_push($meeting_blocks[$meeting_date],$meeting->block_id);
              $block_level_distinct_meetings[0]++;
            }
          }
        }
		  }
      
      foreach($meetings_1 as $meeting)
		  {
        if($meeting->is_internal == true){
          $internal_meetings[0] = $internal_meetings[0] + 1;
          if(!in_array(date('Y-m-d', strtotime($meeting->activity_date)), $activity_dates[0]) && !in_array(date('Y-m-d', strtotime($meeting->activity_date)), $internal_meeting_dates[0])){
            $internal_meeting_dates[0][] = date('Y-m-d', strtotime($meeting->activity_date));
          }
        }
      }
        
		  
      $LD_activities_date_1 = array();
      $LD_activities_1 = $this->Claim_model->get_distince_ld_activities($login_id, $_fromDate1, $_toDate1);
      foreach($LD_activities_1 as $LD_activitie)
      {
        if(!in_array($LD_activitie->dat, $activity_dates[0])){
          $LD_activities_date_1[] = $LD_activitie->dat;
        }
      }
      $distinct_LD_count_1 = count($LD_activities_date_1);
      
		  $state_holiday_1 = count($state_holiday_list_1);
		  $sun_2_3_sat_1 = count($sun_2_3_list_sat_1);
		  //$he_visit_count_1 = $this->school_visits_model->get_HE_visit_count($login_id, $state_id, $_fromDate1, $_toDate1);
		  //$le_visit_count_1 = $this->school_visits_model->get_LE_visit_count($login_id, $state_id, $_fromDate1, $_toDate1);
			
			$p_days_1 = $productive_days_1 - ($state_holiday_1 + $leaves[0] + $sun_2_3_sat_1 + $auth_dec_holidays[0]+ count($internal_meeting_dates[0]) + $distinct_LD_count_1);
		  $visitproductivity_1[$login_id] = ($p_days_1 > 0 ? round((($school_visit_count_1) + ($refresher_trainings[0] * 2) + ($annual_trainings[0] * 2) + ($district_level_distinct_meetings[0] + $block_level_distinct_meetings[0])) / ($p_days_1), 1) : 0);
		  $avg_visit_prod_month_1 = $avg_visit_prod_month_1 + $visitproductivity_1[$login_id];

		  //$school_visit_days_1[$login_id] = $he_visit_count_1+$le_visit_count_1;
		  $school_visit_days_1[$login_id] = $school_visit_count_1;
		  $school_visit_month_1 = $school_visit_month_1 + $school_visit_days_1[$login_id];
		  
		  $count_users_1++;
	  }
	  else{
		  $visitproductivity_1[$login_id] = 0;
		  $school_visit_days_1[$login_id] = 0;
	  }
      /*************************************************************************/
    if(in_array(date('Y-m',strtotime($_fromDate2)), $months_list[$login_id])){			
      //echo"<br>cond 2 : ".$login_id." >> ".$_fromDate2." >> ".$_toDate2;
      
      if(strtotime($from_date[$login_id]) > strtotime($_fromDate2)){
        $_fromDate2 = $from_date[$login_id];
      } 
      if(strtotime($to_date[$login_id]) < strtotime($_toDate2)){
        $_toDate2 = $to_date[$login_id];
      } 
      $productive_days_2 = $days_diff = (strtotime($_toDate2)- strtotime($_fromDate2))/(24*60*60)+1;
      
      //echo"<br>cond 2 : ".$login_id." >> ".$_fromDate2." >> ".$_toDate2;
      
      
		  $no_visit_2 = $this->No_visit_days_model->get_all($login_id, $_fromDate2, $_toDate2);
		  foreach($no_visit_2 as $no_visit)
		  {
        if ($no_visit->no_visit_reason == 'Local Authority Declared Holiday')
          $auth_dec_holidays[1] = $auth_dec_holidays[1] + 1;
        else
          $no_visit_days[1] = $no_visit_days[1] + 1;
		  }
      
      $state_holiday_list_2 = $this->Holidays_model->get_state_holidays_list($state_id, $_fromDate2, $_toDate2, $current_group);
      foreach($state_holiday_list_2 as $stateholiday)
      {
        if(!in_array(date('Y-m-d', strtotime($stateholiday->activity_date)), $activity_dates[1])){
          $activity_dates[1][] = date('Y-m-d', strtotime($stateholiday->activity_date));
        }
      }
      $sun_2_3_list_sat_2 = $this->Holidays_model->get_2_3_saturday_list($_fromDate2, $_toDate2, $current_group);
      foreach($sun_2_3_list_sat_2 as $holiday)
      {
        if(!in_array(date('Y-m-d', strtotime($holiday->activity_date)), $activity_dates[1])){
          $activity_dates[1][] = date('Y-m-d', strtotime($holiday->activity_date));
        }
      }
        
      $leaves_arr_2 = $this->Leaves_model->get_all_leave($login_id, $_fromDate2, $_toDate2);
      $leaves_data_2 = $this->common_functions->return_month_leave_distinct($leaves_arr_2, $_fromDate2, $_toDate2, $state_holiday_list_2, $sun_2_3_list_sat_2);
      $leaves[1] = count($leaves_data_2);

		  $trainings_2 = $this->Trainings_model->get_all($login_id, $_fromDate2, $_toDate2);
		  foreach($trainings_2 as $training)
		  {
        if(!in_array(date('Y-m-d', strtotime($training->activity_date)), $activity_dates[1])){
          $activity_dates[1][] = date('Y-m-d', strtotime($training->activity_date));
        }
        if ($training->mode == 'internal')
          $home_district_trainings[1] = $home_district_trainings[1] + 1;
        else
          $outstation_trainings[1] = $outstation_trainings[1] + 1;
        
        if ($training->training_type == 'refresher')
          $refresher_trainings[1] = $refresher_trainings[1] + 1;
        else if ($training->training_type == 'annual')
          $annual_trainings[1] = $annual_trainings[1] + 1;      
		  }
		  
      $school_visit_count_2 = 0;
      $school_visits_2 = $this->school_visits_model->get_all($login_id, $_fromDate2, $_toDate2);
      foreach($school_visits_2 as $school_visit)
      {
        $school_visit_count_2++;
        if(!in_array(date('Y-m-d', strtotime($school_visit->activity_date)), $activity_dates[1])){
          $activity_dates[1][] = date('Y-m-d', strtotime($school_visit->activity_date));
        }
      }
      
      // MEETINGS STARTED
      $meeting_blocks = array();
		  $meetings_2 = $this->Meetings_model->get_all($login_id, $_fromDate2, $_toDate2);
		  foreach($meetings_2 as $meeting)
		  {
        if(!in_array(date('Y-m-d', strtotime($meeting->activity_date)), $activity_dates[1])){
          $activity_dates[1][] = date('Y-m-d', strtotime($meeting->activity_date));
        }
        
        $meeting_date = date('Y-m-d', strtotime($meeting->activity_date));
        if (!array_key_exists($meeting_date,$meeting_blocks))
        {
          $meeting_blocks[$meeting_date] = array();
        }
        
        if ($meeting->block_id == 0 and $meeting->is_internal == false){
          $district_level_meetings[1] = $district_level_meetings[1] + 1;
          if (in_array($meeting->meet_with,$spark_meet_with))
              $district_level_distinct_meetings[1]++;
				}
        else if ($meeting->block_id > 0 and $meeting->is_internal == false){
          $block_level_meetings[1] = $block_level_meetings[1] + 1;
          if (!in_array($meeting->block_id,$meeting_blocks[$meeting_date])) {
            if (in_array($meeting->meet_with,$spark_meet_with)) {
              array_push($meeting_blocks[$meeting_date],$meeting->block_id);
              $block_level_distinct_meetings[1]++;
            }
          }
        }
		  }
      
      foreach($meetings_2 as $meeting)
		  {
        if ($meeting->is_internal == true){
          $internal_meetings[1] = $internal_meetings[1] + 1;
          if(!in_array(date('Y-m-d', strtotime($meeting->activity_date)), $activity_dates[1]) && !in_array(date('Y-m-d', strtotime($meeting->activity_date)), $internal_meeting_dates[1])){
            $internal_meeting_dates[1][] = date('Y-m-d', strtotime($meeting->activity_date));
          }
        }
      }
      
      
      $LD_activities_date_2 = array();
      $LD_activities_2 = $this->Claim_model->get_distince_ld_activities($login_id, $_fromDate2, $_toDate2);
      foreach($LD_activities_2 as $LD_activitie)
      {
        if(!in_array($LD_activitie->dat, $activity_dates[1])){
          $LD_activities_date_2[] = $LD_activitie->dat;
        }
      }
      $distinct_LD_count_2 = count($LD_activities_date_2);
      
      $state_holiday_2 = count($state_holiday_list_2);
		  $sun_2_3_sat_2 = count($sun_2_3_list_sat_2);
      //$he_visit_count_2 = $this->school_visits_model->get_HE_visit_count($login_id, $state_id, $_fromDate2, $_toDate2);
		  //$le_visit_count_2 = $this->school_visits_model->get_LE_visit_count($login_id, $state_id, $_fromDate2, $_toDate2);
		  
      //echo"<br>$login_id 1>> ".$he_visit_count_2." 2>> ".$le_visit_count_2."  3>> ".$refresher_trainings[1]."  4>> ".$annual_trainings[1]."  5>> ".$district_level_meetings[1]."  6>>  ".$block_level_meetings[1]."  7>>  ".$productive_days_2."  8>>  ".$state_holiday_2."  9>>  ".$leaves[1]."  10>>  ".$sun_2_3_sat_2."  11>> ".$auth_dec_holidays[1];
      $p_days_2 = ($productive_days_2 - ($state_holiday_2 + $leaves[1] + $sun_2_3_sat_2 + $auth_dec_holidays[1]+ count($internal_meeting_dates[1]) + $distinct_LD_count_2));
      $visitproductivity_2[$login_id] = ($p_days_2 > 0 ? round((($school_visit_count_2) + ($refresher_trainings[1] * 2) + ($annual_trainings[1] * 2) + ($district_level_distinct_meetings[1] + $block_level_distinct_meetings[1])) / ($p_days_2), 1) : 0);
		  $avg_visit_prod_month_2 = $avg_visit_prod_month_2 + $visitproductivity_2[$login_id];
		  $school_visit_days_2[$login_id] = $school_visit_count_2;
		  $school_visit_month_2 = $school_visit_month_2 + $school_visit_days_2[$login_id];
		  
		  $count_users_2++;
	  }
	  else{
		  $visitproductivity_2[$login_id] = 0;
		  $school_visit_days_2[$login_id] = 0;
	  }	
      
      /*************************************************************************/	
    if(in_array(date('Y-m',strtotime($_fromDate_C)), $months_list[$login_id])){			
      
      if(strtotime($from_date[$login_id]) > strtotime($_fromDate_C)){
        $_fromDate_C = $from_date[$login_id];
      } 
      if(strtotime($to_date[$login_id]) < strtotime($_toDate_C)){
        $_toDate_C = $to_date[$login_id];
      } 
      //echo"<br>cond 3 : ".$login_id." >> ".$_fromDate_C." >> ".$_toDate_C;
      
      $productive_days_3 = $days_diff = (strtotime($_toDate_C)- strtotime($_fromDate_C))/(24*60*60)+1;
      
		  $no_visit_3 = $this->No_visit_days_model->get_all($login_id, $_fromDate_C, $_toDate_C);
		  foreach($no_visit_3 as $no_visit)
		  {
			if ($no_visit->no_visit_reason == 'Local Authority Declared Holiday')
			  $auth_dec_holidays[2] = $auth_dec_holidays[2] + 1;
			else
			  $no_visit_days[2] = $no_visit_days[2] + 1;
		  }
      
      $state_holiday_list_3 = $this->Holidays_model->get_state_holidays_list($state_id, $_fromDate_C, $_toDate_C, $current_group);
      foreach($state_holiday_list_3 as $stateholiday)
      {
        if(!in_array(date('Y-m-d', strtotime($stateholiday->activity_date)), $activity_dates[2])){
          $activity_dates[2][] = date('Y-m-d', strtotime($stateholiday->activity_date));
        }
      }
      $sun_2_3_list_sat_3 = $this->Holidays_model->get_2_3_saturday_list($_fromDate_C, $_toDate_C, $current_group);
      foreach($sun_2_3_list_sat_3 as $holiday)
      {
        if(!in_array(date('Y-m-d', strtotime($holiday->activity_date)), $activity_dates[2])){
          $activity_dates[2][] = date('Y-m-d', strtotime($holiday->activity_date));
        }
      }
        
      $leaves_arr_3 = $this->Leaves_model->get_all_leave($login_id, $_fromDate_C, $_toDate_C);
      $leaves_data_3 = $this->common_functions->return_month_leave_distinct($leaves_arr_3, $_fromDate_C, $_toDate_C, $state_holiday_list_3, $sun_2_3_list_sat_3);
      $leaves[2] = count($leaves_data_3);

		  $trainings_3 = $this->Trainings_model->get_all($login_id, $_fromDate_C, $_toDate_C);
		  foreach($trainings_3 as $training)
		  {
        if(!in_array(date('Y-m-d', strtotime($training->activity_date)), $activity_dates[2])){
          $activity_dates[2][] = date('Y-m-d', strtotime($training->activity_date));
        }
        if ($training->mode == 'internal')
          $home_district_trainings[2] = $home_district_trainings[2] + 1;
        else
          $outstation_trainings[2] = $outstation_trainings[2] + 1;
          
        if ($training->training_type == 'refresher')
          $refresher_trainings[2] = $refresher_trainings[2] + 1;
        else if ($training->training_type == 'annual')
          $annual_trainings[2] = $annual_trainings[2] + 1;      
		  }
		  
      $school_visit_count_3 = 0;
      $school_visits_3 = $this->school_visits_model->get_all($login_id, $_fromDate_C, $_toDate_C);
      foreach($school_visits_3 as $school_visit)
      {
        $school_visit_count_3++;
        if(!in_array(date('Y-m-d', strtotime($school_visit->activity_date)), $activity_dates[2])){
          $activity_dates[2][] = date('Y-m-d', strtotime($school_visit->activity_date));
        }
      }
      
      // MEETINGS STARTED
      $meeting_blocks = array();
		  $meetings_3 = $this->Meetings_model->get_all($login_id, $_fromDate_C, $_toDate_C);
		  foreach($meetings_3 as $meeting)
		  {
        if(!in_array(date('Y-m-d', strtotime($meeting->activity_date)), $activity_dates[2])){
          $activity_dates[2][] = date('Y-m-d', strtotime($meeting->activity_date));
        }
        
        $meeting_date = date('Y-m-d', strtotime($meeting->activity_date));
        if (!array_key_exists($meeting_date,$meeting_blocks))
        {
          $meeting_blocks[$meeting_date] = array();
        }
        
        if ($meeting->block_id == 0 and $meeting->is_internal == false){
          $district_level_meetings[2] = $district_level_meetings[2] + 1;
          if (in_array($meeting->meet_with,$spark_meet_with))
              $district_level_distinct_meetings[2]++;
				}
        else if ($meeting->block_id > 0 and $meeting->is_internal == false){
          $block_level_meetings[2] = $block_level_meetings[2] + 1;
          if (!in_array($meeting->block_id,$meeting_blocks[$meeting_date])) {
            if (in_array($meeting->meet_with,$spark_meet_with)) {
              array_push($meeting_blocks[$meeting_date],$meeting->block_id);
              $block_level_distinct_meetings[2]++;
            }
          }
        }
		  }
      
		  foreach($meetings_3 as $meeting)
		  {
        if ($meeting->is_internal == true){
          $internal_meetings[2] = $internal_meetings[2] + 1;
          if(!in_array(date('Y-m-d', strtotime($meeting->activity_date)), $activity_dates[2]) && !in_array(date('Y-m-d', strtotime($meeting->activity_date)), $internal_meeting_dates[2])){
            $internal_meeting_dates[2][] = date('Y-m-d', strtotime($meeting->activity_date));
          }
        }
		  }
      
      
      $LD_activities_date_3 = array();
      $LD_activities_3 = $this->Claim_model->get_distince_ld_activities($login_id, $_fromDate_C, $_toDate_C);
      foreach($LD_activities_3 as $LD_activitie)
      {
        if(!in_array($LD_activitie->dat, $activity_dates[2])){
          $LD_activities_date_3[] = $LD_activitie->dat;
        }
      }
      $distinct_LD_count_3 = count($LD_activities_date_3);
      
      $state_holiday_3 = count($state_holiday_list_3);
		  $sun_2_3_sat_3 = count($sun_2_3_list_sat_3);
      
		  //$he_visit_count_3 = $this->school_visits_model->get_HE_visit_count($login_id, $state_id, $_fromDate_C, $_toDate_C);
		  //$le_visit_count_3 = $this->school_visits_model->get_LE_visit_count($login_id, $state_id, $_fromDate_C, $_toDate_C);
		  
      //echo"<br>$login_id : ".$school_visit_count_3." >>>rt ".($refresher_trainings[2] * 2)." >>>at ".($annual_trainings[2] * 2)." >>>dm ".$district_level_meetings[2]." >>>bm ".$block_level_distinct_meetings[2]." >pd ".$productive_days_3." >sh ".$state_holiday_3." >L ".$leaves[2]." >SS ".$sun_2_3_sat_3." >AH ".$auth_dec_holidays[2]." >IM ". count($internal_meeting_dates[2])." >DL ".$distinct_LD_count_3;
      //echo "<br>".$block_level_distinct_meetings[2];
      $p_days_3 = ($productive_days_3 - ($state_holiday_3 + $leaves[2] + $sun_2_3_sat_3 + $auth_dec_holidays[2]+ count($internal_meeting_dates[2]) + $distinct_LD_count_3));
      $visitproductivity_3[$login_id] = ($p_days_3 > 0 ? round((($school_visit_count_3) + ($refresher_trainings[2] * 2) + ($annual_trainings[2] * 2) + ($district_level_distinct_meetings[2] + $block_level_distinct_meetings[2])) / ($p_days_3), 1) : 0);
		  
      $avg_visit_prod_month_3 = $avg_visit_prod_month_3 + $visitproductivity_3[$login_id];
		  $school_visit_days_3[$login_id] = $school_visit_count_3;
		  $school_visit_month_3 = $school_visit_month_3 + $school_visit_days_3[$login_id];
		  
		  $count_users_3++;
 	  }
	  else{
		  $visitproductivity_3[$login_id] = 0;
		  $school_visit_days_3[$login_id] = 0;
	  }
      
      /*************************************************************************/     	
      //echo"<br>cond 4 : ".$login_id." >> ".$from_date[$login_id]." >> ".$to_date[$login_id];
      
      $no_visit_yt = $this->No_visit_days_model->get_all($login_id, $from_date[$login_id], $to_date[$login_id]);
      foreach($no_visit_yt as $no_visit)
      {
        if ($no_visit->no_visit_reason == 'Local Authority Declared Holiday')
          $auth_dec_holidays[3] = $auth_dec_holidays[3] + 1;
        else
          $no_visit_days[3] = $no_visit_days[3] + 1;
      } 
      
      $state_holiday_list_yt = $this->Holidays_model->get_state_holidays_list($state_id, $from_date[$login_id], $to_date[$login_id], $current_group);
      foreach($state_holiday_list_yt as $stateholiday)
      {
        if(!in_array(date('Y-m-d', strtotime($stateholiday->activity_date)), $activity_dates[3])){
          $activity_dates[3][] = date('Y-m-d', strtotime($stateholiday->activity_date));
        }
      }
      $sun_2_3_list_sat_yt = $this->Holidays_model->get_2_3_saturday_list($from_date[$login_id], $to_date[$login_id], $current_group);
      foreach($sun_2_3_list_sat_yt as $holiday)
      {
        if(!in_array(date('Y-m-d', strtotime($holiday->activity_date)), $activity_dates[3])){
          $activity_dates[3][] = date('Y-m-d', strtotime($holiday->activity_date));
        }
      }
        
      $leaves_arr_yt = $this->Leaves_model->get_all_leave($login_id, $from_date[$login_id], $to_date[$login_id]);
      $leaves_data_yt = $this->common_functions->return_month_leave_distinct($leaves_arr_yt, $from_date[$login_id], $to_date[$login_id], $state_holiday_list_yt, $sun_2_3_list_sat_yt);
      $leaves[3] = count($leaves_data_yt);  
      
      $trainings_yt = $this->Trainings_model->get_all($login_id, $from_date[$login_id], $to_date[$login_id]);
      foreach($trainings_yt as $training)
      {
        if(!in_array(date('Y-m-d', strtotime($training->activity_date)), $activity_dates[3])){
          $activity_dates[3][] = date('Y-m-d', strtotime($training->activity_date));
        }
        if ($training->mode == 'internal')
          $home_district_trainings[3] = $home_district_trainings[3] + 1;
        else
          $outstation_trainings[3] = $outstation_trainings[3] + 1;
        
        if ($training->training_type == 'refresher')
          $refresher_trainings[3] = $refresher_trainings[3] + 1;
        else if ($training->training_type == 'annual')
          $annual_trainings[3] = $annual_trainings[3] + 1;      
      }
      
      $school_visit_count_yt = 0;
      $school_visits_yt = $this->school_visits_model->get_all($login_id, $from_date[$login_id], $to_date[$login_id]);
      foreach($school_visits_yt as $school_visit)
      {
        $school_visit_count_yt++;
        if(!in_array(date('Y-m-d', strtotime($school_visit->activity_date)), $activity_dates[3])){
          $activity_dates[3][] = date('Y-m-d', strtotime($school_visit->activity_date));
        }
      }
          
      // MEETINGS STARTED
      $meeting_blocks = array();
      $meetings_yt = $this->Meetings_model->get_all($login_id, $from_date[$login_id], $to_date[$login_id]);
      foreach($meetings_yt as $meeting)
      {
        if(!in_array(date('Y-m-d', strtotime($meeting->activity_date)), $activity_dates[3])){
          $activity_dates[3][] = date('Y-m-d', strtotime($meeting->activity_date));
        }
        
        $meeting_date = date('Y-m-d', strtotime($meeting->activity_date));
        if (!array_key_exists($meeting_date,$meeting_blocks))
        {
          $meeting_blocks[$meeting_date] = array();
        }
        
        if ($meeting->block_id == 0 and $meeting->is_internal == false){
          $district_level_meetings[3] = $district_level_meetings[3] + 1;
          if (in_array($meeting->meet_with,$spark_meet_with))
              $district_level_distinct_meetings[3]++;
				}
        else if ($meeting->block_id > 0 and $meeting->is_internal == false){
          $block_level_meetings[3] = $block_level_meetings[3] + 1;
          if (!in_array($meeting->block_id,$meeting_blocks[$meeting_date])) {
            if (in_array($meeting->meet_with,$spark_meet_with)) {
              array_push($meeting_blocks[$meeting_date],$meeting->block_id);
              $block_level_distinct_meetings[3]++;
            }
          }
        }
      }
      
      foreach($meetings_yt as $meeting)
      {
        if ($meeting->is_internal == true){
          $internal_meetings[3] = $internal_meetings[3] + 1;
          if(!in_array(date('Y-m-d', strtotime($meeting->activity_date)), $activity_dates[3]) && !in_array(date('Y-m-d', strtotime($meeting->activity_date)), $internal_meeting_dates[3])){
            $internal_meeting_dates[3][] = date('Y-m-d', strtotime($meeting->activity_date));
          }
        }  
      }
      
      $LD_activities_date_yt = array();
      $LD_activities_yt = $this->Claim_model->get_distince_ld_activities($login_id, $from_date[$login_id], $to_date[$login_id]);
      foreach($LD_activities_yt as $LD_activitie)
      {
        if(!in_array($LD_activitie->dat, $activity_dates[3])){
          $LD_activities_date_yt[] = $LD_activitie->dat;
        }
      }
      $distinct_LD_count_yt = count($LD_activities_date_yt);
      
      //-----------------------FIND STATE HOLIDAYS
      $state_holiday_yt = count($state_holiday_list_yt);
		  //-----------------------FIND SUN AND 2,3 SAT
      $sun_2_3_sat_yt = count($sun_2_3_list_sat_yt);
      
      //-----------------------FIND HE VISIT COUNT
      //$he_visit_count_yt = $this->school_visits_model->get_HE_visit_count($login_id, $state_id, $from_date[$login_id], $to_date[$login_id]);
      //-----------------------FIND LE VISIT COUNT
      //$le_visit_count_yt = $this->school_visits_model->get_LE_visit_count($login_id, $state_id, $from_date[$login_id], $to_date[$login_id]);
      //echo"<br>".$productive_days_yt[$login_id]." >> ".$state_holiday_yt." >> ".$leaves[3]." >> ".$sun_2_3_sat_yt." >> ".$auth_dec_holidays[3];
      $net_value = $productive_days_yt[$login_id] - ($state_holiday_yt + $leaves[3] + $sun_2_3_sat_yt + $auth_dec_holidays[3] + count($internal_meeting_dates[3]) + $distinct_LD_count_yt);

      //-----------------------FIND VISIT PRODUCTIVITY 
      $visitproductivity_yt[$login_id] = ($net_value > 0 ? round((($school_visit_count_yt) + ($refresher_trainings[3] * 2) + ($annual_trainings[3] * 2) + ($district_level_distinct_meetings[3] + $block_level_distinct_meetings[3])) / ($net_value),1) : 0);

      $avg_visit_prod_month_yt = $avg_visit_prod_month_yt + $visitproductivity_yt[$login_id];

      //-----------------------FIND NO. OF SCHOOL VISITED IN THE MONTH
      $school_visit_days_yt[$login_id] = $school_visit_count_yt;
      $school_visit_month_yt = $school_visit_month_yt + $school_visit_days_yt[$login_id];
    }

    $count_record = count($user_ids);

    $this->data['school_visit_month_1'] = $school_visit_month_1;
    $this->data['school_visit_month_2'] = $school_visit_month_2;
    $this->data['school_visit_month_3'] = $school_visit_month_3;
    $this->data['school_visit_month_yt'] = $school_visit_month_yt;

    if($count_record > 0){
      $this->data['visit_1'] = ($count_users_1 > 0 ? round(($avg_visit_prod_month_1/$count_users_1),1) : 0);
      $this->data['visit_2'] = ($count_users_2 > 0 ? round(($avg_visit_prod_month_2/$count_users_2),1) : 0);
      $this->data['visit_3'] = ($count_users_3 > 0 ? round(($avg_visit_prod_month_3/$count_users_3),1) : 0);
      $this->data['visit_yt'] = round(($avg_visit_prod_month_yt/$count_record),1);
    }
    else{
      $this->data['visit_1'] = 0;      $this->data['visit_2'] = 0;      $this->data['visit_3'] = 0;      $this->data['visit_yt'] = 0;
    }

    $this->data['schoolvisit_days_1'] = $school_visit_days_1;
    $this->data['schoolvisit_days_2'] = $school_visit_days_2;
    $this->data['schoolvisit_days_3'] = $school_visit_days_3;
    $this->data['schoolvisit_days_yt'] = $school_visit_days_yt;

    $this->data['visit_productivity_1'] = $visitproductivity_1;
    $this->data['visit_productivity_2'] = $visitproductivity_2;
    $this->data['visit_productivity_3'] = $visitproductivity_3;
    $this->data['visit_productivity_yt'] = $visitproductivity_yt;
    
    $this->data['state_id'] = $state_id;
    $this->data['monthno'] = $inpmonth;
    $this->data['year'] = $inpyear;
    
    $this->data['user_names'] = $user_names;
    $this->data['state_name'] = $state_data['name'];
    $this->data['current_month_name'] = $month_name;
    $this->data['prev_month1_name'] = $prev_month1_name;
    $this->data['prev_month2_name'] = $prev_month2_name;

    $download = (isset($_POST['download']) and $_POST['download'] == 1 ? $_POST['download'] : 0);
    $pdf = (isset($_POST['pdf']) and $_POST['pdf'] == 1 ? $_POST['pdf'] : 0);
    
		if ($download == 1)
    {
      $myFile = "./uploads/reports/state_productivity_report.xls";
      $this->load->library('parser');
			$this->data['download'] = $download;
      //pass retrieved data into template and return as a string
      $stringData = $this->parser->parse('reports/state_productivity_report', $this->data, true);
      //open excel and write string into excel
      $fh = fopen($myFile, 'w') or die("can't open file");
      fwrite($fh, $stringData);
      fclose($fh);
      //download excel file
      $this->common_functions->downloadExcel("state_productivity_report");
      unlink($myFile);
    }
    else if($pdf == 1)
    {
        //pass retrieved data into template and return as a string
        $this->data['pdf'] = 1;
        $summary = $this->load->view('reports/state_productivity_report',$this->data,true);
        $this->common_functions->create_pdf($summary,'state_productivity_report');
    }
    else{
      $this->data['partial'] = 'reports/state_productivity_report';
      $this->data['page_title'] = 'State Productivity Report';
      $this->load->view('templates/index', $this->data);
    }
  }

  function spark_school_rating()
  {
    $this->output->enable_profiler(false);
    $month = 4;
    $year = 2016;
    $this->data['year'] = $month;
    $this->data['month'] = $year;

    if($this->data['current_role']=="state_person" || $this->data['current_role']=="manager"){
				
				$states = $this->states_model->get_by_id($this->data['user_state_id']);
    }
    else{
				
				$states = $this->states_model->get_all();
    }
    $this->data['states'] = $states;
    $this->data['partial'] = 'reports/spark_school_rating';
    $this->data['page_title'] = 'Spark School Rating';
    $this->load->view('templates/index', $this->data); 
  }

  function spark_school_rating_report()
  {
    if(!isset($_POST['start_month']) && !isset($_POST['start_year']))
    {
        redirect('reports/spark_school_rating');
    }
    //$state_id = $_POST['spark_state_id'];
    $posteddata = (isset($_POST['spark_id']) ? $_POST['spark_id'] : $this->data['current_user_id'].'-'.$this->data['current_name'].'-'.$this->data['user_state_id']);
    
    $startmonth  = $_POST['start_month'];
    $endmonth  = $_POST['end_month'];
    $startyear = $_POST['start_year'];
    $endyear = $_POST['end_year'];
    $startdate = '01/'.$startmonth.'/'.$startyear;
    $enddate = '01/'.$endmonth.'/'.$endyear;
    $enddate = date('t/m/Y', strtotime($endyear.'-'.$endmonth.'-01'));

    if(!empty($posteddata))
    {
      //Explode data into separte value 
      $final_posted_data = explode("-",$posteddata);  
      //Check for Empty Login _ID     
      if(count($final_posted_data) > 0){
          //Login ID
          $login_id    = $final_posted_data[0]; 
          $spark_name  = $final_posted_data[1];
          $state_id    = $final_posted_data[2];      
      }
    }
    $period = date('M-y', strtotime('01-'.$startmonth.'-'.$startyear));
    if(!empty($_POST['end_month']) && strtotime($startdate) != strtotime($enddate)){
        $period .= " to ".date('M-y', strtotime('01-'.$endmonth.'-'.$endyear));
    }
    if(empty($state_id) || empty($login_id)){
       redirect('reports/spark_school_rating');
    }
    $state_data = $this->states_model->getById($state_id);

    $spark_data = $this->getSparkInfo($startdate,$enddate,$login_id,$state_id);

    $this->load->Model('Assessment_model');
    $this->load->Model('Observations_model');
    $this->load->Model('Classes_model');
    $this->load->Model('Subjects_model');
    $this->load->Model('districts_model');

    //$state_classes = $this->Subjects_model->check_mapping($state_id);
    
    $classes = array();
    array_push($classes, 0);
    $subject_ids = array();
    $class_subjects = array();
    $c = 0;
    /*
    foreach($state_classes as $state_class)
    {
        $class_id = $state_class->class_id;
        $subject_id = $state_class->subject_id;
        if(!in_array($class_id, $classes)){
          array_push($classes, $class_id);
        }
        
        if(!in_array($subject_id, $subject_ids)){
          array_push($subject_ids, $subject_id);
        }
        
        $subjectData = $this->Subjects_model->getById($subject_id);

        $class_subjects[$class_id][$c]['id'] = $subjectData['id'];
        $class_subjects[$class_id][$c]['name'] = $subjectData['name'];
        $c++;
    } */
    
    $observationsData = $this->Observations_model->getReportsObservation($state_id, array(), 0);
    
    $class_observation = array();
    $c = 0;
    foreach($observationsData as $observations)
    {
      $class_observation[$observations->class_id][$c]['id'] = $observations->id;
      $class_observation[$observations->class_id][$c]['question'] = $observations->question;
      $class_observation[$observations->class_id][$c]['weightage'] = $observations->weightage;
      $c++;
    }
    
    $assessment_data = $this->Assessment_model->getSparkSchoolAssessment($startdate,$enddate,$state_id, array(), $login_id, "");

    
    $activity_date_arr = array();
    $school_ids = array();
    $district_ids = array();
    $class_subject_question_ans_counts = array();
    $i = 0;
    //echo '<pre>';
    //print_r($class_observation);
    //print_r($assessment_data);
    $classes = $this->Classes_model->get_all();
    $class_names = array();
    if (count($classes) > 0)
    {
      foreach($classes as $class)
      {
        $class_names[$class->id] = $class->name;
      }
    }
    $subjects = $this->Subjects_model->get_all();
    $subject_names = array();
    if (count($subjects) > 0)
    {
      foreach($subjects as $subject)
      {
        $subject_names[$subject->id] = $subject->name;
      }
    }
    
    foreach($assessment_data as $assessments)
    {
      $activity_date = date('d-m-Y', strtotime($assessments->activity_date));
      $sv_subjects = json_decode($assessments->subjects);
      $sv_classes = json_decode($assessments->classes);
      //print_r($assessments->classes);
      //print_r($sv_classes);
      //print_r($class_names);
      if (is_array($sv_classes)) {
        foreach($sv_classes as $key=>$sv_class) {
          $sv_classes[$key] = $class_names[$sv_class];
        }
        sort($sv_classes);
        $sv_classes = implode(",",$sv_classes);
      } else {
        $sv_classes = "";
      }
      if (is_array($sv_subjects)) {
        foreach($sv_subjects as $key=>$sv_subject) {
          $sv_subjects[$key] = $subject_names[$sv_subject];
        }
        sort($sv_subjects);
        $sv_subjects = implode(",",$sv_subjects);
      } else {
        $sv_subjects = "";
      }
      //print_r($sv_classes);
      //exit;
      $district_id = $assessments->district_id;
      $school_id = $assessments->school_id;
      $question_id = $assessments->question_id;
      $class_id = $assessments->class_id;
      $subject_id = $assessments->subject_id;
      
      if(!isset($activity_date_arr[$activity_date])){
        $activity_date_arr[$activity_date]['school_id'][$i] = $school_id;
        $activity_date_arr[$activity_date]['district_id'][$i] = $district_id;
        $activity_date_arr[$activity_date]['classes'][$i] = $sv_classes;
        $activity_date_arr[$activity_date]['subjects'][$i] = $sv_subjects;
        $i++;
      }else{
        if(!in_array($school_id, $activity_date_arr[$activity_date]['school_id'])){
          $activity_date_arr[$activity_date]['school_id'][$i] = $school_id;
          $activity_date_arr[$activity_date]['district_id'][$i] = $district_id;
          $activity_date_arr[$activity_date]['subjects'][$i] = $sv_subjects;
          $activity_date_arr[$activity_date]['classes'][$i] = $sv_classes;
          $i++;
        }
      }
      
      if(!in_array($school_id, $school_ids))
        array_push($school_ids, $school_id);
      
      if(!in_array($district_id, $district_ids))
        array_push($district_ids, $district_id);
      
      $csq_key = $activity_date.'_'.$school_id.'_'.$question_id;
        
      $ans_value = $assessments->answer;
      
      if($ans_value == 1)
        $class_subject_question_ans_counts[$csq_key] = 1;
      else  
        $class_subject_question_ans_counts[$csq_key] = 0;
    }
    ksort($activity_date_arr);
    //print_r($activity_date_arr);
    //print_r($class_subject_question_ans_counts);
    //die;
        
    $school_data = array();
    if (count($school_ids) > 0)
    {
      $this->load->model('schools_model');
      $schoolData = $this->schools_model->get_schools_details($school_ids);
      foreach($schoolData as $schools)
      {
        $school_data[$schools->id]['name'] = $schools->name;
        $school_data[$schools->id]['dise_code'] = $schools->dise_code;
      }
    }
    
    $district_data = array();
    if (count($district_ids) > 0)
    {
      $this->load->model('districts_model');
      $districtData = $this->districts_model->getByIds($district_ids);
      foreach($districtData as $districts)
      {
        $district_data[$districts->id] = $districts->name;
      }
    }
    
    $this->data['classes'] = $classes;
    $this->data['school_data'] = $school_data;
    $this->data['district_data'] = $district_data;
    $this->data['class_names'] = $class_names;
    $this->data['class_subjects'] = $class_subjects;
    $this->data['class_observation'] = $class_observation;
    $this->data['activity_date_arr'] = $activity_date_arr;
    $this->data['question_arr'] = $observationsData;
    $this->data['data_counts'] = $class_subject_question_ans_counts;
    
    $this->data['STATE'] = $state_data['name'];
    $this->data['PERIOD'] = $period;
    
    $this->data['blocks_count'] = $spark_data[0]->blocks_count;
    $this->data['spark'] = $spark_data[0]->name;
    $this->data['district'] = $spark_data[0]->districts;
    $this->data['no_of_teachers_trained'] = $spark_data[0]->no_of_teachers_trained;
    $this->data['annual_training'] = $spark_data[0]->annual_training;
    $this->data['refresher_training'] = $spark_data[0]->refresher_training;

    $this->data['state_id'] = $state_id;
    $this->data['user_login_id'] = $posteddata;
    $this->data['start_month'] = $startmonth;
    $this->data['end_month'] = $endmonth;
    $this->data['start_year'] = $startyear;
    $this->data['end_year'] = $endyear;

    $download = (isset($_POST['download']) and $_POST['download'] == 1 ? $_POST['download'] : 0);
    
    $pdf = (isset($_POST['pdf']) and $_POST['pdf'] == 1 ? $_POST['pdf'] : 0);

		if($download == 1)
    {
        //pass retrieved data into template and return as a string
        $this->data['excel'] = 1;
        $myFile = "./uploads/reports/spark_school_rating_report.xls";
        $this->load->library('parser');
        $this->data['download'] = $download;
        
        $stringData = $this->parser->parse('reports/spark_school_rating_report', $this->data, true);

        //open excel and write string into excel
        $fh = fopen($myFile, 'w') or die("can't open file");
        fwrite($fh, $stringData);
        fclose($fh);
        //download excel file
        $this->common_functions->downloadExcel("spark_school_rating_report");
        unlink($myFile);
    }
    else if($pdf == 1)
    {
        //pass retrieved data into template and return as a string
        $this->data['pdf'] = 1;
        $summary = $this->load->view('reports/spark_school_rating_report',$this->data,true);
        $this->common_functions->create_pdf($summary,'spark_school_rating_report');
    }
    else{
      $this->data['partial'] = 'reports/spark_school_rating_report';
      $this->data['page_title'] = 'Spark School Rating';
      $this->load->view('templates/index', $this->data);
    }
  }

  function block_school_rating()
  {
    $this->output->enable_profiler(false);
    $month = 4;
    $year = 2016;
    $this->data['year'] = $month;
    $this->data['month'] = $year;

    if($this->data['current_role']=="state_person" || $this->data['current_role']=="manager"){
				
				$states = $this->states_model->get_by_id($this->data['user_state_id']);
    }
    else{
				
				$states = $this->states_model->get_all();
    }
    $this->data['states'] = $states;
    $download = 0;
    $this->data['partial'] = 'reports/block_school_rating';
    $this->data['page_title'] = 'Block School Rating';
    $this->load->view('templates/index', $this->data);
  }

  function block_school_rating_report()
  {
    if(!isset($_POST['start_month']) && !isset($_POST['start_year']))
    {
        redirect('reports/block_school_rating');
    }
    
    $state_id 	  = $_POST['state_id'];
    $district_id  = $_POST['district_id'];
    $block_id		  = $_POST['block_id'];
    $startmonth = $_POST['start_month'];
    $endmonth   = $_POST['end_month'];
    $startyear  = $_POST['start_year'];
    $endyear    = $_POST['end_year'];
    
    $startdate 	= '01/'.$startmonth.'/'.$startyear;
    $enddate 		= date('t/m/Y', strtotime($endyear.'-'.$endmonth.'-01'));
    
    $this->load->Model('Subjects_model');
    $this->load->Model('Classes_model');
    $this->load->Model('Observations_model');
    $this->load->Model('Assessment_model');
    
    $state_classes = $this->Subjects_model->check_mapping($state_id);
    
    $classes = array();
    $subject_ids = array();
    $class_subjects = array();
    $c = 0;
    /*
    foreach($state_classes as $state_class)
    {
        $class_id = $state_class->class_id;
        $subject_id = $state_class->subject_id;
        if(!in_array($class_id, $classes)){
          array_push($classes, $class_id);
        }
        
        if(!in_array($subject_id, $subject_ids)){
          array_push($subject_ids, $subject_id);
        }
        
        $subjectData = $this->Subjects_model->getById($subject_id);

        $class_subjects[$class_id][$c]['id'] = $subjectData['id'];
        $class_subjects[$class_id][$c]['name'] = $subjectData['name'];
        $c++;
    }*/
    
    $observationsData = $this->Observations_model->getReportsObservation($state_id, array(), 0);
    
    $class_observation = array();
    $c = 0;
    
    foreach($observationsData as $observations)
    {
      $class_observation[$observations->class_id][$c]['id'] = $observations->id;
      $class_observation[$observations->class_id][$c]['question'] = $observations->question;
      $class_observation[$observations->class_id][$c]['weightage'] = $observations->weightage;
      $c++;
    }
    
    $classes = $this->Classes_model->get_all();
    $class_names = array();
    if (count($classes) > 0)
    {
      foreach($classes as $class)
      {
        $class_names[$class->id] = $class->name;
      }
    }
    $subjects = $this->Subjects_model->get_all();
    $subject_names = array();
    if (count($subjects) > 0)
    {
      foreach($subjects as $subject)
      {
        $subject_names[$subject->id] = $subject->name;
      }
    }
    
    $block_data        = $this->getBlockInfo($startdate,$enddate,$state_id,$district_id,$block_id);
	  //$rating_data 			 = $this->getBlockRating($startdate,$enddate,$state_id,$district_id, $block_id);
    $assessment_data = $this->Assessment_model->getSchoolAssessmentList($startdate, $enddate, array(), $state_id, $district_id, $block_id, "");
    //print_r($assessment_data);
    //exit;
    $activity_date_arr = array();
    $school_ids = array();
    $district_ids = array();
    $class_subject_question_ans_counts = array();
    $i = 0;
    foreach($assessment_data as $assessments)
    {
      $activity_date = date('d-m-Y', strtotime($assessments->activity_date));
      $sv_subjects = json_decode($assessments->subjects);
      $sv_classes = json_decode($assessments->classes);
      //print_r($assessments->classes);
      //print_r($sv_classes);
      //print_r($class_names);
      if (is_array($sv_classes)) {
        foreach($sv_classes as $key=>$sv_class) {
          $sv_classes[$key] = $class_names[$sv_class];
        }
        sort($sv_classes);
        $sv_classes = implode(",",$sv_classes);
      } else {
        $sv_classes = "";
      }
      if (is_array($sv_subjects)) {
        foreach($sv_subjects as $key=>$sv_subject) {
          $sv_subjects[$key] = $subject_names[$sv_subject];
        }
        sort($sv_subjects);
        $sv_subjects = implode(",",$sv_subjects);
      } else {
        $sv_subjects = "";
      }
      
      $district_id = $assessments->district_id;
      $school_id = $assessments->school_id;
      $question_id = $assessments->question_id;
      $class_id = $assessments->class_id;
      $subject_id = $assessments->subject_id;
      
      if(!isset($activity_date_arr[$activity_date])){
        $activity_date_arr[$activity_date]['school_id'][$i] = $school_id;
        $activity_date_arr[$activity_date]['district_id'][$i] = $district_id;
        $activity_date_arr[$activity_date]['classes'][$i] = $sv_classes;
        $activity_date_arr[$activity_date]['subjects'][$i] = $sv_subjects;
        $i++;
      }else{
        if(!in_array($school_id, $activity_date_arr[$activity_date]['school_id'])){
          $activity_date_arr[$activity_date]['school_id'][$i] = $school_id;
          $activity_date_arr[$activity_date]['district_id'][$i] = $district_id;
          $activity_date_arr[$activity_date]['subjects'][$i] = $sv_subjects;
          $activity_date_arr[$activity_date]['classes'][$i] = $sv_classes;
          $i++;
        }
      }
            
      if(!in_array($school_id, $school_ids))
        array_push($school_ids, $school_id);
      
      if(!in_array($district_id, $district_ids))
        array_push($district_ids, $district_id);
      
      $csq_key = $activity_date.'_'.$school_id.'_'.$question_id;
        
      $ans_value = $assessments->answer;
      
      if($ans_value == 1)
        $class_subject_question_ans_counts[$csq_key] = 1;
      else  
        $class_subject_question_ans_counts[$csq_key] = 0;
    }
    ksort($activity_date_arr);
    //print_r($class_subject_question_ans_counts);
        
    $school_data = array();
    if (count($school_ids) > 0)
    {
      $this->load->model('schools_model');
      $schoolData = $this->schools_model->get_schools_details($school_ids);
      foreach($schoolData as $schools)
      {
        $school_data[$schools->id]['name'] = $schools->name;
        $school_data[$schools->id]['dise_code'] = $schools->dise_code;
      }
    }
    
    $start_date 	= '01-'.$startmonth.'-'.$startyear;
    $end_date 		= '01-'.$endmonth.'-'.$endyear;
    $period = date('M-y', strtotime($start_date));
    if(!empty($_POST['end_month']) && strtotime($start_date) != strtotime($end_date)){
        $period .= " to ".date('M-y', strtotime($end_date));
    }
    
    $this->data['classes'] = $classes;
    $this->data['school_data'] = $school_data;
    $this->data['class_names'] = $class_names;
    $this->data['class_subjects'] = $class_subjects;
    $this->data['class_observation'] = $class_observation;
    $this->data['activity_date_arr'] = $activity_date_arr;
    $this->data['question_arr'] = $observationsData;
    $this->data['data_counts'] = $class_subject_question_ans_counts;
    
    $this->data['PERIOD'] = $period;
        
    $this->data['schools_count'] = $block_data[0]->no_of_schools;
    $this->data['block'] = $block_data[0]->blocks;
    $this->data['district'] = $block_data[0]->districts;
    $this->data['no_of_teachers_trained'] = $block_data[0]->no_of_teachers_trained;
    $this->data['annual_training'] = $block_data[0]->annual_training;
    $this->data['refresher_training'] = $block_data[0]->refresher_training;
    
    $this->data['rating_data'] = array();
    $this->data['assessment_data'] = $assessment_data;

    $this->data['state_id'] = $state_id;
    $this->data['district_id'] = $district_id;
    $this->data['block_id'] = $block_id;
    $this->data['start_month'] = $startmonth;
    $this->data['end_month'] = $endmonth;
    $this->data['start_year'] = $startyear;
    $this->data['end_year'] = $endyear;

    $download = (isset($_POST['download']) and $_POST['download'] == 1 ? $_POST['download'] : 0);
    $pdf = (isset($_POST['pdf']) and $_POST['pdf'] == 1 ? $_POST['pdf'] : 0);
    
		if ($download == 1)
    {
      $this->data['excel'] = 1;
      $myFile = "./uploads/reports/block_school_rating_report.xls";
      $this->load->library('parser');
      $this->data['download'] = $download;
      //pass retrieved data into template and return as a string
      $stringData = $this->parser->parse('reports/block_school_rating_report', $this->data, true);

      //open excel and write string into excel
      $fh = fopen($myFile, 'w') or die("can't open file");
      fwrite($fh, $stringData);
      fclose($fh);
      //download excel file
      $this->common_functions->downloadExcel("block_school_rating_report");
      unlink($myFile);
    }
    else if($pdf == 1)
    {
        //pass retrieved data into template and return as a string
        $this->data['pdf'] = 1;
        $summary = $this->load->view('reports/block_school_rating_report',$this->data,true);
        $this->common_functions->create_pdf($summary,'block_school_rating_report');
    }
    else{
      $this->data['partial'] = 'reports/block_school_rating_report';
      $this->data['page_title'] = 'Block School Rating';
      $this->data['title'] = 'Block School Rating';
      $this->load->view('templates/index', $this->data);
    }
  }

  function district_school_rating()
  {
    $this->output->enable_profiler(false);
    $month = 4;
    $year = 2016;
    $this->data['year'] = $month;
    $this->data['month'] = $year;
    if($this->data['current_role']=="state_person" || $this->data['current_role']=="manager"){
				
				$states = $this->states_model->get_by_id($this->data['user_state_id']);
    }
    else{
				
				$states = $this->states_model->get_all();
    }
    $this->data['states'] = $states;
    $download = 0;
    $this->data['partial'] = 'reports/district_school_rating'; //'reports/state';
    $this->data['page_title'] = 'District School Rating';
    $this->load->view('templates/index', $this->data);
  }

  function district_school_rating_report()
  {
    if(!isset($_POST['start_month']) && !isset($_POST['start_year']))
    {
        redirect('reports/district_school_rating');
    }
    
    $state_id 		= $_POST['state_id'];
    $district_id	= $_POST['district_id'];
    $startmonth   = $_POST['start_month'];	   
    $startyear    = $_POST['start_year'];
    $startdate    = '01/'.$startmonth.'/'.$startyear;
    $startdate    = date('t/m/Y', strtotime($startyear.'-'.$startmonth.'-01'));

    $results          = $this->getDistrictInfo($state_id,$district_id);
	  $trained          = $this->getDistrictTrained($startmonth,$startyear,$state_id,$district_id);

    $month_name = "";
    $prev_month1_name   = "";
    $prev_month2_name   = "";
		$cm_no 		= 0;
		$pv1_no 	= 0;
		$pv2_no 	= 0;
		$current_year1 = 0;
		$current_year2 = 0;
		
    switch($startmonth){
      case "1":
        $month_name = date('F', mktime(0, 0, 0, $startmonth, 10));
        $cm_no      = $startmonth;
        $prev_month1_name   = date('F', mktime(0, 0, 0, 12, 10));
        $pv1_no = 12;
        $current_year1 = $startyear - 1;
        $prev_month2_name   = date('F', mktime(0, 0, 0, 12 - $startmonth, 10));
        $pv2_no = 12 - $startmonth;
        $current_year2 = $startyear - 1;
        break;
      case "2":
        $month_name = date('F', mktime(0, 0, 0, $startmonth, 10));
        $cm_no      = $startmonth;
        $prev_month1_name   = date('F', mktime(0, 0, 0, $startmonth-1, 10));
        $pv1_no = $startmonth-1;
        $current_year1 = $startyear;
        $prev_month2_name   = date('F', mktime(0, 0, 0, 12, 10));
        $pv2_no = 12;
        $current_year2 = $startyear - 1;
        break;
      default:
        $month_name   = date('F', mktime(0, 0, 0, $startmonth, 10));
        $cm_no      = $startmonth;
        $prev_month1_name   = date('F', mktime(0, 0, 0, $startmonth-1, 10));
        $pv1_no = $startmonth-1;
        $current_year1 = $startyear;
        $prev_month2_name   = date('F', mktime(0, 0, 0, $startmonth-2, 10));
        $pv2_no = $startmonth-2;
        $current_year2 = $startyear;
    }
    
    $cm_result = $this->getDistrictRatingData($cm_no, $startyear, $state_id, $district_id);
    $pv1_result = $this->getDistrictRatingData($pv1_no,$current_year1,$state_id,$district_id);
    $pv2_result = $this->getDistrictRatingData($pv2_no,$current_year2,$state_id,$district_id);
    
		$total_block = $this->getBlockCount($state_id,$district_id);
		$nameblock = $this->getBlockName($state_id,$district_id);
	
    $this->data['trained'] = $trained;
    $this->data['cm_result'] = $cm_result;
    $this->data['pv1_result'] = $pv1_result;
    $this->data['pv2_result'] = $pv2_result;
    $this->data['nameblock'] = $nameblock;
    $this->data['month_name'] = $month_name;
    $this->data['prev_month1_name'] = $prev_month1_name;
    $this->data['prev_month2_name'] = $prev_month2_name;
    $this->data['state_name'] = $results[0]->state_name;
    $this->data['district_name'] = $results[0]->district_name;

    $this->data['state_id'] = $state_id;
    $this->data['district_id'] = $district_id;
    $this->data['start_month'] = $startmonth;
    $this->data['start_year'] = $startyear;

    $download = (isset($_POST['download']) and $_POST['download'] == 1 ? $_POST['download'] : 0);
    $pdf = (isset($_POST['pdf']) and $_POST['pdf'] == 1 ? $_POST['pdf'] : 0);
    
		if ($download == 1)
    {
      $this->data['excel'] = 1;
      $myFile = "./uploads/reports/district_school_rating_report.xls";
      $this->load->library('parser');
      $this->data['download'] = $download;
      //pass retrieved data into template and return as a string
      $stringData = $this->parser->parse('reports/district_school_rating_report', $this->data, true);

      //open excel and write string into excel
      $fh = fopen($myFile, 'w') or die("can't open file");
      fwrite($fh, $stringData);
      fclose($fh);
      //download excel file
      $this->common_functions->downloadExcel("district_school_rating_report");
      unlink($myFile);
    }
    else if($pdf == 1)
    {
        //pass retrieved data into template and return as a string
        $this->data['pdf'] = 1;
        $summary = $this->load->view('reports/district_school_rating_report',$this->data,true);
        $this->common_functions->create_pdf($summary,'district_school_rating_report');
    }
    else{
      $this->data['partial'] = 'reports/district_school_rating_report'; //'reports/state';
      $this->data['page_title'] = 'District School Rating';
      $this->load->view('templates/index', $this->data);
    }
  }

  function getDistrictRatingData($cm_no, $startyear, $state_id, $district_id, $duration)
  {
    $this->load->Model('Assessment_model');
    $this->load->Model('school_visits_model');
    
    $assessment_data1 = $this->Assessment_model->getSparkRatingReport($cm_no, $startyear, $state_id, $district_id, $duration);
    
    $blocks_arr = array();
    $report_ids = array();
    foreach($assessment_data1 as $assessments)
    {
      $block_id = $assessments->block_id;
      $report_id = $assessments->id;
      $school_id = $assessments->school_id;
      
      if($school_id == '0')
      {
        $sv_data = $this->school_visits_model->getByDiseCode($assessments->activity_id);
        $school_id = $sv_data['schoolid'];
      }
      
      if(!isset($blocks_arr[$block_id]))
      {
        $blocks_arr[$block_id]['school_id'][] =  $school_id;
        $blocks_arr[$block_id]['report_id'][] =  $report_id;
      }
        if(!in_array($school_id, $blocks_arr[$block_id]['school_id']))
          array_push($blocks_arr[$block_id]['school_id'], $school_id);
      
        if(!in_array($report_id, $blocks_arr[$block_id]['report_id']))
          array_push($blocks_arr[$block_id]['report_id'], $report_id);
      
    }
    
    $rating_data = array();
    foreach($blocks_arr as $block_id=>$data)
    {
      $school_data = $data['school_id'];
      $school_count = count($data['school_id']); //CHANGE SCHOOL COUNT
      //$school_count = count($data['report_id']);
      
      $report_id = $data['report_id'];
      
      $ratings_A = 0;
      $ratings_B = 0;
      $ratings_C = 0;
      $rating_count = 0;
      $ratingData = $this->Assessment_model->getSparkRatingCount($report_id);
      foreach($ratingData as $ratings){
        if($ratings['subject_rating'] == 'A'){
          $ratings_A = $ratings['Total'];
        }
        if($ratings['subject_rating'] == 'B'){
          $ratings_B = $ratings['Total'];
        }
        if($ratings['subject_rating'] == 'C'){
          $ratings_C = $ratings['Total'];
        }
        $rating_count = $ratings_A + $ratings_B + $ratings_C;
      }
      $rating_data[$block_id]['school_count'] = $school_count;
      $rating_data[$block_id]['rating_A'] = ($ratings_A > 0 ? ($ratings_A/$rating_count)*100 : 0);
      $rating_data[$block_id]['rating_B'] = ($ratings_B > 0 ? ($ratings_B/$rating_count)*100 : 0);
      $rating_data[$block_id]['rating_C'] = ($ratings_C > 0 ? ($ratings_C/$rating_count)*100 : 0);
    }
    return $rating_data;
  }

  function state_school_rating()
  {
    $this->output->enable_profiler(false);
    $month = 4;
    $year = 2016;
    $this->data['year'] = $month;
    $this->data['month'] = $year;

    if($this->data['current_role']=="state_person"){
				
				$states = $this->states_model->get_by_id($this->data['user_state_id']);
    }
    else{
				
				$states = $this->states_model->get_all();
    }
    
    $this->data['states'] = $states;
    $this->data['partial'] = 'reports/state_school_rating';
    $this->data['page_title'] = 'State School Rating';
    $this->load->view('templates/index', $this->data); 
  }

  function state_school_rating_report()
  {
    if(!isset($_POST['start_month']) && !isset($_POST['start_year']))
    {
        redirect('reports/state_school_rating');
    }
    
    $state_id 		 = $_POST['state_id'];
	  $startmonth       = $_POST['start_month'];	   
	  $startyear        = $_POST['start_year'];
    $startdate        = '01/'.$startmonth.'/'.$startyear;
	  $startdate        = date('t/m/Y', strtotime($startyear.'-'.$startmonth.'-01'));

	  $results          = $this->getStateInfo($state_id);
	  $trained          = $this->getStateTrained($startmonth,$startyear,$state_id);
    $trainedresults = array();
    foreach($trained as $value)
    {
      $trainedresults[$value->district_name]['refresher_training'] = $value->refresher_training;
      $trainedresults[$value->district_name]['annual_training'] = $value->annual_training;
    }
    
    $month_name = "";
    $prev_month1_name   = "";
    $prev_month2_name   = "";
		$cm_no 		= 0;
		$pv1_no 	= 0;
		$pv2_no 	= 0;
		$current_year1 = 0;
		$current_year2 = 0;
		
    switch($startmonth){

      case "1":
        $month_name = date('F', mktime(0, 0, 0, $startmonth, 10));
        $cm_no      = $startmonth;
        $prev_month1_name   = date('F', mktime(0, 0, 0, 12, 10));
        $pv1_no = 12;
        $current_year1 = $startyear - 1;
        $prev_month2_name   = date('F', mktime(0, 0, 0, 12 - $startmonth, 10));
        $pv2_no = 12 - $startmonth;
        $current_year2 = $startyear - 1;
        break;
      case "2":
        $month_name = date('F', mktime(0, 0, 0, $startmonth, 10));
        $cm_no      = $startmonth;
        $prev_month1_name   = date('F', mktime(0, 0, 0, $startmonth-1, 10));
        $pv1_no = $startmonth-1;
        $current_year1 = $startyear;
        $prev_month2_name   = date('F', mktime(0, 0, 0, 12, 10));
        $pv2_no = 12;
        $current_year2 = $startyear - 1;
        break;
      default:
        $month_name   = date('F', mktime(0, 0, 0, $startmonth, 10));
        $cm_no      = $startmonth;
        $prev_month1_name   = date('F', mktime(0, 0, 0, $startmonth-1, 10));
        $pv1_no = $startmonth-1;
        $current_year1 = $startyear;
        $prev_month2_name   = date('F', mktime(0, 0, 0, $startmonth-2, 10));
        $pv2_no = $startmonth-2;
        $current_year2 = $startyear;
    }
		
		$cm_result            = $this->getStateRatingData($cm_no,$startyear,$state_id);
		$pv1_result           = $this->getStateRatingData($pv1_no,$current_year1,$state_id);
		$pv2_result           = $this->getStateRatingData($pv2_no,$current_year2,$state_id);
    
		$total_district = $this->getDistrictCount($state_id);
		$namedistrict = $this->getDistrictName($state_id);
      
    $this->data['trained'] = $trainedresults;
    $this->data['cmresults'] = $cm_result;
    $this->data['pv1results'] = $pv1_result;
    $this->data['pv2results'] = $pv2_result;
    $this->data['namedistrict'] = $namedistrict;
    $this->data['month_name'] = $month_name;
    $this->data['prev_month1_name'] = $prev_month1_name;
    $this->data['prev_month2_name'] = $prev_month2_name;

    $this->data['state_name'] = $results[0]->state_name;

    $this->data['state_id'] = $state_id;
    $this->data['start_month'] = $startmonth;
    $this->data['start_year'] = $startyear;

    $download = (isset($_POST['download']) and $_POST['download'] == 1 ? $_POST['download'] : 0);
    $pdf = (isset($_POST['pdf']) and $_POST['pdf'] == 1 ? $_POST['pdf'] : 0);
    
		if ($download == 1)
    {
      $this->data['excel'] = 1;
      $myFile = "./uploads/reports/state_school_rating_report.xls";
      $this->load->library('parser');
      $this->data['download'] = $download;
      //pass retrieved data into template and return as a string
      $stringData = $this->parser->parse('reports/state_school_rating_report', $this->data, true);

      //open excel and write string into excel
      $fh = fopen($myFile, 'w') or die("can't open file");
      fwrite($fh, $stringData);
      fclose($fh);
      //download excel file
      $this->common_functions->downloadExcel("state_school_rating_report");
      unlink($myFile);
    }
    else if($pdf == 1)
    {
        //pass retrieved data into template and return as a string
        $this->data['pdf'] = 1;
        $summary = $this->load->view('reports/state_school_rating_report',$this->data,true);
        $this->common_functions->create_pdf($summary,'state_school_rating_report');
    }
    else{
      $this->data['partial'] = 'reports/state_school_rating_report'; 
      $this->data['page_title'] = 'State School Rating';
      $this->load->view('templates/index', $this->data);
    }
  }
  
  function getStateRatingData($cm_no, $startyear, $state_id, $duration='')
  {
    $this->load->Model('Assessment_model');
    $this->load->Model('school_visits_model');
    
    $assessment_data1 = $this->Assessment_model->getSparkRatingReport($cm_no, $startyear, $state_id, '', $duration);
    //print_r($assessment_data1);
    $districts_arr = array();
    $report_ids = array();
    foreach($assessment_data1 as $assessments)
    {
      $district_id = $assessments->district_id;
      $report_id = $assessments->id;
      $school_id = $assessments->school_id;
      
      if($school_id == '0')
      {
        $sv_data = $this->school_visits_model->getByDiseCode($assessments->activity_id);
        $school_id = $sv_data['schoolid'];
      }
      
      if(!isset($districts_arr[$district_id]))
      {
        $districts_arr[$district_id]['school_id'][] =  $school_id;
        $districts_arr[$district_id]['report_id'][] =  $report_id;
      }
        if(!in_array($school_id, $districts_arr[$district_id]['school_id']))
          array_push($districts_arr[$district_id]['school_id'], $school_id);
      
        if(!in_array($report_id, $districts_arr[$district_id]['report_id']))
          array_push($districts_arr[$district_id]['report_id'], $report_id);
      
    }
    
    $rating_data = array();
    
    foreach($districts_arr as $district_id=>$data)
    {
      $school_data = $data['school_id'];
      $school_count = count($data['school_id']); //CHANGE SCHOOL COUNT
      //$school_count = count($data['report_id']);
      
      $report_id = $data['report_id'];
      
      $ratings_A = 0;
      $ratings_B = 0;
      $ratings_C = 0;
      $rating_count = 0;
      $ratingData = $this->Assessment_model->getSparkRatingCount($report_id);
      foreach($ratingData as $ratings){
        if($ratings['subject_rating'] == 'A'){
          $ratings_A = $ratings['Total'];
        }
        if($ratings['subject_rating'] == 'B'){
          $ratings_B = $ratings['Total'];
        }
        if($ratings['subject_rating'] == 'C'){
          $ratings_C = $ratings['Total'];
        }
        $rating_count = $ratings_A + $ratings_B + $ratings_C;
      }
      $rating_data[$district_id]['school_count'] = $school_count;
      $rating_data[$district_id]['rating_A'] = ($ratings_A > 0 ? ($ratings_A/$rating_count)*100 : 0);
      $rating_data[$district_id]['rating_B'] = ($ratings_B > 0 ? ($ratings_B/$rating_count)*100 : 0);
      $rating_data[$district_id]['rating_C'] = ($ratings_C > 0 ? ($ratings_C/$rating_count)*100 : 0);
    }
    return $rating_data;
  }
  
  function getSparkInfo($startdate,$enddate,$loginid,$stateid) 
  {
     $select_stmt = "Select (select name from ssc_sparks where id = '$loginid') name,
     (select count(distinct block_id)
        from ssc_school_visits
       where (case when '$loginid' = '-1' then -1 else user_id end) = (case when '$loginid' = '-1' then -1 else '$loginid' end)
         and state_id = '$stateid'
         and date(activity_date) between str_to_date('$startdate','%d/%m/%Y') and  str_to_date('$enddate','%d/%m/%Y')) blocks_count,
     (select GROUP_CONCAT(distinct sd.name)
        from ssc_school_visits ssv,
             ssc_districts sd
       where ssv.district_id = sd.id
         and ssv.state_id = sd.state_id
         and ssv.user_id = '$loginid'
         and ssv.state_id ='$stateid'
         and date(ssv.activity_date) between str_to_date('$startdate','%d/%m/%Y') and  str_to_date('$enddate','%d/%m/%Y')) districts,
     (select IFNULL(sum(no_of_teachers_trained), 0)
        from ssc_trainings
       where (case when '$loginid' = '-1' then -1 else user_id end) = (case when '$loginid' = '-1' then -1 else '$loginid' end)
         and state_id = '$stateid'
     and subject_id IN (1, 2)
         and date(activity_date) between str_to_date('$startdate','%d/%m/%Y') and  str_to_date('$enddate','%d/%m/%Y')) no_of_teachers_trained,
     (SELECT count(training_duration)
        FROM ssc_trainings
       where (case when '$loginid' = '-1' then -1 else user_id end) = (case when '$loginid' = '-1' then -1 else '$loginid' end)
         and state_id = '$stateid'
     and subject_id IN (1, 2)
         and date(activity_date) between str_to_date('$startdate','%d/%m/%Y') and  str_to_date('$enddate','%d/%m/%Y')
         and training_type = 'annual') annual_training,
     (SELECT count(training_duration)
        FROM ssc_trainings
       where (case when '$loginid' = '-1' then -1 else user_id end) = (case when '$loginid' = '-1' then -1 else '$loginid' end)
         and state_id = '$stateid'
     and subject_id IN (1, 2)
         and date(activity_date) between str_to_date('$startdate','%d/%m/%Y') and  str_to_date('$enddate','%d/%m/%Y')
         and training_type = 'refresher') refresher_training";
    
     $select_query = $this->db->query($select_stmt);
     
//Execute query 
      $row = $select_query->result();
      return ($row);
    }
	
  function getBlockInfo($startdate,$enddate,$state_id,$district_id,$block_id){
		 $select_stmt = "Select (select count(distinct ss.id)
          from ssc_school_visits ssv,
               ssc_schools ss
          where ssv.school_id = ss.id
            and ssv.block_id = '$block_id'
            and ssv.district_id = '$district_id'
            and ssv.state_id ='$state_id'
            and ssv.status ='approved'
            and ssv.activity_date between str_to_date('$startdate','%d/%m/%Y') and  str_to_date('$enddate','%d/%m/%Y')) no_of_schools,
	   (select GROUP_CONCAT(distinct sb.name)
          from ssc_school_visits ssv,
               ssc_blocks sb
          where ssv.block_id = sb.id
            and ssv.state_id = sb.state_id
            and ssv.block_id = '$block_id'
            and ssv.district_id = '$district_id'
            and ssv.state_id ='$state_id'
            and ssv.status ='approved'
            and ssv.activity_date between str_to_date('$startdate','%d/%m/%Y') and  str_to_date('$enddate','%d/%m/%Y')) blocks,
       (select GROUP_CONCAT(distinct sd.name)
          from ssc_school_visits ssv,
               ssc_districts sd
          where ssv.district_id = sd.id
            and ssv.state_id = sd.state_id
            and ssv.block_id = '$block_id'
            and ssv.district_id = '$district_id'
            and ssv.state_id ='$state_id'
            and ssv.status ='approved'
            and ssv.activity_date between str_to_date('$startdate','%d/%m/%Y') and  str_to_date('$enddate','%d/%m/%Y')) districts,
       (select IFNULL(sum(no_of_teachers_trained), 0)
          from ssc_trainings
          where block_id = '$block_id'
            and district_id = '$district_id'
            and state_id = '$state_id'
            and status ='approved'
            and subject_id in (1, 2)
            and activity_date between str_to_date('$startdate','%d/%m/%Y') and  str_to_date('$enddate','%d/%m/%Y')) no_of_teachers_trained,
       (SELECT count(training_duration)
          FROM ssc_trainings
          where block_id = '$block_id'
            and district_id = '$district_id'
            and state_id = '$state_id'
            and status ='approved'
            and subject_id in (1, 2)
            and activity_date between str_to_date('$startdate','%d/%m/%Y') and  str_to_date('$enddate','%d/%m/%Y')
            and training_type = 'annual') annual_training,
       (SELECT count(training_duration)
          FROM ssc_trainings
          where block_id = '$block_id'
            and district_id = '$district_id'
            and state_id = '$state_id'
            and status ='approved'
            and subject_id in (1, 2)
            and activity_date between str_to_date('$startdate','%d/%m/%Y') and  str_to_date('$enddate','%d/%m/%Y')
            and training_type = 'refresher') refresher_training";
      
       $select_query = $this->db->query($select_stmt);
       //Execute query 
       $row = $select_query->result();
       return ($row);
	}
	
	function getBlockCount($state_id,$district_id){
		$select_stmt = "SELECT count(*) as dis_count FROM `ssc_blocks` WHERE state_id='$state_id' AND district_id= '$district_id'";
    $select_query = $this->db->query($select_stmt);
    //Execute query 
    $row = $select_query->result();
    return ($row[0]->dis_count);
	}
  
	function getBlockName($state_id,$district_id){
		$select_stmt = "SELECT id,name FROM `ssc_blocks` WHERE state_id='$state_id' AND district_id= '$district_id' order by name";
    $select_query = $this->db->query($select_stmt);
    //Execute query 
    $row = $select_query->result();
    return ($row);
	}

	function getDistrictInfo($stateid,$districtid){
      $select_stmt = "select
                      (select name from ssc_districts where id = '$districtid') district_name,
                      (select name from ssc_states where id = '$stateid') state_name";
      $select_query = $this->db->query($select_stmt);
       
      $row = $select_query->result();
      return ($row);
	 }
   
  function getDistrictTrained($startmonth,$startyear,$state_id,$district_id){
      $select_stmt = "select sb.name block_name,
           q.block_id,
           IFNULL(max(q.no_of_teachers_trained_annual), 0) no_of_teachers_trained_annual,
           IFNULL(max(q.no_of_teachers_trained_refresher), 0) no_of_teachers_trained_refresher,
           IFNULL(max(q.annual_training), 0) annual_training,
           IFNULL(max(q.refresher_training), 0) refresher_training,
           max(q.no_of_schools) no_of_schools
      from
      (Select st.block_id,
             sum((case when st.training_type = 'annual' then st.no_of_teachers_trained else null end)) no_of_teachers_trained_annual,
             sum((case when st.training_type != 'refresher' then st.no_of_teachers_trained else null end)) no_of_teachers_trained_refresher,
             count((case when st.training_type = 'annual' then st.training_type else null end)) annual_training,
             count((case when st.training_type != 'refresher' then st.training_type else null end)) refresher_training,
             null no_of_schools
        from ssc_trainings st
        where st.state_id = '$state_id'
        and st.district_id = '$district_id'
        and st.subject_id in (1, 2)
        and month(st.activity_date) = '$startmonth'
        and year(st.activity_date) = '$startyear'
        and st.status = 'approved'
        group by st.block_id
      union all
      select ssv.block_id,
             null,
             null,
             null,
             null,
             count(distinct ss.id) no_of_schools
        from ssc_school_visits ssv,
             ssc_schools ss
       where ssv.school_id = ss.id
         and ssv.state_id = '$state_id'
         and ssv.district_id = '$district_id'
         and month(ssv.activity_date) = '$startmonth'
         and year(ssv.activity_date) = '$startyear'
         and ssv.status = 'approved'
        group by ssv.block_id) q,
      ssc_blocks sb
      where q.block_id = sb.id
      group by sb.name, q.block_id";
		   $select_query = $this->db->query($select_stmt);
		   
		  //Execute query 
			$row = $select_query->result();

			return ($row);
	}

  function getDistrictCount($state_id){
		$select_stmt = "SELECT count(*) as dis_count FROM `ssc_districts` WHERE 
		state_id='$state_id'";
       $select_query = $this->db->query($select_stmt);
       
	//Execute query 
        $row = $select_query->result();
        return ($row[0]->dis_count);
	}

  function getDistrictName($state_id){
		$select_stmt = "SELECT id, name FROM `ssc_districts` WHERE 
		state_id='$state_id' order by name";
       $select_query = $this->db->query($select_stmt);
       
	//Execute query 
        $row = $select_query->result();
        return ($row);
	}
	
	function getStateRating($startmonth,$startyear,$state_id){
	 $select_query =  "SELECT r.district_name
	,r.total
	,ROUND((r.rating_A_schools / r.total_schools) * 100, 1) rating_A_perct
	,ROUND((r.rating_B_schools / r.total_schools) * 100, 1) rating_B_perct
	,ROUND((r.rating_C_schools / r.total_schools) * 100, 1) rating_C_perct
FROM (
	SELECT i.activity_date
		,i.district_name
		,i.school_id
		,i.state_id
		,i.district_id
		,count(CASE 
				WHEN month(i.activity_date) = '$startmonth'
					AND year(i.activity_date) = '$startyear'
					AND i.state_id = '$state_id'
					THEN i.district_name
				ELSE 0
				END) total
		,count((
				CASE 
					WHEN i.overall_rating = 'A'
						THEN i.school_id
					ELSE NULL
					END
				)) rating_A_schools
		,count((
				CASE 
					WHEN i.overall_rating = 'B'
						THEN i.school_id
					ELSE NULL
					END
				)) rating_B_schools
		,count((
				CASE 
					WHEN i.overall_rating = 'C'
						THEN i.school_id
					ELSE NULL
					END
				)) rating_C_schools
		,count(i.school_id) total_schools
	FROM (
		SELECT k.activity_date
			,sd.name district_name
			,sd.id
			,k.state_id
			,k.district_id
			,k.school_id
			,IFNULL((
					CASE 
						WHEN ((english_rating > 0 AND maths_rating > 0) AND ((maths_rating + english_rating) / 2) >= 76)
              THEN 'A'
            WHEN ((english_rating > 0 AND maths_rating > 0) AND ((maths_rating + english_rating) / 2) <= 75 AND ((maths_rating + english_rating) / 2) >= 51)
              THEN 'B'
            WHEN (english_rating = 0 AND maths_rating > 0 AND maths_rating >= 76)
              THEN 'A'
            WHEN (english_rating = 0 AND maths_rating > 0 AND (maths_rating <= 75 AND maths_rating >= 51))
              THEN 'B'
            WHEN ((maths_rating = 0 AND english_rating > 0) AND english_rating >= 76)
              THEN 'A'
            WHEN ((maths_rating = 0 AND english_rating > 0) AND (english_rating <= 75 AND english_rating >= 51))
              THEN 'B'    
						ELSE 'C'
						END
					), 0) overall_rating
		FROM ssc_districts sd
		INNER JOIN (
			SELECT ssv.activity_date
				,ssv.id
				,ssv.state_id
				,ssv.district_id
				,ssv.block_id
				,ssv.cluster_id
				,ssv.school_id
				,ssv.user_id
				,sa.school_visit_id
				,sa.assessment_id
				,MAX((
						CASE 
							WHEN sa.subject_id = 1
								THEN (((sa.trained_teachers + sa.tlm_usage + sa.sequence_usage + sa.prog_chart_usage) / 4) * 100)
							ELSE - 1
							END
						)) maths_rating
				,MAX((
						CASE 
							WHEN sa.subject_id = 2
								THEN (((sa.trained_teachers + sa.tlm_usage + sa.sequence_usage + sa.prog_chart_usage) / 4) * 100)
							ELSE - 1
							END
						)) english_rating
			FROM ssc_assessment sa
			INNER JOIN ssc_school_visits ssv ON ssv.id = sa.school_visit_id
				AND sa.subject_id IN (
					1
					,2
					)
				AND month(ssv.activity_date) = '$startmonth'
				AND year(ssv.activity_date) = '$startyear'
				AND ssv.state_id = '$state_id'
        AND ssv.status = 'approved'
        and ssv.id = (select max(ssv2.id) from ssc_school_visits ssv2
          where month(ssv2.activity_date) = '$startmonth'
            and year(ssv2.activity_date) = '$startyear'
            and ssv2.school_id = ssv.school_id
            and ssv2.user_id = ssv.user_id
            and ssv2.district_id = ssv.district_id
            and ssv2.block_id = ssv.block_id)
			GROUP BY DATE(ssv.activity_date), ssv.school_id
			) k ON k.district_id = sd.id
		) i
	GROUP BY i.district_name
	) r
GROUP BY r.district_name
ORDER BY r.district_name";
/*WHEN ((maths_rating + english_rating) / 2) >= 76
							THEN 'A'
						WHEN (
								((maths_rating + english_rating) / 2) <= 75
								AND ((maths_rating + english_rating) / 2) >= 51
								)
							THEN 'B'
						ELSE 'C'
						END*/
	$select_query =  $this->db->query($select_query);
     
    //Execute query 
    $row = $select_query->result();
	
	return ($row);
	}

	function getStateInfo($state_id){
		$select_stmt = "select name state_name from ssc_states where id = '$state_id'";
       $select_query = $this->db->query($select_stmt);
       
	//Execute query 
        $row = $select_query->result();
		return ($row);
	 }
	
	function getStateTrained($startmonth,$startyear,$state_id){
		$select_stmt = "select sd.name district_name,
       q.district_id,
       IFNULL(max(q.no_of_teachers_trained_fd), 0) no_of_teachers_trained_fd,
       IFNULL(max(q.no_of_teachers_trained_hd), 0) no_of_teachers_trained_hd,
       IFNULL(max(q.annual_training), 0) annual_training,
       IFNULL(max(q.refresher_training), 0) refresher_training,
       max(q.no_of_schools) no_of_schools
  from
  (Select st.district_id,
         sum((case when st.training_duration = '24:00' then st.no_of_teachers_trained else null end)) no_of_teachers_trained_fd,
         sum((case when st.training_duration != '24:00' then st.no_of_teachers_trained else null end)) no_of_teachers_trained_hd,
         count((case when st.training_duration = '24:00' then st.training_duration else null end)) annual_training,
         count((case when st.training_duration != '24:00' then st.training_duration else null end)) refresher_training,
         null no_of_schools
    from ssc_trainings st
   where st.state_id = '$state_id'
     and st.subject_id in (1, 2)
     and month(st.activity_date) = '$startmonth'
     and year(st.activity_date) = '$startyear'
     and st.status = 'approved'
   group by st.district_id
  union all
  select ssv.district_id,
         null,
         null,
         null,
         null,
         count(distinct ss.id) no_of_schools
    from ssc_school_visits ssv,
         ssc_schools ss
   where ssv.school_id = ss.id
     and ssv.state_id ='$state_id'
     and month(ssv.activity_date) = '$startmonth'
     and year(ssv.activity_date) = '$startyear'
     and ssv.status = 'approved'
    group by ssv.district_id) q,
  ssc_districts sd
where q.district_id = sd.id
 group by sd.name, q.district_id;
";
		   $select_query = $this->db->query($select_stmt);
		   
		  //Execute query 
			$row = $select_query->result();
			 // echo("<pre>");
		     // print_r($row);
		     // die();
			return ($row);
	}
	
  function getStatesName(){
		$select_stmt = "SELECT id, name FROM `ssc_states` order by name";
    $select_query = $this->db->query($select_stmt);
    $row = $select_query->result();
    return ($row);
	}

  function national_school_rating()
  {
    $this->output->enable_profiler(false);
    $month = 4;
    $year = 2016;
    $this->data['year'] = $month;
    $this->data['month'] = $year;

    $this->data['partial'] = 'reports/national_school_rating';
    $this->data['page_title'] = 'National School Rating';
    $this->load->view('templates/index', $this->data); 
  }

  function national_school_rating_report()
  {
    if(!isset($_POST['start_month']) && !isset($_POST['start_year']))
    {
        redirect('reports/national_school_rating');
    }
    $startmonth       = $_POST['start_month'];	   
	  $startyear        = $_POST['start_year'];
    $startdate        = '01/'.$startmonth.'/'.$startyear;
	  $startdate        = date('t/m/Y', strtotime($startyear.'-'.$startmonth.'-01'));

    $month_name = "";
    $prev_month1_name   = "";
    $prev_month2_name   = "";
		$cm_no 		= 0;
		$pv1_no 	= 0;
		$pv2_no 	= 0;
		$current_year1 = 0;
		$current_year2 = 0;
		
    switch($startmonth){

      case "1":
        $month_name = date('F', mktime(0, 0, 0, $startmonth, 10));
        $cm_no      = $startmonth;
        $prev_month1_name   = date('F', mktime(0, 0, 0, 12, 10));
        $pv1_no = 12;
        $current_year1 = $startyear - 1;
        $prev_month2_name   = date('F', mktime(0, 0, 0, 12 - $startmonth, 10));
        $pv2_no = 12 - $startmonth;
        $current_year2 = $startyear - 1;
        break;
      case "2":
        $month_name = date('F', mktime(0, 0, 0, $startmonth, 10));
        $cm_no      = $startmonth;
        $prev_month1_name   = date('F', mktime(0, 0, 0, $startmonth-1, 10));
        $pv1_no = $startmonth-1;
        $current_year1 = $startyear;
        $prev_month2_name   = date('F', mktime(0, 0, 0, 12, 10));
        $pv2_no = 12;
        $current_year2 = $startyear - 1;
        break;
      default:
        $month_name   = date('F', mktime(0, 0, 0, $startmonth, 10));
        $cm_no      = $startmonth;
        $prev_month1_name   = date('F', mktime(0, 0, 0, $startmonth-1, 10));
        $pv1_no = $startmonth-1;
        $current_year1 = $startyear;
        $prev_month2_name   = date('F', mktime(0, 0, 0, $startmonth-2, 10));
        $pv2_no = $startmonth-2;
        $current_year2 = $startyear;
    }
		
		$cm_result            = $this->getNationalRatingData($cm_no,$startyear);
		$pv1_result           = $this->getNationalRatingData($pv1_no,$current_year1);
		$pv2_result           = $this->getNationalRatingData($pv2_no,$current_year2);
    //print_r($pv2_result);die;
    
		$namestate = $this->getStatesName();
      
    //$this->data['trained'] = $trainedresults;
    $this->data['cmresults'] = $cm_result;
    $this->data['pv1results'] = $pv1_result;
    $this->data['pv2results'] = $pv2_result;
    $this->data['namestate'] = $namestate;
    $this->data['month_name'] = $month_name;
    $this->data['prev_month1_name'] = $prev_month1_name;
    $this->data['prev_month2_name'] = $prev_month2_name;
    $this->data['start_month'] = $startmonth;
    $this->data['start_year'] = $startyear;

    $download = (isset($_POST['download']) and $_POST['download'] == 1 ? $_POST['download'] : 0);
    $pdf = (isset($_POST['pdf']) and $_POST['pdf'] == 1 ? $_POST['pdf'] : 0);
    
		if ($download == 1)
    {
      $this->data['excel'] = 1;
      $myFile = "./uploads/reports/national_school_rating_report.xls";
      $this->load->library('parser');
      $this->data['download'] = $download;
      //pass retrieved data into template and return as a string
      $stringData = $this->parser->parse('reports/national_school_rating_report', $this->data, true);

      //open excel and write string into excel
      $fh = fopen($myFile, 'w') or die("can't open file");
      fwrite($fh, $stringData);
      fclose($fh);
      //download excel file
      $this->common_functions->downloadExcel("national_school_rating_report");
      unlink($myFile);
    }
    else if($pdf == 1)
    {
      //pass retrieved data into template and return as a string
      $this->data['pdf'] = 1;
      $summary = $this->load->view('reports/national_school_rating_report',$this->data,true);
      $this->common_functions->create_pdf($summary,'national_school_rating_report');
    }
    else{
      $this->data['partial'] = 'reports/national_school_rating_report'; 
      $this->data['page_title'] = 'National School Rating';
      $this->load->view('templates/index', $this->data);
    }
  }

  function getNationalRatingData($cm_no, $startyear)
  {
    $this->load->Model('Assessment_model');
    
    $assessment_data1 = $this->Assessment_model->getSparkRatingReport($cm_no, $startyear);
    
    $districts_arr = array();
    $report_ids = array();
    foreach($assessment_data1 as $assessments)
    {
      $state_id = $assessments->state_id;
      $report_id = $assessments->id;
      $school_id = $assessments->school_id;
      
      if(!isset($districts_arr[$state_id]))
      {
        $districts_arr[$state_id]['school_id'][] =  $school_id;
        $districts_arr[$state_id]['report_id'][] =  $report_id;
      }
        if(!in_array($school_id, $districts_arr[$state_id]['school_id']))
          array_push($districts_arr[$state_id]['school_id'], $school_id);
      
        if(!in_array($report_id, $districts_arr[$state_id]['report_id']))
          array_push($districts_arr[$state_id]['report_id'], $report_id);
      
    }
    
    $rating_data = array();
    foreach($districts_arr as $state_id=>$data)
    {
      $school_data = $data['school_id'];
      $school_count = count($data['school_id']);
      
      $report_id = $data['report_id'];
      
      $ratings_A = 0;
      $ratings_B = 0;
      $ratings_C = 0;
      $rating_count = 0;
      $ratingData = $this->Assessment_model->getSparkRatingCount($report_id);
      foreach($ratingData as $ratings){
        if($ratings['subject_rating'] == 'A'){
          $ratings_A = $ratings['Total'];
        }
        if($ratings['subject_rating'] == 'B'){
          $ratings_B = $ratings['Total'];
        }
        if($ratings['subject_rating'] == 'C'){
          $ratings_C = $ratings['Total'];
        }
        $rating_count = $ratings_A + $ratings_B + $ratings_C;
      }
      $rating_data[$state_id]['school_count'] = $school_count;
      $rating_data[$state_id]['rating_A'] = ($ratings_A > 0 ? round(($ratings_A/$rating_count)*100,2) : 0);
      $rating_data[$state_id]['rating_B'] = ($ratings_B > 0 ? round(($ratings_B/$rating_count)*100,2) : 0);
      $rating_data[$state_id]['rating_C'] = ($ratings_C > 0 ? round(($ratings_C/$rating_count)*100,2) : 0);
    }
    return $rating_data;
  }

	function return_months($start_date, $end_date){
	   
		$a = $start_date;
		$b = $end_date;

		$i = date("Y-m", strtotime($a));
		$months = array();
		while($i <= date("Y-m", strtotime($b))){
			$months[] = $i;
			if(substr($i, 5, 2) == "12")
				$i = (date("Y", strtotime($i."-01")) + 1)."-01";
			else
				$i++;
		}
		return $months;
	}
 
  function state_school_assessment()
  {
    $this->output->enable_profiler(false);
    $month = 4;
    $year = 2016;
    $this->data['year'] = $month;
    $this->data['month'] = $year;

    if($this->data['current_role']=="state_person"){
				
				$states = $this->states_model->get_by_id($this->data['user_state_id']);
    }
    else{
				
				$states = $this->states_model->get_all();
    }
    
    $this->data['states'] = $states;
    $this->data['partial'] = 'reports/state_school_assessment';
    $this->data['page_title'] = 'State School Assessment';
    $this->load->view('templates/index', $this->data); 
  }

  function stateSchoolAssessmentReport()
  {
    if(!isset($_POST['start_month']) && !isset($_POST['start_year']))
    {
        redirect('reports/state_school_assessment');
    }
    $state_id = $_POST['state_id'];
    $display_type  = $_POST['display_type'];
    $startmonth  = $_POST['start_month'];
    $startyear = $_POST['start_year'];
    $startdate = '01-'.$startmonth.'-'.$startyear;
    $enddate = date('t-m-Y', strtotime($startyear.'-'.$startmonth.'-01'));

    $this->load->Model('Assessment_model');
    $this->load->Model('Observations_model');
    $this->load->Model('Classes_model');
    $this->load->Model('Subjects_model');
    $this->load->Model('districts_model');

    $state_classes = $this->Subjects_model->check_mapping($state_id);
    
    $classes = array();
    $subject_ids = array();
    $class_subjects = array();
    $c = 0;
    foreach($state_classes as $state_class)
    {
        $class_id = $state_class->class_id;
        $subject_id = $state_class->subject_id;
        if(!in_array($class_id, $classes)){
          array_push($classes, $class_id);
        }
        
        if(!in_array($subject_id, $subject_ids)){
          array_push($subject_ids, $subject_id);
        }
        
        $subjectData = $this->Subjects_model->getById($subject_id);

        $class_subjects[$class_id][$c]['id'] = $subjectData['id'];
        $class_subjects[$class_id][$c]['name'] = $subjectData['name'];
        $c++;
    }
    
    $district_data = $this->districts_model->get_all_district($state_id);
    
    $observationsData = $this->Observations_model->getReportsObservation($state_id, $classes);
    
    $class_observation = array();
    $c = 0;
    foreach($observationsData as $observations)
    {
      $class_observation[$observations->class_id][$c]['id'] = $observations->id;
      $class_observation[$observations->class_id][$c]['question'] = $observations->question;
      $c++;
    }
    
    //echo"<br>".$startdate." >> ".$enddate." >> ".$state_id;
    //print_r($classes);
    $assessment_data = $this->Assessment_model->getStateClassAssessment($startdate, $enddate, $state_id, $classes);
    
    $district_ids = array();
    $resultData = array();
    $school_visit_counts = array();
    $class_subject_question_ans_counts = array();
    //print_r($assessment_data);
    foreach($assessment_data as $assessments)
    {   
        $district_id = $assessments->district_id;
        $class_id = $assessments->class_id;
        $subject_id = $assessments->subject_id;
        $school_id = $assessments->school_id;
          
        if(!isset($school_visit_counts[$district_id])){
          $school_visit_counts[$district_id] = array();
        }
        if(!in_array($school_id, $school_visit_counts[$district_id]))
            array_push($school_visit_counts[$district_id],$school_id);
        
        if(!in_array($district_id, $district_ids))
          array_push($district_ids, $district_id);
        
        $csq_key = $district_id.'_'.$class_id.'_'.$subject_id.'_'.$assessments->question_id;
        
        $ans_value = $assessments->answer;
        
        if(!isset($class_subject_question_ans_counts[$csq_key]['total']))
          $class_subject_question_ans_counts[$csq_key]['total'] = 1;
        else  
          $class_subject_question_ans_counts[$csq_key]['total'] = $class_subject_question_ans_counts[$csq_key]['total']+1;
        
        if($ans_value == 1){
          if(!isset($class_subject_question_ans_counts[$csq_key]['yes']))
            $class_subject_question_ans_counts[$csq_key]['yes'] = 1;
          else  
            $class_subject_question_ans_counts[$csq_key]['yes'] = $class_subject_question_ans_counts[$csq_key]['yes']+1;
        }
    }
    
    $class_names = array();
    if (count($classes) > 0)
    {
      $this->load->model('classes_model');
      $classData = $this->classes_model->getByIds($classes);
      foreach($classData as $class)
      {
        $class_names[$class->id] = $class->name;
      }
    }
    
    $district_data = array();
    if (count($district_ids) > 0)
    {
      $this->load->model('districts_model');
      $district_data = $this->districts_model->getByIds($district_ids);
      
    }
    
    $state_data = $this->states_model->getById($state_id);
    $period = date('M-y', strtotime($startdate));
    
    $this->data['district_names'] = $district_data;
    $this->data['classes'] = $classes;
    $this->data['class_names'] = $class_names;
    $this->data['class_subjects'] = $class_subjects;
    $this->data['class_observation'] = $class_observation;
    $this->data['district_school_visit'] = $school_visit_counts;
    $this->data['question_arr'] = $observationsData;
    $this->data['data_counts'] = $class_subject_question_ans_counts;
    
    $this->data['STATE'] = $state_data['name'];
    $this->data['PERIOD'] = $period;
    $this->data['state_id'] = $state_id;
    $this->data['start_month'] = $startmonth;
    $this->data['start_year'] = $startyear;
    $this->data['display_type'] = $display_type;
    
    $download = (isset($_POST['download']) and $_POST['download'] == 1 ? $_POST['download'] : 0);
    $pdf = (isset($_POST['pdf']) and $_POST['pdf'] == 1 ? $_POST['pdf'] : 0);

		if ($download == 1)
    {
        $this->data['excel'] = 1;
        $myFile = "./uploads/reports/state_school_assessment_report.xls";
        $this->load->library('parser');
        $this->data['download'] = $download;
        //pass retrieved data into template and return as a string
        $stringData = $this->parser->parse('reports/state_school_assessment_report', $this->data, true);

        //open excel and write string into excel
        $fh = fopen($myFile, 'w') or die("can't open file");
        fwrite($fh, $stringData);
        fclose($fh);
        //download excel file
        $this->common_functions->downloadExcel("state_school_assessment_report");
        unlink($myFile);
    }
    else if($pdf == 1)
    {
        //pass retrieved data into template and return as a string
        $this->data['pdf'] = 1;
        $summary = $this->load->view('reports/state_school_assessment_report',$this->data,true);
        $this->common_functions->create_pdf($summary,'state_school_assessment_report');
    }
    else{
      $this->data['partial'] = 'reports/state_school_assessment_report';
      $this->data['page_title'] = 'Spark School Rating';
      $this->load->view('templates/index', $this->data);
    }
  }
  
  function return_state_userids($state_id, $start_date, $end_date)
  {
    $users_data = $this->Users_model->getSparkByStateDuration($state_id, $start_date, $end_date);
    
    $user_ids = array();
    foreach($users_data as $usersdata){
      $user_ids[] = $usersdata['user_id'];
    }
    return $user_ids;  
  } 

  function state_training()
  {
    $month = 0;
    $year = 0;
    $this->data['year'] = $month;
    $this->data['month'] = $year;
    $this->data['partial'] = 'reports/state';
    
    $this->load->Model('states_model');
    $this->load->Model('districts_model');
    
    if($this->data['current_role']=="state_person" || $this->data['current_role']=="manager"){
      $states = $this->states_model->get_by_id($this->data['user_state_id']);
      $this->data['states'] = $states;
      
      $this->load->model('districts_model');
      $districts = $this->districts_model->get_state_districts($this->data['user_state_id']);
      $this->data['districts'] = $districts;
    }
    else{
				
				$states = $this->states_model->get_all();
				$this->data['states'] = $states;
    }

    $this->data['title'] = 'State Productivity Report';
    $this->data['partial'] = 'reports/state_training';
    $this->load->view('templates/index', $this->data);
  }
  
  function stateTrainingReport()
  {
    $state_id = $this->input->post('state_id');
    $inpmonth = $this->input->post('monthno');
    $inpyear = $this->input->post('year');
    
    if(empty($state_id) && empty($inpmonth) && empty($inpyear))
    {
        redirect('reports/state_training');
    }
    
    $inpdate			 = '01/'.$inpmonth.'/'.$inpyear;
    $inpdate 		 = date('t/m/Y', strtotime($inpyear.'-'.$inpmonth.'-01'));

    $this->load->model('states_model');
    $this->load->model('Trainings_model');
    $this->load->model('Users_model');
    $this->load->model('districts_model');
    $this->load->model('blocks_model');

    $state_data = $this->states_model->getById($state_id);

    $start_date = date('Y-m-d', strtotime($inpyear.'-'.$inpmonth.'-01'));
    //$start_date1 = date('Y-m-d', strtotime($inpyear.'-'.($inpmonth-2).'-01'));
    $end_date = date('Y-m-t', strtotime($inpyear.'-'.$inpmonth.'-01'));
    if(strtotime($end_date) > now()){
      $end_date = date('Y-m-d');
    }
    //$users_data = $this->Users_model->getTrainingSparkByState($state_id, $start_date, $end_date);
    $users_data = $this->Users_model->getSparkByStateDuration($state_id, $start_date, $end_date);
    
    $_fromDate_C = date("Y-m-d",strtotime($inpyear."-".$inpmonth."-01"));
    $_toDate_C = date("Y-m-t",strtotime($inpyear."-".$inpmonth."-01"));
    
    $dates = $this->createRange($_fromDate_C, $_toDate_C);
    
    $i = 0;
    $user_names = array();
    $taining_data = array();
    $new_dates = array();
    $t = 0;
    foreach($users_data as $usersdata){
      
      /*$user_id = $usersdata->user_id;
      $login_id = $usersdata->login_id;
      $name = $usersdata->name;*/
      $user_id = $usersdata['user_id'];
      $login_id = $usersdata['login_id'];
      $name = $usersdata['name'];
      //$user_names[]['name'] = $name;
      
      foreach($dates as $activity_date){
        $user_training = $this->Trainings_model->get_fullday_training($user_id, $activity_date, $activity_date);
        if(empty($user_training)){
          $taining_data[$user_id][$activity_date] = 0;
          $new_dates[$activity_date] = (!empty($new_dates[$activity_date]) ? 0+$new_dates[$activity_date] : 0); 
        }
        else{
            $new_dates[$activity_date] = (!empty($new_dates[$activity_date]) ? 1+$new_dates[$activity_date] : 1); 
            $details = array();  
            $statedata = $this->states_model->getById($user_training[0]->state_id);
            $details[] = $statedata['name'];  
            //$details[] = ($user_training[0]->training_duration == '24:00' ? 'Full Day' : 'Half Day');
            if($user_training[0]->district_id != 0){
              $districtdata = $this->districts_model->getById($user_training[0]->district_id);
              $details[] = $districtdata['name'];  
            }
            if($user_training[0]->block_id != 0){
              $blockdata = $this->blocks_model->getById($user_training[0]->block_id);
              $details[] = $blockdata['name'];  
            }
            $taining_data[$user_id][$activity_date] = implode(', ', $details);  
            $t++;
        }
      }
    }
    //echo"<br>count : ".$t;
    //print_r($users_data);
    //print_r($users_data);
    //die;
    $month_name = date('F', mktime(0, 0, 0, $inpmonth, 10));
    
    $this->data['net_count'] = $t;
    $this->data['new_dates'] = $new_dates;
    $this->data['monthno'] = $inpmonth;
    $this->data['year'] = $inpyear;
    $this->data['state_id'] = $state_id;
    $this->data['sparks'] = $users_data;
    $this->data['dates'] = $dates;
    $this->data['training_data'] = $taining_data;
    $this->data['state_name'] = $state_data['name'];
    $this->data['current_month_name'] = $month_name;
    
    $download = (isset($_POST['download']) and $_POST['download'] == 1 ? $_POST['download'] : 0);
    $pdf = (isset($_POST['pdf']) and $_POST['pdf'] == 1 ? $_POST['pdf'] : 0);
    
		if ($download == 1)
    {
      $myFile = "./uploads/reports/state_training_report.xls";
      $this->load->library('parser');
			$this->data['download'] = $download;
      //pass retrieved data into template and return as a string
      $stringData = $this->parser->parse('reports/state_training_report', $this->data, true);
      //open excel and write string into excel
      $fh = fopen($myFile, 'w') or die("can't open file");
      fwrite($fh, $stringData);
      fclose($fh);
      //download excel file
      $this->common_functions->downloadExcel("state_training_report");
      unlink($myFile);
    }
    else if($pdf == 1)
    {
        //pass retrieved data into template and return as a string
        $this->data['pdf'] = 1;
        $summary = $this->load->view('reports/state_training_report',$this->data,true);
        $this->common_functions->create_pdf($summary,'state_training_report');
    }
    else{
      $this->data['partial'] = 'reports/state_training_report';
      $this->data['page_title'] = 'Sparks State Training Report';
      $this->load->view('templates/index', $this->data);
    }
  }
  
  function createRange($start, $end, $format = 'Y-m-d') {
    $start  = new DateTime($start);
    $end    = new DateTime($end);
    $invert = $start > $end;

    $dates = array();
    $dates[] = $start->format($format);
    while ($start != $end) {
        $start->modify(($invert ? '-' : '+') . '1 day');
        $dates[] = $start->format($format);
    }
    return $dates;
  }
  
  function testline_responses()
  {
    $this->load->Model('Test_questions_model');
    
    $district_id = $this->data['user_district_id'];
    $login_id = $this->data['current_user_id'];
    $state_id = $this->data['user_state_id'];
    $role = $this->data['current_role'];
    
    $spark_ids = array();
    $sel_state = '';
    if($this->data['current_role']=="field_user")
		{
      $spark_ids = array($login_id);
			//$response_data = $this->Test_questions_model->get_all_testline_response_data('', $spark_ids);
		}
    else if($this->data['current_role'] == "manager")
		{
      $this->load->Model('Team_members_model');
      $userData = $this->Team_members_model->get_team($login_id);
      if(count($userData) > 0){
        foreach($userData as $users){
          if(!in_array($users->id,$spark_ids)){
            $spark_ids[] = $users->id;
          }
        }
      }
			//$response_data = $this->Test_questions_model->get_all_testline_response_data('', $spark_ids);
		}
		else if($this->data['current_role']=="state_person")
		{ 
      $sel_state = $state_id; 
      //$response_data = $this->Test_questions_model->get_all_testline_response_data($state_id, '');
    }
    
    $questions_data = $this->Test_questions_model->get_all_testline_questions_data($sel_state, $spark_ids);
    $response_data = $this->Test_questions_model->get_all_testline_response_data($sel_state, $spark_ids);
    
    //print_r($response_data);
    $classes = array();
    $classes_subjects = array();
    $classes_question_ids = array();
    $classes_subjects_question_ids = array();
    $classes_subjects_questions = array();
    foreach($questions_data as $questions)
    {
      if(!in_array($questions->class_name, $classes))
        array_push($classes, $questions->class_name);
      
      if(!isset($classes_subjects[$questions->class_name])){
        $classes_subjects[$questions->class_name] = array();
      }
      if(!in_array($questions->subject_name, $classes_subjects[$questions->class_name]))
       array_push($classes_subjects[$questions->class_name],$questions->subject_name);    
       
      $classes_question_ids[$questions->class_name][] = $questions->question_id;    
      
      $classes_subjects_question_ids[$questions->class_name][$questions->subject_name][] = $questions->question_id;    
      $classes_subjects_questions[$questions->class_name][$questions->subject_name][] = $questions->question;    
    }
    
    $this->data['response_data'] = $response_data;
    $this->data['classes'] = $classes;
    $this->data['classes_subjects'] = $classes_subjects;
    $this->data['classes_question_ids'] = $classes_question_ids;
    $this->data['classes_subjects_question_ids'] = $classes_subjects_question_ids;
    
    $download = 1;
    if ($download == 1)
    {
      $myFile = "./uploads/reports/test_question_responses.xls";
      $this->load->library('parser');
			$this->data['download'] = $download;
      //pass retrieved data into template and return as a string
      $stringData = $this->parser->parse('reports/test_question_response_report', $this->data, true);
      //open excel and write string into excel
      $fh = fopen($myFile, 'w') or die("can't open file");
      fwrite($fh, $stringData);
      fclose($fh);
      //download excel file
      $this->common_functions->downloadExcel("test_question_responses");
      unlink($myFile);
    }
    else{
      $this->data['partial'] = 'reports/test_question_response_report';
      $this->data['page_title'] = 'Sparks State Training Report';
      $this->load->view('templates/index', $this->data);
    }
  }
  
  function feedback_results()
  {
    $states = $this->states_model->get_by_id($this->data['user_state_id']);
    $this->data['state_name'] = $states[0]->name;
      
    $this->data['partial'] = 'reports/feedback_360_report';
    $this->data['page_title'] = '360 Feedback Report';
    $this->load->view('templates/index', $this->data);
  
  }
  
  function school_abc_audit_report()
  {
    $this->output->enable_profiler(false);
    $month = date('m');
    $year = date('Y');
    $this->data['start_year'] = $month;
    $this->data['start_month'] = $year;

    if(!empty($_GET))
    {
      if(empty($_GET['start_month']) || empty($_GET['start_year']))
      {
          redirect('reports/observation_rating_report');
      }
      $startmonth       = $_GET['start_month'];	   
      $startyear        = $_GET['start_year'];
      $duration        = $_GET['duration'];
      $value_type        = $_GET['value_type'];
      
      $download = (!empty($_GET['download']) ? 1 : 0); 
      $pdf = (!empty($_GET['pdf']) ? 1 : 0); 
       
      if(!empty($_GET['state_id']) && !empty($_GET['district_id'])  && !empty($_GET['block_id'])  && !empty($_GET['cluster_id']))
      {
        $this->cluster_school_observation_report($startmonth, $startyear, $duration, $value_type, $download, $pdf, $_GET['state_id'], $_GET['district_id'], $_GET['block_id'], $_GET['cluster_id']);
      }
      if(!empty($_GET['state_id']) && !empty($_GET['district_id'])  && !empty($_GET['block_id'])  && empty($_GET['cluster_id']))
      {
        $this->block_school_observation_report($startmonth, $startyear, $duration, $value_type, $download, $pdf, $_GET['state_id'], $_GET['district_id'], $_GET['block_id']);
      }
      if(!empty($_GET['state_id']) && !empty($_GET['district_id'])  && empty($_GET['block_id']) && empty($_GET['cluster_id']))
      {
        $this->district_school_observation_report($startmonth, $startyear, $duration, $value_type, $download, $pdf, $_GET['state_id'], $_GET['district_id']);
      }
      if(!empty($_GET['state_id']) && empty($_GET['district_id'])  && empty($_GET['block_id'])  && empty($_GET['cluster_id']))
      {
        $this->state_school_observation_report($startmonth, $startyear, $duration, $value_type, $download, $pdf, $_GET['state_id']);
      }
      if(empty($_GET['state_id']) && empty($_GET['district_id'])  && empty($_GET['block_id'])  && empty($_GET['cluster_id']))
      {
        redirect('reports/observation_rating_report');
      }
    }
    else{
      
      $states = array();
      if($this->data['current_role']=="field_user"){
        $this->load->Model('Users_model');
        $districts = array();
        $districtData = $this->Users_model->user_districts($this->data['current_user_id']);
        foreach($districtData as $district){
          $districts[$district->id] = $district->name;
        }
        $this->data['districts'] = $districts;
      }
      else if($this->data['current_role']=="state_person" || $this->data['current_role']=="manager"){
          $states = $this->states_model->get_by_id($this->data['user_state_id']);
      }
      else{
          $states = $this->states_model->get_all();
      }
      $this->data['states'] = $states;
      $this->data['partial'] = 'reports/observation_rating_report';
      $this->data['page_title'] = 'Block School Observation';
      $this->load->view('templates/index', $this->data);
    }
  }

  function state_school_observation_report($startmonth, $startyear, $duration, $value_type, $download, $pdf, $state_id)
  {
    $this->load->Model('Observations_model');
    
    $startdate        = '01/'.$startmonth.'/'.$startyear;
	  $enddate        = date('t/m/Y', strtotime($startyear.'-'.$startmonth.'-01'));

	  $results          = $this->getStateInfo($state_id);
	  
    $month_name   = date('F', mktime(0, 0, 0, $startmonth, 10));
    $cm_no      = $startmonth;

		$cm_result = $this->getStateRatingData($cm_no,$startyear,$state_id, $duration);
		
    $cm_ob_result = $this->getStateObservationData($cm_no, $startyear, $state_id, $duration);
    
    $district_observations_arr = $cm_ob_result['district_observations_arr'];
    $user_arr = $cm_ob_result['user_arr'];
    
		$total_district = $this->getDistrictCount($state_id);
		$namedistrict = $this->getDistrictName($state_id);
    
    $observationsData = $this->Observations_model->getReportsObservation($state_id, array(), 0);
    
    $class_observation = array();
    $c = 0;
    foreach($observationsData as $observations)
    {
      $class_observation[$observations->class_id][$c]['id'] = $observations->id;
      $class_observation[$observations->class_id][$c]['question'] = $observations->question;
      $class_observation[$observations->class_id][$c]['weightage'] = $observations->weightage;
      $c++;
    }
     
    $this->data['report_type'] = 'State'; 
    $this->data['record_type'] = 'District'; 
    $this->data['value_type'] = $value_type; 
    $this->data['duration'] = $duration;
    $this->data['user_arr'] = $user_arr;
    $this->data['cm_ob_result'] = $district_observations_arr;
    $this->data['class_observation'] = $class_observation;
    $this->data['cm_result'] = $cm_result;
    $this->data['namerecord'] = $namedistrict;
    $this->data['month_name'] = $month_name;

    $this->data['state_name'] = $results[0]->state_name;
    $this->data['state_id'] = $state_id;
    $this->data['district_id'] = '';
    $this->data['start_month'] = $startmonth;
    $this->data['start_year'] = $startyear;

		if ($download == 1)
    {
      $this->data['excel'] = 1;
      $myFile = "./uploads/reports/school_observation_report.xls";
      $this->load->library('parser');
      $this->data['download'] = $download;
      //pass retrieved data into template and return as a string
      $stringData = $this->parser->parse('reports/state_school_observation_report', $this->data, true);

      //open excel and write string into excel
      $fh = fopen($myFile, 'w') or die("can't open file");
      fwrite($fh, $stringData);
      fclose($fh);
      //download excel file
      $this->common_functions->downloadExcel("school_observation_report");
      unlink($myFile);
    }
    else if($pdf == 1)
    {
        //pass retrieved data into template and return as a string
        $this->data['pdf'] = 1;
        $summary = $this->load->view('reports/state_school_observation_report',$this->data,true);
        $this->common_functions->create_pdf($summary,'state_school_observation_report');
    }
    else{
      $this->data['partial'] = 'reports/state_school_observation_report'; 
      $this->data['page_title'] = 'State School Observation';
      $this->load->view('templates/index', $this->data);
    }
  }
  
  function getStateObservationData($month, $year, $state_id, $duration)
  {
    $this->load->Model('Assessment_model');
    $this->load->Model('Users_model');
    
    $startdate = '01/'.$month.'/'.$year;
    
    if($duration == 'YTD'){
      $sess_month = SESSION_START_MONTH_ABC;
      $sess_year = date('Y');
      if($month < $sess_month || $year < $sess_year){
        $sess_year = date('Y') - 1;
      }
      $startdate        = '01/'.$sess_month.'/'.$sess_year;
    }
    
    $enddate = date('t/m/Y', strtotime($year.'-'.$month.'-01'));
    
    $assessment_data = $this->Assessment_model->getSchoolAssessmentData($startdate, $enddate, $state_id);
    //print_r($assessment_data);
    $district_observations_arr = array();
    $district_school_observations_arr = array();
    $user_ids = array();
    $user_names = array();
    $return_data = array();
    $district_schools = array();
    foreach($assessment_data as $assessments)
    {
      $district_id = $assessments->district_id;
      $question_id = $assessments->question_id;
      $answer = $assessments->answer;
      $school_id = $assessments->school_id;
      
      if(!isset($user_ids[$district_id])){
        $userData = $this->Users_model->getById($assessments->user_id);
        $user_ids[$district_id][] = $assessments->user_id;
        $user_names[$district_id][] = $userData['name'];
      }else{
        if(!in_array($assessments->user_id,$user_ids[$district_id])){
          $userData = $this->Users_model->getById($assessments->user_id);
          array_push($user_ids[$district_id],$assessments->user_id);
          array_push($user_names[$district_id],$userData['name']);
        }
      }
      
      if(!isset($district_observations_arr[$district_id][$school_id][$question_id])){
        $district_observations_arr[$district_id][$school_id][$question_id][] =  $answer;
      }
      else{
        array_push($district_observations_arr[$district_id][$school_id][$question_id], $answer);
      }
    }
    //print_r($district_observations_arr);
    $return_data['district_observations_arr'] = $district_observations_arr;
    $return_data['user_arr'] = $user_names;
    
    return $return_data;
  }
  
  function district_school_observation_report($startmonth, $startyear, $duration, $value_type, $download, $pdf, $state_id, $district_id)
  {
    $startdate    = '01/'.$startmonth.'/'.$startyear;
    $enddate    = date('t/m/Y', strtotime($startyear.'-'.$startmonth.'-01'));

    $this->load->Model('Observations_model');
    $results          = $this->getDistrictInfo($state_id,$district_id);
	  
    $month_name   = date('F', mktime(0, 0, 0, $startmonth, 10));
    $cm_no      = $startmonth;

    $cm_result = $this->getDistrictRatingData($cm_no, $startyear, $state_id, $district_id, $duration);
    
    $observationsData = $this->Observations_model->getReportsObservation($state_id, array(), 0);
    
    $class_observation = array();
    $c = 0;
    foreach($observationsData as $observations)
    {
      $class_observation[$observations->class_id][$c]['id'] = $observations->id;
      $class_observation[$observations->class_id][$c]['question'] = $observations->question;
      $class_observation[$observations->class_id][$c]['weightage'] = $observations->weightage;
      $c++;
    }
    
    $cm_ob_result = $this->getDistrictObservationData($cm_no,$startyear, $state_id, $district_id, $duration);
    
    $block_observations_arr = $cm_ob_result['block_observations_arr'];
    //$user_arr = $cm_ob_result['user_arr'];
    
    $total_block = $this->getBlockCount($state_id,$district_id);
		$nameblock = $this->getBlockName($state_id,$district_id);
    
    $this->data['report_type'] = 'District'; 
    $this->data['record_type'] = 'Block'; 
    $this->data['value_type'] = $value_type;
    $this->data['duration'] = $duration;
    $this->data['class_observation'] = $class_observation;
    $this->data['cm_result'] = $cm_result;
    $this->data['cm_ob_result'] = $block_observations_arr;
    $this->data['namerecord'] = $nameblock;
    $this->data['month_name'] = $month_name;
    $this->data['state_name'] = $results[0]->state_name;
    $this->data['district_name'] = $results[0]->district_name;

    $this->data['state_id'] = $state_id;
    $this->data['district_id'] = $district_id;
    $this->data['block_id'] = '';
    $this->data['start_month'] = $startmonth;
    $this->data['start_year'] = $startyear;

		if ($download == 1)
    {
      $this->data['excel'] = 1;
      $myFile = "./uploads/reports/district_school_observation_report.xls";
      $this->load->library('parser');
      $this->data['download'] = $download;
      //pass retrieved data into template and return as a string
      $stringData = $this->parser->parse('reports/state_school_observation_report', $this->data, true);

      //open excel and write string into excel
      $fh = fopen($myFile, 'w') or die("can't open file");
      fwrite($fh, $stringData);
      fclose($fh);
      //download excel file
      $this->common_functions->downloadExcel("district_school_observation_report");
      unlink($myFile);
    }
    else if($pdf == 1)
    {
        //pass retrieved data into template and return as a string
        $this->data['pdf'] = 1;
        $summary = $this->load->view('reports/state_school_observation_report',$this->data,true);
        $this->common_functions->create_pdf($summary,'district_school_observation_report');
    }
    else{
      $this->data['partial'] = 'reports/state_school_observation_report'; //'reports/state';
      $this->data['page_title'] = 'District School Rating';
      $this->load->view('templates/index', $this->data);
    }
  }

  function getDistrictObservationData($month, $year, $state_id, $district_id, $duration)
  {
    $this->load->Model('Assessment_model');
    $this->load->Model('Users_model');
    
    $startdate = '01/'.$month.'/'.$year;
    if($duration == 'YTD'){
      $sess_month = SESSION_START_MONTH_ABC;
      $sess_year = date('Y');
      if($month < $sess_month || $year < $sess_year){
        $sess_year = date('Y') - 1;
      }
      $startdate        = '01/'.$sess_month.'/'.$sess_year;
    }
    $enddate = date('t/m/Y', strtotime($year.'-'.$month.'-01'));
    
    $assessment_data = $this->Assessment_model->getSchoolAssessmentData($startdate, $enddate, $state_id, $district_id);
    
    $block_observations_arr = array();
    $user_ids = array();
    $user_names = array();
    $return_data = array();
    foreach($assessment_data as $assessments)
    {
      $block_id = $assessments->block_id;
      $question_id = $assessments->question_id;
      $answer = $assessments->answer;
      $school_id = $assessments->school_id;
      
      if(!isset($user_ids[$block_id])){
        $userData = $this->Users_model->getById($assessments->user_id);
        $user_ids[$block_id][] = $assessments->user_id;
        $user_names[$block_id][] = $userData['name'];
      }else{
        if(!in_array($assessments->user_id,$user_ids[$block_id])){
          $userData = $this->Users_model->getById($assessments->user_id);
          array_push($user_ids[$block_id],$assessments->user_id);
          array_push($user_names[$block_id],$userData['name']);
        }
      }
      
      if(!isset($block_observations_arr[$block_id][$school_id][$question_id]))
        $block_observations_arr[$block_id][$school_id][$question_id][] =  $answer;
      else
        array_push($block_observations_arr[$block_id][$school_id][$question_id], $answer);
    }
    
    $return_data['block_observations_arr'] = $block_observations_arr;
    $return_data['user_arr'] = $user_names;
    
    return $return_data;
  }
   
  function block_school_observation_report($startmonth, $startyear, $duration, $value_type, $download, $pdf, $state_id, $district_id, $block_id)
  {
    $startdate        = '01/'.$startmonth.'/'.$startyear;
	  $enddate        = date('t/m/Y', strtotime($startyear.'-'.$startmonth.'-01'));
    
    $month_name   = date('F', mktime(0, 0, 0, $startmonth, 10));
    
    $this->load->Model('Observations_model');
    $this->load->Model('Assessment_model');
    
    $observationsData = $this->Observations_model->getReportsObservation($state_id, array(), 0);
    
    $class_observation = array();
    $c = 0;
    
    foreach($observationsData as $observations)
    {
      $class_observation[$observations->class_id][$c]['id'] = $observations->id;
      $class_observation[$observations->class_id][$c]['question'] = $observations->question;
      $class_observation[$observations->class_id][$c]['weightage'] = $observations->weightage;
      $c++;
    }
    
    $this->load->Model('clusters_model');
    
    $cluster_ids = array();
    $clusterData = $this->clusters_model->get_all_cluster($block_id);
    foreach($clusterData as $cluster)
    {
      $cluster_ids[] = $cluster->id;
    }
    
    $cluster_data = array();
    $cluster_names = array();
    $cm_result = array();
    $cluster_data = $this->clusters_model->getByIds($cluster_ids);
  
    foreach($cluster_data as $cluster)
    {
      $cluster_names[$cluster->id] = $cluster->name;
    }
    
    $cm_result = $this->getBlockRatingData($startmonth, $startyear, $state_id, $district_id, $cluster_ids, $duration);
  
    $cm_ob_result = $this->getBlockObservationData($startmonth, $startyear,$state_id, $district_id, $block_id, $duration);
    
    $cluster_observations_arr = $cm_ob_result['cluster_observations_arr'];
    $user_arr = $cm_ob_result['user_arr'];
    
    $block_data = $this->getBlockInfo($startdate, $enddate, $state_id, $district_id, $block_id);
    
    $this->data['report_type'] = 'Block'; 
    $this->data['record_type'] = 'Cluster'; 
    $this->data['value_type'] = $value_type;
    $this->data['duration'] = $duration;
    $this->data['cluster_names'] = $cluster_names;
    $this->data['cm_result'] = $cm_result;
    $this->data['cm_ob_result'] = $cluster_observations_arr;
    $this->data['class_observation'] = $class_observation;
    
    $this->data['schools_count'] = $block_data[0]->no_of_schools;
    $this->data['block'] = $block_data[0]->blocks;
    $this->data['district'] = $block_data[0]->districts;
    
    $this->data['state_id'] = $state_id;
    $this->data['district_id'] = $district_id;
    $this->data['block_id'] = $block_id;
    $this->data['start_month'] = $startmonth;
    $this->data['start_year'] = $startyear;
    $this->data['month_name'] = $month_name;
    
    //$download = (isset($_POST['download']) and $_POST['download'] == 1 ? $_POST['download'] : 0);
    //$pdf = (isset($_POST['pdf']) and $_POST['pdf'] == 1 ? $_POST['pdf'] : 0);
    
		if ($download == 1)
    {
      $this->data['excel'] = 1;
      $myFile = "./uploads/reports/block_school_observation_report.xls";
      $this->load->library('parser');
      $this->data['download'] = $download;
      //pass retrieved data into template and return as a string
      $stringData = $this->parser->parse('reports/block_school_observation_report', $this->data, true);

      //open excel and write string into excel
      $fh = fopen($myFile, 'w') or die("can't open file");
      fwrite($fh, $stringData);
      fclose($fh);
      //download excel file
      $this->common_functions->downloadExcel("block_school_observation_report");
      unlink($myFile);
    }
    else if($pdf == 1)
    {
        //pass retrieved data into template and return as a string
        $this->data['pdf'] = 1;
        $summary = $this->load->view('reports/block_school_observation_report',$this->data,true);
        $this->common_functions->create_pdf($summary,'block_school_observation_report');
    }
    else{
      $this->data['partial'] = 'reports/block_school_observation_report';
      $this->data['page_title'] = 'Block School Observation';
      $this->data['title'] = 'Block School Observation';
      $this->load->view('templates/index', $this->data);
    }
  }
  
  function getBlockRatingData($cm_no, $startyear, $state_id, $district_id,$clusert_ids, $duration)
  {
    $this->load->Model('Assessment_model');
    
    $assessment_data1 = $this->Assessment_model->getClusterRatingReport($cm_no, $startyear, $state_id, $district_id, $clusert_ids, $duration);
    
    $clusters_arr = array();
    $report_ids = array();
    foreach($assessment_data1 as $assessments)
    {
      $cluster_id = $assessments->cluster_id;
      $report_id = $assessments->id;
      $school_id = $assessments->school_id;
      
      if(!isset($clusters_arr[$cluster_id]))
      {
        $clusters_arr[$cluster_id]['school_id'][] =  $school_id;
        $clusters_arr[$cluster_id]['report_id'][] =  $report_id;
      }
        if(!in_array($school_id, $clusters_arr[$cluster_id]['school_id']))
          array_push($clusters_arr[$cluster_id]['school_id'], $school_id);
      
        if(!in_array($report_id, $clusters_arr[$cluster_id]['report_id']))
          array_push($clusters_arr[$cluster_id]['report_id'], $report_id);
      
    }
    
    $rating_data = array();
    foreach($clusters_arr as $cluster_id=>$data)
    {
      $school_data = $data['school_id'];
      $school_count = count($data['school_id']); //CHANGE SCHOOL COUNT
      //$school_count = count($data['report_id']);
      
      $report_id = $data['report_id'];
      
      $ratings_A = 0;
      $ratings_B = 0;
      $ratings_C = 0;
      $rating_count = 0;
      $ratingData = $this->Assessment_model->getSparkRatingCount($report_id);
      foreach($ratingData as $ratings){
        if($ratings['subject_rating'] == 'A'){
          $ratings_A = $ratings['Total'];
        }
        if($ratings['subject_rating'] == 'B'){
          $ratings_B = $ratings['Total'];
        }
        if($ratings['subject_rating'] == 'C'){
          $ratings_C = $ratings['Total'];
        }
        $rating_count = $ratings_A + $ratings_B + $ratings_C;
      }
      $rating_data[$cluster_id]['school_count'] = $school_count;
      $rating_data[$cluster_id]['rating_A'] = ($ratings_A > 0 ? ($ratings_A/$rating_count)*100 : 0);
      $rating_data[$cluster_id]['rating_B'] = ($ratings_B > 0 ? ($ratings_B/$rating_count)*100 : 0);
      $rating_data[$cluster_id]['rating_C'] = ($ratings_C > 0 ? ($ratings_C/$rating_count)*100 : 0);
    }
    return $rating_data;
  }

  function getBlockObservationData($month, $year, $state_id, $district_id, $block_id, $duration)
  {
    $this->load->Model('Assessment_model');
    $this->load->Model('Users_model');
    
    $startdate = '01/'.$month.'/'.$year;
    if($duration == 'YTD'){
      $sess_month = SESSION_START_MONTH_ABC;
      $sess_year = date('Y');
      if($month < $sess_month || $year < $sess_year){
        $sess_year = date('Y') - 1;
      }
      $startdate        = '01/'.$sess_month.'/'.$sess_year;
    }
    $enddate = date('t/m/Y', strtotime($year.'-'.$month.'-01'));
    
    $assessment_data = $this->Assessment_model->getSchoolAssessmentData($startdate, $enddate, $state_id, $district_id, $block_id);
    
    $cluster_observations_arr = array();
    $user_ids = array();
    $user_names = array();
    $return_data = array();
    foreach($assessment_data as $assessments)
    {
      $cluster_id = $assessments->cluster_id;
      $question_id = $assessments->question_id;
      $answer = $assessments->answer;
      $school_id = $assessments->school_id;
      
      if(!isset($user_ids[$cluster_id])){
        $userData = $this->Users_model->getById($assessments->user_id);
        $user_ids[$cluster_id][] = $assessments->user_id;
        $user_names[$cluster_id][] = $userData['name'];
      }else{
        if(!in_array($assessments->user_id,$user_ids[$cluster_id])){
          $userData = $this->Users_model->getById($assessments->user_id);
          array_push($user_ids[$cluster_id],$assessments->user_id);
          array_push($user_names[$cluster_id],$userData['name']);
        }
      }
      
      if(!isset($cluster_observations_arr[$cluster_id][$school_id][$question_id]))
        $cluster_observations_arr[$cluster_id][$school_id][$question_id][] =  $answer;
      else
        array_push($cluster_observations_arr[$cluster_id][$school_id][$question_id], $answer);
    }
    $return_data['cluster_observations_arr'] = $cluster_observations_arr;
    $return_data['user_arr'] = $user_names;
    
    return $return_data;
  }
  
  function cluster_school_observation_report($startmonth, $startyear, $duration, $value_type, $download, $pdf, $state_id, $district_id, $block_id, $cluster_id)
  {
    $startdate        = '01/'.$startmonth.'/'.$startyear;
	  $enddate        = date('t/m/Y', strtotime($startyear.'-'.$startmonth.'-01'));
    
    $this->load->Model('Observations_model');
    $this->load->Model('Assessment_model');
    
    $month_name   = date('F', mktime(0, 0, 0, $startmonth, 10));
    
    $observationsData = $this->Observations_model->getReportsObservation($state_id, array(), 0);
    
    $class_observation = array();
    $c = 0;
    
    foreach($observationsData as $observations)
    {
      $class_observation[$observations->class_id][$c]['id'] = $observations->id;
      $class_observation[$observations->class_id][$c]['question'] = $observations->question;
      $class_observation[$observations->class_id][$c]['weightage'] = $observations->weightage;
      $c++;
    }
    
    $cm_result = $this->Assessment_model->getSchoolRatingReport($startmonth, $startyear, $state_id, $district_id, $cluster_id, $duration);
    //print_r($cm_result);
    
    $cm_ob_result = $this->getClusterObservationData($startmonth, $startyear,$state_id, $district_id, $block_id, $cluster_id, $duration);
    //print_r($cm_ob_result);
    
    $school_observations_arr = $cm_ob_result['school_observations_arr'];
    
    $block_data = $this->getBlockInfo($startdate, $enddate, $state_id, $district_id, $block_id);
    
    $this->load->Model('clusters_model');
    $cluster_data = $this->clusters_model->getById($cluster_id);
    
    $this->data['value_type'] = $value_type;
    $this->data['duration'] = $duration;
    $this->data['cm_result'] = $cm_result;
    $this->data['cm_ob_result'] = $school_observations_arr;
    $this->data['class_observation'] = $class_observation;
    
    $this->data['schools_count'] = count($cm_result);
    $this->data['block'] = $block_data[0]->blocks;
    $this->data['district'] = $block_data[0]->districts;
    $this->data['cluster_name'] = $cluster_data['name'];
    
    $this->data['month_name'] = $month_name;
    $this->data['state_id'] = $state_id;
    $this->data['district_id'] = $district_id;
    $this->data['block_id'] = $block_id;
    $this->data['cluster_id'] = $cluster_id;
    $this->data['start_month'] = $startmonth;
    $this->data['start_year'] = $startyear;
    
		if ($download == 1)
    {
      $this->data['excel'] = 1;
      $myFile = "./uploads/reports/cluster_school_observation_report.xls";
      $this->load->library('parser');
      $this->data['download'] = $download;
      //pass retrieved data into template and return as a string
      $stringData = $this->parser->parse('reports/cluster_school_observation_report', $this->data, true);

      //open excel and write string into excel
      $fh = fopen($myFile, 'w') or die("can't open file");
      fwrite($fh, $stringData);
      fclose($fh);
      //download excel file
      $this->common_functions->downloadExcel("cluster_school_observation_report");
      unlink($myFile);
    }
    else if($pdf == 1)
    {
        //pass retrieved data into template and return as a string
        $this->data['pdf'] = 1;
        $summary = $this->load->view('reports/cluster_school_observation_report',$this->data,true);
        $this->common_functions->create_pdf($summary,'cluster_school_observation_report');
    }
    else{
      $this->data['partial'] = 'reports/cluster_school_observation_report';
      $this->data['page_title'] = 'Cluster School Observation';
      $this->data['title'] = 'Cluster School Observation';
      $this->load->view('templates/index', $this->data);
    }
  }
  
  function getClusterObservationData($month, $year, $state_id, $district_id, $block_id, $cluster_id, $duration)
  {
    $this->load->Model('Assessment_model');
    $this->load->Model('Users_model');
    
    $startdate = '01/'.$month.'/'.$year;
    
    if($duration == 'YTD'){
      $sess_month = SESSION_START_MONTH_ABC;
      $sess_year = date('Y');
      if($month < $sess_month || $year < $sess_year){
        $sess_year = date('Y') - 1;
      }
      $startdate        = '01/'.$sess_month.'/'.$sess_year;
    }
    
    /*if($duration == 'YTD' || $year < $sess_year){
      $sess_month = SESSION_START_MONTH;
      $sess_year = date('Y');
      if($month < $sess_month){
        $sess_year = date('Y') - 1;
      }
      $startdate        = '01/'.$sess_month.'/'.$sess_year;
    }*/
    $enddate = date('t/m/Y', strtotime($year.'-'.$month.'-01'));
    
    $assessment_data = $this->Assessment_model->getSchoolAssessmentData($startdate, $enddate, $state_id, $district_id, $block_id, $cluster_id);
    
    $school_observations_arr = array();
    $return_data = array();
    foreach($assessment_data as $assessments)
    {
      $school_id = $assessments->school_id;
      $question_id = $assessments->question_id;
      $answer = $assessments->answer;
      
      if(!isset($school_observations_arr[$school_id][$question_id]))
        $school_observations_arr[$school_id][$question_id][] =  $answer;
      else
        array_push($school_observations_arr[$school_id][$question_id], $answer);
    }
    $return_data['school_observations_arr'] = $school_observations_arr;
    
    return $return_data;
  }
  
  function survey_report()
	{
		$this->load->Model('survey_model');
		
		if(!empty($_POST) && $_POST['submit'] == 'Get Test Data')
    {
			if($_POST['activitydatestart'] != '' && $_POST['activitydateend'] != ''){
				$start_date = date('Y-m-d', strtotime($_POST['activitydatestart']));
				$end_date = date('Y-m-d', strtotime($_POST['activitydateend']));
				
				if($this->data['current_role'] == 'state_person'){
					$state_id = (!empty($_POST['state_id']) ? $_POST['state_id'] : $this->data['user_state_id']);
				}
				else{
					$state_id = (!empty($_POST['state_id']) ? $_POST['state_id'] : '');
				}
				$survey_id = (!empty($_POST['survey_id']) ? $_POST['survey_id'] : '');
				
				$surveyTests = $this->survey_model->get_survey_tests($start_date, $end_date, $state_id, $survey_id);
				
				$surveyQuestions = $this->survey_model->get_survey_questions(array($survey_id), 'sort_order');
				
				$survey_ids = array();
				$spark_ids = array();
				$district_ids = array();
				$block_ids = array();
				$cluster_ids = array();
				$school_ids = array();
				$answer_data = array();
				foreach($surveyTests as $surveyTest)
				{
					if(!in_array($surveyTest->survey_id, $survey_ids))
						array_push($survey_ids, $surveyTest->survey_id);
					if(!in_array($surveyTest->spark_id, $spark_ids))
						array_push($spark_ids, $surveyTest->spark_id);
					if(!in_array($surveyTest->district_id, $district_ids))
						array_push($district_ids, $surveyTest->district_id);
					if(!in_array($surveyTest->block_id, $block_ids))
						array_push($block_ids, $surveyTest->block_id);
					if(!in_array($surveyTest->cluster_id, $cluster_ids))
						array_push($cluster_ids, $surveyTest->cluster_id);
					if(!in_array($surveyTest->school_id, $school_ids))
						array_push($school_ids, $surveyTest->school_id);
						
					$survey_answers = $this->survey_model->get_survey_test_answers($surveyTest->id);
					
					$ques_ans = array();
					foreach($survey_answers as $ac_data)
					{
						$question_id = $ac_data->question_id;
						$question = $ac_data->question;
						$question_type = $ac_data->question_type;
						
						if($question_type == 'subjective'){
							$answer = $ac_data->answers;
						}
						else if($question_type == 'objective'){
							if($ac_data->answers != ''){
								$optionData = $this->survey_model->getOptionById($ac_data->answers);
								$answer = (!empty($optionData) ? $optionData[0]->option_value : '');
							}
							else{
								$answer = 'N/A';
							}
						}
						else if($question_type == 'multichoice'){
							if(!empty($ac_data->answers)){
								$multi_ans = json_decode($ac_data->answers);
								$ans_arr = array();
								foreach($multi_ans as $multians){
									$optionData = $this->survey_model->getOptionById($multians);
									$ans_arr[] = (!empty($optionData) ? $optionData[0]->option_value : '');
								}
								//$answer = json_encode($ans_arr, JSON_UNESCAPED_UNICODE);		
								$answer = implode(',', $ans_arr);		
							}
							else{
								$answer = 'N/A';
							}
						}
						else if($question_type == 'yes-no'){
							if($ac_data->answers == 0)
								$answer = 'No';
							else
								$answer = 'Yes';
						}
						else if($question_type == 'yes-no-na'){
							if($ac_data->answers == 0)
								$answer = 'N/A';
							else if($ac_data->answers == 1)
								$answer = 'Yes';
							else if($ac_data->answers == 2)
								$answer = 'No';  
						}
						else{
							$answer = $ac_data->answers;
						}
						
						$ques_ans[$question_id] = $answer;
					}
					//$answer_data[$surveyTest->id] = json_encode($ques_ans, JSON_UNESCAPED_UNICODE);		
					$answer_data[$surveyTest->id] = $ques_ans;		
				}
				
				$survey_names = array();
				if (count($survey_ids) > 0){
					$surveyData = $this->survey_model->getByIds($survey_ids);
					foreach($surveyData as $survey){
						$survey_names[$survey->id] = $survey->title;
					}
				}
				
				$spark_names = array();
				if (count($spark_ids) > 0){
					$this->load->model('Users_model');  
					$users = $this->Users_model->getByIds($spark_ids);
					foreach($users as $user){
						$spark_names[$user->id] = $user->name;
					}
				}

				$district_names = array();
				if (count($district_ids) > 0){
					$this->load->model('districts_model');
					$districts = $this->districts_model->getByIds($district_ids);
					foreach($districts as $district){
						$district_names[$district->id] = $district->name;
					}
				}

				$block_names = array();
				if (count($block_ids) > 0){
					$this->load->model('blocks_model');
					$blocks = $this->blocks_model->getByIds($block_ids);
					foreach($blocks as $block){
						$block_names[$block->id] = $block->name;
					}
				}

				$cluster_names = array();
				if (count($cluster_ids) > 0){
					$this->load->model('clusters_model');
					$clusters = $this->clusters_model->getByIds($cluster_ids);
					foreach($clusters as $cluster){
						$cluster_names[$cluster->id] = $cluster->name;
					}
				}
		 
				$school_names = array();
				$dise_codes = array();
				if (count($school_ids) > 0){
					$this->load->model('schools_model');
					$schools = $this->schools_model->getByIds($school_ids);
					foreach($schools as $school){
						$school_names[$school->id] = $school->name;
						$dise_codes[$school->id] = $school->dise_code;
					}
				}
				
				/*$this->data['answer_data'] = $answer_data;
				$this->data['surveyTests'] = $surveyTests;
				$this->data['survey_names'] = $survey_names;
				$this->data['spark_names'] = $spark_names;
				$this->data['district_names'] = $district_names;
				$this->data['block_names'] = $block_names;	
				$this->data['cluster_names'] = $cluster_names;
				$this->data['school_names'] = $school_names;*/
				
				$download = 1;
				if($download == 1){
				
				$surveyData = array();
				$surveyData[0] = array('S.No.', 'Survey', 'Spark', 'District', 'Block', 'Cluster', 'DISE Code', 'School', 'GPS Location', 'Latitude', 'Longitude' );
				foreach($surveyQuestions as $surveyQuestion)
				{
					array_push($surveyData[0], $surveyQuestion->question);
				}
					
				//print_r($answer_data);
				$i = 1;
				foreach ($surveyTests as $e_key){ 
					
					$survey_name = $survey_names[$e_key->survey_id];
					$spark_name = (isset($spark_names[$e_key->spark_id]) ? $spark_names[$e_key->spark_id] : $e_key->spark_id);
					$district_name = (!empty($e_key->district_id) ? $district_names[$e_key->district_id] : '-');
					$block_name = (!empty($e_key->block_id) ? $block_names[$e_key->block_id] : '-');
					$cluster_name = (!empty($e_key->cluster_id) ? $cluster_names[$e_key->cluster_id] : '-');
					$dise_code = (!empty($e_key->school_id) ? $dise_codes[$e_key->school_id] : '-');
					$school_name = (!empty($e_key->school_id) ? $school_names[$e_key->school_id] : '-');
					$location = (!empty($e_key->location) ? $e_key->location : '-');
					$latitude = (!empty($e_key->latitude) ? $e_key->latitude : '-');
					$longitude = (!empty($e_key->longitude) ? $e_key->longitude : '-');
					//$ques_ans = $answer_data[$e_key->id];
						
					$surveyData[$i] = array($i, $survey_name, $spark_name, $district_name, $block_name, $cluster_name, $dise_code, $school_name, $location, $latitude, $longitude);
					foreach($surveyQuestions as $surveyQuestion)
					{
						if(!empty($answer_data[$e_key->id][$surveyQuestion->id]))
							array_push($surveyData[$i], $answer_data[$e_key->id][$surveyQuestion->id]);
						else	
							array_push($surveyData[$i], 'N/A');
					}
					$i++; 
				}	
				//print_r($surveyData);	
				
				$this->writeCSV($surveyData, "survey_data");	
				
				//$this->array_to_csv_download($surveyData, "survey_data.csv", ";");	
				/*$myFile = "./uploads/reports/test_data.xls";
				$this->load->library('parser');
				$this->data['download'] = $download;
				//pass retrieved data into template and return as a string
				$stringData = $this->parser->parse('admin/survey/test_data', $this->data, true);
				//open excel and write string into excel
				$fh = fopen($myFile, 'w') or die("can't open file");
				fwrite($fh, $stringData);
				fclose($fh);
				//download excel file
				$this->common_functions->downloadExcel("test_data");
				unlink($myFile);*/
				}
				else{
					$custom_scripts = ['map.js']; 
					$this->data['custom_scripts'] = $custom_scripts; 
					$this->data['partial'] = 'admin/survey/test_data';
					$this->load->view('templates/index', $this->data);
				}
			}
			else{
				redirect('admin/survey/test_data');
			}
		}	
		else{
			
			$cur_state_id = '';
			if($this->data['current_role'] == 'state_person'){
				$cur_state_id = $this->data['user_state_id'];
			}
			$custom_scripts = ['map.js']; 
			$this->data['surveys'] = $this->survey_model->get_all(1);
			$this->data['states'] = $this->states_model->get_all_states($cur_state_id);
			$this->data['custom_scripts'] = $custom_scripts; 
			$this->data['partial'] = 'admin/survey/test_data';
			$this->load->view('templates/index', $this->data);	
		}
	}
	
	function writeCSV($data, $filename) {   

			//Use tab as field separator
			$newTab  = "\t";
			$newLine  = "\n";

			//$fputcsv  =  count($headings) ? '"'. implode('"'.$newTab.'"', $headings).'"'.$newLine : '';
			$fputcsv = "";
			// Loop over the * to export
			if (! empty($data)) {
				foreach($data as $item) {
					$fputcsv .= '"'. implode('"'.$newTab.'"', $item).'"'.$newLine;
				}
			}
			
			//Convert CSV to UTF-16
			$encoded_csv = mb_convert_encoding($fputcsv, 'UTF-16LE', 'UTF-8');

			// Output CSV-specific headers
			header('Set-Cookie: fileDownload=true; path=/'); //This cookie is needed in order to trigger the success window.
			header("Pragma: public");
			header("Expires: 0");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Cache-Control: private",false);
			header("Content-Type: application/octet-stream");
			header("Content-Disposition: attachment; filename=\"$filename.csv\";" );
			header("Content-Transfer-Encoding: binary");
			header('Content-Length: '. strlen($encoded_csv));
			echo chr(255) . chr(254) . $encoded_csv; //php array convert to csv/excel

			exit;
	}

	
	function array_to_csv_download($array, $filename = "survey_data.csv", $delimiter=";") {
    // open raw memory as file so no temp files needed, you might run out of memory though
			$f = fopen('php://memory', 'w'); 
			// loop over the input array
			foreach ($array as $line) { 
					// generate csv lines from the inner arrays
					fputcsv($f, $line, $delimiter); 
			}
			// reset the file pointer to the start of the file
			fseek($f, 0);
			//$encoded_csv = mb_convert_encoding($f, 'UTF-16LE', 'UTF-8');
			// tell the browser it's going to be a csv file
			header("Cache-Control: public"); 
			header("Content-Type: application/octet-stream");
			//header('Content-Type: application/csv');
			//header('Content-Type: application/vnd.ms-excel; charset=utf-8');
			// tell the browser we want to save it instead of displaying it
			//header('Content-Type:application/csv; charset=utf-8'); 
			header('Content-Disposition: attachment; filename="'.$filename.'";');
			// make php send the generated csv lines to the browser
			//echo "\xEF\xBB\xBF"; // UTF-8 BOM
			fprintf( $f, "\xEF\xBB\xBF");
			fpassthru($f);
			//header('Content-Length: '. strlen($encoded_csv));
			//echo chr(255) . chr(254) . $encoded_csv; //php array convert to csv/excel
			//exit;
	}
	
	//Reports FOR SSS APP USAGE
	function sss_app_usage()
  {
    $this->output->enable_profiler(false);
    $month = date('m');
    $year = date('Y');
    $this->data['start_year'] =  $year;
    $this->data['start_month'] = $month;

    if(!empty($_GET))
    {
      if(empty($_GET['start_month']) || empty($_GET['start_year']))
      {
          redirect('reports/sss_analytics_report');
      }
      $startmonth       = $_GET['start_month'];	   
      $startyear        = $_GET['start_year'];
      //$duration        = $_GET['duration'];
      
      $download = (!empty($_GET['download']) ? $_GET['download'] : 0); 
      $pdf = (!empty($_GET['pdf']) ? $_GET['pdf'] : 0); 
       
      if(!empty($_GET['cluster_id']))
      {
        $this->get_cluster_analytics_report('cluster', $startmonth, $startyear, $download, $pdf, $_GET['state_id'], $_GET['district_id'], $_GET['block_id'], $_GET['cluster_id']);
      }
      else if(!empty($_GET['state_id']) && !empty($_GET['district_id'])  && !empty($_GET['block_id']))
      {
        $this->get_analytics_report('block', $startmonth, $startyear, $download, $pdf, $_GET['state_id'], $_GET['district_id'], $_GET['block_id']);
      }
      else if(!empty($_GET['state_id']) && !empty($_GET['district_id'])  && empty($_GET['block_id']))
      {
        $this->get_analytics_report('district', $startmonth, $startyear,$download, $pdf, $_GET['state_id'], $_GET['district_id']);
      }
      else if(!empty($_GET['state_id']) && empty($_GET['district_id'])  && empty($_GET['block_id']))
      {
        $this->get_analytics_report('state', $startmonth, $startyear, $download, $pdf, $_GET['state_id']);
      }
      else if(empty($_GET['state_id']) && empty($_GET['district_id'])  && empty($_GET['block_id']))
      {
        redirect('reports/sss_app_usage');
      }
    }
    else{
      $this->load->Model('states_model');
      if($this->data['current_role']=="state_person" || $this->data['current_role']=="manager" || $this->data['current_role']=="field_user"){
          $states = $this->states_model->get_by_id($this->data['user_state_id']);
      }
      else{
          $states = $this->states_model->get_all();
      }
      $this->data['states'] = $states;
      $this->data['partial'] = 'reports/analytics_report';
      $this->data['page_title'] = 'Block School Observation';
      $this->load->view('templates/index', $this->data);
    }
  }

  function get_analytics_report($report_type, $startmonth, $startyear, $download, $pdf, $state_id, $district_id='', $block_id='', $cluster_id='')
  {
    //$sess_month = SESSION_START_MONTH;
    $sess_month = '04';
    $sess_year = date('Y');
    if($startmonth < $sess_month){
      $sess_year = date('Y') - 1;
    }
    
    $session_start_date = $sess_year."-".$sess_month."-01";
    
    $startdate  =  $startyear."-".$startmonth."-01";
	  $enddate    = date('Y-m-t', strtotime($startyear.'-'.$startmonth.'-01'));
	  $end_date   = date('t-m-Y', strtotime($startyear.'-'.$startmonth.'-01'));
    
    $month_name = date('F', mktime(0, 0, 0, $startmonth, 10));
    
    $month_year = date('m_Y', strtotime($startdate));
    
    $this->data['startdate'] = $startdate;
    $this->data['enddate'] = $end_date;
    $this->data['start_month'] = $startmonth;
    $this->data['start_year'] = $startyear;
    $this->data['month_name'] = $month_name;
    
    $this->data['state_id'] = $state_id;
    $this->data['district_id'] = $district_id;
    $this->data['block_id'] = $block_id;
    $this->data['cluster_id'] = '';
    
    $this->load->Model('Dashboard_model');
    $this->load->Model('schools_model');
    
    $stateData = $this->schools_model->get_data('states', array('id'=>$state_id));
    $state_name = $stateData[0]->name;
    $this->data['state_name'] = $state_name;
    
    if(!empty($district_id)){
			$districtData = $this->schools_model->get_data('districts', array('id'=>$district_id));
			$district_name = $districtData[0]->name;
			$this->data['district_name'] = $district_name;
		}
    
    if(!empty($block_id)){
			$blockData = $this->schools_model->get_data('blocks', array('id'=>$block_id));
			$block_name = $blockData[0]->name;
			$this->data['block_name'] = $block_name;
		}
    
    $sparkData = $this->Dashboard_model->getAllSparkByStateDuration($state_id, $startdate, $enddate, array('field_user'));
    
    $spark_ids = array();
    foreach($sparkData as $sparks)
    {
        if(!in_array($sparks->user_id, $spark_ids))
          array_push($spark_ids, $sparks->user_id);
    }
    
    $user_ids = array();
    $district_ids = array();
    $block_ids = array();
    $cluster_ids = array();
    $school_ids = array();
    $data_arr = array();
    $type_values = array();
    $schoolData = array();
    
    if($report_type == 'block'){
      $this->load->Model('clusters_model');
      if(!empty($block_id))
        $data_arr = $this->clusters_model->get_all_cluster($block_id);
      
				foreach($data_arr as $cluster)
				{
					$cluster_ids[] = $cluster->id;
				}
			
			$schoolData = $this->schools_model->get_total_school_count(array(),array(),array(), $cluster_ids);	
			
			$column_name = 'Cluster';
			$select_field = 'cluster_id';
			$select_cond = 'block_id';
			$select_value = $block_id;
    }
    else if($report_type == 'district'){
      $this->load->Model('blocks_model');
      if(!empty($district_id))
        $data_arr = $this->blocks_model->get_all_block($district_id);
      
				foreach($data_arr as $block)
				{
					$block_ids[] = $block->id;
				}

			$schoolData = $this->schools_model->get_total_school_count(array(), array(), $block_ids);		
			
			$column_name = 'Block';
			$select_field = 'block_id';
			$select_cond = 'district_id';
			$select_value = $district_id;
    }
    else if($report_type == 'state'){
      $this->load->Model('districts_model');
      if(!empty($state_id))
        $data_arr = $this->districts_model->get_all_district($state_id);
      
				foreach($data_arr as $district)
				{
					$district_ids[] = $district->id;
				}

			$schoolData = $this->schools_model->get_total_school_count(array(), $district_ids);		
			
			$column_name = 'District';
			$select_field = 'district_id';
			$select_cond = 'state_id';
			$select_value = $state_id;
    }
    $this->data['column_name'] = $column_name;
    
    //$ackData = $this->schools_model->get_school_ack_delivery($select_field, $select_cond, $select_value);
		//$appActiveTeacher = $this->schools_model->get_active_teachers($select_field, $select_cond, $select_value, $month_year);
		
		$year_date_range = array($session_start_date, $enddate);
		
		$month_date_range = array($startdate, $enddate);
		
		$month_range = $this->return_months($session_start_date, $enddate);
		
		$month_array = array();
		foreach($month_range as $monthrange)
		{
			$m_yr = explode('-',$monthrange);
			$month_array[] = $m_yr[1]."_".$m_yr[0];
		}
		
		$appUsage = $this->schools_model->get_school_app_usage($select_field, $select_cond, $select_value, array($month_year), 'govt teacher');
		$appGreater_1_hr = $this->schools_model->get_teacher_gr_1hr($select_field, $select_cond, $select_value, array($month_year), 'govt teacher');
		$appSchoolRegister = $this->schools_model->get_app_school_registered('diseCode', $select_field, $select_cond, $select_value, $month_date_range, 'govt teacher');
		$appTeacherRegister = $this->schools_model->get_app_teacher_registered($select_field, $select_cond, $select_value, $month_date_range, 'govt teacher');
		$netTeacherRegister = $this->schools_model->get_app_teacher_registered($select_field, $select_cond, $select_value, $year_date_range, 'govt teacher');
    //$schoolNotRegister = $this->schools_model->get_app_school_not_registered($select_field, $select_cond, $select_value, $month_date_range);
		
    
		$appUsage_yt = $this->schools_model->get_school_app_usage($select_field, $select_cond, $select_value, $month_array, 'govt teacher');
		$appGreater_1_hr_yt = $this->schools_model->get_teacher_gr_1hr($select_field, $select_cond, $select_value, $month_array, 'govt teacher');
		$appSchoolRegister_yt = $this->schools_model->get_app_school_registered('diseCode', $select_field, $select_cond, $select_value, $year_date_range, 'govt teacher');
		$appTeacherRegister_yt = $this->schools_model->get_app_teacher_registered($select_field, $select_cond, $select_value, $year_date_range, 'govt teacher');
    $schoolRegister_yt = $this->schools_model->get_app_school_not_registered($select_field, $select_cond, $select_value, $year_date_range);
    
    //print_r($appUsage);
    //print_r($appActiveTeacher);
		
		$total_items = array();
		$total_video_hr = array();
    $total_greater_1_hr = array();
    $school_registered = array();
    $total_registers = array();
		$net_registers = array();
		
		$month_values = array();
		foreach($appUsage as $data){
			$month_values['total_items'][$data->field_id] = $data->total_item;
		}
		
		foreach($appGreater_1_hr as $data){
			$month_values['total_greater_1_hr'][$data->field_id] = $data->Total_teacher;
		}
    
    foreach($appSchoolRegister as $data){
			$month_values['app_school_registered'][$data->field_id] = $data->TOTAL_SCHOOLS;
		}
		
    /*foreach($schoolNotRegister as $data){
			$month_values['not_registered'][$data->field_id] = $data->UNREGISTER_SCHOOLS;
		}*/
		
		foreach($appTeacherRegister as $data){
			$month_values['total_registers'][$data->field_id] = $data->TOTAL_SCHOOLS;
		}
		
		/*foreach($netTeacherRegister as $data){
			$month_values['net_registers'][$data->field_id] = $data->TOTAL_SCHOOLS;
		}*/
    
		$year_values = array();
		foreach($appUsage_yt as $data){
			$year_values['total_items'][$data->field_id] = $data->total_item;
		}
		
		foreach($appGreater_1_hr_yt as $data){
			$year_values['total_greater_1_hr'][$data->field_id] = $data->Total_teacher;
		}
    
    foreach($appSchoolRegister_yt as $data){
			$year_values['app_school_registered'][$data->field_id] = $data->TOTAL_SCHOOLS;
		}
		
		foreach($schoolRegister_yt as $data){
			$year_values['school_registered'][$data->field_id] = $data->REGISTER_SCHOOLS;
		}
		
		foreach($appTeacherRegister_yt as $data){
			$year_values['total_registers'][$data->field_id] = $data->TOTAL_SCHOOLS;
		}
		
		foreach($netTeacherRegister as $data){
			$year_values['net_registers'][$data->field_id] = $data->TOTAL_SCHOOLS;
		}
    
    //print_r($month_values);
    /*$total_users = array();
    foreach($ackData as $ack_data)
    {
			//$total_users[$ack_data->field_id] = $ack_data->TOTAL_USER;
		}
		$total_active_teacher = array();
    foreach($appActiveTeacher as $app_data)
    {
			$total_active_teacher[$app_data->field_id] = $app_data->Total_teacher;
		}*/
    
    if(!empty($data_arr)){
      foreach($data_arr as $data){
        $type_values[$data->id] = $data->name;
      }
      if(!empty($meet_with[0]))
        $type_values[0] = 'Others';
    }
    
    $this->load->Model('Users_model');
    
    $user_names = array();
    if(!empty($user_ids)){
      $user_data = $this->Users_model->getByIds($user_ids);
      foreach($user_data as $data){
        $user_names[$data->id] = $data->name;
      }
    }
    
    $school_data = array();
    foreach($schoolData as $schools)
    {
			$school_data[$schools->field_id] = $schools->TOTAL_RECORD;
		}
    
    $this->data['report_type'] = $report_type;
    $this->data['type_values'] = $type_values;
    $this->data['school_data'] = $school_data;
    $this->data['month_values'] = $month_values;
    $this->data['year_values'] = $year_values;
    
    if ($download == 1)
    {
      $myFile = "./uploads/reports/sss_app_usage.xls";
      $this->load->library('parser');
			$this->data['download'] = $download;
      //pass retrieved data into template and return as a string
      $stringData = $this->parser->parse('reports/analytics_report_data', $this->data, true);
      //open excel and write string into excel
      $fh = fopen($myFile, 'w') or die("can't open file");
      fwrite($fh, $stringData);
      fclose($fh);
      //download excel file
      $this->common_functions->downloadExcel("sss_app_usage");
      unlink($myFile);
    }
    else if($pdf == 1)
    {
        //pass retrieved data into template and return as a string
        $this->data['pdf'] = 1;
        $summary = $this->load->view('reports/analytics_report_data',$this->data,true);
        $this->common_functions->create_pdf($summary,'sss_app_usage');
    }
    else{
      $this->data['partial'] = 'reports/analytics_report_data';
      $this->data['page_title'] = 'SSS App Usage';
      $this->load->view('templates/index', $this->data);
    }
    
  }
  
  function get_cluster_analytics_report($report_type, $startmonth, $startyear, $download, $pdf, $state_id='', $district_id='', $block_id='', $cluster_id='')
  {
		$this->data['state_id'] = $state_id;
    $this->data['district_id'] = $district_id;
    $this->data['block_id'] = $block_id;
		$this->data['cluster_id'] = $cluster_id;
    
    $this->load->Model('Dashboard_model');
    $this->load->Model('schools_model');
    
    $stateData = $this->schools_model->get_data('states', array('id'=>$state_id));
    $state_name = $stateData[0]->name;
    $this->data['state_name'] = $state_name;
    
    $districtData = $this->schools_model->get_data('districts', array('id'=>$district_id));
    $district_name = $districtData[0]->name;
    $this->data['district_name'] = $district_name;
    
    $blockData = $this->schools_model->get_data('blocks', array('id'=>$block_id));
    $block_name = $blockData[0]->name;
    $this->data['block_name'] = $block_name;
    
    $clusterData = $this->schools_model->get_data('clusters', array('id'=>$cluster_id));
    $cluster_name = $clusterData[0]->name;
    $this->data['cluster_name'] = $cluster_name;
    
    $startdate  = $startyear."-".$startmonth."-01";
	  $enddate    = date('Y-m-t', strtotime($startyear.'-'.$startmonth.'-01'));
	  $end_date   = date('t-m-Y', strtotime($startyear.'-'.$startmonth.'-01'));
    $month_name = date('F', mktime(0, 0, 0, $startmonth, 10));
    $month_year = date('m_Y', strtotime($startdate));
    
    $startdate = date('Y-m-d', strtotime($startdate));
    $enddate = date('Y-m-d', strtotime($end_date));
    $this->data['start_month'] = $startmonth;
    $this->data['start_year'] = $startyear;
    $this->data['month_name'] = $month_name;
    $this->data['cluster_id'] = $cluster_id;
    
    $appData = $this->schools_model->get_app_registered($startdate, $enddate, $cluster_id, 'govt teacher');
     
    //$appData = $this->schools_model->get_app_usage($month_year, $cluster_id, 'govt teacher');
    //$appSparkData = $this->schools_model->get_app_usage_sparks($month_year, $cluster_id, 'govt teacher');
		
		$this->data['appData'] = $appData;
		//$this->data['appSparkData'] = $appSparkData; 
    
    if ($download == 1)
    {
      $myFile = "./uploads/reports/sss_app_usage.xls";
      $this->load->library('parser');
			$this->data['download'] = $download;
      //pass retrieved data into template and return as a string
      $stringData = $this->parser->parse('reports/analytics_cluster_data', $this->data, true);
      //open excel and write string into excel
      $fh = fopen($myFile, 'w') or die("can't open file");
      fwrite($fh, $stringData);
      fclose($fh);
      //download excel file
      $this->common_functions->downloadExcel("sss_app_usage");
      unlink($myFile);
    }
    else if($pdf == 1)
    {
        //pass retrieved data into template and return as a string
        $this->data['pdf'] = 1;
        $summary = $this->load->view('reports/analytics_cluster_data',$this->data,true);
        $this->common_functions->create_pdf($summary,'sss_app_usage');
    }
    else{
      $this->data['partial'] = 'reports/analytics_cluster_data';
      $this->data['page_title'] = 'SSS App Usage';
      $this->load->view('templates/index', $this->data);
    }
			
	}
  
  function get_register_teachers_list()
  {
		$reporttype = $_POST['reporttype'];
		$record_id = $_POST['record_id'];
		$startmonth = $_POST['start_month'];
		$startyear = $_POST['start_year'];
    
    $this->load->Model('schools_model');
    
    $this->data['report_type'] = $reporttype;
		
		if($reporttype == 'District'){
			$districtData = $this->schools_model->get_data('districts', array('id'=>$record_id));
			$district_name = $districtData[0]->name;
			$this->data['report_name'] = $district_name;
			$field_name = 'district_id';
		}
    else if($reporttype == 'Block'){
			$blockData = $this->schools_model->get_data('blocks', array('id'=>$record_id));
			$block_name = $blockData[0]->name;
			$this->data['report_name'] = $block_name;
			$field_name = 'block_id';
		}
    
    $startdate  = $startyear."-".$startmonth."-01";
	  $enddate    = date('Y-m-t', strtotime($startyear.'-'.$startmonth.'-01'));
	  $end_date   = date('t-m-Y', strtotime($startyear.'-'.$startmonth.'-01'));
    $month_name = date('F', mktime(0, 0, 0, $startmonth, 10));
    $month_year = date('m_Y', strtotime($startdate));
    
    $startdate = date('Y-m-d', strtotime($startdate));
    $enddate = date('Y-m-d', strtotime($end_date));
    $this->data['start_month'] = $startmonth;
    $this->data['start_year'] = $startyear;
    $this->data['month_name'] = $month_name;
    
    $appData = $this->schools_model->get_app_registered_teachers($startdate, $enddate, $field_name, $record_id, 'govt teacher');
    
    //print_r($appData);
    
    $this->data['appData'] = $appData;
		
		$download = 1;
    
    if ($download == 1)
    {
      $myFile = "./uploads/reports/sss_app_register_teachers.xls";
      $this->load->library('parser');
			$this->data['download'] = $download;
      //pass retrieved data into template and return as a string
      $stringData = $this->parser->parse('reports/sss_app_register_teachers', $this->data, true);
      //open excel and write string into excel
      $fh = fopen($myFile, 'w') or die("can't open file");
      fwrite($fh, $stringData);
      fclose($fh);
      //download excel file
      $this->common_functions->downloadExcel("sss_app_register_teachers");
      unlink($myFile);
      exit;
    }
    else if($pdf == 1)
    {
        //pass retrieved data into template and return as a string
        $this->data['pdf'] = 1;
        $summary = $this->load->view('reports/sss_app_register_teachers',$this->data,true);
        $this->common_functions->create_pdf($summary,'sss_app_register_teachers');
    }
    else{
      $this->data['partial'] = 'reports/analytics_cluster_data';
      $this->data['page_title'] = 'SSS App Usage';
      $this->load->view('templates/index', $this->data);
    }
			
	}
  
  
  function get_cluster_analytics_report_bk($report_type, $startmonth, $startyear, $download, $pdf, $state_id='', $district_id='', $block_id='', $cluster_id='')
  {
		$this->data['state_id'] = $state_id;
    $this->data['district_id'] = $district_id;
    $this->data['block_id'] = $block_id;
		$this->data['cluster_id'] = $cluster_id;
    
    $this->load->Model('Dashboard_model');
    $this->load->Model('schools_model');
    
    $stateData = $this->schools_model->get_data('states', array('id'=>$state_id));
    $state_name = $stateData[0]->name;
    $this->data['state_name'] = $state_name;
    
    $districtData = $this->schools_model->get_data('districts', array('id'=>$district_id));
    $district_name = $districtData[0]->name;
    $this->data['district_name'] = $district_name;
    
    $blockData = $this->schools_model->get_data('blocks', array('id'=>$block_id));
    $block_name = $blockData[0]->name;
    $this->data['block_name'] = $block_name;
    
    $clusterData = $this->schools_model->get_data('clusters', array('id'=>$cluster_id));
    $cluster_name = $clusterData[0]->name;
    $this->data['cluster_name'] = $cluster_name;
    
    $startdate  = $startyear."-".$startmonth."-01";
	  $enddate    = date('Y-m-t', strtotime($startyear.'-'.$startmonth.'-01'));
	  $end_date   = date('t-m-Y', strtotime($startyear.'-'.$startmonth.'-01'));
    $month_name = date('F', mktime(0, 0, 0, $startmonth, 10));
    $month_year = date('m_Y', strtotime($startdate));
    
    $this->data['startdate'] = $startdate;
    $this->data['enddate'] = $end_date;
    $this->data['start_month'] = $startmonth;
    $this->data['start_year'] = $startyear;
    $this->data['month_name'] = $month_name;
    $this->data['cluster_id'] = $cluster_id;
    
    $appData = $this->schools_model->get_app_usage($month_year, $cluster_id, 'govt teacher');
    
    $appSparkData = $this->schools_model->get_app_usage_sparks($month_year, $cluster_id, 'govt teacher');
		
		$this->data['appData'] = $appData;
		$this->data['appSparkData'] = $appSparkData;
    
    if ($download == 1)
    {
      $myFile = "./uploads/reports/sss_app_usage.xls";
      $this->load->library('parser');
			$this->data['download'] = $download;
      //pass retrieved data into template and return as a string
      $stringData = $this->parser->parse('reports/analytics_cluster_data', $this->data, true);
      //open excel and write string into excel
      $fh = fopen($myFile, 'w') or die("can't open file");
      fwrite($fh, $stringData);
      fclose($fh);
      //download excel file
      $this->common_functions->downloadExcel("sss_app_usage");
      unlink($myFile);
    }
    else if($pdf == 1)
    {
        //pass retrieved data into template and return as a string
        $this->data['pdf'] = 1;
        $summary = $this->load->view('reports/analytics_cluster_data',$this->data,true);
        $this->common_functions->create_pdf($summary,'sss_app_usage');
    }
    else{
      $this->data['partial'] = 'reports/analytics_cluster_data';
      $this->data['page_title'] = 'SSS App Usage';
      $this->load->view('templates/index', $this->data);
    }
			
	}
  
    
  //REPORTS FOR NOT REGISTERED SCHOOL
  function teachers_not_registered()
  {
    $this->output->enable_profiler(false);
    
    if(!empty($_GET))
    {
      $download = (!empty($_GET['download']) ? $_GET['download'] : 0); 
      $pdf = (!empty($_GET['pdf']) ? $_GET['pdf'] : 0); 
       
      if(empty($_GET['state_id']) && empty($_GET['district_id'])  && empty($_GET['block_id']) && empty($_GET['cluster_id']))
      {
        redirect('reports/school_not_registered');
      } 
      else if(!empty($_GET['cluster_id']))
      {
        $this->get_cluster_not_registered_report('cluster', $download, $pdf, $_GET['state_id'], $_GET['district_id'], $_GET['block_id'], $_GET['cluster_id']);
      }
      else 
      {
				if(!empty($_GET['block_id']))
					$report_type = 'block';
				else if(!empty($_GET['district_id']))
					$report_type = 'district';
				else 
					$report_type = 'state';
        $this->get_school_not_registered_report($report_type, $download, $pdf, $_GET['state_id'], $_GET['district_id'], $_GET['block_id']);
      }
    }
    else{
      $this->load->Model('states_model');
      if($this->data['current_role']=="state_person" || $this->data['current_role']=="manager" || $this->data['current_role']=="field_user"){
          $states = $this->states_model->get_by_id($this->data['user_state_id']);
      }
      else{
          $states = $this->states_model->get_all();
      }
      $this->data['states'] = $states;
      $this->data['partial'] = 'reports/school_not_registered';
      $this->data['page_title'] = 'School Report';
      $this->load->view('templates/index', $this->data);
    }
  }

  function get_school_not_registered_report($report_type, $download, $pdf, $state_id='', $district_id='', $block_id='')
  {
    $this->data['state_id'] = $state_id;
    $this->data['district_id'] = $district_id;
    $this->data['block_id'] = $block_id;
    $this->data['report_type'] = $report_type;
    $this->data['cluster_id'] = '';
    
    $this->load->Model('schools_model');
    
    $stateData = $this->schools_model->get_data('states', array('id'=>$state_id));
    $state_name = $stateData[0]->name;
    $this->data['state_name'] = $state_name;
    
    if(!empty($district_id)){
			$districtData = $this->schools_model->get_data('districts', array('id'=>$district_id));
			$district_name = $districtData[0]->name;
			$this->data['district_name'] = $district_name;
		}
    
    if(!empty($block_id)){
			$blockData = $this->schools_model->get_data('blocks', array('id'=>$block_id));
			$block_name = $blockData[0]->name;
			$this->data['block_name'] = $block_name;
		}
    
    $district_ids = array();
    $block_ids = array();
    $cluster_ids = array();
    $school_ids = array();
    $data_arr = array();
    $type_values = array();
    $schoolData = array();
    
    if($report_type == 'block'){
      $this->load->Model('clusters_model');
      if(!empty($block_id))
        $data_arr = $this->clusters_model->get_all_cluster($block_id);
      
				foreach($data_arr as $cluster)
				{
					$cluster_ids[] = $cluster->id;
				}
			
			$column_name = 'Cluster';
			$select_field = 'cluster_id';
			$select_cond = 'block_id';
			$select_value = $block_id;
    }
    else if($report_type == 'district'){
      $this->load->Model('blocks_model');
      if(!empty($district_id))
        $data_arr = $this->blocks_model->get_all_block($district_id);
      
				foreach($data_arr as $block)
				{
					$block_ids[] = $block->id;
				}

			$column_name = 'Block';
			$select_field = 'block_id';
			$select_cond = 'district_id';
			$select_value = $district_id;
    }
    else if($report_type == 'state'){
      $this->load->Model('districts_model');
      if(!empty($state_id))
        $data_arr = $this->districts_model->get_all_district($state_id);
      
				foreach($data_arr as $district)
				{
					$district_ids[] = $district->id;
				}
			$column_name = 'District';
			$select_field = 'district_id';
			$select_cond = 'state_id';
			$select_value = $state_id;
    }
    $this->data['column_name'] = $column_name;
    
    if(!empty($data_arr)){
      foreach($data_arr as $data){
        $type_values[$data->id] = $data->name;
      }
      if(!empty($meet_with[0]))
        $type_values[0] = 'Others';
    }
    
    $schoolData = $this->schools_model->get_report_school_count('', $district_ids, $block_ids, $cluster_ids);		
    $school_data = array();
    foreach($schoolData as $schools)
    {
			$school_data[$schools->field_id] = $schools->TOTAL_RECORD;
		}
		
    $callData = $this->schools_model->get_report_school_count('call_status', $district_ids, $block_ids, $cluster_ids);		
		$call_data = array();
    foreach($callData as $calls)
    {
			$call_data[$calls->field_id] = $calls->TOTAL_RECORD;
		}
			
    $appSchoolRegister = $this->schools_model->get_app_school_registered('phone_number', $select_field, $select_cond, $select_value);
    $school_registered = array();
    foreach($appSchoolRegister as $schoolRegister)
    {
			$school_registered[$schoolRegister->field_id] = $schoolRegister->TOTAL_SCHOOLS;
		}
		
		/*$ackSchoolRegister = $this->schools_model->get_ack_school_registered('phone_number', $select_field, $select_cond, $select_value);
    $ack_registered = array();
    foreach($ackSchoolRegister as $schoolRegister)
    {
			$ack_registered[$schoolRegister->field_id] = $schoolRegister->TOTAL_SCHOOLS;
		}*/
		
		$ackSchoolNotRegister = $this->schools_model->get_ack_school_not_registered('contact_number', $select_field, $select_cond, $select_value);
    $ack_not_registered = array();
    foreach($ackSchoolNotRegister as $schoolRegister)
    {
			$ack_not_registered[$schoolRegister->field_id] = $schoolRegister->TOTAL_SCHOOLS;
		}
		
		$lastUpdate = $this->schools_model->get_last_teacher_regristerd();
    $this->data['last_update_date'] = $lastUpdate['LAST_UPDATE'];
		
		asort($type_values);
    $this->data['type_values'] = $type_values;
    $this->data['school_data'] = $school_data;
    $this->data['school_registered'] = $school_registered;
    $this->data['ack_not_registered'] = $ack_not_registered;
    $this->data['call_data'] = $call_data;
    
    if ($download == 1)
    {
      $myFile = "./uploads/reports/teachers_not_registered.xls";
      $this->load->library('parser');
			$this->data['download'] = $download;
      //pass retrieved data into template and return as a string
      $stringData = $this->parser->parse('reports/school_not_registered_report', $this->data, true);
      //open excel and write string into excel
      $fh = fopen($myFile, 'w') or die("can't open file");
      fwrite($fh, $stringData);
      fclose($fh);
      //download excel file
      $this->common_functions->downloadExcel("teachers_not_registered");
      unlink($myFile);
    }
    else if($pdf == 1)
    {
        //pass retrieved data into template and return as a string
        $this->data['pdf'] = 1;
        $summary = $this->load->view('reports/school_not_registered_report',$this->data,true);
        $this->common_functions->create_pdf($summary,'teacher_not_registered');
    }
    else{
      $this->data['partial'] = 'reports/school_not_registered_report';
      $this->data['page_title'] = 'Meeting Data';
      $this->load->view('templates/index', $this->data);
    }
    
  }
  
					 
  function get_cluster_not_registered_report($report_type, $download, $pdf, $state_id='', $district_id='', $block_id='', $cluster_id='')
  {
		$this->data['state_id'] = $state_id;
    $this->data['district_id'] = $district_id;
    $this->data['block_id'] = $block_id;
		$this->data['cluster_id'] = $cluster_id;
    
    $this->load->Model('schools_model');
    
    $stateData = $this->schools_model->get_data('states', array('id'=>$state_id));
    $state_name = $stateData[0]->name;
    $this->data['state_name'] = $state_name;
    
    $districtData = $this->schools_model->get_data('districts', array('id'=>$district_id));
    $district_name = $districtData[0]->name;
    $this->data['district_name'] = $district_name;
    
    $blockData = $this->schools_model->get_data('blocks', array('id'=>$block_id));
    $block_name = $blockData[0]->name;
    $this->data['block_name'] = $block_name;
    
    $clusterData = $this->schools_model->get_data('clusters', array('id'=>$cluster_id));
    $cluster_name = $clusterData[0]->name;
    $this->data['cluster_name'] = $cluster_name;
    
    $appData = $this->schools_model->get_not_register_schools($cluster_id);
		
		$this->data['appData'] = $appData;
		
    if ($download == 1)
    {
      $myFile = "./uploads/reports/teacher_not_registered.xls";
      $this->load->library('parser');
			$this->data['download'] = $download;
      //pass retrieved data into template and return as a string
      $stringData = $this->parser->parse('reports/cluster_not_register_schools', $this->data, true);
      //open excel and write string into excel
      $fh = fopen($myFile, 'w') or die("can't open file");
      fwrite($fh, $stringData);
      fclose($fh);
      //download excel file
      $this->common_functions->downloadExcel("teacher_not_registered");
      unlink($myFile);
    }
    else if($pdf == 1)
    {
        //pass retrieved data into template and return as a string
        $this->data['pdf'] = 1;
        $summary = $this->load->view('reports/cluster_not_register_schools',$this->data,true);
        $this->common_functions->create_pdf($summary,'teacher_not_registered');
    }
    else{
      $this->data['partial'] = 'reports/cluster_not_register_schools';
      $this->data['page_title'] = 'Not Register Teachers';
      $this->load->view('templates/index', $this->data);
    }
			
	}
	
	function get_not_register_school_list()
  {
		$reporttype = $_POST['reporttype'];
		$record_id = $_POST['record_id'];
    
    $this->load->Model('schools_model');
    
    $this->data['report_type'] = $reporttype;
		
		if($reporttype == 'District'){
			$districtData = $this->schools_model->get_data('districts', array('id'=>$record_id));
			$district_name = $districtData[0]->name;
			$this->data['report_name'] = $district_name;
			$field_name = 'district_id';
		}
    else if($reporttype == 'Block'){
			$blockData = $this->schools_model->get_data('blocks', array('id'=>$record_id));
			$block_name = $blockData[0]->name;
			$this->data['report_name'] = $block_name;
			$field_name = 'block_id';
		}
    
    $appData = $this->schools_model->get_not_registered_schools($field_name, $record_id);
    
    //print_r($appData);
    
    $this->data['appData'] = $appData;
		
		$download = 1;
    
    if ($download == 1)
    {
      $myFile = "./uploads/reports/not_register_schools.xls";
      $this->load->library('parser');
			$this->data['download'] = $download;
      //pass retrieved data into template and return as a string
      $stringData = $this->parser->parse('reports/not_register_schools', $this->data, true);
      //open excel and write string into excel
      $fh = fopen($myFile, 'w') or die("can't open file");
      fwrite($fh, $stringData);
      fclose($fh);
      //download excel file
      $this->common_functions->downloadExcel("not_register_schools");
      unlink($myFile);
      exit;
    }
    else if($pdf == 1)
    {
        //pass retrieved data into template and return as a string
        $this->data['pdf'] = 1;
        $summary = $this->load->view('reports/not_register_schools',$this->data,true);
        $this->common_functions->create_pdf($summary,'not_register_schools');
    }
    else{
      $this->data['partial'] = 'reports/not_register_schools';
      $this->data['page_title'] = 'SSS App Usage';
      $this->load->view('templates/index', $this->data);
    }
			
	}
  
  function get_not_register_teacher_list()
  {
		$reporttype = $_GET['rtype'];
		$record_id = $_GET['rid'];
    
    $this->load->Model('schools_model');
    
    $this->data['report_type'] = $reporttype;
		
		if($reporttype == 'District'){
			$districtData = $this->schools_model->get_data('districts', array('id'=>$record_id));
			$district_name = $districtData[0]->name;
			$this->data['report_name'] = $district_name;
			$field_name = 'district_id';
		}
    else if($reporttype == 'Block'){
			$blockData = $this->schools_model->get_data('blocks', array('id'=>$record_id));
			$block_name = $blockData[0]->name;
			$this->data['report_name'] = $block_name;
			$field_name = 'block_id';
		}
    
    $appData = $this->schools_model->get_teacher_not_registered($field_name, $record_id);
    
    //print_r($appData);
    
    $this->data['appData'] = $appData;
		
		$download = 1;
		$pdf = 2;
    
    if ($download == 1)
    {
      $myFile = "./uploads/reports/not_register_teachers.xls";
      $this->load->library('parser');
			$this->data['download'] = $download;
      //pass retrieved data into template and return as a string
      $stringData = $this->parser->parse('reports/not_register_teachers', $this->data, true);
      //open excel and write string into excel
      $fh = fopen($myFile, 'w') or die("can't open file");
      fwrite($fh, $stringData);
      fclose($fh);
      //download excel file
      $this->common_functions->downloadExcel("not_register_teachers");
      unlink($myFile);
      exit;
    }
    else if($pdf == 1)
    {
        //pass retrieved data into template and return as a string
        $this->data['pdf'] = 1;
        $summary = $this->load->view('reports/not_register_teachers',$this->data,true);
        $this->common_functions->create_pdf($summary,'not_register_teachers');
    }
    else{
      $this->data['partial'] = 'reports/not_register_teachers';
      $this->data['page_title'] = 'SSS App Usage';
      $this->load->view('templates/index', $this->data);
    }
			
	}
  
	
	public function save_school_status()
  {
		$this->load->Model('schools_model');
    $rid = $_POST['rid'];
    $current_status = $_POST['current_status'];
    $phone_change = $_POST['phone_change'];
    
    if($current_status == 'Number Changed'){
			$check_other_record = $this->schools_model->get_school_teachers(array('id !='=>$rid, 'contact_number'=>$phone_change));
			if(!empty($check_other_record)){
				$teacher_name = $check_other_record[0]->teacher_name;
				$dise_code = $check_other_record[0]->dise_code;
				echo 'Number already exist for teacher '.$teacher_name.' with disecode '.$dise_code;
				exit;
			}
			else{
				$mdata['contact_number'] = $phone_change;
				
				//CHECK APP REGISTERED WITH NEW NUMBER OR NOT
				$chkRegistration = $this->schools_model->get_data('app_registrations', array('phone_number'=>$phone_change));
				$mdata['map_flag'] = (!empty($chkRegistration) ? 1 : 0);
			}
		}
		else if($current_status == 'Promoted')
		{
			$mdata['map_flag'] = 2;
		}
    
    $recordData = $this->schools_model->get_school_teachers(array('id'=>$rid));
		
		$last_status_count = $recordData[0]->last_status_count+1;
		$mdata['last_status'] = $current_status;
		$mdata['last_status_count'] = $last_status_count;
		$mdata['updated_on'] = date('Y-m-d H:i:s');
    $mdata['updated_by'] = $this->data['current_user_id'];
    $res = $this->schools_model->update_school_teachers($mdata, $rid);
    echo $current_status." (".$last_status_count.")";
  }


  
}
