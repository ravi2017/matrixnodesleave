<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Person_details extends CI_Controller {

	function __construct()	
	{
		parent::__construct();
		$this->load->helper('url');
		$this->data['current_page'] = 'person_details';
		$this->load->model('person_details_model');
		$this->load->library('form_validation');
		$this->load->library('common_functions');
		$this->load->helper('date');
    if($this->session->userdata('logged_in'))
		{
      $session_data = $this->session->userdata('logged_in');
      $this->data['current_user_id'] = $session_data['id'];
      $this->data['current_username'] = $session_data['username'];
      $this->data['current_name'] = $session_data['name'];
      $this->data['current_role'] = $session_data['role'];
      $this->data['user_state_id'] = $session_data['state_id'];
      $this->data['user_district_id'] = $session_data['district_id'];
      
    }
    else
    {
      //If no session, redirect to login page
      redirect('login', 'refresh');
    }
    

	}
  

  function index()
  {
		if($this->data['current_role']=="field_user"){
				
				$states = $this->states_model->get_by_id($this->data['user_state_id']);
				$this->data['states'] = $states;
				$districts = new District();
				$districts->where("id",$this->data['user_district_id'])->get();
				$this->data['districts'] = $districts;
				$blocks = new Block();
				$blocks->where("district_id",$this->data['user_district_id'])->get();
				$this->data['blocks'] = $blocks;
			}else{
				
				$states = $this->states_model->get_all();
				$this->data['states'] = $states;
				
				
			}
	
    $this->data['partial'] = 'person_details/index';
    $this->data['page_title'] = 'person_details';
    $this->load->view('templates/index', $this->data);    
  }
  
	function add()
  {
		
   //echo"<pre>";
	 //print_r($_POST);exit();
    
    
   //echo"<pre>";
	 //print_r($_POST);exit();
    $mdata['type']= $this->input->post('type');
   
    $mdata['state_id'] = $this->input->post('state_id');
    $mdata['district_id'] = $this->input->post('district_id');
    $mdata['block_id'] = $this->input->post('block_id');
    $mdata['cluster_id'] = $this->input->post('cluster_id');
    $mdata['school_id'] = $this->input->post('school_id');
    $mdata['person_name'] = $this->input->post('person_name');
    $mdata['contact_number'] = $this->input->post('contact_number');
    
   //echo"<pre>";
	 //print_r($mdata);exit();
    $this->person_details_model->add($mdata);
        
    $this->session->set_flashdata('alert', 'person data submit succefully');
    redirect('person_details', 'refresh');    
  }
  
  
}
