<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cron extends CI_Controller {

  function __construct()
  {
   parent::__construct();
    $this->load->model('states_model');
    $this->load->model('Telegrams_model');
    $this->load->model('users_model');
    $this->load->library('Common_functions');

   /*if ($this->input->is_cli_request() == 1)
   {
     
   }
   else
   {
     redirect('login', 'refresh');
   }*/
  }

  function index()
  {
   echo "sasdasd";
  }

  function send_telegaram()
  {
    $this->load->Model('States_model');
    
    $stateData = $this->States_model->get_all();
    foreach($stateData as $states)
    {
      $state_code = $states->name;
      $this->send_message($states->name);
    }
  }

  function send_message($state)
  {
    $data = $this->get_state_activity_new($state);
    //echo"<pre>";print_r($data);
    //die;
    $apiToken = TELEGRAM_TOKEN;
    //$messageArr = $data['telegram_message'];
    $state_data = $data['state_data'];
    $school_visit_ids = $data['school_visit_ids'];
    $meeting_ids = $data['meeting_ids'];
    $training_ids = $data['training_ids'];
    
    $group_code = array();
    $telegramGroups = $this->Telegrams_model->getGroupByStateId();
    foreach($telegramGroups as $telegramGroups)
    {
      $group_code[$telegramGroups->id] = $telegramGroups->group_id;
    }
    
    foreach($state_data as $key=>$statedata){
      
      foreach($statedata as $state_id=>$messages){
        foreach($messages as $key1=>$message){
            //echo"<pre>state_id : ".$state_id." >> StateGroup : ".$group_code[$state_id]." >> ".$message;
            
            $group_id = $group_code[$state_id];
            
            $postData = array('chat_id' => '-'.$group_id, 'parse_mode'=>'MARKDOWN','text' => $message);
            //$apiToken = "878987885:AAHxJiQ5rq7D-xxyi2e8H2_o1zElNhCOnRs";
            //$postData = array('chat_id' => '-381289775', 'parse_mode'=>'MARKDOWN','text' => $message);

            $url = "https://api.telegram.org/bot$apiToken/sendMessage";

            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
            $response = curl_exec($ch);
            $result_array = json_decode($response);
            curl_close($ch);

            //print_r($result_array);

            if(isset($result_array->ok) && $result_array->ok == 1){
              //echo 'Success';
              $updateData['telegram_flag'] = 1;
              $this->load->model('School_visits_model');
              if(!empty($school_visit_ids[$key][$key1]))
                $this->School_visits_model->update_table_info('school_visits', $updateData, $school_visit_ids[$key][$key1]);
              if(!empty($meeting_ids[$key][$key1]))  
                $this->School_visits_model->update_table_info('meetings', $updateData, $meeting_ids[$key][$key1]);
              if(!empty($training_ids[$key][$key1]))
                $this->School_visits_model->update_table_info('trainings', $updateData, $training_ids[$key][$key1]);
            }
            else{
              //echo 'Error';
            }
        }
      }
    }
  } 

  function get_state_activity_new($state)
  {
    $this->load->model('School_visits_model');
    $this->load->model('Meetings_model');
    $this->load->model('Trainings_model');
    $this->load->model('Users_model');
    $this->load->model('Tracking_model');
    $this->load->model('Spark_feedback_model');
    $this->load->Model('States_model');
    
    $state_data = $this->States_model->getByName($state);
    $state_id = $state_data['id'];
    
    $sparkData = $this->Users_model->getTelegramSparks($state_id, array('field_user','manager'), 0);
    //echo"<br>".$state_id." >> ".count($sparkData);
    $i = 0;
    $finalData = array();
    $state_data = array();
    $data = array();
    $visit_id = array();  
    $meeting_id = array();
    $training_id = array();
    foreach($sparkData as $sparks)
    {
      $spark_id = $sparks->user_id;
        
      $school_visit_data = $this->School_visits_model->get_spark_telegram_count($spark_id);
      $visit_count = count($school_visit_data);
      
      $meeting_data = $this->Meetings_model->get_spark_telegram_count($spark_id);
      $meeting_count = count($meeting_data);
      
      $training_data = $this->Trainings_model->get_spark_telegram_count($spark_id);
      $training_count = count($training_data);
      
      if($visit_count > 0 || $meeting_count > 0 || $training_count > 0){
        
        $visit_ids = array();  
        $meeting_ids = array();
        $training_ids = array();
        
        $visitData = array();  
        $meetingData = array();
        $trainingData = array();
        
        $state_ids = array();
        $district_ids = array();
        $block_ids = array();
        $cluster_ids = array();
        $school_ids = array();
        $subject_ids = array();
    
        $dateArr = array();

        if($visit_count > 0){
          //$message .= "\n".'School Visit Count :'.$visit_count;
          foreach($school_visit_data as $school_visits){
            if(!in_array($school_visits->id, $visit_ids)){
              $visit_ids[] = $school_visits->id;
              
              $activity_date = date('d-m-Y', strtotime($school_visits->activity_date));
              $dateArr[$activity_date]['visit_ids'][] = $school_visits->id;
              
              if(!in_array($school_visits->state_id, $state_ids))
                  array_push($state_ids, $school_visits->state_id);
              if(!in_array($school_visits->district_id, $district_ids))  
                  array_push($district_ids,$school_visits->district_id);
              if(!in_array($school_visits->block_id, $block_ids))
                  array_push($block_ids,$school_visits->block_id);
              if(!in_array($school_visits->cluster_id, $cluster_ids))  
                  array_push($cluster_ids,$school_visits->cluster_id);
              if(!in_array($school_visits->school_id, $school_ids))
                  array_push($school_ids,$school_visits->school_id);
              
              $visitData[$school_visits->id]['activity_date'] = $school_visits->activity_date;  
              $visitData[$school_visits->id]['state_id'] = $school_visits->state_id;  
              $visitData[$school_visits->id]['district_id'] = $school_visits->district_id;  
              $visitData[$school_visits->id]['block_id'] = $school_visits->block_id;  
              $visitData[$school_visits->id]['cluster_id'] = $school_visits->cluster_id;  
              $visitData[$school_visits->id]['school_id'] = $school_visits->school_id;  
            }
          }
        }
        
        if($meeting_count > 0){
          //$message .= "\n".'Meeting Count : '.$meeting_count;
          foreach($meeting_data as $meetings){
            if(!in_array($meetings->id, $meeting_ids))
              $meeting_ids[] = $meetings->id;
              
              $activity_date = date('d-m-Y', strtotime($meetings->activity_date));
              $dateArr[$activity_date]['meeting_ids'][] = $meetings->id;
              
              if(!in_array($meetings->state_id, $state_ids))
                  array_push($state_ids, $meetings->state_id);
              if(!in_array($meetings->district_id, $district_ids))  
                  array_push($district_ids,$meetings->district_id);
              if(!in_array($meetings->block_id, $block_ids))
                  array_push($block_ids,$meetings->block_id);
              if(!in_array($meetings->cluster_id, $cluster_ids))  
                  array_push($cluster_ids,$meetings->cluster_id);
                  
              $meetingData[$meetings->id]['activity_date'] = $meetings->activity_date;  
              $meetingData[$meetings->id]['state_id'] = $meetings->state_id;  
              $meetingData[$meetings->id]['district_id'] = $meetings->district_id;  
              $meetingData[$meetings->id]['block_id'] = $meetings->block_id;  
              $meetingData[$meetings->id]['cluster_id'] = $meetings->cluster_id;
              $meetingData[$meetings->id]['meet_with'] = $meetings->meet_with;
          }
        }
        
        if($training_count > 0){
          //$message .= "\n".'Training Count : '.$training_count;
          foreach($training_data as $trainings){
            if(!in_array($trainings->id, $training_ids))
              $training_ids[] = $trainings->id;
              
              $activity_date = date('d-m-Y', strtotime($trainings->activity_date));
              $dateArr[$activity_date]['training_ids'][] = $trainings->id;
              
              if(!in_array($trainings->state_id, $state_ids))
                  array_push($state_ids, $trainings->state_id);
              if(!in_array($trainings->district_id, $district_ids))  
                  array_push($district_ids,$trainings->district_id);
              if(!in_array($trainings->block_id, $block_ids))
                  array_push($block_ids,$trainings->block_id);
              if(!in_array($trainings->cluster_id, $cluster_ids))  
                  array_push($cluster_ids,$trainings->cluster_id);
              if(!in_array($trainings->subject_id, $subject_ids))  
                  array_push($subject_ids,$trainings->subject_id);

              $trainingData[$trainings->id]['activity_date'] = $trainings->activity_date;  
              $trainingData[$trainings->id]['state_id'] = $trainings->state_id;  
              $trainingData[$trainings->id]['district_id'] = $trainings->district_id;  
              $trainingData[$trainings->id]['block_id'] = $trainings->block_id;  
              $trainingData[$trainings->id]['cluster_id'] = $trainings->cluster_id;
              $trainingData[$trainings->id]['no_of_teachers_trained'] = $trainings->no_of_teachers_trained;
              $trainingData[$trainings->id]['subject_id'] = $trainings->subject_id;
          }
        }
        //echo"<pre>";
        //print_r($trainingData);
        
        $state_names = array();
        $state_names[0] = '';
        if (count($state_ids) > 0)
        {
          $this->load->model('states_model');  
          $states = $this->states_model->get_by_ids($state_ids);
          foreach($states as $state)
          {
            $state_names[$state->id] = $state->name;
          }
        }

        $district_names = array();
        $district_names[0] = '';
        if (count($district_ids) > 0)
        {
          $this->load->model('districts_model');
          $districts = $this->districts_model->getByIds($district_ids);
          foreach($districts as $district)
          {
            $district_names[$district->id] = $district->name;
          }
        }

        $block_names = array();
        $block_names[0] = '';
        if (count($block_ids) > 0)
        {
          $this->load->model('blocks_model');
          $blocks = $this->blocks_model->getByIds($block_ids);
          foreach($blocks as $block)
          {
            $block_names[$block->id] = $block->name;
          }
        }

        $cluster_names = array();
        $cluster_names[0] = '';
        if (count($cluster_ids) > 0)
        {
          $this->load->model('clusters_model');
          $clusters = $this->clusters_model->getByIds($cluster_ids);
          foreach($clusters as $cluster)
          {
            $cluster_names[$cluster->id] = $cluster->name;
          }
        }
     
        $school_names = array();
        $school_names[0] = '';
        if (count($school_ids) > 0)
        {
          $this->load->model('schools_model');
          $schools = $this->schools_model->getByIds($school_ids);
          foreach($schools as $school)
          {
            $school_names[$school->id] = $school->name." (".$school->dise_code.")";
          }
        }

        $subject_names = array();
        $subject_names[0] = '';
        if (count($subject_ids) > 0)
        {
          $this->load->model('Subjects_model');
          $subjects = $this->Subjects_model->getByIds($subject_ids);
          foreach($subjects as $subject)
          {
            $subject_names[$subject->id] = $subject->name;
          }
        }
        
        $created_at = date('Y-m-d');
        $national_rankings = '-';
        $state_ranking = '-';
        $training_days = '-';

        $training_rankings = $this->Trainings_model->get_spark_training_report($spark_id, '', $created_at);
        $feedback_ranking = $this->Spark_feedback_model->spark_feedback_new($created_at,$spark_id);
        
        if(!empty($feedback_ranking) && $feedback_ranking[0]->average > 0){
          $teacher_feedback_ranking = $feedback_ranking[0]->average;
          /*if($teacher_feedback_ranking > 4.5){
              $teacher_feedback_ranking = $teacher_feedback_ranking.' (Great)';
          }
          if($teacher_feedback_ranking > 4 && $teacher_feedback_ranking <= 4.5 ){
              $teacher_feedback_ranking = $teacher_feedback_ranking.' (Good)';
          }
          if($teacher_feedback_ranking >= 3.5 && $teacher_feedback_ranking <= 4 ){
              $teacher_feedback_ranking = $teacher_feedback_ranking.' (Sad)';
          }
          if($teacher_feedback_ranking < 3.5){
              $teacher_feedback_ranking = $teacher_feedback_ranking.' (Bad)';
          }*/
        }
        else{
           $teacher_feedback_ranking = 'Low response rate'; 
        }
        if(!empty($training_rankings)){
          if($training_rankings[0]->national_ranking > 0)
            $national_rankings = $training_rankings[0]->national_ranking;
          if($training_rankings[0]->state_ranking > 0)  
            $state_ranking = $training_rankings[0]->state_ranking;
          $training_days = $training_rankings[0]->training_days;
        }
        
        $visit_id_arr = array();
        $meeting_id_arr = array();
        $training_id_arr = array();
        $state_activity_arr = array();
        foreach($dateArr as $dates=>$value){
          
          if(isset($value['visit_ids'])){
            $visit_ids = $value['visit_ids'];
            
            //$message .= "\n<hr>";
            foreach($visit_ids as $visitid){
              
              $message = '';
              //$message .= "\n*Name:* ".$sparks->name." (".$sparks->login_id.")";
              $message .= "\n*Name:* ".$sparks->name;
              if($training_days > 0){
                $message .= "\nNTR: ".$national_rankings." (Days trained ".$training_days.")";
                $message .= "\nSTR: ".$state_ranking;
              }else{
                $message .= "\nNTR: ".$national_rankings;
                $message .= "\nSTR: ".$state_ranking;
              }
              $message .= "\nTFR: ".$teacher_feedback_ranking;
              $message .= "\n*Date:* ".$dates;
              
              $track_in_data = $this->Tracking_model->get_activity_tracking('schoolvisit', $visitid);
              $track_out_data = $this->Tracking_model->get_activity_tracking('schoolvisit_out', $visitid);
              
              if(!empty($track_in_data) && !empty($track_out_data)){
                $lat1 = $track_in_data[0]->tracking_latitude;
                $long1 = $track_in_data[0]->tracking_longitude;
                
                $lat2 = $track_out_data[0]->tracking_latitude;
                $long2 = $track_out_data[0]->tracking_longitude;
                
                $location_data = $this->get_google_distance($lat1, $long1, $lat2, $long2); //fetch distance between IN & OUT locations
                
                $tData['distance'] = $location_data['distance'];
                $tData['source_address'] = $location_data['source_address'];
                $tData['destination_address'] = $location_data['destination_address'];
                $this->School_visits_model->update_schoolvisit_data($tData, $visitid);
              }
              
              //$message .= "\n*School Visit Count: *".count($visit_ids);
                
              if(!in_array($visitid, $visit_id_arr)){
                array_push($visit_id_arr, $visitid);
              }
              
              //$message .= "\nID: ".$visitid;
              if(!empty($school_names[$visitData[$visitid]['school_id']]))  
                $message .= "\n*School:* ".$school_names[$visitData[$visitid]['school_id']];
              
              $message .= "\n*Location:* ".$state_names[$visitData[$visitid]['state_id']];
              if(!empty($district_names[$visitData[$visitid]['district_id']]))
                $message .= ", ".$district_names[$visitData[$visitid]['district_id']];
              if(!empty($block_names[$visitData[$visitid]['block_id']]))
                $message .= ", ".$block_names[$visitData[$visitid]['block_id']];
              if(!empty($cluster_names[$visitData[$visitid]['cluster_id']]))
                $message .= ", ".$cluster_names[$visitData[$visitid]['cluster_id']];
                
              $assess_data = $this->get_assessment_data('school_visit', $visitid) ; 
              if(!empty($assess_data))
                $message .= "\n".$assess_data;
              
              $in_time = (!empty($track_in_data) && isset($track_in_data[0]->tracking_time) ? date('H:i', strtotime($track_in_data[0]->tracking_time)) : '--');
              $out_time = (!empty($track_in_data) && isset($track_out_data[0]->tracking_time) ? date('H:i', strtotime($track_out_data[0]->tracking_time)) : '--');
              
              $message .= "\n*School-Visit-IN:* ".$in_time;
              $message .= "\n*School-Visit-OUT:* ".$out_time;
              $message .= "\n";  
              
              $state_activity_arr[$visitData[$visitid]['state_id']][] = $message;
            }
          }
          
          if(isset($value['meeting_ids'])){
            $meeting_ids = $value['meeting_ids'];
            
            foreach($meeting_ids as $meetingid){
              
              $message = '';
              //$message .= "\n*Name:* ".$sparks->name." (".$sparks->login_id.")";
              $message .= "\n*Name:* ".$sparks->name;
              $message .= "\n*Meet With:* ".$meetingData[$meetingid]['meet_with'];
              if($training_days > 0){
                $message .= "\nNTR: ".$national_rankings." (Days trained ".$training_days.")";
                $message .= "\nSTR: ".$state_ranking;
              }
              else{
                $message .= "\nNTR: ".$national_rankings;
                $message .= "\nSTR: ".$state_ranking;
              }
              $message .= "\nTFR: ".$teacher_feedback_ranking;
              $message .= "\n*Date:* ".$dates;
              
              $track_in_data = $this->Tracking_model->get_activity_tracking('review_meeting', $meetingid);
              $track_out_data = $this->Tracking_model->get_activity_tracking('review_meeting_out', $meetingid);
              
              if(!empty($track_in_data) && !empty($track_out_data)){
                $lat1 = $track_in_data[0]->tracking_latitude;
                $long1 = $track_in_data[0]->tracking_longitude;
                
                $lat2 = $track_out_data[0]->tracking_latitude;
                $long2 = $track_out_data[0]->tracking_longitude;
                
                $location_data = $this->get_google_distance($lat1, $long1, $lat2, $long2); //fetch distance between IN & OUT locations
                
                $tData['distance'] = $location_data['distance'];
                $tData['source_address'] = $location_data['source_address'];
                $tData['destination_address'] = $location_data['destination_address'];
                $this->Meetings_model->update_meeting_data($tData, $meetingid);
              }
              
              if(!in_array($meetingid, $meeting_id_arr)){
                array_push($meeting_id_arr, $meetingid);
              }
              $message .= "\n*Location:* ".$state_names[$meetingData[$meetingid]['state_id']];
              if(!empty($district_names[$meetingData[$meetingid]['district_id']]))
                $message .= ", ".$district_names[$meetingData[$meetingid]['district_id']];
              if(!empty($block_names[$meetingData[$meetingid]['block_id']]))
                $message .= ", ".$block_names[$meetingData[$meetingid]['block_id']];
              if(!empty($cluster_names[$meetingData[$meetingid]['cluster_id']]))
                $message .= ", ".$cluster_names[$meetingData[$meetingid]['cluster_id']];
              $assess_data = $this->get_assessment_data('meeting', $meetingid) ; 
              if(!empty($assess_data))
                $message .= "\n".$assess_data;
              
              $in_time = (!empty($track_in_data) && isset($track_in_data[0]->tracking_time) ? date('H:i', strtotime($track_in_data[0]->tracking_time)) : '--');
              $out_time = (!empty($track_in_data) && isset($track_out_data[0]->tracking_time) ? date('H:i', strtotime($track_out_data[0]->tracking_time)) : '--');
              
              $message .= "\n*Meeting-IN:* ".$in_time;
              $message .= "\n*Meeting-OUT:* ".$out_time;
              $message .= "\n";   
              $state_activity_arr[$meetingData[$meetingid]['state_id']][] = $message;
            }
          }
          
          if(isset($value['training_ids'])){
            $training_ids = $value['training_ids'];

            foreach($training_ids as $trainingid){
              
              $message = '';
              //$message .= "\n*Name:* ".$sparks->name." ($sparks->login_id)";
              $message .= "\n*Name:* ".$sparks->name;
              $message .= "\nNTR: ".$national_rankings." (Days trained ".$training_days.")";
              $message .= "\nSTR: ".$state_ranking;
              $message .= "\nTFR: ".$teacher_feedback_ranking;
              $message .= "\n*Date:* ".$dates;
              
              $track_in_data = $this->Tracking_model->get_activity_tracking('training', $trainingid);
              $track_out_data = $this->Tracking_model->get_activity_tracking('training_out', $trainingid);
              
              if(!empty($track_in_data) && !empty($track_out_data)){
                $lat1 = $track_in_data[0]->tracking_latitude;
                $long1 = $track_in_data[0]->tracking_longitude;
                
                $lat2 = $track_out_data[0]->tracking_latitude;
                $long2 = $track_out_data[0]->tracking_longitude;
                
                $location_data = $this->get_google_distance($lat1, $long1, $lat2, $long2); //fetch distance between IN & OUT locations
                
                $tData['distance'] = $location_data['distance'];
                $tData['source_address'] = $location_data['source_address'];
                $tData['destination_address'] = $location_data['destination_address'];
                $this->Trainings_model->update_training_data($tData, $trainingid);
              }
              
              
              if(!in_array($trainingid, $training_id_arr)){
                array_push($training_id_arr, $trainingid);
              }
              
              $message .= "\n*Subject:*".$subject_names[$trainingData[$trainingid]['subject_id']];
              $message .= "\n*Location:* ".$state_names[$trainingData[$trainingid]['state_id']];
              if(!empty($district_names[$trainingData[$trainingid]['district_id']]))
                $message .= ", ".$district_names[$trainingData[$trainingid]['district_id']];
              if(!empty($block_names[$trainingData[$trainingid]['block_id']]))
                $message .= ", ".$block_names[$trainingData[$trainingid]['block_id']];
              if(!empty($cluster_names[$trainingData[$trainingid]['cluster_id']]))
                $message .= ", ".$cluster_names[$trainingData[$trainingid]['cluster_id']];
              
              if(!empty($trainingData[$trainingid]['no_of_teachers_trained']))
                $message .= "\n*Teacher Trained:* ".$trainingData[$trainingid]['no_of_teachers_trained'];
              
              $assess_data = $this->get_assessment_data('training', $trainingid) ; 
              if(!empty($assess_data))
                $message .= "".$assess_data;  
              
              $in_time = (!empty($track_in_data) && isset($track_in_data[0]->tracking_time) ? date('H:i', strtotime($track_in_data[0]->tracking_time)) : '--');
              $out_time = (!empty($track_in_data) && isset($track_out_data[0]->tracking_time) ? date('H:i', strtotime($track_out_data[0]->tracking_time)) : '--');
              
              $message .= "\n*Training-IN:* ".$in_time;
              $message .= "\n*Training-OUT:* ".$out_time;
              $message .= "\n";  
              $state_activity_arr[$trainingData[$trainingid]['state_id']][] = $message;
            }
          }
        }
        
        $data[$i] = $message;
        $visit_id[$i] = $visit_id_arr;
        $meeting_id[$i] = $meeting_id_arr;
        $training_id[$i] = $training_id_arr;
        $state_data[$i] = $state_activity_arr;
        $i++;
      }
    }
    
    //$finalData['telegram_message'] = $data;
    $finalData['school_visit_ids'] = $visit_id;
    $finalData['meeting_ids'] = $meeting_id;
    $finalData['training_ids'] = $training_id;
    $finalData['state_data'] = $state_data;
    
    //print_r($finalData);//die;
    
    return $finalData;
  }
   
  function get_assessment_data($type, $activity_id)
  {
    $this->load->model('Assessment_model');
    $this->load->model('Observations_model');
    
    $data = $this->Assessment_model->getWhatsappByActivityId($type, $activity_id);

    $data_arr = array();
    $i = 0;
    foreach($data as $ac_data){
      $data_arr[$i]['question_id'] = $ac_data->question_id;
      $data_arr[$i]['question'] = $ac_data->question;
      $class_id = $ac_data->class_id;
      $question_type = $ac_data->question_type;
      $data_arr[$i]['question_type'] = $question_type;
      $data_arr[$i]['subject_wise'] = $ac_data->subject_wise;
      $data_arr[$i]['class_name'] = '';
      $data_arr[$i]['subject_name'] = '';
      if($class_id != 0)
      {
        $this->load->model('Classes_model');
        $class_data = $this->Classes_model->getById($class_id); 
        $data_arr[$i]['class_name'] = $class_data['name'];
      }
      
      if($ac_data->subject_wise == 1){
        $this->load->model('Subjects_model');
        $sdata = $this->Subjects_model->getById($ac_data->subject_id);
        $data_arr[$i]['subject_name'] = $sdata['name'];
      }
      
      if($question_type == 'subjective'){
        $data_arr[$i]['answer'] = $ac_data->answer;
      }
      else if($question_type == 'objective'){
        if($ac_data->answer != ''){
          $optionData = $this->Observations_model->getOptionById($ac_data->answer);
          if(!empty($optionData))
            $data_arr[$i]['answer'] = $optionData[0]->option_value;
        }
        else{
          $data_arr[$i]['answer'] = '';
        }
      }
      else if($question_type == 'yes-no'){
        if($ac_data->answer == 0)
          $data_arr[$i]['answer'] = 'No';
        else
          $data_arr[$i]['answer'] = 'Yes';
      }
      else if($question_type == 'yes-no-na'){
        if($ac_data->answer == 0)
          $data_arr[$i]['answer'] = 'N/A';
        else if($ac_data->answer == 1)
          $data_arr[$i]['answer'] = 'Yes';
        else if($ac_data->answer == 2)  
          $data_arr[$i]['answer'] = 'No';
      }
      else{
        $data_arr[$i]['answer'] = $ac_data->answer;
      }
      $i++;
    }
    $newArr = array();
    $m = 0;
    foreach($data_arr as $dataarr)
    {
      if(!empty($dataarr['class_name'])){
        if(!empty($dataarr['subject_name'])){
          if(isset($dataarr['answer']) && $dataarr['answer'] !=''){
            $newArr[$dataarr['class_name']][$dataarr['subject_name']]['question'][] = $dataarr['question'];
            $newArr[$dataarr['class_name']][$dataarr['subject_name']]['answer'][] = $dataarr['answer'];
          }
        }
        else{
          if(isset($dataarr['answer']) && $dataarr['answer'] !=''){
            $newArr[$dataarr['class_name']]['no_subject']['question'][] = $dataarr['question'];
            $newArr[$dataarr['class_name']]['no_subject']['answer'][] = $dataarr['answer'];
          }
        }
      }
      else{
        if(isset($dataarr['answer']) && $dataarr['answer'] !=''){
          $newArr['no_class']['no_subject']['question'][] = $dataarr['question'];
          $newArr['no_class']['no_subject']['answer'][] = $dataarr['answer'];
        }
      }
      $m++;
    }
    $assessment = $newArr;
    
    //echo"<pre>";
    //print_r($assessment);
    
    $return_message = '';
    if(!empty($assessment)){
      foreach($assessment as $class=>$class_data) { 
         if($class != 'no_class') { 
           $return_message .= "\n*".ucfirst($class)."#*";
          }else{ 
           //$return_message .= "\n*General Question(s)*";
          } 
          foreach($class_data as $subjects=>$subject_data) {
         
          if($subjects != 'no_subject') {
           $return_message .= "\nSubject: ".ucfirst($subjects);
          } 
          for($k=0; $k<count($subject_data['question']); $k++) {
             $q = $k+1; 
             //$question = str_replace('(Scale: 1- Poor, 2- Satisfactory, 3-Good, 4-Excellent)','',$subject_data['question'][$k]);
             $question = rtrim($subject_data['question'][$k],':');
             $question = rtrim($question,': ');
             $question = rtrim($question,' :');
             $return_message .= "\n- ".$question.': ';
             $return_message .= $subject_data['answer'][$k];
           }
         } 
       } 
    }
    return $return_message;
  }
  
  function rating_report()
  {
    $this->load->Model('states_model');
    $this->load->Model('Users_model');
    
    $prevmonth = date('m-Y', strtotime('-1 months'));
    $exp = explode('-',$prevmonth);
    echo $month = $exp[0];
    echo $year = $exp[1];
    //$month = date('m');
    //$year = date('Y');
      
    $stateData = $this->states_model->get_all();
    
    foreach($stateData as $states)
    {
      $state_id = $states->id;
      $stateSparks = $this->Users_model->getSparkByState($state_id);
      
      foreach($stateSparks as $sparks)
      {
        $spark_id = $sparks->user_id;
        
        $this->save_rating_report_data_new($month, $year, $state_id, $spark_id);
      }
      //die;
    }
    
  }

  function save_rating_report_data_new($month, $year, $state_id, $login_id)
  {
    $startdate = '01/'.$month.'/'.$year;
    $enddate = date('t/m/Y', strtotime($year.'-'.$month.'-01'));

    $this->load->Model('Assessment_model');
    $this->load->Model('Observations_model');
    $this->load->Model('Classes_model');
    $this->load->Model('Subjects_model');
    $this->load->Model('districts_model');

    $state_classes = $this->Subjects_model->check_mapping($state_id);
    
    $classes = array();
    $subject_ids = array();
    $class_subjects = array();
    $c = 0;
    /*foreach($state_classes as $state_class)
    {
        $class_id = $state_class->class_id;
        $subject_id = $state_class->subject_id;
        if(!in_array($class_id, $classes)){
          array_push($classes, $class_id);
        }
        
        if(!in_array($subject_id, $subject_ids)){
          array_push($subject_ids, $subject_id);
        }
        
        $subjectData = $this->Subjects_model->getById($subject_id);

        $class_subjects[$class_id][$c]['id'] = $subjectData['id'];
        $class_subjects[$class_id][$c]['name'] = $subjectData['name'];
        $c++;
    } */
    
    $observationsData = $this->Observations_model->getReportsObservation($state_id, array(), 0);
    
    $class_observation = array();
    $c = 0;
    foreach($observationsData as $observations)
    {
      $class_observation[$observations->class_id][$c]['id'] = $observations->id;
      $class_observation[$observations->class_id][$c]['question'] = $observations->question;
      $class_observation[$observations->class_id][$c]['weightage'] = $observations->weightage;
      $c++;
    }
    
    $assessment_data = $this->Assessment_model->getSparkSchoolAssessment($startdate,$enddate,$state_id, array(), $login_id, "");

    //print_r($assessment_data);
    
    if(!empty($assessment_data)){
      $activity_date_arr = array();
      $school_ids = array();
      $district_ids = array();
      $class_subject_question_ans_counts = array();
      $i = 0;
      foreach($assessment_data as $assessments)
      {
        $activity_date = date('d-m-Y', strtotime($assessments->activity_date));
        $district_id = $assessments->district_id;
        $school_id = $assessments->school_id;
        $question_id = $assessments->question_id;
        $class_id = $assessments->class_id;
        $subject_id = $assessments->subject_id;
        
        $ratingReport = array();
        $ratingReport['activity_date'] = date('Y-m-d', strtotime($assessments->activity_date));
        $ratingReport['activity_id'] = $assessments->ssv_id;
        $ratingReport['spark_id'] = $assessments->user_id;
        $ratingReport['state_id'] = $assessments->state_id;
        $ratingReport['school_id'] = $assessments->school_id;
        
        $check_spark_rating = $this->Assessment_model->selectSparkRating($ratingReport);
        //print_r($check_spark_rating);
        
        if(empty($check_spark_rating)){
          $ratingReport['block_id'] = $assessments->block_id;
          $ratingReport['district_id'] = $assessments->district_id;
          $spark_rating_id = $this->Assessment_model->addSparkRating($ratingReport);
        }
        else{
          $spark_rating_id = $check_spark_rating[0]->id;
        }
        
        if(!isset($activity_date_arr[$activity_date])){
          $activity_date_arr[$activity_date]['school_id'][$i] = $school_id;
          $activity_date_arr[$activity_date]['district_id'][$i] = $district_id;
          $i++;
        }else{
          if(!in_array($school_id, $activity_date_arr[$activity_date]['school_id'])){
            $activity_date_arr[$activity_date]['school_id'][$i] = $school_id;
            $activity_date_arr[$activity_date]['district_id'][$i] = $district_id;
            $i++;
          }
        }
        
        $csq_key = $activity_date.'_'.$school_id.'_'.$question_id;
          
        $ans_value = $assessments->answer;
        
        if($ans_value == 1)
          $class_subject_question_ans_counts[$csq_key] = 1;
        else  
          $class_subject_question_ans_counts[$csq_key] = 0;
      }

      $data_counts = $class_subject_question_ans_counts;
    
     foreach($activity_date_arr as $activity_date=>$schools) { 

       foreach($schools['school_id'] as $key=>$school) {
        $total_ans = 0;  
        $total_yes = 0;  
        $ratingReport = array();
        $ratingReport['activity_date'] = date('Y-m-d', strtotime($activity_date));
        $ratingReport['spark_id'] = $login_id;
        $ratingReport['school_id'] = $school;
        
        $spark_rating_data = $this->Assessment_model->selectSparkRating($ratingReport);
        $spark_rating_id = $spark_rating_data[0]->id;
        
        foreach($class_observation as $key=>$value) { 
          $total_ans++;
          //foreach($class_subjects[$key] as $subjects){
              $subject_ans = 0;  
              $subject_yes = 0;  
              $ques_arr = array();
              foreach($value as $observation){
                
                $ques_key = $activity_date.'_'.$school.'_'.$observation['id'];
                
                $ques_value = '--';
                $subject_ans = $subject_ans + $observation['weightage'];
                if(isset($data_counts[$ques_key])){
                    $ques_value = $data_counts[$ques_key] * $observation['weightage'];
                    $subject_yes = $ques_value + $subject_yes;
                    $total_yes = $total_yes + $ques_value;
                }
                $ques_arr[] = $observation['id'];
              }
              //echo"<br>ans : ".$subject_ans." >> ".$subject_yes;
              $ratingDetail = array();
              $ratingDetail['rating_report_id'] = $spark_rating_id;
              $ratingDetail['class_id'] = $key;
              $ratingDetail['subject_id'] = 0; //$subjects['id'];
              $ratingDetail['question_ids'] = implode(',',$ques_arr);
              
              $selectRatingDetails = $this->Assessment_model->selectSparkRatingDetail($ratingDetail);
              
              $subject_rating = $this->common_functions->retrun_rating($subject_ans, $subject_yes);
              $subject_rating_value = $this->common_functions->retrun_rating_value($subject_ans, $subject_yes);
              
              if(empty($selectRatingDetails)){
                $ratingDetail['subject_rating'] = $subject_rating;
                $ratingDetail['subject_rating_value'] = $subject_rating_value;
                $this->Assessment_model->addSparkRatingDetails($ratingDetail);
              }
              else{
                $ratingData['subject_rating'] = $subject_rating;
                $ratingData['subject_rating_value'] = $subject_rating_value;
                $detail_id = $this->Assessment_model->updateSparkRatingDetails($ratingData, $selectRatingDetails[0]->id);
              }
          //}
        }
        
        $overall_rating = $this->common_functions->retrun_rating($total_ans, $total_yes); 
        $overall_rating_value = $this->common_functions->retrun_rating_value($total_ans, $total_yes); 
        
        $updateRating = array();
        $ratingReport['overall_rating'] = $overall_rating;
        $ratingReport['overall_rating_value'] = $overall_rating_value;
        
        $this->Assessment_model->updateSparkRating($ratingReport, $spark_rating_id);
      } 
    } 
    
    }
    
  }
  
  function save_rating_report_data($month, $year, $state_id, $login_id)
  {
    $startdate = '01/'.$month.'/'.$year;
    $enddate = date('t/m/Y', strtotime($year.'-'.$month.'-01'));

    $this->load->Model('Assessment_model');
    $this->load->Model('Observations_model');
    $this->load->Model('Classes_model');
    $this->load->Model('Subjects_model');
    $this->load->Model('districts_model');

    $state_classes = $this->Subjects_model->check_mapping($state_id);
    
    $classes = array();
    $subject_ids = array();
    $class_subjects = array();
    $c = 0;
    foreach($state_classes as $state_class)
    {
        $class_id = $state_class->class_id;
        $subject_id = $state_class->subject_id;
        if(!in_array($class_id, $classes)){
          array_push($classes, $class_id);
        }
        
        if(!in_array($subject_id, $subject_ids)){
          array_push($subject_ids, $subject_id);
        }
        
        $subjectData = $this->Subjects_model->getById($subject_id);

        $class_subjects[$class_id][$c]['id'] = $subjectData['id'];
        $class_subjects[$class_id][$c]['name'] = $subjectData['name'];
        $c++;
    }
    
    $observationsData = $this->Observations_model->getReportsObservation($state_id, $classes);
    
    $class_observation = array();
    $c = 0;
    foreach($observationsData as $observations)
    {
      $class_observation[$observations->class_id][$c]['id'] = $observations->id;
      $class_observation[$observations->class_id][$c]['question'] = $observations->question;
      $class_observation[$observations->class_id][$c]['weightage'] = $observations->weightage;
      $c++;
    }
    
    $assessment_data = $this->Assessment_model->getSparkSchoolAssessment($startdate,$enddate,$state_id, $classes, $login_id);
    //print_r($assessment_data);
    
    if(!empty($assessment_data)){
      $activity_date_arr = array();
      $school_ids = array();
      $district_ids = array();
      $class_subject_question_ans_counts = array();
      $i = 0;
      foreach($assessment_data as $assessments)
      {
        $activity_date = date('d-m-Y', strtotime($assessments->activity_date));
        $district_id = $assessments->district_id;
        $school_id = $assessments->school_id;
        $question_id = $assessments->question_id;
        $class_id = $assessments->class_id;
        $subject_id = $assessments->subject_id;
        
        $ratingReport = array();
        $ratingReport['activity_date'] = date('Y-m-d', strtotime($assessments->activity_date));
        $ratingReport['activity_id'] = $assessments->ssv_id;
        $ratingReport['spark_id'] = $assessments->user_id;
        $ratingReport['state_id'] = $assessments->state_id;
        $ratingReport['school_id'] = $assessments->school_id;
        
        $check_spark_rating = $this->Assessment_model->selectSparkRating($ratingReport);
        //print_r($check_spark_rating);
        
        if(empty($check_spark_rating)){
          $ratingReport['block_id'] = $assessments->block_id;
          $ratingReport['district_id'] = $assessments->district_id;
          $spark_rating_id = $this->Assessment_model->addSparkRating($ratingReport);
        }
        else{
          $spark_rating_id = $check_spark_rating[0]->id;
        }
        
        if(!isset($activity_date_arr[$activity_date])){
          $activity_date_arr[$activity_date]['school_id'][$i] = $school_id;
          $activity_date_arr[$activity_date]['district_id'][$i] = $district_id;
          $i++;
        }else{
          if(!in_array($school_id, $activity_date_arr[$activity_date]['school_id'])){
            $activity_date_arr[$activity_date]['school_id'][$i] = $school_id;
            $activity_date_arr[$activity_date]['district_id'][$i] = $district_id;
            $i++;
          }
        }
        
        
        $csq_key = $activity_date.'_'.$school_id.'_'.$class_id.'_'.$subject_id.'_'.$question_id;
          
        $ans_value = $assessments->answer;
        
        if($ans_value == 1)
          $class_subject_question_ans_counts[$csq_key] = 1;
        else  
          $class_subject_question_ans_counts[$csq_key] = 0;
      }

      $data_counts = $class_subject_question_ans_counts;
    
     foreach($activity_date_arr as $activity_date=>$schools) { 

       foreach($schools['school_id'] as $key=>$school) {
        $total_ans = 0;  
        $total_yes = 0;  
        $ratingReport = array();
        $ratingReport['activity_date'] = date('Y-m-d', strtotime($activity_date));
        $ratingReport['spark_id'] = $login_id;
        $ratingReport['school_id'] = $school;
        
        $spark_rating_data = $this->Assessment_model->selectSparkRating($ratingReport);
        $spark_rating_id = $spark_rating_data[0]->id;
        
        foreach($class_observation as $key=>$value) { 
          $total_ans++;
          foreach($class_subjects[$key] as $subjects){
              $subject_ans = 0;  
              $subject_yes = 0;  
              $ques_arr = array();
              foreach($value as $observation){
                
                $ques_key = $activity_date.'_'.$school.'_'.$key.'_'.$subjects['id'].'_'.$observation['id'];
                
                $ques_value = '--';
                $subject_ans++;
                if(isset($data_counts[$ques_key])){
                    $ques_value = $data_counts[$ques_key];
                    $subject_yes = $ques_value + $subject_yes;
                    $total_yes = $total_yes + $ques_value;
                }
                $ques_arr[] = $observation['id'];
              }
              //echo"<br>ans : ".$subject_ans." >> ".$subject_yes;
              $ratingDetail = array();
              $ratingDetail['rating_report_id'] = $spark_rating_id;
              $ratingDetail['class_id'] = $key;
              $ratingDetail['subject_id'] = $subjects['id'];
              $ratingDetail['question_ids'] = implode(',',$ques_arr);
              
              $selectRatingDetails = $this->Assessment_model->selectSparkRatingDetail($ratingDetail);
              
              $subject_rating = $this->common_functions->retrun_rating($subject_ans, $subject_yes);
              $subject_rating_value = $this->common_functions->retrun_rating_value($subject_ans, $subject_yes);
              
              if(empty($selectRatingDetails)){
                $ratingDetail['subject_rating'] = $subject_rating;
                $ratingDetail['subject_rating_value'] = $subject_rating_value;
                $this->Assessment_model->addSparkRatingDetails($ratingDetail);
              }
              else{
                $ratingData['subject_rating'] = $subject_rating;
                $ratingData['subject_rating_value'] = $subject_rating_value;
                $detail_id = $this->Assessment_model->updateSparkRatingDetails($ratingData, $selectRatingDetails[0]->id);
              }
          }
        }
        
        $overall_rating = $this->common_functions->retrun_rating($total_ans, $total_yes); 
        $overall_rating_value = $this->common_functions->retrun_rating_value($total_ans, $total_yes); 
        
        $updateRating = array();
        $ratingReport['overall_rating'] = $overall_rating;
        $ratingReport['overall_rating_value'] = $overall_rating_value;
        
        $this->Assessment_model->updateSparkRating($ratingReport, $spark_rating_id);
      } 
    } 
    
    }
    
  }
  
  function get_state_activity($state)
  {
    $this->load->model('School_visits_model');
    $this->load->model('Meetings_model');
    $this->load->model('Trainings_model');
    $this->load->model('Users_model');
    
    $this->load->Model('States_model');
    $state_data = $this->States_model->getByName($state);
    $state_id = $state_data['id'];
    
    $sparkData = $this->Users_model->getTelegramSparks($state_id);
    
    $i = 0;
    $finalData = array();
    $data = array();
    $visit_id = array();  
    $meeting_id = array();
    $training_id = array();
    foreach($sparkData as $sparks)
    {
      $spark_id = $sparks->user_id;
        
      $school_visit_data = $this->School_visits_model->get_spark_telegram_count($spark_id);
      $visit_count = count($school_visit_data);
      
      $meeting_data = $this->Meetings_model->get_spark_telegram_count($spark_id);
      $meeting_count = count($meeting_data);
      
      $training_data = $this->Trainings_model->get_spark_telegram_count($spark_id);
      $training_count = count($training_data);
      
      if($visit_count > 0 || $meeting_count > 0 || $training_count > 0){
        $message = '';
        $message .= "\n*Name:* ".$sparks->name." (".$sparks->login_id.")";
        
        $visit_ids = array();  
        $meeting_ids = array();
        $training_ids = array();
        
        $visitData = array();  
        $meetingData = array();
        $trainingData = array();
        
        $state_ids = array();
        $district_ids = array();
        $block_ids = array();
        $cluster_ids = array();
        $school_ids = array();
        $subject_ids = array();
    
        $dateArr = array();

        if($visit_count > 0){
          //$message .= "\n".'School Visit Count :'.$visit_count;
          foreach($school_visit_data as $school_visits){
            if(!in_array($school_visits->id, $visit_ids)){
              $visit_ids[] = $school_visits->id;
              
              $activity_date = date('d-m-Y', strtotime($school_visits->activity_date));
              $dateArr[$activity_date]['visit_ids'][] = $school_visits->id;
              
              if(!in_array($school_visits->state_id, $state_ids))
                  array_push($state_ids, $school_visits->state_id);
              if(!in_array($school_visits->district_id, $district_ids))  
                  array_push($district_ids,$school_visits->district_id);
              if(!in_array($school_visits->block_id, $block_ids))
                  array_push($block_ids,$school_visits->block_id);
              if(!in_array($school_visits->cluster_id, $cluster_ids))  
                  array_push($cluster_ids,$school_visits->cluster_id);
              if(!in_array($school_visits->school_id, $school_ids))
                  array_push($school_ids,$school_visits->school_id);
              
              $visitData[$school_visits->id]['activity_date'] = $school_visits->activity_date;  
              $visitData[$school_visits->id]['state_id'] = $school_visits->state_id;  
              $visitData[$school_visits->id]['district_id'] = $school_visits->district_id;  
              $visitData[$school_visits->id]['block_id'] = $school_visits->block_id;  
              $visitData[$school_visits->id]['cluster_id'] = $school_visits->cluster_id;  
              $visitData[$school_visits->id]['school_id'] = $school_visits->school_id;  
            }
          }
        }
        
        if($meeting_count > 0){
          //$message .= "\n".'Meeting Count : '.$meeting_count;
          foreach($meeting_data as $meetings){
            if(!in_array($meetings->id, $meeting_ids))
              $meeting_ids[] = $meetings->id;
              
              $activity_date = date('d-m-Y', strtotime($meetings->activity_date));
              $dateArr[$activity_date]['meeting_ids'][] = $meetings->id;
              
              if(!in_array($meetings->state_id, $state_ids))
                  array_push($state_ids, $meetings->state_id);
              if(!in_array($meetings->district_id, $district_ids))  
                  array_push($district_ids,$meetings->district_id);
              if(!in_array($meetings->block_id, $block_ids))
                  array_push($block_ids,$meetings->block_id);
              if(!in_array($meetings->cluster_id, $cluster_ids))  
                  array_push($cluster_ids,$meetings->cluster_id);
                  
              $meetingData[$meetings->id]['activity_date'] = $meetings->activity_date;  
              $meetingData[$meetings->id]['state_id'] = $meetings->state_id;  
              $meetingData[$meetings->id]['district_id'] = $meetings->district_id;  
              $meetingData[$meetings->id]['block_id'] = $meetings->block_id;  
              $meetingData[$meetings->id]['cluster_id'] = $meetings->cluster_id;
          }
        }
        
        if($training_count > 0){
          //$message .= "\n".'Training Count : '.$training_count;
          foreach($training_data as $trainings){
            if(!in_array($trainings->id, $training_ids))
              $training_ids[] = $trainings->id;
              
              $activity_date = date('d-m-Y', strtotime($trainings->activity_date));
              $dateArr[$activity_date]['training_ids'][] = $trainings->id;
              
              if(!in_array($trainings->state_id, $state_ids))
                  array_push($state_ids, $trainings->state_id);
              if(!in_array($trainings->district_id, $district_ids))  
                  array_push($district_ids,$trainings->district_id);
              if(!in_array($trainings->block_id, $block_ids))
                  array_push($block_ids,$trainings->block_id);
              if(!in_array($trainings->cluster_id, $cluster_ids))  
                  array_push($cluster_ids,$trainings->cluster_id);
              if(!in_array($trainings->subject_id, $subject_ids))  
                  array_push($subject_ids,$trainings->subject_id);

              $trainingData[$trainings->id]['activity_date'] = $trainings->activity_date;  
              $trainingData[$trainings->id]['state_id'] = $trainings->state_id;  
              $trainingData[$trainings->id]['district_id'] = $trainings->district_id;  
              $trainingData[$trainings->id]['block_id'] = $trainings->block_id;  
              $trainingData[$trainings->id]['cluster_id'] = $trainings->cluster_id;
              $trainingData[$trainings->id]['no_of_teachers_trained'] = $trainings->no_of_teachers_trained;
              $trainingData[$trainings->id]['subject_id'] = $trainings->subject_id;
          }
        }
        
        $state_names = array();
        $state_names[0] = '';
        if (count($state_ids) > 0)
        {
          $this->load->model('states_model');  
          $states = $this->states_model->get_by_ids($state_ids);
          foreach($states as $state)
          {
            $state_names[$state->id] = $state->name;
          }
        }

        $district_names = array();
        $district_names[0] = '';
        if (count($district_ids) > 0)
        {
          $this->load->model('districts_model');
          $districts = $this->districts_model->getByIds($district_ids);
          foreach($districts as $district)
          {
            $district_names[$district->id] = $district->name;
          }
        }

        $block_names = array();
        $block_names[0] = '';
        if (count($block_ids) > 0)
        {
          $this->load->model('blocks_model');
          $blocks = $this->blocks_model->getByIds($block_ids);
          foreach($blocks as $block)
          {
            $block_names[$block->id] = $block->name;
          }
        }

        $cluster_names = array();
        $cluster_names[0] = '';
        if (count($cluster_ids) > 0)
        {
          $this->load->model('clusters_model');
          $clusters = $this->clusters_model->getByIds($cluster_ids);
          foreach($clusters as $cluster)
          {
            $cluster_names[$cluster->id] = $cluster->name;
          }
        }
     
        $school_names = array();
        $school_names[0] = '';
        if (count($school_ids) > 0)
        {
          $this->load->model('schools_model');
          $schools = $this->schools_model->getByIds($school_ids);
          foreach($schools as $school)
          {
            $school_names[$school->id] = $school->name." (".$school->dise_code.")";
          }
        }

        $subject_names = array();
        $subject_names[0] = '';
        if (count($subject_ids) > 0)
        {
          $this->load->model('Subjects_model');
          $subjects = $this->Subjects_model->getByIds($subject_ids);
          foreach($subjects as $subject)
          {
            $subject_names[$subject->id] = $subject->name;
          }
        }

        
        $visit_id_arr = array();
        $meeting_id_arr = array();
        $training_id_arr = array();
        foreach($dateArr as $dates=>$value){
          
          $message .= "\n*Date:* ".$dates;
          
          if(isset($value['visit_ids'])){
            $visit_ids = $value['visit_ids'];
            
            //$message .= "\n<hr>";

            $message .= "\n*School Visit Count: *".count($visit_ids);
            foreach($visit_ids as $visitid){
              
              if(!in_array($visitid, $visit_id_arr)){
                array_push($visit_id_arr, $visitid);
              }
              
              //$message .= "\nID: ".$visitid;
              if(!empty($school_names[$visitData[$visitid]['school_id']]))  
                $message .= "\n*School:* ".$school_names[$visitData[$visitid]['school_id']];
              
              $message .= "\n*Location:* ".$state_names[$visitData[$visitid]['state_id']];
              if(!empty($district_names[$visitData[$visitid]['district_id']]))
                $message .= ", ".$district_names[$visitData[$visitid]['district_id']];
              if(!empty($block_names[$visitData[$visitid]['block_id']]))
                $message .= ", ".$block_names[$visitData[$visitid]['block_id']];
              if(!empty($cluster_names[$visitData[$visitid]['cluster_id']]))
                $message .= ", ".$cluster_names[$visitData[$visitid]['cluster_id']];
                
              $assess_data = $this->get_assessment_data('school_visit', $visitid) ; 
              if(!empty($assess_data))
                $message .= "\n".$assess_data;
              //$message .= "\n";
            }
          }
          if(isset($value['meeting_ids'])){
            $meeting_ids = $value['meeting_ids'];
            
            //$message .= "\n------------------";
            
            $message .= "\n*Meeting Count: *".count($meeting_ids);
            foreach($meeting_ids as $meetingid){
              if(!in_array($meetingid, $meeting_id_arr)){
                array_push($meeting_id_arr, $meetingid);
              }
              //$message .= "\nID: ".$meetingid;
              $message .= "\n*Location:* ".$state_names[$meetingData[$meetingid]['state_id']];
              if(!empty($district_names[$meetingData[$meetingid]['district_id']]))
                $message .= ", ".$district_names[$meetingData[$meetingid]['district_id']];
              if(!empty($block_names[$meetingData[$meetingid]['block_id']]))
                $message .= ", ".$block_names[$meetingData[$meetingid]['block_id']];
              if(!empty($cluster_names[$meetingData[$meetingid]['cluster_id']]))
                $message .= ", ".$cluster_names[$meetingData[$meetingid]['cluster_id']];
              $assess_data = $this->get_assessment_data('meeting', $meetingid) ; 
              if(!empty($assess_data))
                $message .= "\n".$assess_data;
              //$message .= "\n";    
            }
          }
          if(isset($value['training_ids'])){
            $training_ids = $value['training_ids'];
            //$message .= "\n------------------";
            
            //$message .= "\n*Training Count: *".count($training_ids);
            foreach($training_ids as $trainingid){
              if(!in_array($trainingid, $training_id_arr)){
                array_push($training_id_arr, $trainingid);
              }
              //$message .= "\nID: ".$trainingid;
              $message .= "\n*Subject:*".$subject_names[$trainingData[$trainingid]['subject_id']];
              $message .= "\n*Location:* ".$state_names[$trainingData[$trainingid]['state_id']];
              if(!empty($district_names[$trainingData[$trainingid]['district_id']]))
                $message .= ", ".$district_names[$trainingData[$trainingid]['district_id']];
              if(!empty($block_names[$trainingData[$trainingid]['block_id']]))
                $message .= ", ".$block_names[$trainingData[$trainingid]['block_id']];
              if(!empty($cluster_names[$trainingData[$trainingid]['cluster_id']]))
                $message .= ", ".$cluster_names[$trainingData[$trainingid]['cluster_id']];
              
              if(!empty($trainingData[$trainingid]['no_of_teachers_trained']))
                $message .= "\n*Teacher Trained:* ".$trainingData[$trainingid]['no_of_teachers_trained'];
              
              $assess_data = $this->get_assessment_data('training', $trainingid) ; 
              if(!empty($assess_data))
                $message .= "".$assess_data;  
              $message .= "\n";  
            }
          }
        }
        
        $data[$i] = $message;
        $visit_id[$i] = $visit_id_arr;
        $meeting_id[$i] = $meeting_id_arr;
        $training_id[$i] = $training_id_arr;
        $i++;
      }
    }
    
    $finalData['telegram_message'] = $data;
    $finalData['school_visit_ids'] = $visit_id;
    $finalData['meeting_ids'] = $meeting_id;
    $finalData['training_ids'] = $training_id;
    //print_r($finalData);die;
    
    return $finalData;
  }

  function state_person_telegaram()
  {
    $this->load->Model('States_model');
    
    $stateData = $this->States_model->get_all();
    foreach($stateData as $states)
    {
      $state_code = $states->name;
      $this->state_person_message($states->name);
    }
  }

  function state_person_message($state)
  {
    $data = $this->get_state_person_activity($state);
    //echo"<pre>";print_r($data);
    //die;
    $apiToken = TELEGRAM_TOKEN;
    $messageArr = $data['telegram_message'];
    //$state_data = $data['state_data'];
    $school_visit_ids = $data['school_visit_ids'];
    $meeting_ids = $data['meeting_ids'];
    $training_ids = $data['training_ids'];
    
    foreach($messageArr as $key=>$message){
        //echo"<hr>state_id : ".$state_id." StateGroup : ".$group_code[$state_id]." >> ".$message;
        
        $postData = array('chat_id' => '-363786491', 'parse_mode'=>'MARKDOWN','text' => $message);

        $url = "https://api.telegram.org/bot$apiToken/sendMessage";

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
        $response = curl_exec($ch);
        $result_array = json_decode($response);
        curl_close($ch);
        //print_r($result_array);

        if(isset($result_array->ok) && $result_array->ok == 1){
          $updateData['telegram_flag'] = 1;
          $this->load->model('School_visits_model');
          if(!empty($school_visit_ids[$key]))
            $this->School_visits_model->update_table_info('school_visits', $updateData, $school_visit_ids[$key]);
          if(!empty($meeting_ids[$key]))  
            $this->School_visits_model->update_table_info('meetings', $updateData, $meeting_ids[$key]);
          if(!empty($training_ids[$key]))
            $this->School_visits_model->update_table_info('trainings', $updateData, $training_ids[$key]);
        }
        else{
          //echo 'Error';
        }
    }
  } 

  function get_state_person_activity($state)
  {
    $this->load->model('School_visits_model');
    $this->load->model('Meetings_model');
    $this->load->model('Trainings_model');
    $this->load->model('Users_model');
    $this->load->model('Tracking_model');
    
    $this->load->Model('States_model');
    $state_data = $this->States_model->getByName($state);
    $state_id = $state_data['id'];
    
    $sparkData = $this->Users_model->getTelegramSparks($state_id, array('state_person'), 1);
    //echo"<br>".$state_id." >> ".count($sparkData);
    $i = 0;
    $finalData = array();
    $data = array();
    $visit_id = array();  
    $meeting_id = array();
    $training_id = array();
    foreach($sparkData as $sparks)
    {
      $spark_id = $sparks->user_id;
        
      $school_visit_data = $this->School_visits_model->get_spark_telegram_count($spark_id);
      $visit_count = count($school_visit_data);
      
      $meeting_data = $this->Meetings_model->get_spark_telegram_count($spark_id);
      $meeting_count = count($meeting_data);
      
      $training_data = $this->Trainings_model->get_spark_telegram_count($spark_id);
      $training_count = count($training_data);
      
      if($visit_count > 0 || $meeting_count > 0 || $training_count > 0){
        $message = '';
        $message .= "\n*Name:* ".$sparks->name." (".$sparks->login_id.")";
        
        $visit_ids = array();  
        $meeting_ids = array();
        $training_ids = array();
        
        $visitData = array();  
        $meetingData = array();
        $trainingData = array();
        
        $state_ids = array();
        $district_ids = array();
        $block_ids = array();
        $cluster_ids = array();
        $school_ids = array();
        $subject_ids = array();
    
        $dateArr = array();

        if($visit_count > 0){
          //$message .= "\n".'School Visit Count :'.$visit_count;
          foreach($school_visit_data as $school_visits){
            if(!in_array($school_visits->id, $visit_ids)){
              $visit_ids[] = $school_visits->id;
              
              $activity_date = date('d-m-Y', strtotime($school_visits->activity_date));
              $dateArr[$activity_date]['visit_ids'][] = $school_visits->id;
              
              if(!in_array($school_visits->state_id, $state_ids))
                  array_push($state_ids, $school_visits->state_id);
              if(!in_array($school_visits->district_id, $district_ids))  
                  array_push($district_ids,$school_visits->district_id);
              if(!in_array($school_visits->block_id, $block_ids))
                  array_push($block_ids,$school_visits->block_id);
              if(!in_array($school_visits->cluster_id, $cluster_ids))  
                  array_push($cluster_ids,$school_visits->cluster_id);
              if(!in_array($school_visits->school_id, $school_ids))
                  array_push($school_ids,$school_visits->school_id);
              
              $visitData[$school_visits->id]['activity_date'] = $school_visits->activity_date;  
              $visitData[$school_visits->id]['state_id'] = $school_visits->state_id;  
              $visitData[$school_visits->id]['district_id'] = $school_visits->district_id;  
              $visitData[$school_visits->id]['block_id'] = $school_visits->block_id;  
              $visitData[$school_visits->id]['cluster_id'] = $school_visits->cluster_id;  
              $visitData[$school_visits->id]['school_id'] = $school_visits->school_id;  
            }
          }
        }
        
        if($meeting_count > 0){
          //$message .= "\n".'Meeting Count : '.$meeting_count;
          foreach($meeting_data as $meetings){
            if(!in_array($meetings->id, $meeting_ids))
              $meeting_ids[] = $meetings->id;
              
              $activity_date = date('d-m-Y', strtotime($meetings->activity_date));
              $dateArr[$activity_date]['meeting_ids'][] = $meetings->id;
              
              if(!in_array($meetings->state_id, $state_ids))
                  array_push($state_ids, $meetings->state_id);
              if(!in_array($meetings->district_id, $district_ids))  
                  array_push($district_ids,$meetings->district_id);
              if(!in_array($meetings->block_id, $block_ids))
                  array_push($block_ids,$meetings->block_id);
              if(!in_array($meetings->cluster_id, $cluster_ids))  
                  array_push($cluster_ids,$meetings->cluster_id);
                  
              $meetingData[$meetings->id]['activity_date'] = $meetings->activity_date;  
              $meetingData[$meetings->id]['state_id'] = $meetings->state_id;  
              $meetingData[$meetings->id]['district_id'] = $meetings->district_id;  
              $meetingData[$meetings->id]['block_id'] = $meetings->block_id;  
              $meetingData[$meetings->id]['cluster_id'] = $meetings->cluster_id;
              $meetingData[$meetings->id]['meet_with'] = $meetings->meet_with;
          }
        }
        
        if($training_count > 0){
          //$message .= "\n".'Training Count : '.$training_count;
          foreach($training_data as $trainings){
            if(!in_array($trainings->id, $training_ids))
              $training_ids[] = $trainings->id;
              
              $activity_date = date('d-m-Y', strtotime($trainings->activity_date));
              $dateArr[$activity_date]['training_ids'][] = $trainings->id;
              
              if(!in_array($trainings->state_id, $state_ids))
                  array_push($state_ids, $trainings->state_id);
              if(!in_array($trainings->district_id, $district_ids))  
                  array_push($district_ids,$trainings->district_id);
              if(!in_array($trainings->block_id, $block_ids))
                  array_push($block_ids,$trainings->block_id);
              if(!in_array($trainings->cluster_id, $cluster_ids))  
                  array_push($cluster_ids,$trainings->cluster_id);
              if(!in_array($trainings->subject_id, $subject_ids))  
                  array_push($subject_ids,$trainings->subject_id);

              $trainingData[$trainings->id]['activity_date'] = $trainings->activity_date;  
              $trainingData[$trainings->id]['state_id'] = $trainings->state_id;  
              $trainingData[$trainings->id]['district_id'] = $trainings->district_id;  
              $trainingData[$trainings->id]['block_id'] = $trainings->block_id;  
              $trainingData[$trainings->id]['cluster_id'] = $trainings->cluster_id;
              $trainingData[$trainings->id]['no_of_teachers_trained'] = $trainings->no_of_teachers_trained;
              $trainingData[$trainings->id]['subject_id'] = $trainings->subject_id;
          }
        }
        
        $state_names = array();
        $state_names[0] = '';
        if (count($state_ids) > 0)
        {
          $this->load->model('states_model');  
          $states = $this->states_model->get_by_ids($state_ids);
          foreach($states as $state)
          {
            $state_names[$state->id] = $state->name;
          }
        }

        $district_names = array();
        $district_names[0] = '';
        if (count($district_ids) > 0)
        {
          $this->load->model('districts_model');
          $districts = $this->districts_model->getByIds($district_ids);
          foreach($districts as $district)
          {
            $district_names[$district->id] = $district->name;
          }
        }

        $block_names = array();
        $block_names[0] = '';
        if (count($block_ids) > 0)
        {
          $this->load->model('blocks_model');
          $blocks = $this->blocks_model->getByIds($block_ids);
          foreach($blocks as $block)
          {
            $block_names[$block->id] = $block->name;
          }
        }

        $cluster_names = array();
        $cluster_names[0] = '';
        if (count($cluster_ids) > 0)
        {
          $this->load->model('clusters_model');
          $clusters = $this->clusters_model->getByIds($cluster_ids);
          foreach($clusters as $cluster)
          {
            $cluster_names[$cluster->id] = $cluster->name;
          }
        }
     
        $school_names = array();
        $school_names[0] = '';
        if (count($school_ids) > 0)
        {
          $this->load->model('schools_model');
          $schools = $this->schools_model->getByIds($school_ids);
          foreach($schools as $school)
          {
            $school_names[$school->id] = $school->name." (".$school->dise_code.")";
          }
        }

        $subject_names = array();
        $subject_names[0] = '';
        if (count($subject_ids) > 0)
        {
          $this->load->model('Subjects_model');
          $subjects = $this->Subjects_model->getByIds($subject_ids);
          foreach($subjects as $subject)
          {
            $subject_names[$subject->id] = $subject->name;
          }
        }

        
        $visit_id_arr = array();
        $meeting_id_arr = array();
        $training_id_arr = array();
        foreach($dateArr as $dates=>$value){
          
          $message .= "\n*Date:* ".$dates;
          
          $attendanceIn = $this->Tracking_model->getAttendance($sparks->user_id, $dates, 'in', 'tracking_time asc', 1);
          $attendanceOut = $this->Tracking_model->getAttendance($sparks->user_id, $dates, 'out', 'tracking_time desc', 1);
          
          //echo"<pre>";
          //print_r($attendanceIn);
          $message .= "\n*Attendance IN:* ";
          $message .= date('H:i', strtotime($attendanceIn[0]->tracking_time));
          $message .= " (".$attendanceIn[0]->tracking_location.")";
          if(!empty($attendanceOut)){                
            $message .= "\n*Attendance OUT:* ";
            $message .= date('H:i', strtotime($attendanceOut[0]->tracking_time));
            $message .= " (".$attendanceOut[0]->tracking_location.")";
            //if($attendanceOut[0]->tracking_time != 'app')
              //$message .= " (System)";
          }
          
          if(isset($value['visit_ids'])){
            $visit_ids = $value['visit_ids'];
            
            //$message .= "\n<hr>";

            $message .= "\n*School Visit Count: *".count($visit_ids);
            foreach($visit_ids as $visitid){
              
              if(!in_array($visitid, $visit_id_arr)){
                array_push($visit_id_arr, $visitid);
              }
              
              $track_in_data = $this->Tracking_model->get_activity_tracking('schoolvisit', $visitid);
              $track_out_data = $this->Tracking_model->get_activity_tracking('schoolvisit_out', $visitid);
              
              if(!empty($track_in_data) && !empty($track_out_data)){
                $lat1 = $track_in_data[0]->tracking_latitude;
                $long1 = $track_in_data[0]->tracking_longitude;
                
                $lat2 = $track_out_data[0]->tracking_latitude;
                $long2 = $track_out_data[0]->tracking_longitude;
                
                $location_data = $this->get_google_distance($lat1, $long1, $lat2, $long2); //fetch distance between IN & OUT locations
                
                $tData['distance'] = $location_data['distance'];
                $tData['source_address'] = $location_data['source_address'];
                $tData['destination_address'] = $location_data['destination_address'];
                $this->School_visits_model->update_schoolvisit_data($tData, $visitid);
              }
              
              //$message .= "\nID: ".$visitid;
              if(!empty($school_names[$visitData[$visitid]['school_id']]))  
                $message .= "\n*School:* ".$school_names[$visitData[$visitid]['school_id']];
              
              $message .= "\n*Location:* ".$state_names[$visitData[$visitid]['state_id']];
              if(!empty($district_names[$visitData[$visitid]['district_id']]))
                $message .= ", ".$district_names[$visitData[$visitid]['district_id']];
              if(!empty($block_names[$visitData[$visitid]['block_id']]))
                $message .= ", ".$block_names[$visitData[$visitid]['block_id']];
              if(!empty($cluster_names[$visitData[$visitid]['cluster_id']]))
                $message .= ", ".$cluster_names[$visitData[$visitid]['cluster_id']];
                
              $assess_data = $this->get_assessment_data('school_visit', $visitid) ; 
              if(!empty($assess_data))
                $message .= "\n".$assess_data;
              
              $in_time = (!empty($track_in_data) && isset($track_in_data[0]->tracking_time) ? date('H:i', strtotime($track_in_data[0]->tracking_time)) : '--');  
              $out_time = (!empty($track_out_data) && isset($track_out_data[0]->tracking_time) ? date('H:i', strtotime($track_out_data[0]->tracking_time)) : '--');
              $message .= "\n*School-Visit-IN:* ".$in_time;
              $message .= "\n*School-Visit-OUT:* ".$out_time;  
              $message .= "\n";  
            }
          }
          if(isset($value['meeting_ids'])){
            $meeting_ids = $value['meeting_ids'];
            
            //$message .= "\n------------------";
            
            $message .= "\n*Meeting Count: *".count($meeting_ids);
            foreach($meeting_ids as $meetingid){
              if(!in_array($meetingid, $meeting_id_arr)){
                array_push($meeting_id_arr, $meetingid);
              }
              
              $track_in_data = $this->Tracking_model->get_activity_tracking('review_meeting', $meetingid);
              $track_out_data = $this->Tracking_model->get_activity_tracking('review_meeting_out', $meetingid);
              
              if(!empty($track_in_data) && !empty($track_out_data)){
                $lat1 = $track_in_data[0]->tracking_latitude;
                $long1 = $track_in_data[0]->tracking_longitude;
                
                $lat2 = $track_out_data[0]->tracking_latitude;
                $long2 = $track_out_data[0]->tracking_longitude;
                
                $location_data = $this->get_google_distance($lat1, $long1, $lat2, $long2); //fetch distance between IN & OUT locations
                
                $tData['distance'] = $location_data['distance'];
                $tData['source_address'] = $location_data['source_address'];
                $tData['destination_address'] = $location_data['destination_address'];
                $this->Meetings_model->update_meeting_data($tData, $meetingid);
              }
              
              //$message .= "\nID: ".$meetingid;
              $message .= "\n*Meet With:* ".$meetingData[$meetingid]['meet_with'];
              $message .= "\n*Location:* ".$state_names[$meetingData[$meetingid]['state_id']];
              if(!empty($district_names[$meetingData[$meetingid]['district_id']]))
                $message .= ", ".$district_names[$meetingData[$meetingid]['district_id']];
              if(!empty($block_names[$meetingData[$meetingid]['block_id']]))
                $message .= ", ".$block_names[$meetingData[$meetingid]['block_id']];
              if(!empty($cluster_names[$meetingData[$meetingid]['cluster_id']]))
                $message .= ", ".$cluster_names[$meetingData[$meetingid]['cluster_id']];
              $assess_data = $this->get_assessment_data('meeting', $meetingid) ; 
              if(!empty($assess_data))
                $message .= "\n".$assess_data;
              
              $in_time = (!empty($track_in_data) && isset($track_in_data[0]->tracking_time) ? date('H:i', strtotime($track_in_data[0]->tracking_time)) : '--');  
              $out_time = (!empty($track_out_data) && isset($track_out_data[0]->tracking_time) ? date('H:i', strtotime($track_out_data[0]->tracking_time)) : '--');
              $message .= "\n*Meeting-IN:* ".$in_time;
              $message .= "\n*Meeting-OUT:* ".$out_time;  
              $message .= "\n";  
            }
          }
          if(isset($value['training_ids'])){
            $training_ids = $value['training_ids'];
            //$message .= "\n------------------";
            
            //$message .= "\n*Training Count: *".count($training_ids);
            foreach($training_ids as $trainingid){
              if(!in_array($trainingid, $training_id_arr)){
                array_push($training_id_arr, $trainingid);
              }
              
              $track_in_data = $this->Tracking_model->get_activity_tracking('training', $trainingid);
              $track_out_data = $this->Tracking_model->get_activity_tracking('training_out', $trainingid);
              
              if(!empty($track_in_data) && !empty($track_out_data)){
                $lat1 = $track_in_data[0]->tracking_latitude;
                $long1 = $track_in_data[0]->tracking_longitude;
                
                $lat2 = $track_out_data[0]->tracking_latitude;
                $long2 = $track_out_data[0]->tracking_longitude;
                
                $location_data = $this->get_google_distance($lat1, $long1, $lat2, $long2); //fetch distance between IN & OUT locations
                
                $tData['distance'] = $location_data['distance'];
                $tData['source_address'] = $location_data['source_address'];
                $tData['destination_address'] = $location_data['destination_address'];
                $this->Trainings_model->update_training_data($tData, $trainingid);
              }
              
              //$message .= "\nID: ".$trainingid;
              $message .= "\n*Subject:*".$subject_names[$trainingData[$trainingid]['subject_id']];
              $message .= "\n*Location:* ".$state_names[$trainingData[$trainingid]['state_id']];
              if(!empty($district_names[$trainingData[$trainingid]['district_id']]))
                $message .= ", ".$district_names[$trainingData[$trainingid]['district_id']];
              if(!empty($block_names[$trainingData[$trainingid]['block_id']]))
                $message .= ", ".$block_names[$trainingData[$trainingid]['block_id']];
              if(!empty($cluster_names[$trainingData[$trainingid]['cluster_id']]))
                $message .= ", ".$cluster_names[$trainingData[$trainingid]['cluster_id']];
              
              if(!empty($trainingData[$trainingid]['no_of_teachers_trained']))
                $message .= "\n*Teacher Trained:* ".$trainingData[$trainingid]['no_of_teachers_trained'];
              
              $assess_data = $this->get_assessment_data('training', $trainingid) ; 
              if(!empty($assess_data))
                $message .= "".$assess_data;  
              
              $in_time = (!empty($track_in_data) && isset($track_in_data[0]->tracking_time) ? date('H:i', strtotime($track_in_data[0]->tracking_time)) : '--');  
              $out_time = (!empty($track_out_data) && isset($track_out_data[0]->tracking_time) ? date('H:i', strtotime($track_out_data[0]->tracking_time)) : '--');
              $message .= "\n*Training-IN:* ".$in_time;
              $message .= "\n*Training-OUT:* ".$out_time;  
              $message .= "\n";  
            }
          }
        }
        
        $data[$i] = $message;
        $visit_id[$i] = $visit_id_arr;
        $meeting_id[$i] = $meeting_id_arr;
        $training_id[$i] = $training_id_arr;
        $i++;
      }
    }
    
    $finalData['telegram_message'] = $data;
    $finalData['school_visit_ids'] = $visit_id;
    $finalData['meeting_ids'] = $meeting_id;
    $finalData['training_ids'] = $training_id;
    //print_r($finalData);die;
    
    return $finalData;
  }

  function highSparkSummaryCron()
  {
    $prevmonth = date('m-Y', strtotime('-1 months'));
    $exp = explode('-',$prevmonth);
    //print_r($exp);die;
    $inpmonth = $exp[0];
    $inpyear = $exp[1];
    
    //$inpmonth = date('m');
    //$inpyear = date('Y');
    
    $this->load->Model('States_model');
    
    $state_ids = array('1');
    $stateData = $this->States_model->get_by_ids($state_ids);
    //$stateData = $this->States_model->get_all();
    foreach($stateData as $states)
    {
      $state_id = $states->id;
      $this->create_high_enroll_data($state_id,$inpmonth,$inpyear);
    }
  }
  
  function highSparkSummaryCron1()
  {
    $prevmonth = date('m-Y', strtotime('-1 months'));
    $exp = explode('-',$prevmonth);
    $inpmonth = $exp[0];
    $inpyear = $exp[1];
    
    $this->load->Model('States_model');
    
    $state_ids = array('2');
    $stateData = $this->States_model->get_by_ids($state_ids);
    foreach($stateData as $states)
    {
      $state_id = $states->id;
      $this->create_high_enroll_data($state_id,$inpmonth,$inpyear);
    }
    
  }

  function highSparkSummaryCron2()
  {
    $prevmonth = date('m-Y', strtotime('-1 months'));
    $exp = explode('-',$prevmonth);
    $inpmonth = $exp[0];
    $inpyear = $exp[1];
    
    $this->load->Model('States_model');
    $state_ids = array('3','4');
    $stateData = $this->States_model->get_by_ids($state_ids);
    foreach($stateData as $states)
    {
      $state_id = $states->id;
      $this->create_high_enroll_data($state_id,$inpmonth,$inpyear);
    }
    
  }
  
  function highSparkSummaryCron3()
  {
    $prevmonth = date('m-Y', strtotime('-1 months'));
    $exp = explode('-',$prevmonth);
    $inpmonth = $exp[0];
    $inpyear = $exp[1];
    
    $this->load->Model('States_model');
    $state_ids = array('5','6');
    $stateData = $this->States_model->get_by_ids($state_ids);
    foreach($stateData as $states)
    {
      $state_id = $states->id;
      $this->create_high_enroll_data($state_id,$inpmonth,$inpyear);
    }
    
  }
  
  function create_high_enroll_data($state_id,$inpmonth,$inpyear)
  {
      $inpdate			 = '01/'.$inpmonth.'/'.$inpyear;
      $inpdate 		 = date('t/m/Y', strtotime($inpyear.'-'.$inpmonth.'-01'));

      $this->load->model('Sparks_model');
      $this->load->model('States_model');
      $this->load->model('No_visit_days_model');
      $this->load->model('school_visits_model');
      $this->load->model('Meetings_model');
      $this->load->model('Leaves_model');
      $this->load->model('Trainings_model');
      $this->load->model('Calls_model');
      $this->load->model('Users_model');
      $this->load->model('Holidays_model');
      $this->load->model('districts_model');

      $state_data = $this->States_model->getById($state_id);

      $start_date = date('Y-m-d', strtotime($inpyear.'-'.$inpmonth.'-01'));
      $start_date1 = date('Y-m-d', strtotime($inpyear.'-'.($inpmonth-2).'-01'));
      $end_date = date('Y-m-t', strtotime($inpyear.'-'.$inpmonth.'-01'));

      $users_data = $this->Users_model->getAllSparkByStateDuration($state_id, $start_date1, $end_date);
      //print_r($users_data);

      $i = 0;
      $user_ids = array();
      $user_names = array();
      foreach($users_data as $usersdata){
        $user_ids[] = $usersdata['user_id'];
        $user_names[$usersdata['user_id']]['id'] = $usersdata['user_id'];
        $user_names[$usersdata['user_id']]['name'] = $usersdata['name'];
        $i++;
      }
      //echo implode(',',$user_ids);

      //Check for Current month 
      $current_month = (Int) date("m", strtotime(date("Y-m-d")));
      $current_year = (Int) date("Y", strtotime(date("Y-m-d")));

      $session_start_year = $inpyear;
      if($inpmonth < SESSION_START_MONTH)
      {
        $session_start_year = $inpyear-1;
      }

      $_fromDate_C = date("Y-m-d",strtotime($inpyear."-".$inpmonth."-01"));
      $_toDate_C = date("Y-m-t",strtotime($inpyear."-".$inpmonth."-01"));
      $productive_days_3 = date("t",strtotime($inpyear."-".$inpmonth."-01"));
      
      foreach($user_ids as $login_id){
        $user_names[$login_id]['data'] = array();
        $user_district_ids = array();
        $user_districts = $this->Users_model->get_all_worked_districts($login_id, $state_id, $start_date1, $end_date);
        foreach($user_districts as $user_district)
        {
          array_push($user_district_ids,$user_district['district_id']);
        }
        
        $v_datac = $this->fetchSparkEnrol($state_id, $login_id, $user_district_ids, date("m",strtotime($_fromDate_C)), date("Y",strtotime($_fromDate_C)));    
        //echo"<pre>";
        //print_r($v_datac);
        if (isset($v_datac[0]))
          array_push($user_names[$login_id]['data'],$v_datac[0]);        
      }
      
      foreach($user_names as $userid=>$user_data) {
        $users = $user_data['data'];
        $average = 0;
        
        if(isset($users[0])) 
        { 
          $cond['state_id'] = $state_id;
          $cond['month_year'] = $inpyear.'-'.sprintf('%02d', $inpmonth);
          $cond['spark_id'] = $userid;
            
          $count = $this->Sparks_model->check_high_spark_summary($cond);
          if($count == 0){
            
            $userc = $users[0];
            $data['state_id'] = $state_id;
            $data['month_year'] = $inpyear.'-'.sprintf('%02d', $inpmonth);
            $data['spark_id'] = $userid;
            $data['district_ids'] = $userc->district_name;
            $data['avg_district_enrolment'] = $userc->avrg;

            $data['school_visited'] = $userc->No_of_schools_visited;
            $data['he_visit'] = $userc->HE_visit_count;
            $data['he_percent_visit'] = $userc->HE_Visit_Perct;
            $data['le_visit'] = $userc->LE_visit_count;
            $data['le_percent_visit'] = $userc->LE_Visit_Perct;
            $this->Sparks_model->save_high_spark_summary($data);
          }
          else{
            $userc = $users[0];
            $mdata['district_ids'] = $userc->district_name;
            $mdata['avg_district_enrolment'] = $userc->avrg;

            $mdata['school_visited'] = $userc->No_of_schools_visited;
            $mdata['he_visit'] = $userc->HE_visit_count;
            $mdata['he_percent_visit'] = $userc->HE_Visit_Perct;
            $mdata['le_visit'] = $userc->LE_visit_count;
            $mdata['le_percent_visit'] = $userc->LE_Visit_Perct;
            $this->Sparks_model->update_high_spark_summary($mdata, $cond);
          }
        }
      }
    }

  function fetchSparkEnrol($stateid, $userid, $district_ids, $start_month, $current_year)
  {	 
        $_stateid = $stateid; 
        $_fromDate = '01/'.$start_month.'/'.$current_year;
        $_toDate = date('t/m/Y', strtotime($current_year.'-'.$start_month.'-01'));
        $district_ids = implode(",",$district_ids);
        $district_ids = ($district_ids == "") ? 0 : $district_ids;
        $select_stmt = "SELECT n.user sid, n.name spark
	,n.district district_name
	,ifnull((n.avrg), 0) avrg
	,ifnull((n.he + n.le), 0) No_of_schools_visited
	,ifnull((n.he), 0) HE_visit_count
	,ifnull(round(((n.he / (n.he + n.le)) * 100), 0), 0) HE_Visit_Perct
	,ifnull((n.le), 0) LE_visit_count
	,ifnull(round(((n.le / (n.he + n.le)) * 100), 0), 0) LE_Visit_Perct
FROM (
	SELECT *
	FROM (
		SELECT sst.id STATE
			,ssu.id user
			,ssu.name name
			,ssd.name district
		FROM ssc_states sst
			,ssc_sparks ssu
			,ssc_districts ssd
		WHERE sst.id = '$stateid'
			AND ssu.id = '$userid'
			AND ssd.id in ($district_ids)
			AND ssu.ROLE in('field_user','manager','state_person')
		GROUP BY sst.id
			,ssu.id
			,ssu.name
			,ssd.name
		) j
	LEFT JOIN (
		SELECT f.id user_id
			,f.user_name
			,f.district_name
			,f.avrg
			,count(CASE 
					WHEN f.overallcnt = 'HE'
						THEN f.district_name
					ELSE NULL
					END) he
			,count(CASE 
					WHEN f.overallcnt = 'LE'
						THEN f.district_name
					ELSE NULL
					END) le
		FROM (
			SELECT d.id
				,d.user_name
				,d.district_name
				,d.avrg
				,ifnull((
						CASE 
							WHEN d.is_high_enrollment = 1
								THEN 'HE'
							WHEN d.is_high_enrollment = 0
								THEN 'LE'
							ELSE 0
							END
						), 0) overallcnt
			FROM (
				SELECT ssu.id
					,ssu.name user_name
					,s.*
				FROM ssc_sparks ssu
				INNER JOIN (
					SELECT w.user_id
						,w.state_id
						,w.district_id
						,ssd.name district_name
						,w.school_id
						,w.activity
						,round(w.average, 0) avrg
						,w.is_high_enrollment
						,w.no_of_students
					FROM ssc_districts ssd
					INNER JOIN (
						SELECT sv.district_id
							,sv.state_id
							,sv.school_id
							,sv.user_id
							,cast(sv.activity_date AS DATE) activity
							,r.average
							,r.no_of_students
							,r.is_high_enrollment
						FROM ssc_school_visits sv
						INNER JOIN (
							SELECT *
							FROM ssc_schools sss
							INNER JOIN (
								SELECT sss.district_id district
									,sss.avg_student average
								FROM ssc_schools sss
								GROUP BY district
								) q ON sss.district_id = q.district
							) r ON sv.school_id = r.id
							AND sv.district_id = r.district_id
              AND sv.status = 'approved'
						) w ON w.district_id = ssd.id
					WHERE DATE (w.activity) BETWEEN str_to_date('".$_fromDate."', '%d/%m/%Y')
							AND str_to_date('".$_toDate."', '%d/%m/%Y')
						AND w.state_id = '$stateid'
					) s ON ssu.id = s.user_id
				) d
			) f
		GROUP BY f.user_name
		) l ON l.user_id = j.user
	) n
ORDER BY spark";
  //if ($userid == 72)
    //echo"<hr>".$select_stmt;
		// die();
		$select_query = $this->db->query($select_stmt);
		
        //Execute query 
      
		$row = $select_query->result();
 // echo("<pre>");
		// print_r($row);
		// die(); 
        return ($row);
           
    }
       
  function training_calculater()
  {
      $this->load->model('Trainings_model');
      
      $training_date_data = array();
      $training_teacher_data = array();
      $training_dates = $this->Trainings_model->get_training_date_data();
      foreach($training_dates as $training_date)
      {
          $training_date_data[$training_date->user_id] = $training_date->date_count;
      }
      
      $training_teachers = $this->Trainings_model->get_training_teacher_data();
      foreach($training_teachers as $training_teacher)
      {
          $training_teacher_data[$training_teacher->user_id] = $training_teacher->teacher_trained;
      }

      $stateData = $this->states_model->get_all();
      $current_date = date('Y-m-d');
      foreach($stateData as $states)
      {
        $stateSparks = $this->users_model->getSparkByCurrentState($states->id, array('field_user','manager'));
        foreach($stateSparks as $stateSpark)
        {
          $get_spark_training = $this->Trainings_model->get_spark_training_report($stateSpark->user_id, '',$current_date);
          
          if(count($get_spark_training) == 0)
          {
            $data = array();
            $data['spark_id'] = $stateSpark->user_id;
            $data['spark_name'] = $stateSpark->name;
            $data['state_id'] = $stateSpark->state_id;
            $data['state_name'] = $states->name;
            $data['created_at'] = $current_date;
            $data['training_days'] = (isset($training_date_data[$stateSpark->user_id]) ? $training_date_data[$stateSpark->user_id] : 0);
            $data['teacher_trained'] = (isset($training_teacher_data[$stateSpark->user_id]) ? $training_teacher_data[$stateSpark->user_id] : 0);
            $this->Trainings_model->save_spark_training_report($data);
          }
          else{
            $data = array();
            $data['spark_name'] = $stateSpark->name;
            $data['state_id'] = $stateSpark->state_id;
            $data['state_name'] = $states->name;
            $data['training_days'] = (isset($training_date_data[$stateSpark->user_id]) ? $training_date_data[$stateSpark->user_id] : 0);
            $data['teacher_trained'] = (isset($training_teacher_data[$stateSpark->user_id]) ? $training_teacher_data[$stateSpark->user_id] : 0);
            $this->Trainings_model->update_spark_training_ranking($data, $get_spark_training[0]->id);
          }
          
        }
      }
      
      $rating = 0;
      $rating_inc = 0;
      $training_days = 0; 
      $national_trainings = $this->Trainings_model->get_spark_training_report('', '', $current_date, 'training_days desc');
      foreach($national_trainings as $national_training)
      {
        $rating_inc = $rating_inc + 1;
        if($training_days != $national_training->training_days){
          $rating = $rating + $rating_inc;
          $rating_inc = 0;
          $training_days = $national_training->training_days;
        }
        
        $rankingData['national_ranking'] = $rating;
        $this->Trainings_model->update_spark_training_ranking($rankingData, $national_training->id);
      }
      
      $state_trainings = $this->Trainings_model->get_spark_training_report('', '', $current_date, 'state_id asc, training_days desc');
      //echo"<pre>";
      //print_r($state_trainings);
      $state_id = '';
      foreach($state_trainings as $state_training)
      {
        if($state_training->state_id != $state_id){
          $rating_inc = 0;
          $training_days = 0;
          $rating = 0;
          $state_id = $state_training->state_id;
        }
        $rating_inc = $rating_inc + 1;
        if($training_days != $state_training->training_days){
          $rating = $rating + $rating_inc;
          $rating_inc = 0;
          $training_days = $state_training->training_days;
        }
        
        $srankingData['state_ranking'] = $rating;
        $this->Trainings_model->update_spark_training_ranking($srankingData, $state_training->id);
      }
      
  }   

  function spark_training_report()
  {
      $this->load->model('Trainings_model');
      
      $current_date = date('Y-m-d');
      $national_trainings = $this->Trainings_model->get_spark_training_report('', '', $current_date, 'training_days desc');
      
      $data = array();
      $rating = 0;
      $training_days = 0; 
      $message = "*Ranking* | *Training Days* | *Spark*";
      foreach($national_trainings as $national_training)
      {
        if($training_days != $national_training->training_days){
          $rating++;
          $training_days = $national_training->training_days;
        }
        if($national_training->training_days == 0){
          $rating = '-';
        }
        $message .= "\n".$rating." *|* ".$national_training->training_days." *|* ".$national_training->spark_name;
      }
      //$message .= '</table>';
      
      $apiToken = "878987885:AAHxJiQ5rq7D-xxyi2e8H2_o1zElNhCOnRs";
      $postData = array('chat_id' => '-255481027', 'parse_mode'=>'MARKDOWN','text' => $message);
      
      $this->send_telegaram_message($apiToken, $postData);
      
  }
  
  function district_rating()
	{
    $this->load->Model('Teacher_feedback_model');
    
    $teacher_feedbacks = $this->Teacher_feedback_model->get_all();
    
    $district_ratings = array();
    if (count($teacher_feedbacks) > 0)
    {
      foreach($teacher_feedbacks as $teacher_feedback)
      {
        if (!isset($district_ratings[$teacher_feedback->district_id]))
        {
          $district_ratings[$teacher_feedback->district_id]['name'] = $teacher_feedback->name;
          $district_ratings[$teacher_feedback->district_id]['total'] = 0;
          $district_ratings[$teacher_feedback->district_id]['ratings'] = array();
        }
        $district_ratings[$teacher_feedback->district_id]['ratings'][$teacher_feedback->question_id] = round($teacher_feedback->avg_rating,1);
        $district_ratings[$teacher_feedback->district_id]['total'] = $district_ratings[$teacher_feedback->district_id]['total'] + round($teacher_feedback->avg_rating,1);
      }
    }
    
     $message = "*Ranking* | *District*";
     foreach($district_ratings as $district_rating) {
       $message .= "\n".round($district_rating['total']/7,1)." | ".$district_rating['name'];
     } 
     
      $apiToken = "878987885:AAHxJiQ5rq7D-xxyi2e8H2_o1zElNhCOnRs";
      $postData = array('chat_id' => '-255481027', 'parse_mode'=>'MARKDOWN','text' => $message);
      
      $this->send_telegaram_message($apiToken, $postData); 
    //print_r($district_ratings);

	}
  
  function spark_feedback()
  {
    $this->load->model('Spark_feedback_model');
    $spark_feedbacks = $this->Spark_feedback_model->spark_feedback_new();
    
    $message = "*Ranking* | *Spark*";
    foreach($spark_feedbacks as $spark_feedback) { 
      $ranking = '-';
      if($spark_feedback->total_feedbacks > 0){
         $ranking = $spark_feedback->national_rating;
      }
      $message .= "\n".$ranking." | ".$spark_feedback->spark_name;
    } 
    $apiToken = "878987885:AAHxJiQ5rq7D-xxyi2e8H2_o1zElNhCOnRs";
    $postData = array('chat_id' => '-255481027', 'parse_mode'=>'MARKDOWN','text' => $message);
    
    $this->send_telegaram_message($apiToken, $postData); 
  }
  
  function send_telegaram_message($apiToken, $postData)
  {
    $url = "https://api.telegram.org/bot$apiToken/sendMessage";

    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    //curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
    $response = curl_exec($ch);
    $result_array = json_decode($response);
    curl_close($ch);
    
  }
  
  function getDistrictState()
  {
      $lat1 = '24.5512';
      $long1 = '84.2832';
    $details = "https://maps.googleapis.com/maps/api/geocode/json?latlng=".$lat1.",".$long1."&sensor=false&key=AIzaSyAQBbK2ICEZErl3kTEcAtGOpls3U0N9WhI";
   echo '<pre>'; //http://maps.googleapis.com/maps/api/geocode/json?latlng=44.42514,26.10540&sensor=false&key=AIzaSyAy31bXa5Fqhvx1waKyP-_RyfBfoFmA_VM
      $json = file_get_contents($details);
      $details = json_decode($json, TRUE);
      print_r($details);
      
  }
  
  function get_google_distance($lat1, $long1, $lat2, $long2)
  {
    $details = "https://maps.googleapis.com/maps/api/directions/json?origin=".$lat1.",".$long1."&destination=".$lat2.",".$long2."&mode=driving&alternatives=true&sensor=false&key=".GOOGLE_API_KEY;
    
      $json = file_get_contents($details);
      $details = json_decode($json, TRUE);
      
      $routes = $details['routes'];
      $countRoutes = count($routes);
      
      $shortest_distance = 0;
      $end_address = '';
      $end_address = '';
      for($i=0; $i<$countRoutes; $i++)
      {
        $distance = $routes[$i]['legs'][0]['distance']['text'];
        $source_address = $routes[$i]['legs'][0]['start_address'];
        $end_address = $routes[$i]['legs'][0]['end_address'];
        $exp = explode(" ",$distance);
        $distance_val = $exp[0];
        $distance_param = $exp[1];
        if($distance_param == 'km')
        {
          $distance_val = $distance_val*1000;
        }
        else if($distance_param == 'mi'){
          $distance_val = $distance_val*1609;
        }
        if($i == 0){
          $shortest_distance = $distance_val;
        }else{  
          if($distance_val < $shortest_distance){
            $shortest_distance = $distance_val;
          }
        }
      }
      $returnData['distance'] = round($shortest_distance,2);
      $returnData['destination_address'] = $end_address;
      $returnData['source_address'] = $source_address;
      return $returnData; 
  }

  function set_trainings_mode()
  {
    $this->load->Model('trainings_model');
    $this->load->Model('user_district_blocks_model');
    $trainingData = $this->trainings_model->get_all_trainings();
    
    foreach($trainingData as $trainings)
    {
      
      $training_id = $trainings->id;
      $user_id = $trainings->user_id;
      $block_id = $trainings->block_id;
      $user_block_count = $this->user_district_blocks_model->get_count($user_id, $block_id);
      
      //echo"<br>total : ".$user_block_count[0]->total;
      $mode = ($user_block_count[0]->total > 0 ? 'internal' : 'external');
      
      $data['mode'] = $mode;
      $this->trainings_model->update_training_data($data, $training_id);
    }
  }     
  
  function save_teacher_feedback_rankings()
  {
    $this->load->Model('Teacher_acknowledge_model');
    
    $teacher_feedbacks_all = $this->Teacher_acknowledge_model->select_all_ack();
    //print_r($teacher_feedbacks_all);
    if(!empty($teacher_feedbacks_all)){
      foreach($teacher_feedbacks_all as $feedback_all)
      {
        $data = array();
        $data['state_id'] = '';
        $data['district_id'] = $feedback_all->district_id;
        $data['district_name'] = $feedback_all->name;
        $data['question_id'] = $feedback_all->question_id;
        $data['avg_rating'] = $feedback_all->avg_rating;
        $data['created_at'] = date('Y-m-d');
        
        $mdata['state_id'] = 0;
        $mdata['district_id'] = $feedback_all->district_id;
        $mdata['question_id'] = $feedback_all->question_id;
        $mdata['created_at'] = date('Y-m-d');
        $chk_record = $this->Teacher_acknowledge_model->check_feedback_data($mdata);
        if($chk_record == 0){
          $this->Teacher_acknowledge_model->insert_teacher_feedback_rankings($data);
        }
      }
    }
    
    $states = $this->states_model->get_all();
    
    foreach($states as $state)
    {
      $teacher_feedbacks = $this->Teacher_acknowledge_model->select_all_ack($state->id);

      if(!empty($teacher_feedbacks)){
        foreach($teacher_feedbacks as $feedback)
        {
          $data = array();
          $data['state_id'] = $state->id;
          $data['district_id'] = $feedback->district_id;
          $data['district_name'] = $feedback->name;
          $data['question_id'] = $feedback->question_id;
          $data['avg_rating'] = $feedback->avg_rating;
          $data['created_at'] = date('Y-m-d');
          
          $mdata1['state_id'] = $state->id;
          $mdata1['district_id'] = $feedback->district_id;
          $mdata1['question_id'] = $feedback->question_id;
          $mdata1['created_at'] = date('Y-m-d');
          $chk_record = $this->Teacher_acknowledge_model->check_feedback_data($mdata1);
          if($chk_record == 0){
            $this->Teacher_acknowledge_model->insert_teacher_feedback_rankings($data);
          }
        }
      }
    }
  }

  function testline_responses()
  {
    $this->load->Model('Test_questions_model');
    $this->load->Model('schools_model');
    if ($this->uri->total_segments() == 4)
      $limit = $this->uri->segment(4);
    else
      $limit = 0;

    $test_data = $this->Test_questions_model->get_all_testline_data($limit);

    $school_ids = array();
    $question_ids = array();
    $response_arr = array();
    $correct = 0;
    $incorrect = 0;
    foreach($test_data as $test) {

      $test_responses = $this->Test_questions_model->get_all_testline_responses($test->id);

      $student_count_key = 'total_students_'.$test->subject_id.'_'.$test->class_id;
      if (!in_array($test->school_id,$school_ids)) {
        array_push($school_ids,$test->school_id);
        $response_arr[$test->school_id]['data'] = array();
      }
      
      if (array_key_exists($student_count_key,$response_arr[$test->school_id])) {
        $response_arr[$test->school_id][$student_count_key] = $response_arr[$test->school_id][$student_count_key] + 1;
      } else {
        $response_arr[$test->school_id][$student_count_key] = 1;
      }

      if (count($test_responses) > 0) 
      {        
        foreach($test_responses as $test_response) {
          if (!in_array($test_response->question_id,$question_ids)) {
            array_push($question_ids,$test_response->question_id);
          }

          if (!array_key_exists($test_response->question_id,$response_arr[$test->school_id]['data'])) {
            $response_arr[$test->school_id]['data'][$test_response->question_id] = array();
            $response_arr[$test->school_id]['data'][$test_response->question_id]['test_ids'] = array();
            $response_arr[$test->school_id]['data'][$test_response->question_id]['right_attempt'] = 0;
            $response_arr[$test->school_id]['data'][$test_response->question_id]['wrong_attempt'] = 0;
            $response_arr[$test->school_id]['data'][$test_response->question_id]['total_students'] = 0;
          }
          array_push($response_arr[$test->school_id]['data'][$test_response->question_id]['test_ids'],$test->id);
          $response_arr[$test->school_id]['data'][$test_response->question_id]['subject_id'] = $test->subject_id;
          $response_arr[$test->school_id]['data'][$test_response->question_id]['class_id'] = $test->class_id;
          $response_arr[$test->school_id]['data'][$test_response->question_id]['user_id'] = $test->spark_id;
          $response_arr[$test->school_id]['data'][$test_response->question_id]['concept_id'] = 0;
          $response_arr[$test->school_id]['data'][$test_response->question_id]['state_id'] = $test->state_id;
          $response_arr[$test->school_id]['data'][$test_response->question_id]['district_id'] = $test->district_id;
          $response_arr[$test->school_id]['data'][$test_response->question_id]['block_id'] = $test->block_id;
          $response_arr[$test->school_id]['data'][$test_response->question_id]['cluster_id'] = $test->cluster_id;
          $response_arr[$test->school_id]['data'][$test_response->question_id]['test_date'] = $test->date_of_test;
          $response_arr[$test->school_id]['data'][$test_response->question_id]['created_at'] = $test->created_at;
          
          if($test_response->is_correct == 1)
            $response_arr[$test->school_id]['data'][$test_response->question_id]['right_attempt'] = $response_arr[$test->school_id]['data'][$test_response->question_id]['right_attempt']+1;
          else  
            $response_arr[$test->school_id]['data'][$test_response->question_id]['wrong_attempt'] = $response_arr[$test->school_id]['data'][$test_response->question_id]['wrong_attempt']+1;
          
          $response_arr[$test->school_id]['data'][$test_response->question_id]['total_students'] = $response_arr[$test->school_id]['data'][$test_response->question_id]['total_students'] + 1;
        }
      }
      else
      {
        $test_questions = $this->Test_questions_model->getByStateSubjectClassType($test->state_id, $test->subject_id, $test->class_id, 'base_line');

        foreach($test_questions as $test_response) {
          if (!in_array($test_response->id,$question_ids)) {
            array_push($question_ids,$test_response->id);
          }

          if (!array_key_exists($test_response->id,$response_arr[$test->school_id]['data'])) {
            $response_arr[$test->school_id]['data'][$test_response->id] = array();
            $response_arr[$test->school_id]['data'][$test_response->id]['test_ids'] = array();
            $response_arr[$test->school_id]['data'][$test_response->id]['right_attempt'] = 0;
            $response_arr[$test->school_id]['data'][$test_response->id]['wrong_attempt'] = 0;
            $response_arr[$test->school_id]['data'][$test_response->id]['total_students'] = 0;
          }
          array_push($response_arr[$test->school_id]['data'][$test_response->id]['test_ids'],$test->id);
          $response_arr[$test->school_id]['data'][$test_response->id]['subject_id'] = $test->subject_id;
          $response_arr[$test->school_id]['data'][$test_response->id]['class_id'] = $test->class_id;
          $response_arr[$test->school_id]['data'][$test_response->id]['user_id'] = $test->spark_id;
          $response_arr[$test->school_id]['data'][$test_response->id]['concept_id'] = 0;
          $response_arr[$test->school_id]['data'][$test_response->id]['state_id'] = $test->state_id;
          $response_arr[$test->school_id]['data'][$test_response->id]['district_id'] = $test->district_id;
          $response_arr[$test->school_id]['data'][$test_response->id]['block_id'] = $test->block_id;
          $response_arr[$test->school_id]['data'][$test_response->id]['cluster_id'] = $test->cluster_id;
          $response_arr[$test->school_id]['data'][$test_response->id]['test_date'] = $test->date_of_test;
          $response_arr[$test->school_id]['data'][$test_response->id]['created_at'] = $test->created_at;
          
          $response_arr[$test->school_id]['data'][$test_response->id]['wrong_attempt'] = $response_arr[$test->school_id]['data'][$test_response->id]['wrong_attempt']+1;
          
          $response_arr[$test->school_id]['data'][$test_response->id]['total_students'] = $response_arr[$test->school_id]['data'][$test_response->id]['total_students'] + 1;
        }
      }
    }
    
    if (count($question_ids) > 0)
    {
      $questions = $this->Test_questions_model->getQuestionConcepts($question_ids);
      $question_details = array();
      foreach($questions as $question) 
      {
        $question_details[$question->id] = array();
        $question_details[$question->id]['concept_id'] = $question->concept_id;
      }
    }

    foreach($response_arr as $school_id=>$res_data)
    {
      $ques_data = $res_data['data'];
      foreach($ques_data as $question_id=>$data)
      {
         $total_students = $res_data['total_students_'.$data['subject_id'].'_'.$data['class_id']];
         $previous_data = $this->Test_questions_model->check_base_line_test_responses($school_id, $data['class_id'], $data['subject_id'], $question_id);
          
         if(empty($previous_data))
         {
           $sdata['subject_id'] = $data['subject_id'];
           $sdata['class_id'] = $data['class_id'];
           $sdata['user_id'] = $data['user_id'];
           $sdata['concept_id'] = $question_details[$question_id]['concept_id'];
           $sdata['school_id'] = $school_id;
           $sdata['question_id'] = $question_id;
           $sdata['students_taking_test'] = $total_students;
           $sdata['not_attempted'] = 0;
           $sdata['wrong_attempted'] = $data['wrong_attempt'];
           $sdata['right_attempted'] = $data['right_attempt'];
           $sdata['state_id'] = $data['state_id'];
           $sdata['district_id'] = $data['district_id'];
           $sdata['block_id'] = $data['block_id'];
           $sdata['cluster_id'] = $data['cluster_id'];
           $sdata['test_date'] = (!empty($data['test_date']) ? $data['test_date'] : date('Y-m-d')) ;
           $sdata['created_at'] = $data['created_at'];
           
           $this->Test_questions_model->add_end_base_line_test_responses('ssc_base_line_test_responses', $sdata);
         } 
         else
         {
           $privious_id = $previous_data['id'];
           
           $mdata['wrong_attempted'] = $data['wrong_attempt'] + $previous_data['wrong_attempted'];
           $mdata['right_attempted'] = $data['right_attempt'] + $previous_data['right_attempted'];
           $mdata['students_taking_test'] = $total_students + $previous_data['students_taking_test'];
           $this->Test_questions_model->update_end_base_line_test_responses('ssc_base_line_test_responses', $mdata, $privious_id);
         }
         $this->Test_questions_model->update_responded($data['test_ids']);
      }
    }
    
    echo "DONE";
    //print_r($response_arr);
    //print_r($response_array);
  }
  
  function ackdelivery_to_schoolteachers()
  {
		$this->load->Model('schools_model');	
		$ackData = $this->schools_model->get_ack_delivery_data();
		
		
		foreach($ackData as $ack_data)
		{
			$ack_id = $ack_data->id;
			
			$cond = array('dise_code'=>$ack_data->dise_code, 'contact_number'=>$ack_data->contact_no);
			$check_teacher = $this->schools_model->get_school_teachers($cond);
		
			if(count($check_teacher) == 0)
			{
				$data['school_name'] = $ack_data->name;
				$data['dise_code'] = $ack_data->dise_code;
				$data['teacher_name'] = $ack_data->teacher_name;
				$data['contact_number'] = $ack_data->contact_no;
				$data['activity_date'] = $ack_data->issue_date;
				$data['school_id'] = $ack_data->school_id;
				$data['state_id'] = $ack_data->state_id;
				$data['district_id'] = $ack_data->district_id;
				$data['block_id'] = $ack_data->block_id;
				$data['cluster_id'] = $ack_data->cluster_id;
				
				$this->schools_model->add_school_teachers($data);
			}
			
			$mdata = array('school_teachers_flag'=>1);
			$this->schools_model->update_ack_info($mdata, $ack_id);
		}
		
	}  

  
	function save_sss_app_usage_back()
	{
		$this->load->Model('schools_model');
		
		$report_dates = $this->return_report_dates();
    
    $year_start_date = $report_dates['year_start_date'];
    $month_start_date = $report_dates['month_start_date'];
    $current_date = $report_dates['current_date'];
    $month_year = $report_dates['month_year'];
		
		$url_type = (isset($_GET['type']) ? $_GET['type'] : 'prod');
		
		if($url_type == 'prod'){
			$baseurl = "http://ssscmsapi.samparksmartshala.org:8082";
		}
		else{
			$baseurl = "http://3.6.188.10:8181";
		}
		
		$url1 = "$baseurl/apis/userlikes?date=".$current_date;
		$this->schools_model->remove_date('cron_validations', array('cron_name'=>'userlikes'));
		$this->schools_model->add_data('cron_validations', array('cron_name'=>'userlikes', 'cron_start'=> date('Y-m-d H:i:s')));
		$result1 = $this->get_sss_app_usage($url1, 'total_likes', $month_year);
		if($result1 == 'success'){
			$this->schools_model->update_data('cron_validations', array('cron_end'=> date('Y-m-d H:i:s')), array('cron_name'=>'userlikes'));
		
			$url2 = "$baseurl/apis/userposts?date=".$current_date;
			$this->schools_model->remove_date('cron_validations', array('cron_name'=>'userposts'));
			$this->schools_model->add_data('cron_validations', array('cron_name'=>'userposts', 'cron_start'=> date('Y-m-d H:i:s')));
			$result2 = $this->get_sss_app_usage($url2, 'total_posts', $month_year);
			
			if($result2 == 'success'){
				$this->schools_model->update_data('cron_validations', array('cron_end'=> date('Y-m-d H:i:s')), array('cron_name'=>'userposts'));
				
				$url3 = "$baseurl/apis/usercomments?date=".$current_date;
				$this->schools_model->remove_date('cron_validations', array('cron_name'=>'usercomments'));
				$this->schools_model->add_data('cron_validations', array('cron_name'=>'usercomments', 'cron_start'=> date('Y-m-d H:i:s')));
				$result3 = $this->get_sss_app_usage($url3, 'total_comments', $month_year);
				
				if($result3 == 'success'){
					$this->schools_model->update_data('cron_validations', array('cron_end'=> date('Y-m-d H:i:s')), array('cron_name'=>'usercomments'));
					
					$url5 = "$baseurl/apis/useritems?date=".$current_date;
					$this->schools_model->remove_date('cron_validations', array('cron_name'=>'useritems'));
					$this->schools_model->add_data('cron_validations', array('cron_name'=>'useritems', 'cron_start'=> date('Y-m-d H:i:s')));
					$result5 = $this->get_sss_app_usage($url5, 'items_viewed', $month_year);
					
					if($result5 == 'success'){
						$this->schools_model->update_data('cron_validations', array('cron_end'=> date('Y-m-d H:i:s')), array('cron_name'=>'useritems'));
						
						$url4 = "$baseurl/apis/uservideos?date=".$current_date;
						$this->schools_model->remove_date('cron_validations', array('cron_name'=>'uservideos'));
						$this->schools_model->add_data('cron_validations', array('cron_name'=>'uservideos', 'cron_start'=> date('Y-m-d H:i:s')));
						$result4 = $this->get_sss_app_usage($url4, 'video_viewed', $month_year);
						if($result4 == 'success')
							$this->schools_model->update_data('cron_validations', array('cron_end'=> date('Y-m-d H:i:s')), array('cron_name'=>'uservideos'));
					}
				}
			}
		}
		
		$this->update_app_usage_other_details();
	}
	
	
	function update_sss_app_usage()
	{
		$this->load->Model('schools_model');
		
		$report_dates = $this->return_report_dates();
    
    $year_start_date = $report_dates['year_start_date'];
    $month_start_date = $report_dates['month_start_date'];
    $current_date = $report_dates['current_date'];
    $month_year = $report_dates['month_year'];
		
		$url_type = (isset($_GET['type']) ? $_GET['type'] : 'prod');
		
		if($url_type == 'prod'){
			$baseurl = "http://ssscmsapi.samparksmartshala.org:8082";
		}
		else{
			$baseurl = "http://3.6.188.10:8181";
		}
		
		$apptype = (isset($_GET['apptype']) ? $_GET['apptype'] : 'items_viewed');
		
		if($apptype == 'userlikes'){
			$url1 = "$baseurl/apis/userlikes?date=".$current_date;
			$this->schools_model->remove_date('cron_validations', array('cron_name'=>'userlikes'));
			$this->schools_model->add_data('cron_validations', array('cron_name'=>'userlikes', 'cron_start'=> date('Y-m-d H:i:s')));
			$result1 = $this->get_sss_app_usage($url1, 'total_likes', $month_year);
			if($result1 == 'success')
			$this->schools_model->update_data('cron_validations', array('cron_end'=> date('Y-m-d H:i:s')), array('cron_name'=>'userlikes'));
		}
		
		if($apptype == 'userposts'){
			$url2 = "$baseurl/apis/userposts?date=".$current_date;
			$this->schools_model->remove_date('cron_validations', array('cron_name'=>'userposts'));
			$this->schools_model->add_data('cron_validations', array('cron_name'=>'userposts', 'cron_start'=> date('Y-m-d H:i:s')));
			$result2 = $this->get_sss_app_usage($url2, 'total_posts', $month_year);
			if($result2 == 'success')
			$this->schools_model->update_data('cron_validations', array('cron_end'=> date('Y-m-d H:i:s')), array('cron_name'=>'userposts'));
		}
		
		if($apptype == 'usercomments'){
			$url3 = "$baseurl/apis/usercomments?date=".$current_date;
			$this->schools_model->remove_date('cron_validations', array('cron_name'=>'usercomments'));
			$this->schools_model->add_data('cron_validations', array('cron_name'=>'usercomments', 'cron_start'=> date('Y-m-d H:i:s')));
			$result3 = $this->get_sss_app_usage($url3, 'total_comments', $month_year);
			if($result3 == 'success')
			$this->schools_model->update_data('cron_validations', array('cron_end'=> date('Y-m-d H:i:s')), array('cron_name'=>'usercomments'));
		}
		
		if($apptype == 'uservideos'){
			$url4 = "$baseurl/apis/uservideos?date=".$current_date;
			$this->schools_model->remove_date('cron_validations', array('cron_name'=>'uservideos'));
			$this->schools_model->add_data('cron_validations', array('cron_name'=>'uservideos', 'cron_start'=> date('Y-m-d H:i:s')));
			$result4 = $this->get_sss_app_usage($url4, 'video_viewed', $month_year);
			if($result4 == 'success')
			$this->schools_model->update_data('cron_validations', array('cron_end'=> date('Y-m-d H:i:s')), array('cron_name'=>'uservideos'));
		}
		
		if($apptype == 'useritems'){
			$url5 = "$baseurl/apis/useritems?date=".$current_date;
			$this->schools_model->remove_date('cron_validations', array('cron_name'=>'useritems'));
			$this->schools_model->add_data('cron_validations', array('cron_name'=>'useritems', 'cron_start'=> date('Y-m-d H:i:s')));
			$result5 = $this->get_sss_app_usage($url5, 'items_viewed', $month_year);
			if($result5 == 'success')
			$this->schools_model->update_data('cron_validations', array('cron_end'=> date('Y-m-d H:i:s')), array('cron_name'=>'useritems'));
		}
	}
	
	function save_sss_app_usage()
	{
		$this->load->Model('schools_model');
		
		$report_dates = $this->return_report_dates();
    
    $year_start_date = $report_dates['year_start_date'];
    $month_start_date = $report_dates['month_start_date'];
    $current_date = $report_dates['current_date'];
    $month_year = $report_dates['month_year'];
		
		$url_type = (isset($_GET['type']) ? $_GET['type'] : 'prod');

		$baseurl = "http://ssscmsapi.samparksmartshala.org:8082/apis";
		
		$tbl_name = 'cron_validations';
		
		$postData = $this->schools_model->get_data($tbl_name, array('cron_name'=>'userposts', 'Date(cron_start)'=> date('Y-m-d')));
		if(empty($postData)){
		$url2 = "$baseurl/userposts?date=".$current_date;
		$this->schools_model->remove_date($tbl_name, array('cron_name'=>'userposts'));
		$this->schools_model->add_data($tbl_name, array('cron_name'=>'userposts', 'cron_start'=> date('Y-m-d H:i:s')));
		$result2 = $this->get_sss_app_usage($url2, 'total_posts', $month_year);
		$this->schools_model->update_data($tbl_name, array('cron_end'=> date('Y-m-d H:i:s'), 'status'=>$result2), array('cron_name'=>'userposts'));
		}			
		$commentData = $this->schools_model->get_data($tbl_name, array('cron_name'=>'usercomments', 'Date(cron_start)'=> date('Y-m-d')));
		if(empty($commentData)){
		$url3 = "$baseurl/usercomments?date=".$current_date;
		$this->schools_model->remove_date($tbl_name, array('cron_name'=>'usercomments'));
		$this->schools_model->add_data($tbl_name, array('cron_name'=>'usercomments', 'cron_start'=> date('Y-m-d H:i:s')));
		$result3 = $this->get_sss_app_usage($url3, 'total_comments', $month_year);
		$this->schools_model->update_data($tbl_name, array('cron_end'=> date('Y-m-d H:i:s'), 'status'=>$result3), array('cron_name'=>'usercomments'));
		}		
		$itemsData = $this->schools_model->get_data($tbl_name, array('cron_name'=>'useritems', 'Date(cron_start)'=> date('Y-m-d')));
		if(empty($itemsData)){
		$url5 = "$baseurl/useritems?date=".$current_date;
		$this->schools_model->remove_date($tbl_name, array('cron_name'=>'useritems'));
		$this->schools_model->add_data($tbl_name, array('cron_name'=>'useritems', 'cron_start'=> date('Y-m-d H:i:s')));
		$result5 = $this->get_sss_app_usage($url5, 'items_viewed', $month_year);
		$this->schools_model->update_data($tbl_name, array('cron_end'=> date('Y-m-d H:i:s'), 'status'=>$result5), array('cron_name'=>'useritems'));
		}
		$likesData = $this->schools_model->get_data($tbl_name, array('cron_name'=>'userlikes', 'Date(cron_start)'=> date('Y-m-d')));
		if(empty($likesData)){
		$url1 = "$baseurl/userlikes?date=".$current_date;
		$this->schools_model->remove_date($tbl_name, array('cron_name'=>'userlikes'));
		$this->schools_model->add_data($tbl_name, array('cron_name'=>'userlikes', 'cron_start'=> date('Y-m-d H:i:s')));
		$result1 = $this->get_sss_app_usage($url1, 'total_likes', $month_year);
		$this->schools_model->update_data($tbl_name, array('cron_end'=> date('Y-m-d H:i:s'), 'status'=>$result1), array('cron_name'=>'userlikes'));
		}
		$videosData = $this->schools_model->get_data($tbl_name, array('cron_name'=>'uservideos', 'Date(cron_start)'=> date('Y-m-d')));
		if(empty($videosData)){
		$url4 = "$baseurl/uservideos?date=".$current_date;
		$this->schools_model->remove_date($tbl_name, array('cron_name'=>'uservideos'));
		$this->schools_model->add_data($tbl_name, array('cron_name'=>'uservideos', 'cron_start'=> date('Y-m-d H:i:s')));
		$result4 = $this->get_sss_app_usage($url4, 'video_viewed', $month_year);
		$this->schools_model->update_data($tbl_name, array('cron_end'=> date('Y-m-d H:i:s'), 'status'=>$result4), array('cron_name'=>'uservideos'));
		}
		
		$this->schools_model->remove_date($tbl_name, array('cron_name'=>'update_register_details'));
		$this->schools_model->add_data($tbl_name, array('cron_name'=>'update_register_details', 'cron_start'=> date('Y-m-d H:i:s')));
    
    $this->update_app_usage_other_details();
    
    $this->schools_model->update_data($tbl_name, array('cron_end'=> date('Y-m-d H:i:s'), 'status'=>'Success'), array('cron_name'=>'update_register_details'));
    
	}
		
	
	function get_sss_app_usage($url, $field_type, $month_year)
  {
		$this->load->Model('schools_model');
		
		$curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_HEADER, false);
    // execute and return string (this should be an empty string '')
    $result = curl_exec($curl);
    
		if (curl_errno($curl)) {
				$error_msg = curl_error($curl);
		}
    curl_close($curl);
    
    if(isset($error_msg))
    {
			return $error_msg;
		}
		
		// the value of $str is actually bool(true), not empty string ''
		$resultArray = json_decode($result);
		
		$data = array();
		if(!empty($resultArray)){
			
			$userstats = $resultArray->userstats;
			foreach($userstats as $usertype=>$phonearray)
			{
				foreach($phonearray as $phone_number=>$value)
				{
					if($usertype == 'spark')
					{
						$cond_usage = array('phone_number'=>$phone_number, 'month_year'=>$month_year);
						$usageData = $this->schools_model->get_data('app_usage_spark_details', $cond_usage);
						
						if(empty($usageData)){

							$data['phone_number'] = $phone_number;
							$data['usertype'] = $usertype;
							if($field_type == 'video_viewed'){
								$data['video_viewed_actual'] = $value;
								$data[$field_type] = $value/(60*60);
							}
							else{
								$data[$field_type] = $value;
							}
							$data['month_year'] = $month_year;
							$data['created_at'] = date('Y-m-d H:i:s');
							$this->schools_model->addSparkAppUsage($data);
						}
						else{
							$usage_id = $usageData[0]->id;
							if($field_type == 'video_viewed'){
								$mdata['video_viewed_actual'] = $value;
								$mdata[$field_type] = $value/(60*60);
							}
							else{
								$mdata[$field_type] = $value;
							}
							$this->schools_model->update_data('app_usage_spark_details', $mdata, array('id'=>$usage_id));
						}
					}
					else{
						$cond_usage = array('phone_number'=>$phone_number, 'month_year'=>$month_year);
						$usageData = $this->schools_model->get_data('app_usage_details', $cond_usage);
						
						if(empty($usageData)){
							$data['phone_number'] = $phone_number;
							$data['usertype'] = $usertype;
							if($field_type == 'video_viewed'){
								$data['video_viewed_actual'] = $value;
								$data[$field_type] = $value/(60*60);
							}
							else{
								$data[$field_type] = $value;
							}
							$data['month_year'] = $month_year;
							$data['created_at'] = date('Y-m-d H:i:s');
							$this->schools_model->addSSSAppUsage($data);
						}
						else{
							$usage_id = $usageData[0]->id;
							if($field_type == 'video_viewed'){
								$mdata['video_viewed_actual'] = $value;
								$mdata[$field_type] = $value/(60*60);
							}
							else{
								$mdata[$field_type] = $value;
							}
							$this->schools_model->update_data('app_usage_details', $mdata, array('id'=>$usage_id));
						}
					}
				}
			}
			return 'success';
		}
		else{
			return 'error';
		}
	}
	
	function update_app_usage_other_details()
	{
		$report_dates = $this->return_report_dates();
    
    $year_start_date = $report_dates['year_start_date'];
    $month_start_date = $report_dates['month_start_date'];
    $current_date = $report_dates['current_date'];
    $month_year = $report_dates['month_year'];
    
		$this->save_app_usage_teacher_data('app_usage_spark_details', $month_year);
		$this->save_app_usage_teacher_data('app_usage_details', $month_year);
	}
		
	function save_app_usage_teacher_data($table_name, $month_year)
	{
		$this->load->Model('schools_model');
		
		//$table_name = 'app_usage_spark_details';
		$cond_usage = array('diseCode'=>Null, 'month_year'=>$month_year);
		$usageData = $this->schools_model->get_data($table_name, $cond_usage);							
		
		if(!empty($usageData)){
			foreach($usageData as $udata)
			{
				$usage_id = $udata->id;
				$phone_number = $udata->phone_number;
				$month_year = $udata->month_year;
				
				$cond = array('phone_number'=>$phone_number);
				$teacherData = $this->schools_model->get_data('app_registrations', $cond);
				
				if(!empty($teacherData)){	
					
					$data = array();
					$teacher_result = $teacherData[0];		
					$data['usertype'] = $teacher_result->usertype;
					$data['fullName'] = $teacher_result->fullName;
					$data['pincode'] = $teacher_result->pincode;
					$data['empCode'] = $teacher_result->empCode;
					$data['diseCode'] = $teacher_result->diseCode;
					$data['state_id'] = $teacher_result->state_id;
					$data['district_id'] = $teacher_result->district_id;
					$data['block_id'] = $teacher_result->block_id;
					$data['cluster_id'] = $teacher_result->cluster_id;
					$data['school_id'] = $teacher_result->school_id;
					
					$this->schools_model->update_data($table_name, $data, array('id'=>$usage_id));
				}
			}
		}
		
	}

	function get_sss_app_usage_back($url, $field_type, $month_year)
  {
		$this->load->Model('schools_model');
		
		//$result = file_get_contents($url);
		//$resultArray = json_decode($result);
		
    $curl = curl_init();
   
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_HEADER, false);

    // execute and return string (this should be an empty string '')
    $result = curl_exec($curl);

    curl_close($curl);

    // the value of $str is actually bool(true), not empty string ''
    //var_dump($str);
		$resultArray = json_decode($result);
		
		//print_r($resultArray);
		//exit;
		
		//$month_year = date('m_Y', strtotime($month_date));
		
		$insert_spark = 0;
		$insert_other = 0;
		$update_spark = 0; 
		$update_other = 0; 
		$data = array();
		if(!empty($resultArray)){
			
			$userstats = $resultArray->userstats;
			foreach($userstats as $usertype=>$phonearray)
			{
				//echo "<br>".$usertype;
				//print_r($phonearray);
				foreach($phonearray as $phone_number=>$value)
				{
					$cond = array('phone_number'=>$phone_number);
					$teacherData = $this->schools_model->get_data('app_registrations', $cond);
					
					if(count($teacherData) > 0){
						
						$usertype = $teacherData[0]->usertype;
						
						if($usertype == 'spark')
						{
							$cond_usage = array('phone_number'=>$phone_number, 'month_year'=>$month_year);
							$usageData = $this->schools_model->get_data('app_usage_spark_details', $cond_usage);
							
							if(empty($usageData)){
								$teacher_result = $teacherData[0];	
								$data['fullName'] = $teacher_result->fullName;
								$data['phone_number'] = $teacher_result->phone_number;
								$data['usertype'] = $teacher_result->usertype;
								$data['pincode'] = $teacher_result->pincode;
								$data['empCode'] = $teacher_result->empCode;
								$data['diseCode'] = $teacher_result->diseCode;
								$data['state_id'] = $teacher_result->state_id;
								$data['district_id'] = $teacher_result->district_id;
								$data['block_id'] = $teacher_result->block_id;
								$data['cluster_id'] = $teacher_result->cluster_id;
								$data['school_id'] = $teacher_result->school_id;
								if($field_type == 'video_viewed'){
									$data['video_viewed_actual'] = $value;
									$data[$field_type] = $value/(60*60);
								}
								else{
									$data[$field_type] = $value;
								}
								$data['month_year'] = $month_year;
								$data['created_at'] = date('Y-m-d H:i:s');
								$this->schools_model->addSparkAppUsage($data);
								$insert_spark++;
							}
							else{
								$usage_id = $usageData[0]->id;
								if($field_type == 'video_viewed'){
									$mdata['video_viewed_actual'] = $value;
									$mdata[$field_type] = $value/(60*60);
								}
								else{
									$mdata[$field_type] = $value;
								}
								$this->schools_model->update_data('app_usage_spark_details', $mdata, array('id'=>$usage_id));
								$update_spark++;
							}
						}
						else{
							$cond_usage = array('phone_number'=>$phone_number, 'month_year'=>$month_year);
							$usageData = $this->schools_model->get_data('app_usage_details', $cond_usage);
							
							if(empty($usageData)){
								$teacher_result = $teacherData[0];	
								$data['fullName'] = $teacher_result->fullName;
								$data['phone_number'] = $teacher_result->phone_number;
								$data['usertype'] = $teacher_result->usertype;
								$data['pincode'] = $teacher_result->pincode;
								$data['empCode'] = $teacher_result->empCode;
								$data['diseCode'] = $teacher_result->diseCode;
								$data['state_id'] = $teacher_result->state_id;
								$data['district_id'] = $teacher_result->district_id;
								$data['block_id'] = $teacher_result->block_id;
								$data['cluster_id'] = $teacher_result->cluster_id;
								$data['school_id'] = $teacher_result->school_id;
								if($field_type == 'video_viewed'){
									$data['video_viewed_actual'] = $value;
									$data[$field_type] = $value/(60*60);
								}
								else{
									$data[$field_type] = $value;
								}
								$data['month_year'] = $month_year;
								$data['created_at'] = date('Y-m-d H:i:s');
								$this->schools_model->addSSSAppUsage($data);
								$insert_other++;
							}
							else{
								$usage_id = $usageData[0]->id;
								if($field_type == 'video_viewed'){
									$mdata['video_viewed_actual'] = $value;
									$mdata[$field_type] = $value/(60*60);
								}
								else{
									$mdata[$field_type] = $value;
								}
								$this->schools_model->update_data('app_usage_details', $mdata, array('id'=>$usage_id));
								$update_other++;
							}
						}
					}
				}
			}
			
			//echo"<br>".$insert_spark;
			//echo"<br>".$insert_other;
			//echo"<br>".$update_spark; 
			//echo"<br>".$update_other;
			return 'success';
		}
		else{
			return 'error';
		}
	}

  
  function save_sss_teacher_registration()
  {
		$this->load->Model('schools_model');
		
		$current_date = date('Y-m-d',strtotime('-1 day'));
		
		$month_date = (isset($_GET['date']) ? date('Y-m-d', strtotime($_GET['date'])) : $current_date);
		
		//$url = "http://sssapi.samparksmartshala.org:8081/apis/users?date=".$month_date;
		//$url = "http://sssprodapi.samparksmartshala.org:8081/apis/users?date=".$month_date;
		$url = "http://ssscmsapi.samparksmartshala.org:8082/apis/users?date=".$month_date;
		
		//$result = file_get_contents($url);
		//$resultArray = json_decode($result);
		
		$curl = curl_init();
   
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_HEADER, false);

    // execute and return string (this should be an empty string '')
    $result = curl_exec($curl);

    curl_close($curl);

    // the value of $str is actually bool(true), not empty string ''
    //var_dump($str);
		$resultArray = json_decode($result);
		//print_r($resultArray);exit;
		
		$data = array();
		if(!empty($resultArray)){
			foreach($resultArray as $results)
			{
				//echo "<br>".$results->phone_number;
				$cond = array('phone_number'=> $results->phone_number);
				$check_teacher = $this->schools_model->check_teacher_registraion($cond);
				if(empty($check_teacher)){
					$data['fullName'] = $results->fullName;
					$data['phone_number'] = $results->phone_number;
					$data['usertype'] = $results->usertype;
					$data['pincode'] = $results->pincode;
					$data['empCode'] = $results->empCode;
					$data['diseCode'] = $results->diseCode;
					$data['state_id'] = $results->state_id;
					$data['district_id'] = $results->district_id;
					$data['block_id'] = $results->block_id;
					$data['cluster_id'] = $results->cluster_id;
					$data['is_verified'] = ($results->is_verified == true ? 1 : 0);
					$data['is_new'] = ($results->is_new == true ? 1 : 0);
					$data['is_active'] = ($results->is_active == true ? 1 : 0);
					//$data['school_id'] = (isset($school_list[$results->diseCode]) ? $school_list[$results->diseCode] : 0);
					$data['school_id'] = 0;
					$data['register_date'] = date('Y-m-d', strtotime($results->registration_date));
					$data['video_viewed'] = 0;
					$data['total_pdf_viewed'] = 0;
					$data['total_scert_viewed'] = 0;
					$data['items_viewed'] = 0;
					$data['total_posts'] = 0;
					$data['total_comments'] = 0;
					$data['total_likes'] = 0;
					$data['map_flag'] = 1;
					$data['created_at'] = date('Y-m-d H:i:s', strtotime($results->created_at));
					$this->schools_model->add_teacher_registraion($data);
					
					$udata['map_flag'] = 1;
					$this->schools_model->update_data('school_teachers', $udata, array('contact_number'=>$results->phone_number));
				}	//END if
			}	//END for
		} //END if
		
		$this->set_registered_disecode();
	}
	
	function set_registered_disecode()
	{
		$this->load->Model('schools_model');
		
		$this->schools_model->remove_date('cron_validations', array('cron_name'=>'set_registered_disecode'));
		$this->schools_model->add_data('cron_validations', array('cron_name'=>'set_registered_disecode', 'cron_start'=> date('Y-m-d H:i:s')));
		
		$getData = $this->schools_model->get_unmapped_disecode();
		
		foreach($getData as $data)
		{
			$dise_code = $data->diseCode;
			$checkData = $this->schools_model->school_by_dice_number($dise_code);
			
			if(empty($checkData)){
				$mdata['map_dise_code'] = 2;
				$this->schools_model->update_data('app_registrations', $mdata, array('diseCode'=>$dise_code));
			}
			else{
				$mdata['map_dise_code'] = 1;
				$this->schools_model->update_data('app_registrations', $mdata, array('diseCode'=>$dise_code));
				$this->schools_model->update_data('schools', $mdata, array('dise_code'=>$dise_code));
			}
		}
		$this->schools_model->update_data('cron_validations', array('cron_end'=> date('Y-m-d H:i:s')), array('cron_name'=>'set_registered_disecode'));
		
		$this->set_registered_school_disecode();
	}
	
	function set_registered_school_disecode()
	{
		$this->load->Model('schools_model');
		
		$this->schools_model->remove_date('cron_validations', array('cron_name'=>'set_registered_school_disecode'));
		$this->schools_model->add_data('cron_validations', array('cron_name'=>'set_registered_school_disecode', 'cron_start'=> date('Y-m-d H:i:s')));
		
		$getData = $this->schools_model->get_data('schools', array('map_dise_code'=>1, 'app_register_date'=> NULL));
		//$getData = $this->schools_model->get_mapped_disecode();
		
		foreach($getData as $data)
		{
			$dise_code = $data->dise_code;
			$checkRegisterDate = $this->schools_model->get_mapped_disecode_date($dise_code);
			
			$mdata['app_register_date'] = $checkRegisterDate['register_date'];
			$this->schools_model->update_data('schools', $mdata, array('dise_code'=>$dise_code));
		}
		$this->schools_model->update_data('cron_validations', array('cron_end'=> date('Y-m-d H:i:s')), array('cron_name'=>'set_registered_school_disecode'));
	}
	
	
	function return_report_dates()
  {
    $dates = array();
    $dates['year_start_date'] = date('2020-04-01');
    
    if(date('d') < 7){
      $month_start_date = date('Y-m-d', strtotime('first day of last month'));
      $current_date = date('Y-m-d', strtotime('last day of last month'));
    }else{
       //$month_start_date = date('2020-06-01');
       //$current_date = date('2020-06-30');
       $month_start_date = date('Y-m-01');
       $current_date = date('Y-m-d',strtotime('-1 day'));
    }
    $last_month_start_date = date('Y-m-1',strtotime('-1 month',strtotime($month_start_date)));
    $last_month_end_date = date('Y-m-t',strtotime('-1 month',strtotime($month_start_date)));
    $dates['last_month_start_date'] = $last_month_start_date;
    $dates['last_month_end_date'] = $last_month_end_date;
    $dates['month_start_date'] = $month_start_date;
    $dates['current_date'] = $current_date;
    $dates['month_year'] = date('m_Y', strtotime($month_start_date));
    
    return $dates;
  }
  
  function return_months($start_date, $end_date){
	   
		$a = $start_date;
		$b = $end_date;

		$i = date("Y-m", strtotime($a));
		$months = array();
		while($i <= date("Y-m", strtotime($b))){
			$months[] = $i;
			if(substr($i, 5, 2) == "12")
				$i = (date("Y", strtotime($i."-01")) + 1)."-01";
			else
				$i++;
		}
		return $months;
	}
 
  function spark_dashboard_appusage()
  {
		$this->load->Model('States_model');
    //$state_ids = array('4');
    //$stateData = $this->states_model->get_by_ids($state_ids);
    $stateData = $this->States_model->get_all();
     
    foreach($stateData as $states)
    {
      $this->spark_app_usage($states);
    }
    $this->save_baithak_ranking();
    $this->save_baithak_overall_ranking();
    
    foreach($stateData as $states)
    {
      $this->save_baithak_state_overall_ranking($states->id);
    }
    
  } 
	
	function spark_app_usage($states)
  {
		$this->load->Model('Dashboard_model');
		$this->load->Model('schools_model');
    $report_dates = $this->return_report_dates();
    
    $state_id = $states->id;
    $year_start_date = $report_dates['year_start_date'];
    $month_start_date = $report_dates['month_start_date'];
    $current_date = $report_dates['current_date'];
    $month_year = $report_dates['month_year'];
    
    $monthData = $this->return_months($year_start_date, $month_start_date);
    
    $month_arr = array();
    foreach($monthData as $mdata)
    {
			$exp = explode('-',$mdata);
			$month_arr[] = $exp[1].'_'.$exp[0];
		}

		$baithak_fields = $this->Dashboard_model->get_baithak_fields();
    $fields_arr = array();
    foreach($baithak_fields as $baithak_field)
    {
      $fields_arr[$baithak_field->field_name] = $baithak_field->id;
    }
		
		$sparksData = $this->Dashboard_model->getAllSparkByStateDuration($state_id, $month_start_date, $current_date, array('field_user'));
		
		$month_year_array = array($month_year);
		foreach($sparksData as $sparks){
			$spark_id = $sparks->user_id;
			$login_id = $sparks->login_id;
			$role = $sparks->role;
			
			$blockData = $this->Dashboard_model->user_distinct_blocks($spark_id);
			$sparksBlock = array();
			foreach($blockData as $blocks){
				if(!in_array($blocks->id, $sparksBlock))
					array_push($sparksBlock, $blocks->id);
			}

			//echo "<br>".$login_id;
			$analyticData = $this->get_analytics_report($month_year_array, $sparksBlock, $login_id, $month_start_date, $current_date);
			
			$analyticData_yr = $this->get_analytics_report($month_arr, $sparksBlock, $login_id, $year_start_date, $current_date);
			//print_r($analyticData);
			//print_r($analyticData_yr);
			
			foreach($fields_arr as $field_value=>$field_id){
				$data = array();
				$data['state_id']	= $state_id;
				$data['role']	= $role;
				$data['spark_id']	= $spark_id;
				$data['month_year']	= $month_year;
				$data['field_id']	= $field_id;
				$data['month_value']	= (isset($analyticData[$field_value]) ? $analyticData[$field_value] : 0);
				$data['year_value']	= (isset($analyticData_yr[$field_value]) ? $analyticData_yr[$field_value] : 0);
				$data['created_at']	= date('Y-m-d H:i:s');
				
				$cond = array('month_year'=>$month_year, 'spark_id'=>$spark_id, 'field_id'=>$field_id);
        $chk_data = $this->Dashboard_model->select_spark_baithak_value($cond);
        if(empty($chk_data)){
          $this->Dashboard_model->insert_spark_baithak_value($data);
        }
        else{
          if(isset($analyticData[$field_value])){
						$mdata = array();
            $mdata['state_id'] = $state_id;
            $mdata['month_value'] = (isset($analyticData[$field_value]) ? $analyticData[$field_value] : 0);
            $mdata['year_value']	= (isset($analyticData_yr[$field_value]) ? $analyticData_yr[$field_value] : 0);
            $this->Dashboard_model->update_spark_baithak_value($cond, $mdata);
          }
        }
			}
		}
  }
	
	
	function get_analytics_report($month_year_array, $sparksBlock, $login_id, $start_date, $end_date)
  {
    $this->load->Model('schools_model');
    
    $SSS_APP_POINTS = unserialize(SSS_APP_POINTS);
    $SSS_APP_TEACHER_POINTS = unserialize(SSS_APP_TEACHER_POINTS);
    
		$month_year = $month_year_array;
		$block_ids = $sparksBlock;
		
		$school_counts = 0;
		$school_registered = 0;
		$teacher_registered = 0;
		$greater_1_hr_count = 0;
		
		$total_register_teacher = 0;
		
		$percent_active_gr_1hr = 0;
		$percent_school_registered = 0;
		$teacher_vs_school = 0;

		if(!empty($block_ids)){
			
			$schoolData = $this->schools_model->get_by_block_ids($block_ids, 1);		
			
			$school_counts = count($schoolData);
			//$school_registered = $this->schools_model->get_spark_appusage($block_ids, $month_year, 'diseCode');
			$school_registered = $this->schools_model->get_school_regristerd($block_ids, $start_date, $end_date, 'govt teacher');
			$greater_1_hr_count = $this->schools_model->get_spark_appusage($block_ids, $month_year, 'phone_number', '1', 'govt teacher');
			
			$teacher_registered = $this->schools_model->get_teacher_regristerd($block_ids, $start_date, $end_date, 'govt teacher');
			$total_register_teacher = $this->schools_model->get_teacher_regristerd($block_ids, '', '', 'govt teacher');
			
			$percent_active_gr_1hr = ($teacher_registered > 0 ? round(($greater_1_hr_count*100/$teacher_registered),2) : 0);
			$percent_school_registered = ($school_counts > 0 ? round(($school_registered*100/$school_counts),2) : 0);
			$teacher_vs_school = ($school_counts > 0 ? round(($teacher_registered/$school_counts),2) : 0);
			
			$blockFeedbackData = $this->schools_model->get_spark_app_block_data($block_ids, $month_year, 'govt teacher');
		}
		
		$sparkAppData = $this->schools_model->get_spark_app_data($login_id, $month_year_array);
		
		$teacher_post = (isset($blockFeedbackData['POST_COUNT']) && $blockFeedbackData['POST_COUNT'] > 0 ? $blockFeedbackData['POST_COUNT'] : 0);
		$teacher_comment = (isset($blockFeedbackData['COMMENT_COUNT']) && $blockFeedbackData['COMMENT_COUNT'] > 0 ? $blockFeedbackData['COMMENT_COUNT'] : 0);
		$teacher_like = (isset($blockFeedbackData['LIKE_COUNT']) && $blockFeedbackData['LIKE_COUNT'] > 0 ? $blockFeedbackData['LIKE_COUNT'] : 0);

		$spark_post = (isset($sparkAppData['POST_COUNT']) ? $sparkAppData['POST_COUNT'] : 0);
		$spark_comment = (isset($sparkAppData['COMMENT_COUNT']) ? $sparkAppData['COMMENT_COUNT'] : 0);
		$spark_like = (isset($sparkAppData['LIKE_COUNT']) ? $sparkAppData['LIKE_COUNT'] : 0);
		
		$spark_engagement = $SSS_APP_POINTS['POST']*$spark_post + $SSS_APP_POINTS['COMMENT']*$spark_comment + $SSS_APP_POINTS['LIKE']*$spark_like;
		$teacher_engagement = $SSS_APP_TEACHER_POINTS['POST']*$teacher_post + $SSS_APP_TEACHER_POINTS['COMMENT']*$teacher_comment + $SSS_APP_TEACHER_POINTS['LIKE']*$teacher_like;
		
    $data['total_schools'] = $school_counts;
    $data['school_registered_count'] = $school_registered;
    $data['registered_teacher_count'] = $teacher_registered;
    $data['active_greater_1hr_count'] = $greater_1_hr_count;
    
    $data['teacher_post'] = $teacher_post;
    $data['teacher_comment'] = $teacher_comment;
    $data['teacher_like'] = $teacher_like;
    $data['teacher_engagement'] = $teacher_engagement;
    $data['avg_teacher_engagement'] = ($total_register_teacher > 0 ? $teacher_engagement/$total_register_teacher : 0);
    
    $data['spark_post'] = $spark_post;
    $data['spark_comment'] = $spark_comment;
    $data['spark_like'] = $spark_like;
    $data['spark_engagement'] = $spark_engagement;
    
    $data['school_registered'] = $percent_school_registered;
    $data['registered_teacher'] = $teacher_vs_school;
    $data['active_greater_1hr'] = $percent_active_gr_1hr;
    
    $data['school_registered_rank'] = 0;
    $data['registered_teacher_rank'] = 0;
    $data['active_greater_1hr_rank'] = 0;
    $data['spark_engagement_rank'] = 0;
    $data['avg_teacher_engagement_rank'] = 0;
    
    $data['baithak_rank'] = 0;
    $data['baithak_rating'] = 0;
    
    $data['baithak_state_rank'] = 0;
    $data['baithak_state_rating'] = 0;
    
    return $data;
  }
	
	function save_baithak_ranking()
	{
		$report_dates = $this->return_report_dates();
    
    $year_start_date = $report_dates['year_start_date'];
    $month_start_date = $report_dates['month_start_date'];
    $current_date = $report_dates['current_date'];
    $month_year = $report_dates['month_year'];
    
		$this->load->Model('Dashboard_model');
		$school_registered_fields = $this->Dashboard_model->get_baithak_fields('school_registered');
		$registered_teacher_fields = $this->Dashboard_model->get_baithak_fields('registered_teacher');
		//$active_greater_1hr_fields = $this->Dashboard_model->get_baithak_fields('active_greater_1hr');
		$spark_engagement_fields = $this->Dashboard_model->get_baithak_fields('spark_engagement');
		$avg_teacher_fields = $this->Dashboard_model->get_baithak_fields('avg_teacher_engagement');
		
		$school_registered_rank = $this->Dashboard_model->get_baithak_fields('school_registered_rank');
		$registered_teacher_rank = $this->Dashboard_model->get_baithak_fields('registered_teacher_rank');
		//$active_greater_1hr_rank = $this->Dashboard_model->get_baithak_fields('active_greater_1hr_rank');
		$spark_engagement_rank = $this->Dashboard_model->get_baithak_fields('spark_engagement_rank');
		$avg_teacher_rank = $this->Dashboard_model->get_baithak_fields('avg_teacher_engagement_rank');
		
		$this->get_baithak_ranking($month_year, $school_registered_fields[0]->id, $school_registered_rank[0]->id);
		$this->get_baithak_ranking($month_year, $registered_teacher_fields[0]->id, $registered_teacher_rank[0]->id);
		$this->get_baithak_ranking($month_year, $spark_engagement_fields[0]->id, $spark_engagement_rank[0]->id);
		$this->get_baithak_ranking($month_year, $avg_teacher_fields[0]->id, $avg_teacher_rank[0]->id);
		
	}	
	
	function get_baithak_ranking($month_year, $field_id, $rank_field_id)
	{
		$cond = array('month_year'=>$month_year, 'field_id'=>$field_id);
    $month_ranking_data = $this->Dashboard_model->select_spark_baithak_value($cond, 'all', 'month_value desc');
    
    $newjoin_sparks = $this->Dashboard_model->getAllSparksByJoiningDate('2020-04-01');
    
    $newSparkIds = array();
    foreach($newjoin_sparks as $newJoinSparks)
    {
      $newSparkIds[] = $newJoinSparks->id;
    }
		
		$spark_ids = array();
    //$state_spark_ranking = array();
    $month_ranking = array();
    $i = 0;
    $previous_val = 0;
    $rank = 1;
    //$max_rank = 1;
    foreach($month_ranking_data as $ranking)
    {  
      if(!in_array($ranking->spark_id, $spark_ids))
					array_push($spark_ids, $ranking->spark_id);
						
			if($i == 0){
				$previous_val = $ranking->month_value;
			}
			if($ranking->month_value < $previous_val)
			{
				$previous_val = $ranking->month_value;
				$rank = $i+1;
			}
			$month_ranking[$ranking->spark_id] = $rank;
			$i++;
		
      $max_rank = $rank;
    }
    
    $year_ranking_data = $this->Dashboard_model->select_spark_baithak_value($cond, 'all', 'year_value desc');
		
		//$spark_ids = array();
    //$state_spark_ranking = array();
    $year_ranking = array();
    $i = 0;
    $previous_val = 0;
    $rank = 1;
    $new_spark_ids = array();
    foreach($year_ranking_data as $ranking)
    {  
			if(!in_array($ranking->spark_id, $newSparkIds)){
				if($i == 0){
					$previous_val = $ranking->year_value;
				}
				if($ranking->year_value < $previous_val)
				{
					$previous_val = $ranking->year_value;
					$rank = $i+1;
				}
				$year_ranking[$ranking->spark_id] = $rank;
				$i++;
			}
			else{
        $new_spark_ids[] = $ranking->spark_id;
      }
      //$max_rank = $rank;
    }
    //print_r($year_ranking_data);
    
    $spark_ids = array_unique($spark_ids);
    foreach($spark_ids as $spark_id)
    {
			$ndata = array();
      $ndata['month_value'] = (isset($month_ranking[$spark_id]) ? $month_ranking[$spark_id] : 0);
      $ndata['year_value'] = (isset($year_ranking[$spark_id]) ? $year_ranking[$spark_id] : 0);
      $ncond = array('spark_id'=>$spark_id, 'field_id' => $rank_field_id, 'month_year'=>$month_year);
      
      $this->Dashboard_model->update_spark_baithak_value($ncond, $ndata);
    }
    
    $new_spark_ids = array_unique($new_spark_ids);
    foreach($new_spark_ids as $spark_id)
    {
      $nscond1 = array('spark_id'=>$spark_id, 'field_id' => $rank_field_id, 'month_year'=>$month_year);
      $this->Dashboard_model->update_spark_baithak_value($nscond1, array('year_value'=>9999));
    }
    
	}

	function save_baithak_overall_ranking()
  {
		$this->load->Model('Dashboard_model');	
    $report_dates = $this->return_report_dates();
    
    $year_start_date = $report_dates['year_start_date'];
    $month_start_date = $report_dates['month_start_date'];
    $current_date = $report_dates['current_date'];
    $month_year = $report_dates['month_year'];
    
    $newjoin_sparks = $this->Dashboard_model->getAllSparksByJoiningDate('2020-04-01');
    
    $newSparkIds = array();
    foreach($newjoin_sparks as $newJoinSparks)
    {
      $newSparkIds[] = $newJoinSparks->id;
    }
    $new_spark_ids = array();
    
    $month_rankings = $this->Dashboard_model->get_baithak_fields_rankings($month_year, '', 'month_value', array('school_registered','registered_teacher','avg_teacher_engagement','spark_engagement'), 'field_user');
    
    $month_ranking_val = array();
    foreach($month_rankings as $month_ranking)
    {
			$month_ranking_val[$month_ranking->spark_id][$month_ranking->field_name] = 	$this->get_weitage_value($month_ranking->field_name, $month_ranking->FIELD_VAL);
		}
		
		$spark_mranking = array();
		foreach($month_ranking_val as $sp_id=>$monthval)
		{
			$spark_mranking[$sp_id] = $monthval['school_registered'] + $monthval['registered_teacher'] + $monthval['avg_teacher_engagement'] + $monthval['spark_engagement'];
		}
		arsort($spark_mranking);
		//print_r($spark_mranking);
		
		$i = 0;
		$previous_val = 0;
		$rank = 1;
		$month_max_rank = 0;
		$m_rankings = array();
		$spark_ids = array();
		foreach($spark_mranking as $spark_id=>$ranking){
			
			if(!in_array($spark_id, $spark_ids))
        array_push($spark_ids, $spark_id);		
				
			if($i == 0){
				$previous_val = $ranking;
			}
			if($ranking < $previous_val)
			{
				$previous_val = $ranking;
				$rank = $i+1;
			}
			$m_rankings[$spark_id] = $rank;
			$i++;
			$month_max_rank = $rank;
		}
		//print_r($m_rankings);
		
		$val_25 = $this->return_value_percent(25, $month_max_rank);
    $val_75 = $this->return_value_percent(75, $month_max_rank);
    $month_rating = array();
    foreach($m_rankings as $spark_id=>$ranking)
    {
      if($ranking <= $val_25)
      {
        $month_rating[$spark_id] = 0;
      }
      else if($ranking > $val_75){
        $month_rating[$spark_id] = 2;
      }
      else{
        $month_rating[$spark_id] = 1;
      }
    }
    //print_r($month_rating);
    //exit;
		
    $year_rankings = $this->Dashboard_model->get_baithak_fields_rankings($month_year, '', 'year_value', array('school_registered','registered_teacher','avg_teacher_engagement','spark_engagement'), 'field_user');
    
    $year_ranking_val = array();
    foreach($year_rankings as $year_ranking)
    {
			$year_ranking_val[$year_ranking->spark_id][$year_ranking->field_name] = 	$this->get_weitage_value($year_ranking->field_name, $year_ranking->FIELD_VAL);
		}
		
		$spark_yranking = array();
		foreach($year_ranking_val as $sp_id=>$yearval)
		{
			$spark_yranking[$sp_id] = $yearval['school_registered'] + $yearval['registered_teacher'] + $yearval['avg_teacher_engagement'] + $yearval['spark_engagement'];
		}
		arsort($spark_yranking);
		//print_r($spark_ranking);
		
		$i = 0;
		$previous_val = 0;
		$rank = 1;
		$year_max_rank = 0;
		$y_rankings = array();
		$new_spark_ids = array();
		foreach($spark_yranking as $spark_id=>$ranking){
			if(!in_array($spark_id, $newSparkIds)){
				if($i == 0){
					$previous_val = $ranking;
				}
				if($ranking < $previous_val)
				{
					$previous_val = $ranking;
					$rank = $i+1;
				}
				$y_rankings[$spark_id] = $rank;
				$i++;
				$year_max_rank = $rank;
			}
			else{
        $new_spark_ids[] = $spark_id;
      }	
		}
		//print_r($year_rankings);
		
		$val_25 = $this->return_value_percent(25, $year_max_rank);
    $val_75 = $this->return_value_percent(75, $year_max_rank);
    $year_rating = array();
    foreach($y_rankings as $spark_id=>$ranking)
    {
      if($ranking <= $val_25)
      {
        $year_rating[$spark_id] = 0;
      }
      else if($ranking > $val_75){
        $year_rating[$spark_id] = 2;
      }
      else{
        $year_rating[$spark_id] = 1;
      }
    }
    
    $baithak_rank_data = $this->Dashboard_model->get_baithak_fields('baithak_rank');
    $baithak_rating_data = $this->Dashboard_model->get_baithak_fields('baithak_rating');
    
    //print_r($spark_ids);
    //print_r($m_rankings);
    //print_r($month_rating);
    foreach($spark_ids as $sparkid)
		{
			$sdata1 = array();
			$ndata1 = array();
			//echo"<br>55 >> ".$spark_id;
			$ndata1['month_value'] = (isset($m_rankings[$sparkid]) ? $m_rankings[$sparkid] : 0);
			$ndata1['year_value'] = (isset($y_rankings[$sparkid]) ? $y_rankings[$sparkid] : 0);
			$ncond1 = array('spark_id'=>$sparkid, 'field_id' => $baithak_rank_data[0]->id, 'month_year'=>$month_year);
			$this->Dashboard_model->update_spark_baithak_value($ncond1, $ndata1);
			
			$sdata1['month_value'] = (isset($month_rating[$sparkid]) ? $month_rating[$sparkid] : 0);
			$sdata1['year_value'] = (isset($year_rating[$sparkid]) ? $year_rating[$sparkid] : 0);
			$scond1 = array('spark_id'=>$sparkid, 'field_id' => $baithak_rating_data[0]->id, 'month_year'=>$month_year);
			$this->Dashboard_model->update_spark_baithak_value($scond1, $sdata1);
		}
		
		$new_spark_ids = array_unique($new_spark_ids);
    foreach($new_spark_ids as $spark_id)
    {
      $sscond1 = array('spark_id'=>$spark_id, 'field_id' => $baithak_rank_data[0]->id, 'month_year'=>$month_year);
      $this->Dashboard_model->update_spark_baithak_value($sscond1, array('year_value'=>9999));
      
      $nscond1 = array('spark_id'=>$spark_id, 'field_id' => $baithak_rating_data[0]->id, 'month_year'=>$month_year);
      $this->Dashboard_model->update_spark_baithak_value($nscond1, array('year_value'=>9999));
    }
   
  }
  

  function save_baithak_state_overall_ranking($state_id)
  {
		$this->load->Model('Dashboard_model');	
    $report_dates = $this->return_report_dates();
    
    $year_start_date = $report_dates['year_start_date'];
    $month_start_date = $report_dates['month_start_date'];
    $current_date = $report_dates['current_date'];
    $month_year = $report_dates['month_year'];
    
    $newjoin_sparks = $this->Dashboard_model->getAllSparksByJoiningDate('2020-04-01');
    
    $newSparkIds = array();
    foreach($newjoin_sparks as $newJoinSparks)
    {
      $newSparkIds[] = $newJoinSparks->id;
    }
    $new_spark_ids = array();
    
    $month_rankings = $this->Dashboard_model->get_baithak_fields_rankings($month_year, $state_id, 'month_value', array('school_registered','registered_teacher','avg_teacher_engagement','spark_engagement'), 'field_user');
    
    $month_ranking_val = array();
    foreach($month_rankings as $month_ranking)
    {
			$month_ranking_val[$month_ranking->spark_id][$month_ranking->field_name] = 	$this->get_weitage_value($month_ranking->field_name, $month_ranking->FIELD_VAL);
		}
		
		$spark_mranking = array();
		foreach($month_ranking_val as $sp_id=>$monthval)
		{
			$spark_mranking[$sp_id] = $monthval['school_registered'] + $monthval['registered_teacher'] + $monthval['avg_teacher_engagement'] + $monthval['spark_engagement'];
		}
		arsort($spark_mranking);
		//print_r($spark_mranking);
		
		$i = 0;
		$previous_val = 0;
		$rank = 1;
		$month_max_rank = 0;
		$m_rankings = array();
		$spark_ids = array();
		foreach($spark_mranking as $spark_id=>$ranking){
			
			if(!in_array($spark_id, $spark_ids))
        array_push($spark_ids, $spark_id);		
				
			if($i == 0){
				$previous_val = $ranking;
			}
			if($ranking < $previous_val)
			{
				$previous_val = $ranking;
				$rank = $i+1;
			}
			$m_rankings[$spark_id] = $rank;
			$i++;
			$month_max_rank = $rank;
		}
		//print_r($m_rankings);
		
		$val_25 = $this->return_value_percent(25, $month_max_rank);
    $val_75 = $this->return_value_percent(75, $month_max_rank);
    $month_rating = array();
    foreach($m_rankings as $spark_id=>$ranking)
    {
      if($ranking <= $val_25)
      {
        $month_rating[$spark_id] = 0;
      }
      else if($ranking > $val_75){
        $month_rating[$spark_id] = 2;
      }
      else{
        $month_rating[$spark_id] = 1;
      }
    }
    //print_r($month_rating);
    //exit;
    
    $year_rankings = $this->Dashboard_model->get_baithak_fields_rankings($month_year, $state_id, 'year_value', array('school_registered','registered_teacher','avg_teacher_engagement','spark_engagement'), 'field_user');
    
    $year_ranking_val = array();
    foreach($year_rankings as $year_ranking)
    {
			$year_ranking_val[$year_ranking->spark_id][$year_ranking->field_name] = 	$this->get_weitage_value($year_ranking->field_name, $year_ranking->FIELD_VAL);
		}
		
		$spark_yranking = array();
		foreach($year_ranking_val as $sp_id=>$yearval)
		{
			$spark_yranking[$sp_id] = $yearval['school_registered'] + $yearval['registered_teacher'] + $yearval['avg_teacher_engagement'] + $yearval['spark_engagement'];
		}
		arsort($spark_yranking);
		//print_r($spark_ranking);
		
		$i = 0;
		$previous_val = 0;
		$rank = 1;
		$year_max_rank = 0;
		$y_rankings = array();
		$new_spark_ids = array();
		foreach($spark_yranking as $spark_id=>$ranking){
			if(!in_array($spark_id, $newSparkIds)){
				if($i == 0){
					$previous_val = $ranking;
				}
				if($ranking < $previous_val)
				{
					$previous_val = $ranking;
					$rank = $i+1;
				}
				$y_rankings[$spark_id] = $rank;
				$i++;
				$year_max_rank = $rank;
			}
			else{
        $new_spark_ids[] = $spark_id;
      }	
		}
		//print_r($year_rankings);
		
		$val_25 = $this->return_value_percent(25, $year_max_rank);
    $val_75 = $this->return_value_percent(75, $year_max_rank);
    $year_rating = array();
    foreach($y_rankings as $spark_id=>$ranking)
    {
      if($ranking <= $val_25)
      {
        $year_rating[$spark_id] = 0;
      }
      else if($ranking > $val_75){
        $year_rating[$spark_id] = 2;
      }
      else{
        $year_rating[$spark_id] = 1;
      }
    }
    
    $baithak_rank_data = $this->Dashboard_model->get_baithak_fields('baithak_state_rank');
    $baithak_rating_data = $this->Dashboard_model->get_baithak_fields('baithak_state_rating');
    
    //print_r($spark_ids);
    //print_r($m_rankings);
    //print_r($month_rating);
    foreach($spark_ids as $sparkid)
		{
			$sdata1 = array();
			$ndata1 = array();
			//echo"<br>55 >> ".$spark_id;
			$ndata1['month_value'] = (isset($m_rankings[$sparkid]) ? $m_rankings[$sparkid] : 0);
			$ndata1['year_value'] = (isset($y_rankings[$sparkid]) ? $y_rankings[$sparkid] : 0);
			$ncond1 = array('spark_id'=>$sparkid, 'field_id' => $baithak_rank_data[0]->id, 'month_year'=>$month_year);
			$this->Dashboard_model->update_spark_baithak_value($ncond1, $ndata1);
			
			$sdata1['month_value'] = (isset($month_rating[$sparkid]) ? $month_rating[$sparkid] : 0);
			$sdata1['year_value'] = (isset($year_rating[$sparkid]) ? $year_rating[$sparkid] : 0);
			$scond1 = array('spark_id'=>$sparkid, 'field_id' => $baithak_rating_data[0]->id, 'month_year'=>$month_year);
			$this->Dashboard_model->update_spark_baithak_value($scond1, $sdata1);
		}
		
		$new_spark_ids = array_unique($new_spark_ids);
    foreach($new_spark_ids as $spark_id)
    {
      $sscond1 = array('spark_id'=>$spark_id, 'field_id' => $baithak_rank_data[0]->id, 'month_year'=>$month_year);
      $this->Dashboard_model->update_spark_baithak_value($sscond1, array('year_value'=>9999));
      
      $nscond1 = array('spark_id'=>$spark_id, 'field_id' => $baithak_rating_data[0]->id, 'month_year'=>$month_year);
      $this->Dashboard_model->update_spark_baithak_value($nscond1, array('year_value'=>9999));
    }
  }
  

  function return_value_percent($percent_value, $current_value)
  {
    if($current_value > 0)
      $net_value = round(($current_value*$percent_value)/100,2);
    else  
      $net_value = 0;
    
    return $net_value;  
  }
  
  function get_weitage_value($field, $value)
  {
		switch($field)
		{
			case ('school_registered_rank' || 'school_registered'):
				$WEIGHTAGE = 4;	//for 25% we take 4
				break;
			case ('registered_teacher_rank' || 'registered_teacher'):
				$WEIGHTAGE = 4;
				break;
			case ('active_greater_1hr_rank' || 'active_greater_1hr'):
				$WEIGHTAGE = 4;
				break;
			case ('spark_engagement_rank' || 'spark_engagement'):
				$WEIGHTAGE = 4;
				break;
			case ('avg_teacher_engagement_rank' || 'avg_teacher_engagement'):
				$WEIGHTAGE = 4;
				break;
			default:  
				$WEIGHTAGE = 1;
		}
		if($value > 0)
      $number = $value/$WEIGHTAGE;
    else  
      $number = 0;
      
    return $number;
	}
	
	function add_spark_earn_leave()
  {
	  $this->load->Model('Users_model');
	  $this->load->Model('Leaves_model');
	  
	  $month_array = array(1, 4, 7, 10);
	  
	  $current_month = date('m');
	  
	  if(in_array($current_month, $month_array)){
			$current_date = date('Y-m-01');
			$sparkData = $this->users_model->get_all_users('',1);
			
			foreach($sparkData as $sparks)
			{
				$creditDeails = $this->Leaves_model->get_leave_credit_details($sparks->id, 'Earned Leave', '', '', $current_date);
				//print_r($creditDeails);
				if(empty($creditDeails))
				{
					$sparkData = $this->Users_model->getById($sparks->id);
					
					$spark_group_leave = $this->Leaves_model->get_spark_group_leave($sparkData['user_group']);
					$leave_count = $spark_group_leave['leave_count']*3;
					
					$data['spark_id'] = $sparks->id;
					$data['leave_type'] = 'Earned Leave';
					$data['earned_date'] = $current_date;
					$data['created_at'] = $current_date;
					$data['leave_count'] = $leave_count;
					$this->Leaves_model->insert_leave_credit_details($data);
					
					$creditData = $this->Leaves_model->get_leave_credits($sparks->id, 'Earned Leave');
					
					if(empty($creditData)){
					$cdata['spark_id'] = $sparks->id;
						$cdata['leave_type'] = 'Earned Leave';
						$cdata['leave_earned'] = $leave_count;  
						$this->Leaves_model->insert_leave_credit($cdata);  
					}
					else{
						$mdata['leave_earned'] = $creditData[0]->leave_earned+$leave_count;  
						$this->Leaves_model->update_leave_credit($mdata, $creditData[0]->id);    
					}
				}
			}
		}
  }
  
  function add_spark_block_holiday()
  {
		$this->load->Model('Trainings_model');
		$this->load->Model('Sparks_model');
		$this->load->Model('Leaves_model');
		
		//$current_date = date('2019-12-02');
		$current_date = date('Y-m-d');
		$previous_date = date('Y-m-d', strtotime('-1 day', strtotime($current_date)));
		
		$home_blocks = array();
		$userHomeBlocks = $this->Sparks_model->get_last_home_blocks();
		foreach($userHomeBlocks as $userBlocks)
		{
			$home_blocks[$userBlocks->user_id] = $userBlocks->block_id;
		}
		//print_r($home_blocks);
		$trainingData = $this->Trainings_model->get_all_trainings($current_date);
		foreach($trainingData as $trainings)
		{
			$spark_id = $trainings->user_id;
			$block_id = $trainings->block_id;
			//echo"<br>Home : ".$trainings->user_id." >> ".$trainings->block_id;
			if(!isset($home_blocks[$spark_id][$block_id])){
				
				$preDateAcivity = $this->Trainings_model->get_previous_activity($spark_id, $current_date);
				//print_r($preDateAcivity);
				
				if(!empty($preDateAcivity)){
					
					$pre_block = $preDateAcivity['block_id'];
					$pre_state = $preDateAcivity['state_id'];
					$pre_activity_date = date('Y-m-d', strtotime($preDateAcivity['activity_date']));
					//echo"<br>".$pre_block." >> ".$preDateAcivity['activity_date']." >> ".$previous_date;
					
					if($pre_activity_date != $previous_date && !isset($home_blocks[$spark_id][$pre_block])){
						$date_arr = $this->getDatesFromRange($pre_activity_date, $current_date);
						$date_arr = array_diff($date_arr, array($pre_activity_date));
						//print_r($date_arr);
						
						$temp_date_arr = $date_arr;
						$holiday_array = array();
						$this->load->Model('Holidays_model');
						
						if(!empty($date_arr)){
							$noVisitDates = $this->Trainings_model->get_dates_novisit_days($spark_id, $date_arr);
							
							foreach($noVisitDates as $e_key){
								$holiday_array['No Visit Day'][] = $e_key->activity_date;
								//echo "<br>".$spark_id." >> ".$e_key->activity_date." >> No Visit Day";
								//unset($date_arr[$e_key->activity_date]);
								$date_arr = array_diff($date_arr, array($e_key->activity_date));
							}
						}
						
						if(!empty($date_arr)){
							$offDates = $this->Trainings_model->get_dates_offdays('field_user', $date_arr);
							
							foreach($offDates as $e_key){
								$holiday_array['Off Day'][] = $e_key->activity_date;
								//echo "<br>".$spark_id." >> ".$e_key->activity_date." >> OFF Day";
								$date_arr = array_diff($date_arr, array($e_key->activity_date));
							}
						}
						
						if(!empty($date_arr)){	
							$holidayDates = $this->Trainings_model->get_dates_holidays('field_user', $pre_state, $date_arr);
							
							foreach($holidayDates as $e_key){
								$holiday_array['State Holiday'][] = $e_key->activity_date;
								$date_arr = array_diff($date_arr, array($e_key->activity_date));
							}
						}
						
						//print_r($date_arr);
						if(empty($date_arr)){
							
							//echo "<br>inside holiday";
							//print_r($temp_date_arr);
							
							//$leave_count = count($holiday_array['No Visit Day'])+count($holiday_array['Off Day'])+count($holiday_array['State Day']);
							$leave_count = count($temp_date_arr);
							
							if($leave_count > 0){
								$creditDeails = $this->Leaves_model->get_leave_credit_details($spark_id, 'Block Holiday', '', '', $current_date);
								//print_r($creditDeails);
								if(empty($creditDeails))
								{
									$data['spark_id'] = $spark_id;
									$data['leave_type'] = 'Block Holiday';
									$data['earned_date'] = $current_date;
									$data['created_at'] = $current_date;
									$data['leave_count'] = $leave_count;
									$this->Leaves_model->insert_leave_credit_details($data);
									
									$creditData = $this->Leaves_model->get_leave_credits($spark_id, 'Block Holiday');
									
									if(empty($creditData)){
										$cdata['spark_id'] = $spark_id;
										$cdata['leave_type'] = 'Block Holiday';
										$cdata['leave_earned'] = $leave_count;  
										$this->Leaves_model->insert_leave_credit($cdata);  
									}
									else{
										$mdata['leave_earned'] = $creditData[0]->leave_earned+$leave_count;  
										$this->Leaves_model->update_leave_credit($mdata, $creditData[0]->id);    
									}
								}
							}
							
						}
						
					}
				}
			}
		}
	}
	
	function getDatesFromRange($start, $end, $format = 'Y-m-d') { 
				
			// Declare an empty array 
			$array = array(); 
			// Variable that store the date interval 
			// of period 1 day 
			$interval = new DateInterval('P1D'); 
		
			$realEnd = new DateTime($end); 
			$realEnd->add($interval); 
		
			$period = new DatePeriod(new DateTime($start), $interval, $realEnd); 
		
			// Use loop to store date into array 
			foreach($period as $date) {          
				if($date->format($format) != $start && $date->format($format) != $end)       
					$array[] = $date->format($format);  
			} 
			// Return the array elements 
			return $array; 
	} 
  
	/*function update_districtblock_startdate()
	{
		$this->load->Model('Dashboard_model');
		$report_dates = $this->return_report_dates();
    
    $year_start_date = $report_dates['year_start_date'];
    $month_start_date = $report_dates['month_start_date'];
    $current_date = $report_dates['current_date'];
    $month_year = $report_dates['month_year'];
		
		$sparks = $this->Dashboard_model->getAllSparkByStateDuration(1, $month_start_date, $current_date);
		
		print_r($sparks);
		
	}*/
	
	function update_school_teachers_mapping()	
	{
		$this->load->Model('schools_model');
		
		$registerdData = $this->schools_model->get_data('app_registrations', array('map_flag'=>0, 'state_id'=>4), 2500);
		
		foreach($registerdData as $data)
		{
			$contact_number = $data->phone_number;
			
			$udata['map_flag'] = 1;
			$this->schools_model->update_data('school_teachers', $udata, array('contact_number'=>$contact_number));
			
			$this->schools_model->update_data('app_registrations', $udata, array('phone_number'=>$contact_number));
		}
		
	}

	function temp_spark_earn_leave()
  {
	  $this->load->Model('Users_model');
	  $this->load->Model('Leaves_model');
	  
	  $month_array = array(1, 4, 7, 10);
	  
	  $loginids = array('1001E', '0003E', '0004E', '0005C', '0009E', '0010E', '0011C', '0013C', '0016C', '0017C', '0018C', '0019C', '0022C', '3004E', '0029C', '0030C', '0039C', '0040C', '3002E', '0042C', '0043E', '2003E', '0044E', '0045C', '0046C', '0047C', '0048C', '0050C', '0054C', '2004E', '0049E', '0051E', '0031E', '3003E', '0057E', '0058E', '0059E', '0060E', '0061E', '0062E', '0066E', '0069E', '0070E', '0071E', '0074E', '0075E', '0076E', '0078E', '0079E', '0080E', '0081E', '0082E', '0086E', '0087E', '0088E', '0089E', '0090E', '0091E', '0094E', '3005E', '0096E', '0097E', '0098E', '0099E', '2008E', '0100E', '0105E', '0107E', '0108E', '0109E', '0114E', '0116E', '0119E', '0122E', '0125E', '0129E', '0128E', '0126E', '0131E', '0132E', '0133E', '0136E', '0137E', '0139E', '0141E', '0142E', '0143E', '0145E', '0146E', '0147E', '0148E', '0151E', '0152E', '0153E', '0154E', '0155E', '0156E', '0157E', '0159E', '0160E', '0161E', '0163E', '0165E', '0166E', '0168E', '0169E', '0170E', '0172E', '0174E', '0175E', '0176E', '0178E', '0179E', '0180E', '0182E', '0186E', '0187E');
	  
	  $spark_leave_array = array('1001E'=>	4.5, '0003E'=>	4.5,'0004E'=>	4.5,'0005C'=>	4.5,'0009E'=>	4.5,'0010E'=>	4.5,'0011C'=>	4.5,'0013C'=>	4.5,'0016C'=>	4.5,'0017C'=>	4.5,'0018C'=>	4.5,'0019C'=>	4.5,'0022C'=>	4.5,'3004E'=>	4.5,'0029C'=>	4.5,'0030C'=>	4.5,'0039C'=>	4.5,'0040C'=>	4.5,'3002E'=>	4.5,'0042C'=>	4.5,'0043E'=>	4.5,'2003E'=>	4.5,'0044E'=>	4.5,'0045C'=>	4.5,'0046C'=>	4.5,'0047C'=>	4.5,'0048C'=>	4.5,'0050C'=>	4.5,'0054C'=>	4.5,'2004E'=>	4.5,'0049E'=>	4.5,'0051E'=>	4.5,'0031E'=>	4.5,'3003E'=>	4.5,'0057E'=>	4.5,'0058E'=>	4.5,'0059E'=>	4.5,'0060E'=>	4.5,'0061E'=>	4.5,'0062E'=>	4.5,'0066E'=>	4.5,'0069E'=>	4.5,'0070E'=>	4.5,'0071E'=>	4.5,'0074E'=>	4.5,'0075E'=>	4.5,'0076E'=>	4.5,'0078E'=>	4.5,'0079E'=>	4.5,'0080E'=>	4.5,'0081E'=>	4.5,'0082E'=>	4.5,'0086E'=>	4.5,'0087E'=>	4.5,'0088E'=>	4.5,'0089E'=>	4.5,'0090E'=>	4.5,'0091E'=>	4.5,'0094E'=>	4.5,'3005E'=>	4.5,'0096E'=>	4.5,'0097E'=>	4.5,'0098E'=>	4.5,'0099E'=>	4.5,'2008E'=>	4.5,'0100E'=>	4.5,'0105E'=>	4.5,'0107E'=>	4.5,'0108E'=>	4.5,'0109E'=>	4.5,'0114E'=>	4.5,'0116E'=>	4.5,'0119E'=>	4.5,'0122E'=>	4.5,'0125E'=>	4.5,'0129E'=>	4.5,'0128E'=>	4.5,'0126E'=>	4.5,'0131E'=>	4.5,'0132E'=>	4.5,'0133E'=>	4.5,'0136E'=>	4.5,'0137E'=>	4.5,'0139E'=>	4.5,'0141E'=>	4.5,'0142E'=>	4.5,'0143E'=>	4.5,'0145E'=>	4.5,'0146E'=>	4.5,'0147E'=>	4.5,'0148E'=>	4.5,'0151E'=>	4.5,'0152E'=>	4.5,'0153E'=>	4.5,'0154E'=>	4.5,'0155E'=>	4.5,'0156E'=>	4.5,'0157E'=>	4.5,'0159E'=>	4.5,'0160E'=>	4.5,'0161E'=>	4.5,'0163E'=>	4.5,'0165E'=>	4.5,'0166E'=>	4.5,'0168E'=>	4.5,'0169E'=>	4.5,'0170E'=>	4.5,'0172E'=>	4.5,'0174E'=>	4.5,'0175E'=>	4.5,'0176E'=>	4.5,'0178E'=>	4.5,'0179E'=>	4.5,'0180E'=>	4.5,'0182E'=>	2.5,'0186E'=>	3,'0187E'=>	3);
	  
	  $current_month = date('m');
	  
	  	$currentdate = date('Y-07-01');
	  	$current_date = date('Y-m-01');
			//$sparkData = $this->users_model->get_all_users('',1);
			$sparkData = $this->users_model->getByLoginIds($loginids);

		
		foreach($sparkData as $sparks)
		{	
			if(in_array($sparks->login_id, $loginids)){	
				echo"<br>".$sparks->login_id;
				$creditDeails = $this->Leaves_model->get_leave_credit_details($sparks->id, 'Earned Leave', '', '', $currentdate);
				//print_r($creditDeails);
				if(empty($creditDeails))
				{
					$sparkData = $this->Users_model->getById($sparks->id);
					
					$spark_group_leave = $this->Leaves_model->get_spark_group_leave($sparkData['user_group']);
					$leave_count = $spark_leave_array[$sparkData['login_id']];
					
					$data['spark_id'] = $sparks->id;
					$data['leave_type'] = 'Earned Leave';
					$data['earned_date'] = '2020-07-01';
					$data['created_at'] = $current_date;
					$data['leave_count'] = $leave_count;
					print_r($data);
					$this->Leaves_model->insert_leave_credit_details($data);
					
					$creditData = $this->Leaves_model->get_leave_credits($sparks->id, 'Earned Leave');
					
					if(empty($creditData)){
					$cdata['spark_id'] = $sparks->id;
						$cdata['leave_type'] = 'Earned Leave';
						$cdata['leave_earned'] = $leave_count; 
						echo"<br>in if"; 
						print_r($cdata);
						$this->Leaves_model->insert_leave_credit($cdata);  
					}
					else{
						$mdata['leave_earned'] = $creditData[0]->leave_earned+$leave_count;  
						echo"<br>in else"; 
						print_r($mdata);
						$this->Leaves_model->update_leave_credit($mdata, $creditData[0]->id);    
					}
				}
				echo"<hr>";	
			}else{
				echo"<br>loginid : ".$sparks->login_id;			
			}
		}
  }	
}

?>
