<?php

class Leaves extends CI_Controller  {
  function __construct() {
    parent::__construct();
    $this->load->helper(array('form', 'url'));
    $this->load->model('states_model');
    $this->load->model('leaves_model');
    $this->load->model('users_model');
    $this->load->library('form_validation');
    $this->load->library('common_functions');   
			
		if($this->session->userdata('logged_in'))
		{
				$session_data = $this->session->userdata('logged_in');
				$this->data['current_user_id'] = $session_data['id'];
				$this->data['current_username'] = $session_data['username'];
				$this->data['current_name'] = $session_data['name'];
				$this->data['current_role'] = $session_data['role'];
				$this->data['user_state_id'] = $session_data['state_id'];
				$this->data['user_district_id'] = $session_data['district_id'];
				$this->data['spark_id'] = $session_data['id'];
				$this->data['spark_state_id'] = $session_data['state_id'];
				$this->data['current_group'] = $session_data['user_group'];
		}
		else
		{
				redirect('admin/login', 'refresh');
		}
		$this->data['breadcrumps'] = ['States'];
		$this->data['USER_GROUPS'] = unserialize(USER_GROUP);
		$this->data['LEAVE_TYPES'] = unserialize(LEAVE_TYPES);
		$this->data['ATTENDANCE_REG_TYPE'] = unserialize(ATTENDANCE_REG_TYPE);
  }

  function index() {	
    $this->data['partial'] = 'admin/leaves/index';
    $this->data['page_title'] = 'Group Leave';
    //array_push($this->data['breadcrumps'],"Listing");
    $this->load->view('templates/index', $this->data);
  }
  
  function group_leaves() {	
    $this->data['groupleave_list'] = $this->leaves_model->get_all_group_leave();
    
    $this->data['partial'] = 'admin/leaves/group_leaves';
    $this->data['page_title'] = 'Group Leave';
    //array_push($this->data['breadcrumps'],"Listing");
    $this->load->view('templates/index', $this->data);
  }
	
  public function add_group_leave()
  {
    $this->data['partial'] = 'admin/leaves/form_group_leaves';
    $this->data['action'] = 'create_group_leave';
    $this->data['page_title'] = 'Add Group Leave';
    $this->load->view('templates/index', $this->data);
  }
	
  function create_group_leave() 
  {		     
    $mdata['user_group'] = $this->input->post('user_group');
    $mdata['leave_count'] = $this->input->post('leave_count');
    $mdata['start_date'] = date('Y-m-d', strtotime($this->input->post('leavestart')));
    $mdata['created_at'] = date('Y-m-d H:i:s');
    $mdata['created_by'] = $this->data['current_user_id'];

		$groupData = $this->leaves_model->get_by_group($this->input->post('user_group'), 1);
		if(!empty($groupData)){
			$pre_id = $groupData[0]->id;
			$udata['end_date'] = date('Y-m-d H:i:s', strtotime($this->input->post('leavestart') . '-1 day'));
			$udata['updated_at'] = date('Y-m-d H:i:s');
			$udata['updated_by'] = $this->data['current_user_id'];
			$this->leaves_model->update_leave_group($udata, $pre_id);
		}
    $res = $this->leaves_model->insert_leave_group($mdata);
    if($res)
    {
      header('location:'.base_url()."admin/leaves/group_leaves");
    }
    else
    {
      $this->data['partial'] = 'admin/leaves/form_group_leaves';
      $this->data['action'] = 'create_group_leave';
      $this->data['page_title'] = 'Add Group Leave';
      $this->load->view('templates/index', $this->data);	
    }
  }
  
  public function edit_group_leave($id)
	{
		$groupleaves = $this->leaves_model->groupLeaveById($id);
		$this->data['groupleaves'] = $groupleaves; 
    $this->data['partial'] = 'admin/leaves/form_group_leaves';
    $this->data['action'] = 'update_group_leave';
    $this->data['page_title'] = 'Edit Group Leave';
    $this->load->view('templates/index', $this->data);
	}
	
  function update_group_leave() 
	{	
    $mdata['user_group'] = $this->input->post('user_group');
    $mdata['leave_count'] = $this->input->post('leave_count');
    $mdata['start_date'] = date('Y-m-d', strtotime($this->input->post('leavestart')));
    $mdata['updated_at'] = date('Y-m-d H:i:s');
    $mdata['updated_by'] = $this->data['current_user_id'];

    $res=$this->leaves_model->update_leave_group($mdata, $_POST['id']);
    if($res)
    {
      header('location:'.base_url()."admin/leaves/group_leaves");
    }
    else
    {
      $this->data['groupleaves'] = $this->leaves_model->getById($_POST['id']);
      $this->data['partial'] = 'admin/leaves/form_group_leaves';
      $this->data['action'] = 'update_group_leave';
      $this->data['page_title'] = 'Edit Leave';
      $this->load->view('templates/index', $this->data);		
    }
  }
	
   public function delete_group_leave($id)
   {
		$State = $this->leaves_model->groupLeaveById($id);
			
		$this->leaves_model->delete_a_group_leave($id);
		header('location:'.base_url()."admin/leaves/group_leaves");
   }
  
  function leave_types() {	
    $this->data['stateleaves'] = $this->leaves_model->get_all_state_leaves();
    
    $this->data['partial'] = 'admin/leaves/leave_types';
    $this->data['page_title'] = 'Leave Types';
    $this->load->view('templates/index', $this->data);
  }
	
  public function add_state_leave()
  {
		$this->data['states'] = $this->states_model->get_all();
    $this->data['partial'] = 'admin/leaves/form_leave_types';
    $this->data['action'] = 'create_state_leave';
    $this->data['page_title'] = 'Add Leave Type';
    $this->load->view('templates/index', $this->data);
  }
	
  function create_state_leave() 
  {		     
    $mdata['state_code'] = $this->input->post('state_code');
    $mdata['leave_type'] = $this->input->post('leave_type');
    $mdata['leave_count'] = $this->input->post('leave_count');
    $mdata['start_date'] = date('Y-m-d H:i:s');
    $mdata['created_at'] = date('Y-m-d H:i:s');
    $mdata['created_by'] = $this->data['current_user_id'];

		$groupData = $this->leaves_model->get_by_state_leave($this->input->post('state_code'), $this->input->post('leave_type'), 1);
		if(!empty($groupData)){
			$pre_id = $groupData[0]->id;
			$udata['end_date'] = date('Y-m-d H:i:s');
			$udata['updated_at'] = date('Y-m-d H:i:s');
			$udata['updated_by'] = $this->data['current_user_id'];
			$this->leaves_model->update_state_leave($udata, $pre_id);
		}
    $res = $this->leaves_model->insert_state_leave($mdata);
    if($res)
    {
      header('location:'.base_url()."admin/leaves/leave_types");
    }
    else
    {
      $this->data['partial'] = 'admin/leaves/form_leave_types';
      $this->data['action'] = 'create_state_leave';
      $this->data['page_title'] = 'Add State Leave Type';
      $this->load->view('templates/index', $this->data);	
    }
  }
  
  public function edit_state_leave($id)
	{
		$this->data['states'] = $this->states_model->get_all();
		$stateleaves = $this->leaves_model->stateLeaveById($id);
		$this->data['stateleaves'] = $stateleaves; 
    $this->data['partial'] = 'admin/leaves/form_leave_types';
    $this->data['action'] = 'update_state_leave';
    $this->data['page_title'] = 'Edit Leave Type';
    $this->load->view('templates/index', $this->data);
	}
	
  function update_state_leave() 
	{	
    $mdata['state_code'] = $this->input->post('state_code');
    $mdata['leave_type'] = $this->input->post('leave_type');
    $mdata['leave_count'] = $this->input->post('leave_count');
    $mdata['updated_at'] = date('Y-m-d H:i:s');
    $mdata['updated_by'] = $this->data['current_user_id'];

    $res=$this->leaves_model->update_state_leave($mdata, $_POST['id']);
    if($res)
    {
      header('location:'.base_url()."admin/leaves/leave_types");
    }
    else
    {
      $this->data['stateleaves'] = $this->leaves_model->getById($_POST['id']);
      $this->data['partial'] = 'admin/leaves/form_leave_types';
      $this->data['action'] = 'update_state_leave';
      $this->data['page_title'] = 'Edit Leave Type';
      $this->load->view('templates/index', $this->data);		
    }
  }
	
   public function delete_state_leave($id)
   {
		$State = $this->leaves_model->stateLeaveById($id);
			
		$this->leaves_model->delete_a_state_leave($id);
		header('location:'.base_url()."admin/leaves/leave_types");
   }
  
	 public function credit_leave_list()	
	 {
		 $this->load->model('Users_model');
		 //$this->load->model('Attendance_model');
		 
		 $this->data['spark_id'] = '';
		 
		 $creditleavelist = array();
		 $spark_ids = array();
		 $sparkNames = array();
		 if(!empty($_POST) && $_POST['submit'] == 'Get List')
     {
				$spark_id = $_POST['spark_id'];
				
				//$creditleavestart = ($_POST['creditleavestart'] != '' ? date('Y-m-d', strtotime($_POST['creditleavestart'])) : '');
				//$creditleaveend = ($_POST['creditleaveend'] != '' ? date('Y-m-d', strtotime($_POST['creditleaveend'])) : '');
				$creditleavelist = $this->leaves_model->get_leave_credits($spark_id);
				foreach($creditleavelist as $creditleaves)
				{
				  if(!in_array($creditleaves->spark_id, $spark_ids))
						array_push($spark_ids, $creditleaves->spark_id);	
				}
				if(!empty($spark_ids)){
					 $sparkData = $this->Users_model->getByIds($spark_ids);
					 foreach($sparkData as $sparks)
					 {
						 $sparkNames[$sparks->id] = $sparks->name;
					 }
				}
				$this->data['spark_id'] = $spark_id;
		 }
		 
		 if($this->data['current_role'] == 'super_admin' || $this->data['current_role'] == 'HR'){
			$sparks = $this->Users_model->get_all_users();
			$this->data['sparks'] = $sparks;
		 }
		 
		 $this->data['sparkNames'] = $sparkNames;
		 $this->data['creditleavelist'] = $creditleavelist;

		 $this->data['page_title'] = 'Credit Leave List';
		 $this->data['partial'] = 'admin/leaves/credit_leave_list';
     $this->load->view('templates/index', $this->data);
	 }
  
	 public function credit_leaves()
   {
			$this->load->model('Users_model');
			
      if(!empty($_POST) && $_POST['submit'] == 'Save')
      {
				$spark_id = $_POST['spark_id'];
				$leave_type = $_POST['leave_type'];
				$leave_count = $_POST['leave_count'];
				$description = $_POST['description'];
				
				/*$leavedate = date('Y-m-d', strtotime($_POST['leavedate']));
				$check_leave = $this->leaves_model->get_leave_credit_details($spark_id, $leave_type,'', '', $leavedate);
				
				//if(empty($check_leave))
				//{ */
				
					$data['spark_id'] = $spark_id;
					$data['earned_date'] = $leavedate;
					$data['leave_type'] = $leave_type;
					$data['leave_count'] = $leave_count;
					$data['description'] = $description;
					$data['created_at'] = date('Y-m-d');
					$data['created_by'] = $this->data['current_user_id'];
					$this->leaves_model->insert_leave_credit_details($data);
					
					$creditData = $this->leaves_model->get_leave_credits($spark_id, $leave_type);
					if(empty($creditData)){
						$cdata['spark_id'] = $spark_id;
						$cdata['leave_type'] = $leave_type;
						$cdata['leave_earned'] = $leave_count;  
						$this->leaves_model->insert_leave_credit($cdata);  
					}
					else{
						$mdata['leave_earned'] = $creditData[0]->leave_earned+$leave_count;  
						$this->leaves_model->update_leave_credit($mdata, $creditData[0]->id);    
					}
					$this->session->set_flashdata('success', 'Leave Added Successfully.');
					redirect('admin/leaves/credit_leave_list'); 
				/*}
				else{
					$this->session->set_flashdata('alert', 'Leave already added.');
				}*/
			}
      
		 $sparks = $this->Users_model->get_all_users();
		 $this->data['sparks'] = $sparks;
		 $this->data['page_title'] = 'Credit Leave';
		 $this->data['partial'] = 'admin/leaves/mark_leaves';
     $this->load->view('templates/index', $this->data);
   }
      
  function carry_forward_policies() {	
    $this->data['groupleave_list'] = $this->leaves_model->get_all_carry_forward_policies();
    
    $this->data['partial'] = 'admin/leaves/carry_forward_policies';
    $this->data['page_title'] = 'Group Leave';
    //array_push($this->data['breadcrumps'],"Listing");
    $this->load->view('templates/index', $this->data);
  }
  
  public function add_carry_forward_policy()
  {
    $this->data['partial'] = 'admin/leaves/form_carry_forward_policy';
    $this->data['action'] = 'create_carry_forward_policy';
    $this->data['page_title'] = 'Add Group Leave';
    $this->load->view('templates/index', $this->data);
  }
	
  function create_carry_forward_policy() 
  {		     
    $mdata['user_group'] = $this->input->post('user_group');
    $mdata['leave_type'] = $this->input->post('leave_type');
    $mdata['forward_value'] = $this->input->post('forward_value');
    $mdata['forward_type'] = $this->input->post('forward_type');
    $mdata['max_value'] = $this->input->post('max_value');
    $mdata['created_at'] = date('Y-m-d H:i:s');
    $mdata['created_by'] = $this->data['current_user_id'];
/*
		$groupData = $this->leaves_model->get_by_group($this->input->post('user_group'), 1);
		if(!empty($groupData)){
			$pre_id = $groupData[0]->id;
			$udata['end_date'] = date('Y-m-d H:i:s', strtotime($this->input->post('leavestart') . '-1 day'));
			$udata['updated_at'] = date('Y-m-d H:i:s');
			$udata['updated_by'] = $this->data['current_user_id'];
			$this->leaves_model->update_leave_group($udata, $pre_id);
		} */
    $res = $this->leaves_model->insert_carry_forward_policy($mdata);
    if($res)
    {
      header('location:'.base_url()."admin/leaves/carry_forward_policies");
    }
    else
    {
      $this->data['partial'] = 'admin/leaves/form_carry_forward_policy';
      $this->data['action'] = 'create_carry_forward_policy';
      $this->data['page_title'] = 'Add Group Leave';
      $this->load->view('templates/index', $this->data);	
    }
  }
  
  public function edit_carry_forward_policy($id)
	{
		$groupleaves = $this->leaves_model->carryForwardPolicyById($id);
		$this->data['groupleaves'] = $groupleaves; 
    $this->data['partial'] = 'admin/leaves/edit_form_carry_forward_policy';
    $this->data['action'] = 'update_carry_forward_policy';
    $this->data['page_title'] = 'Edit Group Leave';
    $this->load->view('templates/index', $this->data);
	}
	
  function update_carry_forward_policy() 
	{	
    $mdata['forward_value'] = $this->input->post('forward_value');
    $mdata['forward_type'] = $this->input->post('forward_type');
    $mdata['max_value'] = $this->input->post('max_value');
    $mdata['updated_at'] = date('Y-m-d H:i:s');
    $mdata['updated_by'] = $this->data['current_user_id'];

    $res=$this->leaves_model->update_carry_forward_policy($mdata, $_POST['id']);
    if($res)
    {
      header('location:'.base_url()."admin/leaves/carry_forward_policies");
    }
    else
    {
      $this->data['groupleaves'] = $this->leaves_model->getById($_POST['id']);
      $this->data['partial'] = 'admin/leaves/edit_form_carry_forward_policy';
      $this->data['action'] = 'update_carry_forward_policy';
      $this->data['page_title'] = 'Edit Leave';
      $this->load->view('templates/index', $this->data);		
    }
  }
	
  public function delete_carry_forward_policy($id)
  {
		$State = $this->leaves_model->groupLeaveById($id);
			
		$this->leaves_model->delete_a_group_leave($id);
		header('location:'.base_url()."admin/leaves/group_leaves");
  }
     
	 function credit_leave_details()
	 {
		 $cur_id = $_REQUEST['cur_id'];

		 $creditData = $this->leaves_model->get_credit_leave_details($cur_id);
		 
		 $this->data['creditData'] = $creditData;
    
			$this->load->view('admin/leaves/credit_data',$this->data);
		 
	 }
	 
	 public function spark_leave_details()	
	 {
		 $this->load->model('Users_model');
		 //$this->load->model('Attendance_model');
		 
		 $leaveslist = array();
		 $creditleavelist = array();
		 $spark_ids = array();
		 $sparkNames = array();
		 $creditleavestart = '';
		 $creditleaveend = '';
		 $spark_id = '';
		 
		 //$creditleavestart = date('Y-m-01');
		 //$creditleaveend = date('Y-m-d');
		 
		 $inpmonth = date('m');
		 $inpyear = date('Y');
		 $session_start_year = date('Y');
			if($inpmonth < SESSION_START_MONTH)
			{
				$session_start_year = $inpyear-1;
			}
		 
		 $creditleavestart = date("Y-m-d",strtotime($session_start_year."-".SESSION_START_MONTH."-01"));
		 $creditleaveend = date("Y-m-d");
		 		
		 if(!empty($_POST) && $_POST['submit'] != '')
     {
			  $spark_id = $_POST['spark_id'];
				
				//$creditleavestart = ($_POST['creditleavestart'] != '' ? date('Y-m-d', strtotime($_POST['creditleavestart'])) : '');
				//$creditleaveend = ($_POST['creditleaveend'] != '' ? date('Y-m-d', strtotime($_POST['creditleaveend'])) : '');
				
				$leaveslist = $this->leaves_model->get_all($spark_id, $creditleavestart, $creditleaveend,'id desc');
				
				$this->data['spark_id'] = $spark_id;	
		 }
		 
		 $this->data['creditleavestart'] = $creditleavestart;
		 $this->data['creditleaveend'] = $creditleaveend;
		 $this->data['leaveslist'] = $leaveslist;

		$download = (isset($_POST['download']) and $_POST['download'] == 1 ? $_POST['download'] : 0);
    $pdf = (isset($_POST['pdf']) and $_POST['pdf'] == 1 ? $_POST['pdf'] : 0);
    
		if ($download == 1)
    {
      $myFile = "./uploads/reports/spark_leave_details.xls";
      $this->load->library('parser');
			$this->data['download'] = $download;
      //pass retrieved data into template and return as a string
      $stringData = $this->parser->parse('admin/leaves/spark_leave_details', $this->data, true);
      //open excel and write string into excel
      $fh = fopen($myFile, 'w') or die("can't open file");
      fwrite($fh, $stringData);
      fclose($fh);
      //download excel file
      $this->common_functions->downloadExcel("spark_leave_details");
      unlink($myFile);
    }
    else if($pdf == 1)
    {
				//pass retrieved data into template and return as a string
        $this->data['pdf'] = 1;
        $summary = $this->load->view('admin/leaves/spark_leave_details',$this->data,true);
        $this->common_functions->create_pdf($summary,'spark_leave_details');
    }
    else{
			if($this->data['current_role'] == 'super_admin' || $this->data['current_role'] == 'HR'){
				$sparks = $this->Users_model->get_all_users();
				$this->data['sparks'] = $sparks;
			 }
			 
			 $this->data['page_title'] = 'Credit Leave List';
			 $this->data['partial'] = 'admin/leaves/spark_leave_details';
			 $this->load->view('templates/index', $this->data);
		 }
	 }
	 
}
	
	
	
	
	
	
	
