<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sparks extends CI_Controller {

	function __construct()	
	{
		parent::__construct();
		$this->load->helper('url');
    $this->load->helper('application');
		$this->data['current_page'] = 'sparks';
		$this->load->library('form_validation');
		$this->load->library('common_functions');
    $this->load->model('sparks_model');
    $this->load->model('users_model');
    $this->load->model('team_members_model');
		$this->load->model('states_model');
		$this->load->model('Employees_model');
		$this->load->helper('date');
    
    if($this->session->userdata('admin_logged_in'))
		{
		  $session_data = $this->session->userdata('admin_logged_in');
      $this->data['current_user_id'] = $session_data['id'];
      $this->data['current_username'] = $session_data['username'];
      $this->data['current_name'] = $session_data['name'];
      $this->data['current_role'] = $session_data['role'];
      $this->data['user_state_id'] = $session_data['state_id'];
      $this->data['user_district_id'] = $session_data['district_id'];
      $this->data['current_group'] = $session_data['user_group'];
    }
    else
    {
      //If no session, redirect to login page
      redirect('admin/login', 'refresh');
    }
	}

	public function index()
	{
      //$this->data['sparks_list'] = $this->sparks_model->get_all_sparks($this->data['current_user_id']); 
      $this->data['sparks_list'] = $this->sparks_model->get_sparks_by_role(array('field_user', 'manager', 'state_person', 'accounts', 'reports'));
      //print_r($this->data['sparks_list']);exit;
      $this->data['partial'] = 'admin/sparks/index';
      $this->data['page_title'] = 'sparks';
      $this->load->view('templates/index', $this->data);
	}
  
  public function sparklist()
	{
      $this->data['sparks_list'] = $this->sparks_model->get_sparks_by_role(array('field_user', 'manager', 'state_person', 'accounts', 'reports'));
      
      $myFile = "./uploads/reports/sparklist.xls";
      $this->load->library('parser');
      //pass retrieved data into template and return as a string
      $stringData = $this->parser->parse('admin/sparks/sparklist', $this->data, true);
      //open excel and write string into excel
      $fh = fopen($myFile, 'w') or die("can't open file");
      fwrite($fh, $stringData);
      fclose($fh);
      //download excel file
      $this->common_functions->downloadExcel("sparklist");
      unlink($myFile);
	}

	public function add()
	{
      $this->data['partial'] = 'admin/sparks/form';
      $this->data['action'] = 'create';
      $this->data['logged_in_user'] = $this->data['current_user_id'];
      $this->data['page_title'] = 'Add User';
      $this->load->model('states_model');
      $this->data['states'] = $this->states_model->get_all_states();
      $this->load->view('templates/index', $this->data);
	}
	
	public function create()
	{
      //print_r($_POST);exit;
      $mdata['login_id'] = $this->input->post('login_id');
      $mdata['role'] = $this->input->post('role');
      $mdata['name'] = $this->input->post('name');
      $mdata['email'] = $this->input->post('email');
      $mdata['mobile'] = $this->input->post('mobile');
      $mdata['password'] = md5($this->input->post('password'));
      $mdata['district_id'] = 0;
      $mdata['login_status'] = 1;
      $mdata['state_id'] = $this->input->post('state_id');
      $mdata['manager_id'] = $this->input->post('manager_id');
      $mdata['created_on'] = date('Y-m-d H:i:s');
      $mdata['created_by'] = $this->data['current_user_id'];
      
      $user_id = $this->sparks_model->add($mdata);

      if($user_id > 0){
        //$district_id = $this->input->post('district_id');
        $block_ids = $this->input->post('block_id');
        foreach ($block_ids as $block_id) {
          $dist_block = explode('_', $block_id);
          $this->sparks_model->save_user_district_block($user_id, $dist_block);
          $this->Employees_model->save_user_district_block_history($user_id, $dist_block, $this->data['current_user_id']);
        }

        $sdata['user_id'] = $user_id;
        $sdata['state_id'] = $mdata['state_id'];
        $sdata['start_date'] = date("Y-m-d");
        $this->sparks_model->add_user_state($sdata);
            
        header('location:'.base_url()."admin/sparks/");

      }else{
        $this->session->set_flashdata('alert', 'User id already exist.');
        redirect('sparks', 'refresh');
      }
          
	}

	public function edit()
	{
      $id = $this->uri->segment(4);
      $this->data['action'] = 'update';
      $this->data['user'] = $this->sparks_model->getById($id);
      $this->load->model('states_model');
      $this->data['states'] = $this->states_model->get_all_states();
      $districts = $this->sparks_model->get_member_district_ids($id);
      $district_ids = array(); $i = 0;
      foreach ($districts as $district) {
        $district_ids[$i] = $district->district_id;
        $i++;
      }
      $this->data['district_ids'] = json_encode(array_values(array_unique($district_ids)));
      $blocks = $this->sparks_model->get_member_block_ids($id);
      $block_ids = array(); $j = 0;
      foreach ($blocks as $block) {
        $block_ids[$j] = $block->block_id;
        $j++;
      }
      $this->data['block_ids'] = json_encode(array_values(array_unique($block_ids)));
      $this->data['logged_in_user'] = $this->data['current_user_id'];
      $this->data['page_title'] = 'Edit User';
      $this->data['partial'] = 'admin/sparks/edit_form';
      $this->load->view('templates/index', $this->data);
	}
  
	public function update()
	{
      $mdata['login_id'] = $this->input->post('login_id');
      $mdata['role'] = $this->input->post('role');
      $mdata['name'] = $this->input->post('name');
      $mdata['email'] = $this->input->post('email');
      $mdata['mobile'] = $this->input->post('mobile');
      $mdata['manager_id'] = $this->input->post('manager_id');
      $password = $this->input->post('password');
      $mdata['district_id'] = 0;
      $mdata['state_id'] = $this->input->post('state_id');
      $mdata['updated_on'] = date('Y-m-d H:i:s');
      $mdata['updated_by'] = $this->data['current_user_id'];
      if ($password != "")
      {
        $mdata['password'] = md5($this->input->post('password'));
      }
      $this->sparks_model->update_info($mdata, $_POST['id']);

      $user_id = $_POST['id'];
      $districts = $this->sparks_model->get_member_district_ids($user_id);
      $district_ids = array(); $i = 0;
      foreach ($districts as $district) {
        $district_ids[$i] = $district->district_id;
        $i++;
      }
      $blocks = $this->sparks_model->get_member_block_ids($user_id);
      $block_ids = array(); $j = 0;
      foreach ($blocks as $block) {
        $block_ids[$j] = $block->block_id;
        $j++;
      }
      $blocks_old = array();
      for($i = 0; $i < count($district_ids); $i++){
        $temp = array($district_ids[$i], $block_ids[$i]);
        $blocks_old[$i] = implode("_", $temp);
      }

      $blocks_new = array();
      if(!empty($this->input->post('block_id'))){
        $blocks_new = $this->input->post('block_id');
      }

      $blocks_delete = array_diff($blocks_old, $blocks_new);
      //print_r($blocks_delete);exit;

      foreach ($blocks_delete as $blocks_del) {
        $dist_block = explode('_', $blocks_del);
        $this->sparks_model->delete_user_district_block($user_id, $dist_block);
        $this->Employees_model->update_user_district_block_history($user_id, $dist_block, $this->data['current_user_id']);
      }

      $blocks_save = array_diff($blocks_new, $blocks_old);
      
      foreach ($blocks_save as $blocks_s) {
        $dist_block = explode('_', $blocks_s);
        $this->sparks_model->save_user_district_block($user_id, $dist_block);
        $this->Employees_model->save_user_district_block_history($user_id, $dist_block, $this->data['current_user_id']);
      }
      header('location:'.base_url()."admin/sparks/".$this->index());
	}
				  
	public function delete($id)
	{
      $this->users_model->delete_a_user($id);
      $this->team_members_model->delete_spark_district_block($id);
      $this->Employees_model->update_user_district_block_history($id, '', $this->data['current_user_id']);
      header('location:'.base_url()."admin/sparks".$this->index());
	}

  public function changestatus()
  {
    $id = $this->uri->segment(4);
    $this->data['action'] = 'update';
    $userData = $this->sparks_model->getById($id);
    //print_r($this->data['newsletter']);
    extract ($userData);
    if( $status==1){
      $new_status=0;
    }
    else
    {
      $new_status="1";
    }
    $mdata['status'] = $new_status;
    $mdata['updated_on'] = date('Y-m-d H:i:s');
    $mdata['updated_by'] = $this->data['current_user_id'];
    $res=$this->sparks_model->update_info($mdata, $id);
    
    if($new_status == 0){
      $this->load->model('user_district_blocks_model');
      //Delete all district block mapped with spark id
      $this->user_district_blocks_model->delete_user_district_block($id);		
      //Set end date of user_district_block_history table
      $this->Employees_model->update_user_district_block_history($id, '', $this->data['current_user_id']);  
      //Set End date of spark_user_state
      $this->sparks_model->set_spark_state_end_date($id); 
    }
    else{
			//Insert new entry in spark_user_state on set Active status
			$sdata = array();
			$sdata['user_id'] = $id;
			$sdata['state_id'] = $state_id;
			$sdata['start_date'] = date("Y-m-d");
			$this->sparks_model->add_user_state($sdata);
			
			//Check employee mapping & insert,if not exist, in employee detail table
			$checkDetails = $this->Employees_model->check_detail_exist($id);
			if(empty($checkDetails))
			{
				$data['spark_id'] = $id;
				$data['created_by'] = $this->data['current_user_id'];
				$data['created_at'] = date('Y-m-d H:i:s');
				$this->Employees_model->add_spark_details($data);
			}
		}
    if($res)
    {
      header('location:'.base_url()."admin/sparks".$this->index());
    }
  }

  public function change_state(){
    $this->data['page_title'] = 'Spark Movement';
    $this->data['sparks_list'] = $this->sparks_model->get_sparks_by_role(array('manager', 'field_user'));
    $this->data['states'] = $this->states_model->get_all_states();
    //echo "<pre>";print_r($this->data['sparks']);exit;
    $this->data['partial'] = 'admin/sparks/change_state';
    $this->load->view('templates/index', $this->data); 
  }

  public function submit_change_state(){
    $state_id = $_POST['state_id'];
    $user_id = $_POST['user_id'];
    $date = $_POST['date'];
    if(!empty($state_id)){
      $state_head_id = 0;
    	$this->db->select('id, name')->where_in('role', array('state_person'));
      $res = $this->db->where('state_id', $state_id)->get('sparks')->result();
    	$row = array();
    	$row_set = array();
    	foreach ($res as $r) {
        $state_head_id = $r->id;
      }

      $this->sparks_model->change_state($state_id, $user_id, $date, $state_head_id);
      $this->sparks_model->delete_user_district_block($user_id);
      $this->Employees_model->update_user_district_block_history($user_id, '', $this->data['current_user_id'], $date);
      header('location:'.base_url()."admin/sparks/change_state");
    }
  }

  public function get_manager_list(){
    $this->db->select('id, name,login_id')->where_in('role', array('manager', 'state_person'));
    $res = $this->db->where('state_id', $_GET['state'])->get('sparks')->result();
    $row = array();
    $row_set = array();
    foreach ($res as $r) {
      $row['id'] = $r->id;
      $row['value'] = $r->name." (".$r->login_id.")";
      array_push($row_set, $row);
    }
    //print_r($row_set);exit;
    echo json_encode($row_set); //format the array into json data
  }

  public function check_user_id(){
    $res = $this->sparks_model->check_user_id($_POST['user_id']);
    if($res == 0){
      echo "User id already exists.";
    }else{
      echo '';
    }
  }

  public function attendance_list()
  {
    $spark_id = $this->data['current_user_id'];
    $state_id = '';
    $this->data['state_list'] = $this->states_model->get_all_states();
      
    $attendance_array = array();
    $attandance_data = array();
    $activitydatestart = date('Y-m-d');
    $activitydateend = date('Y-m-d');
    $time_filter = '';
    
    if(isset($_POST) && !empty($_POST['submit']))
    {
      $state_id = $_POST['state_id'];
      $time_filter = $_POST['time_filter'];
      
      $this->load->Model('Users_model');
      $sparkData = $this->Users_model->getSparkByCurrentState($state_id);
      $spark_ids = array();
      foreach($sparkData as $sparks){
        $spark_ids[] = $sparks->user_id;
      }
      //print_r($spark_ids);
      $activitydatestart = (!empty($_POST['activitydatestart']) ? date('Y-m-d',strtotime($_POST['activitydatestart'])) : date('Y-m-d'));
      $activitydateend = (!empty($_POST['activitydateend']) ? date('Y-m-d',strtotime($_POST['activitydateend'])) : date('Y-m-d'));
      
      $activity_type = array('attendance-in', 'attendance-out');
      
      $this->load->model('Tracking_model');   
      $attandance_data = $this->Tracking_model->get_track_attendance_list($spark_ids, $activitydatestart, $activitydateend, $activity_type);
      
      //print_r($attandance_data);
      
      $attendance_array = array();
      foreach($attandance_data as $attandances){
        
          $login_id = $attandances->login_id;
          $login_name = $attandances->name;
          $tracking_time = $attandances->tracking_time;
          $activity_type = $attandances->activity_type;
          $created_by = $attandances->created_by;
          $tracking_location = (!empty($attandances->tracking_location) ? $attandances->tracking_location : '');
          $lat_long_location = (!empty($attandances->lat_long_location) ? $attandances->lat_long_location : '');
          $tracking_date = date('d-m-Y', strtotime($attandances->tracking_time));
          
          
          if(!isset($attendance_array[$tracking_date][$login_id])){
            $attendance_array[$tracking_date][$login_id] = array();
          }
          //else{
            if($activity_type == 'attendance-in'){
              if(!isset($attendance_array[$tracking_date][$login_id]['attendance-in'])){
                $attendance_array[$tracking_date][$login_id]['attendance-in'] = array();
                $attendance_array[$tracking_date][$login_id]['attendance-in'][] = $tracking_time;
                $attendance_array[$tracking_date][$login_id]['attendance-in-location'][] = (!empty($tracking_location) ? $tracking_location : $lat_long_location);
              }
              else{
                $attendance_array[$tracking_date][$login_id]['attendance-in'][] = $tracking_time;
                $attendance_array[$tracking_date][$login_id]['attendance-in-location'][] = (!empty($tracking_location) ? $tracking_location : $lat_long_location);
              }
              $attendance_array[$tracking_date][$login_id]['login_name'][] = $login_name;
              
            }
            if($activity_type == 'attendance-out'){
              if(!isset($attendance_array[$tracking_date][$login_id]['attendance-out'])){
                $attendance_array[$tracking_date][$login_id]['attendance-out'] = array();
                $attendance_array[$tracking_date][$login_id]['attendance-out'][] = $tracking_time;
                $attendance_array[$tracking_date][$login_id]['attendance-out-location'][] = (!empty($tracking_location) ? $tracking_location : $lat_long_location);
              }
              else{
                $attendance_array[$tracking_date][$login_id]['attendance-out'][] = $tracking_time;
                $attendance_array[$tracking_date][$login_id]['attendance-out-location'][] = (!empty($tracking_location) ? $tracking_location : $lat_long_location);
              }
              $attendance_array[$tracking_date][$login_id]['created_by'][] = $created_by;
            }
          //}
      }
      //print_r($attendance_array);
      //die;
    }

    $custom_scripts = ['map.js']; 
    $this->data['time_filter'] = $time_filter; 
    $this->data['custom_scripts'] = $custom_scripts; 
    $this->data['activitydatestart'] = date('d-m-Y',strtotime($activitydatestart)); 
    $this->data['activitydateend'] = date('d-m-Y',strtotime($activitydateend)); 
    $this->data['attendance_array'] = $attendance_array;
    $this->data['state_id'] = $state_id;
    
    $download = (isset($_POST['download']) and $_POST['download'] == 1 ? $_POST['download'] : 0);
    $pdf = (isset($_POST['pdf']) and $_POST['pdf'] == 1 ? $_POST['pdf'] : 0);
    
		if ($download == 1)
    {
      $myFile = "./uploads/reports/attendance_list.xls";
      $this->load->library('parser');
			$this->data['download'] = $download;
      //pass retrieved data into template and return as a string
      $stringData = $this->parser->parse('admin/sparks/attendance_list', $this->data, true);
      //open excel and write string into excel
      $fh = fopen($myFile, 'w') or die("can't open file");
      fwrite($fh, $stringData);
      fclose($fh);
      //download excel file
      $this->common_functions->downloadExcel("attendance_list");
      unlink($myFile);
    }
    else if($pdf == 1)
    {
        //pass retrieved data into template and return as a string
        $this->data['pdf'] = 1;
        $summary = $this->load->view('admin/sparks/attendance_list',$this->data,true);
        $this->common_functions->create_pdf($summary,'attendance_list');
    }
    else{
      $this->data['partial'] = 'admin/sparks/attendance_list';			
      $this->load->view('templates/index', $this->data);
    }
  }
  
  public function sparks_leave_list()
	{
      $stateData = $this->states_model->get_all_states();
      
      $start_date = date('2019-09-21');
      $end_date = date('2019-10-20');
      
      $this->load->Model('Users_model');
      $this->load->Model('Leaves_model');
      
      $sparks_leave_data = array();
        
      foreach($stateData as $states){
        $sparkData = $this->Users_model->getSparkByCurrentState($states->id);
        foreach($sparkData as $sparks){
          //$spark_ids[] = $sparks->user_id;
          
          $leavesData = $this->Leaves_model->get_all_leave($sparks->user_id, $start_date, $end_date);
          
          $sparks_leave_data[$sparks->user_id]['state_name'] = $states->name;
          $sparks_leave_data[$sparks->user_id]['name'] = $sparks->name;
          $sparks_leave_data[$sparks->user_id]['login_id'] = $sparks->login_id;
          $sparks_leave_data[$sparks->user_id]['leave_data'] = $leavesData;
        }
        //print_r($sparks_leave_data);
      }
      
      $this->data['sparks_leave_data'] =  $sparks_leave_data; 
      
      $myFile = "./uploads/reports/sparkleavelist.xls";
      $this->load->library('parser');
      //pass retrieved data into template and return as a string
      $stringData = $this->parser->parse('admin/sparks/sparkleavelist', $this->data, true);
      //open excel and write string into excel
      $fh = fopen($myFile, 'w') or die("can't open file");
      fwrite($fh, $stringData);
      fclose($fh);
      //download excel file
      $this->common_functions->downloadExcel("sparkleavelist");
      unlink($myFile);
	}
  
  
  function get_user_blocks()
  {
    $this->load->Model('Users_model');
    
    $user_blocks = $this->Users_model->user_blocks($_GET['uid']);
    
    $block_list = array();
    
    foreach($user_blocks as $user_block)
    {
      $block_list['id'][] = $user_block->id;
      $block_list['value'][] = $user_block->name;
    }
    
    echo json_encode($user_blocks);
    exit;
  }
  
  function save_home_block()
  {
      $user_id = $_POST['userid'];
      
      $mdata['home_block'] = $_POST['block_id'];
      $this->sparks_model->update_info($mdata, $user_id);  
      
      $hblockData = $this->sparks_model->get_last_home_blocks($user_id, 1);
      
      if(isset($_POST['move_block'])){
        
        if(!empty($hblockData)){    
          $mdata1['end_date'] = date('Y-m-d');
          $this->sparks_model->update_home_blocks($mdata1, $hblockData[0]->id);
        }
        $data['user_id'] = $_POST['userid'];
        $data['block_id'] = $_POST['block_id'];
        $data['start_date'] = date('Y-m-d');
        $this->sparks_model->insert_user_home_block($data);
      }
      else{
        if(!empty($hblockData)){      
          $mdata1['block_id'] = $_POST['block_id'];
          $this->sparks_model->update_home_blocks($mdata1, $hblockData[0]->id);
        }
        else{
          $data['user_id'] = $_POST['userid'];
          $data['block_id'] = $_POST['block_id'];
          $data['start_date'] = date('Y-m-d');
          $this->sparks_model->insert_user_home_block($data);
        }
      }
      header('location:'.base_url()."admin/sparks".$this->index());
  }
}
