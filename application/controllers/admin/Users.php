<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends CI_Controller {

	function __construct()	
	{
		parent::__construct();
		$this->load->helper('url');
                $this->load->helper('application');
		$this->data['current_page'] = 'users';
		$this->load->library('form_validation');
		$this->load->library('common_functions');
		$this->load->model('users_model');
                $this->load->model('sparks_model');
		$this->load->helper('date');
    
    if($this->session->userdata('admin_logged_in'))
		{
		  $session_data = $this->session->userdata('admin_logged_in');
      $this->data['id'] = $session_data['id'];
      $this->data['current_username'] = $session_data['username'];
      $this->data['current_name'] = $session_data['name'];
      $this->data['current_role'] = $session_data['role'];
      $this->data['user_state_id'] = $session_data['state_id'];
      $this->data['user_district_id'] = $session_data['district_id'];
    }
    else
    {
      //If no session, redirect to login page
      redirect('admin/login', 'refresh');
    }
	}

	public function index()
	{
//      $this->data['users_list'] = $this->users_model->get_all_users($this->data['id']);
      $this->data['users_list'] = $this->sparks_model->get_sparks_by_role(array('super_admin'));

      $this->data['partial'] = 'admin/users/index';
      $this->data['page_title'] = 'Users';
      $this->load->view('templates/index', $this->data);
	}

	public function add()
	{
      $this->data['partial'] = 'admin/users/form';
      $this->data['action'] = 'create';
      $this->data['page_title'] = 'Add User';
      $this->load->model('states_model');
      $this->data['states'] = $this->states_model->get_all_states();
      $this->load->view('templates/index', $this->data);
	}
	
	public function create()
	{
      $mdata['login_id'] = $this->input->post('login_id');
      $mdata['role'] = $this->input->post('role');
      $mdata['name'] = $this->input->post('name');
      $mdata['email'] = $this->input->post('email');
      $mdata['mobile'] = $this->input->post('mobile');
      $mdata['password'] = md5($this->input->post('password'));
      $mdata['district_id'] = $this->input->post('district_id');
      $mdata['state_id'] = 1; //$this->input->post('state_id');
      $mdata['login_status'] = 1;

      $this->users_model->add($mdata);
          
      header('location:'.base_url()."admin/users/");
	}

	public function edit()
	{
      $id = $this->uri->segment(4);
      $this->data['action'] = 'update';
      $this->data['user'] = $this->users_model->getById($id);
      $this->load->model('states_model');
      $this->data['states'] = $this->states_model->get_all_states();
      $this->data['page_title'] = 'Edit User';
      $this->data['partial'] = 'admin/users/form';
      $this->load->view('templates/index', $this->data);
	}
  
	public function update()
	{
      $mdata['login_id'] = $this->input->post('login_id');
      $mdata['role'] = $this->input->post('role');
      $mdata['name'] = $this->input->post('name');
      $mdata['email'] = $this->input->post('email');
      $mdata['mobile'] = $this->input->post('mobile');
      $password = md5($this->input->post('password'));
      $mdata['district_id'] = $this->input->post('district_id');
      $mdata['state_id'] = 1; //$this->input->post('state_id');
      if ($password != "")
      {
        $mdata['password'] = md5($this->input->post('password'));
      }
      $this->users_model->update_info($mdata, $_POST['id']);
      //code by gagan start
      header('location:'.base_url()."admin/users/".$this->index());
	}
				  
	public function delete($id)
	{
      $this->users_model->delete_a_user($id);
      header('location:'.base_url()."admin/users".$this->index());
	}

  public function changestatus()
  {
    $id = $this->uri->segment(4);
    $this->data['action'] = 'update';
    $banner = $this->users_model->getById($id);
    //print_r($this->data['newsletter']);
    extract ($banner);
    if( $status==1){
      $new_status=0;
    }
    else
    {
      $new_status="1";
    }
    $mdata['status'] = $new_status;
    $res=$this->users_model->update_info($mdata, $id);
    if($res)
    {
      header('location:'.base_url()."admin/users".$this->index());
    }
  }
}
