<?php

class Subjects extends CI_Controller  {
  function __construct() {
    parent::__construct();
	
    $this->load->helper(array('form', 'url'));
    $this->load->model('subjects_model');
    $this->load->model('subjects_model');
    $this->load->library('form_validation');
  
    $this->data['current_page'] = 'subjects';
    $this->load->library('common_functions'); 
    
		if($this->session->userdata('admin_logged_in'))
		{
		  $session_data = $this->session->userdata('admin_logged_in');
      $this->data['user_id'] = $session_data['id'];
      $this->data['current_user_id'] = $session_data['id'];
      $this->data['current_username'] = $session_data['username'];
      $this->data['current_name'] = $session_data['name'];
      $this->data['current_role'] = $session_data['role'];
      $this->data['user_state_id'] = $session_data['state_id'];
      $this->data['user_district_id'] = $session_data['district_id'];
      $this->data['current_group'] = $session_data['user_group'];
    }
		else
		{
      redirect('admin/login', 'refresh');
		}		        
  }

  function index() 
  {
    $this->data['subjects_list'] = $this->subjects_model->get_all();
    $this->data['partial'] = 'admin/subjects/index';
    $this->data['page_title'] = 'Subjects';
    $this->load->view('templates/index', $this->data);
    //$this->load->view('admin/admins/index', array('error' => ' ' ));
  }

  public function save(){
    $data = array('name'=> $_POST['name']);
    $this->subjects_model->insert_subjects_to_db($data);
    redirect('admin/subjects', 'refresh');
  }

  public function add()
  {	
    $this->data['partial'] = 'admin/subjects/form';
    $this->data['action'] = 'create';
    $this->data['page_title'] = 'Add Subject';
    $this->load->view('templates/index', $this->data);		
  }

  function create() 
  {	
    $this->load->model('subjects_model');
    
    $name = $this->input->post('name');
    $cdata = $this->subjects_model->getByName($name);
    
    if(empty($cdata)){
      $mdata['name'] = $name;
      $mdata['created_on'] = date('Y-m-d H:i:s');
      $mdata['created_by'] = $this->data['current_user_id'];
      $class_id = $this->subjects_model->add($mdata);
      header('location:'.base_url()."admin/subjects/".$this->index());
    }
    else{
      $this->session->set_flashdata('alert', 'Class already exists.');
      header('location:'.base_url()."admin/subjects/add");
    }
  }

  public function edit()
  {
    $this->load->model('subjects_model');
    $id = $this->uri->segment(4);
    $this->data['subjects'] = $this->subjects_model->getById($id);

		$this->data['partial'] = 'admin/subjects/form';
		$this->data['action'] = 'update';
		$this->data['page_title'] = 'Edit Class';
		$this->load->view('templates/index', $this->data);		
  }

  function update() 
  {	
		$this->load->model('subjects_model');
    
    $name = $this->input->post('name');
    $cdata = $this->subjects_model->getByOtherName($_POST['id'], $name);
    
    if(empty($cdata)){
      $mdata['name'] = $name;
      $mdata['updated_on'] = date('Y-m-d H:i:s');
      $mdata['updated_by'] = $this->data['current_user_id'];
      $res = $this->subjects_model->update_subject($_POST['id'], $mdata);
      header('location:'.base_url()."admin/subjects".$this->index());
    }
    else{
      $this->session->set_flashdata('alert', 'Subject already exists.');
      header('location:'.base_url()."admin/subjects/edit/".$_POST['id']);
    }
  }
  
  public function delete($id)
  {
    $this->subjects_model->delete_a_subject($id);
    header('location:'.base_url()."admin/subjects".$this->index());
  }
  
  public function changestatus()
  {
    $id = $this->uri->segment(4);
    $this->data['action'] = 'update';
    $banner = $this->subjects_model->getById($id);
    extract ($banner);
    if( $status==1){
      $new_status=0;
    }
    else
    {
      $new_status="1";
    }
    $mdata['status'] = $new_status;
    $mdata['updated_on'] = date('Y-m-d H:i:s');
    $mdata['updated_by'] = $this->data['current_user_id'];
    $res=$this->subjects_model->update_subject($id, $mdata);
    header('location:'.base_url()."admin/subjects".$this->index());
  }

}
