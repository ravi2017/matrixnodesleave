<?php

class Schools extends CI_Controller  {
  function __construct() {
    parent::__construct();
    $this->load->helper(array('form', 'url'));
    $this->load->model('users_model');
    $this->load->library('form_validation');
    $this->load->library('common_functions');   
    
		if($this->session->userdata('admin_logged_in'))
		{
		  $session_data = $this->session->userdata('admin_logged_in');
      //$this->data['id'] = $session_data['id'];
      $this->data['current_user_id'] = $session_data['id'];
      $this->data['current_username'] = $session_data['username'];
      $this->data['current_name'] = $session_data['name'];
      $this->data['current_role'] = $session_data['role'];
      $this->data['user_state_id'] = $session_data['state_id'];
      $this->data['user_district_id'] = $session_data['district_id'];
    }
		else
		{
      redirect('admin/login', 'refresh');
		}	
    $this->load->model('schools_model');
    $this->load->model('states_model');   
  }

  function upload()
  {
    $this->load->model('schools_model');
    $this->load->model('clusters_model');
    $this->load->model('blocks_model');
    $this->load->model('districts_model');
    $state_id = $_POST['state'];
    $new_state = 0;
    $student_count = 0;
    $teacher_logins = array();
    $fp = fopen($_FILES['fileToUpload']['tmp_name'],'r') or die("can't open file");
    $count = 0;
    //$all_disecodes = array();

    $check_state = $this->states_model->get_by_id($state_id);
    
    if ($check_state[0]->dise_code == "")
    {      
      $this->session->set_flashdata('alert', "Kindly update the dise code of the STATE before uploading their schools.");
      redirect('admin/schools', 'refresh');
      exit;
    }
    
    while($csv_line = fgetcsv($fp,1024))
    {
        $teacher_login = array();
        $count++;
        $error = 0;
        if($count == 1)
        {
          $district_name = $csv_line[0];//remove if you want to have primary key,
          $block_name = $csv_line[1];
          $cluster_name = $csv_line[2];
          $village = $csv_line[3];
          $dise_code = $csv_line[4];
          $school_name = $csv_line[5];
          $student_count = $csv_line[6];
          if ($district_name != "District Name")
            $error = 1;
          if ($block_name != "Block Name")
            $error = 1;
          if ($cluster_name != "Cluster Name")
            $error = 1;
          if ($village != "Village Name")
            $error = 1;
          if ($dise_code != "DISE Code")
            $error = 1;
          if ($school_name != "School Name")
            $error = 1;
          if ($student_count != "No Of Students")
            $error = 1;

          if ($error == 1)
          {
            $this->session->set_flashdata('alert', "The template is not correct or you had made some changes in the first (header) row of the template. Kindly download the template again and upload schools using the new template.");
            redirect('admin/schools', 'refresh');
          }
          continue;
        }//keep this if condition if you want to remove the first row
        for($i = 0, $j = count($csv_line); $i < $j; $i++)
        {          
          $district_name = $csv_line[0];//remove if you want to have primary key,
          $block_name = $csv_line[1];
          $cluster_name = $csv_line[2];
          $village = $csv_line[3];
          $dise_code = $csv_line[4];
          $school_name = $csv_line[5];
          $student_count = $csv_line[6];
        }
        
        if (strlen($dise_code) == 10)
        {
          $dise_code = str_pad($dise_code,11,'0',0);
        }
        if (substr($dise_code,0,2) == $check_state[0]->dise_code and strlen($dise_code) == 11)
        {
          //array_push($all_disecodes,$dise_code);

          $check_school = $this->schools_model->getByDisecode($dise_code);
          
          if (count($check_school) == 0)
          {
            $state_dise_code = substr($dise_code,0,2);
            $district_dise_code = substr($dise_code,2,2);
            $block_dise_code = substr($dise_code,4,2);
                        
            /*district*/            
            $check_districts = $this->districts_model->get_districts_by_name($state_id, $district_name);
            
            if (count($check_districts) == 1)
            {
              $district_id = $check_districts[0]->id;
            }
            else
            {
              $new_district = array();
              $new_district['name'] = strtoupper($district_name);
              $new_district['state_id'] = $state_id;
              $new_district['dise_code'] = $district_dise_code;
              $district_id = $this->districts_model->insert_district_to_db($new_district);
            }
            /*district*/
            
            /*block*/            
            $check_blocks = $this->blocks_model->get_blocks_by_name($district_id, $block_name);
            if (count($check_blocks) == 1)
            {
              $block_id = $check_blocks[0]->id;
            }
            else
            {
              $new_block = array();
              $new_block['name'] = strtoupper($block_name);
              $new_block['dise_code'] = $block_dise_code;
              $new_block['state_id'] = $state_id;
              $new_block['district_id'] = $district_id;
              $new_block['login_id'] = '';
              $block_id = $this->blocks_model->insert_block_to_db($new_block);
            }
            /*block*/
            
            /*cluster*/            
            $check_clusters = $this->clusters_model->get_clusters_by_name($block_id, $cluster_name);
            if (count($check_clusters) == 1)
            {
              $cluster_id = $check_clusters[0]->id;
            }
            else
            {
              $new_cluster = array();
              $new_cluster['name'] = strtoupper($cluster_name);
              $new_cluster['dise_code'] = '';
              $new_cluster['state_id'] = $state_id;
              $new_cluster['district_id'] = $district_id;
              $new_cluster['block_id'] = $block_id;
              $cluster_id = $this->clusters_model->insert_cluster_to_db($new_cluster);
            }
            /*cluster*/

            /*school*/            
            
            $schooldata['name'] = strtoupper($school_name);
            $schooldata['state_id'] = $state_id;
            $schooldata['district_id'] = $district_id;
            $schooldata['block_id'] = $block_id;
            $schooldata['village_id'] = 0;
            $schooldata['status'] = 1;
            $schooldata['cluster_id'] = $cluster_id;
            $schooldata['dise_code'] = $dise_code;
            $schooldata['phone'] = '';
            $schooldata['srp_drp_name'] = '';
            $schooldata['category'] = '';
            $schooldata['teacher_name'] = '';
            $schooldata['contact_no'] = '';
            $schooldata['no_of_students'] = $student_count;
            $schooldata['created_on'] = date('Y-m-d H:i:s');
            $schooldata['created_by'] = $this->data['current_user_id'];
            $school_id = $this->schools_model->insert_school_to_db($schooldata);
            
            /*school*/
          }
          else{
            $school_id = $check_school['id'];
            $mData['no_of_students'] = $student_count;
            $mData['status'] = 1;
            $mData['updated_on'] = date('Y-m-d H:i:s');
            $mData['updated_by'] = $this->data['current_user_id'];
            
            $this->schools_model->update_info($mData, $school_id);
          }
            
        }
        
        $i++;
        //$data = array(
          //  'id' => $insert_csv['id'] ,
            //'empName' => $insert_csv['empName'],
            //'empAddress' => $insert_csv['empAddress']);
        //$data['crane_features']=$this->db->insert('tableName', $data);
    }
    //$old_schools = new School();
    //$old_schools->where("state_id",$state_id)->where_not_in('dise_code',$all_disecodes)->select('dise_code')->get();

    $this->schools_model->reset_school_rating($state_id);
    $school_averages = $this->schools_model->get_school_district_avg($state_id);
    foreach($school_averages as $school_average)
    {        
      $district_id = $school_average->district_id;
      $average_student = $school_average->AVG_VAL;
      
      $this->schools_model->set_high_enroll_school($district_id, $average_student);
    }
    
    //$old_schools->where("state_id",$state_id)->where_not_in('dise_code',$all_disecodes)->update(array("status"=>0));
    fclose($fp) or die("can't close file");
    $data['success']="success";
    $this->session->set_flashdata('success', "File uploaded and all the schools added successfully");
    redirect('admin/schools', 'refresh');
  }
  
  function disable()
  {
    //$this->load->model('teacher_details_model');
    
    $teacher_logins = array();
    $fp = fopen($_FILES['fileToUpload']['tmp_name'],'r') or die("can't open file");
    $count = 0;
    while($csv_line = fgetcsv($fp,1024))
    {
        $teacher_login = array();
        $count++;
        $error = 0;
        if($count == 1)
        {
          $dise_code = $csv_line[0];
          $action = $csv_line[1];
          if ($action != "Status (disable/enable)")
            $error = 1;
          if ($dise_code != "DISE Code")
            $error = 1;

          if ($error == 1)
          {
            $this->session->set_flashdata('alert', "The template is not correct or you had made some changes in the first (header) row of the template. Kindly download the template again and disable/enable schools using the new template.");
            redirect('admin/schools', 'refresh');
          }
          continue;
        }//keep this if condition if you want to remove the first row
        for($i = 0, $j = count($csv_line); $i < $j; $i++)
        {          
          $dise_code = $csv_line[0];
          $action = strtolower($csv_line[1]);
        }
        
        if (strlen($dise_code) < 11)
        {
          $dise_code = str_pad($dise_code,11,'0',0);
        }
        $check_school = new School();
        $check_school->where("dise_code",$dise_code)->get();

        if ($check_school->result_count() > 0)
        {
          if ($action == "enable")
            $check_school->where("dise_code",$dise_code)->update(array("status"=>1));
          else
            $check_school->where("dise_code",$dise_code)->update(array("status"=>0));
        }
        $i++;
        //$data = array(
          //  'id' => $insert_csv['id'] ,
            //'empName' => $insert_csv['empName'],
            //'empAddress' => $insert_csv['empAddress']);
        //$data['crane_features']=$this->db->insert('tableName', $data);
    }
    fclose($fp) or die("can't close file");
    $data['success']="success";
    $this->session->set_flashdata('success', "File uploaded and all the schools get updated as per the uploaded file");
    redirect('admin/schools', 'refresh');
  }
  
  function download_logins()
  {	
    //$this->load->model('teacher_details_model');
    $selectd_district = 0;
    $selectd_state = 0;
    $selectd_block = 0;
    $selectd_cluster = 0;
    
    if (isset($_POST['state_id']) and $_POST['state_id'] > 0)
    {
      $selectd_state = $_POST['state_id'];
    }
    if (isset($_POST['district_id']) and $_POST['district_id'] > 0)
    {
      $selectd_district = $_POST['district_id'];
    }
    if (isset($_POST['block_id']) and $_POST['block_id'] > 0)
    {
      $selectd_block = $_POST['block_id'];         
    }
    if (isset($_POST['cluster_id']) and $_POST['cluster_id'] > 0)
    {
      $selectd_cluster = $_POST['cluster_id'];         
    }
    //$logins = $this->teacher_details_model->getall($selectd_state, $selectd_district , $selectd_block, $selectd_cluster);


    $myFile = "./uploads/logins.xls";
    $this->load->library('parser');
    $this->data['logins'] = $logins;
    //pass retrieved data into template and return as a string
    $stringData = $this->parser->parse('admin/schools/logins', $this->data, true);

    //open excel and write string into excel
    $fh = fopen($myFile, 'w') or die("can't open file");
    fwrite($fh, $stringData);

    fclose($fh);
    //download excel file
    $this->downloadExcel("logins");
    unlink($myFile);
  }
	
  function index()
  {		
    $custom_scripts = ['ajax-select.js','schools.js'];   
    $this->data['custom_scripts'] = $custom_scripts;
    $selectd_district = 0;
    $selectd_state = 0;
    $selectd_block = 0;
    $selectd_cluster = 0;
    if (isset($_GET['state_id']) and $_GET['state_id'] > 0)
    {
      $selectd_state = $_GET['state_id'];

      $this->data['state_id'] = $selectd_state;
      
    }
    if (isset($_GET['district_id']) and $_GET['district_id'] > 0)
    {
      $selectd_district = $_GET['district_id'];
      $this->data['district_id'] = $selectd_district;

    }
    if (isset($_GET['block_id']) and $_GET['block_id'] > 0)
    {
      $selectd_block = $_GET['block_id'];
      $this->data['block_id'] = $selectd_block;    
         
    }
    if (isset($_GET['cluster_id']) and $_GET['cluster_id'] > 0)
    {
      $selectd_cluster = $_GET['cluster_id'];
      $this->data['cluster_id'] = $selectd_cluster;  
         
    }

    if (isset($_GET['state_id']))
    {
      $this->data['selectd_state']  = $_GET['state_id'];
      $this->data['state_id']  = $_GET['state_id'];
      $this->data['district_id']  = $_GET['district_id'];
      $this->data['selectd_district']  = $_GET['district_id'];
      $this->data['block_id']  = $_GET['block_id'];
      $this->data['selectd_block']  = $_GET['block_id'];
      $this->data['cluster_id']  = $_GET['cluster_id'];
      $this->data['selectd_cluster']  = $_GET['cluster_id'];
      $this->data['school_list'] = $this->schools_model->search_schools($selectd_state, $selectd_district , $selectd_block, $selectd_cluster);    
                
      $this->data['total_records'] = count($this->data['school_list']);
    }
    $states = $this->states_model->get_all();
    $this->data['states'] = $states;
    
    $this->data['partial'] = 'admin/schools/index';
    $this->data['page_title'] = 'Schools';
    $this->load->view('templates/index', $this->data);
  }
	
	public function add()
	{
    $this->load->model('states_model');
    $this->data['states'] = $this->states_model->get_all_states();
     if (isset($_GET['state_id']) and $_GET['state_id'] > 0)
    {
      $selectd_state = $_GET['state_id'];
   
      $this->data['state_id'] = $selectd_state;
      
    }
    if (isset($_GET['district_id']) and $_GET['district_id'] > 0)
    {
      $selectd_district = $_GET['district_id'];
			$this->data['district_id'] = $selectd_district;
    
    }
    if (isset($_GET['block_id']) and $_GET['block_id'] > 0)
    {
      $selectd_block = $_GET['block_id'];
      $this->data['block_id'] = $selectd_block;    
         
    }
    if (isset($_GET['cluster_id']) and $_GET['cluster_id'] > 0)
    {
      $selectd_cluster = $_GET['cluster_id'];
      $this->data['cluster_id'] = $selectd_cluster;  
         
    }
    $this->data['partial'] = 'admin/schools/form';
    $this->data['action'] = 'create';
    $this->data['page_title'] = 'Add School';
    $this->load->view('templates/index', $this->data);
  }
	
  function create() 
  {		     
    $mdata['name'] = $this->input->post('name');
    $mdata['state_id'] = $this->input->post('state_id');
    $mdata['district_id'] = $this->input->post('district_id');
    $mdata['block_id'] = $this->input->post('block_id');
    $mdata['cluster_id'] = $this->input->post('cluster_id');
    //$mdata['village_id'] = $this->input->post('village_id');
    $mdata['dise_code'] = $this->input->post('dise_code');
    $mdata['srp_drp_name'] = $this->input->post('srp_drp_name');
    $mdata['created_on'] = date('Y-m-d H:i:s');
    $mdata['created_by'] = $this->data['current_user_id'];
    
    $this->load->database();
    $school_check = $this->schools_model->school_by_dice_number($mdata['dise_code']);
    //$school_check = $this->schools_model->school_by_dice_number('dfdsfdsfdsf');
    if (isset($school_check[0]) and isset($school_check[0]->id) and $school_check[0]->id > 0)
    {
      $this->data['name'] = $mdata['name'];
      $this->data['srp_drp_name'] = $mdata['srp_drp_name'];
      $this->data['dise_code'] = $mdata['dise_code'];
      $this->data['cluster_id'] = $mdata['cluster_id'];
      $this->data['block_id'] = $mdata['block_id'];
      $this->data['district_id'] = $mdata['district_id'];
      $this->data['state_id'] = $mdata['state_id'];
      $this->load->model('states_model');
      $this->data['states'] = $this->states_model->get_all_states();
      $this->data['error'] = "DISE Code already exists";
      $this->data['partial'] = 'admin/schools/form';
      $this->data['action'] = 'create';
      $this->data['page_title'] = 'Add School';
      $this->load->view('templates/index', $this->data);
    }
    else
    {
      //$res= $this->db->insert('schools', $mdata);
      $res= $this->schools_model->insert_school_to_db($mdata);
      header('location:'.base_url()."admin/schools");
    }
  }
	
	public function edit($id)
	{
    $this->load->model('states_model');
    $this->data['states'] = $this->states_model->get_all_states();
    $this->data['school'] = $this->schools_model->getById($id);
    $this->data['partial'] = 'admin/schools/form';
    $this->data['action'] = 'update';
    $this->data['page_title'] = 'Edit School';
    $this->load->view('templates/index', $this->data);
	}
	
  function update() 
	{	
    $mdata['name'] = $this->input->post('name');
    $mdata['state_id'] = $this->input->post('state_id');
    $mdata['district_id'] = $this->input->post('district_id');
    $mdata['block_id'] = $this->input->post('block_id');
    $mdata['cluster_id'] = $this->input->post('cluster_id');
   // $mdata['village_id'] = $this->input->post('village_id');
    $mdata['dise_code'] = $this->input->post('dise_code');
    $mdata['srp_drp_name'] = $this->input->post('srp_drp_name');
    $mdata['updated_on'] = date('Y-m-d H:i:s');
    $mdata['updated_by'] = $this->data['current_user_id'];
    $res=$this->schools_model->update_info($mdata, $_POST['id']);
    if($res)
    {
      header('location:'.base_url()."admin/schools");
    }
    else
    {
      $this->load->model('states_model');
      $this->data['states'] = $this->states_model->get_all_states();
      $this->data['school'] = $this->schools_model->getById($_POST['id']);
      $this->data['partial'] = 'admin/schools/form';
      $this->data['action'] = 'update';
      $this->data['page_title'] = 'Edit School';
      $this->load->view('templates/index', $this->data);		
    }
  }
	
	public function delete($id)
	{
		$School = $this->schools_model->getById($id);
		//if ($School['image'] != "" and file_exists("./".$School['image']))
	//		unlink("./".$School['image']);
			
		$this->schools_model->delete_a_school($id);
    header('location:'.base_url()."admin/schools");
	}
  
  public function show()
  {
		$id = $this->uri->segment(4);
		$school = $this->schools_model->getById($id);
    $un_responsed_questions = $this->schools_model->get_question($id);
    
    $options = "";
    
    $this->data['school_over'] = 0;
    if (count($un_responsed_questions) == 0)
    {
      $this->data['school_over'] = 1;
    } else {
      if ($un_responsed_questions['question_type'] == "objective" or $un_responsed_questions['question_type'] == "multiple")
      {
        $options = $this->questions_model->get_options($un_responsed_questions['id']);
      }
    }
    
    $this->data['school'] = $school;
    $this->data['cur_question'] = $un_responsed_questions;
    $this->data['options'] = $options;
    $this->data['partial'] = 'admin/schools/show';
    $this->data['page_title'] = $school['title'];
    $this->load->view('templates/school', $this->data);	
  }
  
	public function changestatus()
	{
		$id = $this->uri->segment(4);
		$this->data['action'] = 'update';
		$school = $this->schools_model->getById($id);
		extract ($school);
		if( $status==1){
      $new_status=0;
		}
		else
		{
			$new_status=1;
		}
		$mdata['status'] = $new_status;
    $mdata['updated_on'] = date('Y-m-d H:i:s');
    $mdata['updated_by'] = $this->data['current_user_id'];
		$res=$this->schools_model->update_info($mdata, $id);
		if($res)
    {
      $this->schools_model->reset_school_rating(0,$district_id);
      $school_averages = $this->schools_model->get_school_district_avg(0,$district_id);
      foreach($school_averages as $school_average)
      {        
        $district_id = $school_average->district_id;
        $average_student = $school_average->AVG_VAL;
        
        $this->schools_model->set_high_enroll_school($district_id, $average_student);
        
      }
      $this->schools_model->set_high_enroll_school($district_id, $average_student);
      header('location:'.base_url()."admin/schools".$this->index());
    }
  }
  
  public function changestatusmultiple()
	{
     $selectd_district = 0;
      $selectd_state = 0;
      $selectd_block = 0;
      $selectd_cluster = 0;
      if (isset($_POST['state_id']) and $_POST['state_id'] > 0)
      {
        $selectd_state = $_POST['state_id'];
     
        $this->data['state_id'] = $selectd_state;
        
      }
      if (isset($_POST['district_id']) and $_POST['district_id'] > 0)
      {
        $selectd_district = $_POST['district_id'];
        $this->data['district_id'] = $selectd_district;
      
      }
      if (isset($_POST['block_id']) and $_POST['block_id'] > 0)
      {
        $selectd_block = $_POST['block_id'];
        $this->data['block_id'] = $selectd_block;    
           
      }
      if (isset($_POST['cluster_id']) and $_POST['cluster_id'] > 0)
      {
        $selectd_cluster = $_POST['cluster_id'];
        $this->data['cluster_id'] = $selectd_cluster;  
           
      }
      
      if(isset($_POST['disable']))
      {
        $id = $this->uri->segment(4);
        $this->data['action'] = 'update';
        foreach($_POST['school_id'] as $a_key)
          {	
              $school = $this->schools_model->getById($a_key);
              extract ($school);
              $mdata['status'] = 0;
              $this->schools_model->update_info($mdata, $id);
          }
      }
      if(isset($_POST['enable']))
      {
        $id = $this->uri->segment(4);
        $this->data['action'] = 'update';
        foreach($_POST['school_id'] as $a_key)
          {	
              $school = $this->schools_model->getById($a_key);
              extract ($school);
              $mdata['status'] = 1;
              $this->schools_model->update_info($mdata, $id);
          }
      }

      redirect(base_url().'admin/schools?state_id='.$selectd_state.'&district_id='.$selectd_district.'&block_id='.$selectd_block.'&cluster_id='.$selectd_cluster);
  }
  
  function get_list()
  {
    $village = $_GET['village'];
    $schools = $this->schools_model->get_village_schools($village,"");
    
    if(count($schools) > 0){
      foreach ($schools as $row){
       $row1['value'] = htmlentities(stripslashes($row->name));
       $row1['id'] = $row->id;
       $row_set[] = $row1;
      }
      echo json_encode($row_set); //format the array into json data
    }    
  }
  
  function downloadExcel($name) {
      $myFile = "./uploads/".$name.".xls";
      header("Content-Length: " . filesize($myFile));
      header('Content-Type: application/vnd.ms-excel');
      header('Content-Disposition: attachment; filename='.$name.'.xls');

      readfile($myFile);
  }
  
  
  function set_school_average()
  {
      $this->schools_model->reset_school_rating();
      $school_averages = $this->schools_model->get_school_district_avg();
      
      foreach($school_averages as $school_average)
      {        
        $district_id = $school_average->district_id;
        $average_student = $school_average->AVG_VAL;
        
        $this->schools_model->set_high_enroll_school($district_id, $average_student);
        
      }
      $this->session->set_flashdata('success', "High Enroll Schools successfully marked");
      redirect('admin/schools', 'refresh');
  }
  
  
  function school_upload()
  {		
    $states = $this->states_model->get_all();
    $this->data['states'] = $states;
    
    $this->data['partial'] = 'admin/schools/school_upload';
    $this->data['page_title'] = 'Schools';
    $this->load->view('templates/index', $this->data);
  }
		
  function upload_school_list()
  {
    $this->load->model('schools_model');
    $state_id = $_POST['state'];
    $new_state = 0;
    $student_count = 0;
    $teacher_logins = array();
    $fp = fopen($_FILES['fileToUpload']['tmp_name'],'r') or die("can't open file");
    $count = 0;
    //$all_disecodes = array();

    $check_state = $this->states_model->get_by_id($state_id);
    
    if ($check_state[0]->dise_code == "")
    {      
      $this->session->set_flashdata('alert', "Kindly update the dise code of the STATE before uploading their schools.");
      redirect('admin/schools/school_upload', 'refresh');
      exit;
    }
    
    while($csv_line = fgetcsv($fp,1024))
    {
        $teacher_login = array();
        $count++;
        $error = 0;
        //print_r($csv_line);
        if($count == 1)
        {
					//S.No	State	District	Block	Cluster	School Name	Dise Code	Teacher Name	Contact Number	Date
					$sno = trim($csv_line[0]);
          $state_name = trim($csv_line[1]);
          $district_name = trim($csv_line[2]);
          $block_name = trim($csv_line[3]);
          $cluster_name = trim($csv_line[4]);
          $school_name = trim($csv_line[5]);
          $dise_code = trim($csv_line[6]);
          $teacher_name = trim($csv_line[7]);
          $contact_number = trim($csv_line[8]);
          $activity_date = trim($csv_line[9]);
          
          if ($state_name != "State")
            $error = 1;
          if ($district_name != "District")
            $error = 2;
          if ($block_name != "Block")
            $error = 3;
          if ($cluster_name != "Cluster")
            $error = 4;
          if ($dise_code != "Dise Code")
            $error = 5;
          if ($school_name != "School Name")
            $error = 6;
          if ($teacher_name != "Teacher Name")
            $error = 7;
          if ($contact_number != "Contact Number")
            $error = 8;
          if ($activity_date != "Date")
            $error = 9;
					
					if ($error != 0)
          {
            $this->session->set_flashdata('alert', "The template is not correct or you had made some changes in the first (header) row of the template.");
            redirect('admin/schools/school_upload', 'refresh');
          }
          continue;
        }//keep this if condition if you want to remove the first row
        for($i = 0, $j = count($csv_line); $i < $j; $i++)
        {          
          $sno = trim($csv_line[0]);
          $state_name = trim($csv_line[1]);
          $district_name = trim($csv_line[2]);
          $block_name = trim($csv_line[3]);
          $cluster_name = trim($csv_line[4]);
          $school_name = trim($csv_line[5]);
          $dise_code = trim($csv_line[6]);
          $teacher_name = trim($csv_line[7]);
          $contact_number = trim($csv_line[8]);
          $activity_date = trim($csv_line[9]);
        }
        
        if (strlen($dise_code) == 10)
        {
          $dise_code = str_pad($dise_code,11,'0',0);
        }
        if (substr($dise_code,0,2) == $check_state[0]->dise_code and strlen($dise_code) == 11)
        {
          //array_push($all_disecodes,$dise_code);

          $check_school = $this->schools_model->getByDisecode($dise_code);
          
          $school_id = 0;
					$state_id = 0;
					$district_id = 0;
					$block_id = 0;
					$cluster_id = 0;
          if(count($check_school) > 0)
          {
            $school_id = $check_school['id'];
            $state_id = $check_school['state_id'];
            $district_id = $check_school['district_id'];
            $block_id = $check_school['block_id'];
            $cluster_id = $check_school['cluster_id'];
          }
          
          $contact_length = strlen($contact_number);
          if($contact_length > 10){
						$contact_number = substr($contact_number, -10);
					}
          
          if(strlen($contact_number) == 10 && preg_match('/^[1-9][0-9]*$/', $contact_number)){
          
						$cond = array('dise_code'=>$dise_code, 'contact_number'=>$contact_number);
						$check_teacher = $this->schools_model->get_school_teachers($cond);
          
						if(count($check_teacher) == 0)
						{
							$data = array();
							
							$chk_registration = $this->schools_model->check_teacher_registraion(array('phone_number'=>$contact_number));
							
							$data['map_flag'] = (count($chk_registration) > 0 ? 1 : 0);
							$data['school_id'] = $school_id;
							$data['cluster_id'] = $cluster_id;
							$data['block_id'] = $block_id;
							$data['district_id'] = $district_id;
							$data['state_id'] = $state_id;
							
							$data['state_name'] = $state_name;
							$data['district_name'] = $district_name;
							$data['block_name'] = $block_name;
							$data['cluster_name'] = $cluster_name;
							$data['school_name'] = $school_name;
							$data['dise_code'] = $dise_code;
							$data['teacher_name'] = $teacher_name;
							$data['contact_number'] = $contact_number;
							$data['activity_date'] = ($activity_date != '' ? date('Y-m-d', strtotime($activity_date)) : date('Y-m-d'));
							
							$this->schools_model->add_school_teachers($data);
							
							
						}
						else{
								$mdata = array();
								$school_teacher_id = $check_teacher[0]->id;
								$mdata['report_count'] = $check_teacher[0]->report_count+1;
								$mdata['teacher_name'] = $teacher_name;
								//$mdata['contact_number'] = $contact_number;
								$this->schools_model->update_school_teachers($mdata, $school_teacher_id);
						}
					}
        }
        $i++;
    }
    fclose($fp) or die("can't close file");
    $data['success']="success";
    $this->session->set_flashdata('success', "File uploaded and all the schools added successfully");
    redirect('admin/schools/school_upload', 'refresh');
  }
  
  
  function leave_upload()
  {		
    $states = $this->states_model->get_all();
    $this->data['states'] = $states;
    
    $this->data['partial'] = 'admin/schools/leave_upload';
    $this->data['page_title'] = 'Leaves';
    $this->load->view('templates/index', $this->data);
  }
		
  function upload_leave_list()
  {
    $this->load->model('Users_model');
    $this->load->model('Leaves_model');

    $fp = fopen($_FILES['fileToUpload']['tmp_name'],'r') or die("can't open file");
    $count = 0;

    while($csv_line = fgetcsv($fp,1024))
    {
        $count++;
        $error = 0;
        //print_r($csv_line);
        if($count == 1)
        {
					//S.No	State	District	Block	Cluster	School Name	Dise Code	Teacher Name	Contact Number	Date
          $login_id = trim($csv_line[0]);
          $leave_type = trim($csv_line[1]);
          $leave_count = trim($csv_line[2]);
          
          if ($login_id != "Login Id")
            $error = 1;
          if ($leave_type != "Leave Type")
            $error = 2;
          if ($leave_count != "Leave Count")
            $error = 3;
          
					if ($error != 0)
          {
            $this->session->set_flashdata('alert', "The template is not correct or you had made some changes in the first (header) row of the template111.");
            redirect('admin/schools/leave_upload', 'refresh');
          }
          continue;
        }//keep this if condition if you want to remove the first row
        for($i = 0, $j = count($csv_line); $i < $j; $i++)
        {          
          $login_id = trim($csv_line[0]);
          $leave_type = trim($csv_line[1]);
          $leave_count = trim($csv_line[2]);
        }
        
          //array_push($all_disecodes,$dise_code);
					$current_date = date('Y-06-01');
          $sparkData = $this->Users_model->getById('', $login_id);
          
					if(!empty($sparkData))
					{
						$spark_id = $sparkData['id'];
          	
						$creditDeails = $this->Leaves_model->get_leave_credit_details($spark_id, $leave_type, '', '', $current_date);
						//print_r($creditDeails);
						if(empty($creditDeails))
						{
							$sparkData = $this->Users_model->getById($spark_id);
							
							$spark_group_leave = $this->Leaves_model->get_spark_group_leave($sparkData['user_group']);
							
							$data['spark_id'] = $spark_id;
							$data['leave_type'] = $leave_type;
							$data['earned_date'] = $current_date;
							$data['created_at'] = $current_date;
							$data['leave_count'] = $leave_count;
							$this->Leaves_model->insert_leave_credit_details($data);
							
							$creditData = $this->Leaves_model->get_leave_credits($spark_id, $leave_type);
							
							if(empty($creditData)){
								$cdata['spark_id'] = $spark_id;
								$cdata['leave_type'] = $leave_type;
								$cdata['leave_earned'] = $leave_count;  
								$this->Leaves_model->insert_leave_credit($cdata);  
							}
							else{
								$mdata['leave_earned'] = $creditData[0]->leave_earned+$leave_count;  
								$this->Leaves_model->update_leave_credit($mdata, $creditData[0]->id);    
							}
						}
					}
        $i++;
    }
    fclose($fp) or die("can't close file");
    $data['success']="success";
    $this->session->set_flashdata('success', "File uploaded and all the leaves added successfully");
    redirect('admin/schools/leave_upload', 'refresh');
  }
  
    
}
	
	
	
	
	
	
	
