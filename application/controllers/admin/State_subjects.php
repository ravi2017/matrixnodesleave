<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class State_subjects extends CI_Controller {

	function __construct()	
	{
		parent::__construct();
		$this->load->helper('url');
    $this->load->helper('application');
		$this->data['current_page'] = 'Observations';
		$this->load->library('form_validation');
		$this->load->library('common_functions');
    $this->load->model('observations_model');
    $this->load->model('users_model');
		$this->load->model('states_model');
		$this->load->model('classes_model');
		$this->load->helper('date');
    
    if($this->session->userdata('admin_logged_in'))
		{
		  $session_data = $this->session->userdata('admin_logged_in');
      $this->data['current_user_id'] = $session_data['id'];
      $this->data['current_username'] = $session_data['username'];
      $this->data['current_name'] = $session_data['name'];
      $this->data['current_role'] = $session_data['role'];
      $this->data['user_state_id'] = $session_data['state_id'];
      $this->data['user_district_id'] = $session_data['district_id'];
    }
    else
    {
      //If no session, redirect to login page
      redirect('admin/login', 'refresh');
    }
	}
  

  public function index()
	{
    $mapping_list = $this->observations_model->get_all_mapping();
    
    $this->data['mapping_list'] = $mapping_list;
    
    $this->data['partial'] = 'admin/state_subjects/index';
    $this->data['page_title'] = 'observations';
    $this->load->view('templates/index', $this->data);
	}

  
  public function add()
	{
      $this->load->model('subjects_model');
      $this->data['subjects'] = $this->subjects_model->get_all();
      
      $this->load->model('states_model');
      $this->data['states'] = $this->states_model->get_all_states();
      
      $this->load->model('classes_model');
      $this->data['classes'] = $this->classes_model->get_all();
      
      $this->data['partial'] = 'admin/state_subjects/mapping_form';
      $this->data['action'] = 'add_mapping';
      $this->data['logged_in_user'] = $this->data['current_user_id'];
      $this->data['page_title'] = 'Add State, Class & Subject Mapping';
      
      $this->load->view('templates/index', $this->data);
	}
  
  public function add_mapping()
	{
      //print_r($_POST);exit;
      $state_ids = $this->input->post('state_id');
      $class_ids = $this->input->post('class_id');
      $subject_ids = $this->input->post('subject_id');
      
      $success = false;
      if(empty($state_ids) || empty($class_ids) || empty($subject_ids)){
        $this->session->set_flashdata('error', 'Please select all required fields.');
        header('location:'.base_url()."admin/state_subjects/add");
      }
      foreach($state_ids as $stateid){
        foreach($class_ids as $classid){
          foreach($subject_ids as $subjectid){
            $mdata['subject_id'] = $subjectid;
            $mdata['state_id'] = $stateid;
            $mdata['class_id'] = $classid;
            $mdata['created_on'] = date('Y-m-d H:i:s');
            $mdata['created_by'] = $this->data['current_user_id'];
            $mappingData = $this->observations_model->check_mapping($stateid, $subjectid, $classid);
            if(empty($mappingData))
              $observation_id = $this->observations_model->add_mapping($mdata);
          }
        }
      }
      $this->session->set_flashdata('success', 'Subject added successfully.');
      header('location:'.base_url()."admin/state_subjects/");
	}

  public function changemapstatus()
  {
    $id = $this->uri->segment(4);
    $this->data['action'] = 'update';
    $banner = $this->observations_model->getMapById($id);
    extract ($banner);
    if( $status==1){
      $new_status=0;
    }
    else
    {
      $new_status="1";
    }
    $mdata['status'] = $new_status;
    $mdata['updated_on'] = date('Y-m-d H:i:s');
    $mdata['updated_by'] = $this->data['current_user_id'];
    $res=$this->observations_model->update_map_info($mdata, $id);
    header('location:'.base_url()."admin/state_subjects");
  }
  
  public function deletemapping($id)
	{
      $this->observations_model->delete_mapping($id);
      header('location:'.base_url()."admin/state_subjects");
	}

}
