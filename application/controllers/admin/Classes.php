<?php

class Classes extends CI_Controller  {
  function __construct() {
    parent::__construct();
	
    $this->load->helper(array('form', 'url'));
    $this->load->model('subjects_model');
    $this->load->model('classes_model');
    $this->load->library('form_validation');
  
    $this->data['current_page'] = 'classes';
    $this->load->library('common_functions'); 
    
		if($this->session->userdata('admin_logged_in'))
		{
		  $session_data = $this->session->userdata('admin_logged_in');
      $this->data['user_id'] = $session_data['id'];
      $this->data['current_user_id'] = $session_data['id'];
      $this->data['current_username'] = $session_data['username'];
      $this->data['current_name'] = $session_data['name'];
      $this->data['current_role'] = $session_data['role'];
      $this->data['user_state_id'] = $session_data['state_id'];
      $this->data['user_district_id'] = $session_data['district_id'];
    }
		else
		{
      redirect('admin/login', 'refresh');
		}		        
  }

  function index() 
  {
    $this->data['classes_list'] = $this->classes_model->get_all();
    $this->data['partial'] = 'admin/classes/index';
    $this->data['page_title'] = 'Classes';
    $this->load->view('templates/index', $this->data);
    //$this->load->view('admin/admins/index', array('error' => ' ' ));
  }

  public function save(){
    $data = array('name'=> $_POST['name']);
    $this->classes_model->insert_classes_to_db($data);
    redirect('admin/classes', 'refresh');
  }

  public function add()
  {	
    //$this->load->model('subjects_model');
    //$this->data['subjects'] = $this->subjects_model->get_all();
    $this->data['partial'] = 'admin/classes/form';
    $this->data['action'] = 'create';
    $this->data['page_title'] = 'Add Holiday';
    $this->load->view('templates/index', $this->data);		
  }

  function create() 
  {	
    $this->load->model('classes_model');
    
    $name = $this->input->post('name');
    $cdata = $this->classes_model->getByName($name);
    
    if(empty($cdata)){
      $mdata['name'] = $name;
      $mdata['created_on'] = date('Y-m-d H:i:s');
      $mdata['created_by'] = $this->data['current_user_id'];
      //$mdata['subject_id'] = implode(',', $this->input->post('subjects'));
      $class_id = $this->classes_model->add($mdata);
      header('location:'.base_url()."admin/classes/".$this->index());
    }
    else{
      $this->session->set_flashdata('alert', 'Class already exists.');
      header('location:'.base_url()."admin/classes/add");
    }
  }

  public function edit()
  {
    //$this->load->model('subjects_model');
    //$this->data['subjects'] = $this->subjects_model->get_all();
    $id = $this->uri->segment(4);
		
    $classes = $this->classes_model->getById($id);
    //$subject_ids = $classes['subject_id'];
    
    $this->data['classes'] = $classes;
    //$this->data['subject_ids'] = explode(',', $subject_ids);
		$this->data['partial'] = 'admin/classes/form';
		$this->data['action'] = 'update';
		$this->data['page_title'] = 'Edit Class';
		$this->load->view('templates/index', $this->data);		
  }

  function update() 
  {	
		$this->load->model('classes_model');
    
    $name = $this->input->post('name');
    $cdata = $this->classes_model->getByOtherName($_POST['id'], $name);
    
    if(empty($cdata)){
      $mdata['name'] = $name;
      $mdata['updated_on'] = date('Y-m-d H:i:s');
      $mdata['updated_by'] = $this->data['current_user_id'];
      //$mdata['subject_id'] = implode(',', $this->input->post('subjects'));
      $res = $this->classes_model->update_class($_POST['id'], $mdata);
      header('location:'.base_url()."admin/classes".$this->index());
    }
    else{
      $this->session->set_flashdata('alert', 'Class already exists.');
      header('location:'.base_url()."admin/classes/edit/".$_POST['id']);
    }
  }
  
  public function delete($id)
  {
    $this->classes_model->delete_a_class($id);
    header('location:'.base_url()."admin/classes".$this->index());
  }
  
  public function changestatus()
  {
    $id = $this->uri->segment(4);
    $this->data['action'] = 'update';
    $banner = $this->classes_model->getById($id);
    extract ($banner);
    if( $status==1){
      $new_status=0;
    }
    else
    {
      $new_status="1";
    }
    $mdata['status'] = $new_status;
    $mdata['updated_on'] = date('Y-m-d H:i:s');
    $mdata['updated_by'] = $this->data['current_user_id'];
    $res=$this->classes_model->update_class($id, $mdata);
    header('location:'.base_url()."admin/classes".$this->index());
  }

}
