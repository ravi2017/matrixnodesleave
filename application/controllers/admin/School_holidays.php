<?php

class School_holidays extends CI_Controller  {
  function __construct() {
    parent::__construct();
	
    $this->load->helper(array('form', 'url'));
    $this->load->model('subjects_model');
    $this->load->model('classes_model');
    $this->load->model('school_holidays_model');
    $this->load->library('form_validation');
  
    $this->data['current_page'] = 'holidays';
    $this->load->library('common_functions'); 

    
		if($this->session->userdata('admin_logged_in'))
		{
		  $session_data = $this->session->userdata('admin_logged_in');
      $this->data['id'] = $session_data['id'];
      $this->data['current_username'] = $session_data['username'];
      $this->data['current_name'] = $session_data['name'];
      $this->data['current_role'] = $session_data['role'];
      $this->data['user_state_id'] = $session_data['state_id'];
      $this->data['user_district_id'] = $session_data['district_id'];
      $this->data['current_group'] = $session_data['user_group'];
    }
    if($this->session->userdata('logged_in'))
		{
		  $session_data = $this->session->userdata('logged_in');
      $this->data['id'] = $session_data['id'];
      $this->data['current_username'] = $session_data['username'];
      $this->data['current_name'] = $session_data['name'];
      $this->data['current_role'] = $session_data['role'];
      $this->data['user_state_id'] = $session_data['state_id'];
      $this->data['user_district_id'] = $session_data['district_id'];
      $this->data['current_group'] = $session_data['user_group'];
    }
		else
		{
      redirect('admin/login', 'refresh');
		}		        
  }

  function index() 
  {
		$inpmonth = date('m');
		$inpyear = date('Y');
		$session_start_year = date('Y');
		if($inpmonth < SESSION_START_MONTH)
		{
			$session_start_year = $inpyear-1;
		}
		$start_date = date("Y-m-d",strtotime($session_start_year."-".SESSION_START_MONTH."-01"));
		$end_date = date("Y-m-d");
		
    $this->data['holidays_list'] = $this->school_holidays_model->get_all_holidays_admin($start_date);
    $this->data['partial'] = 'admin/school_holidays/index';
    $this->data['page_title'] = 'School Holidays';
    $this->load->view('templates/index', $this->data);
    //$this->load->view('admin/admins/index', array('error' => ' ' ));
  }

  public function add()
  {	
    $this->load->model('states_model');
    $this->data['states'] = $this->states_model->get_all_states();
    $this->data['partial'] = 'admin/school_holidays/form';
    $this->data['action'] = 'create';
    $this->data['page_title'] = 'Add Holiday';
    $this->load->view('templates/index', $this->data);		
  }

  function create() 
  {		
    //print_r($_POST); exit;
    
    if(!empty($_POST['states']) && $_POST['start_date'] != '' && $_POST['end_date'] != ''){
			
			$start_date = date('Y-m-d', strtotime($_POST['start_date']));
			$end_date = date('Y-m-d', strtotime($_POST['end_date']));
			
      foreach($_POST['states'] as $state_id)
      {
        $data = array('name'=> $_POST['name'],'holiday_type'=> $_POST['holiday_type'], 'start_date'=> $start_date, 'end_date'=> $end_date, 'state_id' => $state_id, 'total_days' => $_POST['total_days'], 'created_at'=>date('Y-m-d H:i:s'));
        $school_holiday_id = $this->school_holidays_model->add($data);
        
        $all_dates = $this->get_dates($start_date, $end_date);
        foreach($all_dates as $holiday_date)
        {
					$hdata = array('school_holiday_id'=> $school_holiday_id, 'state_id'=> $state_id, 'holiday_date'=> $holiday_date);
					$this->school_holidays_model->add_holiday_dates($hdata);
				}
      }
		}
    header('location:'.base_url()."admin/school_holidays/".$this->index());
  }

  public function edit()
  {
    $this->load->model('states_model');
    $this->data['states'] = $this->states_model->get_all_states();
		$id = $this->uri->segment(4);
		$holidays = $this->school_holidays_model->getById($id);
    $state_ids = array();
    foreach ($holidays as $holiday) {
      array_push($state_ids, $holiday['state_id']);
    }
    $this->data['holidays'] = $holidays;
    $this->data['state_ids'] = $state_ids;
		$this->data['partial'] = 'admin/school_holidays/form';
		$this->data['action'] = 'update';
		$this->data['page_title'] = 'Edit Holiday';
		$this->load->view('templates/index', $this->data);		
  }

  function update() 
  {	
		$this->load->library('form_validation');
    $this->form_validation->set_rules('name', 'Name', 'required');
    $this->form_validation->set_rules('holiday_date', 'Holiday Date', 'required');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		if ($this->form_validation->run() == FALSE)
    {
      $id = $this->uri->segment(4);
      $this->data['admins'] = $this->school_holidays_model->getById($id);
      $this->data['partial'] = 'admin/school_holidays/form';
      $this->data['action'] = 'update';
      $this->data['page_title'] = 'Edit Holiday';
      $this->load->view('templates/index', $this->data);	
    }
    else
    {
      $data = array('name'=> $_POST['name'], 'holiday_date'=> implode("-",array_reverse(explode("-",$_POST['holiday_date']))));
      $this->school_holidays_model->update_holiday($_POST['id'], $data);
      $this->school_holidays_model->delete_state_holidays($_POST['id']);
      foreach($_POST['states'] as $state)
      {
        $mdata['state_id'] = $state;
        $mdata['holiday_id'] = $_POST['id'];
        $res= $this->db->insert('states_holidays', $mdata);
      }
      if($res)
      {
        header('location:'.base_url()."admin/school_holidays".$this->index());
      }
    }
  }
  
  public function delete($id)
  {
    //$admins = $this->school_holidays_model->getById($id);
    $this->school_holidays_model->delete_a_holidays($id);
    $this->school_holidays_model->delete_a_holiday_dates($id);
    header('location:'.base_url()."admin/school_holidays".$this->index());
  }
  
  public function changestatus()
  {
    $id = $this->uri->segment(4);
    $this->data['action'] = 'update';
    $banner = $this->admins_model->getById($id);
    //print_r($this->data['newsletter']);
    extract ($banner);
    if( $status==1){
      $new_status=0;
    }
    else
    {
      $new_status="1";
    }
    $mdata['status'] = $new_status;
    $res=$this->admins_model->update_info($mdata, $id);
    if($res)
    {
      header('location:'.base_url()."admin/school_holidays".$this->index());
    }
  }

 
  function work_days_between_two_dates(\DateTime $begin, \DateTime $end) {
      $leavedays = [];
      $workdays = [];
      $all_days = new DatePeriod($begin, new DateInterval('P1D'), $end->modify('+1 day'));
      //echo"<pre>";
      //print_r($all_days);
      foreach ($all_days as $day) {
          $dow = (int)$day->format('w');
          $dom = (int)$day->format('j');
          if (1 <= $dow && $dow <= 6) { // Mon - Fri
              $workdays[] = $day;
          } 
          /*else if (6 == $dow && 0 == $dom % 2) { // Even Saturday
              $workdays[] = $day;
          }
          */
          else{
            //$leavedays[] = $day;
            $workdays[] = $day;
          }
      }
      return $workdays;
  }
  
  function get_dates($start_date, $end_date)
  {
    //$begin = new \DateTime('2019-01-01');
    //$end   = new \DateTime('2019-12-31');
    $begin = new \DateTime($start_date);
    $end   = new \DateTime($end_date);
    $days  = $this->work_days_between_two_dates($begin, $end);
    $dates = array();
    foreach ($days as $day) {
        $dates[] = $day->format('Y-m-d').PHP_EOL;
    }
    return $dates;
  }
}
