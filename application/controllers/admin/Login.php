<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

  function __construct()
  {
   parent::__construct();
   
  }

  function index()
  {
    $this->load->helper(array('form'));
    $this->data['partial'] = 'admin/login/index';
    $this->load->view('templates/login_view', $this->data);
  }

}

?>
