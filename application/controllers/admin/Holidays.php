<?php

class Holidays extends CI_Controller  {
  function __construct() {
    parent::__construct();
	
    $this->load->helper(array('form', 'url'));
    $this->load->model('subjects_model');
    $this->load->model('classes_model');
    $this->load->model('holidays_model');
    $this->load->library('form_validation');
  
    $this->data['current_page'] = 'holidays';
    $this->load->library('common_functions'); 
    $this->load->library('ckeditor');
    $this->load->library('ckfinder');
    $this->ckeditor->basePath = base_url().'assets/admin/ckeditor/';
    $this->ckeditor->config['toolbar'] ='Full'; 
    $this->ckeditor->config['language'] = 'en';
    $this->ckeditor->config['width'] = '100%';
    $this->ckeditor->config['height'] = '300px';   
    
		if($this->session->userdata('logged_in'))
		{
		  $session_data = $this->session->userdata('logged_in');
      $this->data['id'] = $session_data['id'];
      $this->data['current_username'] = $session_data['username'];
      $this->data['current_name'] = $session_data['name'];
      $this->data['current_role'] = $session_data['role'];
      $this->data['user_state_id'] = $session_data['state_id'];
      $this->data['user_district_id'] = $session_data['district_id'];
      $this->data['current_group'] = $session_data['user_group'];
      
      $this->data['USER_GROUPS'] = unserialize(USER_GROUP);
    }
		else
		{
      redirect('admin/login', 'refresh');
		}		        
  }

  function index() 
  {
		$inpmonth = date('m');
		$inpyear = date('Y');
		$session_start_year = date('Y');
		if($inpmonth < SESSION_START_MONTH)
		{
			$session_start_year = $inpyear-1;
		}
		$start_date = date("Y-m-d",strtotime($session_start_year."-".SESSION_START_MONTH."-01"));
		$end_date = date("Y-m-d");
		
    $this->data['holidays_list'] = $this->holidays_model->get_all_holidays_admin($start_date);
    $this->data['partial'] = 'admin/holidays/index';
    $this->data['page_title'] = 'Holidays';
    $this->load->view('templates/index', $this->data);
    //$this->load->view('admin/admins/index', array('error' => ' ' ));
  }

  public function master() 
  {
    $this->data['holidays_list'] = $this->holidays_model->get_all_holidays();
    $this->data['partial'] = 'admin/holidays/master';
    $this->data['action'] = 'save';
    $this->data['page_title'] = 'Holidays';
    $this->load->view('templates/index', $this->data);
    //$this->load->view('admin/admins/index', array('error' => ' ' ));
  }

  public function save(){
    $data = array('name'=> $_POST['name'], 'holiday_date'=> $_POST['date']);
    $this->holidays_model->insert_holidays_to_db($data);
    redirect('admin/holidays', 'refresh');
  }

  public function add()
  {	
    $this->load->model('states_model');
    $this->data['states'] = $this->states_model->get_all_states();
    $this->data['partial'] = 'admin/holidays/form';
    $this->data['action'] = 'create';
    $this->data['page_title'] = 'Add Holiday';
    $this->load->view('templates/index', $this->data);		
  }

  function create() 
  {		
    //print_r($_POST); exit;
    
    if (isset($_POST['is_national']) && $_POST['is_national'] == 1)
    {
      $data = array('name'=> $_POST['name'],'holiday_type'=> $_POST['holiday_type'], 'activity_date'=> $_POST['holiday_date'], 'national'=> 1, 'user_id'=> 0);
      $this->holidays_model->add($data);
    }
    else
    {
			if($_POST['name'] != '' && $_POST['holiday_date'] != ''){
				foreach($_POST['states'] as $state_id)
				{
					$data = array('name'=> $_POST['name'],'holiday_type'=> $_POST['holiday_type'], 'activity_date'=> $_POST['holiday_date'], 'stateid' => $state_id, 'national'=> 0, 'user_id'=> 0);
					$this->holidays_model->add($data);
				}
      }
    }
    header('location:'.base_url()."admin/holidays/".$this->index());
  }

  public function edit()
  {
    $this->load->model('states_model');
    $this->data['states'] = $this->states_model->get_all_states();
		$id = $this->uri->segment(4);
		$holidays = $this->holidays_model->getById($id);
    $state_ids = array();
    foreach ($holidays as $holiday) {
      array_push($state_ids, $holiday['state_id']);
    }
    $this->data['holidays'] = $holidays;
    $this->data['state_ids'] = $state_ids;
		$this->data['partial'] = 'admin/holidays/form';
		$this->data['action'] = 'update';
		$this->data['page_title'] = 'Edit Holiday';
		$this->load->view('templates/index', $this->data);		
  }

  function update() 
  {	
		$this->load->library('form_validation');
    $this->form_validation->set_rules('name', 'Name', 'required');
    $this->form_validation->set_rules('holiday_date', 'Holiday Date', 'required');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		if ($this->form_validation->run() == FALSE)
    {
      $id = $this->uri->segment(4);
      $this->data['admins'] = $this->holidays_model->getById($id);
      $this->data['partial'] = 'admin/holidays/form';
      $this->data['action'] = 'update';
      $this->data['page_title'] = 'Edit Holiday';
      $this->load->view('templates/index', $this->data);	
    }
    else
    {
      $data = array('name'=> $_POST['name'], 'holiday_date'=> implode("-",array_reverse(explode("-",$_POST['holiday_date']))));
      $this->holidays_model->update_holiday($_POST['id'], $data);
      $this->holidays_model->delete_state_holidays($_POST['id']);
      foreach($_POST['states'] as $state)
      {
        $mdata['state_id'] = $state;
        $mdata['holiday_id'] = $_POST['id'];
        $res= $this->db->insert('states_holidays', $mdata);
      }
      if($res)
      {
        header('location:'.base_url()."admin/holidays".$this->index());
      }
    }
  }
  
  public function delete($id)
  {
    //$admins = $this->holidays_model->getById($id);
    $this->holidays_model->delete_a_holidays($id);
    header('location:'.base_url()."admin/holidays".$this->index());
  }
  
  public function changestatus()
  {
    $id = $this->uri->segment(4);
    $this->data['action'] = 'update';
    $banner = $this->admins_model->getById($id);
    //print_r($this->data['newsletter']);
    extract ($banner);
    if( $status==1){
      $new_status=0;
    }
    else
    {
      $new_status="1";
    }
    $mdata['status'] = $new_status;
    $res=$this->admins_model->update_info($mdata, $id);
    if($res)
    {
      header('location:'.base_url()."admin/admins".$this->index());
    }
  }

  function get_list()
  {
    $subject_id = $_GET['subject_id'];
    $class_id = $_GET['class_id'];
		$base_line=" ";
		$end_line =" ";
		if(isset($_GET['base_line']) and $_GET['base_line'] !=''){
			$base_line=$_GET['base_line'];
		}
		if(isset($_GET['end_line']) and $_GET['end_line'] !=''){
			$end_line=$_GET['end_line'];
		}
    $holidays = $this->holidays_model->get_all_holidays($class_id, $subject_id,$progress_chart='',$base_line,$end_line);
    if(count($holidays) > 0){
      foreach ($holidays as $row){
        $value = "";
        $row1['value'] = $value.ucwords(htmlentities(stripslashes($row->name)));
        $row1['id'] = $row->id;
        $row_set[] = $row1;
      }
      echo json_encode($row_set); //format the array into json data
    }
  } 
  
  function work_days_between_two_dates(\DateTime $begin, \DateTime $end) {
      $leavedays = [];
      $all_days = new DatePeriod($begin, new DateInterval('P1D'), $end->modify('+1 day'));
      //echo"<pre>";
      //print_r($all_days);
      foreach ($all_days as $day) {
          $dow = (int)$day->format('w');
          $dom = (int)$day->format('j');
          if (1 <= $dow && $dow <= 6) { // Mon - Fri
              $workdays[] = $day;
          } 
          /*else if (6 == $dow && 0 == $dom % 2) { // Even Saturday
              $workdays[] = $day;
          }
          */
          else{
            $leavedays[] = $day;
          }
      }
      return $leavedays;
  }
  
  function get_sat_sun_dates()
  {
    $begin = new \DateTime('2019-01-01');
    $end   = new \DateTime('2019-12-31');
    $days  = $this->work_days_between_two_dates($begin, $end);
    //echo "<br>Count ".count($days);
    foreach ($days as $day) {
        echo"<br>".$day->format('Y-m-d').PHP_EOL;
    }
  }
}
