<?php

class Offdays extends CI_Controller  {
  function __construct() {
    parent::__construct();
	
    $this->load->helper(array('form', 'url'));
    $this->load->model('offdays_model');
    $this->load->library('form_validation');
  
    $this->data['current_page'] = 'Offdays';
    $this->load->library('common_functions'); 
    
		if($this->session->userdata('logged_in'))
		{
		  $session_data = $this->session->userdata('logged_in');
      $this->data['id'] = $session_data['id'];
      $this->data['current_username'] = $session_data['username'];
      $this->data['current_name'] = $session_data['name'];
      $this->data['current_role'] = $session_data['role'];
      $this->data['user_state_id'] = $session_data['state_id'];
      $this->data['user_district_id'] = $session_data['district_id'];
      $this->data['current_group'] = $session_data['user_group'];
      
      $this->data['USER_GROUPS'] = unserialize(USER_GROUP);
    }
		else
		{
      redirect('admin/login', 'refresh');
		}		        
  }

  function index() 
  {
		$inpmonth = date('m');
		$inpyear = date('Y');
		$session_start_year = date('Y');
		if($inpmonth < SESSION_START_MONTH)
		{
			$session_start_year = $inpyear-1;
		}
		$start_date = date("Y-m-d",strtotime($session_start_year."-".SESSION_START_MONTH."-01"));
		$end_date = date("Y-m-d");
		
    $this->data['holidays_list'] = $this->offdays_model->get_all_offdays_admin($start_date);
    $this->data['partial'] = 'admin/offdays/index';
    $this->data['page_title'] = 'Offdays';
    $this->load->view('templates/index', $this->data);
  }

  public function add()
  {	
    $this->data['partial'] = 'admin/offdays/form';
    $this->data['action'] = 'create';
    $this->data['page_title'] = 'Add Offdays';
    $this->load->view('templates/index', $this->data);		
  }

  function create() 
  {		
    //print_r($_POST); exit;
    
			$holiday_date = $_POST['holiday_date'];
			$type = date('l', strtotime($holiday_date));
			
			$type_array = array('Saturday', 'Sunday');
			if(!in_array($type, $type_array)){
				$this->session->set_flashdata('alert', 'You can only add Saturday/Sunday date.');
				redirect('admin/offdays/add');
			} 
			$data = array('user_group'=> $_POST['user_group'], 'activity_date'=> date('Y-m-d', strtotime($holiday_date)), 'type'=>$type);
      $this->offdays_model->add($data);
      $this->session->set_flashdata('success', 'Offday added successfully.');
			header('location:'.base_url()."admin/offdays/".$this->index());
  }

  public function delete($id)
  {
    //$admins = $this->offdays_model->getById($id);
    $this->offdays_model->delete_a_offdays($id);
    header('location:'.base_url()."admin/offdays".$this->index());
  }

}
