<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Employees extends CI_Controller {

	function __construct()	
	{
		parent::__construct();
		$this->load->helper('url');
    $this->load->helper('application');
		$this->data['current_page'] = 'employees';
		$this->load->library('form_validation');
		$this->load->library('common_functions');
    $this->load->model('employees_model');
    $this->load->model('Employees_model');
    $this->load->model('users_model');
    $this->load->model('team_members_model');
		$this->load->model('states_model');
		$this->load->helper('date');
    
    //if($this->session->userdata('admin_logged_in'))
    if($this->session->userdata('logged_in'))
		{
		  $session_data = $this->session->userdata('logged_in');
      $this->data['current_user_id'] = $session_data['id'];
      $this->data['current_username'] = $session_data['username'];
      $this->data['current_name'] = $session_data['name'];
      $this->data['current_role'] = $session_data['role'];
      $this->data['user_state_id'] = $session_data['state_id'];
      $this->data['user_district_id'] = $session_data['district_id'];
      $this->data['current_group'] = $session_data['user_group'];
    }
    else
    {
      //If no session, redirect to login page
      redirect('admin/login', 'refresh');
    }
    
    $this->data['LEAVE_TYPES_SORT'] = unserialize(LEAVE_TYPES_SORT);
    $this->data['USER_GROUPS'] = unserialize(USER_GROUP);
    $this->data['EMP_BRANCH'] = unserialize(EMP_BRANCH);
    $this->data['EMP_DEPARTMENT'] = unserialize(EMP_DEPARTMENT);
    $this->data['EMP_GRADE'] = unserialize(EMP_GRADE);
    $this->data['EMP_CATEGORY'] = unserialize(EMP_CATEGORY);
    $this->data['EMP_DIVISIONS'] = unserialize(EMP_DIVISIONS);
    $this->data['PAYMENT_MODE'] = unserialize(PAYMENT_MODE);
    $this->data['EMP_ROLES'] = unserialize(EMP_ROLES);
    $this->data['EMP_GENDER'] = unserialize(EMP_GENDER);
    $this->data['BLOOD_GROUPS'] = unserialize(BLOOD_GROUP);
    $this->data['APPRAISAL_MONTHS'] = unserialize(APPRAISAL_MONTHS);
    $this->data['EMP_UNITS'] = unserialize(EMP_UNITS);
    $this->data['EMP_DESIGNATIONS'] = unserialize(EMP_DESIGNATIONS);
    $this->data['EMP_PROJECTS'] = unserialize(EMP_PROJECTS);
    $this->data['STATES_ARR'] = unserialize(STATES_ARR);
    
	}

	public function index()
	{
      $this->data['employees_list'] = $this->Employees_model->get_employees_by_role();;
      //print_r($this->data['employees_list']);exit;
      $this->data['partial'] = 'admin/employees/index';
      $this->data['page_title'] = 'employees';
      $this->load->view('templates/index', $this->data);
	}
  
  public function employeelist()
	{
      $this->data['employees_list'] = $this->Employees_model->get_employees_by_role(array('field_user', 'manager', 'state_person', 'accounts', 'reports', 'HR'));
      
      $myFile = "./uploads/reports/employeelist.xls";
      $this->load->library('parser');
      //pass retrieved data into template and return as a string
      $stringData = $this->parser->parse('admin/employees/employeelist', $this->data, true);
      //open excel and write string into excel
      $fh = fopen($myFile, 'w') or die("can't open file");
      fwrite($fh, $stringData);
      fclose($fh);
      //download excel file
      $this->common_functions->downloadExcel("employeelist");
      unlink($myFile);
	}

	public function add()
	{
			$statesData = $this->states_model->get_all_master_states();
			$this->data['statesData'] = $statesData;
			
			$sparks = $this->Employees_model->get_all_reporting_user();
			$this->data['sparks'] = $sparks;
      $this->data['partial'] = 'admin/employees/form';
      $this->data['action'] = 'create_employee';
      $this->data['logged_in_user'] = $this->data['current_user_id'];
      $this->data['page_title'] = 'Add Employee';
      $this->load->view('templates/index', $this->data);
	}
	
	public function create_employee()
	{
      //print_r($_POST);exit;
      $state_id = $this->input->post('state_id');
      $mdata['login_id'] = $this->input->post('login_id');
      $mdata['name'] = $this->input->post('name');
      $mdata['email'] = $this->input->post('email');
      $mdata['mobile'] = $this->input->post('mobile');
      $mdata['role'] = $this->input->post('role');
      $mdata['state_id'] = $this->input->post('state_id');
      $mdata['manager_id'] = ($this->input->post('manager_id') !='' ? $this->input->post('manager_id') : 0);
      $mdata['user_group'] = $this->input->post('user_group');
      $mdata['password'] = md5($this->input->post('login_id'));
      $mdata['district_id'] = 0;
      $mdata['login_status'] = 1;
      $mdata['created_on'] = date('Y-m-d H:i:s');
      $mdata['created_by'] = $this->data['current_user_id'];
      
      $user_id = $this->users_model->add($mdata);

      if($user_id > 0){
        
        $pdata['spark_id'] = $user_id;
        $pdata['personal_email'] = $this->input->post('personal_email');
        $pdata['father_name'] = $this->input->post('father_name');
        $pdata['spouse_name'] = $this->input->post('spouse_name');
        $pdata['aadhar_number'] = $this->input->post('aadhar_number');
        $pdata['aadhar_number_full'] = $this->input->post('aadhar_number');
        $pdata['gender'] = $this->input->post('gender');
        $pdata['birth_date'] = date('Y-m-d', strtotime($this->input->post('birth_date')));
        $pdata['joining_date'] = date('Y-m-d', strtotime($this->input->post('joining_date')));
        $pdata['branch'] = $this->input->post('branch');
        $pdata['department'] = $this->input->post('department');
        $pdata['grade'] = $this->input->post('grade');
        $pdata['emp_category'] = $this->input->post('emp_category');
        $pdata['division'] = $this->input->post('division');
        $pdata['attendance_cycle'] = $this->input->post('attendance_cycle');
        $pdata['emergency_number'] = $this->input->post('emergency_number');
        $pdata['blood_group'] = $this->input->post('blood_group');
        $pdata['marital_status'] = $this->input->post('marital_status');
        $pdata['designation'] = $this->input->post('designation');
        $pdata['unit_name'] = $this->input->post('unit_name');
        $pdata['project_name'] = $this->input->post('project_name');
        
        $pdata['pan_number'] = $this->input->post('pan_number');
        $pdata['pf_number'] = $this->input->post('pf_number');
        $pdata['uan_number'] = $this->input->post('uan_number');
        
        $pdata['notice_days'] = ($this->input->post('notice_days') != '' ? $this->input->post('notice_days') : 0);
        if($this->input->post('appraisal_month') != '')
        $pdata['appraisal_month'] = $this->input->post('appraisal_month');
        if($this->input->post('appraisal_year') != '')
        $pdata['appraisal_year'] = $this->input->post('appraisal_year');
        $pdata['bike'] = $this->input->post('bike');
        
        /*if(isset($_FILES) && $_FILES['emp_pic']['tmp_name'] != ''){
					print_r($_FILES);
				}*/
        
        $pdata['created_at'] = date('Y-m-d H:i:s');
				$pdata['created_by'] = $this->data['current_user_id'];
        $this->Employees_model->add_spark_details($pdata);
        
        $sdata['user_id'] = $user_id;
        $sdata['state_id'] = $state_id;
        $sdata['start_date'] = date("Y-m-d");
        $this->users_model->add_user_state($sdata);
        
        header('location:'.base_url()."admin/employees/edit/$user_id");
      }else{
        $this->session->set_flashdata('alert', 'User id already exist.');
        redirect('admin/employees', 'refresh');
      }
	}

	public function edit()
	{		
			$statesData = $this->states_model->get_all_master_states();
			$this->data['statesData'] = $statesData;
			
			$sparks = $this->Employees_model->get_all_reporting_user();
			$this->data['sparks'] = $sparks;
      $id = $this->uri->segment(4);
      $this->data['action'] = 'update_employee';
      $this->data['user'] = $this->employees_model->getById($id);
      $this->data['logged_in_user'] = $this->data['current_user_id'];
      $this->data['page_title'] = 'Edit Employee';
      $this->data['partial'] = 'admin/employees/form';
      $this->load->view('templates/index', $this->data);
	}
  
	public function update_employee()
	{
      $spark_id = $_POST['id'];
      
      //$mdata['login_id'] = $this->input->post('login_id');
      $mdata['role'] = $this->input->post('role');
      $mdata['name'] = $this->input->post('name');
      $mdata['email'] = $this->input->post('email');
      $mdata['mobile'] = $this->input->post('mobile');
      $mdata['state_id'] = $this->input->post('state_id');
      $mdata['manager_id'] = ($this->input->post('manager_id') !='' ? $this->input->post('manager_id') : 0);
      $mdata['user_group'] = $this->input->post('user_group');
      $mdata['updated_on'] = date('Y-m-d H:i:s');
      $mdata['updated_by'] = $this->data['current_user_id'];
      $this->users_model->edit($mdata, $spark_id);
      
      $pdata['personal_email'] = $this->input->post('personal_email');
			$pdata['father_name'] = $this->input->post('father_name');
			$pdata['spouse_name'] = $this->input->post('spouse_name');
			$pdata['aadhar_number'] = $this->input->post('aadhar_number');
			
			if( strpos($this->input->post('aadhar_number'), '****') === false){
				$pdata['aadhar_number_full'] = $this->input->post('aadhar_number');
			}
			
			$pdata['gender'] = $this->input->post('gender');
			$pdata['birth_date'] = date('Y-m-d', strtotime($this->input->post('birth_date')));
			$pdata['joining_date'] = date('Y-m-d', strtotime($this->input->post('joining_date')));
			$pdata['branch'] = $this->input->post('branch');
			$pdata['department'] = $this->input->post('department');
			$pdata['grade'] = $this->input->post('grade');
			$pdata['emp_category'] = $this->input->post('emp_category');
			$pdata['division'] = $this->input->post('division');
			$pdata['attendance_cycle'] = $this->input->post('attendance_cycle');
			$pdata['emergency_number'] = $this->input->post('emergency_number');
			$pdata['blood_group'] = $this->input->post('blood_group');
			$pdata['marital_status'] = $this->input->post('marital_status');
			$pdata['designation'] = $this->input->post('designation');
      $pdata['unit_name'] = $this->input->post('unit_name');
      $pdata['project_name'] = $this->input->post('project_name');
      
      $pdata['pan_number'] = $this->input->post('pan_number');
			$pdata['pf_number'] = $this->input->post('pf_number');
			$pdata['uan_number'] = $this->input->post('uan_number');
      
			$pdata['notice_days'] = ($this->input->post('notice_days') != '' ? $this->input->post('notice_days') : 0);
			if($this->input->post('appraisal_month') != '')
				$pdata['appraisal_month'] = $this->input->post('appraisal_month');
			if($this->input->post('appraisal_year') != '')
				$pdata['appraisal_year'] = $this->input->post('appraisal_year');
			$pdata['bike'] = $this->input->post('bike');
			
			/*if(isset($_FILES) && $_FILES['emp_pic']['tmp_name'] != ''){
				print_r($_FILES);
			}*/
			
			//$pdata['emp_pic'] = $this->input->post('emp_pic');
			
			$pdata['created_at'] = date('Y-m-d H:i:s');
			$pdata['created_by'] = $this->data['current_user_id'];
			$this->Employees_model->update_spark_details($pdata, $spark_id);
			
			$this->session->set_flashdata('success', 'Employee updated successfully.');
      redirect('admin/employees/edit/'.$spark_id, 'refresh');
      //header('location:'.base_url()."admin/employees/".$this->index());
	}
				  
	public function delete($id)
	{
      $this->users_model->delete_a_user($id);
      $this->team_members_model->delete_employee_district_block($id);
      header('location:'.base_url()."admin/employees".$this->index());
	}

  public function changestatus()
  {
    $id = $this->uri->segment(4);
    $this->data['action'] = 'update';
    $employees = $this->users_model->getById($id);
    //print_r($this->data['newsletter']);
    extract ($employees);
    if( $status==1){
      $new_status=0;
    }
    else
    {
      $new_status="1";
    }
    $mdata['status'] = $new_status;
    $mdata['updated_on'] = date('Y-m-d H:i:s');
    $mdata['updated_by'] = $this->data['current_user_id'];
    $res=$this->users_model->update_info($mdata, $id);
    
    if($new_status == 0){
      $this->load->model('user_district_blocks_model');
      $this->user_district_blocks_model->delete_user_district_block($id);
      $this->Employees_model->update_user_district_block_history($id, '', $this->data['current_user_id']);
    }
    if($res)
    {
      header('location:'.base_url()."admin/employees");
    }
  }
  
  function employee_address()
  {
			if(empty($this->uri->segment(4)))
			{
				redirect('admin/employees');	
			}
			$saprk_id = $this->uri->segment(4);
		  
		  $currentAddress = $this->employees_model->get_employee_addresses($saprk_id, 'current');
		  
		  $permanentAddress = $this->employees_model->get_employee_addresses($saprk_id, 'permanent');
		  
		  if(!empty($currentAddress)){
				$this->data['currentAddress'] = $currentAddress;	
			}
		  
		  if(!empty($permanentAddress)){
				$this->data['permanentAddress'] = $permanentAddress;	
			}
		  
		  $this->data['action'] = 'update_employee_address';
			$statesData = $this->states_model->get_all_master_states();
			
			$this->data['spark_id'] = $saprk_id;
		  $this->data['statesData'] = $statesData;
			$this->data['partial'] = 'admin/employees/employee_address';
      $this->data['page_title'] = 'Employee Addresses';
      $this->load->view('templates/index', $this->data);	
	}
	
	function update_employee_address()
	{
		$spark_id = $_POST['spark_id'];
		$data['spark_id'] = $spark_id;
		$same_as = (isset($_POST['same_as']) && $_POST['same_as'] == 'on' ? 1 : 0);
		
		$data['type'] = 'current';
		$data['flat_number'] = $_POST['flat_number'];
		$data['premise'] = $_POST['premise'];
		$data['road'] = $_POST['road'];
		$data['area'] = $_POST['area'];
		$data['town'] = $_POST['town'];
		$data['pincode'] = $_POST['pincode'];
		$data['state'] = $_POST['state'];
		$data['country'] = $_POST['country'];
		$data['same_as'] = $same_as;
		
		$currentAddress = $this->employees_model->get_employee_addresses($spark_id, 'current');
		
		if(empty($currentAddress)){
			$data['created_at'] = date('Y-m-d H:i:s');
			$data['created_by'] = $this->data['current_user_id'];
			$this->employees_model->add_spark_address($data);
		}
		else{
			$data['updated_by'] = $this->data['current_user_id'];
			$address_id = $currentAddress[0]->id;
			$this->employees_model->update_spark_address($data, $address_id);
		}
		
		$pdata['type'] = 'permanent';
		$pdata['same_as'] = $same_as;
		if(isset($_POST['same_as']) && $_POST['same_as'] == 'on'){
			$pdata['spark_id'] = $spark_id;
			$pdata['flat_number'] = $_POST['flat_number'];
			$pdata['premise'] = $_POST['premise'];
			$pdata['road'] = $_POST['road'];
			$pdata['area'] = $_POST['area'];
			$pdata['town'] = $_POST['town'];
			$pdata['pincode'] = $_POST['pincode'];
			$pdata['state'] = $_POST['state'];
			$pdata['country'] = $_POST['country'];
		}
		else{
			$pdata['spark_id'] = $spark_id;
			$pdata['flat_number'] = $_POST['p_flat_number'];
			$pdata['premise'] = $_POST['p_premise'];
			$pdata['road'] = $_POST['p_road'];
			$pdata['area'] = $_POST['p_area'];
			$pdata['town'] = $_POST['p_town'];
			$pdata['pincode'] = $_POST['p_pincode'];
			$pdata['state'] = $_POST['p_state'];
			$pdata['country'] = $_POST['p_country'];
		}
		
		$permanentAddress = $this->employees_model->get_employee_addresses($spark_id, 'permanent');
		if(empty($permanentAddress)){
			$pdata['created_at'] = date('Y-m-d H:i:s');
			$pdata['created_by'] = $this->data['current_user_id'];
			$this->employees_model->add_spark_address($pdata);
		}
		else{
			$pdata['updated_by'] = $this->data['current_user_id'];
			$p_address_id = $permanentAddress[0]->id;
			$this->employees_model->update_spark_address($pdata, $p_address_id);
		}
		$this->session->set_flashdata('success', 'Employee address updated successfully.');
    redirect('admin/employees/employee_address/'.$spark_id, 'refresh');
	}
  
  function bank_details()
  {
		if(empty($this->uri->segment(4)))
		{
			redirect('admin/employees');	
		}
		$spark_id = $this->uri->segment(4);
		
		$bank_details = $this->employees_model->get_bank_address($spark_id);
				
		if(isset($_POST['submit']) && $_POST['submit'] == 'Save'){
			
			$pdata['payment_mode'] = $_POST['payment_mode'];
			$pdata['bank_branch'] = $_POST['bank_branch'];
			$pdata['company_bank'] = $_POST['company_bank'];
			$pdata['account_number'] = $_POST['account_number'];
			$pdata['account_name'] = $_POST['account_name'];
			$pdata['account_type'] = $_POST['account_type'];
			$pdata['rtgs_ifsc'] = $_POST['rtgs_ifsc'];
			$pdata['rtgs_location'] = $_POST['rtgs_location'];
			$pdata['remarks'] = $_POST['remarks'];
			
			if(empty($bank_details))
			{
				$pdata['spark_id'] = $spark_id;
				$pdata['created_at'] = date('Y-m-d H:i:s');
				$pdata['created_by'] = $this->data['current_user_id'];
				$this->employees_model->add_bank_address($pdata);
				$this->session->set_flashdata('success', 'Employee bank details added successfully.');
			}
			else{
				$pdata['updated_by'] = $this->data['current_user_id'];
				$this->employees_model->update_bank_address($pdata, $spark_id);
				$this->session->set_flashdata('success', 'Employee bank details updated successfully.');
			}
			redirect('admin/employees/bank_details/'.$spark_id, 'refresh');
		}
			
		$branchesData = $this->employees_model->get_all_branch();
		$banksData = $this->employees_model->get_all_company_banks();
		$this->data['bank_details'] = (!empty($bank_details) ? $bank_details[0] : array());
		$this->data['branchesData'] = $branchesData;
		$this->data['banksData'] = $banksData;
		$this->data['partial'] = 'admin/employees/employee_bank_details';
		$this->data['page_title'] = 'Employee Bank Details';
		$this->load->view('templates/index', $this->data);	
	}
	
	function employee_exit_details()
	{
		if(empty($this->uri->segment(4)))
		{
			redirect('admin/employees');	
		}
		$spark_id = $this->uri->segment(4);
		
		$emp_details = $this->employees_model->getById($spark_id);	
		
		if(empty($emp_details)){
			redirect('admin/employees');	
		}
		
		if(isset($_POST['submit']) && $_POST['submit'] == 'Save'){
			
			$exit_date = date('Y-m-d', strtotime($_POST['exit_date']));;
			$pdata['schedule_exit_date'] = date('Y-m-d', strtotime($_POST['schedule_exit_date']));
			$pdata['exit_date'] = $exit_date;
			$pdata['resignation_date'] = date('Y-m-d', strtotime($_POST['resignation_date']));
			$pdata['resign_reason'] = $_POST['resign_reason'];
			$pdata['notice_fully_served'] = $_POST['notice_fully_served'];
			$pdata['notice_served'] = ($_POST['notice_fully_served'] == 'Yes' ? NOTICE_DAYS : $_POST['notice_served']);
			
			$this->employees_model->update_spark_details($pdata, $spark_id);
			
			//Update ssc_sparks table with exit date 
			$this->employees_model->update_details('sparks', array('exit_date'=>$exit_date), array('id'=>$spark_id));
			
			$this->session->set_flashdata('success', 'Employee exit details updated successfully.');
			redirect('admin/employees/employee_exit_details/'.$spark_id, 'refresh');
		}	
		$this->data['spark_id'] = $spark_id;
		$this->data['emp_details'] = $emp_details;
		$this->data['partial'] = 'admin/employees/employee_exit_details';
		$this->data['page_title'] = 'Employee Exit Details';
		$this->load->view('templates/index', $this->data);	
  }
  
  
  function reset_password()
  {
		if(empty($this->uri->segment(4)))
		{
			redirect('admin/employees');	
		}
		$empid = $this->uri->segment(4);
		$this->data['spark_id'] = $empid;
		
    $employees = $this->users_model->getById($empid);

    if(empty($employees))
    {
			redirect('admin/employees');	
		}

    if(isset($_POST['submit']) && $_POST['submit'] == 'Save')
    {
			$spark_id = $this->input->post('spark_id');
			$new_pwd = $this->input->post('new_pwd');
			
			if($spark_id != '' && $new_pwd != '')
			{
				$employees = $this->users_model->getById($spark_id);
				$data['password'] = md5($new_pwd);
				$this->users_model->edit($data, $spark_id);
				$this->session->set_flashdata('success', 'Password reset successfully.');
				redirect('admin/employees/reset_password/'.$empid, 'refresh');
			}
		}
		$this->data['partial'] = 'admin/employees/reset_password';
		$this->data['page_title'] = 'Employee Reset Password';
		$this->load->view('templates/index', $this->data);	
	} 
	
	function employee_salary_details()
	{
		if(empty($this->uri->segment(4)))
		{
			redirect('admin/employees');	
		}
		$spark_id = $this->uri->segment(4);
		
		$emp_details = $this->employees_model->getById($spark_id);	
		
		if(empty($emp_details)){
			redirect('admin/employees');	
		}
		
		if(isset($_POST['submit']) && $_POST['submit'] == 'Save'){
			
			$pdata['spark_id'] = $_POST['spark_id'];
			$pdata['effective_date'] = date('Y-m-d', strtotime($_POST['effective_date']));
			$pdata['basic_salary'] = $_POST['basic_salary'];
			$pdata['hra'] = $_POST['hra'];
			$pdata['conveyance_allowance'] = $_POST['conveyance_allowance'];
			$pdata['special_allowance'] = $_POST['special_allowance'];
			$pdata['children_education_allowance'] = $_POST['children_education_allowance'];
			$pdata['children_education_hostel'] = $_POST['children_education_hostel'];
			$pdata['medical_allowance'] = $_POST['medical_allowance'];
			$pdata['bike_allowance'] = $_POST['bike_allowance'];
			$pdata['consultancy_charges'] = $_POST['consultancy_charges'];
			$pdata['ampb'] = $_POST['ampb'];
			$pdata['performance_incentive'] = $_POST['performance_incentive'];
			$pdata['pf_allowance'] = $_POST['pf_allowance'];
			$pdata['esic_employer_deductior'] = $_POST['esic_employer_deductior'];
			$pdata['provident_fund'] = $_POST['provident_fund'];
			$pdata['esic'] = $_POST['esic'];
			$pdata['profession_tax'] = $_POST['profession_tax'];
			$pdata['tds'] = $_POST['tds'];
			$pdata['laptop_advance'] = $_POST['laptop_advance'];
			$pdata['telephone_advance'] = $_POST['telephone_advance'];
			$pdata['other_advance'] = $_POST['other_advance'];
			$pdata['tds_profession_fee'] = $_POST['tds_profession_fee'];
			$pdata['non_emplyoee_dedution'] = $_POST['non_emplyoee_dedution'];
			$pdata['pf_control'] = $_POST['pf_control'];
			$pdata['telephone_peimbusemer'] = $_POST['telephone_peimbusemer'];
			$pdata['car_running_amount'] = $_POST['car_running_amount'];
			$pdata['car_lease_amount'] = $_POST['car_lease_amount'];
			$pdata['air_ticket_amount'] = $_POST['air_ticket_amount'];
			$pdata['flexi_benefit_amount'] = $_POST['flexi_benefit_amount'];
			$pdata['pf_wages'] = $_POST['pf_wages'];
			$pdata['ampi_entitlement'] = $_POST['ampi_entitlement'];
			$pdata['pension_wage'] = $_POST['pension_wage'];
			$pdata['esic_wage_for_checking'] = $_POST['esic_wage_for_checking'];
			$pdata['eps_employer'] = $_POST['eps_employer'];
			$pdata['total_monthly'] = $_POST['total_monthly'];

			$cond['spark_id'] = $_POST['spark_id'];
			$cond['effective_date'] = date('Y-m-d', strtotime($_POST['effective_date']));
			
			$getData = $this->employees_model->check_employee_details('sparks_salary_details', $cond);
			
			if(!empty($getData)){
				$pdata['created_at'] = date('Y-m-d H:i:s');
				$pdata['created_by'] = $this->data['current_user_id'];
				$this->employees_model->update_details('sparks_salary_details', $pdata, $cond);
			}
			else{
				$pdata['updated_by'] = $this->data['current_user_id'];
				$this->employees_model->add_spark_details($pdata, 'sparks_salary_details');
			}
			redirect('admin/employees/employee_salary_details/'.$spark_id, 'refresh');
		}	
		$salary_data = $this->employees_model->get_employee_salary_details($spark_id, 1);
		
		$this->data['spark_id'] = $spark_id;
		$this->data['salary_data'] = $salary_data;
		$this->data['emp_details'] = $emp_details;
		$this->data['partial'] = 'admin/employees/employee_salary_details';
		$this->data['page_title'] = 'Employee Salary Details';
		$this->load->view('templates/index', $this->data);	
  }
  
  function map_spark_employee()
	{
		$sparksData = $this->employees_model->get_all();
		
		foreach($sparksData as $sparks){
			$checkDetails = $this->employees_model->check_detail_exist($sparks->id);
			
			if(empty($checkDetails))
			{
				$data['spark_id'] = $sparks->id;
				$data['created_by'] = $this->data['current_user_id'];
				$data['created_at'] = date('Y-m-d H:i:s');
				$this->employees_model->add_spark_details($data);
			}
		}
		
	}
	
	function upload()
  {
    $fp = fopen($_FILES['fileToUpload']['tmp_name'],'r') or die("can't open file");
    $count = 0;
    //$all_disecodes = array();

    while($csv_line = fgetcsv($fp,1024))
    {
        $count++;
        $error = 0;
        if($count == 1)
        {
          $emp_code = $csv_line[0];//remove if you want to have primary key,
          $att_date = $csv_line[1];
          $in_time = $csv_line[2];
          $out_time = $csv_line[3];
          $att_type = $csv_line[4];
          $reason = (isset($csv_line[5]) ? $csv_line[5] : '');
          if ($emp_code != "Empolyee Code")
            $error = 1;
          if ($att_date != "Date")
            $error = 1;
          if ($in_time != "In Time")
            $error = 1;
          if ($out_time != "Out Time")
            $error = 1;
          if ($att_type != "Attendance Type")
            $error = 1;

          if ($error == 1)
          {
            $this->session->set_flashdata('alert', "The template is not correct or you had made some changes in the first (header) row of the template. Kindly download the template again and upload attendance using the new template.");
            redirect('activity/attendance_regularizations', 'refresh');
          }
          continue;
        }//keep this if condition if you want to remove the first row
        for($i = 0, $j = count($csv_line); $i < $j; $i++)
        { 
					$emp_code = $csv_line[0];//remove if you want to have primary key,
          $att_date = str_replace('/', '-',$csv_line[1]);
          $att_date = date('Y-m-d', strtotime($att_date));
          $in_time = $csv_line[2];
          $out_time = $csv_line[3];
          $att_type = $csv_line[4];
          $reason = (isset($csv_line[5]) ? $csv_line[5] : '');
        }
        
        if (strlen($emp_code) == 5)
        {
          $emp_code = str_pad($emp_code,5,'0',0);
        }
        if (strlen($emp_code) == 5)
        {
          //array_push($all_disecodes,$dise_code);

          $check_employee = $this->users_model->getById('', $emp_code);
          
          if(!empty($check_employee))
          {
						$this->load->Model('Attendance_model');
						$spark_id = $check_employee['id'];
						
						$check_attendance = $this->Attendance_model->check_attendance($spark_id, $att_date);
						
						if(!empty($check_attendance))
						{
							$att_id = $check_attendance[0]->id;
							
							$mdata['attendance_date'] =  $att_date;
							$mdata['source'] =  'upload';
							$mdata['attendance_type'] =  $att_type;
							$mdata['in_time'] =  date('Y-m-d H:i:s', strtotime($att_date." ".$in_time));
							$mdata['out_time'] = date('Y-m-d H:i:s', strtotime($att_date." ".$out_time));
							$mdata['status'] =  'approved';
							$mdata['updated_at'] =  date('Y-m-d');
							$mdata['updated_by'] =  $this->data['current_user_id'];
							$mdata['reason'] =  $reason;
							
							$this->Attendance_model->updateregularizations($mdata, $att_id);
						}
						else{
							$data['spark_id'] =  $spark_id;
							$data['attendance_date'] =  $att_date;
							$data['created_at'] =  date('Y-m-d');
							$data['created_by'] =  $this->data['current_user_id'];
							$data['source'] =  'upload';
							$data['attendance_type'] =  $att_type;
							$data['in_time'] =  date('Y-m-d H:i:s', strtotime($att_date." ".$in_time));
							$data['out_time'] =  date('Y-m-d H:i:s', strtotime($att_date." ".$out_time));
							$data['status'] =  'approved';
							$data['reason'] =  $reason;
							
							$this->Attendance_model->insert_regularizations($data);
						}
					}
        }
        $i++;
    }
    //$old_schools->where("state_id",$state_id)->where_not_in('dise_code',$all_disecodes)->update(array("status"=>0));
    fclose($fp) or die("can't close file");
    $data['success']="success";
    $this->session->set_flashdata('success', "File uploaded and all the schools added successfully");
    redirect('activity/attendance_regularizations', 'refresh');
  }
  
	function employee_attendance()
	{
		$this->load->Model('leaves_model');
		$this->load->Model('Attendance_model');
		$this->load->Model('Tracking_model');
		$this->load->Model('Holidays_model');
		$role = array('field_user', 'manager', 'state_person', 'accounts', 'executive');
		
		//$division = (isset($_POST['division']) ? $_POST['division'] : '');
		$state_id = (isset($_POST['division']) ? $_POST['division'] : '');
		//$state = '';
		$employeeData = $this->Employees_model->get_employees_list($role, 1, $state_id);
		
		$cur_date = date('d-m-Y');
		$start_date = (empty($_POST['attendancestart']) ? date('d-m-Y', strtotime($cur_date. ' -30 days')) : date('d-m-Y', strtotime($_POST['attendancestart'])));
		$end_date = (empty($_POST['attendanceend']) ? date('d-m-Y') : date('d-m-Y', strtotime($_POST['attendanceend'])));
		
		$activity_arr = array();
		$attendanceDates = array();
		$dates_array = array();
				

		$attendancelist = $this->Attendance_model->get_attendance_reg_list(array(), date('Y-m-d', strtotime($start_date)), date('Y-m-d', strtotime($end_date)), 'approved');
		
		foreach($attendancelist as $attendances)
		{
			$login_id = $attendances->login_id;
			//$tracking_time = $attendances->tracking_time;
				
			$tracking_date = date('Y-m-d', strtotime($attendances->attendance_date));
			
			if($attendances->attendance_type == 'PST' || $attendances->attendance_type == 'Full Day')
			{
				$attendance_type = 'FPCAuto';
			}
			else if($attendances->attendance_type == 'First Half' || $attendances->attendance_type == 'Second Half')
			{
				$attendance_type = 'ABSHD';
			}
			else{
				$attendance_type = $attendances->attendance_type;
			}
			
			if(!isset($attendanceDates[$tracking_date][$login_id])){
				$attendanceDates[$tracking_date][$login_id] = $attendance_type;
			}
			else{
				$attendanceDates[$tracking_date][$login_id] = $attendance_type;
			}
			if(!isset($activity_arr[$tracking_date][$login_id])){
				$activity_arr[$tracking_date][$login_id] = $attendance_type;
			}
			if(!in_array($tracking_date, $dates_array))
				array_push($dates_array, $tracking_date); 
		}	  
		
		$activity_type = array('attendance-in', 'attendance-out');
		$attandance_data = $this->Tracking_model->get_track_attendance_list(array(), date('Y-m-d', strtotime($start_date)), date('Y-m-d', strtotime($end_date)), $activity_type);
		
		foreach($attandance_data as $attandances){
			$login_id = $attandances->login_id;
			$tracking_time = $attandances->tracking_time;
			$tracking_date = date('Y-m-d', strtotime($attandances->tracking_time));
			
			if(!isset($attendanceDates[$tracking_date][$login_id])){
				$attendanceDates[$tracking_date][$login_id] = 'PST';
			}
			if(!isset($activity_arr[$tracking_date][$login_id])){
				$activity_arr[$tracking_date][$login_id] = 'PST';
			}
			if(!in_array($tracking_date, $dates_array))
				array_push($dates_array, $tracking_date); 
		}
		
		
		$field_OffDays = array();
		$HO_OffDays = array();
		$fieldOffDays = $this->Holidays_model->get_all_sat_sun_list(date('Y-m-d', strtotime($start_date)), date('Y-m-d', strtotime($end_date)));
		foreach($fieldOffDays as $offdays)
		{
			if($offdays->user_group == 'field_user')
				$field_OffDays[$offdays->activity_date] = $offdays->type;
			else
				$HO_OffDays[$offdays->activity_date] = $offdays->type;
		}
		
		$field_Holidays = array();
		$HO_Holidays = array();
		$HolidaysData = $this->Holidays_model->get_all_holidays_list(date('Y-m-d', strtotime($start_date)), date('Y-m-d', strtotime($end_date)));
		foreach($HolidaysData as $offdays)
		{
			if($offdays->holiday_type == 'field_user')
				$field_Holidays[$offdays->stateid][$offdays->activity_date] = $offdays->name;
			else	
				$field_Holidays[$offdays->stateid][$offdays->activity_date] = $offdays->name;
		}
		
		$LEAVE_TYPES_SORTS = unserialize(LEAVE_TYPES_SORT);
		
		$leaves_dates_array = array();
		$leave_data = $this->leaves_model->get_all_leave_list('', date('Y-m-d', strtotime($start_date)), date('Y-m-d', strtotime($end_date)));
		foreach($leave_data as $leaves)
		{
			$getDates = $this->common_functions->createRange($leaves->leave_from_date, $leaves->leave_end_date, 'Y-m-d');
			
			foreach($getDates as $getDate)
			{
				$login_id = $leaves->login_id;
				$ltype = strtolower($leaves->leave_type);
				if(!isset($leaves_dates_array[$getDate][$login_id])){
					$leaves_dates_array[$getDate][$login_id] = $LEAVE_TYPES_SORTS[$ltype];
				}
				//else{
					//$leaves_dates_array[$getDate][$login_id] = $LEAVE_TYPES_SORTS[$ltype];
				//}
				
				if(!isset($activity_arr[$getDate][$login_id])){
					$activity_arr[$getDate][$login_id] = $LEAVE_TYPES_SORTS[$ltype];
				}
				if(!in_array($getDate, $dates_array))
					array_push($dates_array, $getDate); 
			}
		}
    //print_r($activity_arr);  
		$rangeDates = $this->common_functions->createRange($start_date, $end_date);
		
		$this->data['state_id'] = $state_id;
		$this->data['fOffDays'] = $field_OffDays;
		$this->data['hOffDays'] = $HO_OffDays;
		$this->data['field_holiday'] = $field_Holidays;
		$this->data['HO_holiday'] = $HO_Holidays;
		$this->data['activity_arr'] = $activity_arr;
		$this->data['attendanceDates'] = $attendanceDates;
		$this->data['dates_array'] = $dates_array;
		$this->data['leaves_dates_array'] = $leaves_dates_array;
		$this->data['rangeDates'] = $rangeDates;
		$this->data['attendancestart'] = $start_date;
		$this->data['attendanceend'] = $end_date;
		$this->data['employees_list'] = $employeeData;
		
		$download = (isset($_POST['download']) and $_POST['download'] == 1 ? $_POST['download'] : 0);
    $pdf = (isset($_POST['pdf']) and $_POST['pdf'] == 1 ? $_POST['pdf'] : 0);
    
		if ($download == 1)
    {
      $this->data['excel'] = 1;
      $myFile = "./uploads/reports/employee_attendance_details.xls";
      $this->load->library('parser');
      $this->data['download'] = $download;
      //pass retrieved data into template and return as a string
      $stringData = $this->parser->parse('admin/employees/employee_attendance_details', $this->data, true);

      //open excel and write string into excel
      $fh = fopen($myFile, 'w') or die("can't open file");
      fwrite($fh, $stringData);
      fclose($fh);
      //download excel file
      $this->common_functions->downloadExcel("employee_attendance_details");
      unlink($myFile);
    }
    else if($pdf == 1)
    {
        //pass retrieved data into template and return as a string
        $this->data['pdf'] = 1;
        $summary = $this->load->view('admin/employees/employee_attendance_details',$this->data,true);
        $this->common_functions->create_pdf($summary,'employee_attendance_details');
    }
    else{
			$this->data['partial'] = 'admin/employees/employee_attendance_details';
			$this->data['page_title'] = 'Employee Attendance Details';
			$this->load->view('templates/index', $this->data);	
		}
	}	
  
}
