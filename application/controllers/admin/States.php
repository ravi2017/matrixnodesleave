<?php

class States extends CI_Controller  {
  function __construct() {
    parent::__construct();
    $this->load->helper(array('form', 'url'));
    $this->load->model('states_model');
    $this->load->model('users_model');
    $this->load->library('form_validation');
    $this->load->library('common_functions');   
    
		if($this->session->userdata('admin_logged_in'))
		{
		  $session_data = $this->session->userdata('admin_logged_in');
      $this->data['current_user_id'] = $session_data['id'];
      $this->data['current_username'] = $session_data['username'];
      $this->data['current_name'] = $session_data['name'];
      $this->data['current_role'] = $session_data['role'];
      $this->data['user_state_id'] = $session_data['state_id'];
      $this->data['user_district_id'] = $session_data['district_id'];
      $this->data['current_group'] = $session_data['user_group'];
    }
		else
		{
      redirect('admin/login', 'refresh');
		}
    $this->data['breadcrumps'] = ['States'];
  }

  function index() {	
    $this->data['state_list'] = $this->states_model->get_all_states();
    $this->data['partial'] = 'admin/states/index';
    $this->data['page_title'] = 'States';
    array_push($this->data['breadcrumps'],"Listing");
    $this->load->view('templates/index', $this->data);
  }
	
	public function add()
	{
    $this->data['partial'] = 'admin/states/form';
    $this->data['action'] = 'create';
    $this->data['page_title'] = 'Add State';
    $this->load->view('templates/index', $this->data);
  }
	
  function create() 
  {		     
    $mdata['name'] = $this->input->post('name');
    $mdata['dise_code'] = $this->input->post('dise_code');
    $mdata['created_on'] = date('Y-m-d H:i:s');
    $mdata['created_by'] = $this->data['current_user_id'];

    $res = $this->states_model->insert_state_to_db($mdata);
    if($res)
    {
      header('location:'.base_url()."admin/states");
    }
    else
    {
      $this->data['partial'] = 'admin/states/form';
      $this->data['action'] = 'create';
      $this->data['page_title'] = 'Add State';
      $this->load->view('templates/index', $this->data);	
    }
  }
	
	public function edit($id)
	{
    $this->data['state'] = $this->states_model->getById($id);
    $this->data['partial'] = 'admin/states/form';
    $this->data['action'] = 'update';
    $this->data['page_title'] = 'Edit State';
    $this->load->view('templates/index', $this->data);
	}
	
  function update() 
	{	
    $mdata['name'] = $this->input->post('name');
    $mdata['dise_code'] = $this->input->post('dise_code');
    $mdata['updated_on'] = date('Y-m-d H:i:s');
    $mdata['updated_by'] = $this->data['current_user_id'];

    $res=$this->states_model->update_info($mdata, $_POST['id']);
    if($res)
    {
      header('location:'.base_url()."admin/states");
    }
    else
    {
      $this->data['state'] = $this->states_model->getById($_POST['id']);
      $this->data['partial'] = 'admin/states/form';
      $this->data['action'] = 'update';
      $this->data['page_title'] = 'Edit State';
      $this->load->view('templates/index', $this->data);		
    }
  }
	
	public function delete($id)
	{
		$State = $this->states_model->getById($id);
		//if ($State['image'] != "" and file_exists("./".$State['image']))
	//		unlink("./".$State['image']);
			
		$this->states_model->delete_a_state($id);
    header('location:'.base_url()."admin/states");
	}
  
  public function show()
  {
		$id = $this->uri->segment(4);
		$state = $this->states_model->getById($id);
    $un_responsed_questions = $this->states_model->get_question($id);
    
    $options = "";
    
    $this->data['state_over'] = 0;
    if (count($un_responsed_questions) == 0)
    {
      $this->data['state_over'] = 1;
    } else {
      if ($un_responsed_questions['question_type'] == "objective" or $un_responsed_questions['question_type'] == "multiple")
      {
        $options = $this->questions_model->get_options($un_responsed_questions['id']);
      }
    }
    
    $this->data['state'] = $state;
    $this->data['cur_question'] = $un_responsed_questions;
    $this->data['options'] = $options;
    $this->data['partial'] = 'admin/states/show';
    $this->data['page_title'] = $state['title'];
    $this->load->view('templates/state', $this->data);	
  }
  
	public function changestatus()
	{
		$id = $this->uri->segment(4);
		$this->data['action'] = 'update';
		$state = $this->states_model->getById($id);
		extract ($state);
		if( $status=="1"){
      $new_status="0";
		}
		else
		{
			$new_status="1";
		}
		$mdata['status'] = $new_status;
    $mdata['updated_on'] = date('Y-m-d H:i:s');
    $mdata['updated_by'] = $this->data['current_user_id'];
		$res=$this->states_model->update_info($mdata, $id);
		if($res)
    {
      header('location:'.base_url()."admin/states".$this->index());
    }
  }
  
  function get_list()
  {
    $states = $this->states_model->get_all_states();
    
    if(count($states) > 0){
      foreach ($states as $row){
       $row1['value'] = htmlentities(stripslashes($row->name));
       $row1['id'] = $row->id;
       $row_set[] = $row1;
      }
      echo json_encode($row_set); //format the array into json data
    }    
  }
  
  function maturity_index() {	
    $this->data['state_list'] = $this->states_model->get_all_states();
    $this->data['maturity_list'] = $this->states_model->get_maturity_index();
    $this->data['partial'] = 'admin/states/maturity_index';
    $this->data['page_title'] = 'Program Maturity Index';
    $this->load->view('templates/index', $this->data);
  }
  
  function create_maturity_index()
  {
    $exp_state = explode('_',$this->input->post('sel_state'));
    $state_id = $exp_state[0];
    $state_name = $exp_state[1];
    $score = $this->input->post('maturity_score');
    
    $inpmonth = date('m');
    $inpyear = date('Y');
    $month_year = sprintf('%02d', $inpmonth)."_".$inpyear;
    
    $previous_data = $this->states_model->get_maturity_index($state_id, $month_year);
    
    if(empty($previous_data)){
      $mdata['state_id'] = $state_id;
      $mdata['state'] = $state_name;
      $mdata['score'] = $score;
      $mdata['month_year'] = $month_year;
      $res = $this->states_model->insert_maturity_index($mdata);
    }
    header('location:'.base_url()."admin/states/maturity_index"); 
  }
  
  function delete_maturity($id)
  {
     $this->states_model->delete_a_maturity($id);
     header('location:'.base_url()."admin/states/maturity_index"); 
  }
}
	
	
	
	
	
	
	
