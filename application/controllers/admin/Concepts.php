<?php

class Concepts extends CI_Controller  {
  function __construct() {
    parent::__construct();
	
    $this->load->helper(array('form', 'url'));
    $this->load->model('subjects_model');
    $this->load->model('classes_model');
    $this->load->model('concepts_model');
    $this->load->library('form_validation');
  
    $this->data['current_page'] = 'concepts';
    $this->load->library('common_functions'); 
    $this->load->library('ckeditor');
    $this->load->library('ckfinder');
    $this->ckeditor->basePath = base_url().'assets/admin/ckeditor/';
    $this->ckeditor->config['toolbar'] ='Full'; 
    $this->ckeditor->config['language'] = 'en';
    $this->ckeditor->config['width'] = '100%';
    $this->ckeditor->config['height'] = '300px';   
    
		if($this->session->userdata('admin_logged_in'))
		{
		  $session_data = $this->session->userdata('admin_logged_in');
      $this->data['current_user_id'] = $session_data['id'];
      $this->data['current_username'] = $session_data['username'];
      $this->data['current_name'] = $session_data['name'];
      $this->data['current_role'] = $session_data['role'];
      $this->data['user_state_id'] = $session_data['state_id'];
      $this->data['user_district_id'] = $session_data['district_id'];
    }
		else
		{
      redirect('admin/login', 'refresh');
		}		        
  }

  function index() 
  {
    $this->data['concepts_list'] = $this->concepts_model->get_all_concepts();
    $this->data['partial'] = 'admin/concepts/index';
    $this->data['page_title'] = 'Concepts';
    $this->load->view('templates/index', $this->data);
    //$this->load->view('admin/admins/index', array('error' => ' ' ));
  }
  
  public function add()
  {	
		$this->data['subjects_list'] = $this->subjects_model->get_all();
		$this->data['classes_list'] = $this->classes_model->get_all();
    $this->data['partial'] = 'admin/concepts/form';
    $this->data['action'] = 'create';
    $this->data['page_title'] = 'Add Concept';
    $this->load->view('templates/index', $this->data);		
  }

  function create() 
  {		
    $this->load->library('form_validation');
    $this->form_validation->set_rules('name', 'Name', 'required');
    
    $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
    if ($this->form_validation->run() == FALSE)
    { 
      $this->data['partial'] = 'admin/concepts/form';
      $this->data['action'] = 'create';
      $this->data['page_title'] = 'Add Concept';
      $this->load->view('templates/index', $this->data);
    }
    else
    {
      $mdata['name'] = $this->input->post('name');
      $mdata['class_id'] = $this->input->post('class_id');
      $mdata['subject_id'] = $this->input->post('subject_id');
      $mdata['progress_chart'] = 0;
      $mdata['end_line'] =0;
      $mdata['base_line'] =0;
      foreach($_POST['available'] as $available) {
        if ($available == "end_line")
          $mdata['end_line'] = 1;
        if ($available == "base_line")
          $mdata['base_line'] = 1;
      }
      //$res= $this->db->insert('concepts', $mdata);
      $res= $this->concepts_model->insert_concepts_to_db($mdata);
      //redirect code
      if($res)
      {
        header('location:'.base_url()."admin/concepts/".$this->index());
      }
    }
  }

  public function edit()
  {
		$this->data['subjects_list'] = $this->subjects_model->get_all();
		$this->data['classes_list'] = $this->classes_model->get_all();
		$id = $this->uri->segment(4);
		$this->data['admins'] = $this->concepts_model->getById($id);
		$this->data['partial'] = 'admin/concepts/form';
		$this->data['action'] = 'update';
		$this->data['page_title'] = 'Edit Concept';
		$this->load->view('templates/index', $this->data);		
  }

  function update() 
  {	
		$this->load->library('form_validation');
		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		if ($this->form_validation->run() == FALSE)
			{

				$id = $this->uri->segment(4);
				$this->data['admins'] = $this->concepts_model->getById($id);
				$this->data['partial'] = 'admin/concepts/form';
				$this->data['action'] = 'update';
				$this->data['page_title'] = 'Edit Concept';
				$this->load->view('templates/index', $this->data);	
			}
			else
			{
				$mdata['name'] = $this->input->post('name');
        $mdata['class_id'] = $this->input->post('class_id');
        $mdata['subject_id'] = $this->input->post('subject_id');
				$mdata['progress_chart'] = 0;
        $mdata['end_line'] =0;
        $mdata['base_line'] =0;
        foreach($_POST['available'] as $available) {
          if ($available == "end_line")
            $mdata['end_line'] = 1;
          if ($available == "base_line")
            $mdata['base_line'] = 1;
        }
				$res=$this->concepts_model->update_info($mdata, $_POST['id']);
        if($res)
        {
          header('location:'.base_url()."admin/concepts".$this->index());
        }
		  }
  }
  
  public function delete($id)
  {
    $admins = $this->concepts_model->getById($id);
    $this->concepts_model->delete_a_concepts($id);
    header('location:'.base_url()."admin/concepts".$this->index());
  }

  function get_list()
  {
    $subject_id = $_GET['subject_id'];
    $class_id = $_GET['class_id'];
    $test_type = $_GET['test_type'];
		$base_line=" ";
		$end_line =" ";
		if($test_type == 'base_line'){
			$base_line=1;
		}
		if($test_type == 'end_line'){
			$end_line=1;
		}
    $concepts = $this->concepts_model->get_all_concepts($class_id, $subject_id,$progress_chart='',$base_line,$end_line);
    if(count($concepts) > 0){
      foreach ($concepts as $row){
        $value = "";
        $row1['value'] = $value.ucwords(htmlentities(stripslashes($row->name)));
        $row1['id'] = $row->id;
        $row_set[] = $row1;
      }
      echo json_encode($row_set); //format the array into json data
    }
  } 
}
