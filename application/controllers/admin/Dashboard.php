<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		
		$this->load->helper('url');
    $this->load->model('users_model');
    $this->load->model('districts_model');
		$this->load->model('team_members_model');
    
		if($this->session->userdata('admin_logged_in'))
		{
		  $session_data = $this->session->userdata('admin_logged_in');
      $this->data['id'] = $session_data['id'];
      $this->data['current_username'] = $session_data['username'];
      $this->data['current_name'] = $session_data['name'];
      $this->data['current_role'] = $session_data['role'];
      $this->data['user_state_id'] = $session_data['state_id'];
      $this->data['user_district_id'] = $session_data['district_id'];
    }
		else
		{
      redirect('admin/login', 'refresh');
		}		
   
	}
  
  function routes()
  {
    echo '<pre>';
    print_r($this->router->routes);
    echo '</pre>';
    //exit();
  }
  
	public function index()
	{
		if($this->session->userdata('admin_logged_in'))
		{
		  $session_data = $this->session->userdata('admin_logged_in');
		  $data['id'] = $session_data['id'];
		  $role = $session_data['role'];
		  //echo $role ;exit();
			$data['admins_list'] = $this->users_model->get_all_users();
			
			$this->data['partial'] = 'admin/dashboard';
			$this->load->view('templates/index', $this->data);	
		}
		else
		{
      redirect('admin/login', 'refresh');
		}		
	}
	
	public function logout()
	{
		$this->session->unset_userdata('admin_logged_in');
		//session_destroy();
		redirect('admin/login', 'refresh');
	}
  
  function get_districts()
  {
    $this->load->model('districts_model');  
    $this->load->model('schools_model');  
    $state = $_GET['state'];
    $districts = $this->districts_model->get_state_districts($state, $this->data['user_district_id'], "");
    $limited = "all";
    if(count($districts) > 0){
      foreach ($districts as $row){
        if ($limited == "1")
        {
          if (in_array($row->id,$allowed_disricts))
          {
            $value = "";
            if ($row->dise_code != "")
              $value = "(".$row->dise_code.") ";
            $row1['value'] = $value.ucwords(htmlentities(stripslashes($row->name)));
            $row1['id'] = $row->id;
            $row_set[] = $row1;
          }
        }
        else
        {
          $school_count = $this->schools_model->get_school_count(0, $row->id);
          $value = "";
          if ($row->dise_code != "")
            $value = "(".$row->dise_code.") ";
          $row1['school_count'] = $school_count;
          $row1['value'] = $value.ucwords(htmlentities(stripslashes($row->name)));
          $row1['id'] = $row->id;
          $row_set[] = $row1;
        }
      }
      echo json_encode($row_set); die;//format the array into json data
    }    
  }
  
  function get_villages()
  {
    $session_data = $this->session->userdata('logged_in');
    $limited = "0";
    if(isset($session_data['id'])){
      $limited = "1";
      $allowed_villages = array();
      $t_id = $session_data['id'];
      $teacher_details = $this->teacher_details_model->getById($t_id);
      
      if ($teacher_details['village_id'] > 0)
        array_push($allowed_villages,$teacher_details['village_id']);
        
      if (count($allowed_villages) == 0)
        $limited = "0";
    }
    
    //$limited = "0";
    $cluster = $_GET['cluster'];
    $villages = $this->villages_model->get_cluster_villages($cluster, "");
    
    if(count($villages) > 0){
      foreach ($villages as $row){
        if ($limited == "1")
        {
          if (in_array($row->id,$allowed_villages))
          {
            $value = "";
            if ($row->dise_code != "")
              $value = "(".$row->dise_code.") ";
            $row1['value'] = $value.ucwords(htmlentities(stripslashes($row->name)));
            $row1['id'] = $row->id;
            $row_set[] = $row1;
          }
        }
        else
        {
          $value = "";
          if ($row->dise_code != "")
            $value = "(".$row->dise_code.") ";
          $row1['value'] = $value.ucwords(htmlentities(stripslashes($row->name)));
          $row1['id'] = $row->id;
          $row_set[] = $row1;
        }
      }
      echo json_encode($row_set); //format the array into json data
    }    
  }
  
  function get_schools()
  {    
    $this->load->model('schools_model');  
    $session_data = $this->session->userdata('logged_in');
    $limited = "all";
    
    if (isset($_GET['cluster']))
      $cluster = $_GET['cluster'];
    else
      $cluster = 0;
      
    if (isset($_GET['block']))
      $block = $_GET['block'];
    else
      $block = 0;
    $schools = $this->schools_model->search_schools(0,0,$block,$cluster);
   
    $product_id = $_GET['product_id'];
    $this->load->model('product_issued_model');  
    $d = $this->product_issued_model->getSchoolsByProduct($product_id);
    $alloted_schools = array();
    $alloted_schools_quantity = array();
    foreach($d as $school_data)
    {
      array_push($alloted_schools,$school_data->school_id);
      $alloted_schools_quantity[$school_data->school_id] = $school_data->quantity;
    }
    
    if(count($schools) > 0){
      foreach ($schools as $row){
        if ($limited == "1")
        {
          if (in_array($row->id,$allowed_schools))
          {
            $value = "";
            if ($row->dise_code != "")
              $value = "(".$row->dise_code.") ";
            $row1['block'] = $value.ucwords(htmlentities(stripslashes($row->block_name)));
            $row1['cluster'] = ucwords(htmlentities(stripslashes($row->cluster_name)));
            $row1['value'] = ucwords(htmlentities(stripslashes($row->name)));
            $row1['id'] = $row->id;
            $row1['cluster_id'] = $row->cluster_id;
            $row1['block_id'] = $row->block_id;
            $row_set[] = $row1;
          }
        }
        else
        {
          $value = "";
          if ($row->dise_code != "")
            $value = "(".$row->dise_code.") ";
          $row1['value'] = $value.ucwords(htmlentities(stripslashes($row->name)));
          $row1['id'] = $row->id;
          $row1['dise_code'] = $row->dise_code;
          $row1['block'] = ucwords(htmlentities(stripslashes($row->block_name)));
          $row1['cluster'] = ucwords(htmlentities(stripslashes($row->cluster_name)));
          $row1['cluster_id'] = $row->cluster_id;
          $row1['block_id'] = $row->block_id;
          if (in_array($row->id,$alloted_schools) > 0)
          {
            $row1['alloted'] = "true";
            $row1['quantity'] = $alloted_schools_quantity[$row->id];
          }
          else
          {
            $row1['alloted'] = "false";
            $row1['quantity'] = 1;
          }
          $row_set[] = $row1;
        }
      }
      echo json_encode($row_set); //format the array into json data
    }    
  }
  
  function get_blocks()
  { 
    $manager = (isset($_GET['manager']) ? $_GET['manager'] : '');
    $role = (isset($_GET['role']) ? $_GET['role'] : '');
    $district = $_GET['district'];
  
    $district_detail = $this->districts_model->getById($district);
    $state_id = $district_detail['state_id'];
    
    $state_field_user_ids = array();
    $alloted_block_ids = array();
    if($role == 'manager' and ($manager != "" and $manager != 0)){
      $state_field_users = $this->db->select('id')->where(array('state_id'=>$state_id, "role"=>$role))->where_not_in('id', $manager)->get('users')->result();
    }else{
      $state_field_users = $this->db->select('id')->where(array('state_id'=>$state_id, "role"=>$role))->get('users')->result();
    }
    foreach($state_field_users as $state_field_user)
      array_push($state_field_user_ids,$state_field_user->id);
    
    if (count($state_field_user_ids) > 0)
    {
      if(isset($_GET['user_id'])){
        $alloted_blocks = $this->db->select('block_id')->where_in('user_id',$state_field_user_ids)->where_not_in('user_id', $_GET['user_id'])->get('spark_users_district_block')->result();
      }else{
        $alloted_blocks = $this->db->select('block_id')->where_in('user_id',$state_field_user_ids)->get('spark_users_district_block')->result();
      }
      
      foreach($alloted_blocks as $alloted_block)
        array_push($alloted_block_ids,$alloted_block->block_id);
    }
    
    //print_r($alloted_block_ids);
    if ($manager != "" and $manager != 0){
      $user = $this->db->select('role')->where('id',$manager)->get('users')->row();
      if($user->role == 'state_person'){
        $this->load->model('blocks_model');  
        $this->load->model('schools_model');  
        $district = $_GET['district'];
        $blocks = $this->blocks_model->get_district_blocks($district, "");
      }else{
        $blocks = $this->team_members_model->get_member_blocks($manager,$district);
      }
    }
    else
    {
      $this->load->model('blocks_model');  
      $this->load->model('schools_model');  
      $district = $_GET['district'];
      $blocks = $this->blocks_model->get_district_blocks($district, "");
    }
    
    $limited = "all";
    $row_set = array();
    if(count($blocks) > 0){
      foreach ($blocks as $row){
        //echo " -- ".$row->id;
        if (!in_array($row->id,$alloted_block_ids))
        {
          $value = "";
          $row1['value'] = $value.ucwords(htmlentities(stripslashes($row->name)));
          $row1['id'] = $row->id;
          $row_set[] = $row1;
        }
      }
      echo json_encode($row_set);  //format the array into json data
    }    
  }
  
  function get_clusters()
  {
    $this->load->model('clusters_model');  
    $this->load->model('schools_model');  
    $block = $_GET['block'];
    $clusters = $this->clusters_model->get_block_clusters($block, "");
    $limited = "all";
    if(count($clusters) > 0){
      foreach ($clusters as $row){
        $school_count = $this->schools_model->get_school_count(0, 0, 0, $row->id);
        $value = "";
        if ($row->dise_code != "")
          $value = "(".$row->dise_code.") ";
        $row1['school_count'] = $school_count;
        $row1['value'] = $value.ucwords(htmlentities(stripslashes($row->name)));
        $row1['id'] = $row->id;
        $row_set[] = $row1;
      }
      echo json_encode($row_set); //format the array into json data
    }    
  }

  function get_districts_new()
  {  
    $state = $_GET['state'];
    $manager = $_GET['manager'];
    $curuser = $_GET['curuser'];
    $manager_role = $this->db->select('role')->where('id', $manager)->get('sparks')->row();
    if($manager_role->role != 'state_person'){
      $user = $this->db->select('role')->where('id',$manager)->get('users')->row();
      if($user->role == 'state_person'){
        $this->load->model('districts_model');
        $districts = $this->districts_model->get_state_districts($state);
      }else{
        $districts = $this->team_members_model->get_member_districts($manager);
      }
    }
    else
    { 
      $this->load->model('districts_model');
      $districts = $this->districts_model->get_state_districts($state);
    }
    //print_r($districts);exit;
    $limited = "all";
    if(count($districts) > 0){
      foreach ($districts as $row){
        if ($limited == "1")
        {
          if (in_array($row->id,$allowed_disricts))
          {
            $value = "";
            $row1['value'] = $value.ucwords(htmlentities(stripslashes($row->name)));
            $row1['id'] = $row->id;
            $row_set[] = $row1;
          }
        }
        else
        {
          $value = "";
          $row1['value'] = $value.ucwords(htmlentities(stripslashes($row->name)));
          $row1['id'] = $row->id;
          $row_set[] = $row1;
        }
      }
      echo json_encode($row_set); die;//format the array into json data
    }    
  }

  function get_blocks_new()
  { 
    $manager = $_GET['manager'];
    $district = $_GET['district'];
    $role = $_GET['role'];
    $cur_user = (isset($_GET['user_id']) and isset($_GET['user_id']) > 0) ? $_GET['user_id'] : 0;

    $district_detail = $this->districts_model->getById($district);
    $state_id = $district_detail['state_id'];
    
    $state_field_user_ids = array();
    $alloted_block_ids = array();

    $musers = $this->db->select('id')->where('manager_id', $manager)->get('sparks')->result();
    foreach ($musers as $muser) {
      $res = $this->db->select('block_id, user_id')->where('user_id', $muser->id)->get('spark_users_district_block')->result();
      foreach ($res as $r) {
        if($cur_user != $r->user_id)
          array_push($alloted_block_ids, $r->block_id);
      }
    }
    if($role == 'manager' and ($manager != "" and $manager != 0)){
      $state_field_users = $this->db->select('id')->where(array('state_id'=>$state_id, "role"=>$role))->where_not_in('id', $manager)->get('users')->result();
    }else{
      $state_field_users = $this->db->select('id')->where(array('state_id'=>$state_id, "role"=>$role))->get('users')->result();
    }
    foreach($state_field_users as $state_field_user)
      array_push($state_field_user_ids,$state_field_user->id);
    
    if (count($state_field_user_ids) > 0)
    {
      if(isset($_GET['user_id'])){
        $alloted_blocks = $this->db->select('block_id')->where_in('user_id',$state_field_user_ids)->where_not_in('user_id', $_GET['user_id'])->get('spark_users_district_block')->result();
      }else{
        $alloted_blocks = $this->db->select('block_id')->where_in('user_id',$state_field_user_ids)->get('spark_users_district_block')->result();
      }
      
      foreach($alloted_blocks as $alloted_block)
        array_push($alloted_block_ids,$alloted_block->block_id);
    }

    //print_r($alloted_block_ids);exit;
    
    if ($manager != "" and $manager != 0){
      $user = $this->db->select('role')->where('id',$manager)->get('sparks')->row();
      if($user->role == 'state_person'){
        $this->load->model('blocks_model');  
        $this->load->model('schools_model');  
        $district = $_GET['district'];
        $blocks = $this->blocks_model->get_district_blocks($district, "");
      }else{
        $blocks = $this->team_members_model->get_member_blocks($manager,$district);
      }
    }
    else
    {
      $this->load->model('blocks_model');  
      $this->load->model('schools_model');  
      $district = $_GET['district'];
      $blocks = $this->blocks_model->get_district_blocks($district, "");
    }
    $limited = "all";
    $row_set = array();
    if(count($blocks) > 0){
      foreach ($blocks as $row){
        //echo " -- ".$row->id;
        if (!in_array($row->id,$alloted_block_ids))
        {
          $value = "";
          $row1['value'] = $value.ucwords(htmlentities(stripslashes($row->name)));
          $row1['id'] = $row->id;
          $row_set[] = $row1;
        }
      }
      echo json_encode($row_set);  //format the array into json data
    }    
  }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
