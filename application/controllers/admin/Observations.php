<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Observations extends CI_Controller {

	function __construct()	
	{
		parent::__construct();
		$this->load->helper('url');
    $this->load->helper('application');
		$this->data['current_page'] = 'Observations';
		$this->load->library('form_validation');
		$this->load->library('common_functions');
    $this->load->model('observations_model');
    $this->load->model('users_model');
		$this->load->model('states_model');
		$this->load->model('classes_model');
		$this->load->helper('date');
    
    if($this->session->userdata('admin_logged_in'))
		{
		  $session_data = $this->session->userdata('admin_logged_in');
      $this->data['current_user_id'] = $session_data['id'];
      $this->data['current_username'] = $session_data['username'];
      $this->data['current_name'] = $session_data['name'];
      $this->data['current_role'] = $session_data['role'];
      $this->data['user_state_id'] = $session_data['state_id'];
      $this->data['user_district_id'] = $session_data['district_id'];
      $this->data['current_group'] = $session_data['user_group'];
    }
    else
    {
      //If no session, redirect to login page
      redirect('admin/login', 'refresh');
    }
	}

	public function index()
	{
      $this->data['observations_list'] = $this->observations_model->get_all_observations();
      
      $this->data['partial'] = 'admin/observations/index';
      $this->data['page_title'] = 'observations';
      $this->load->view('templates/index', $this->data);
	}

	public function add($activity_type)
	{
      $this->data['partial'] = 'admin/observations/'.$activity_type;
      $this->data['action'] = 'create';
      $this->data['logged_in_user'] = $this->data['current_user_id'];
      $this->data['page_title'] = 'Add Observation Questions';
      $this->load->model('states_model');
      $this->data['states'] = $this->states_model->get_all_states();
      
      $this->load->model('classes_model');
      $this->data['classes'] = $this->classes_model->get_all();
      
      $this->load->view('templates/index', $this->data);
	}
	
	public function create()
	{
      //print_r($_POST);exit;
      $state_ids = $this->input->post('state_id');
      
      $success = false;
      if(empty($state_ids) || $this->input->post('question_type') == '' || $this->input->post('question') == ''){
        $this->session->set_flashdata('error', 'Please select all required fields.');
        header('location:'.base_url()."admin/observations/");
      }
      foreach($state_ids as $stateid){
        $class_ids = $this->input->post('class_id');
        if(!empty($class_ids)){
          foreach($class_ids as $classid){
            $mdata['activity_type'] = $this->input->post('activity_type');
            $mdata['state_id'] = $stateid;
            $mdata['class_id'] = $classid;
            $mdata['question_type'] = $this->input->post('question_type');
            $mdata['question'] = $this->input->post('question');
            $mdata['subject_wise'] = (isset($_POST['subject_wise']) and $_POST['subject_wise'] == "yes" ? 1 : 0);
            $mdata['productivity_flag'] = (isset($_POST['productivity_flag']) and $_POST['productivity_flag'] == "yes" ? 1 : 0);
            $mdata['whatsapp_flag'] = (isset($_POST['whatsapp_flag']) and $_POST['whatsapp_flag'] == "yes" ? 1 : 0);
            $mdata['report_flag'] = (!empty($_POST['report_flag']) ? $_POST['report_flag'] : 0);
            $mdata['weightage'] = (!empty($_POST['weightage']) ? $_POST['weightage'] : 0);
            $mdata['created_by'] = $this->data['current_user_id'];
            $mdata['created_at'] = date('Y-m-d');
            $mdata['status'] = 1;
            $observation_id = $this->observations_model->add($mdata);
            
            if ($mdata['question_type'] == "objective")
            {
              foreach($_POST['options'] as $option)
              {
                if ($option != "")
                {
                  $option_data['question_id'] = $observation_id;
                  $option_data['option_value'] = $option;
                  $option_data['status'] = 1;
                  $this->observations_model->add_option($option_data);
                }
              }
            }
          }
        }
        else{
            $mdata['activity_type'] = $this->input->post('activity_type');
            $mdata['state_id'] = $stateid;
            $mdata['class_id'] = 0;
            $mdata['question_type'] = $this->input->post('question_type');
            $mdata['question'] = $this->input->post('question');
            $mdata['subject_wise'] = (isset($_POST['subject_wise']) and $_POST['subject_wise'] == "yes" ? 1 : 0);
            $mdata['productivity_flag'] = (isset($_POST['productivity_flag']) and $_POST['productivity_flag'] == "yes" ? 1 : 0);
            $mdata['whatsapp_flag'] = (isset($_POST['whatsapp_flag']) and $_POST['whatsapp_flag'] == "yes" ? 1 : 0);
            $mdata['weightage'] = (!empty($_POST['weightage']) ? $_POST['weightage'] : 0);
            $mdata['report_flag'] = (!empty($_POST['report_flag']) ? $_POST['report_flag'] : 0);
            $mdata['created_by'] = $this->data['current_user_id'];
            $mdata['created_at'] = date('Y-m-d');
            $mdata['status'] = 1;
            $observation_id = $this->observations_model->add($mdata);
            if ($mdata['question_type'] == "objective")
            {
              foreach($_POST['options'] as $option)
              {
                if ($option != "")
                {
                  $option_data['question_id'] = $observation_id;
                  $option_data['option_value'] = $option;
                  $option_data['status'] = 1;
                  $this->observations_model->add_option($option_data);
                }
              }
            }
        }
      }

      //if($observation_id > 0){
        $this->session->set_flashdata('success', 'Observation question added successfully.');
        header('location:'.base_url()."admin/observations/");
      /*}else{
        $this->session->set_flashdata('alert', 'Observation id already exist.');
        redirect('sparks', 'refresh');
      }*/
	}

	public function edit()
	{
      $id = $this->uri->segment(4);
      $this->data['id'] = $id;
      $this->data['action'] = 'update';
      $observation = $this->observations_model->getById($id);
      $observation_options = $this->observations_model->getObtionsByObservation($id);
      $this->data['observations'] = $observation;
      $this->data['observation_options'] = $observation_options;
      
      $this->load->model('states_model');
      $this->data['states'] = $this->states_model->get_all_states();
      
      $this->load->model('classes_model');
      $this->data['classes'] = $this->classes_model->get_all();
      
      $this->data['logged_in_user'] = $this->data['current_user_id'];
      $this->data['page_title'] = 'Edit User';
      $this->data['partial'] = 'admin/observations/'.$observation['activity_type'];
      $this->load->view('templates/index', $this->data);
	}
  
	public function update()
	{
      $id = $_POST['id'];
      $observation = $this->observations_model->getById($id);
      //$mdata['activity_type'] = $this->input->post('activity_type');
      //$mdata['state_id'] = $_POST['state_id'][0];
      if ($this->input->post('activity_type') == "school_visit")
      {
        $mdata['class_id'] = $_POST['class_id'][0];
      }
      //$mdata['question_type'] = $this->input->post('question_type');
      $question_type = $this->input->post('question_type');
      $mdata['question'] = $this->input->post('question');
      $mdata['subject_wise'] = (isset($_POST['subject_wise']) and $_POST['subject_wise'] == "yes" ? 1 : 0);
      $mdata['productivity_flag'] = (isset($_POST['productivity_flag']) and $_POST['productivity_flag'] == "yes" ? 1 : 0);
      $mdata['whatsapp_flag'] = (isset($_POST['whatsapp_flag']) and $_POST['whatsapp_flag'] == "yes" ? 1 : 0);
      $mdata['weightage'] = (!empty($_POST['weightage']) ? $_POST['weightage'] : 0);  
      $mdata['report_flag'] = (!empty($_POST['report_flag']) ? $_POST['report_flag'] : 0);
      $mdata['created_by'] = $this->data['current_user_id'];
      //$mdata['created_at'] = date('Y-m-d');
      //$mdata['status'] = 0;
      $this->observations_model->update_info($mdata, $_POST['id']);
      if ($observation['question_type'] == "objective")
      {
        $updated_options = array();
        foreach($_POST['options'] as $key=>$option)
        {
          if ($option != "")
          {
            if (isset($_POST['optionids']) && isset($_POST['optionids'][$key]))
            {
              $option_id = $_POST['optionids'][$key];
              $option_data['question_id'] = $_POST['id'];
              $option_data['option_value'] = $option;
              $option_data['status'] = 1;
              $this->observations_model->edit_option($option_data,$option_id);
            }
            else
            {
              $option_data['question_id'] = $_POST['id'];
              $option_data['option_value'] = $option;
              $option_data['status'] = 1;
              $option_id = $this->observations_model->add_option($option_data);
            }
            array_push($updated_options,$option_id);
            
            if (count($updated_options) > 0)
              $this->observations_model->disable_options($_POST['id'],$updated_options);
          }
        }
      }
      header('location:'.base_url()."admin/observations/".$this->index());
	}
				  
	public function delete($id)
	{
      //$this->observations_model->delete_a_observation($id);
      header('location:'.base_url()."admin/observations".$this->index());
	}
  
  public function disableall()
  {
      $ids = explode(",",$_POST['observations']);
      $this->observations_model->disable_observations($ids);
      header('location:'.base_url()."admin/observations".$this->index());
  } 

  public function changestatus()
  {
    $id = $this->uri->segment(4);
    $this->data['action'] = 'update';
    $banner = $this->observations_model->getById($id);
    //print_r($this->data['newsletter']);
    extract ($banner);
    if( $status==1){
      $new_status=0;
    }
    else
    {
      $new_status="1";
    }
    $mdata['status'] = $new_status;
    $res=$this->observations_model->update_info($mdata, $id);
    if($res)
    {
      header('location:'.base_url()."admin/observations".$this->index());
    }
  }

  public function check_user_id(){
    $res = $this->observations_model->check_user_id($_POST['user_id']);
    if($res == 0){
      echo "Observation id already exists.";
    }else{
      echo '';
    }
  }
  
  public function sorting_observations()
	{
      $activity_type = unserialize(ACTIVITY_ARRAY);
      
      $this->data['activities'] = $activity_type;
      $this->data['partial'] = 'admin/observations/sorting_observations';
      $this->data['logged_in_user'] = $this->data['current_user_id'];
      $this->data['page_title'] = 'Add Observation Sorting';
      $this->load->model('states_model');
      $this->data['states'] = $this->states_model->get_all_states();

      if(!empty($_POST) && !empty($_POST['state_id']) && !empty($_POST['type']))
      {
        $this->load->model('observations_model');
        $state_id = $_POST['state_id']; 
        $type = $_POST['type']; 
        
        $this->data['state_id'] = $state_id;
        $this->data['type'] = $type;
      
        if($_POST['submit'] == 'Manage Sorting')
        {
          $observations = $_POST['sort_order'];
          
          foreach($observations as $qid=>$value)
          {
            $data['sort_order'] = ($value[0] != '' ? $value[0] : 0);
            $this->load->model('observations_model');
            $this->observations_model->edit($data, $qid);
          }
        }
        $this->data['observations_list'] = $this->observations_model->get_all_observations(0, $state_id, $type, 'class_id asc, sort_order asc', 1);
      }
      $this->load->view('templates/index', $this->data);
	}
  
  function observations_updated()
  {
    $this->load->Model('Sparks_model');
    $data['observation_flag'] = 1;
    $this->Sparks_model->update_info($data);
    header('location:'.base_url()."admin/observations".$this->index());  
  }

}
