<?php

class Calendar extends CI_Controller  {
  function __construct() {
    parent::__construct();
	
    $this->load->helper(array('form', 'url'));
    $this->load->model('subjects_model');
    $this->load->model('classes_model');
    $this->load->model('calendar_model');
    $this->load->model('users_model');
    $this->load->library('form_validation');
  
    $this->data['current_page'] = 'calendar';
    $this->load->library('common_functions'); 
    $this->load->library('ckeditor');
    $this->load->library('ckfinder');
    $this->ckeditor->basePath = base_url().'assets/admin/ckeditor/';
    $this->ckeditor->config['toolbar'] ='Full'; 
    $this->ckeditor->config['language'] = 'en';
    $this->ckeditor->config['width'] = '100%';
    $this->ckeditor->config['height'] = '300px';   
    
		if($this->session->userdata('admin_logged_in'))
		{
		  $session_data = $this->session->userdata('admin_logged_in');
      $this->data['id'] = $session_data['id'];
      $this->data['current_username'] = $session_data['username'];
      $this->data['current_name'] = $session_data['name'];
      $this->data['current_role'] = $session_data['role'];
      $this->data['user_state_id'] = $session_data['state_id'];
      $this->data['user_district_id'] = $session_data['district_id'];
    }
		else
		{
      redirect('admin/login', 'refresh');
		}		        
  }

  function index() 
  {
    $this->data['calendar_list'] = $this->calendar_model->get_all_calendar();
    $this->data['partial'] = 'admin/calendar/index';
    $this->data['page_title'] = 'Calendar';
    $this->load->view('templates/index', $this->data);
    //$this->load->view('admin/admins/index', array('error' => ' ' ));
  }
  
  public function add()
  {	
    $this->data['users'] = $this->users_model->get_all_users();
    $this->data['partial'] = 'admin/calendar/form';
    $this->data['action'] = 'create';
    $this->data['page_title'] = 'Add Calendar';
    $this->load->view('templates/index', $this->data);		
  }

  function create() 
  {
    $this->load->library('form_validation');
    $this->form_validation->set_rules('user_id', 'User', 'required');
    $this->form_validation->set_rules('calendar_date', 'Calendar Date', 'required');
    $this->form_validation->set_rules('task', 'Task', 'required');
    
    $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
    if ($this->form_validation->run() == FALSE)
    { 
      $this->data['partial'] = 'admin/calendar/form';
      $this->data['action'] = 'create';
      $this->data['page_title'] = 'Add Calendar';
      $this->load->view('templates/index', $this->data);
    }
    else
    {
      $mdata['user_id'] = $this->input->post('user_id');
      $mdata['task'] = $this->input->post('task');
      $d = array_reverse(explode("-",$this->input->post('calendar_date')));
      $mdata['calendar_date'] = implode("-",$d);
      $res= $this->db->insert('calendar', $mdata);
      //redirect code
      if($res)
      {
        header('location:'.base_url()."admin/calendar/".$this->index());
      }
    }
  }

  public function edit()
  {
		$id = $this->uri->segment(4);
		$this->data['calendar'] = $this->calendar_model->getById($id);
		$this->data['partial'] = 'admin/calendar/form';
		$this->data['action'] = 'update';
		$this->data['page_title'] = 'Edit Calendar';
		$this->load->view('templates/index', $this->data);		
  }

  function update() 
  {	
		$this->load->library('form_validation');
    $this->form_validation->set_rules('user_id', 'User', 'required');
    $this->form_validation->set_rules('calendar_date', 'Calendar Date', 'required');
    $this->form_validation->set_rules('task', 'Task', 'required');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		if ($this->form_validation->run() == FALSE)
    {
      $id = $this->uri->segment(4);
      $this->data['admins'] = $this->calendar_model->getById($id);
      $this->data['partial'] = 'admin/calendar/form';
      $this->data['action'] = 'update';
      $this->data['page_title'] = 'Edit Calendar';
      $this->load->view('templates/index', $this->data);	
    }
    else
    {
      $mdata['user_id'] = $this->input->post('user_id');
      $mdata['task'] = $this->input->post('task');
      $d = array_reverse(explode("-",$this->input->post('calendar_date')));
      $mdata['calendar_date'] = implode("-",$d);
      $res=$this->calendar_model->update_info($mdata, $_POST['id']);
      if($res)
      {
        header('location:'.base_url()."admin/calendar".$this->index());
      }
    }
  }
  
  public function delete($id)
  {
    $admins = $this->calendar_model->getById($id);
    $this->calendar_model->delete_a_calendar($id);
    header('location:'.base_url()."admin/calendar".$this->index());
  }
  
  public function changestatus()
  {
    $id = $this->uri->segment(4);
    $this->data['action'] = 'update';
    $banner = $this->admins_model->getById($id);
    //print_r($this->data['newsletter']);
    extract ($banner);
    if( $status==1){
      $new_status=0;
    }
    else
    {
      $new_status="1";
    }
    $mdata['status'] = $new_status;
    $res=$this->admins_model->update_info($mdata, $id);
    if($res)
    {
      header('location:'.base_url()."admin/admins".$this->index());
    }
  }
}
