<?php
class Travels extends CI_Controller  {
  function __construct() {
    parent::__construct();
	
    $this->load->helper(array('form', 'url'));
    $this->load->model('travels_model');
    $this->load->library('form_validation');
  
    $this->data['current_page'] = 'travels';
    $this->load->library('common_functions'); 
    
		if($this->session->userdata('admin_logged_in'))
		{
		  $session_data = $this->session->userdata('admin_logged_in');
      $this->data['user_id'] = $session_data['id'];
      $this->data['current_user_id'] = $session_data['id'];
      $this->data['current_username'] = $session_data['username'];
      $this->data['current_name'] = $session_data['name'];
      $this->data['current_role'] = $session_data['role'];
      $this->data['user_state_id'] = $session_data['state_id'];
      $this->data['user_district_id'] = $session_data['district_id'];
      
      $this->data['travel_roles'] = unserialize(TREVEL_ROLES);
    }
		else
		{
      redirect('admin/login', 'refresh');
		}		        
  }

  function index() 
  {
    $this->data['travels_list'] = $this->travels_model->get_all();
    $this->data['partial'] = 'admin/travels/index';
    $this->data['page_title'] = 'Travels';
    $this->load->view('templates/index', $this->data);
    //$this->load->view('admin/admins/index', array('error' => ' ' ));
  }

  public function save(){
    $data = array('name'=> $_POST['name']);
    $this->travels_model->insert_travels_to_db($data);
    redirect('admin/travels', 'refresh');
  }

  public function add()
  {	
    $this->data['partial'] = 'admin/travels/form';
    $this->data['action'] = 'create';
    $this->data['page_title'] = 'Add Holiday';
    $this->load->view('templates/index', $this->data);		
  }

  function create() 
  {	
    $this->load->model('travels_model');
    
    $travel_mode = $this->input->post('travel_mode');
    $cdata = $this->travels_model->getByName($travel_mode);
    
    if(empty($cdata)){
			
      $mdata['travel_mode'] = $travel_mode;
      $is_autocalculated = $this->input->post('is_autocalculated');
      $roles = implode(",", $this->input->post('role'));
      if(!empty($is_autocalculated)){
        $mdata['travel_cost'] = $this->input->post('travel_cost');
      }else{
        $mdata['travel_cost'] = 0;
        $mdata['is_autocalculated'] = 0;
      }
      $mdata['role'] = $roles;
      $mdata['created_on'] = date('Y-m-d H:i:s');
      $mdata['created_by'] = $this->data['current_user_id'];
      $travel_id = $this->travels_model->add($mdata);
      header('location:'.base_url()."admin/travels/".$this->index());
    }
    else{
      $this->session->set_flashdata('alert', 'Travel mode already exists.');
      header('location:'.base_url()."admin/travels/add");
    }
  }

  public function edit()
  {
    $id = $this->uri->segment(4);
		
    $travels = $this->travels_model->getById($id);
    
    $this->data['travels'] = $travels;
		$this->data['partial'] = 'admin/travels/form';
		$this->data['action'] = 'update';
		$this->data['page_title'] = 'Edit Travel Mode';
		$this->load->view('templates/index', $this->data);		
  }

  function update() 
  {	
		$this->load->model('travels_model');
    
    $travel_mode = $this->input->post('travel_mode');
    $cdata = $this->travels_model->getByOtherName($_POST['id'], $travel_mode);
    
    if(empty($cdata)){
      $mdata['travel_mode'] = $travel_mode;
      $is_autocalculated = $this->input->post('is_autocalculated');
      $roles = implode(",", $this->input->post('role'));
      if(!empty($is_autocalculated)){
        $mdata['travel_cost'] = $this->input->post('travel_cost');
        $mdata['is_autocalculated'] = 1;
      }else{
        $mdata['travel_cost'] = 0;
        $mdata['is_autocalculated'] = 0;
      }
      $mdata['role'] = $roles;
      $mdata['updated_on'] = date('Y-m-d H:i:s');
      $mdata['updated_by'] = $this->data['current_user_id'];
      $res = $this->travels_model->update_travel($_POST['id'], $mdata);
      header('location:'.base_url()."admin/travels".$this->index());
    }
    else{
      $this->session->set_flashdata('alert', 'Travel mode already exists.');
      header('location:'.base_url()."admin/travels/edit/".$_POST['id']);
    }
  }
  
  public function delete($id)
  {
    $this->travels_model->delete_a_travel($id);
    header('location:'.base_url()."admin/travels".$this->index());
  }
  
  public function changestatus()
  {
    $id = $this->uri->segment(4);
    $this->data['action'] = 'update';
    $banner = $this->travels_model->getById($id);
    extract ($banner);
    if( $status==1){
      $new_status=0;
    }
    else
    {
      $new_status="1";
    }
    $mdata['status'] = $new_status;
    $mdata['updated_on'] = date('Y-m-d H:i:s');
    $mdata['updated_by'] = $this->data['current_user_id'];
    $res=$this->travels_model->update_travel($id, $mdata);
    header('location:'.base_url()."admin/travels".$this->index());
  }

}
