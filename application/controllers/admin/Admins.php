<?php

class Admins extends CI_Controller  {
  function __construct() {
    parent::__construct();
	
    $this->load->helper(array('form', 'url'));
    $this->load->model('admins_model');
    $this->load->library('form_validation');
    $this->data['current_page'] = 'admins';
    $this->load->library('common_functions'); 
    $this->load->library('ckeditor');
    $this->load->library('ckfinder');
    $this->data['current_page'] = 'admins';
    $this->ckeditor->basePath = base_url().'assets/admin/ckeditor/';
    $this->ckeditor->config['toolbar'] ='Full'; 
    $this->ckeditor->config['language'] = 'en';
    $this->ckeditor->config['width'] = '100%';
    $this->ckeditor->config['height'] = '300px';           
  }



  function index() 
  {
	  
    $this->data['admins_list'] = $this->admins_model->get_all_admins();
    $this->data['partial'] = 'admin/admins/index';
    $this->data['page_title'] = 'admins';
    $this->load->view('templates/index', $this->data);
    //$this->load->view('admin/admins/index', array('error' => ' ' ));
  }
  function get_admins()
	 {
		$this->load->model('admins_model');
		if (isset($_GET['term'])){
		$q = strtolower($_GET['term']);
		$this->admins_model->get_admins($q);
		}
exit();
	
  }

  public function add()
  {	
    $this->data['partial'] = 'admin/admins/form';
    $this->data['action'] = 'create';
    $this->data['page_title'] = 'Add Page';
    $this->load->view('templates/index', $this->data);		
  }

  function create() 
  {		
  
	$this->load->library('form_validation');
	$this->form_validation->set_rules('username', 'Username', 'required');
	$this->form_validation->set_rules('password', 'Password ', 'required');
	$this->form_validation->set_rules('email', 'Email ', 'required');
	$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
	if ($this->form_validation->run() == FALSE)
	{
		$this->data['partial'] = 'admin/admins/form';
		$this->data['action'] = 'create';
		$this->data['page_title'] = 'Add Page';
		$this->load->view('templates/index', $this->data);	
	}
	else
	{
		$mdata['username'] = $this->input->post('username');
		$mdata['password'] = $this->input->post('password');
		$mdata['email'] = $this->input->post('email');
		$mdata['role'] = $this->input->post('role');
		$mdata['status'] = 1;
		$res= $this->db->insert('admins', $mdata);
		//redirect code
		if($res)
		{
			header('location:'.base_url()."admin/admins/".$this->index());
		}
	}
  }

  public function edit()
  {

		$id = $this->uri->segment(4);
		$this->data['admins'] = $this->admins_model->getById($id);
		$this->data['partial'] = 'admin/admins/form';
		$this->data['action'] = 'update';
		$this->data['page_title'] = 'Edit admins';
		$this->load->view('templates/index', $this->data);		
  }

  function update() 
  {	
		$this->load->library('form_validation');
		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('password', 'Password ', 'required');
		$this->form_validation->set_rules('email', 'Email ', 'required');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		if ($this->form_validation->run() == FALSE)
			{
			
				
				$id = $this->uri->segment(4);
				$this->data['admins'] = $this->admins_model->getById($id);
				
				$this->data['partial'] = 'admin/admins/form';
				$this->data['action'] = 'update';
				$this->data['page_title'] = 'Edit admins';
				$this->load->view('templates/index', $this->data);	
			}
			else
			{
				$mdata['username'] = $this->input->post('username');
				$mdata['password'] = $this->input->post('password');
				$mdata['email'] = $this->input->post('email');
				$mdata['role'] = $this->input->post('role');
				$res=$this->admins_model->update_info($mdata, $_POST['id']);
			if($res)
			{
			  header('location:'.base_url()."admin/admins".$this->index());
			}
		  }
  }

 
  
  public function delete($id)
  {
    $admins = $this->admins_model->getById($id);

    $this->admins_model->delete_a_admins($id);
    header('location:'.base_url()."admin/admins".$this->index());
  }
  
  public function changestatus()
  {
    $id = $this->uri->segment(4);
    $this->data['action'] = 'update';
    $banner = $this->admins_model->getById($id);
    //print_r($this->data['newsletter']);
    extract ($banner);
    if( $status==1){
      $new_status=0;
    }
    else
    {
      $new_status="1";
    }
    $mdata['status'] = $new_status;
    $res=$this->admins_model->update_info($mdata, $id);
    if($res)
    {
      header('location:'.base_url()."admin/admins".$this->index());
    }
  }
 
}
