<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Activity_map extends CI_Controller {	 
  
  function __construct()  {    
    parent::__construct();
    $this->output->enable_profiler(FALSE);    
    $this->load->helper('url');    
    $this->load->model('users_model'); 
		$this->load->library('Common_functions');
	
    if($this->session->userdata('logged_in'))
		{
      $session_data = $this->session->userdata('logged_in');
      $this->data['current_user_id'] = $session_data['id'];
      $this->data['current_username'] = $session_data['username'];
      $this->data['current_name'] = $session_data['name'];
      $this->data['current_role'] = $session_data['role'];
      $this->data['user_state_id'] = $session_data['state_id'];
      $this->data['user_district_id'] = $session_data['district_id'];
		  $this->data['login_id'] = $session_data['login_id'];
		  $this->data['current_group'] = $session_data['user_group'];
    }
    else
    {
      //If no session, redirect to login page
      redirect('login', 'refresh');
    }
    $current_date=date('d');
    $lstedate=$current_date-4;
    $todaydate = Array();
    for($i=$current_date; $i>=$lstedate; $i--){
      //echo $i;
       array_push($todaydate,$i);
    }
    $this->data['todaydate'] = $todaydate;
  }		

  public function index()		
  {				   
    $activity_type = '';
    $spark_id = $this->data['current_user_id'];
    $state_id = 0;
    $spark_state_id = 0;
    $trackings = array();
    
    if($this->data['current_role'] == 'state_person' || $this->data['current_role'] == 'manager'){
      $spark_state_id = $this->data['user_state_id'];
    }  
    if(isset($_GET['activity_type'])){
      
      if(isset($_GET['spark_id']) && $_GET['spark_id'] > 0)
      {
        $spark_state_id = (isset($_GET['spark_state_id']) ? $_GET['spark_state_id'] : 0);
        $spark_detail = $_GET['spark_id'];
        $spark_details = explode("-",$spark_detail);
        $spark_id = $spark_details[0];
      }
      
      if(isset($_GET['activity_type']) && $_GET['activity_type'] != '')
      {
        $activity_type = $_GET['activity_type'];
      }
      
      $this->load->model('tracking_model'); 
      $activities = $this->tracking_model->get_activity_list($spark_id, $activity_type);
      
      $random_tracking = array('random-tracking-1','random-tracking-2','random-tracking-3');
      
      foreach($activities as $activity)
      {
        if(in_array($activity->activity_type, $random_tracking)){
          $activity->activity_type = 'random-tracking';
        }
        $type_detail = $activity->activity_type;
        $string = str_replace("\n", "", $activity->tracking_location);
        $activity->tracking_location = str_replace("\r", "", $string);
        switch($type_detail)
        {
          case 'schoolvisit':
                $tracking = array();
                $tracking['activity'] = 'School Visit';
                $tracking['type'] = 'schoolvisit';
                $tracking['spark_id'] = $activity->spark_id;
                $tracking['activity_id'] = $activity->activity_id;
                $tracking['time'] = date("d-m-Y H:i",strtotime($activity->tracking_time));
                $tracking['latitude'] = $activity->tracking_latitude;
                $tracking['longitude'] = $activity->tracking_longitude;
                $tracking['location'] = $activity->tracking_location;
                $tracking['distance'] = $activity->distance_val;
                $tracking['marker'] = 'schoolvisit.png';
                array_push($trackings,$tracking);
                break;
          case 'review_meeting':
                $tracking = array();
                $tracking['activity'] = 'Meeting';
                $tracking['type'] = 'review_meeting';
                $tracking['spark_id'] = $activity->spark_id;
                $tracking['activity_id'] = $activity->activity_id;
                $tracking['time'] = date("d-m-Y H:i",strtotime($activity->tracking_time));
                $tracking['latitude'] = $activity->tracking_latitude;
                $tracking['longitude'] = $activity->tracking_longitude;
                $tracking['location'] = $activity->tracking_location;
                $tracking['distance'] = $activity->distance_val;
                $tracking['marker'] = 'meeting.png';
                array_push($trackings,$tracking);
                break;
          case 'training':
                $tracking = array();
                $tracking['activity'] = 'Training';
                $tracking['type'] = 'training';
                $tracking['spark_id'] = $activity->spark_id;
                $tracking['activity_id'] = $activity->activity_id;
                $tracking['time'] = date("d-m-Y H:i",strtotime($activity->tracking_time));
                $tracking['latitude'] = $activity->tracking_latitude;
                $tracking['longitude'] = $activity->tracking_longitude;
                $tracking['location'] = $activity->tracking_location;
                $tracking['distance'] = $activity->distance_val;
                $tracking['marker'] = 'training.png';
                array_push($trackings,$tracking);
                break;
          case 'distance_travel':
                $tracking = array();
                $tracking['activity'] = 'Distance-Travel';
                $tracking['type'] = 'distance_travel';
                $tracking['spark_id'] = $activity->spark_id;
                $tracking['activity_id'] = $activity->activity_id;
                $tracking['time'] = date("d-m-Y H:i",strtotime($activity->tracking_time));
                $tracking['latitude'] = $activity->tracking_latitude;
                $tracking['longitude'] = $activity->tracking_longitude;
                $tracking['location'] = $activity->tracking_location;
                $tracking['distance'] = $activity->distance_val;
                $tracking['marker'] = 'longdistance.png';
                array_push($trackings,$tracking);
                break;      
          case 'random-tracking':
                $tracking = array();
                $tracking['activity'] = 'Random-Tracking';
                $tracking['type'] = 'random-tracking';
                $tracking['spark_id'] = $activity->spark_id;
                $tracking['activity_id'] = '';
                $tracking['time'] = date("d-m-Y H:i",strtotime($activity->tracking_time));
                $tracking['latitude'] = $activity->tracking_latitude;
                $tracking['longitude'] = $activity->tracking_longitude;
                $tracking['location'] = $activity->tracking_location;
                $tracking['distance'] = '';
                $tracking['marker'] = 'randomtracking.png';
                array_push($trackings,$tracking);
                break;
           case 'attendance-in':
                $tracking = array();
                $tracking['activity'] = 'Attendance-IN';
                $tracking['type'] = 'attendance-in';
                $tracking['spark_id'] = $activity->spark_id;
                $tracking['activity_id'] = '';
                $tracking['time'] = date("d-m-Y H:i",strtotime($activity->tracking_time));
                $tracking['latitude'] = $activity->tracking_latitude;
                $tracking['longitude'] = $activity->tracking_longitude;
                $tracking['location'] = $activity->tracking_location;
                $tracking['distance'] = $activity->distance_val;
                $tracking['marker'] = 'attendance_in.png';
                array_push($trackings,$tracking);
        }
      }
    }  
    
    $this->data['activities'] = $trackings;
    $google_map_api = "https://maps.googleapis.com/maps/api/js?key=".GOOGLE_API_KEY_MAP."&callback=initMap";
    //$google_map_api = "https://maps.googleapis.com/maps/api/js?key=".GOOGLE_API_KEY;
    $custom_styles = []; 
    $custom_scripts = [$google_map_api, 'map.js']; 
    //$custom_scripts = [$google_map_api,'googlemap1.js?ver=1','map.js']; 
    $this->data['current_role'] = $this->data['current_role'];     
    $this->data['custom_scripts'] = $custom_scripts;     
    $this->data['custom_styles'] = $custom_styles;
    $this->data['activity_type'] = $activity_type;
    $this->data['spark_id'] = $spark_id;
    $this->data['spark_state_id'] = $spark_state_id;
    $this->data['partial'] = 'activity_map/index';			
    $this->load->view('templates/index', $this->data);
  }

}

