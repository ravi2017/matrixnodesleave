<?php

error_reporting(0);
require APPPATH . '/libraries/REST_Controller.php';
header("Content-Type: application/json;charset=utf-8");

class Accounts extends REST_Controller {

    function __construct() {
        parent::__construct();
        //Enable profiler
        //$this->output->enable_profiler(true);
        $method = $this->router->fetch_method();
        $this->user_id = 0;
        $this->app_version_error_message = '';
        $this->user_state_id = 0;
        $this->user_district_id = 0;
        $this->local_id = 0;
        error_log("---------------------------inside construct event 1 ", 0);
            //-------------------------
        $this->load->helper('file');
        $json = json_encode($this->request->body);
        $file = fopen(FCPATH.'/uploads/json-'.date("d-m-Y").'.txt',"a+");
        fwrite($file,"\n\n-------------------".$method."-------".date("H:i")."-----------\n".$json);
        fclose($file);
        
        $this->api_version_no = 0;
        $version_status = $this->app_version_check();
        if ($version_status == false)
        {
//          $response = array("http_code" => 401, 'delete_local' => '1', 'status' => 'failed', 'sync_error' => true, 'message' => $this->app_version_error_message);
//          $this->response($response);
        }

            $json = $this->request->body;
         if (isset($json['Data']) and !isset($json['Data']['app_version']))
{
$this->new_app_message = 'New version 5.5 of "Spark Activity" application is live in play store. Please update the application from play store before further use.';
if ($method == "myperformance.json")
{
       $response = array("http_code" => 503, 'delete_local' => '1', 'status' => 'failed', 'sync_error' => true, 'message' => $this->new_app_message);
          $this->response($response);
}
if ($method == "tracking.json" || $method == "other_activity.json" || $method == "school_visit.json" || $method == "meeting.json" || $method == "training.json")
{
$this->new_app_message  = 'Update application version to 5.5';
if (isset($json['Data']['local_id']))
{
       $response = array("http_code" => 200,  'local_id'=>$json['Data']['local_id'], 'delete_local' => '1', 'status' => 'failed', 'sync_error' => true, 'message' => $this->new_app_message);
          $this->response($response);
}
if (isset($json['Data']['local_event_id']))
{
       $response = array("http_code" => 200,  'local_event_id'=>$json['Data']['local_event_id'], 'delete_local' => '1', 'status' => 'failed', 'sync_error' => true, 'message' => $this->new_app_message);
          $this->response($response);
}
}

       $response = array("http_code" => 503, 'delete_local' => '1', 'status' => 'failed', 'sync_error' => true, 'message' => $this->new_app_message);
          $this->response($response);
}

        //method login.json
        if (empty($method == 'login.json') && ($method != 'app_version.json') && ($method != 'distict_state_name_list.json') && ($method != 'allclusters.json') && ($method != 'correct_surveys.json')) {
            error_log("inside construct event login.json", 0);
            $this->load->model('users_model');
            $json = $this->request->body;
            
            //--------------------------
            if (empty($json)) {
                error_log("inside construct event login.json JSON empty", 0);
                $this->response(array("http_code" => 400, 'status' => 'failed', 'sync_error' => true, 'message' => 'Unformatted or empty Data'));
            }
            //error_log("inside construct event print json =".print_r($json), 0);
            $user = $json['Data']['user'];
            $auth_token = $json['Data']['auth_token'];
            error_log("inside construct event- sending for validate auth token username=" . $user . " token=" . $auth_token, 0);
            $result = $this->users_model->validate_auth_token($user, $auth_token);
            //error_log(" construct - after validate auth token ", 0);
            if (isset($result) and isset($result['id'])) {
                $this->user_id = $result['id'];
                $this->user_state_id    = $result['state_id'];
                $this->user_district_id = $result['district_id'];
                $this->local_id = $result['login_id'];
                $this->user_name = $result['name'];
                $this->user_email = $result['email'];
                $this->user_mobile = $result['mobile'];
                $this->user_auth_token = $result['auth_token'];
                //Author : Kusum Dhingra for 
                $this->login_id = $result['login_id'];
                $this->role = $result['role'];
                $this->user_group = $result['user_group'];

                $this->observation_flag = $result['observation_flag'];
                $this->test_observation_flag = $result['questions_flag'];
                $this->notification = $result['notification'];
                $app_version = $json['Data']['app_version'];
                $this->load->library('activity_handler');
//		$this->response(array("http_code" => 400, 'status' => 'failed', 'sync_error' => true, 'message' => 'current version : 5.5; your version : '+$app_version));
                
            } else {
                $this->response(array("http_code" => 401, "force_logout" => "1", 'status' => 'failed', 'sync_error' => true, 'message' => 'invalidauthtoken Your permissions has been changed. Kindly Re-Login.'));
            }
        }
        error_log("Account _Construct() ----- exit-----", 0);
    }

    function create_datetime($time)
    {
      $datetime = explode("_",$time);
      $year = substr($datetime[0],0,4);
      $month = substr($datetime[0],4,2);
      $day = substr($datetime[0],6,2);
      $hour = substr($datetime[1],0,2);
      $min = substr($datetime[1],2,2);
      $sec = substr($datetime[1],4,2);
      return $date = date("Y-m-d H:i:s",mktime($hour,$min,$sec,$month,$day,$year));
    }
    
    public function login_post() 
    {
        $this->load->model('states_model');
        $this->load->model('districts_model');
        $this->load->model('blocks_model');
        $this->load->model('classes_model');
        $this->load->model('subjects_model');
        $this->load->model('observations_model');
        $this->load->model('users_model');
        $this->load->model('user_district_blocks_model');
        $json = $this->request->body;
        $version_status = $this->app_version_check();
        if ($version_status == false)
        {
          $response = array("http_code" => 401, 'delete_local' => '1', 'status' => 'failed', 'sync_error' => true, 'message' => $this->app_version_error_message);
          $this->response($response);
        }
        $user = $json['Data']['user'];
        $password = $json['Data']['password'];
        
        if(!isset($json['Data']['android_id'])){
          $this->response(array("http_code" => 401, 'message' => 'Error! You are trying to use old version'));
        }
        
        $device_id = (isset($json['Data']['android_id']) ? $json['Data']['android_id'] : '');
        $result = $this->users_model->login($user, $password);
        if ($result) 
        {
            $sess_array = array();
            
            foreach ($result as $row) 
            {
                $data['auth_token'] = md5(time());
                $data['device_id'] = $device_id;
                $this->users_model->update_info($data, $row->id);
                
                $spark_id = $row->id;
                $device_count = $this->users_model->check_device_info($spark_id, $device_id);
                if($device_count == 0){
                  $deviceData = array();
                  $deviceData['spark_id'] = $spark_id;
                  $deviceData['device_id'] = $device_id;
                
                  $this->users_model->insert_device_info($deviceData);
                }
                $user_districts = array();
                $user_district_blocks = $this->user_district_blocks_model->get_all($row->id);
                foreach($user_district_blocks as $u)
                {
                  if (!array_key_exists($u->district_id,$user_districts))
                  {
                    $user_districts[$u->district_id]['id'] = $u->district_id;
                    $user_districts[$u->district_id]['name'] = $u->district_name;
                    $user_districts[$u->district_id]['blocks'] = [];
                  }
                  $user_districts[$u->district_id]['blocks'][] = array("id"=>$u->block_id, "name"=>$u->block_name);
                }
                $user_districts = array_values($user_districts);
                $spark_id = $row->id;
                $tracking_time = Date("Y-m-d");
                $attendance_in_marked = 0;
                $attendance_out_marked = 0;
                $this->load->model('Tracking_model');
                $attendance = $this->Tracking_model->checkAttendance($spark_id,$tracking_time,'in');
                if (isset($attendance) and isset($attendance['id']) and $attendance['id'] > 0)
                {
                  $attendance_in_marked = 1;
                }
                $attendance = $this->Tracking_model->checkAttendance($spark_id,$tracking_time,'out');
                if (isset($attendance) and isset($attendance['id']) and $attendance['id'] > 0)
                {
                  $attendance_out_marked = 1;
                  //$attendance_in_marked = 0;
                }
                $attendance = $this->Tracking_model->getAttendanceStatus($spark_id,$tracking_time);
                if (isset($attendance) and isset($attendance['id']))
                {
                  if ($attendance['activity_type'] == "attendance-in")
                  {
                    $attendance_in_marked = 1;
                    $attendance_out_marked = 0;
                  }
                  if ($attendance['activity_type'] == "attendance-out")
                  {
                    $attendance_in_marked = 0;
                    $attendance_out_marked = 1;
                  }
                }
                
                $total_states = $this->states_model->get_state_count();
                $total_districts = $this->districts_model->get_district_count();
                $total_blocks = $this->blocks_model->get_block_count();
                $total_classes = $this->classes_model->get_class_count();
                $total_subjects = $this->subjects_model->get_subject_count(1);
                $total_state_subjects = 0;
                $total_questions = 0;
                $get_mappings = $this->observations_model->get_all_mapping();
                $state_class_subjects = array();
                foreach($get_mappings as $get_mapping)
                {
                  if ($get_mapping->state_id > 0 and $get_mapping->class_id > 0 and $get_mapping->subject_id > 0)
                  {
                    $total_state_subjects = $total_state_subjects + 1;
                    array_push($state_class_subjects,$get_mapping->state_id."-".$get_mapping->class_id);
                  }
                }
                
                $get_observations = $this->observations_model->get_all();
                foreach($get_observations as $get_observation)
                {
                  $observation = array(); 
                  $observation_allowed = 1;
                  if ($get_observation->subject_wise == 1)
                  {
                    $check_com = $get_observation->state_id."-".$get_observation->class_id;
                    if (in_array($check_com,$state_class_subjects))
                    {
                      $observation_allowed = 1;
                    }
                    else if($get_observation->activity_type == 'school_visit' && $get_observation->class_id == 0){
                      $observation_allowed = 1;
                    }
                    else
                    {
                      $observation_allowed = 0;
                    }
                  }
                  
                  if ($observation_allowed == 1)
                  {
                    $total_questions = $total_questions + 1;
                  }
                }
                
                $sess_array = array(
                    'login_id' => $row->login_id,
                    'name' => $row->name,
                    'email' => $row->email,
                    'mobile' => $row->mobile,
                    'role' => $row->role,
                    'user_group' => $row->user_group,
                    'state_id' => $row->state_id,
                    'district_id' => 0,
                    'districts' => $user_districts,
                    'attendance_in_marked' => $attendance_in_marked,
                    'attendance_out_marked' => $attendance_out_marked,
                    'total_states' => $total_states,
                    'total_districts' => $total_districts,
                    'total_blocks' => $total_blocks,
                    'total_classes' => $total_classes,
                    'total_subjects' => $total_subjects,
                    'total_state_subjects' => $total_state_subjects,
                    'total_questions' => $total_questions,
                    'auth_token' => $data['auth_token']
                );                
            }
            //for each
            //if (($sess_array['role'] == "field_user" or $sess_array['role'] == "manager") and (count($user_districts) > 0)) {\
            if ($sess_array['user_group'] == "Head Office" || $sess_array['user_group'] == "head_office" || $sess_array['user_group'] == "HO") {
                $this->response(array("http_code" => 402, 'user_info' => $sess_array));
            } else if ((($sess_array['role'] == "field_user" or $sess_array['role'] == "manager") and (count($user_districts) > 0)) || $sess_array['role'] == "state_person") {
                $this->response(array("http_code" => 402, 'user_info' => $sess_array));
            } else {
                $this->response(array("http_code" => 401, 'message' => 'You are not authorized to access the app'));
            }
        } else {
            $this->response(array("http_code" => 401, 'message' => 'Invalid Username OR Password'));
        }
    }

    //MASTER OBSERVATIONS LIST 
    public function get_updates_post() {    
      $master_list = Array();
      
      $notification = "";
      if ($this->local_id == "Gagan")
        $notification = "Re-Sync Master Data to get High Enrollment Schools";
      $state_subjects_array['notification'] = $notification; 
      $state_subjects_array['test_questions_updated'] = $this->test_observation_flag;
      $state_subjects_array['questions_updated'] = $this->observation_flag;
      array_push($master_list, $state_subjects_array);
            
      $this->response(array("http_code" => 402, "master_list" => $master_list));
    }
    //MASTER OBSERVATIONS LIST FUNCTION CLOSED HERE 
    
    public function tracking_post()
    {
      $json = $this->request->body;

      $user = $json['Data']['user'];
      $activity_date = $this->create_datetime($json['Data']['time']);
      $version_status = $this->app_version_check();
      if ($version_status == false)
      {
        $response = array("http_code" => 503,  'local_event_id'=>$json['Data']['local_event_id'], 'delete_local' => '1', 'status' => 'failed', 'sync_error' => true, 'message' => $this->app_version_error_message);
        $this->response($response);
      }
      
      $tData['spark_id'] = $this->user_id; 
      $tData['local_db_id'] = $json['Data']['local_db_id']; 
      $tData['tracking_time'] = isset($json['Data']['time']) ? $activity_date : date("Y-m-d H:i"); 
      $tData['apk_version'] = $json['Data']['app_version']; 
      $tData['android_version'] = $json['Data']['android_version']; 
      $tData['accuracy'] = $json['Data']['accuracy']; 
      $tData['battery_status'] = $json['Data']['battery_status']; 
      $tData['early_out_reason'] = (isset($json['Data']['early_out_reason']) ? $json['Data']['early_out_reason'] : ""); 
      $tData['captured_location'] = (isset($json['Data']['captured_location']) ? $json['Data']['captured_location'] : ""); 
      $tData['tracking_latitude'] = $json['Data']['lat']; 
      $tData['tracking_longitude'] = $json['Data']['log']; 
      $tData['tracking_location'] = $json['Data']['location']; 
      $tData['activity_type'] = $json['Data']['event']; 
      $tData['local_event_id'] = $json['Data']['local_event_id']; 
      
      //added travel mode & cost on 24-Dec-2019
			$travel_mode_id = 0;
			if(!empty($json['Data']['type_info']['travelMode'])){
				$this->load->model('Travels_model');
				$travelData = $this->Travels_model->getByName($json['Data']['type_info']['travelMode']);
				if(!empty($travelData))
					$travel_mode_id = $travelData['id'];
			}
      $tData['travelmodeid'] = $travel_mode_id; 
      $tData['travelcost'] = (isset($json['Data']['type_info']['travelCost']) ? $json['Data']['type_info']['travelCost'] : 0); 
      
      $this->load->model('Tracking_model');
      $entry_allowed = 1;
      $error_message = "";
      
      $date_leave = date("Y-m-d",strtotime($activity_date));
      $user_id = $this->user_id;
      $leave_count_query = "select count(*) as count from ssc_leaves where user_id= '$user_id' and leave_from_date<=date_format('$date_leave','%Y-%m-%d') and leave_end_date>=date_format('$date_leave','%Y-%m-%d') ";
      
      $select_query = $this->db->query($leave_count_query);
      $row = $select_query->result();
      $leaveCount = $row[0]->count;			
      if($leaveCount > 0)
      {
          $entry_allowed = 0;
          $error_message = "1Activity day already marked for leave.";
      }
      
      $check_activity = $this->check_activity($this->user_id, $tData['activity_type'], $activity_date);
      if($check_activity > 0)
      {
          $entry_allowed = 0;
          $error_message = "Activity day already marked.";
      }

      if ($tData['activity_type'] == "attendance-in")
      {
        $attendance = $this->Tracking_model->checkAttendanceTime($this->user_id,$tData['tracking_time'],'in');
        if (isset($attendance) and isset($attendance['id']) and $attendance['id'] > 0)
        {
          $entry_allowed = 0;
          $error_message = "Attendance already marked.";
        }
      }
      if ($tData['activity_type'] == "attendance-out")
      {
        $attendance = $this->Tracking_model->checkAttendanceTime($this->user_id,$tData['tracking_time'],'out');
        if (isset($attendance) and isset($attendance['id']) and $attendance['id'] > 0)
        {
          $this->Tracking_model->update_attendance_out($tData,$attendance['id']);
          $entry_allowed = 0;
          $error_message = "Attendance already marked.";
        }
        else{
          $attendance_out = $this->Tracking_model->checkSystemAttendanceOut($this->user_id, date('Y-m-d', strtotime($tData['tracking_time'])));
          if(!empty($attendance_out)){
             $system_attendance_out_id = $attendance_out['id'];
             $this->Tracking_model->deleteTrackings($system_attendance_out_id); 
             
             $mdata['distance_flag'] = 0;
             $this->Tracking_model->update_tracking_distance_flag($this->user_id, date('Y-m-d', strtotime($tData['tracking_time'])), $mdata); 
          }
        }
      }
      if($entry_allowed == 1) {
        $this->Tracking_model->add($tData);
        $this->response(array("http_code" => 401, 'status' => $entry_allowed, 'message' => '', 'questions_updated' => $this->observation_flag, 'questions_updated' => $this->observation_flag, 'local_event_id'=>$json['Data']['local_event_id'], 'notification' => $this->notification));
      } else {
        $this->response(array("http_code" => 401, 'status' => $entry_allowed, 'message' => $error_message, 'questions_updated' => $this->observation_flag, 'local_event_id'=>$json['Data']['local_event_id'], 'notification' => $this->notification));
      }
    }
    
    public function put_tracking_post($json)
    {
      //$json = $this->request->body;
      
      $tracking_time = (isset($json['Data']['type_info']['date']) ? $this->create_datetime($json['Data']['type_info']['date']) : date("Y-m-d H:i"));
      $this->check_attendance_out($this->user_id, $json);

      $user = $json['Data']['user'];
      $tData['spark_id'] = $this->user_id; 
      $tData['local_db_id'] = (isset($json['Data']['local_db_id']) ? $json['Data']['local_db_id'] : 0); 
      $tData['tracking_time'] = $tracking_time; 
      $tData['apk_version'] = (isset($json['Data']['type_info']['app_version']) ? $json['Data']['type_info']['app_version'] : ''); 
      $tData['android_version'] = (isset($json['Data']['type_info']['android_version']) ? $json['Data']['type_info']['android_version'] : ''); 
      $tData['accuracy'] = (isset($json['Data']['type_info']['accuracy']) ? $json['Data']['type_info']['accuracy'] : ''); 
      $tData['battery_status'] = (isset($json['Data']['type_info']['battery_status']) ? $json['Data']['type_info']['battery_status'] : ''); 
      $tData['tracking_latitude'] = (isset($json['Data']['type_info']['lat']) ? $json['Data']['type_info']['lat'] : ''); 
      $tData['tracking_longitude'] = (isset($json['Data']['type_info']['lon']) ? $json['Data']['type_info']['lon'] : ''); 
      $tData['tracking_location'] = (isset($json['Data']['type_info']['location']) ? $json['Data']['type_info']['location'] : ''); 
      $tData['activity_type'] = (isset($json['Data']['type_info']['activity_type']) ? $json['Data']['type_info']['activity_type'] : ''); 
      $tData['early_out_reason'] = (isset($json['Data']['type_info']['early_out_reason']) ? $json['Data']['type_info']['early_out_reason'] : ''); 
      $tData['captured_location'] = (isset($json['Data']['type_info']['captured_location']) ? $json['Data']['type_info']['captured_location'] : ""); 
      $tData['local_event_id'] = (isset($json['Data']['local_event_id']) ? $json['Data']['local_event_id'] : 0); 
      $tData['activity_id'] = (isset($json['Data']['type_info']['activity_id']) ? $json['Data']['type_info']['activity_id'] : ''); 
			
			//added travel mode & cost on 24-Dec-2019
			$travel_mode_id = '';
			if(!empty($json['Data']['type_info']['travelMode'])){
				$this->load->model('Travels_model');
				$travelData = $this->Travels_model->getByName($json['Data']['type_info']['travelMode']);
				if(!empty($travelData))
					$travel_mode_id = $travelData['id'];
			}
      $tData['travelmodeid'] = $travel_mode_id; 
      $tData['travelcost'] = (isset($json['Data']['type_info']['travelCost']) ? $json['Data']['type_info']['travelCost'] : ''); 
      
      $this->load->model('Tracking_model');
      $entry_allowed = 1;
      $error_message = "";
      
      $date_leave = date("Y-m-d",strtotime($activity_date));
      $user_id = $this->user_id;
      $leave_count_query = "select count(*) as count from ssc_leaves where user_id= '$user_id' and leave_from_date<=date_format('$date_leave','%Y-%m-%d') and leave_end_date>=date_format('$date_leave','%Y-%m-%d')";
      
      $select_query = $this->db->query($leave_count_query);
      $row = $select_query->result();
      $leaveCount = $row[0]->count;			
      if($leaveCount > 0)
      {
//        $response = array("http_code" => 401, 'status' => 'failed', 'sync_error' => true, 'message' => 'Activity day already marked for leave.');
//       return $response;
          $entry_allowed = 0;
          $error_message = "2Activity day already marked for leave.";
      }

      if ($tData['activity_type'] == "attendance-in")
      {
        $attendance = $this->Tracking_model->checkAttendance($this->user_id,$tData['tracking_time'],'in');
        if (isset($attendance) and isset($attendance['id']) and $attendance['id'] > 0)
        {
          $entry_allowed = 1;
          $error_message = "Attendance already marked.";
        }
      }
      if ($tData['activity_type'] == "attendance-out")
      {
        $attendance = $this->Tracking_model->checkAttendance($this->user_id,$tData['tracking_time'],'out');
        if (isset($attendance) and isset($attendance['id']) and $attendance['id'] > 0)
        {
          $entry_allowed = 1;
          $error_message = "Attendance already marked.";
        }
      }
      if ($entry_allowed == 1)
      {
        $this->Tracking_model->add($tData);
        //$this->response(array("http_code" => 401, 'status' => $entry_allowed, 'message' => '', 'questions_updated' => 1));
      }
      else
      {
        //$this->response(array("http_code" => 401, 'status' => $entry_allowed, 'message' => $error_message, 'questions_updated' => 1));
      }
    }
    
    //MASTER LIST 
    public function master_list_post() {    
      $this->load->model('observations_model');
      $this->load->model('states_model');
      $this->load->model('classes_model');
      $this->load->model('users_model');
      $this->load->model('user_district_blocks_model');
      $this->load->model('clusters_model');
      $this->load->model('schools_model');
      $this->load->model('blocks_model');
      $this->load->model('districts_model');
      $this->load->model('subjects_model');
      $this->load->model('Sparks_model');
      $this->load->model('travels_model');
      $this->load->model('Leaves_model');
      
      $version_status = $this->app_version_check();

      if ($version_status == false)
      {
        $response = array("http_code" => 200, 'status' => 'failed', 'sync_error' => true, 'message' => $this->app_version_error_message);
          $this->response($response);
      }
      error_log("inside controller account - master list post ", 0);
      $this->output->enable_profiler(FALSE);
      error_log("inside controller account - master list - BEFORE NEW STATE ", 0);
      $states = $this->states_model->get_by_id($this->user_state_id);

      $total_clusters = 0;
      $total_schools = 0;
      $state_array = Array();
      $school_array = Array();
      $master_list = Array();
      $user_district_blocks = $this->user_district_blocks_model->get_all($this->user_id);
      $temp_districts = array();
      $temp_district_ids = array();
      $temp_block_ids = array();
      $final_data = array();
      
      foreach($user_district_blocks as $user_district_block)
      {
        if (!array_key_exists($user_district_block->district_id,$temp_districts))
        {
          $temp_districts[$user_district_block->district_id]['name'] = $user_district_block->district_name;
          $temp_districts[$user_district_block->district_id]['blocks'] = array();
          array_push($temp_district_ids,$user_district_block->district_id);
          
          $final_data[$user_district_block->district_id]['id'] = $user_district_block->district_id;
          $final_data[$user_district_block->district_id]['name'] = $user_district_block->district_name;
          $final_data[$user_district_block->district_id]['blocks'] = array();
        }
        array_push($temp_districts[$user_district_block->district_id]['blocks'],$user_district_block->block_id);
        array_push($temp_block_ids, $user_district_block->block_id);
      }
      
      $t_school = array();
      if(!empty($temp_block_ids)){
        $clusters = $this->clusters_model->get_by_block_ids($temp_block_ids,'');
        
        foreach($clusters as $cluster)
        {
          $total_clusters = $total_clusters + 1;
          if (!array_key_exists($cluster->block_id,$final_data[$cluster->district_id]['blocks']))
          {
            $final_data[$cluster->district_id]['blocks'][$cluster->block_id]['id'] = $cluster->block_id;
            $final_data[$cluster->district_id]['blocks'][$cluster->block_id]['name'] = $cluster->block_name;
            $final_data[$cluster->district_id]['blocks'][$cluster->block_id]['clusters'] = array();
          }
          $final_data[$cluster->district_id]['blocks'][$cluster->block_id]['clusters'][$cluster->id]['id'] = $cluster->id;         
          $final_data[$cluster->district_id]['blocks'][$cluster->block_id]['clusters'][$cluster->id]['name'] = $cluster->name;         
          $final_data[$cluster->district_id]['blocks'][$cluster->block_id]['clusters'][$cluster->id]['schools'] = array();         
        }
        
        $schools = $this->schools_model->get_by_block_ids($temp_block_ids,'');
        foreach($schools as $school)
        {
          $total_schools = $total_schools + 1;
          $t_school['id'] = $school->id;
          $t_school['name'] = $school->name;
          $t_school['dise_code'] = $school->dise_code;
          $t_school['latitude'] = $school->latitude;
          $t_school['longitude'] = $school->longitude;
          $t_school['is_high_enrolled'] = $school->is_high_enrollment;
          $final_data[$school->district_id]['blocks'][$school->block_id]['clusters'][$school->cluster_id]['schools'][] = $t_school;
        }      
      }
      
      $final_data = array_values($final_data);
      foreach($final_data as $key => $final_data1)
      {
        $final_data[$key]['blocks'] = array_values($final_data[$key]['blocks']);
        foreach($final_data[$key]['blocks'] as $key1 => $final_blocks)
        {
          $final_data[$key]['blocks'][$key1]['clusters'] = array_values($final_data[$key]['blocks'][$key1]['clusters']);
         /* foreach($final_data[$key]['blocks'][$key1]['clusters'] as $key2 => $final_clusters)
          {
            $final_data[$key]['blocks'][$key1]['clusters'][$key2]['schools'] = array_values($final_data[$key]['blocks'][$key1]['clusters'][$key2]['schools']);
          } */
        }
      }
      $state_array['districts'] = $final_data;
      array_push($master_list, $state_array);
      
      //$travel_modes = $this->travels_model->get_all();
      $travel_modes = $this->travels_model->get_travelmode_by_role($this->role, 1);
      $travel_modes_array['travel_modes'] = Array();
      //error_log("inside controller account - master list - before for each subject ", 0);
      
      foreach ($travel_modes as $tm) {
        if ($this->api_version_no > '5.6.1')
        {
          $travel_mode_array = Array();
          $travel_mode_array['id'] = $tm->id;
          $travel_mode_array['name'] = $tm->travel_mode;
          $travel_mode_array['is_autocalculated'] = $tm->is_autocalculated;
          array_push($travel_modes_array['travel_modes'], $travel_mode_array);
        }
        else
        {
          if ($tm->is_autocalculated == 0)
          {
            $travel_mode_array = Array();
            $travel_mode_array['id'] = $tm->id;
            $travel_mode_array['name'] = $tm->travel_mode;
            array_push($travel_modes_array['travel_modes'], $travel_mode_array);
          }
        }
      }
      if ($this->api_version_no > '5.6.1')
      {
        $travel_mode_array = Array();
        $travel_mode_array['id'] = 999;
        $travel_mode_array['name'] = 'With Other';
        $travel_mode_array['is_autocalculated'] = 1;
        array_push($travel_modes_array['travel_modes'], $travel_mode_array);
      }
      array_push($master_list, $travel_modes_array);
      
      $meet_with_arr = array();
      if($this->role == 'state_person')
				$meet_with_arr['meet_with'] = unserialize(MEET_WITH_SH);
			else if ($this->role == 'manager')
				$meet_with_arr['meet_with'] = unserialize(MEET_WITH_MGR);
			else  
				$meet_with_arr['meet_with'] = unserialize(MEET_WITH_FUSER);
				
			array_push($master_list, $meet_with_arr);	
      
      $earnedData = $this->Leaves_model->get_leave_credits($this->user_id);
		
			$leave_credits = array();
			$leave_counts = array();
			foreach($earnedData as $earned)
			{
				$leave_count = array();
				$leave_count['leave_type'] = $earned->leave_type;  
				$leave_count['leave_earned'] = $earned->leave_earned;  
				$leave_count['leave_taken'] = $earned->leave_taken;  
				array_push($leave_counts,$leave_count);
			} 	
			$leave_credits["leave_credits"] = $leave_counts;
			array_push($master_list, $leave_credits);
            
      //get user details
      $spark_id = $this->user_id;
      $user_districts = array();
      $user_district_blocks = $this->user_district_blocks_model->get_all($this->user_id);
      foreach($user_district_blocks as $u)
      {
        if (!array_key_exists($u->district_id,$user_districts))
        {
          $user_districts[$u->district_id]['id'] = $u->district_id;
          $user_districts[$u->district_id]['name'] = $u->district_name;
          $user_districts[$u->district_id]['blocks'] = [];
        }
        $user_districts[$u->district_id]['blocks'][] = array("id"=>$u->block_id, "name"=>$u->block_name);
      }
      $user_districts = array_values($user_districts);
      
      $total_states = $this->states_model->get_state_count();
      $total_districts = $this->districts_model->get_district_count();
      $total_blocks = $this->blocks_model->get_block_count();
      $total_classes = $this->classes_model->get_class_count();
      $total_subjects = $this->subjects_model->get_subject_count(1);
      $total_state_subjects = 0;
      $total_questions = 0;
      $get_mappings = $this->observations_model->get_all_mapping();
      $state_class_subjects = array();
      foreach($get_mappings as $get_mapping)
      {
        if ($get_mapping->state_id > 0 and $get_mapping->class_id > 0 and $get_mapping->subject_id > 0)
        {
          $total_state_subjects = $total_state_subjects + 1;
          array_push($state_class_subjects,$get_mapping->state_id."-".$get_mapping->class_id);
        }
      }

      $get_observations = $this->observations_model->get_all();
      foreach($get_observations as $get_observation)
      {
        $observation = array(); 
        $observation_allowed = 1;
        if ($get_observation->subject_wise == 1)
        {
          $check_com = $get_observation->state_id."-".$get_observation->class_id;
          if (in_array($check_com,$state_class_subjects))
          {
            $observation_allowed = 1;
          }
          else if($get_observation->activity_type == 'school_visit' && $get_observation->class_id == 0){
            $observation_allowed = 1;
          }
          else
          {
            $observation_allowed = 0;
          }
        }
        
        if ($observation_allowed == 1)
        {
          $total_questions = $total_questions + 1;
        }
      }

      $user_info = array(
          'login_id' => $this->login_id,
          'name' => $this->user_name,
          'email' => $this->user_email,
          'mobile' => $this->user_mobile,
          'role' => $this->role,
          'user_group' => $this->user_group,
          'state_id' => $this->user_state_id,
          'district_id' => 0,
          'districts' => $user_districts,
          'total_states' => $total_states,
          'total_districts' => $total_districts,
          'total_blocks' => $total_blocks,
          'total_classes' => $total_classes,
          'total_subjects' => $total_subjects,
          'total_state_subjects' => $total_state_subjects,
          'total_questions' => $total_questions,
          'auth_token' => $this->user_auth_token
      );     
      //get user details
      
      $sparksData = $this->users_model->getAppSparksList(array('field_user', 'manager', 'state_person', 'accounts', 'executive', 'HR'), array('field_user'));
      $sparkslist = array();
      $s = 0;
      foreach($sparksData as $sparks)
      {
				$sparkslist[$s]['id'] = $sparks->id;
				$sparkslist[$s]['login_id'] = $sparks->login_id;
				$sparkslist[$s]['name'] = $sparks->name;
				$sparkslist[$s]['state_id'] = $sparks->state_id;
				$sparkslist[$s]['spark_id'] = $sparks->user_id;
				$s++;
			}
			$sparks_list["sparks_list"] = $sparkslist;
			array_push($master_list, $sparks_list);

      $this->response(array("http_code" => 402, "master_list" => $master_list, 'total_clusters' => $total_clusters, 'total_schools' => $total_schools, 'total_travel_modes' => count($travel_modes_array['travel_modes']), 'user_info' => $user_info));
    }
    //MASTER LIST FUNCTION CLOSED HERE 
    
    //MASTER OBSERVATIONS LIST 
    public function master_observations_post() {    
      $this->load->model('observations_model');
      $this->load->model('states_model');
      $this->load->model('classes_model');
      $this->load->model('users_model');
      $this->load->model('user_district_blocks_model');
      $this->load->model('clusters_model');
      $this->load->model('schools_model');
      $this->load->model('subjects_model');
      $this->load->model('Sparks_model');
      $this->load->model('travels_model');
      
      $version_status = $this->app_version_check();
      if ($version_status == false)
      {
        $response = array("http_code" => 200, 'delete_local' => '1', 'status' => 'failed', 'sync_error' => true, 'message' => $this->app_version_error_message);
          $this->response($response);
      }
      
      error_log("inside controller account - master list post ", 0);
      $this->output->enable_profiler(FALSE);
      error_log("inside controller account - master list - BEFORE NEW STATE ", 0);
      $states = $this->states_model->get_by_id($this->user_state_id);

      $state_array = Array();
      $school_array = Array();
      $master_list = Array();
      $user_district_blocks = $this->user_district_blocks_model->get_all($this->user_id);
      $temp_districts = array();
      $temp_district_ids = array();
      $temp_block_ids = array();
      $final_data = array();
    
      $subjects = $this->subjects_model->get_all(1);
      $subjects_array['subjects'] = Array();
      //error_log("inside controller account - master list - before for each subject ", 0);
      foreach ($subjects as $c) {
          $subject_array = Array();
          $subject_array['id'] = $c->id;
          $subject_array['name'] = $c->name;
          array_push($subjects_array['subjects'], $subject_array);
      }
      array_push($master_list, $subjects_array);
      
      $states = $this->states_model->get_all();
      $activity_type = unserialize(ACTIVITY_ARRAY);
      $classes = $this->classes_model->get_all();
    
      $newVal = (object) array('id'=>0, 'name'=>'General Questions');
      array_push($classes,$newVal);

      $class_array['classes'] = $classes;
      array_push($master_list, $class_array);
      
      $all_observations = Array();
      $state_subjects = Array();
      foreach($activity_type as $typeKey=>$value){
        $all_observations["$typeKey"] = array();
      }
      
      $get_mappings = $this->observations_model->get_all_mapping();
      $state_class_subjects = array();
      foreach($get_mappings as $get_mapping)
      {
        if ($get_mapping->state_id > 0 and $get_mapping->class_id > 0 and $get_mapping->subject_id > 0)
        {
          $state_subject = Array();
          $state_subject['state_id'] = $get_mapping->state_id;
          $state_subject['class_id'] = $get_mapping->class_id;
          $state_subject['subject_id'] = $get_mapping->subject_id;
          array_push($state_class_subjects,$get_mapping->state_id."-".$get_mapping->class_id);
          array_push($state_subjects,$state_subject);
        }
      }
      
      $state_subjects_array['state_subjects'] = $state_subjects;
      array_push($master_list, $state_subjects_array);
      
      $total_questions = 0;
      $get_observations = $this->observations_model->get_all();
      foreach($get_observations as $get_observation)
      {
        $observation = array(); 
        $observation_allowed = 1;
        if ($get_observation->subject_wise == 1)
        {
          $check_com = $get_observation->state_id."-".$get_observation->class_id;
          if (in_array($check_com,$state_class_subjects))
          {
            $observation_allowed = 1;
          }
          else if($get_observation->activity_type == 'school_visit' && $get_observation->class_id == 0){
            $observation_allowed = 1;
          }
          else
          {
            $observation_allowed = 0;
          }
        }
        
        if ($observation_allowed == 1)
        {
          $observation['id'] = $get_observation->id;
          $observation['state_id'] = $get_observation->state_id;
          $observation['class_id'] = $get_observation->class_id;
          $observation['question'] = "Q) ".$get_observation->question; //." - ".$get_observation->state_id;
          $observation['question_type'] = $get_observation->question_type;
          $observation['subject_wise'] = $get_observation->subject_wise;
          $total_questions = $total_questions + 1;
          if ($get_observation->question_type == "objective")
          {
            $options = array();
            $question_options = $this->observations_model->getObtionsByObservation($get_observation->id);
            foreach($question_options as $question_option)
            {
              $option = array();
              $option['option_id'] = $question_option->id;
              $option['option_value'] = $question_option->option_value;
              array_push($options,$option);
            }
            $observation['options'] = $options;
            if (count($options) > 0)
              array_push($all_observations["$get_observation->activity_type"],$observation);
          }
          else if($get_observation->question_type == "yes-no-na"){
            $observation['question_type'] = "objective";
            $options = array();
            $question_options = array('1'=>'Yes', '2'=>'No', '0'=>'N/A');
            foreach($question_options as $key=>$value)
            {
              $option = array();
              $option['option_id'] = $key;
              $option['option_value'] = $value;
              array_push($options,$option);
            }
            $observation['options'] = $options;
            array_push($all_observations["$get_observation->activity_type"],$observation);
          }
          else
            array_push($all_observations["$get_observation->activity_type"],$observation);
        }
      }
      
      $observations_array['observations'] = $all_observations;
      array_push($master_list, $observations_array);
                  
      $sparkData['observation_flag'] = 0;
      $this->Sparks_model->update_info($sparkData, $this->user_id);
      
      $this->response(array("http_code" => 402, "master_list" => $master_list, 'total_questions' => $total_questions, 'total_classes' => count($classes), 'total_subjects' => count($subjects), 'total_state_subjects' => count($state_subjects)));
    }
    //MASTER OBSERVATIONS LIST FUNCTION CLOSED HERE 

    public function other_activity_post() {
      $json = $this->request->body;
        
      $tracking_time = $this->create_datetime($json['Data']['type_info']['date']);

      $version_status = $this->app_version_check();
      if ($version_status == false)
      {
        $response = array("http_code" => 200, 'local_id' => $json['Data']['local_id'],  'delete_local' => '1', 'status' => 'failed', 'sync_error' => true, 'message' => $this->app_version_error_message);
          $this->response($response);
      }
      $check_activity = $this->check_activity($this->user_id, 'distance_travel', $tracking_time);
      
      if($check_activity == 0){  
        
        //$response = array("http_code" => 200, 'server_id' => rand(1,10), 'sync_error' => false, "message" => "Stored Successfully", 'local_id' => $json['Data']['local_id']);
        
        $activity_info = $this->activity_handler->other_activity($json);
        if ($activity_info["message"] == "Stored Successfully")
        {
            $json['Data']['type_info']['activity_type'] = 'distance_travel';
            $json['Data']['type_info']['activity_id'] = $activity_info['server_id'];
            $json['Data']['type_info']['accuracy'] = $json['Data']['type_info']['pickupaccuracy'];
            $json['Data']['type_info']['location'] = $json['Data']['type_info']['pickupAddres'];
            $json['Data']['type_info']['lat'] = $json['Data']['type_info']['pickuplat'];
            $json['Data']['type_info']['lon'] = $json['Data']['type_info']['pickuplon'];
            $json['Data']['type_info']['captured_location'] = (isset($json['Data']['type_info']['captured_address_in']) ? $json['Data']['type_info']['captured_address_in'] : '');
            $this->put_tracking_post($json); // insert tracking info
            
            $tracking_time = (isset($json['Data']['type_info']['date_out']) ? $json['Data']['type_info']['date_out'] : '');
            $json['Data']['type_info']['date'] = $tracking_time; 
            $json['Data']['type_info']['accuracy'] = $json['Data']['type_info']['dropaccuracy'];
            $json['Data']['type_info']['location'] = $json['Data']['type_info']['dropAddres'];
            $json['Data']['type_info']['lat'] = $json['Data']['type_info']['droplat'];
            $json['Data']['type_info']['lon'] = $json['Data']['type_info']['droplon'];
            $json['Data']['type_info']['activity_type'] = 'distance_travel_out';
            $json['Data']['type_info']['activity_id'] = $activity_info['server_id'];
            $json['Data']['type_info']['captured_location'] = (isset($json['Data']['type_info']['captured_address_out']) ? $json['Data']['type_info']['captured_address_out'] : '');
            $this->put_tracking_post($json); // insert tracking info
        }
        $this->response($activity_info);
      }else{
        $get_activity_id = $this->get_activity($this->user_id, 'distance_travel', $tracking_time);
        
        $response = array("http_code" => 200, 'server_id' => $get_activity_id, 'sync_error' => false, "message" => "Stored Successfully", 'local_id' => $json['Data']['local_id']);
        
        //$response = array("http_code" => 200, 'sync_error' => false, 'local_id' => $json['Data']['local_id'], 'delete_local' => '1', 'status' => 'failed', 'sync_error' => true, 'message' => 'LDT on same time not allowed');
        $this->response($response);
      }
    }

    public function school_visit_post() {
        $json = $this->request->body;
        
      $tracking_time = $this->create_datetime($json['Data']['type_info']['date']);

      $version_status = $this->app_version_check();
      if ($version_status == false)
      {
        $response = array("http_code" => 200, 'local_id' => $json['Data']['local_id'], 'delete_local' => '1', 'status' => 'failed', 'sync_error' => true, 'message' => $this->app_version_error_message);
        $this->response($response);
      }
      $check_activity = $this->check_activity($this->user_id, 'schoolvisit', $tracking_time);
      
      if($check_activity == 0){  
        $activity_info = $this->activity_handler->school_visit_activity($json);
        if ($activity_info["message"] == "Stored Successfully")
        {
          $json['Data']['type_info']['activity_type'] = 'schoolvisit';
          $json['Data']['type_info']['activity_id'] = $activity_info['server_id'];
          $json['Data']['type_info']['captured_location'] = (isset($json['Data']['type_info']['captured_address_in']) ? $json['Data']['type_info']['captured_address_in'] : '');
          $this->put_tracking_post($json); // insert tracking info
          
          $tracking_time = (isset($json['Data']['type_info']['date_out']) ? $json['Data']['type_info']['date_out'] : '');
          $json['Data']['type_info']['date'] = $tracking_time; 
          $json['Data']['type_info']['accuracy'] = $json['Data']['type_info']['accuracy_out'];
          $json['Data']['type_info']['location'] = $json['Data']['type_info']['location_out'];
          $json['Data']['type_info']['lat'] = $json['Data']['type_info']['lat_out'];
          $json['Data']['type_info']['lon'] = $json['Data']['type_info']['lon_out'];
          $json['Data']['type_info']['activity_type'] = 'schoolvisit_out';
          $json['Data']['type_info']['activity_id'] = $activity_info['server_id'];
          $json['Data']['type_info']['captured_location'] = (isset($json['Data']['type_info']['captured_address_out']) ? $json['Data']['type_info']['captured_address_out'] : '');
          $this->put_tracking_post($json); // insert tracking info
        }
        $this->response($activity_info);
      }else{
        $get_activity_id = $this->get_activity($this->user_id, 'schoolvisit', $tracking_time);
        
        $response = array("http_code" => 200, 'server_id' => $get_activity_id, 'sync_error' => false, "message" => "Stored Successfully", 'local_id' => $json['Data']['local_id']);
        
        //$response = array("http_code" => 200, 'sync_error' => false, 'local_id' => $json['Data']['local_id'], 'delete_local' => '1', 'status' => 'failed', 'sync_error' => true, 'message' => 'School visit on same time not allowed');
        $this->response($response);
      }
    }
    
    public function attendance_out_post() {
      $json = $this->request->body;
        
      $tracking_time = $this->create_datetime($json['Data']['type_info']['date']);

      $version_status = $this->app_version_check();
      if ($version_status == false)
      {
        $response = array("http_code" => 200, 'local_id' => $json['Data']['local_id'], 'delete_local' => '1', 'status' => 'failed', 'sync_error' => true, 'message' => $this->app_version_error_message);
        $this->response($response);
      }
      $check_activity = $this->check_activity($this->user_id, 'attendance-out', $tracking_time);
      
      if($check_activity == 1){  
        $get_activity = $this->get_activity_detail($this->user_id, 'attendance-out', $tracking_time);
        if ($get_activity->activity_id == 0 || $get_activity->activity_id == '' || $get_activity->activity_id == NULL) {
          $activity_info = $this->activity_handler->attendance_out_activity($json);
          if ($activity_info["message"] == "Stored Successfully")
          {
            $this->load->model('Tracking_model');
            //added travel mode & cost on 24-Dec-2019
						$travel_mode_id = '';
						if(!empty($json['Data']['type_info']['travelMode'])){
							$this->load->model('Travels_model');
							$travelData = $this->Travels_model->getByName($json['Data']['type_info']['travelMode']);
							if(!empty($travelData))
								$travel_mode_id = $travelData['id'];
						}
						$data_tracking['travelmodeid'] = $travel_mode_id; 
						$data_tracking['travelcost'] = (isset($json['Data']['type_info']['travelCost']) ? $json['Data']['type_info']['travelCost'] : ''); 
            
            $data_tracking['activity_id'] = $activity_info['server_id'];
            $this->Tracking_model->update_attendance_out($data_tracking,$get_activity->id);
          }
        } else {
          $activity_info = array("http_code" => 200, 'server_id' => $get_activity->activity_id, 'sync_error' => false, "message" => "Stored Successfully", 'local_id' => $json['Data']['local_id']);          
        }
      } else {
        $activity_info = array("http_code" => 400, 'server_id' => 0, 'sync_error' => false, "message" => "Not Synced", 'local_id' => $json['Data']['local_id']);
      }
      $this->response($activity_info);
    }
    
    public function activity_info_post() {
        $json = $this->request->body;
        
        $activity_info = $this->activity_handler->get_activity_list($json);
        
        $this->response($activity_info);
    }
    
    public function training_post() {
      $json = $this->request->body;
      
      $tracking_time = $this->create_datetime($json['Data']['type_info']['date']);

      $version_status = $this->app_version_check();
      if ($version_status == false)
      {
        $response = array("http_code" => 200,  'local_id' => $json['Data']['local_id'], 'delete_local' => '1', 'status' => 'failed', 'sync_error' => true, 'message' => $this->app_version_error_message);
          $this->response($response);
      }
      $check_activity = $this->check_activity($this->user_id, 'training', $tracking_time);
      
      if($check_activity == 0){        
        $activity_info = $this->activity_handler->training_activity($json);
          
        if ($activity_info["message"] == "Stored Successfully")
        {
          $json['Data']['type_info']['activity_type'] = 'training';
          $json['Data']['type_info']['activity_id'] = $activity_info['server_id'];
          $json['Data']['type_info']['early_out_reason'] = (isset($json['Data']['type_info']['training_early_out_reason']) ? $json['Data']['type_info']['training_early_out_reason'] : "");
          $json['Data']['type_info']['captured_location'] = (isset($json['Data']['type_info']['captured_address_in']) ? $json['Data']['type_info']['captured_address_in'] : '');
          $this->put_tracking_post($json); // insert tracking info
        
          $tracking_time = (isset($json['Data']['type_info']['date_out']) ? $json['Data']['type_info']['date_out'] : '');
          $json['Data']['type_info']['date'] = $tracking_time; 
          $json['Data']['type_info']['accuracy'] = $json['Data']['type_info']['accuracy_out'];
          $json['Data']['type_info']['location'] = $json['Data']['type_info']['location_out'];
          $json['Data']['type_info']['lat'] = $json['Data']['type_info']['lat_out'];
          $json['Data']['type_info']['lon'] = $json['Data']['type_info']['lon_out'];
          $json['Data']['type_info']['early_out_reason'] = (isset($json['Data']['type_info']['training_early_out_reason']) ? $json['Data']['type_info']['training_early_out_reason'] : "");
          $json['Data']['type_info']['activity_type'] = 'training_out';
          $json['Data']['type_info']['activity_id'] = $activity_info['server_id'];
          $json['Data']['type_info']['captured_location'] = (isset($json['Data']['type_info']['captured_address_out']) ? $json['Data']['type_info']['captured_address_out'] : '');
          $this->put_tracking_post($json); // insert tracking info
        }
        $this->response($activity_info);
      }else{
        $get_activity_id = $this->get_activity($this->user_id, 'training', $tracking_time);
        
        $response = array("http_code" => 200, 'server_id' => $get_activity_id, 'sync_error' => false, "message" => "Stored Successfully", 'local_id' => $json['Data']['local_id']);
        
        //$response = array("http_code" => 200, 'sync_error' => false, 'local_id' => $json['Data']['local_id'], 'delete_local' => '1', 'status' => 'failed', 'sync_error' => true, 'message' => 'Training on same time not allowed');
        $this->response($response);
      }
    }
    
    public function meeting_post() {
      $json = $this->request->body;
      
      $tracking_time = $this->create_datetime($json['Data']['type_info']['date']);

      $version_status = $this->app_version_check();
      if ($version_status == false)
      {
         $response = array("http_code" => 200,  'local_id' => $json['Data']['local_id'], 'delete_local' => '1', 'status' => 'failed', 'sync_error' => true, 'message' => $this->app_version_error_message);
          $this->response($response);
      }
      $check_activity = $this->check_activity($this->user_id, 'review_meeting', $tracking_time);
      
      if($check_activity == 0){        

        $activity_info = $this->activity_handler->meeting_activity($json);
                
        if ($activity_info["message"] == "Stored Successfully")
        {
          $json['Data']['type_info']['activity_type'] = 'review_meeting';
          $json['Data']['type_info']['activity_id'] = $activity_info['server_id'];
          $json['Data']['type_info']['captured_location'] = (isset($json['Data']['type_info']['captured_address_in']) ? $json['Data']['type_info']['captured_address_in'] : '');
          $this->put_tracking_post($json); // insert tracking info
          
          $tracking_time = (isset($json['Data']['type_info']['date_out']) ? $json['Data']['type_info']['date_out'] : '');
          $json['Data']['type_info']['date'] = $tracking_time; 
          $json['Data']['type_info']['accuracy'] = $json['Data']['type_info']['accuracy_out'];
          $json['Data']['type_info']['location'] = $json['Data']['type_info']['location_out'];
          $json['Data']['type_info']['lat'] = $json['Data']['type_info']['lat_out'];
          $json['Data']['type_info']['lon'] = $json['Data']['type_info']['lon_out'];
          $json['Data']['type_info']['activity_type'] = 'review_meeting_out';
          $json['Data']['type_info']['activity_id'] = $activity_info['server_id'];
          $json['Data']['type_info']['captured_location'] = (isset($json['Data']['type_info']['captured_address_out']) ? $json['Data']['type_info']['captured_address_out'] : '');
          $this->put_tracking_post($json); // insert tracking info
        }
        $this->response($activity_info);
      }else{
        $get_activity_id = $this->get_activity($this->user_id, 'review_meeting', $tracking_time);
        
        $response = array("http_code" => 200, 'server_id' => $get_activity_id, 'sync_error' => false, "message" => "Stored Successfully", 'local_id' => $json['Data']['local_id']);
        
        //$response = array("http_code" => 200, 'sync_error' => false, 'local_id' => $json['Data']['local_id'], 'delete_local' => '1', 'status' => 'failed', 'sync_error' => true, 'message' => 'Meeting on same time not allowed');
                
        $this->response($response);
      }
    }

    public function no_visit_day_post() {
      $json = $this->request->body;
      
      $tracking_time = $json['Data']['type_info']['date_of_no_visit'];
      $version_status = $this->app_version_check();
      if ($version_status == false)
      {
        $response = array("http_code" => 401, 'status' => 'failed', 'sync_error' => true, 'message' => $this->app_version_error_message);
          $this->response($response);
      }
      $response = $this->activity_handler->no_visit_day($json);
      $this->response($response);
    }
    
    public function check_activity($spark_id, $activity_type, $tracking_time)
    {
			$select = "select count(id) AS TOTAL from ssc_trackings where spark_id= '$spark_id' and activity_type='$activity_type' and tracking_time='$tracking_time'";
      $qry_disatcne = $this->db->query($select);
      $row = $qry_disatcne->result();	
      if(!empty($row)){
				$previous_time = date('Y-m-d H:i:s', strtotime("-2 minutes", strtotime($tracking_time)));
				
				$sel = "select count(id) AS TOTAL from ssc_trackings where spark_id= '$spark_id' and activity_type='$activity_type' and tracking_time between '$previous_time' AND '$tracking_time'";
				$qry = $this->db->query($sel);
				$row = $qry->result();	
			}
			return $row[0]->TOTAL;
    }
    
    public function get_activity($spark_id, $activity_type, $tracking_time)
    {
      $select = "select id from ssc_trackings where spark_id= '$spark_id' and activity_type='$activity_type' and tracking_time='$tracking_time'";
      $qry_disatcne = $this->db->query($select);
      $row = $qry_disatcne->result();
      $current_id = 0;
      if(count($row) > 0){
        $current_id = $row[0]->id;
      }
      return $current_id;
    }
    
    public function get_activity_detail($spark_id, $activity_type, $tracking_time)
    {
      $select = "select * from ssc_trackings where spark_id= '$spark_id' and activity_type='$activity_type' and tracking_time='$tracking_time'";
      $qry_disatcne = $this->db->query($select);
      $row = $qry_disatcne->result();
      $current_id = 0;
      if(count($row) > 0){
        return $row[0];
      }
      else{
				$previous_time = date('Y-m-d H:i:s', strtotime("-2 minutes", strtotime($tracking_time)));
				
				$sel = "select * from ssc_trackings where spark_id= '$spark_id' and activity_type='$activity_type' and tracking_time between '$previous_time' AND '$tracking_time'";
				$qry = $this->db->query($sel);
				$row1 = $qry->result();	
				if(count($row1) > 0){
					return $row1[0];
				}
			}
    }

    public function check_attendance_out($spark_id, $json)
    {
      $current_tracking_time = (isset($json['Data']['type_info']['date']) ? $this->create_datetime($json['Data']['type_info']['date']) : date("Y-m-d H:i:s"));
      
      //UPDATE DISTANCE FLAG IF DISCTANCE ALREADY CALCULATED
      $activity_string = "'attendance-in', 'training-in', 'meeting-in', 'schoolvisit-in', 'attendance-out','schoolvisit', 'review_meeting', 'training'";
      
      $sel_distance = "SELECT count(id) FROM ssc_trackings WHERE spark_id = '$spark_id' AND Date(tracking_time) = date_format('$current_tracking_time','%Y-%m-%d') AND distance_flag = '1' AND activity_type in(".$activity_string.") ";
      $qry_disatcne = $this->db->query($sel_distance);
      $row_distance = $qry_disatcne->result();
      if(count($row_distance) > 0){
          $update = "UPDATE ssc_trackings SET distance_flag = 0 WHERE spark_id = '$spark_id' AND Date(tracking_time) = date_format('$current_tracking_time','%Y-%m-%d') AND activity_type in(".$activity_string.")";  
          $this->db->query($update);
      }
      //UPDATE DISTANCE FLAG IF DISCTANCE ALREADY CALCULATED
      
      //UPDATE ATTENDANCE OUT IF ALREADY INSERTED
      $select = "SELECT * FROM ssc_trackings WHERE spark_id = '$spark_id' AND Date(tracking_time) = date_format('$current_tracking_time','%Y-%m-%d') AND tracking_time < '$current_tracking_time' AND activity_type = 'attendance-out' ";
      $query = $this->db->query($select);
      $row = $query->result();
      
      if(count($row) > 0){
        
        $current_id = $row[0]->id;
        
        $new_track_time = date('Y-m-d H:i:s',strtotime('+20 seconds',strtotime($current_tracking_time)));
        
        $tData['local_db_id'] = (isset($json['Data']['local_db_id']) ? $json['Data']['local_db_id'] : 0); 
        $tData['tracking_time'] = $new_track_time; 
        $tData['apk_version'] = (isset($json['Data']['type_info']['app_version']) ? $json['Data']['type_info']['app_version'] : ''); 
        $tData['android_version'] = (isset($json['Data']['type_info']['android_version']) ? $json['Data']['type_info']['android_version'] : ''); 
        $tData['accuracy'] = (isset($json['Data']['type_info']['accuracy']) ? $json['Data']['type_info']['accuracy'] : ''); 
        $tData['battery_status'] = (isset($json['Data']['type_info']['battery_status']) ? $json['Data']['type_info']['battery_status'] : ''); 
        $tData['tracking_latitude'] = (isset($json['Data']['type_info']['lat']) ? $json['Data']['type_info']['lat'] : ''); 
        $tData['tracking_longitude'] = (isset($json['Data']['type_info']['lon']) ? $json['Data']['type_info']['lon'] : ''); 
        $tData['tracking_location'] = (isset($json['Data']['type_info']['location']) ? $json['Data']['type_info']['location'] : ''); 
        $tData['activity_type'] = 'attendance-out'; 
        $tData['local_event_id'] = (isset($json['Data']['local_event_id']) ? $json['Data']['local_event_id'] : 0); 
        $this->load->model('Tracking_model');
        
        $this->Tracking_model->update_attendance_out($tData, $current_id);
      }
    }

    public function call_post() {
      $json = $this->request->body;

      $response = $this->activity_handler->call_activity($json);
      $this->response($response);
    }

    public function addContact_post() {
        error_log("Inside account - contact post method-----", 0);
        $json = $this->request->body;
        $activity_date = date("Y-m-d", strtotime($json['Data']['date']));
        // if (!$this->validate_activity_date($activity_date))
        //$this->response(array("http_code" => 401, 'status' => 'failed', 'sync_error' => true, 'message' => 'Add contact date cannot be in future date or older more than 7 days from now.'));
        $user_id = $this->user_id;
        $id = $this->id;
        $district_id = $this->user_district_id;
        $block_id = $this->user_block_id;
        $cluster_id = $this->user_cluster_id;
        $firstname = $this->firstname;
        $lastname = $this->lastname;
        $mobile = $this->mobile;
        $designation = $this->designation;
        $source = $this->source;

        $this->load->model("holidays_model");
        $this->load->model("contact_model");

        $holiday = $this->holidays_model->get_all($user_id, $activity_date, $activity_date);

        if (count($holiday) > 0) {
            $this->response(array('status' => 'failed', 'sync_error' => true, 'message' => 'contact cannot be marked for ' . $activity_date . ', as holiday already marked'));
        }
        $this->load->model("contact_model");
        //$mdata['id'] = $this . id;
        $mdata['activity_date'] = $activity_date;
        $mdata['user_id'] = $user_id;
        //  $mdata['state_id'] = $state_id;
        $mdata['district_id'] = $district_id;
        $mdata['block_id'] = $json['Data']['block_id'];
        $mdata['cluster_id'] = $json['Data']['cluster_id'];
        $mdata['firstname'] = $json['Data']['firstname'];
        $mdata['lastname'] = $json['Data']['lastname'];
        $mdata['mobile'] = $json['Data']['mobile'];
        $mdata['designation'] = $json['Data']['designation'];
        $mdata['source'] = $json['Data']['source'];


        $last_id_p=$this->contact_model->add($mdata);


        /*
          $this->load->model("contact_model");
          $contact_array = $json['Data']['type_info']['contact']['id'];

          if (!empty($contact_array) && $last_id_p != null) {

          foreach ($contact_array as $Id) {
          $contact_data = array();
          $contact_data['id'] = $last_id_p;
          $contact_data['first_name'] = $subject['first_name'];
          $contact_data['last_name'] = $subject['last_name'];
          $contact_data['number'] = $subject['number'];
          $contact_data['designation'] = $subject['designation'];

          $this->contact_model->add($contact_data);
          }
          } */

        $this->response(array("http_code" => 200, 'server_id' => $last_id_p, 'sync_error' => false, "contact_id" => $last_id_p, "message" => "Stored Successfully", 'local_id' => $json['Data']['local_id']));
    }
    
    //editcontact post
    public function editContact_post() {
        error_log("Inside account -edit  contact post method-----", 0);
        $json = $this->request->body;
        $activity_date = date("Y-m-d", strtotime($json['Data']['date']));
        // if (!$this->validate_activity_date($activity_date))
        //$this->response(array("http_code" => 401, 'status' => 'failed', 'sync_error' => true, 'message' => 'Add contact date cannot be in future date or older more than 7 days from now.'));
        
        $user_id = $this->user_id;
        $id = $this->id;
        //$contact_id=61;
        $district_id = $this->user_district_id;
        $block_id = $this->user_block_id;
        $cluster_id = $this->user_cluster_id;
        $firstname = $this->firstname;
        $lastname = $this->lastname;
        $mobile = $this->mobile;
        $designation = $this->designation;
        $source = $this->source;

        $this->load->model("holidays_model");
        $this->load->model("contact_model");

        $holiday = $this->holidays_model->get_all($user_id, $activity_date, $activity_date);

        if (count($holiday) > 0) {
            $this->response(array('status' => 'failed', 'sync_error' => true, 'message' => 'contact cannot be marked for ' . $activity_date . ', as holiday already marked'));
        }
       

       

       // $mdata['id'] = $this . $id;
        $mdata['activity_date'] = $activity_date;
        $mdata['user_id'] = $user_id;
        //  $mdata['state_id'] = $state_id;
        $mdata['id'] = $json['Data']['contact_id'];
        $mdata['district_id'] = $json['Data']['district_id'];
        $mdata['block_id'] = $json['Data']['block_id'];
        $mdata['cluster_id'] = $json['Data']['cluster_id'];
        $mdata['firstname'] = $json['Data']['firstname'];
        $mdata['lastname'] = $json['Data']['lastname'];
        $mdata['mobile'] = $json['Data']['mobile'];
        $mdata['designation'] = $json['Data']['designation'];
        $mdata['source'] = $json['Data']['source'];


        $this->contact_model->edit($mdata['id'], $mdata);
        $last_id_p = $this->db-> $json['Data']['contact_id'];
        $this->response(array("http_code" => 200, 'server_id' =>  $json['Data']['contact_id'], 'sync_error' => false, "contact_id" => $json['Data']['contact_id'], "message" => "Updated Successfully", 'local_id' => $json['Data']['local_id']));
    }
    
    public function getcontact_post() {

        $json = $this->request->body;
        //print_r($json);
        $district_id = $json['Data']['district_id'];
        $block_id = $json['Data']['block_id'];
        $cluster_id = $json['Data']['cluster_id'];
        $id = $json['Data']['id'];
        $source = $json['Data']['source'];
        error_log(" -- inside account - getcontact_post dist id=" . $district_id . " blk id=" . $block_id . " clul id=" . $cluster_id . " source=" . $source, 0);
        $this->load->model('contact_model');
        $result = array();
        $result = $this->contact_model->getContacts('', $district_id, $block_id, $cluster_id, $source);
        // print_r($result);
       if(!($source=="BL"||$source=="CL"||$source=="DS"||$source=="bl"||$source=="cl"||$source=="ds") ){
           $this->response(array("http_code" => 401, 'message' => 'Provide VAILD source'));
       }else
       if ($result > 0) {
            $this->response(array("http_code" => 402, 'contact_detail' => $result));
        } else {
            $this->response(array("http_code" => 401, 'message' => 'You are not authorized to access the app'));
        }
        $this->response(array("http_code" => 401, 'message' => 'Invalid username or password'));
    }
    
    public function getallcontact_post() {

        $json = $this->request->body;
        //print_r($json);
        $district_id = $json['Data']['district_id'];
       // $block_id = $json['Data']['block_id'];
       // $cluster_id = $json['Data']['cluster_id'];
       // $id = $json['Data']['id'];
       // $source = $json['Data']['source'];
        error_log(" -- inside account - getcontact_post dist id=" . $district_id . " blk id=" . $block_id . " clul id=" . $cluster_id . " source=" . $source, 0);
        $this->load->model('contact_model');
        $result = array();
        $result = $this->contact_model->getBYId($district_id);
        // print_r($result);
      
       if ($result > 0) {
            $this->response(array("http_code" => 402, 'contact_detail' => $result));
        } else {
            $this->response(array("http_code" => 401, 'message' => 'You are not authorized to access the app'));
        }
        $this->response(array("http_code" => 401, 'message' => 'Invalid username or password'));
    }
    
    public function leave_post() {
        $json = $this->request->body;
        $leave_from_date = date("Y-m-d", strtotime($json['Data']['type_info']['leave_from_date']));
        $leave_end_date = date("Y-m-d", strtotime($json['Data']['type_info']['leave_end_date']));
        $user_id = $this->user_id;
        $state_id = $this->user_state_id;
        $district_id = $this->user_district_id;
        $this->load->model("leaves_model");
        $holiday = $this->leaves_model->get_all($user_id, $leave_from_date, $leave_end_date);

        if (count($holiday) > 0) {
            $this->response(array('status' => 'failed', 'sync_error' => true, 'message' => 'Can not apply leave for ' . $leave_from_date . ', as  already applied'));
        }
        
		
		
        //LOAD TIMEOVERLAP MODEL
        $this->load->model("Timeoverlaptest_model");

        //CHECKING FULL DAY ACTIVITY MARKED -------------
        if (!($this->Timeoverlaptest_model->checkFullDayActivity($user_id, $leave_from_date, 'LEAVE',$leave_end_date))) {
            $this->response(array("http_code" => 401, 'local_id' => $json['Data']['local_id'], 'status' => 'failed', 'sync_error' => true, 'message' => 'Days already marked for activity , can\'t take this request.'));
        }

        $mdata['status']          = 'approved';
        $mdata['leave_from_date']        = $leave_from_date;
        $mdata['leave_end_date']         = $leave_end_date;
        $mdata['user_id']                = $user_id;
        $mdata['state_id']               = $state_id;
        $mdata['district_id']            = $district_id;
        $mdata['leave_type']             = $json['Data']['type_info']['leave_type'];
        $mdata['day_type']               = $json['Data']['type_info']['leave_duration'];
        $mdata['total_days']             = $json['Data']['type_info']['total_days'];
        $mdata['reason']             = (isset($json['Data']['type_info']['reason']) ? $json['Data']['type_info']['reason'] : '');
				
				$creditData = $this->leaves_model->get_leave_credits($this->user_id, $json['Data']['type_info']['leave_type']);
				if(!empty($creditData)){
					$leave_left = $creditData[0]->leave_earned - $creditData[0]->leave_taken;
					if($json['Data']['type_info']['total_days'] > $leave_left){
						$this->response(array("http_code" => 401, 'local_id' => $json['Data']['local_id'], 'status' => 'failed', 'sync_error' => true, 'message' => 'Applied leave count greater than leave left.'));
					}
				}
					
				$this->leaves_model->add($mdata);
        $last_id_p = $this->db->insert_id();
        
        if(!empty($creditData)){
					$ldata['leave_taken'] = $creditData[0]->leave_taken+$json['Data']['type_info']['total_days'];  
					$this->leaves_model->update_leave_credit($ldata, $creditData[0]->id);    
				} 
				
				$earnedData = $this->leaves_model->get_leave_credits($this->user_id);
		
				$leave_counts = array();
				foreach($earnedData as $earned)
				{
					$leave_count = array();
					$leave_count['leave_type'] = $earned->leave_type;  
					$leave_count['leave_earned'] = $earned->leave_earned;  
					$leave_count['leave_taken'] = $earned->leave_taken;  
					array_push($leave_counts,$leave_count);
				} 	
				
        $this->response(array("http_code" => 200, 'server_id' => $last_id_p, 'leave_credits' => $leave_counts, 'sync_error' => false, "message" => "Applied Successfully", 'local_id' => $json['Data']['local_id']));
    }
    
    public function myperformance_post()
    {
      $this->load->model('No_visit_days_model');
      $this->load->model('School_visits_model');
      $this->load->model('Meetings_model');
      $this->load->model('Leaves_model');
      $this->load->model('Trainings_model');
      $this->load->model('Calls_model');
      $this->load->model('Users_model');
      $this->load->model('Holidays_model');
      
      $login_data = file_get_contents("php://input");
      $login_data = json_decode($login_data,1);
      $login_id = $this->user_id;
      $state_id = $this->user_state_id;
      
      $inpmonth = date("n");
      $inpyear = date("Y");
      $inpdate			 = '01/'.$inpmonth.'/'.$inpyear;
      $inpdate 		 = date('t/m/Y', strtotime($inpyear.'-'.$inpmonth.'-01'));
      //Session Data

      $user_blocks = $this->Users_model->user_blocks($login_id);
      $user_block_ids = array();
      foreach($user_blocks as $user_block)
      {
        array_push($user_block_ids,$user_block->id);
      }
      //echo"<br>".implode(',',$user_block_ids);

      $user_districts = $this->Users_model->user_districts($login_id);
      $user_district_ids = array();
      foreach($user_districts as $user_district)
      {
        array_push($user_district_ids,$user_district->id);
      }
      //print_r($user_district_ids);
      
      $current_month = (Int) date("m", strtotime(date("Y-m-d")));
      $current_year = (Int) date("Y", strtotime(date("Y-m-d")));
      $month_name = "";
      $prev_month2_name   = "";
      $prev_month1_name   = "";
      $cm_no 		= 0;
      $pv2_no 	= 0;
      $pv1_no 	= 0;
      $current_year2 = 0;
      $current_year1 = 0;

      switch($inpmonth){

      case "1":
        $month_name = date('F', mktime(0, 0, 0, $inpmonth, 10));
        $cm_no      = $inpmonth;
        $prev_month2_name   = date('F', mktime(0, 0, 0, 12, 10));
        $pv2_no = 12;
        $current_year2 = $inpyear - 1;
        $prev_month1_name   = date('F', mktime(0, 0, 0, 12 - $inpmonth, 10));
        $pv1_no = 12 - $inpmonth;
        $current_year1 = $inpyear - 1;
        break;
      case "2":
        $month_name = date('F', mktime(0, 0, 0, $inpmonth, 10));
        $cm_no      = $inpmonth;
        $prev_month2_name   = date('F', mktime(0, 0, 0, $inpmonth-1, 10));
        $pv2_no = $inpmonth-1;
        $current_year2 = $inpyear;
        $prev_month1_name   = date('F', mktime(0, 0, 0, 12, 10));
        $pv1_no = 12;
        $current_year1 = $inpyear - 1;
        break;
      default:
        $month_name   = date('F', mktime(0, 0, 0, $inpmonth, 10));
        $cm_no      = $inpmonth;
        $prev_month2_name   = date('F', mktime(0, 0, 0, $inpmonth-1, 10));
        $pv2_no = $inpmonth-1;
        $current_year2 = $inpyear;
        $prev_month1_name   = date('F', mktime(0, 0, 0, $inpmonth-2, 10));
        $pv1_no = $inpmonth-2;
        $current_year1 = $inpyear;
      }

      $session_start_year = $inpyear;
      if($inpmonth < SESSION_START_MONTH)
      {
        $session_start_year = $inpyear-1;
      }

      $_fromDate1 = date("Y-m-d",strtotime($current_year1."-".$pv1_no."-01"));
      $_toDate1 = date("Y-m-t",strtotime($current_year1."-".$pv1_no."-01"));
      $productive_days_1 = date("t",strtotime($current_year1."-".$pv1_no."-01"));
      
      $_fromDate2 = date("Y-m-d",strtotime($current_year2."-".$pv2_no."-01"));
      $_toDate2 = date("Y-m-t",strtotime($current_year2."-".$pv2_no."-01"));
      $productive_days_2 = date("t",strtotime($current_year2."-".$pv2_no."-01"));
      
      $_fromDate_1 = date("Y-m-d",strtotime($inpyear."-".$inpmonth."-01"));
      $_toDate_1 = date("Y-m-d",strtotime($inpyear."-".$inpmonth."-15"));
      $productive_days_3 = 15;
      
      $_fromDate_2 = date("Y-m-d",strtotime($inpyear."-".$inpmonth."-16"));
      $_toDate_2 = date("Y-m-t",strtotime($inpyear."-".$inpmonth."-01"));
      $productive_days_4 = date("t",strtotime($inpyear."-".$inpmonth."-01"))-15;
          
      //-----------------------NO VISIT DAYS AND AUTHORITY DECLARED HOLIDAYS
      $no_visit_days = array(0,0,0,0,0);
      $auth_dec_holidays = array(0,0,0,0,0);
      $no_visit_1 = $this->No_visit_days_model->get_all($login_id, $_fromDate1, $_toDate1);
      foreach($no_visit_1 as $no_visit)
      {
        if ($no_visit->no_visit_reason == 'Local Authority Declared Holiday')
          $auth_dec_holidays[0] = $auth_dec_holidays[0] + 1;
        else
          $no_visit_days[0] = $no_visit_days[0] + 1;
      }

      $no_visit_2 = $this->No_visit_days_model->get_all($login_id, $_fromDate2, $_toDate2);
      foreach($no_visit_2 as $no_visit)
      {
        if ($no_visit->no_visit_reason == 'Local Authority Declared Holiday')
          $auth_dec_holidays[1] = $auth_dec_holidays[1] + 1;
        else
          $no_visit_days[1] = $no_visit_days[1] + 1;
      }

      $no_visit_3 = $this->No_visit_days_model->get_all($login_id, $_fromDate_1, $_toDate_1);
      foreach($no_visit_3 as $no_visit)
      {
        if ($no_visit->no_visit_reason == 'Local Authority Declared Holiday')
          $auth_dec_holidays[2] = $auth_dec_holidays[2] + 1;
        else
          $no_visit_days[2] = $no_visit_days[2] + 1;
      }

      $no_visit_4 = $this->No_visit_days_model->get_all($login_id, $_fromDate_2, $_toDate_2);
      foreach($no_visit_4 as $no_visit)
      {
        if ($no_visit->no_visit_reason == 'Local Authority Declared Holiday')
          $auth_dec_holidays[3] = $auth_dec_holidays[3] + 1;
        else
          $no_visit_days[3] = $no_visit_days[3] + 1;
      }

      //-----------------------CALLS STARTED
      $calls_1 = $this->Calls_model->get_all($login_id, $_fromDate1, $_toDate1);
      $calls_2 = $this->Calls_model->get_all($login_id, $_fromDate2, $_toDate2);
      $calls_3 = $this->Calls_model->get_all($login_id, $_fromDate_1, $_toDate_1);
      $calls_4 = $this->Calls_model->get_all($login_id, $_fromDate_2, $_toDate_2);
      
      //-----------------------LEAVES
      $leaves = array(0,0,0,0,0);
      $leaves_1 = $this->Leaves_model->get_all($login_id, $_fromDate1, $_toDate1);
      foreach($leaves_1 as $leave)
        $leaves[0] = $leaves[0] + $leave->total_days;
        
      $leaves_2 = $this->Leaves_model->get_all($login_id, $_fromDate2, $_toDate2);
      foreach($leaves_2 as $leave)
        $leaves[1] = $leaves[1] + $leave->total_days;
        
      $leaves_3 = $this->Leaves_model->get_all($login_id, $_fromDate_1, $_toDate_1);
      foreach($leaves_3 as $leave)
        $leaves[2] = $leaves[2] + $leave->total_days;
        
      $leaves_4 = $this->Leaves_model->get_all($login_id, $_fromDate_2, $_toDate_2);
      foreach($leaves_4 as $leave)
        $leaves[3] = $leaves[3] + $leave->total_days;
			
			//Fetch user group
			$sparkData = $this->Users_model->getById($login_id);
			$current_group = $sparkData['user_group'];
			
      //-----------------------FIND STATE HOLIDAYS
      $state_holiday_1 = $this->Holidays_model->get_state_holidays($login_id, $state_id, $_fromDate1, $_toDate1, $current_group);
      $state_holiday_2 = $this->Holidays_model->get_state_holidays($login_id, $state_id, $_fromDate2, $_toDate2, $current_group);
      $state_holiday_3 = $this->Holidays_model->get_state_holidays($login_id, $state_id, $_fromDate_1, $_toDate_1, $current_group);
      $state_holiday_4 = $this->Holidays_model->get_state_holidays($login_id, $state_id, $_fromDate_2, $_toDate_2, $current_group);
      
      //-----------------------FIND SUN AND 2,3 SAT
      $sun_2_3_sat_1 = $this->Holidays_model->get_2_3_saturday($_fromDate1, $_toDate1, $current_group);
      $sun_2_3_sat_2 = $this->Holidays_model->get_2_3_saturday($_fromDate2, $_toDate2, $current_group);
      $sun_2_3_sat_3 = $this->Holidays_model->get_2_3_saturday($_fromDate_1, $_toDate_1, $current_group);
      $sun_2_3_sat_4 = $this->Holidays_model->get_2_3_saturday($_fromDate_2, $_toDate_2, $current_group);
      
      //-----------------------FIND HE VISIT COUNT
      $he_visit_count_1 = $this->School_visits_model->get_HE_visit_count($login_id, $state_id, $_fromDate1, $_toDate1);
      $he_visit_count_2 = $this->School_visits_model->get_HE_visit_count($login_id, $state_id, $_fromDate2, $_toDate2);
      $he_visit_count_3 = $this->School_visits_model->get_HE_visit_count($login_id, $state_id, $_fromDate_1, $_toDate_1);
      $he_visit_count_4 = $this->School_visits_model->get_HE_visit_count($login_id, $state_id, $_fromDate_2, $_toDate_2);
      
      //-----------------------FIND LE VISIT COUNT
      $le_visit_count_1 = $this->School_visits_model->get_LE_visit_count($login_id, $state_id, $_fromDate1, $_toDate1);
      $le_visit_count_2 = $this->School_visits_model->get_LE_visit_count($login_id, $state_id, $_fromDate2, $_toDate2);
      $le_visit_count_3 = $this->School_visits_model->get_LE_visit_count($login_id, $state_id, $_fromDate_1, $_toDate_1);
      $le_visit_count_4 = $this->School_visits_model->get_LE_visit_count($login_id, $state_id, $_fromDate_2, $_toDate_2);
      
      //-----------------------TRAININGS STARTED
      $home_district_trainings = array(0,0,0,0,0);
      $outstation_trainings = array(0,0,0,0,0);
      $trainings_1 = $this->Trainings_model->get_all($login_id, $_fromDate1, $_toDate1);
      //echo "<br>".$this->Trainings_model->defaultdb->last_query();
      foreach($trainings_1 as $training)
      {
        //if ($training->block_id > 0 and in_array($training->block_id,$user_block_ids))
        if ($training->mode == 'internal')
          $home_district_trainings[0] = $home_district_trainings[0] + 1;
        else
          $outstation_trainings[0] = $outstation_trainings[0] + 1;
      }
      
      $trainings_2 = $this->Trainings_model->get_all($login_id, $_fromDate2, $_toDate2);
      //echo "<br>".$this->Trainings_model->defaultdb->last_query();
      foreach($trainings_2 as $training)
      {
        //if ($training->block_id > 0 and in_array($training->block_id,$user_block_ids))
        if ($training->mode == 'internal')
          $home_district_trainings[1] = $home_district_trainings[1] + 1;
        else
          $outstation_trainings[1] = $outstation_trainings[1] + 1;
      }
      
      $trainings_3 = $this->Trainings_model->get_all($login_id, $_fromDate_1, $_toDate_1);
      foreach($trainings_3 as $training)
      {
        //if ($training->block_id > 0 and in_array($training->block_id,$user_block_ids))
        if ($training->mode == 'internal')
          $home_district_trainings[2] = $home_district_trainings[2] + 1;
        else
          $outstation_trainings[2] = $outstation_trainings[2] + 1;
      }
      
      $trainings_4 = $this->Trainings_model->get_all($login_id, $_fromDate_2, $_toDate_2);
      foreach($trainings_4 as $training)
      {
        //if ($training->block_id > 0 and in_array($training->block_id,$user_block_ids))
        if ($training->mode == 'internal')
          $home_district_trainings[3] = $home_district_trainings[3] + 1;
        else
          $outstation_trainings[3] = $outstation_trainings[3] + 1;
      }

      //-----------------------MEETINGS STARTED
      $district_level_meetings = array(0,0,0,0,0);
      $block_level_meetings = array(0,0,0,0,0);
      $internal_meetings = array(0,0,0,0,0);
      $meetings_1 = $this->Meetings_model->get_all($login_id, $_fromDate1, $_toDate1);
      foreach($meetings_1 as $meeting)
      {
        if ($meeting->is_internal == true)
          $internal_meetings[0] = $internal_meetings[0] + 1;
        else
        {
          if ($meeting->block_id == 0 and $meeting->is_internal == false)
            $district_level_meetings[0] = $district_level_meetings[0] + 1;
          else if ($meeting->block_id > 0 and $meeting->is_internal == false)
            $block_level_meetings[0] = $block_level_meetings[0] + 1;
        }
      }
      //echo "<br>".$this->Meetings_model->defaultdb->last_query();
      $meetings_2 = $this->Meetings_model->get_all($login_id, $_fromDate2, $_toDate2);
      foreach($meetings_2 as $meeting)
      {
        if ($meeting->is_internal == true)
          $internal_meetings[1] = $internal_meetings[1] + 1;
        else
        {
          if ($meeting->block_id == 0 and $meeting->is_internal == false)
            $district_level_meetings[1] = $district_level_meetings[1] + 1;
          else if ($meeting->block_id > 0 and $meeting->is_internal == false)
            $block_level_meetings[1] = $block_level_meetings[1] + 1;
        }
      }
      $meetings_3 = $this->Meetings_model->get_all($login_id, $_fromDate_1, $_toDate_1);
      foreach($meetings_3 as $meeting)
      {
        if ($meeting->is_internal == true)
          $internal_meetings[2] = $internal_meetings[2] + 1;
        else
        {
          if ($meeting->block_id == 0 and $meeting->is_internal == false)
            $district_level_meetings[2] = $district_level_meetings[2] + 1;
          else if ($meeting->block_id > 0 and $meeting->is_internal == false)
            $block_level_meetings[2] = $block_level_meetings[2] + 1;
        }
      }
      $meetings_4 = $this->Meetings_model->get_all($login_id, $_fromDate_2, $_toDate_2);
      foreach($meetings_4 as $meeting)
      {
        if ($meeting->is_internal == true)
          $internal_meetings[3] = $internal_meetings[3] + 1;
        else
        {
          if ($meeting->block_id == 0 and $meeting->is_internal == false)
            $district_level_meetings[3] = $district_level_meetings[3] + 1;
          else if ($meeting->block_id > 0 and $meeting->is_internal == false)
            $block_level_meetings[3] = $block_level_meetings[3] + 1;
        }
      }
      
      //(((n.he + n.le) + ((n.full_day_training + n.half_day_training) * 3) + (n.district_level + n.block_level)) / (n.total_days - (n.STATE_HOLIDAY + IFNULL(n.leaves, 0) + n.SUN_2_3_SAT + n.auth_dec_holidays))) visit_productivity
      //(n.calls / ((n.total_days) - (n.STATE_HOLIDAY + IFNULL(n.leaves, 0) + n.SUN_2_3_SAT + n.auth_dec_holidays))) call_productivity  
      
      $this->load->model('states_model');
      $state_data = $this->states_model->getById($state_id);

      //echo "<br>"."((($he_visit_count_1 + $le_visit_count_1) + ((".count($trainings_1).") * 3) + ($district_level_meetings[0] + $block_level_meetings[0])) / ($productive_days_1 - ($state_holiday_1 + $leaves[0] + $sun_2_3_sat_1 + $auth_dec_holidays[0])))";
      
      $visit_productivity_1 = round((($he_visit_count_1 + $le_visit_count_1) + ((count($trainings_1)) * 3) + ($district_level_meetings[0] + $block_level_meetings[0])) / ($productive_days_1 - ($state_holiday_1 + $leaves[0] + $sun_2_3_sat_1 + $auth_dec_holidays[0])),2);
      
      $visit_productivity_2 = round((($he_visit_count_2 + $le_visit_count_2) + ((count($trainings_2)) * 3) + ($district_level_meetings[1] + $block_level_meetings[1])) / ($productive_days_2 - ($state_holiday_2 + $leaves[1] + $sun_2_3_sat_2 + $auth_dec_holidays[1])),2);
      
      $visit_productivity_3 = round((($he_visit_count_3 + $le_visit_count_3) + ((count($trainings_3)) * 3) + ($district_level_meetings[2] + $block_level_meetings[2])) / ($productive_days_3 - ($state_holiday_3 + $leaves[2] + $sun_2_3_sat_3 + $auth_dec_holidays[2])),2);
      
      $visit_productivity_4 = round((($he_visit_count_4 + $le_visit_count_4) + ((count($trainings_4)) * 3) + ($district_level_meetings[3] + $block_level_meetings[3])) / ($productive_days_4 - ($state_holiday_4 + $leaves[3] + $sun_2_3_sat_4 + $auth_dec_holidays[3])),2);
      
      $visit_productivity = (($he_visit_count_3 + $le_visit_count_3) + ((count($trainings_3)) * 3) + ($district_level_meetings[2] + $block_level_meetings[2])) + (($he_visit_count_4 + $le_visit_count_4) + ((count($trainings_4)) * 3) + ($district_level_meetings[3] + $block_level_meetings[3]));
      
      $call_productivity_1 = round(count($calls_1) / (($productive_days_1) - ($state_holiday_1 + $leaves[0] + $sun_2_3_sat_1 + $auth_dec_holidays[0])),2);
      $call_productivity_2 = round(count($calls_2) / (($productive_days_2) - ($state_holiday_2 + $leaves[1] + $sun_2_3_sat_2 + $auth_dec_holidays[1])),2);
      $call_productivity_3 = round(count($calls_3) / (($productive_days_3) - ($state_holiday_3 + $leaves[2] + $sun_2_3_sat_3 + $auth_dec_holidays[2])),2);
      $call_productivity_4 = round(count($calls_4) / (($productive_days_4) - ($state_holiday_4 + $leaves[3] + $sun_2_3_sat_4 + $auth_dec_holidays[3])),2);
      $call_productivity = count($calls_3) + count($calls_4);
      
      $productivity = Array();
      $productivity['visit'][date("M",strtotime($_fromDate_1))]['fn1'] = $visit_productivity_3;
      $productivity['visit'][date("M",strtotime($_fromDate_1))]['fn2'] = $visit_productivity_4;
      $productivity['visit'][date("M",strtotime($_fromDate2))] = $visit_productivity_2;
      $productivity['visit'][date("M",strtotime($_fromDate1))] = $visit_productivity_1;
      $productivity['call'][date("M",strtotime($_fromDate_1))]['fn1'] = $call_productivity_3;
      $productivity['call'][date("M",strtotime($_fromDate_1))]['fn2'] = $call_productivity_4;
      $productivity['call'][date("M",strtotime($_fromDate2))] = $call_productivity_2;
      $productivity['call'][date("M",strtotime($_fromDate1))] = $call_productivity_1;
      $productivity['percentage']['visit'] = $visit_productivity;
      $productivity['percentage']['call'] = $call_productivity;
      $this->response(array(	"http_code" => 402,
                   'productivity' => $productivity));
      //print_r($this->data);
      exit;

    }

    public function myperformance_old_post() {
        /**
          $mar = Array();
          $productivity= Array();
          $visit= Array();
          $mar1=array('fn1'=>2.2,'fn2'=>2.1);
          $visit['mar']=$mar1;
          $visit['jan'] = 2.2;
          $visit['feb'] = 2.1;
          $mar2=array('fn1'=>2.2,'fn2'=>2.1);
          $call['mar']=$mar1;
          $call['jan'] = 2.2;
          $call['feb'] = 2.1;
          $productivity['visit']=$visit;
          $productivity['call']=$call;
          $productivity['percentage']=array('visit'=>2.2,'call'=>2.1);
          $this->response(array("http_code" => 402, 'productivity' => $productivity));

         */
		 $login_data = file_get_contents("php://input");
		 $login_data = json_decode($login_data,1);
		 
		// $login_id = $this->post('user_login_id');
        //FETCH LOGIN id
		$login_id = $login_data['Data']['user'];
		// $login_id = '2222';
        //$login_id = $this->login_id;
		/*$login_id = '2222';
			   //Session Data
        $session_data = $this->session->userdata('logged_in');
        $login_id   = $session_data['username'];
		$login_id = "'".$login_id."'";
        $spark_name = $session_data['name'];
        
        if(!empty($posted_login_id)){
            //Explode data into separte value 
            $final_posted_data = explode("-",$posted_login_id);  
            //Check for Empty Login _ID
            if(count($final_posted_data) > 0){
                //Login ID
                $login_id    = "'".$final_posted_data[0]."'"; 
                $spark_name  = $final_posted_data[1];
             
            }
        }
//current month
      //  $current_month = (Int) date("m", strtotime(date("Y-m-d")));
//fetch month name  from month integer
      //  $months = array(1 => 'Jan', 2 => 'Feb', 3 => 'Mar', 4 => 'Apr', 5 => 'May', 6 => 'Jun', 7 => 'Jul', 8 => 'Aug', 9 => 'Sep', 10 => 'Oct', 11 => 'Nov', 12 => 'Dec');
       // $current_month_shortname = $months[(int) $current_month];
//-------------------------current month forthnigtly--------------------------------//
// mysqli_next_result( $this->db->conn_id );
        //Call Procedure
        //$fortnightselectquery = $this->db->query("CALL sp_fetch_Fortnight_details($login_id)");
      //  $fortnightselectquery = $this->db->query("SELECT * FROM ssc_report_fortnight_spark WHERE login_id = $login_id");
	//	mysqli_next_result($this->db->conn_id);
        //Fetch Result
      //  $sparkResult = $fortnightselectquery->result();

        //Find total Fortnight record for specific Users
    //    $totalFortnight_for_Spark = count($sparkResult);

        //Starting loop value
    //    $start_value = 0;
        //Loop 
    //    while ($start_value < $totalFortnight_for_Spark) {

       //     $total_call_productivity = $sparkResult[$start_value]->{'call_productivity'};
     //       $total_visit_productivity = $sparkResult[$start_value]->{'visit_productivity'};
        //    $current_forthnightly = $sparkResult[$start_value]->{'current_forthnightly'};

            //FORTNIGHT -> 1
    //        if ($current_forthnightly == 1) {
//TOTAL VISIT PRODUCTIVITY
      //          $visitProductivityforgthnly1 = round($total_visit_productivity, 2);
//TOTAL CALL  PRODUCTIVITY
       //         $callProductivityforgthnly1 = round($total_call_productivity, 2);
            
// FORNIGHT -> 2
      //      else if ($current_forthnightly == 2) {
//TOTAL VISIT PRODUCTIIVTY
          //      $visitProductivityforgthnly2 = round($total_visit_productivity, 2);
//TOTAL CALL PRODUCTIIVTY
        //        $callProductivityforgthnly2 = round($total_call_productivity, 2);
       //     }

       //     $start_value = $start_value + 1;
      //  }
//Average visit and call productivity for CURRENT Month
     //   $total_call_productivity = ($callProductivityforgthnly1 + $callProductivityforgthnly2) / 2;
      //  $total_visit_productivity = ($visitProductivityforgthnly1 + $visitProductivityforgthnly2) / 2;
//
//check for null values
     //   if (is_null($visitProductivityforgthnly2)) {
       //     $visitProductivityforgthnly2 = 0;
       //     $callProductivityforgthnly2 = 0;
        //} else if (is_null($visitProductivityforgthnly1)) {
       //     $visitProductivityforgthnly1 = 0;
       //     $callProductivityforgthnly1 = 0;
       // }
//--------------------------------------end forthnigtly calculation------------------//
//PEVIOUS MONTH 1
//Check if current month -1 fall in previous year
  //      if (($current_month - 1) == 0) {
 //           $current_month = 12;

  //          $previous_month1_name = $months[(int) $current_month];
  //      } else {
 //           $current_month = $current_month - 1;
 //           $previous_month1_name = $months[(int) $current_month];
 //       }
//Check if current month -1 fall in previous year
//        if (($current_month - 1) == 0) {
 //           $current_month = 12;
 //           $previous_month2_name = $months[(int) $current_month];
 //       } else {
 //           $current_month = $current_month - 1;
  //          $previous_month2_name = $months[(int) $current_month];
 //       }
        
        //Fetch State Name
 //       $fetch_previousMonth = $this->db->query("SELECT DISTINCT sm_call_productivity, sm_visit_productivity from ssc_report_montly_spark Where sm_report_month = " . $current_month . " And sm_login_id = " . $login_id);
  //      mysqli_next_result($this->db->conn_id);
   //     $prevMnt_response = $fetch_previousMonth->row_array();
   //     if ($fetch_previousMonth->num_rows() > 0) {
            //VISIT PRODUCTIIVTY
  //          $pre1_visit_prod = ROUND($prevMnt_response['sm_visit_productivity'], 2);
  //          error_log("pre1_call_prod" . $pre1_visit_prod);
            //CALL  PRODUCTIIVTY
  //          $pre1_call_prod = ROUND($prevMnt_response['sm_call_productivity'], 2);
 //       } else {
            //VISIT PRODUCTIIVTY
  //          $pre1_visit_prod = 0;
            //CALL  PRODUCTIIVTY
  //          $pre1_call_prod = 0;
 //       }
        //PEVIOUS MONTH 2
        //Check if current month -1 fall in previous year
  //      if (($current_month - 1) == 0) {
  //          $current_month = 12;
  //          $previous_month2_name = $months[(int) $current_month];
  //      } else {
  //          $current_month = $current_month - 1;
  //          $previous_month2_name = $months[(int) $current_month];
        
        //Fetch State Name
 //       $fetch_previousMonth = $this->db->query("SELECT DISTINCT sm_call_productivity, sm_visit_productivity from ssc_report_montly_spark Where sm_report_month = " . $current_month . " And sm_login_id = " . $login_id);
  //      mysqli_next_result($this->db->conn_id);
  //      $prevMnt_response = $fetch_previousMonth->row_array();
        //MONTH 2
  //      if ($fetch_previousMonth->num_rows() > 0) {
            //VISIT PRODUCTIIVTY
  //          $pre2_visit_prod = ROUND($prevMnt_response['sm_visit_productivity'], 2);
  //          error_log("pre1_call_prod" . $pre1_visit_prod);
            //CALL  PRODUCTIIVTY
 //           $pre2_call_prod = ROUND($prevMnt_response['sm_call_productivity'], 2);
   //     } else {
            //VISIT PRODUCTIIVTY
 //           $pre2_visit_prod = 0;
            //CALL  PRODUCTIIVTY
   //         $pre2_call_prod = 0;
 //       }


        /*
          //Current Month
          $mar = Array();
          $productivity= Array();
          $visit= Array();
          $mar1=array('fn1'=>2.2,'fn2'=>2.1);
          $visit['mar']=$mar1;

          //PREVious months
          $visit['jan'] = 2.2; // $pre1_visit_prod
          $visit['feb'] = 2.1; // $pre2_visit_prod
          $mar2=array('fn1'=>2.2,'fn2'=>2.1);
          $call['mar']=$mar1;
          $call['jan'] = 2.2; // $pre1_call_prod
          $call['feb'] = 2.1;  // $pre2_call_prod
          $productivity['visit']=$visit;
          $productivity['call']
          =$call;
          $productivity['percentage']=array('visit'=>2.2,'call'=>2.1);

         */

//Dynamic months and data population

	
		$result = $this->account_performance_f1($login_id);
		// print_r($result);exit;
		$result_f2 = $this->account_performance_f2($login_id);
		$result1 = $this->account_performance_pv($login_id);
		$result2 = $this->account_performance_pv1($login_id);
		// foreach($result as $key=>$value){
			// $cm_result[$key] = $value;
		// }
		// foreach($result1 as $key1=>$value1){
			// $pv1_result[$key1] = $value1;
		// }
		// foreach($result2 as $key2=>$value2){
			// $pv2_result[$key2] = $value2;
		// }
		$total_visit = ($result[0]->visit_f1 + $result_f2[0]->visit_f2);
		$total_call = ($result[0]->call_f1 + $result_f2[0]->call_f2);
		
		$visit_result = array(	$result[0]->current_month=>array(	"fn1"=>$result[0]->visit_productivity_f1,
																	"fn2"=>$result_f2[0]->visit_productivity_f2),
								$result1[0]->prev_month=>$result1[0]->visit_productivity_prev,
								$result2[0]->prev_2month=>$result2[0]->visit_productivity_2prev);
		$call_result = array(	$result[0]->current_month=>array(	"fn1"=>$result[0]->call_productivity_f1,
																	"fn2"=>$result_f2[0]->call_productivity_f2),
								$result1[0]->prev_month=>$result1[0]->call_productivity_prev,
								$result2[0]->prev_2month=>$result2[0]->call_productivity_2prev);
		$count_result = array(	"visit"=>$total_visit,
								"call"=>$total_call);						
		
		// $perc_result = array(	"visit" => $result[0]->visit_percnt,
								// "call" => $result[0]->call_percnt);

		//$this->response($cm_result);
		 $this->response(array(	"http_code" => 402,
								 'productivity' => array('visit' => $visit_result, 'call' => $call_result, 'percentage' => $count_result)));


	// {
    // "http_code": 402,
    // "productivity": {
        // "visit": {
            // "Dec": {
                // "fn1": null,
                // "fn2": 0
            // },
            // "Nov": 0,
            // "Sep": 0
        // },
        // "call": {
            // "Dec": {
                // "fn1": null,
                // "fn2": 0
            // },
            // "Nov": 0,
            // "Sep": 0
        // },
        // "percentage": {
            // "visit": 0,
            // "call": 0
        // }
    // }
// }

// {
    // "http_code": 402,
    // "productivity": [
        // [
            // {
                // "visit_productivity_f1": "0.0000",
                // "visit_productivity_f2": "0.0000",
                // "call_productivity_f1": "0.0000",
                // "call_productivity_f2": "0.0000"
            // }
        // ],
        // [
            // {
                // "visit_productivity_prev": "0.6818",
                // "call_productivity_prev": "1.6364"
            // }
        // ],
        // [
            // {
                // "visit_productivity_2prev": "1.2000",
                // "call_productivity_2prev": "1.2500"
            // }
        // ]
    // ]
// }









        // $current_month_name = Array();
        // $productivity = Array();
        // $visit = Array();
        // $c_month_visit = array('fn1' => $visitProductivityforgthnly1, 'fn2' => $visitProductivityforgthnly2);
        // $visit[$current_month_shortname] = $c_month_visit;
        // $visit[$previous_month1_name] =  $pre1_visit_prod ;
        // $visit[$previous_month2_name] =  $pre2_visit_prod;

        // $c_month_call = array('fn1' => $callProductivityforgthnly1, 'fn2' => $callProductivityforgthnly2);

        // $call[$current_month_shortname] = $c_month_call;
        // $call[$previous_month1_name] = $pre1_call_prod;
        // $call[$previous_month2_name] = $pre2_call_prod ; 

        // $productivity['visit'] = $visit;
        // $productivity['call'] = $call;
        // $productivity['percentage'] = array('visit' => $total_visit_productivity, 'call' => $total_call_productivity);
        // $this->response(array("http_code" => 402, 'productivity' => $productivity));
    }

    public function validate_activity_date($activity_date) {
        $activity_date = date("Y-m-d", strtotime($activity_date));
        error_log("Account.php Activity Date valiation the date is =".$activity_date);
        error_log("date -7 ".date("Y-m-d", strtotime('-7 day')));
        if (($activity_date < date("Y-m-d", strtotime('-7 day'))) || ($activity_date > date("Y-m-d"))) {
            error_log(" activity date validation failed. ");
            return false;
        } else {
            error_log(" activity date validation passed. ");
            return true;
        }
    }

    public function validate_hierarchy($state_id, $district_ids, $block_id, $cluster_id,$android_local_id) {

        $this->load->model("districts_model");
        $district_array = $this->districts_model->check_district_by_State($district_ids, $state_id);
        if (empty($district_array))
            $this->response(array("http_code" => 200, 'local_id' => $android_local_id, 'status' => 'failed', 'sync_error' => true, 'message' => 'District does not exist in this State'));

        $this->load->model("blocks_model");
        $block_array = $this->blocks_model->check_block_by_DistrictState($block_id, $district_ids, $state_id);
        if (empty($block_array))
            $this->response(array("http_code" => 200, 'local_id' => $android_local_id, 'status' => 'failed', 'sync_error' => true, 'message' => 'Block does not exist in this District'));

        $this->load->model("clusters_model");
        $cluster_array = $this->clusters_model->check_cluster_by_blockDistrictState($cluster_id, $block_id, $district_ids, $state_id);
        if (empty($cluster_array))
            $this->response(array("http_code" => 200, 'local_id' => $android_local_id, 'status' => 'failed', 'sync_error' => true, 'message' => 'Cluster does not exist in this block'));
    }
	
    public function reportdata_post() {
		 $visit= Array();
		 $mar = Array();
		 $productivity= Array();
		
		$this->load->model("school_visits_model");		
        $json = $this->request->body;
        $user = $json['Data']['user'];     
		$result_month = $this->school_visits_model->spark_report_month($user);	
		$this->response(array("http_code" => 402, 'productivity' => $result_month ));	
        $result_fortnight = $this->school_visits_model->spark_report($user);
		
		
		
		if ($result_fortnight) {
            $sess_array = array();
				foreach ($result_fortnight as $row) {
					$val = "fn".$row->current_forthnightly;	
					$month = $row->report_month;
					//$monthname = date("m", strtotime($month));
					$monthName =  $this->monthName($month);
					$visit_fort[$val] = $row->visit_productivity;
					$call_fort[$val] = $row->call_productivity;					
				}  
				/*foreach ($result_month as $row) {
					$val = $this->monthName($row->report_month);	
					/$month = $row->report_month;
					$visit_fort[$val] = $row->sm_visit_productivity;
					$call_fort[$val] = $row->sm_call_productivity;					
				}  */
				$visit[$monthName]	= $visit_fort;	
				$call[$monthName]	= $call_fort;					
				$productivity['visit']=$visit;
				$productivity['call']=$call;
				//array_push($visit['mar'], $mar1);
				//array_push($sess['visit'], $visit);
                $this->response(array("http_code" => 402, 'productivity' => $productivity));
           
        } 
		//$this->response(array("http_code" => 402, 'productivity' => "kusum")); 
        
    }

    function monthName($month_int) {

        $month_int = (int) $month_int;

        $months = array("", "jan", "feb", "mar", "apr", "may", "jun", "jul", "aug", "sep", "oct", "nov", "dec");

        return $months[$month_int];
    }

    public function account_performance_f1($loginid){
	
	
	$select_stmt = "
SELECT IFNULL(round((((n.he + n.le) + ((n.full_day_training_f1 + n.half_day_training_f1) * 3) + (n.district_level_f1 + n.block_level_f1)) / (total_days_f1 - (n.STATE_HOLIDAY_f1 + IFNULL(n.leaves_f1, 0) + n.SUN_2_3_SAT_f1 + n.auth_dec_holidays_f1))),2),0) visit_productivity_f1
	,IFNULL(round((n.calls_f1 / ((total_days_f1) - (n.STATE_HOLIDAY_f1 + IFNULL(n.leaves_f1, 0) + n.SUN_2_3_SAT_f1 + n.auth_dec_holidays_f1))),2),0) call_productivity_f1
	,substr(CONVERT(monthname(curdate()) USING utf8), 1, 3) AS current_month
	,IFNULL((n.he + n.le),0) visit_f1
	, IFNULL(n.calls_f1,0) call_f1
FROM (
	SELECT *
	FROM (
		SELECT sst.id STATE
			,ssu.id user
			,(
				CASE 
					WHEN day(now()) < 15
						THEN day(now())
					ELSE 15
					END
				) total_days_f1
			,(
				SELECT count(*)
				FROM ssc_trainings sstr
				WHERE ssu.district_id = sstr.district_id
					AND sstr.user_id = ssu.id
					AND DATE (sstr.activity_date) BETWEEN CAST(DATE_FORMAT(NOW(), '%Y-%m-01') AS DATE)
						AND (
								CASE 
									WHEN day(now()) < 15
										THEN cast(now() AS DATE)
									ELSE CAST(DATE_FORMAT(NOW(), '%Y-%m-15') AS DATE)
									END)
				) full_day_training_f1
			,(
				SELECT count(*)
				FROM ssc_trainings sstr
				WHERE sstr.user_id = ssu.id
					AND ssu.district_id <> sstr.district_id
					AND sstr.training_duration = '24:00'
					AND DATE (sstr.activity_date) BETWEEN CAST(DATE_FORMAT(NOW(), '%Y-%m-01') AS DATE)
						AND (
								CASE 
									WHEN day(now()) < 15
										THEN cast(now() AS DATE)
									ELSE CAST(DATE_FORMAT(NOW(), '%Y-%m-15') AS DATE)
									END)
				) half_day_training_f1
			,(
				SELECT COUNT(*)
				FROM ssc_meetings ssm
				WHERE ssm.state_id = sst.id
					AND ssm.user_id = ssu.id
					AND ssm.block_id = 0
					AND ssm.is_internal IS FALSE
					AND DATE (ssm.activity_date) BETWEEN CAST(DATE_FORMAT(NOW(), '%Y-%m-01') AS DATE)
						AND (
								CASE 
									WHEN day(now()) < 15
										THEN cast(now() AS DATE)
									ELSE CAST(DATE_FORMAT(NOW(), '%Y-%m-15') AS DATE)
									END
								)
				) district_level_f1
			,(
				SELECT COUNT(*)
				FROM ssc_meetings ssm
				WHERE ssm.state_id = sst.id
					AND ssm.user_id = ssu.id
					AND ssm.block_id <> 0
					AND ssm.is_internal IS FALSE
					AND DATE (ssm.activity_date) BETWEEN CAST(DATE_FORMAT(NOW(), '%Y-%m-01') AS DATE)
						AND (
								CASE 
									WHEN day(now()) < 15
										THEN cast(now() AS DATE)
									ELSE CAST(DATE_FORMAT(NOW(), '%Y-%m-15') AS DATE)
									END
								)
				) block_level_f1
			,(
				SELECT COUNT(*)
				FROM ssc_calls sscc
				WHERE sscc.state_id = sst.id
					AND sscc.user_id = ssu.id
					AND DATE (sscc.activity_date) BETWEEN CAST(DATE_FORMAT(NOW(), '%Y-%m-01') AS DATE)
						AND (
								CASE 
									WHEN day(now()) < 15
										THEN cast(now() AS DATE)
									ELSE CAST(DATE_FORMAT(NOW(), '%Y-%m-15') AS DATE)
									END
								)
				) calls_f1
			,(
				SELECT COUNT(*)
				FROM ssc_no_visit_days nvd
				WHERE nvd.state_id = sst.id
					AND nvd.user_id = ssu.id
					AND no_visit_reason = 'Local Authority Declared Holiday'
					AND DATE (nvd.activity_date) BETWEEN CAST(DATE_FORMAT(NOW(), '%Y-%m-01') AS DATE)
						AND (
								CASE 
									WHEN day(now()) < 15
										THEN cast(now() AS DATE)
									ELSE CAST(DATE_FORMAT(NOW(), '%Y-%m-15') AS DATE)
									END
								)
				) auth_dec_holidays_f1
			,(
				SELECT COUNT(*)
				FROM ssc_offdays ssco
				WHERE DATE (ssco.activity_date) BETWEEN CAST(DATE_FORMAT(NOW(), '%Y-%m-01') AS DATE)
						AND (
								CASE 
									WHEN day(now()) < 15
										THEN cast(now() AS DATE)
									ELSE CAST(DATE_FORMAT(NOW(), '%Y-%m-15') AS DATE)
									END
								)
				) SUN_2_3_SAT_f1
			,(
				SELECT COUNT(*)
				FROM ssc_holidays ssch
				WHERE DATE (ssch.activity_date) BETWEEN CAST(DATE_FORMAT(NOW(), '%Y-%m-01') AS DATE)
						AND (
								CASE 
									WHEN day(now()) < 15
										THEN cast(now() AS DATE)
									ELSE CAST(DATE_FORMAT(NOW(), '%Y-%m-15') AS DATE)
									END
								)
					AND (
						stateid = ssu.state_id
						OR NATIONAL = 1
						OR user_id = ssu.id
						)
				) STATE_HOLIDAY_f1
			,(
				SELECT SUM(DATEDIFF(sscln.cal_leave_end_date, sscln.leave_from_date) + 1)
				FROM (
					SELECT sscl.leave_end_date
						,sscl.leave_from_date
						,CASE 
							WHEN sscl.leave_end_date > LAST_DAY(sscl.leave_from_date)
								THEN LAST_DAY(sscl.leave_from_date)
							ELSE sscl.leave_end_date
							END cal_leave_end_date
						,sscl.state_id
						,sscl.user_id
					FROM ssc_leaves sscl
					WHERE DATE (sscl.leave_from_date) BETWEEN CAST(DATE_FORMAT(NOW(), '%Y-%m-01') AS DATE)
							AND (
									CASE 
										WHEN day(now()) < 15
											THEN cast(now() AS DATE)
										ELSE CAST(DATE_FORMAT(NOW(), '%Y-%m-15') AS DATE)
										END
									)
					) sscln
				WHERE sscln.state_id = sst.id
					AND sscln.user_id = ssu.id
				) leaves_f1
		FROM ssc_states sst
			,ssc_users ssu
			,ssc_school_visits sv
		WHERE sst.id = ssu.state_id
			AND ssu.login_id = '".$loginid."'
		GROUP BY sst.id
			,ssu.id
		) j
	LEFT JOIN (
		SELECT f.id user_id
			,f.user_name
			,f.district_name
			,f.avrg
			,count(CASE 
					WHEN f.overallcnt = 'HE'
						THEN f.district_name
					ELSE NULL
					END) he
			,count(CASE 
					WHEN f.overallcnt = 'LE'
						THEN f.district_name
					ELSE NULL
					END) le
		FROM (
			SELECT d.id
				,d.user_name
				,d.district_name
				,d.avrg
				,ifnull((
						CASE 
							WHEN d.no_of_students >= d.avrg
								THEN 'HE'
							WHEN d.no_of_students < d.avrg
								THEN 'LE'
							ELSE 0
							END
						), 0) overallcnt
			FROM (
				SELECT ssu.id
					,ssu.name user_name
					,ssu.login_id
					,s.*
				FROM ssc_users ssu
				INNER JOIN (
					SELECT w.user_id
						,w.state_id
						,w.district_id
						,ssd.name district_name
						,w.school_id
						,w.activity
						,round(w.average, 0) avrg
						,w.no_of_students
					FROM ssc_districts ssd
					INNER JOIN (
						SELECT DISTINCT sv.district_id
							,sv.state_id
							,sv.school_id
							,sv.user_id
							,cast(sv.activity_date AS DATE) activity
							,r.average
							,r.no_of_students
						FROM ssc_school_visits sv
						INNER JOIN (
							SELECT *
							FROM ssc_schools sss
							INNER JOIN (
								SELECT sss.district_id district
									,sss.avg_student average
								FROM ssc_schools sss
								GROUP BY district
								) q ON sss.district_id = q.district
							) r ON sv.school_id = r.id
							AND sv.district_id = r.district_id
						) w ON w.district_id = ssd.id
					WHERE DATE (w.activity) BETWEEN CAST(DATE_FORMAT(NOW(), '%Y-%m-01') AS DATE)
							AND (
									CASE 
										WHEN day(now()) < 15
											THEN cast(now() AS DATE)
										ELSE CAST(DATE_FORMAT(NOW(), '%Y-%m-15') AS DATE)
										END
									)
					) s ON ssu.id = s.user_id
					AND ssu.login_id = '".$loginid."'
				) d
			) f
		GROUP BY f.user_name
			,f.district_name
		) l ON l.user_id = j.user
	) n";
	// print $select_stmt;exit;
       $select_query = $this->db->query($select_stmt);
       
	//Execute query 
        $row = $select_query->result();
		// echo("<pre>");
		// print_r($row);
		// die();
        return ($row);
	}

    public function account_performance_f2($loginid){
	
	
	$select_stmt = "
SELECT ifnull(round(ifnull((((n.he + n.le) + ((n.full_day_training_f2 + n.half_day_training_f2) * 3) + (n.district_level_f2 + n.block_level_f2)) / (total_days_f2 - (n.STATE_HOLIDAY_f2 + IFNULL(n.leaves_f2, 0) + n.SUN_2_3_SAT_f2 + n.auth_dec_holidays_f2))), 0),2),0) visit_productivity_f2
	,ifnull(round(ifnull((n.calls_f2 / ((total_days_f2) - (n.STATE_HOLIDAY_f2 + IFNULL(n.leaves_f2, 0) + n.SUN_2_3_SAT_f2 + n.auth_dec_holidays_f2))), 0),2),0) call_productivity_f2
	,substr(CONVERT(monthname(curdate()) USING utf8), 1, 3) AS current_month
	,ifnull((n.he + n.le), 0) visit_f2
	,ifnull(n.calls_f2,0) call_f2
FROM (
	SELECT *
	FROM (
		SELECT sst.id STATE
			,ssu.id user
			,(
				CASE 
					WHEN day(now()) > 15
						AND day(now()) <= day(last_day(CURRENT_DATE ()))
						THEN (day(now()) - 15)
					ELSE 0
					END
				) total_days_f2
			,(
				SELECT count(*)
				FROM ssc_trainings sstr
				WHERE ssu.district_id = sstr.district_id
					AND sstr.user_id = ssu.id
					AND DATE (sstr.activity_date) BETWEEN CAST(DATE_FORMAT(NOW(), '%Y-%m-16') AS DATE)
						AND (
								CASE 
									WHEN  day(now()) > 15 and day(now()) < day(last_day(CURRENT_DATE ()))
										THEN cast(now() AS DATE)
									ELSE last_day(CURRENT_DATE ())
									END)
				) full_day_training_f2
			,(
				SELECT count(*)
				FROM ssc_trainings sstr
				WHERE sstr.user_id = ssu.id
					AND ssu.district_id <> sstr.district_id
					AND sstr.training_duration = '24:00'
					AND DATE (sstr.activity_date) BETWEEN CAST(DATE_FORMAT(NOW(), '%Y-%m-16') AS DATE)
						AND (
								CASE 
									WHEN  day(now()) > 15 and day(now()) < day(last_day(CURRENT_DATE ()))
										THEN cast(now() AS DATE)
									ELSE last_day(CURRENT_DATE ())
									END)
				) half_day_training_f2
			,(
				SELECT COUNT(*)
				FROM ssc_meetings ssm
				WHERE ssm.state_id = sst.id
					AND ssm.user_id = ssu.id
					AND ssm.block_id = 0
					AND ssm.is_internal IS FALSE
					AND DATE (ssm.activity_date) BETWEEN CAST(DATE_FORMAT(NOW(), '%Y-%m-16') AS DATE)
						AND (
								CASE 
									WHEN  day(now()) > 15 and day(now()) < day(last_day(CURRENT_DATE ()))
										THEN cast(now() AS DATE)
									ELSE last_day(CURRENT_DATE ())
									END
								)
				) district_level_f2
			,(
				SELECT COUNT(*)
				FROM ssc_meetings ssm
				WHERE ssm.state_id = sst.id
					AND ssm.user_id = ssu.id
					AND ssm.block_id <> 0
					AND ssm.is_internal IS FALSE
					AND DATE (ssm.activity_date) BETWEEN CAST(DATE_FORMAT(NOW(), '%Y-%m-16') AS DATE)
						AND (
								CASE 
									WHEN day(now()) > 15 and  day(now()) < day(last_day(CURRENT_DATE ()))
										THEN cast(now() AS DATE)
									ELSE last_day(CURRENT_DATE ())
									END
								)
				) block_level_f2
			,(
				SELECT COUNT(*)
				FROM ssc_calls sscc
				WHERE sscc.state_id = sst.id
					AND sscc.user_id = ssu.id
					AND DATE (sscc.activity_date) BETWEEN CAST(DATE_FORMAT(NOW(), '%Y-%m-16') AS DATE)
						AND (
								CASE 
									WHEN day(now()) > 15 and  day(now()) < day(last_day(CURRENT_DATE ()))
										THEN cast(now() AS DATE)
									ELSE last_day(CURRENT_DATE ())
									END
								)
				) calls_f2
			,(
				SELECT COUNT(*)
				FROM ssc_no_visit_days nvd
				WHERE nvd.state_id = sst.id
					AND nvd.user_id = ssu.id
					AND no_visit_reason = 'Local Authority Declared Holiday'
					AND DATE (nvd.activity_date) BETWEEN CAST(DATE_FORMAT(NOW(), '%Y-%m-16') AS DATE)
						AND (
								CASE 
									WHEN  day(now()) > 15 and day(now()) < day(last_day(CURRENT_DATE ()))
										THEN cast(now() AS DATE)
									ELSE last_day(CURRENT_DATE ())
									END
								)
				) auth_dec_holidays_f2
			,(
				SELECT COUNT(*)
				FROM ssc_offdays ssco
				WHERE DATE (ssco.activity_date) BETWEEN CAST(DATE_FORMAT(NOW(), '%Y-%m-16') AS DATE)
						AND (
								CASE 
									WHEN  day(now()) > 15 and day(now()) < day(last_day(CURRENT_DATE ()))
										THEN cast(now() AS DATE)
									ELSE last_day(CURRENT_DATE ())
									END
								)
				) SUN_2_3_SAT_f2
			,(
				SELECT COUNT(*)
				FROM ssc_holidays ssch
				WHERE DATE (ssch.activity_date) BETWEEN CAST(DATE_FORMAT(NOW(), '%Y-%m-16') AS DATE)
						AND (
								CASE 
									WHEN  day(now()) > 15 and day(now()) < day(last_day(CURRENT_DATE ()))
										THEN cast(now() AS DATE)
									ELSE last_day(CURRENT_DATE ())
									END
								)
					AND (
						stateid = ssu.state_id
						OR NATIONAL = 1
						OR user_id = ssu.id
						)
				) STATE_HOLIDAY_f2
			,(
				SELECT SUM(DATEDIFF(sscln.cal_leave_end_date, sscln.leave_from_date) + 1)
				FROM (
					SELECT sscl.leave_end_date
						,sscl.leave_from_date
						,CASE 
							WHEN sscl.leave_end_date > LAST_DAY(sscl.leave_from_date)
								THEN LAST_DAY(sscl.leave_from_date)
							ELSE sscl.leave_end_date
							END cal_leave_end_date
						,sscl.state_id
						,sscl.user_id
					FROM ssc_leaves sscl
					WHERE DATE (sscl.leave_from_date) BETWEEN CAST(DATE_FORMAT(NOW(), '%Y-%m-16') AS DATE)
							AND (
									CASE 
										WHEN day(now()) > 15 and  day(now()) < day(last_day(CURRENT_DATE ()))
											THEN cast(now() AS DATE)
										ELSE last_day(CURRENT_DATE ())
										END
									)
					) sscln
				WHERE sscln.state_id = sst.id
					AND sscln.user_id = ssu.id
				) leaves_f2
		FROM ssc_states sst
			,ssc_users ssu
			,ssc_school_visits sv
		WHERE sst.id = ssu.state_id
			AND ssu.login_id = '".$loginid."'
		GROUP BY sst.id
			,ssu.id
		) j
	LEFT JOIN (
		SELECT f.id user_id
			,f.user_name
			,f.district_name
			,f.avrg
			,count(CASE 
					WHEN f.overallcnt = 'HE'
						THEN f.district_name
					ELSE NULL
					END) he
			,count(CASE 
					WHEN f.overallcnt = 'LE'
						THEN f.district_name
					ELSE NULL
					END) le
		FROM (
			SELECT d.id
				,d.user_name
				,d.district_name
				,d.avrg
				,ifnull((
						CASE 
							WHEN d.no_of_students >= d.avrg
								THEN 'HE'
							WHEN d.no_of_students < d.avrg
								THEN 'LE'
							ELSE 0
							END
						), 0) overallcnt
			FROM (
				SELECT ssu.id
					,ssu.name user_name
					,s.*
				FROM ssc_users ssu
				INNER JOIN (
					SELECT w.user_id
						,w.state_id
						,w.district_id
						,ssd.name district_name
						,w.school_id
						,w.activity
						,round(w.average, 0) avrg
						,w.no_of_students
					FROM ssc_districts ssd
					INNER JOIN (
						SELECT DISTINCT sv.district_id
							,sv.state_id
							,sv.school_id
							,sv.user_id
							,cast(sv.activity_date AS DATE) activity
							,r.average
							,r.no_of_students
						FROM ssc_school_visits sv
						INNER JOIN (
							SELECT *
							FROM ssc_schools sss
							INNER JOIN (
								SELECT sss.district_id district
									,sss.avg_student average
								FROM ssc_schools sss
								GROUP BY district
								) q ON sss.district_id = q.district
							) r ON sv.school_id = r.id
							AND sv.district_id = r.district_id
						) w ON w.district_id = ssd.id
					WHERE DATE (w.activity) BETWEEN CAST(DATE_FORMAT(NOW(), '%Y-%m-16') AS DATE)
							AND (
									CASE 
										WHEN  day(now()) > 15 and day(now()) < day(last_day(CURRENT_DATE ()))
											THEN cast(now() AS DATE)
										ELSE last_day(CURRENT_DATE ())
										END
									)
					) s ON ssu.id = s.user_id
					AND ssu.login_id = '".$loginid."'
				) d
			) f
		GROUP BY f.user_name
			,f.district_name
		) l ON l.user_id = j.user
	) n";
	// print $select_stmt;exit;
       $select_query = $this->db->query($select_stmt);
       
	//Execute query 
        $row = $select_query->result();
		// echo("<pre>");
		// print_r($row);
		// die();
        return ($row);
	}
	
    public function account_performance_pv($loginid){
	
$select_stmt = "
SELECT ifnull(round((((n.he + n.le) + ((n.full_day_training_prev + n.half_day_training_prev) * 3) + (n.district_level_prev + n.block_level_prev)) / (total_prev - (n.STATE_HOLIDAY_prev + IFNULL(n.leaves_prev, 0) + n.SUN_2_3_SAT_prev + n.auth_dec_holidays_prev))),2),0) visit_productivity_prev
	,ifnull(round((n.calls_prev / ((total_prev) - (n.STATE_HOLIDAY_prev + IFNULL(n.leaves_prev, 0) + n.SUN_2_3_SAT_prev + n.auth_dec_holidays_prev))),2),0) call_productivity_prev
	,substr(convert(monthname(now() - interval 1 month) using utf8), 1, 3) AS prev_month
FROM (
	SELECT *
	FROM (
		SELECT sst.id STATE
			,ssu.id user
			,day(LAST_DAY(now() - interval 1 month)) total_prev
			,(
				SELECT count(*)
				FROM ssc_trainings sstr
				WHERE ssu.district_id = sstr.district_id
					AND sstr.user_id = ssu.id
					AND DATE (sstr.activity_date) BETWEEN CONCAT(
								date_format(LAST_DAY(now() - interval 1 month), '%Y-%m-')
								,'01'
								)
						AND LAST_DAY(now() - interval 1 month)
				) full_day_training_prev
			,(
				SELECT count(*)
				FROM ssc_trainings sstr
				WHERE sstr.user_id = ssu.id
					AND ssu.district_id <> sstr.district_id
					AND sstr.training_duration = '24:00'
					AND DATE (sstr.activity_date) BETWEEN CONCAT(
								date_format(LAST_DAY(now() - interval 1 month), '%Y-%m-')
								,'01'
								)
						AND LAST_DAY(now() - interval 1 month)
				) half_day_training_prev
			,(
				SELECT COUNT(*)
				FROM ssc_meetings ssm
				WHERE ssm.state_id = sst.id
					AND ssm.user_id = ssu.id
					AND ssm.block_id = 0
					AND ssm.is_internal IS FALSE
					AND DATE (ssm.activity_date) BETWEEN CONCAT(
								date_format(LAST_DAY(now() - interval 1 month), '%Y-%m-')
								,'01'
								)
						AND LAST_DAY(now() - interval 1 month)
				) district_level_prev
			,(
				SELECT COUNT(*)
				FROM ssc_meetings ssm
				WHERE ssm.state_id = sst.id
					AND ssm.user_id = ssu.id
					AND ssm.block_id <> 0
					AND ssm.is_internal IS FALSE
					AND DATE (ssm.activity_date) BETWEEN CONCAT(
								date_format(LAST_DAY(now() - interval 1 month), '%Y-%m-')
								,'01'
								)
						AND LAST_DAY(now() - interval 1 month)
				) block_level_prev
			,(
				SELECT COUNT(*)
				FROM ssc_calls sscc
				WHERE sscc.state_id = sst.id
					AND sscc.user_id = ssu.id
					AND DATE (sscc.activity_date) BETWEEN CONCAT(
								date_format(LAST_DAY(now() - interval 1 month), '%Y-%m-')
								,'01'
								)
						AND LAST_DAY(now() - interval 1 month)
				) calls_prev
			,(
				SELECT COUNT(*)
				FROM ssc_no_visit_days nvd
				WHERE nvd.state_id = sst.id
					AND nvd.user_id = ssu.id
					AND no_visit_reason = 'Local Authority Declared Holiday'
					AND DATE (nvd.activity_date) BETWEEN CONCAT(
								date_format(LAST_DAY(now() - interval 1 month), '%Y-%m-')
								,'01'
								)
						AND LAST_DAY(now() - interval 1 month)
				) auth_dec_holidays_prev
			,(
				SELECT COUNT(*)
				FROM ssc_offdays ssco
				WHERE DATE (ssco.activity_date) BETWEEN CONCAT(
								date_format(LAST_DAY(now() - interval 1 month), '%Y-%m-')
								,'01'
								)
						AND LAST_DAY(now() - interval 1 month)
				) SUN_2_3_SAT_prev
			,(
				SELECT COUNT(*)
				FROM ssc_holidays ssch
				WHERE DATE (ssch.activity_date) BETWEEN CONCAT(
								date_format(LAST_DAY(now() - interval 1 month), '%Y-%m-')
								,'01'
								)
						AND LAST_DAY(now() - interval 1 month)
					AND (
						stateid = ssu.state_id
						OR NATIONAL = 1
						OR user_id = ssu.id
						)
				) STATE_HOLIDAY_prev
			,(
				SELECT SUM(DATEDIFF(sscln.cal_leave_end_date, sscln.leave_from_date) + 1)
				FROM (
					SELECT sscl.leave_end_date
						,sscl.leave_from_date
						,CASE 
							WHEN sscl.leave_end_date > LAST_DAY(sscl.leave_from_date)
								THEN LAST_DAY(sscl.leave_from_date)
							ELSE sscl.leave_end_date
							END cal_leave_end_date
						,sscl.state_id
						,sscl.user_id
					FROM ssc_leaves sscl
					WHERE DATE (sscl.leave_from_date) BETWEEN CONCAT(
									date_format(LAST_DAY(now() - interval 1 month), '%Y-%m-')
									,'01'
									)
							AND LAST_DAY(now() - interval 1 month)
					) sscln
				WHERE sscln.state_id = sst.id
					AND sscln.user_id = ssu.id
				) leaves_prev
		FROM ssc_states sst
			,ssc_users ssu
			,ssc_school_visits sv
		WHERE sst.id = ssu.state_id
			AND ssu.login_id = '".$loginid."'
		GROUP BY sst.id
			,ssu.id
		) j
	LEFT JOIN (
		SELECT f.id user_id
			,f.user_name
			,f.district_name
			,f.avrg
			,count(CASE 
					WHEN f.overallcnt = 'HE'
						THEN f.district_name
					ELSE NULL
					END) he
			,count(CASE 
					WHEN f.overallcnt = 'LE'
						THEN f.district_name
					ELSE NULL
					END) le
		FROM (
			SELECT d.id
				,d.user_name
				,d.district_name
				,d.avrg
				,ifnull((
						CASE 
							WHEN d.no_of_students >= d.avrg
								THEN 'HE'
							WHEN d.no_of_students < d.avrg
								THEN 'LE'
							ELSE 0
							END
						), 0) overallcnt
			FROM (
				SELECT ssu.id
					,ssu.name user_name
					,s.*
				FROM ssc_users ssu
				INNER JOIN (
					SELECT w.user_id
						,w.state_id
						,w.district_id
						,ssd.name district_name
						,w.school_id
						,w.activity
						,round(w.average, 0) avrg
						,w.no_of_students
					FROM ssc_districts ssd
					INNER JOIN (
						SELECT DISTINCT sv.district_id
							,sv.state_id
							,sv.school_id
							,sv.user_id
							,cast(sv.activity_date AS DATE) activity
							,r.average
							,r.no_of_students
						FROM ssc_school_visits sv
						INNER JOIN (
							SELECT *
							FROM ssc_schools sss
							INNER JOIN (
								SELECT sss.district_id district
									,sss.avg_student average
								FROM ssc_schools sss
								GROUP BY district
								) q ON sss.district_id = q.district
							) r ON sv.school_id = r.id
							AND sv.district_id = r.district_id
						) w ON w.district_id = ssd.id
					WHERE DATE (w.activity) BETWEEN CONCAT(
									date_format(LAST_DAY(now() - interval 1 month), '%Y-%m-')
									,'01'
									)
							AND LAST_DAY(now() - interval 1 month)
					) s ON ssu.id = s.user_id
					AND ssu.login_id = '".$loginid."'
				) d
			) f
		GROUP BY f.user_name
			,f.district_name
		) l ON l.user_id = j.user
	) n";
		// print $select_stmt;exit;
       $select_query = $this->db->query($select_stmt);
       
	//Execute query 
        $row = $select_query->result();
			// print $row;exit;
		// echo("<pre>");
		// print_r($row);
		// die();
        return ($row);
	}
	
    public function account_performance_pv1($loginid){
	
		$select_stmt ="
SELECT ifnull(round((((n.he + n.le) + ((n.full_day_training_2prev + n.half_day_training_2prev) * 3) + (n.district_level_2prev + n.block_level_2prev)) / (total_2prev - (n.STATE_HOLIDAY_2prev + IFNULL(n.leaves_2prev, 0) + n.SUN_2_3_SAT_2prev + n.auth_dec_holidays_2prev))),2),0) visit_productivity_2prev
	,ifnull(round((n.calls_2prev / ((total_2prev) - (n.STATE_HOLIDAY_2prev + IFNULL(n.leaves_2prev, 0) + n.SUN_2_3_SAT_2prev + n.auth_dec_holidays_2prev))),2),0) call_productivity_2prev
	,substr(convert(monthname(now() - interval 2 month) using utf8), 1, 3) AS prev_2month
FROM (
	SELECT *
	FROM (
		SELECT sst.id STATE
			,ssu.id user
			,day(LAST_DAY(now() - interval 2 month)) total_2prev
			,(
				SELECT count(*)
				FROM ssc_trainings sstr
				WHERE ssu.district_id = sstr.district_id
					AND sstr.user_id = ssu.id
					AND DATE (sstr.activity_date) BETWEEN CONCAT(
								date_format(LAST_DAY(now() - interval 2 month), '%Y-%m-')
								,'01'
								)
						AND LAST_DAY(now() - interval 2 month)
				) full_day_training_2prev
			,(
				SELECT count(*)
				FROM ssc_trainings sstr
				WHERE sstr.user_id = ssu.id
					AND ssu.district_id <> sstr.district_id
					AND sstr.training_duration = '24:00'
					AND DATE (sstr.activity_date) BETWEEN CONCAT(
								date_format(LAST_DAY(now() - interval 2 month), '%Y-%m-')
								,'01'
								)
						AND LAST_DAY(now() - interval 2 month)
				) half_day_training_2prev
			,(
				SELECT COUNT(*)
				FROM ssc_meetings ssm
				WHERE ssm.state_id = sst.id
					AND ssm.user_id = ssu.id
					AND ssm.block_id = 0
					AND ssm.is_internal IS FALSE
					AND DATE (ssm.activity_date) BETWEEN CONCAT(
								date_format(LAST_DAY(now() - interval 2 month), '%Y-%m-')
								,'01'
								)
						AND LAST_DAY(now() - interval 2 month)
				) district_level_2prev
			,(
				SELECT COUNT(*)
				FROM ssc_meetings ssm
				WHERE ssm.state_id = sst.id
					AND ssm.user_id = ssu.id
					AND ssm.block_id <> 0
					AND ssm.is_internal IS FALSE
					AND DATE (ssm.activity_date) BETWEEN CONCAT(
								date_format(LAST_DAY(now() - interval 2 month), '%Y-%m-')
								,'01'
								)
						AND LAST_DAY(now() - interval 2 month)
				) block_level_2prev
			,(
				SELECT COUNT(*)
				FROM ssc_calls sscc
				WHERE sscc.state_id = sst.id
					AND sscc.user_id = ssu.id
					AND DATE (sscc.activity_date) BETWEEN CONCAT(
								date_format(LAST_DAY(now() - interval 2 month), '%Y-%m-')
								,'01'
								)
						AND LAST_DAY(now() - interval 2 month)
				) calls_2prev
			,(
				SELECT COUNT(*)
				FROM ssc_no_visit_days nvd
				WHERE nvd.state_id = sst.id
					AND nvd.user_id = ssu.id
					AND no_visit_reason = 'Local Authority Declared Holiday'
					AND DATE (nvd.activity_date) BETWEEN CONCAT(
								date_format(LAST_DAY(now() - interval 2 month), '%Y-%m-')
								,'01'
								)
						AND LAST_DAY(now() - interval 2 month)
				) auth_dec_holidays_2prev
			,(
				SELECT COUNT(*)
				FROM ssc_offdays ssco
				WHERE DATE (ssco.activity_date) BETWEEN CONCAT(
								date_format(LAST_DAY(now() - interval 2 month), '%Y-%m-')
								,'01'
								)
						AND LAST_DAY(now() - interval 2 month)
				) SUN_2_3_SAT_2prev
			,(
				SELECT COUNT(*)
				FROM ssc_holidays ssch
				WHERE DATE (ssch.activity_date) BETWEEN CONCAT(
								date_format(LAST_DAY(now() - interval 2 month), '%Y-%m-')
								,'01'
								)
						AND LAST_DAY(now() - interval 2 month)
					AND (
						stateid = ssu.state_id
						OR NATIONAL = 1
						OR user_id = ssu.id
						)
				) STATE_HOLIDAY_2prev
			,(
				SELECT SUM(DATEDIFF(sscln.cal_leave_end_date, sscln.leave_from_date) + 1)
				FROM (
					SELECT sscl.leave_end_date
						,sscl.leave_from_date
						,CASE 
							WHEN sscl.leave_end_date > LAST_DAY(sscl.leave_from_date)
								THEN LAST_DAY(sscl.leave_from_date)
							ELSE sscl.leave_end_date
							END cal_leave_end_date
						,sscl.state_id
						,sscl.user_id
					FROM ssc_leaves sscl
					WHERE DATE (sscl.leave_from_date) BETWEEN CONCAT(
									date_format(LAST_DAY(now() - interval 2 month), '%Y-%m-')
									,'01'
									)
							AND LAST_DAY(now() - interval 2 month)
					) sscln
				WHERE sscln.state_id = sst.id
					AND sscln.user_id = ssu.id
				) leaves_2prev
		FROM ssc_states sst
			,ssc_users ssu
			,ssc_school_visits sv
		WHERE sst.id = ssu.state_id
			AND ssu.login_id = '".$loginid."'
		GROUP BY sst.id
			,ssu.id
		) j
	LEFT JOIN (
		SELECT f.id user_id
			,f.user_name
			,f.district_name
			,f.avrg
			,count(CASE 
					WHEN f.overallcnt = 'HE'
						THEN f.district_name
					ELSE NULL
					END) he
			,count(CASE 
					WHEN f.overallcnt = 'LE'
						THEN f.district_name
					ELSE NULL
					END) le
		FROM (
			SELECT d.id
				,d.user_name
				,d.district_name
				,d.avrg
				,ifnull((
						CASE 
							WHEN d.no_of_students >= d.avrg
								THEN 'HE'
							WHEN d.no_of_students < d.avrg
								THEN 'LE'
							ELSE 0
							END
						), 0) overallcnt
			FROM (
				SELECT ssu.id
					,ssu.name user_name
					,s.*
				FROM ssc_users ssu
				INNER JOIN (
					SELECT w.user_id
						,w.state_id
						,w.district_id
						,ssd.name district_name
						,w.school_id
						,w.activity
						,round(w.average, 0) avrg
						,w.no_of_students
					FROM ssc_districts ssd
					INNER JOIN (
						SELECT DISTINCT sv.district_id
							,sv.state_id
							,sv.school_id
							,sv.user_id
							,cast(sv.activity_date AS DATE) activity
							,r.average
							,r.no_of_students
						FROM ssc_school_visits sv
						INNER JOIN (
							SELECT *
							FROM ssc_schools sss
							INNER JOIN (
								SELECT sss.district_id district
									,sss.avg_student average
								FROM ssc_schools sss
								GROUP BY district
								) q ON sss.district_id = q.district
							) r ON sv.school_id = r.id
							AND sv.district_id = r.district_id
						) w ON w.district_id = ssd.id
					WHERE DATE (w.activity) BETWEEN CONCAT(
									date_format(LAST_DAY(now() - interval 2 month), '%Y-%m-')
									,'01'
									)
							AND LAST_DAY(now() - interval 2 month)
					) s ON ssu.id = s.user_id
					AND ssu.login_id = '".$loginid."'
				) d
			) f
		GROUP BY f.user_name
			,f.district_name
		) l ON l.user_id = j.user
	) n";
		// print $select_stmt;exit;
		
		$select_query = $this->db->query($select_stmt);
       
		//Execute query 
        $row = $select_query->result();
		// print $row;exit;
		return ($row);
	}

    public function app_version_get(){
      $version_status = $this->app_version_check();
      if ($version_status == false)
      {
        $response = array("http_code" => 200, 'local_id' => $json['Data']['local_id'], 'delete_local' => '1', 'status' => 'failed', 'sync_error' => true, 'message' => $this->app_version_error_message);
          $this->response($response);
      }
    }

    public function app_version_check($activity_date = ''){
      $check_version = 1;
            
      $json = $this->request->body;
      $user_app_version = '';
      
      if (isset($json['Data']) and isset($json['Data']['app_version']))
      {
        $user_app_version = $json['Data']['app_version'];
      }
      else
      {
        $this->app_version_error_message = 'Kindly update app version to 5.5';
        return false;
      }
      $app_data = "select * from ssc_app_version where id=(select max(id) from ssc_app_version)";
      $app_data = "select * from ssc_app_version where app_version_code > ".$user_app_version." and urgent_update_required = 1";
      $select_query = $this->db->query($app_data);
      //Execute query 
      $row = $select_query->result();
      
      if(count($row) > 0 and $user_app_version != $row[0]->app_version_code){
        $release_date = $row[0]->release_date;
        $this->api_version_no = $row[0]->app_version_no;
        
        if ($activity_date != "")
        {
          $activity_check_date = strtotime(date("Y-m-d",strtotime($activity_date)));
          $release_check_date = strtotime($release_Date);
          if ($activity_check_date < $release_check_date and $row[0]->urgent_update_required == 1)
          {
            $check_version = 0;
          }
        }
        if ($check_version == 1)
        {
          $this->app_version_error_message = 'Kindly update app version to '.$row[0]->app_version_no;
          return false;
          //$result_app = array("id"=>$row[0]->id,"app_version_no"=>$row[0]->app_version_code,"description"=>'You are using an old version. Kindly update your version to '.$row[0]->app_version_no);
          //$this->response(array("http_code" => 401, 'message' => 'versionupdate' ,'app_version' => $result_app));
        }
      }
      $app_data1 = "select * from ssc_app_version where app_version_code = '".$user_app_version."'";
      $select_query1 = $this->db->query($app_data1);
      //Execute query 
      $row1 = $select_query1->row_array();
      $this->api_version_no = $row1['app_version_no'];
      return true;
    }
    
    public function distict_state_name_list_get(){
			// $json = $this->request->body;
			$version_status = $this->app_version_check();
      if ($version_status == false)
      {
        $response = array("http_code" => 200, 'local_id' => $json['Data']['local_id'], 'delete_local' => '1', 'status' => 'failed', 'sync_error' => true, 'message' => $this->app_version_error_message);
      }
			$states_array = array();
			$district_block_array = array();
		    $query_state = "select id,name from ssc_states";
			$select_state = $this->db->query($query_state);
			$state_results = $select_state->result();
			$s=0;
      $total_states = 0;
      $total_districts = 0;
      $total_blocks = 0;
			foreach($state_results as $state_result) {
        $total_states = $total_states + 1;
                $state_id = $state_result->id;
				$state_name = $state_result->name;
			    $query_district = "select id, trim(name) name from ssc_districts where state_id = '".$state_id."'";
				$select_district = $this->db->query($query_district);
			    $district_results = $select_district->result();
				$count = 0;
				$district_array = array();
				$district_block_array = array();
				foreach($district_results as $district_result) {
           $total_districts = $total_districts + 1;
				   $district_id  = $district_result->id;
				   $district_name  = $district_result->name;
				   $query_block = "select id, trim(name) name from ssc_blocks where district_id ='".$district_id."'";
				   $district_array = array("id"=>$district_id,"name"=>$district_name);
				   $select_block = $this->db->query($query_block);
			       $block_results = $select_block->result();
				   $b=0;
				   $block_array = array();
				   foreach($block_results as $block_result) {
             $total_blocks = $total_blocks + 1;
				     $block_id  = $block_result->id;
				     $block_name  = $block_result->name;
					 $block_array[$b] = array("id"=>$block_id,"name"=>$block_name);
					 $b++;
				   }
				   $district_block_array[$count] = array("id"=>$district_id,"name"=>$district_name,"blocks"=>$block_array);
				  $count++;
				}
				$states_array[$s] = array("id"=>$state_id,"name"=>$state_name,"districts"=>$district_block_array);
				$s++;
	        }
					
          $totals = array("states"=>$total_states, "districts"=>$total_districts, "blocks"=>$total_blocks);
					$this->response(array("http_code" => 200, 
										  'status' => 'success', 
										  'message' => 'Record Found',
										  'states' =>  $states_array,
                      'total_states' => $total_states, 'total_districts' => $total_districts, 'total_blocks' => $total_blocks));						
			
		}
    
    //Test Line OBSERVATIONS LIST 
    public function master_testline_post() {    
      $this->load->model('Test_questions_model');
      $this->load->model('Sparks_model');
      
      $version_status = $this->app_version_check();
      if ($version_status == false)
      {
        $response = array("http_code" => 200, 'delete_local' => '1', 'status' => 'failed', 'sync_error' => true, 'message' => $this->app_version_error_message);
          $this->response($response);
      }
      
      error_log("inside controller account - master list post ", 0);
      $this->output->enable_profiler(FALSE);
      error_log("inside controller account - master list - BEFORE NEW STATE ", 0);
      
      $all_observations = array();
                
      $total_questions = 0;
      $get_observations = $this->Test_questions_model->get_all();
      foreach($get_observations as $get_observation)
      {
        $observation = array(); 
        $observation_allowed = 1;
        
        if ($observation_allowed == 1)
        {
          $observation['id'] = $get_observation->id;
          $observation['state_id'] = $get_observation->state_id;
          $observation['class_id'] = $get_observation->class_id;
          $observation['subject_id'] = $get_observation->subject_id;
          $observation['correct_option'] = $get_observation->correct_option;
          $observation['question'] = "Q) ".$get_observation->question; //." - ".$get_observation->state_id;
          $observation['question_type'] = $get_observation->question_type;
          $observation['subject_wise'] = $get_observation->subject_wise;
          $observation['test_type'] = $get_observation->activity_type;
          $total_questions = $total_questions + 1;
          if ($get_observation->question_type == "objective")
          {
            $options = array();
            $question_options = $this->Test_questions_model->getObtionsByObservation($get_observation->id);
            foreach($question_options as $question_option)
            {
              $option = array();
              $option['option_id'] = $question_option->id;
              $option['option_value'] = $question_option->option_value;
              array_push($options,$option);
            }
            $observation['options'] = $options;
            if (count($options) > 0)
              array_push($all_observations,$observation);
          }
          else if($get_observation->question_type == "yes-no-na"){
            $observation['question_type'] = "objective";
            $options = array();
            $question_options = array('1'=>'Yes', '2'=>'No', '0'=>'N/A');
            foreach($question_options as $key=>$value)
            {
              $option = array();
              $option['option_id'] = $key;
              $option['option_value'] = $value;
              array_push($options,$option);
            }
            $observation['options'] = $options;
            array_push($all_observations,$observation);
          }
          else
            array_push($all_observations,$observation);
        }
      }
                        
      $sparkData['questions_flag'] = 0;
      $this->Sparks_model->update_info($sparkData, $this->user_id);
      
      $this->response(array("http_code" => 402, "total_test_questions" => $total_questions, "test_questions" => $all_observations));
    }
    //Test Line OBSERVATIONS LIST FUNCTION CLOSED HERE 
	
    
    public function testline_post() {
      $json = $this->request->body;
      $this->baseline_test($json, $json['Data']['type_info']['test_type']);
    }
    
    public function baseline_test($json, $test_type) {      
      $type_info = $json['Data']['type_info'];
      
      if ($test_type == "base_line") {
        $test_table = 'baseline_tests';
        $test_answers_table = 'baseline_test_answers';
      } else {
        $test_table = 'endline_tests';
        $test_answers_table = 'endline_test_answers';
      }
      
      $spark_id = $this->user_id;
      $local_id = $type_info['local_id'];
      $class_id = $type_info['class_id'];
      $date_of_test = $type_info['date_of_test'];
      $school_id = $type_info['school_id'];
      $subject_id = $type_info['subject_id'];
      $student_name = $type_info['student_name'];
      $student_gender = $type_info['gender'];
      $student_roll_no = $type_info['student_roll_no'];
      $assesmentData = json_decode($type_info['assesmentQuestion']);
      
      $this->load->Model('Test_questions_model');
      
      $data['spark_id'] = $spark_id;
      $data['class_id'] = $class_id;
      $data['school_id'] = $school_id;
      $data['subject_id'] = $subject_id;
      $data['student_roll_no'] = $student_roll_no;
      $check_test = $this->Test_questions_model->check_test($test_table, $data);

      if(empty($check_test)){
        $data['created_source'] = 'app';
        $data['created_at'] = date('Y-m-d H:i:s');
        $data['created_by'] = $this->user_id;
        $data['student_name'] = $student_name;
        $data['date_of_test'] = $date_of_test;
        $data['student_gender'] = $student_gender;
        $test_id = $this->Test_questions_model->add_test($test_table, $data);
      }
      else{
        $test_id = $check_test['id'];
        $data['student_name'] = $student_name;
        $data['date_of_test'] = $date_of_test;
        $data['student_gender'] = $student_gender;
        $testline_id = $this->Test_questions_model->update_test($test_table, $data, $test_id);
      }
      
      foreach($assesmentData as $assesments)
      {
        $question_id = $assesments->id;
        $answer_id = $assesments->value;
        
        $question_data = $this->Test_questions_model->getById($question_id);
        $correct_ans_id = $question_data['correct_option'];
        
        if ($test_type == "base_line") {
          $adata['baseline_test_id'] = $test_id;
        }
        else {
          $adata['endline_test_id'] = $test_id;
        }
        $adata['question_id'] = $question_id;
        $check_test_answer = $this->Test_questions_model->check_test($test_answers_table, $adata);
        
        if(empty($check_test_answer)){
          $adata['is_correct'] = ($correct_ans_id == $answer_id ? 1 : 0);
          $adata['answer_id'] = $answer_id;
          $test_ans_id = $this->Test_questions_model->add_test($test_answers_table, $adata);
        }
        else{
          $udata['is_correct'] = ($correct_ans_id == $answer_id ? 1 : 0);
          $udata['answer_id'] = $answer_id;
          $test_ans_id = $this->Test_questions_model->update_test($test_answers_table, $udata, $check_test_answer['id']);
        }
      }
      
      $response = array("http_code" => 200, 'server_id' => $test_id, 'sync_error' => false, "message" => "Stored Successfully", 'local_id' => $json['Data']['local_id']);
      $this->response($response);
    }
    
    public function leave_credit_details_post()
    {
			$this->load->model('Leaves_model');
      
      $version_status = $this->app_version_check();
      
      if ($version_status == false)
      {
        $response = array("http_code" => 200, 'status' => 'failed', 'sync_error' => true, 'message' => $this->app_version_error_message);
          $this->response($response);
      }
      error_log("inside controller account - leave credit list post ", 0);
      $this->output->enable_profiler(FALSE);
      error_log("inside controller account - leave credit list - BEFORE NEW STATE ", 0);
			$earnedData = $this->Leaves_model->get_leave_credits($this->user_id);
		        $leave_types = unserialize(LEAVE_TYPES);
			$leave_counts = array();
			foreach($earnedData as $earned)
			{
				$leave_count = array();
				$leave_count['leave_type'] = $earned->leave_type;  
				$leave_count['leave_earned'] = $earned->leave_earned;  
				$leave_count['leave_taken'] = $earned->leave_taken;  
				unset($leave_types[$earned->leave_type]);
				array_push($leave_counts,$leave_count);
			} 	

			foreach($leave_types as $leave_type)
			{	
				$leave_count = array();
				$leave_count['leave_type'] = $leave_type;  
				$leave_count['leave_earned'] = 0;
				$leave_count['leave_taken'] = 0;
				array_push($leave_counts,$leave_count);
			}
			$this->response(array("http_code" => 402, "leave_credits" => $leave_counts));
		}
	
	 public function attendance_regularization_post()
   {
		 $json = $this->request->body;
		 
		 $version_status = $this->app_version_check();
      if ($version_status == false)
      {
        $response = array("http_code" => 200, 'local_id' => $json['Data']['local_id'], 'delete_local' => '1', 'status' => 'failed', 'sync_error' => true, 'message' => $this->app_version_error_message);
        $this->response($response);
      }
     
     $this->load->model('Users_model');
     $this->load->model('Attendance_model');
        
     $attendancedate = $json['Data']['regularizationInfo']['attendance_date'];
     $attendance_type = $json['Data']['regularizationInfo']['attendance_type'];
		  
			$spark_id = $this->user_id;
			$attendancedate = date('Y-m-d', strtotime($attendancedate));
			
			$check_attendance = $this->Attendance_model->check_attendance($spark_id, $attendancedate);
			
			$already_exist = 0;
			if(!empty($check_attendance))
			{
				$attendance_types = array();	
				foreach($check_attendance as $checkattendance)
				{
						$attendance_types[] = $checkattendance->attendance_type;
				}
				
				if($attendance_type == 'Full Day')
				{
					$already_exist = 1;	
					$error_msg = 'Attendance already marked.';
				}
				else if(($attendance_type == 'First Half' || $attendance_type == 'Second Half') && in_array('Full Day',$attendance_types))
				{
					$already_exist = 1;	
					$error_msg = 'Attendance already marked for full day.';
				}
				else if($attendance_type == 'First Half' && in_array('First Half',$attendance_types))
				{
					$already_exist = 1;	
					$error_msg = 'Attendance already marked for first half.';
				}
				else if($attendance_type == 'Second Half' && in_array('Second Half',$attendance_types))
				{
					$already_exist = 1;	
					$error_msg = 'Attendance already marked for second half.';
				}
			}
			
			if($already_exist == 0)
			{
				if($attendance_type == 'Full Day'){
					$in_time = DAY_START;
					$out_time = DAY_END;
				}
				else if($attendance_type == 'First Half'){
					$in_time = DAY_START;
					$out_time = DAY_MID;
				}
				else if($attendance_type == 'Second Half'){
					$in_time = DAY_MID;
					$out_time = DAY_END;
				}
				$data['spark_id'] = $spark_id;
				$data['attendance_date'] = $attendancedate;
				$data['attendance_type'] = $attendance_type;
				$data['in_time'] = $attendancedate." ".$in_time;
				$data['out_time'] = $attendancedate." ".$out_time;
				$data['created_at'] = date('Y-m-d H:i:s');
				$data['source'] = 'app';
				$data['status'] = 'pending';
				$data['created_by'] = $spark_id;
				$data['reason'] = (isset($json['Data']['regularizationInfo']['reason']) ? $json['Data']['regularizationInfo']['reason'] : '');
				
				$last_insert_id = $this->Attendance_model->insert_regularizations($data);
				$response = array("http_code" => 200, 'server_id' => $last_insert_id, 'sync_error' => false, "message" => "Attendance Marked Successfully", 'local_id' => $json['Data']['local_id']);
			}
			else{
				//$response = array("http_code" => 200, 'local_id' => $json['Data']['local_id'], 'delete_local' => '1', 'status' => 'failed', 'sync_error' => true, 'message' => 'Attendance already marked');
				$response = array("http_code" => 200, 'local_id' => $json['Data']['local_id'], 'delete_local' => '1', 'status' => 'failed', 'sync_error' => true, 'message' => $error_msg);
			}
			$this->response($response);
   }
    
  
    //SURVEY OBSERVATIONS LIST 
    public function survey_observations_post() {    
      $this->load->model('survey_model');
      $this->load->model('states_model');
      $this->load->model('users_model');
      $this->load->model('Sparks_model');
      $this->load->model('survey_model');
      
      $version_status = $this->app_version_check();
      if ($version_status == false)
      {
        $response = array("http_code" => 200, 'delete_local' => '1', 'status' => 'failed', 'sync_error' => true, 'message' => $this->app_version_error_message);
        $this->response($response);
      }
      
      error_log("inside controller account - master list post ", 0);
      $this->output->enable_profiler(FALSE);
      error_log("inside controller account - master list - BEFORE NEW STATE ", 0);
      $states = $this->states_model->get_by_id($this->user_state_id);

      $state_array = array();
      $master_list = array();
      $final_data = array();
			
			//print_r($states);
			$state_name = $states[0]->name;
			
      $surveys = $this->survey_model->get_survey_by_state($state_name);
      $surveys_array['survey'] = Array();
      $surveys_array['suvey_observations'] = Array();
      //error_log("inside controller account - master list - before for each subject ", 0);
      foreach ($surveys as $c) {
          $survey_array = Array();
          $survey_array['survey_id'] = $c->survey_id;
          $survey_array['title'] = $c->title;
          $survey_array['display_level'] = $c->display_level;
          $survey_array['take_location'] = $c->take_location;
          $suvey_observations = $this->get_survey_observations($c->survey_id);
          $survey_array['observations'] = $suvey_observations['observations'][$c->survey_id];
          if (count($survey_array['observations']) > 0)
            array_push($surveys_array['survey'], $survey_array);
      }
      array_push($master_list, $surveys_array);
      $this->response(array("http_code" => 402, "surveys" => $surveys_array['survey'], 'total_questions' => $total_questions));
    }
    //MASTER OBSERVATIONS LIST FUNCTION CLOSED HERE 				
    
    function get_survey_observations($survey_id)
    {
			$this->load->Model('survey_model');
			$total_questions = 0;
			$observations_array = array();
			$all_observations[$survey_id] = array();
      $get_observations = $this->survey_model->get_all_observations(0,$survey_id,'sort_order asc','1');
      
      foreach($get_observations as $get_observation)
      {
        $observation = array(); 
        $observation_allowed = 1;
        
          $observation['id'] = $get_observation->id;
          $observation['question'] = $get_observation->question; //." - ".$get_observation->state_id;
          $observation['question_type'] = $get_observation->question_type;
          $observation['subject_wise'] = $get_observation->subject_wise;
          $total_questions = $total_questions + 1;
          
          if($get_observation->question_type == "objective" || $get_observation->question_type == "multichoice")
          {
            $options = array();
            $question_options = $this->survey_model->getObtionsByObservation($get_observation->id);
            foreach($question_options as $question_option)
            {
              $option = array();
              $option['option_id'] = $question_option->id;
              $option['option_value'] = $question_option->option_value;
              array_push($options,$option);
            }
            $observation['options'] = $options;
            if (count($options) > 0)
              array_push($all_observations["$survey_id"],$observation);
          }
          else if($get_observation->question_type == "yes-no-na"){
            $observation['question_type'] = "objective";
            $options = array();
            $question_options = array('1'=>'Yes', '2'=>'No', '0'=>'N/A');
            foreach($question_options as $key=>$value)
            {
              $option = array();
              $option['option_id'] = $key;
              $option['option_value'] = $value;
              array_push($options,$option);
            }
            $observation['options'] = $options;
            array_push($all_observations["$survey_id"],$observation);
          }
          else
            array_push($all_observations["$survey_id"],$observation);
      }
      $observations_array['observations'] = $all_observations;
      
      return $observations_array;
		}
		
		public function survey_test_post() {
      $json = $this->request->body;
			
			$test_table = 'survey_tests';
      $test_answers_table = 'survey_test_answers';	
      
      $type_info = $json['Data']['type_info'];
      
      $spark_id = $this->user_id;
      $survey_id = $type_info['survey_id'];
      $local_id = $json['Data']['local_id'];
      $display_level = $type_info['display_level'];
      $state_id = $type_info['state_id'];
      $district_id = $type_info['district_id'];
      $block_id = $type_info['block_id'];
      $cluster_id = $type_info['cluster_id'];
      $school_id = $type_info['school_id'];
      $date_of_test = $type_info['date_of_test'];
      $lat = (isset($type_info['latitude']) ? $type_info['latitude'] : '');
      $long = (isset($type_info['longitude']) ? $type_info['longitude'] : '');
      $location = (isset($type_info['address']) ? $type_info['address'] : '');
      $assesmentData = json_decode($type_info['assesmentQuestion']);
      
      $this->load->Model('Survey_model');
      
      $data['survey_id'] = $survey_id;
      $data['spark_id'] = $spark_id;
      $data['date_of_test'] = $this->create_datetime($date_of_test);
      
      $check_test = $this->Survey_model->check_test($test_table, $data);
			
      if(!empty($check_test)){
				$survey_test_id = $check_test['id'];
				$mdata['display_level'] = $display_level;
				$mdata['state_id'] = $state_id;
				$mdata['district_id'] = $district_id;
				$mdata['block_id'] = $block_id;
				$mdata['cluster_id'] = $cluster_id;
				$mdata['school_id'] = $school_id;
				$mdata['latitude'] = $lat;
				$mdata['longitude'] = $long;
				$mdata['location'] = $location;
				//$mdata['created_at'] = date('Y-m-d H:i:s');
				//$mdata['created_by'] = $this->user_id;
				//$mdata['date_of_test'] = $date_of_test;
				$this->Survey_model->update_test($test_table, $mdata, $survey_test_id);	
			}
			else{
				$data['display_level'] = $display_level;
				$data['state_id'] = $state_id;
				$data['district_id'] = $district_id;
				$data['block_id'] = $block_id;
				$data['cluster_id'] = $cluster_id;
				$data['school_id'] = $school_id;
				$data['latitude'] = $lat;
				$data['longitude'] = $long;
				$data['location'] = $location;
				$data['created_at'] = date('Y-m-d H:i:s');
				$data['created_by'] = $this->user_id;
				//$data['date_of_test'] = $date_of_test;
				$survey_test_id = $this->Survey_model->add_test($test_table, $data);	
			}
      
      
      foreach($assesmentData as $assesments)
      {
        $question_id = $assesments->id;
        $answer_id = $assesments->value;
        
        $cdata['question_id'] = $question_id;
        $cdata['survey_test_id'] = $survey_test_id;
				
				$check_test_ans = $this->Survey_model->check_test($test_answers_table, $cdata);
				
				if(!empty($check_test_ans)){
					$test_ans_id = $check_test_ans['id'];
					$udata['question_id'] = $question_id;
					$udata['answers'] = $answer_id;
					$test_ans_id = $this->Survey_model->update_test($test_answers_table, $udata, $test_ans_id);
				}
				else{
          $adata['survey_test_id'] = $survey_test_id;
					$adata['question_id'] = $question_id;
					$adata['answers'] = $answer_id;
					$test_ans_id = $this->Survey_model->add_test($test_answers_table, $adata);
				}
      }
      
      $response = array("http_code" => 200, 'server_id' => $survey_test_id, 'sync_error' => false, "message" => "Stored Successfully", 'local_id' => $json['Data']['local_id']);
      $this->response($response);
    }


public function allclusters_get()
  {
    $this->load->model('states_model');
    $this->load->model('districts_model');
    $this->load->model('blocks_model');
    $this->load->model('clusters_model');
    $states = $this->states_model->get_all();
    $states_data = Array();
    foreach($states as $state) {
      $state_id = $state->id;
      $state_name = $state->name;
      $state_short_name = $state->name;
      $state_code = $state->dise_code;
      
      $districts = $this->districts_model->get_state_districts($state_id);
      foreach($districts as $district) {
        $district_id = $district->id;
        $district_name = $district->name;
        $district_code = $district->dise_code;
        
        $blocks = $this->blocks_model->get_district_blocks($district_id,"");
        foreach($blocks as $block) {
          $block_id = $block->id;
          $block_name = $block->name;
          $block_code = $block->dise_code;
          
          $clusters = $this->clusters_model->get_block_clusters($block_id,"");
          foreach($clusters as $cluster) {
            $cluster_id = $cluster->id;
            $cluster_name = $cluster->name;
            $cluster_code = $cluster->dise_code;
            
            $data = Array();
            $data['state_id'] = $state_id;
            $data['state_name'] = $state_name;
            $data['state_short_name'] = $state_short_name;
            $data['state_code'] = $state_code;
            $data['district_id'] = $district_id;
            $data['district_name'] = $district_name;
            $data['district_code'] = $district_code;
            $data['block_id'] = $block_id;
            $data['block_name'] = $block_name;
            $data['block_code'] = $block_code;  
            $data['cluster_id'] = $cluster_id;
            $data['cluster_name'] = $cluster_name;
            $data['cluster_code'] = $cluster_code;
            array_push($states_data,$data);
          }
        }
      }
    }
    $this->response(array("http_code" => 200, 
										  'status' => 'success', 
										  'message' => 'Record Found',
										  'states' =>  $states_data));
  }
  
    public function correct_surveys_get() {
      $this->load->helper('directory');
      $file_path = FCPATH.'/uploads/survey/';
      $files = directory_map($file_path);
      $file = $_GET['file'];
      if (in_array($file,$files)) {
        echo "FIle Exists";
      } else {
        $file = "json-04-02-2020.txt";
        $file = "json-30-01-2020.txt";
        $file = "json-07-02-2020.txt";
        $file = "json-31-01-2020.txt";
        $file = "json-12-02-2020.txt";
        $file = "json-01-02-2020.txt";
        $file = "json-10-02-2020.txt";
        $file = "json-03-02-2020.txt";
        $file = "json-11-02-2020.txt";
        $file = "json-13-02-2020.txt";
        $file = "json-08-02-2020.txt";
        $file = "json-05-02-2020.txt";
        $file = "json-06-02-2020.txt";
      }
      
      //foreach($files as $file) {
        echo "==================<b>FILENAME : ".$file."</b>======================";
        $fn = fopen($file_path.$file,"r");
        $take_next_line = 0;
        while(! feof($fn))  {
          $result = fgets($fn);
          $check_string = "-------------------survey_test.json";
          
          if ($take_next_line == 1) {
            $json = json_decode($result,true);
if (isset($json['Data'])) {
            $output = $this->survey_test_temp($json);
//            if ($output == "not")
              echo $result;
}
            $take_next_line = 0;
          }
          if (strpos($result,$check_string) !== false) {
              echo $result;
  
          $take_next_line = 1;
          }
        }

        fclose($fn);
      //}
    }

		public function survey_test_temp($json) {		
			$test_table = 'survey_tests';
      $test_answers_table = 'survey_test_answers';	
      
      $this->load->Model('Survey_model');
      $this->load->Model('users_model');
      
      $type_info = $json['Data']['type_info'];

      $user = $json['Data']['user'];
      $auth_token = $json['Data']['auth_token'];
      $result = $this->users_model->validate_auth_token_temp($user, $auth_token);
      //error_log(" construct - after validate auth token ", 0);
      if (isset($result) and isset($result['id'])) {
          $spark_id = $result['id'];
      }
      
      $survey_id = $type_info['survey_id'];
      $local_id = $json['Data']['local_id'];
      $display_level = $type_info['display_level'];
      $state_id = $type_info['state_id'];
      $district_id = $type_info['district_id'];
      $block_id = $type_info['block_id'];
      $cluster_id = $type_info['cluster_id'];
      $school_id = $type_info['school_id'];
      $date_of_test = $type_info['date_of_test'];
      $lat = (isset($type_info['latitude']) ? $type_info['latitude'] : '');
      $long = (isset($type_info['longitude']) ? $type_info['longitude'] : '');
      $location = (isset($type_info['address']) ? $type_info['address'] : '');
      $assesmentData = json_decode($type_info['assesmentQuestion']);
      
      
      $data['survey_id'] = $survey_id;
      $data['spark_id'] = $spark_id;
      $data['date_of_test'] = $this->create_datetime($date_of_test);
      $check_test = $this->Survey_model->check_test($test_table, $data);
$output = "not";
      if(!empty($check_test)){
				$survey_test_id = $check_test['id'];
				$mdata['display_level'] = $display_level;
				$mdata['state_id'] = $state_id;
				$mdata['district_id'] = $district_id;
				$mdata['block_id'] = $block_id;
				$mdata['cluster_id'] = $cluster_id;
				$mdata['school_id'] = $school_id;
				$mdata['latitude'] = $lat;
				$mdata['longitude'] = $long;
				$mdata['location'] = $location;
				//$mdata['created_at'] = date('Y-m-d H:i:s');
				//$mdata['created_by'] = $this->user_id;
				//$mdata['date_of_test'] = $date_of_test;
				$this->Survey_model->update_test($test_table, $mdata, $survey_test_id);	
$output = "updated";
			}
			else{
$output = "created";
				$data['display_level'] = $display_level;
				$data['state_id'] = $state_id;
				$data['district_id'] = $district_id;
				$data['block_id'] = $block_id;
				$data['cluster_id'] = $cluster_id;
				$data['school_id'] = $school_id;
				$data['latitude'] = $lat;
				$data['longitude'] = $long;
				$data['location'] = $location;
				$data['created_at'] = date('Y-m-d H:i:s');
				$data['created_by'] = $this->user_id;
				//$data['date_of_test'] = $date_of_test;
				$survey_test_id = $this->Survey_model->add_test($test_table, $data);	
			}
      
      
      foreach($assesmentData as $assesments)
      {
        $question_id = $assesments->id;
        $answer_id = $assesments->value;
        
        $cdata['question_id'] = $question_id;
        $cdata['survey_test_id'] = $survey_test_id;
				
				$check_test_ans = $this->Survey_model->check_test($test_answers_table, $cdata);
				
				if(!empty($check_test_ans)){
					$test_ans_id = $check_test_ans['id'];
					$udata['question_id'] = $question_id;
					$udata['answers'] = $answer_id;
					$test_ans_id = $this->Survey_model->update_test($test_answers_table, $udata, $test_ans_id);
				}
				else{
          $adata['survey_test_id'] = $survey_test_id;
					$adata['question_id'] = $question_id;
					$adata['answers'] = $answer_id;
					$test_ans_id = $this->Survey_model->add_test($test_answers_table, $adata);
				}
      }
      return $output;
      //$response = array("http_code" => 200, 'server_id' => $survey_test_id, 'sync_error' => false, "message" => "Stored Successfully", 'local_id' => $json['Data']['local_id']);
      //$this->response($response);
    }
	
	
		public function stay_activity_post() {
      $json = $this->request->body;
        
      $tracking_time = $this->create_datetime($json['Data']['type_info']['date']);

      $version_status = $this->app_version_check();
      if ($version_status == false)
      {
        $response = array("http_code" => 200, 'local_id' => $json['Data']['local_id'],  'delete_local' => '1', 'status' => 'failed', 'sync_error' => true, 'message' => $this->app_version_error_message);
          $this->response($response);
      }
      $check_activity = $this->check_activity($this->user_id, 'hotel_stay', $tracking_time);
      
      if($check_activity == 0){  
        
        //$response = array("http_code" => 200, 'server_id' => rand(1,10), 'sync_error' => false, "message" => "Stored Successfully", 'local_id' => $json['Data']['local_id']);
        
        $activity_info = $this->activity_handler->stay_activity($json);
        if ($activity_info["message"] == "Stored Successfully")
        {
						$json['Data']['type_info']['date'] = $tracking_time; 
            $json['Data']['type_info']['activity_type'] = 'hotel_stay';
            $json['Data']['type_info']['activity_id'] = $activity_info['server_id'];
            $json['Data']['type_info']['accuracy'] = $json['Data']['type_info']['accuracy'];
            $json['Data']['type_info']['location'] = $json['Data']['type_info']['hotel_address'];
            $json['Data']['type_info']['lat'] = $json['Data']['type_info']['lat'];
            $json['Data']['type_info']['lon'] = $json['Data']['type_info']['lon'];
            $json['Data']['type_info']['captured_location'] = (isset($json['Data']['type_info']['captured_address']) ? $json['Data']['type_info']['captured_address'] : '');
            $this->put_tracking_post($json); // insert tracking info
            
            /*$tracking_time = (isset($json['Data']['type_info']['date_out']) ? $json['Data']['type_info']['date_out'] : '');
            $json['Data']['type_info']['date'] = $tracking_time; 
            $json['Data']['type_info']['accuracy'] = $json['Data']['type_info']['to_accuracy'];
            $json['Data']['type_info']['location'] = $json['Data']['type_info']['to_address'];
            $json['Data']['type_info']['lat'] = $json['Data']['type_info']['to_lat'];
            $json['Data']['type_info']['lon'] = $json['Data']['type_info']['to_lon'];
            $json['Data']['type_info']['activity_type'] = 'hotel_stay_out';
            $json['Data']['type_info']['activity_id'] = $activity_info['server_id'];
            $json['Data']['type_info']['captured_location'] = (isset($json['Data']['type_info']['captured_address_out']) ? $json['Data']['type_info']['captured_address_out'] : '');
            $this->put_tracking_post($json); // insert tracking info*/
        }
        $this->response($activity_info);
      }else{
        $get_activity_id = $this->get_activity($this->user_id, 'hotel_stay', $tracking_time);
        
        $response = array("http_code" => 200, 'server_id' => $get_activity_id, 'sync_error' => false, "message" => "Stored Successfully", 'local_id' => $json['Data']['local_id']);
        
        //$response = array("http_code" => 200, 'sync_error' => false, 'local_id' => $json['Data']['local_id'], 'delete_local' => '1', 'status' => 'failed', 'sync_error' => true, 'message' => 'LDT on same time not allowed');
        $this->response($response);
      }
    }


}
