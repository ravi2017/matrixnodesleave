<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Activity extends CI_Controller {

	function __construct()	
	{
		parent::__construct();
		$this->load->model('activity_approval_model','spark_users_district_block');
		$this->load->model('Users_model');
    $this->load->model('activity_approval_model');
		$this->load->library('common_functions');
		$this->load->model('leaves_model');
    
    $this->data['month_arr'] = unserialize(MONTH_ARRAY);
    $this->data['year_arr'] = unserialize(YEAR_ARRAY);
    
		if($this->session->userdata('logged_in'))
		{
      $session_data = $this->session->userdata('logged_in');
		  $this->data['current_user_id'] = $session_data['id'];
		  $this->data['current_username'] = $session_data['username'];
		  $this->data['current_name'] = $session_data['name'];
		  $this->data['current_role'] = $session_data['role'];
		  $this->data['user_state_id'] = $session_data['state_id'];
		  $this->data['user_district_id'] = $session_data['district_id'];
		  $this->data['login_id'] = $session_data['login_id'];
		  $this->user_id = $session_data['id'];
		  $this->role = $session_data['role'];
		  $this->user_state_id = $session_data['state_id'];
		  $this->user_district_id = $session_data['district_id'];
		  
		  $this->data['spark_id'] = $session_data['id'];
		  $this->data['spark_state_id'] = $session_data['state_id'];
		  $this->data['current_group'] = $session_data['user_group'];
				
			$MEET_WITH = unserialize(MEET_WITH);	
		  if($this->data['current_role'] == 'state_person')
		  {
				$MEET_WITH = unserialize(MEET_WITH_SH);
			}
			else if($this->data['current_role'] == 'manager')
		  {
				$MEET_WITH = unserialize(MEET_WITH_MGR);
			}
			else if($this->data['current_role'] == 'field_user')
		  {
				$MEET_WITH = unserialize(MEET_WITH_FUSER);
			}
			$this->data['MEET_WITH'] = $MEET_WITH;
      
		  $methods_with_login = array('index','school_visit','review_meeting','training', 'call', 'no_visit','get_blocks','get_call_contact_list', 'my_activity','get_state', 'get_districts','my_leave_request','fetch_observation','get_assessment_data','attendance_list','address_mismatch','training_list', 'attendance_calendar', 'my_leave_list', 'block_holidays', 'cancel_leave', 'add_attendance_regularization', 'change_password', 'notification_list', 'attendance_regularizations');
		  
		  $method = $this->router->fetch_method();

		  if ($this->data['current_role'] == "field_user" and !in_array($method, $methods_with_login))
		  {
			//If no session, redirect to login page
			$this->session->set_flashdata('alert', "You are not authorized to access this section");
			redirect('dashboard', 'refresh');
		  }
		  $this->load->library('activity_handler');
		  
		  $this->data['ATTENDANCE_REG_TYPE'] = unserialize(ATTENDANCE_REG_TYPE);
		  $this->data['leaves_types'] = unserialize(LEAVE_TYPES);
		}
		else
		{
		  //If no session, redirect to login page
		  redirect('login', 'refresh');
		}
    
    $INDEX_COLOR = unserialize(INDEX_COLOR);
    $this->data['INDEX_COLOR'] = unserialize(INDEX_COLOR);
	}

  public function index(){

    $data['Data']['limit'] = 5;  
    $activity_info = $this->activity_handler->get_activity_list($data);
    //print_r($activity_info);
    $sparks = $this->activity_approval_model->get_sparks($this->data['current_user_id']);
    $is_validator = $this->get_user_type();
    $this->data['activitystartdate'] = '';
    $this->data['activityenddate'] = '';
    $this->data['sparks'] = $sparks;
    $this->data['spark_id'] = $this->data['current_user_id'];
    $this->data['spark_state_id'] = $this->data['user_state_id'];
    $this->data['is_validator'] = $is_validator;
    $this->data['user_login_id'] = $this->data['current_user_id'];
    $this->data['activity_infos'] = $activity_info['activity_info'];
    $this->data['partial'] = 'activity/index';
    $this->data['page_title'] = 'Activity approval';
    $this->load->view('templates/index', $this->data);
  }

  public function school_visit(){

    $user_districts = $this->Users_model->spark_user_districts($this->data['current_user_id']);

    if(count($user_districts) == 0 && $this->data['current_role'] == 'state_person'){
      $this->load->model('districts_model');
      $user_districts = $this->districts_model->get_all_district($this->data['user_state_id']);
    }
    
    $this->data['mode'] = 'add';
    $this->data['edit_id'] =  '';
    $this->data['activity_date'] = '';
    if(!empty($_GET['id'])){
      $id = $_GET['id'];
      $this->load->model("activity_approval_model");
      $rejected_data = $this->activity_approval_model->rejected_activity_details($id);
      if(count($rejected_data) == 0){
        redirect('activity', 'refresh');
      }
      else{
        $rejected_data = $rejected_data;
        $this->data['edit_id'] =  $id;
        $this->data['mode'] =  'edit';
        $this->data['activity_date'] =  date('Y-m-d H:i', strtotime($rejected_data[0]->activity_date));
      }
    }
    $redirect_url = 'activity/school_visit';

    $is_validator = $this->get_user_type();
    $this->data['is_validator'] = $is_validator;
    
    if(isset($_POST['Submit'])){
      
      if($_POST['district_id'] == 'other')
      {
        $state_id = $_POST['state_id'];
        $district_id = $_POST['other_district_id'];
      }
      else{
        $district_id = $_POST['district_id'];
        $this->load->model('districts_model');
        $district_data = $this->districts_model->getById($district_id);
        $state_id = $district_data['state_id'];
      }
      
      //echo"<pre>";
      //print_r($_POST);exit;
      $edit_id = $_POST['edit_id'];
      $data['Data']['source'] = 'web';
      $data['Data']['local_id'] = $this->user_id;
      $data['Data']['is_validator'] = $is_validator;
      $data['Data']['mode'] = $_POST['mode'];
      $data['Data']['login_id'] = $this->data['login_id'];
      $data['Data']['type_info']['date'] = date('Y-m-d H:i:s', strtotime($_POST['visit_date'].$_POST['visit_time']));
      $data['Data']['type_info']['duration'] = $_POST['txtHours'].":".$_POST['txtMinutes'];
      $data['Data']['type_info']['state_id'] = $state_id;
      $data['Data']['type_info']['district_id'] = $district_id;
      $data['Data']['type_info']['block_id'] = $_POST['block_id'];
      $data['Data']['type_info']['cluster_id'] = $_POST['cluster_id'];
      $data['Data']['type_info']['school_id'] = $_POST['school_id'];
      $data['Data']['type_info']['assessment'] = $_POST['ques'];
      $data['Data']['type_info']['travelMode'] = (isset($_POST['travel_mode']) ? $_POST['travel_mode'] : 0);
      $data['Data']['type_info']['travelCost'] = (isset($_POST['travel_cost']) ? $_POST['travel_cost'] : 0);
      $data['Data']['type_info']['selectedClasses'] = (isset($_POST['sv_class']) ? implode(',', $_POST['sv_class']) : 0);
      $data['Data']['type_info']['selectedSubjects'] = (isset($_POST['sv_subject']) ? implode(',', $_POST['sv_subject']) : 0);
      
      //echo json_encode($data);exit;
      $response = $this->activity_handler->school_visit_activity($data);
      //echo "<hr>";
      //echo json_encode($response);die;
      if(isset($response['status']) && $response['status'] == 'failed'){
        $this->session->set_flashdata('alert', $response['message']);
        if($_POST['mode'] == 'edit')
          redirect($redirect_url.'?id='.$edit_id, 'refresh');
      }
      else
        $this->session->set_flashdata('alert', $response['message']);
      redirect($redirect_url, 'refresh');  
    }
    
    $this->load->model('travels_model');   
    $travel_modes = $this->travels_model->get_all();
    $this->data['travel_modes'] = $travel_modes;
    
    $this->load->model('subjects_model');   
    $subjects = $this->subjects_model->get_all(1);
    $this->data['subjects'] = $subjects;
    
    $this->load->model('Classes_model');   
    $classes = $this->Classes_model->get_all();
    $this->data['classes'] = $classes;
    
    $sdate = date('Y-m-01');
    $edate = date('Y-m-d');
    $this->load->model('school_visits_model');
    $activity_count = $this->school_visits_model->get_all_count($this->data['current_user_id'], $sdate, $edate, 'web');
    $this->data['activity_count'] = $activity_count['total'];
    
    $this->data['is_mandatory'] = false;
    $this->data['observations'] = $this->get_observations('school_visit', $this->user_state_id);
    $this->data['user_districts'] = $user_districts;
    $this->data['partial'] = $redirect_url;
    $this->data['page_title'] = 'School Visit';
    $this->load->view('templates/index', $this->data);
  }

  public function review_meeting(){
    $user_districts = $this->Users_model->spark_user_districts($this->data['current_user_id']);
    $this->data['user_districts'] = $user_districts;

    $this->data['mode'] = 'add';
    $this->data['edit_id'] =  '';
    $this->data['activity_date'] = '';
    if(!empty($_GET['id'])){
      $id = $_GET['id'];
      $this->load->model("activity_approval_model");
      $rejected_data = $this->activity_approval_model->rejected_activity_details($id);
      if(count($rejected_data) == 0){
        redirect('activity', 'refresh');
      }
      else{
        $rejected_data = $rejected_data;
        $this->data['edit_id'] =  $id;
        $this->data['mode'] =  'edit';
        $this->data['activity_date'] =  date('Y-m-d H:i', strtotime($rejected_data[0]->activity_date));
      }
    }

    $redirect_url = 'activity/review_meeting';  
    if(isset($_POST['Submit'])){
      
      if(!empty($_POST['district_id'])){
        if($_POST['district_id'] == 'other')
        {
          $district_id = $_POST['other_district_id'];
        }
        else{
          $district_id = $_POST['district_id'];
        }
      }
      else{
        $district_id = 0;
      }
      if(!empty($_POST['state_id'])){
        if($_POST['district_id'] == 'other')
        {
          $state_id = $_POST['state_id'];
        }
        else{
          $this->load->model('districts_model');
          $district_data = $this->districts_model->getById($district_id);
          $state_id = $district_data['state_id'];
        }
      }else{
        $state_id = 0;
      }
      
      $edit_id = $_POST['edit_id'];
      $json['Data']['source'] = 'web';
      $json['Data']['local_id'] = $this->user_id;
      $json['Data']['mode'] = $_POST['mode'];
      $json['Data']['type_info']['date'] = date('Y-m-d H:i:s', strtotime($_POST['meeting_date'].$_POST['meeting_time']));
      $json['Data']['type_info']['duration'] = $_POST['txtHours'].":".$_POST['txtMinutes'];
      $json['Data']['type_info']['district_id'] = $district_id;
      $json['Data']['type_info']['block_id'] = (!empty($_POST['block_id']) ? $_POST['block_id'] : 0 );
      $json['Data']['type_info']['cluster_id'] = (!empty($_POST['cluster_id']) ? $_POST['cluster_id'] : 0);
      $json['Data']['type_info']['is_internal'] = (!empty($_POST['internal_meeting']) ? 1 : 0);
      $json['Data']['type_info']['location'] = (!empty($_POST['location']) ? trim($_POST['location']) : '');
      $json['Data']['type_info']['designation'] = (!empty($_POST['meet_with']) ? trim($_POST['meet_with']) : '');
      $json['Data']['type_info']['contact_person_name'] = '';
      $json['Data']['type_info']['contact_person_number'] = '';
      $json['Data']['type_info']['meetingType'] = (!empty($_POST['meetingType']) ? $_POST['meetingType'] : '');
      $json['Data']['type_info']['meeting_mode'] = 'Physical';
      $json['Data']['type_info']['no_of_invites'] = 0;
      $json['Data']['type_info']['no_of_attendees'] = $_POST['number_of_attendes'];
      $json['Data']['type_info']['subject'] = '';
      $json['Data']['type_info']['remarks'] = '';
      $json['Data']['type_info']['agenda'] = '';
      $json['Data']['type_info']['assessment'] = $_POST['ques'];
      $json['Data']['type_info']['stateid'] = $state_id;
      $json['Data']['type_info']['travelMode'] = (isset($_POST['travel_mode']) ? $_POST['travel_mode'] : 0);
      $json['Data']['type_info']['travelCost'] = (isset($_POST['travel_cost']) ? $_POST['travel_cost'] : 0);
      
      //echo json_encode($json);
      $response = $this->activity_handler->meeting_activity($json);
      //echo json_encode($response);die;
      if(isset($response['status']) && $response['status'] == 'failed'){
        $this->session->set_flashdata('alert', $response['message']);
        if($_POST['mode'] == 'edit')
          redirect($redirect_url.'?id='.$edit_id, 'refresh');
      }
      else
        $this->session->set_flashdata('alert', $response['message']);
      redirect($redirect_url, 'refresh');
      
    }
    
    $this->load->model('travels_model');   
    $travel_modes = $this->travels_model->get_all();
    $this->data['travel_modes'] = $travel_modes;
    
    $sdate = date('Y-m-01');
    $edate = date('Y-m-d');
    $this->load->model('meetings_model');
    $activity_count = $this->meetings_model->get_all_count($this->data['current_user_id'], $sdate, $edate, 'web');
    $this->data['activity_count'] = $activity_count['total'];
    
    $this->data['is_mandatory'] = false;
    $this->data['observations'] = $this->get_observations('meeting', $this->user_state_id);
    $this->data['partial'] = $redirect_url;
    $this->data['page_title'] = 'Review Meeting';
    $this->load->view('templates/index', $this->data);
  }

  public function training(){

    $user_districts = $this->Users_model->spark_user_districts($this->data['current_user_id']);
    $this->data['user_districts'] = $user_districts;

    $this->data['mode'] = 'add';
    $this->data['edit_id'] =  '';
    $this->data['activity_date'] = '';
    if(!empty($_GET['id'])){
      $id = $_GET['id'];
      $this->load->model("activity_approval_model");
      $rejected_data = $this->activity_approval_model->rejected_activity_details($id);
      if(count($rejected_data) == 0){
        redirect('activity', 'refresh');
      }
      else{
        $rejected_data = $rejected_data;
        $this->data['edit_id'] =  $id;
        $this->data['mode'] =  'edit';
        $this->data['activity_date'] =  date('Y-m-d H:i', strtotime($rejected_data[0]->activity_date));
      }
    }

    $redirect_url = 'activity/training';
      
    if(isset($_POST['Submit'])){

      if($_POST['district_id'] == 'other')
      {
        $state_id = $_POST['state_id'];
        $district_id = $_POST['other_district_id'];
      }
      else{
        $district_id = $_POST['district_id'];
        $this->load->model('districts_model');
        $district_data = $this->districts_model->getById($district_id);
        $state_id = $district_data['state_id'];
      }
      
      $edit_id = $_POST['edit_id'];
      $json['Data']['source'] = 'web';
      $json['Data']['local_id'] = $this->user_id;
      $json['Data']['mode'] = $_POST['mode'];
      $json['Data']['type_info']['date'] = date('Y-m-d H:i:s', strtotime($_POST['training_date'].$_POST['training_time']));
      $json['Data']['type_info']['training_duration'] = $_POST['txtHours'].":".$_POST['txtMinutes'];
      $json['Data']['type_info']['state_id'] = $state_id;
      $json['Data']['type_info']['cluster_id'] = 0;
      $json['Data']['type_info']['district_id'] = $district_id;
      $json['Data']['type_info']['block_id'] = $_POST['block_id'];
      $json['Data']['type_info']['role'] = 'trainer';
      $json['Data']['type_info']['training_type'] =  $_POST['training_type'];
      $json['Data']['type_info']['training_mode'] = 'internal';
      $json['Data']['type_info']['location'] = (!empty($_POST['location']) ? trim($_POST['location']) : '');
      $json['Data']['type_info']['no_of_days'] = 1;
      $json['Data']['type_info']['no_of_attendees'] = $_POST['number_of_attendes'];
      $json['Data']['type_info']['no_of_invitees'] = $_POST['number_of_invitees'];
      $json['Data']['type_info']['training_subject'] =  $_POST['subject'];
      $json['Data']['type_info']['topic'] = '';
      $json['Data']['type_info']['agenda'] = '';
      $json['Data']['type_info']['location'] = '';
      $json['Data']['type_info']['assessment'] = $_POST['ques'];
      $json['Data']['type_info']['travelMode'] = (isset($_POST['travel_mode']) ? $_POST['travel_mode'] : 0);
      $json['Data']['type_info']['travelCost'] = (isset($_POST['travel_cost']) ? $_POST['travel_cost'] : 0);
      
      //echo json_encode($json);
      $response = $this->activity_handler->training_activity($json);
      //echo json_encode($response);die;
      if(isset($response['status']) && $response['status'] == 'failed'){
        $this->session->set_flashdata('alert', $response['message']);
        if($_POST['mode'] == 'edit')
          redirect($redirect_url.'?id='.$edit_id, 'refresh');
      }
      else
        $this->session->set_flashdata('alert', $response['message']);
          redirect($redirect_url, 'refresh');
    }
    
    $this->load->model('travels_model');   
    $travel_modes = $this->travels_model->get_all();
    $this->data['travel_modes'] = $travel_modes;
    
    $this->data['is_mandatory'] = true;
    $this->data['observations'] = $this->get_observations('training', $this->user_state_id);
    
    $this->load->model('subjects_model');
    $subjects = $this->subjects_model->get_all();
    
    $sdate = date('Y-m-01');
    $edate = date('Y-m-d');
    $this->load->model('trainings_model');
    $activity_count = $this->trainings_model->get_all_count($this->data['current_user_id'], $sdate, $edate, 'web');
    $this->data['activity_count'] = $activity_count['total'];
    
    $this->data['subjects'] = $subjects;
    $this->data['partial'] = $redirect_url;
    $this->data['page_title'] = 'Review Training';
    $this->load->view('templates/index', $this->data);
  }
  
  function fetch_observation()
  {
    $typeKey = $_GET['typeKey'];
    $state_id = $_GET['state_id'];
    
    $observations = $this->get_observations($typeKey, $state_id);
    $this->data['observations'] = $observations;
    
    $this->load->view('activity/observations',$this->data);
  }
  
  function get_observations($typeKey, $state_id)
  {
    $this->load->model('observations_model');
    $this->load->model('classes_model');
    $classes = $this->classes_model->get_all();
    
    $newVal = (object) array('id'=>0, 'name'=>0);
    array_push($classes,$newVal);
        
    foreach($classes as $class){  
      $get_observations = $this->observations_model->getObservationBystateClass($state_id, $typeKey, $class->id);
      //echo "<br>class :: ".$class->id;
      //print_r($get_observations);
      if(!empty($get_observations))
      {
        $cur_observations = array();
        foreach ($get_observations as $get_observation)
        {
          if ($get_observation->question_type == "subjective")
          {
            array_push($cur_observations,$get_observation);
          }
          else if ($get_observation->subject_wise == 1){
            $check_com = $get_observation->state_id."-".$class->id;
            $check_val = $this->check_observation_allowed($check_com);
            if($check_val){
              if($get_observation->question_type == 'objective'){
                $options = array();
                $question_options = $this->observations_model->getObtionsByObservation($get_observation->id);
                foreach($question_options as $question_option)
                {
                  $options[$question_option->id] = $question_option->option_value;
                }
                //$get_observation->question_type = 'objective';
                $get_observation->options = $options;
                array_push($cur_observations,$get_observation);
              }
              else{
                array_push($cur_observations,$get_observation);
              }
            }
            else{
              if($get_observation->class_id == 0 && $typeKey == 'school_visit'){
                if($get_observation->question_type == 'objective'){
                  $options = array();
                  $question_options = $this->observations_model->getObtionsByObservation($get_observation->id);
                  foreach($question_options as $question_option)
                  {
                    $options[$question_option->id] = $question_option->option_value;
                  }
                  //$get_observation->question_type = 'objective';
                  $get_observation->options = $options;
                  array_push($cur_observations,$get_observation);
                }
                else{
                  array_push($cur_observations,$get_observation);
                }
              }
            }
          }
          else
          {
            if($get_observation->question_type == 'yes-no'){
              $get_observation->question_type = 'yes-no';
            }
            else{
              $options = array();
              $question_options = $this->observations_model->getObtionsByObservation($get_observation->id);
              foreach($question_options as $question_option)
              {
                $options[$question_option->id] = $question_option->option_value;
              }
              //$get_observation->question_type = 'objective'; //comment by ravi 23-Oct-2019
              $get_observation->options = $options;
            }
            array_push($cur_observations,$get_observation);
          }
        }
        $observation[$typeKey][$class->name] = $cur_observations; 
      }
      else  
        $observation[$typeKey][$class->name] = array(); 
     }
    return $observation[$typeKey];
  }
  
  
  function check_observation_allowed($check_com)
  {
    $this->load->Model('observations_model');
    $get_mappings = $this->observations_model->get_all_mapping();
    $state_class_subjects = array();
    foreach($get_mappings as $get_mapping)
    {
      if ($get_mapping->state_id > 0 and $get_mapping->class_id > 0 and $get_mapping->subject_id > 0)
      {
        $state_subject = Array();
        $state_subject['state_id'] = $get_mapping->state_id;
        $state_subject['class_id'] = $get_mapping->class_id;
        $state_subject['subject_id'] = $get_mapping->subject_id;
        array_push($state_class_subjects,$get_mapping->state_id."-".$get_mapping->class_id);
      }
    }
    if(in_array($check_com,$state_class_subjects))
    {
      $observation_allowed = 1;
    }
    else
    {
      $observation_allowed = 0;
    }
    return $observation_allowed;
  }
  

  public function call(){
    
    $user_districts = $this->Users_model->spark_user_districts($this->data['current_user_id']);
    $this->data['user_districts'] = $user_districts;

    $this->data['mode'] = 'add';
    $this->data['edit_id'] =  '';
    $this->data['activity_date'] = '';
    if(!empty($_GET['id'])){
      $id = $_GET['id'];
      $this->load->model("activity_approval_model");
      $rejected_data = $this->activity_approval_model->rejected_activity_details($id);
      if(count($rejected_data) == 0){
        redirect('activity', 'refresh');
      }
      else{
        $rejected_data = $rejected_data;
        $this->data['edit_id'] =  $id;
        $this->data['mode'] =  'edit';
        $this->data['activity_date'] =  date('Y-m-d H:i', strtotime($rejected_data[0]->activity_date));
      }
    }

    $redirect_url = 'activity/call';
    
    if(isset($_POST['Submit'])){
      $edit_id = $_POST['edit_id'];
      $json['Data']['mode'] = $_POST['mode'];
      //print_r($_POST);die;
      $contact_json['Data']['local_id'] = $this->user_id;
      $contact_json['Data']['date'] = date('Y-m-d H:i:s', strtotime($_POST['call_date'].$_POST['call_time']));
      $contact_json['Data']['district_id'] = $_POST['district_id'];
      $contact_json['Data']['cluster_id'] = (!empty($_POST['cluster_id']) ? $_POST['cluster_id'] : 0);
      $contact_json['Data']['block_id'] = (!empty($_POST['block_id']) ? $_POST['block_id'] : 0);
      $contact_json['Data']['firstname'] = $_POST['firstname'];
      $contact_json['Data']['lastname'] = $_POST['lastname'];
      $contact_json['Data']['mobile'] = $_POST['mobile'];
      $contact_json['Data']['designation'] = $_POST['designation'];
      $contact_json['Data']['source'] = $_POST['source'];

      $add_contact = $this->activity_handler->addContact_post($contact_json);
      $contact_person_id = $add_contact['contact_id'];

      $json['Data']['source'] = 'web';
      $json['Data']['local_id'] = $this->user_id;
      $json['Data']['date'] = date('Y-m-d H:i:s', strtotime($_POST['call_date']));
      $json['Data']['type_info']['district_id'] =  $_POST['district_id'];
      $json['Data']['type_info']['cluster_id'] = (!empty($_POST['cluster_id']) ? $_POST['cluster_id'] : 0);
      $json['Data']['type_info']['block_id'] = (!empty($_POST['block_id']) ? $_POST['block_id'] : 0);
      $json['Data']['type_info']['contact_person_id'] = $contact_person_id;
      
      $response = $this->activity_handler->call_activity($json);
      if(isset($response['status']) && $response['status'] == 'failed'){
        $this->session->set_flashdata('alert', $response['message']);
        if($_POST['mode'] == 'edit')
          redirect($redirect_url.'?id='.$edit_id, 'refresh');
      }
      else
        $this->session->set_flashdata('alert', $response['message']);
      redirect($redirect_url, 'refresh');
    }

    $this->data['partial'] = $redirect_url;
    $this->data['page_title'] = 'Arrange a Call';
    $this->load->view('templates/index', $this->data);
  }

  public function no_visit(){

    $this->data['mode'] = 'add';
    $this->data['edit_id'] =  '';
    $this->data['activity_date'] = '';
    if(!empty($_GET['id'])){
      $id = $_GET['id'];
      $this->load->model("activity_approval_model");
      $rejected_data = $this->activity_approval_model->rejected_activity_details($id);
      if(count($rejected_data) == 0){
        redirect('activity', 'refresh');
      }
      else{
        $rejected_data = $rejected_data;
        $this->data['edit_id'] =  $id;
        $this->data['mode'] =  'edit';
        $this->data['activity_date'] =  date('Y-m-d H:i', strtotime($rejected_data[0]->activity_date));
      }
    }
    
    $redirect_url = 'activity/no_visit';
    if(isset($_POST['Submit'])){
      $edit_id = $_POST['edit_id'];
      $data['Data']['source'] = 'web';
      $data['Data']['local_id'] = $this->user_id;
      $data['Data']['mode'] = $_POST['mode'];
      $data['Data']['type_info']['date_of_no_visit'] = date('Y-m-d H:i:s', strtotime($_POST['no_visit_date']));
      $data['Data']['type_info']['no_visit_reason'] = ($_POST['reason'] != 'other' ? $_POST['reason'] : $_POST['other_reason']);
      $response = $this->activity_handler->no_visit_day($data);
      if(isset($response['status']) && $response['status'] == 'failed'){
        $this->session->set_flashdata('alert', $response['message']);
        if($_POST['mode'] == 'edit')
          redirect($redirect_url.'?id='.$edit_id, 'refresh');
      }
      else
        $this->session->set_flashdata('alert', $response['message']);
      redirect($redirect_url, 'refresh');
      
    }

    $this->data['partial'] = $redirect_url;
    $this->data['page_title'] = 'No Visit Day';
    $this->load->view('templates/index', $this->data);
  }


   public function my_leave_request(){
	 
		$this->load->Model('Leaves_model'); 		
    $this->data['mode'] = 'add';
    $this->data['edit_id'] =  '';

    $redirect_url = 'activity/leave_request';
    if(isset($_POST['Submit'])){
      $edit_id = $_POST['edit_id'];
      
      $exp_type = explode('_', $_POST['leave_type']);
      //$leave_type = $_POST['leave_type'];
      $leave_type = $exp_type[0];
      $json['Data']['source'] = 'web';
      $json['Data']['local_id'] = $this->user_id;
      $json['Data']['mode'] = $_POST['mode'];
      $json['Data']['type_info']['leave_from_date'] = date('Y-m-d', strtotime($_POST['from_date']));
      $json['Data']['type_info']['leave_end_date'] = date('Y-m-d', strtotime($_POST['to_date']));
      $json['Data']['type_info']['leave_type'] = $leave_type;
      $json['Data']['type_info']['total_days'] = $_POST['total_days'];
      $json['Data']['type_info']['day_type'] = $_POST['day_type'];
      $json['Data']['type_info']['reason'] = $_POST['reason'];

      $response = $this->activity_handler->leave_activity($json);
      
      //print_r($response);die;
      if(isset($response['status']) && $response['status'] == 'failed'){
        $this->session->set_flashdata('alert', $response['message']);
        //if($_POST['mode'] == 'edit')
        //redirect($redirect_url.'?id='.$edit_id, 'refresh');
      }
      else{
				$creditData = $this->Leaves_model->get_leave_credits($this->user_id, $leave_type);
				if(!empty($creditData)){
					$mdata['leave_taken'] = $creditData[0]->leave_taken+$_POST['total_days'];  
					$this->Leaves_model->update_leave_credit($mdata, $creditData[0]->id);    
				}  
        $this->session->set_flashdata('success', $response['message']);
      }
      redirect($redirect_url, 'refresh');
    }
    
    $earnedLeaves = array();
    $earnedData = $this->Leaves_model->get_leave_credits($this->data['current_user_id']);
    
    foreach($earnedData as $earned)
    {
			$leave_left = $earned->leave_earned - $earned->leave_taken;
			$earnedLeaves[$earned->leave_type] = $leave_left;
		}
    
    $this->data['earnedLeaves'] = $earnedLeaves;
    $this->data['partial'] = 'activity/leave_post';
    $this->data['page_title'] = 'Post Leave';
    $this->load->view('templates/index', $this->data);
  }

    
  function get_districts()
  {
    $state_id = $_GET['sid'];
    $this->load->model('districts_model');
    $state_district = $this->districts_model->get_state_districts($state_id);
    echo json_encode($state_district);
  }

  function get_blocks()
  {
    $district_id = $_GET['did'];
    $bid = $_GET['bid'];
    if($this->data['current_role'] == 'state_person'){
      $user_blocks = $this->Users_model->spark_user_blocks('', $district_id);
    }
    else if(!empty($bid) && $bid == 'other'){
      $this->load->model('blocks_model');
      $user_blocks = $this->blocks_model->get_district_blocks($district_id,"");
    }
    else{
      $user_blocks = $this->Users_model->spark_user_blocks($this->data['current_user_id'], $district_id);
    }
    echo json_encode($user_blocks);
  }

  function get_state()
  {
    $this->load->model('states_model');
    $states = array();
    $data = $this->states_model->get_all_states();
    echo json_encode($data);
  }

   public function validate_activity_date($activity_date, $days='7') {
        $activity_date = date("Y-m-d", strtotime($activity_date));
        $previous_date = date("Y-m-d", strtotime("-$days day"));
        error_log("Account.php Activity Date valiation the date is =".$activity_date);
        error_log("date -".MAX_PREVIOUS_DAYS." ".$previous_date);
        if (($activity_date < $previous_date) || ($activity_date > date("Y-m-d"))) {
            error_log(" activity date validation failed. ");
            return false;
        } else {
            error_log(" activity date validation passed. ");
            return true;
        }
    }

   public function validate_hierarchy($state_id, $district_ids, $block_id, $cluster_id)
   {
      $this->load->model("districts_model");
      $district_array = $this->districts_model->check_district_by_State($district_ids, $state_id);
      if (empty($district_array)){
          $response = array("http_code" => 401, 'status' => 'failed', 'message' => 'District does not exist in this State');
          return $response;
      }

      $this->load->model("blocks_model");
      $block_array = $this->blocks_model->check_block_by_DistrictState($block_id, $district_ids, $state_id);
      if (empty($block_array)){
          $response = array("http_code" => 401, 'status' => 'failed', 'message' => 'Block does not exist in this District');
          return $response;
      }    

      $this->load->model("clusters_model");
      $cluster_array = $this->clusters_model->check_cluster_by_blockDistrictState($cluster_id, $block_id, $district_ids, $state_id);
      if (empty($cluster_array)){
          $response = array("http_code" => 401, 'status' => 'failed', 'message' => 'Cluster does not exist in this block');
          return $response;
      }
    }
    
  public function approve_activity($activity_type, $id){
    $res = $this->activity_approval_model->approve_activity($activity_type, $id, $this->data['current_user_id']);
    echo $res;
  }

  public function get_call_contact_list()
  {
    $district_id = $_POST['did'];
    $cluster_id = (!empty($_POST['cid']) ? $_POST['cid'] : 0);
    $block_id = (!empty($_POST['bid']) ? $_POST['bid'] : 0);
    $source = $_POST['source'];
    $this->load->model('contact_model');
    $results = $this->contact_model->getContacts('', $district_id, $block_id, $cluster_id, $source);
    //print_r($results);
    if(count($results) > 0){
      foreach ($results as $result){
        $row1['name'] = ucwords(htmlentities(stripslashes($result['firstname'])));
        $row1['id'] = $result['id'];
        $row_set[] = $row1;
      }
      echo json_encode($row_set); die;//format the array into json data
    }
  }

   public function my_activity(){

      $this->load->model('No_visit_days_model');
      $this->load->model('school_visits_model');
      $this->load->model('Meetings_model');
      $this->load->model('Leaves_model');
      $this->load->model('Trainings_model');
      $this->load->model('Calls_model');
      $this->load->model('Users_model');
      $this->load->model('Claim_model');

      $spark_id = $this->data['current_user_id'];
      $state_id = 0;
      $spark_state_id = $this->data['user_state_id'];
      if(isset($_POST['spark_id']) and $_POST['spark_id'] > 0)
      {
        $spark_state_id = (isset($_GET['spark_state_id']) ? $_POST['spark_state_id'] : 0);
        $spark_detail = $_POST['spark_id'];
        $spark_details = explode("-",$spark_detail);
        $spark_id = $spark_details[0];
      }
      
      //$user_id = $_POST['user_login_id'];
      if(isset($_POST['activitystartdate']) and isset($_POST['activityenddate'])){
        $start_month = $_POST['activitystartdate'];
        $_fromDate = date('Y-m-d', strtotime($start_month));
        
        $end_month = $_POST['activityenddate'];
        $_toDate = date('Y-m-d', strtotime($end_month));
      }
      else{
        redirect('activity');
      }
      
      
      $this->data['spark_id'] = $spark_id;
      $this->data['spark_state_id'] = $spark_state_id;
      $this->data['spark_name'] = $this->Users_model->getById($spark_id);
      $this->data['report_date'] = $_fromDate;
      $this->data['report_end_date'] = $_toDate;

      $activities = array();
      $date = $_fromDate;
      $i = 1;
      while($date <= $_toDate)
      {
        $activities[$date] = array();
        $date = date('Y-m-d', strtotime('+'.$i.' day', strtotime($_fromDate)));
        $i++;
      }
	  
      $no_visit_days = $this->No_visit_days_model->get_approval_activities($spark_id, $_fromDate, $_toDate, 'all');
      //echo "<br>".$this->No_visit_days_model->defaultdb->last_query();
      $school_visits = $this->school_visits_model->get_approval_activities($spark_id, $_fromDate, $_toDate, 'all');
      //echo "<br>".$this->school_visits_model->defaultdb->last_query();
      $meetings = $this->Meetings_model->get_approval_activities($spark_id, $_fromDate, $_toDate, 'all');
      //echo "<br>".$this->Meetings_model->defaultdb->last_query();
      $leaves = $this->Leaves_model->get_approval_activities($spark_id, $_fromDate, '', 'all');
      //echo "<br>".$this->Leaves_model->defaultdb->last_query();
      $trainings = $this->Trainings_model->get_approval_activities($spark_id, $_fromDate, $_toDate, 'all');
      //echo "<br>".$this->Trainings_model->defaultdb->last_query();

      $longdistances = $this->Claim_model->get_longdistance_activities($spark_id, $_fromDate, $_toDate);


      $state_ids = array();
      $district_ids = array();
      $block_ids = array();
      $cluster_ids = array();
      $school_ids = array();

      foreach($no_visit_days as $key=>$no_visit_day) { 

        //print_r($no_visit_day);
        $activity = array();
        $activity['activity_type'] = 'No Activity Day';
        $activity['type'] = 'no_visit_days';
        $activity['state'] = $no_visit_day->state_id;
        $activity['id'] = $no_visit_day->id;
        $activity['district'] = $no_visit_day->district_id;
        $activity['block'] = "";//$no_visit_day->block_id;
        $activity['cluster'] = "";//$no_visit_day->cluster_id;
        $activity['status'] = $no_visit_day->status;
        $activity['school'] = 0;
        $activity['duration'] = '';
        $activity['details'] = '<strong>Reason :</strong>'.$no_visit_day->no_visit_reason;
        $activity['time'] = date("H:i",strtotime($no_visit_day->activity_date));
        $activity['source'] = $no_visit_day->source;
        
        array_push($activities[date("Y-m-d",strtotime($no_visit_day->activity_date))],$activity);
        array_push($state_ids,$no_visit_day->state_id);
        array_push($district_ids,$no_visit_day->district_id);
        array_push($block_ids,"");
        array_push($cluster_ids,"");
      }

      //echo '<br>SCHOOL VISIT';
      foreach($school_visits as $key=>$school_visit) {

        //print_r($school_visit);
        $activity = array();
        $activity['activity_type'] = 'School Visit';
        $activity['type'] = 'school_visits';
        $activity['state'] = $school_visit->state_id;
        $activity['id'] = $school_visit->id;
        $activity['district'] = $school_visit->district_id;
        $activity['block'] = $school_visit->block_id;
        $activity['cluster'] = $school_visit->cluster_id;
        $activity['school'] = $school_visit->school_id;
        $activity['status'] = $school_visit->status;
        $activity['duration'] = $school_visit->duration;
        $activity['details'] = '';
        $activity['time'] = date("H:i",strtotime($school_visit->activity_date));
        $activity['source'] = $school_visit->source;
        
        array_push($activities[date("Y-m-d",strtotime($school_visit->activity_date))],$activity);
        array_push($state_ids,$school_visit->state_id);
        array_push($district_ids,$school_visit->district_id);
        array_push($block_ids,$school_visit->block_id);
        array_push($cluster_ids,$school_visit->cluster_id);
        array_push($school_ids,$school_visit->school_id);
      }

      //echo '<br>MEETINGS';
      foreach($meetings as $key=>$meeting) { 

        //print_r($meeting);
        $activity = array();
        $activity['activity_type'] = 'Meeting';
        $activity['type'] = 'meetings';
        $activity['state'] = $meeting->state_id;
        $activity['id'] = $meeting->id;
        $activity['district'] = $meeting->district_id;
        $activity['block'] = $meeting->block_id;
        $activity['cluster'] = $meeting->cluster_id;
        $activity['status'] = $meeting->status;
        $activity['school'] = 0;
        $activity['duration'] = $meeting->duration;
        //$activity['details'].= '<br><strong>No Of Invites : </strong>'.$meeting->no_of_invites;
        $activity['details'] = '<strong>Attendees : </strong>'.$meeting->no_of_attendees;
        if($meeting->meet_with != '')
          $activity['details'].= '<br><strong>Meet With :</strong>'.$meeting->meet_with;
        $activity['time'] = date("H:i",strtotime($meeting->activity_date));
        $activity['source'] = $meeting->source;
        
        array_push($activities[date("Y-m-d",strtotime($meeting->activity_date))],$activity);
        array_push($state_ids,$meeting->state_id);
        array_push($district_ids,$meeting->district_id);
        array_push($block_ids,$meeting->block_id);
        array_push($cluster_ids,$meeting->cluster_id);
      }

      //echo 'TRAININGS';
      foreach($trainings as $key=>$training) { 

        //print_r($training);
        $activity = array();
        $activity['activity_type'] = 'Training';
        $activity['type'] = 'trainings';
        $activity['state'] = $training->state_id;
        $activity['id'] = $training->id;
        $activity['district'] = $training->district_id;
        $activity['block'] = $training->block_id;
        $activity['cluster'] = $training->cluster_id;
        $activity['status'] = $training->status;
        $activity['school'] = 0;    
        $activity['duration'] = $training->training_duration;
        $activity['details'] = '<strong>Teachers Trained : </strong>'.$training->no_of_teachers_trained;   
        $activity['time'] = date("H:i",strtotime($training->activity_date));
        $activity['source'] = $training->source;
        
        array_push($activities[date("Y-m-d",strtotime($training->activity_date))],$activity);      
        array_push($state_ids,$training->state_id);
        array_push($district_ids,$training->district_id);
        array_push($block_ids,$training->block_id);
        array_push($cluster_ids,$training->cluster_id);
      }
      
      //echo 'LEAVES';
      foreach($leaves as $key=>$leave) { 

        $activity = array();
        $activity['activity_type'] = 'Leave';
        $activity['type'] = 'leaves';
        $activity['state'] = $leave->state_id;
        $activity['id'] = $leave->id;
        $activity['district'] = $leave->district_id;
        $activity['status'] = $leave->status;
        $activity['block'] = 0;
        $activity['cluster'] = 0;
        $activity['school'] = 0;
        $activity['duration'] = '';
        $activity['details'] = '<strong>Leave Type : </strong>'.$leave->leave_type.'<br><strong>Leave From : </strong>'.date("d-m-Y",strtotime($leave->leave_from_date)).'<br><strong>Leave Till : </strong>'.date("d-m-Y",strtotime($leave->leave_end_date)).'<br><strong>No Of Days : </strong>'.$leave->total_days;
        $activity['time'] = '';
        $activity['source'] = $leave->source;
        
        if(isset($activities[date("Y-m-d",strtotime($leave->leave_from_date))]))
          array_push($activities[date("Y-m-d",strtotime($leave->leave_from_date))],$activity);      
        array_push($state_ids,$leave->state_id);
        array_push($district_ids,$leave->district_id);
      }
      
      //echo '<br>LONG DISTANCE';
      foreach($longdistances as $key=>$longdistance) {
        
        $fromaddress = explode(',',$longdistance->from_address);
        $from_address = implode(', ',$fromaddress);
        
        $toaddress = explode(',',$longdistance->to_address);
        $to_address = implode(', ',$toaddress);
        
        $activity = array();
        $activity['activity_type'] = 'Long Distance';
        $activity['type'] = 'long_distance';
        $activity['id'] = $longdistance->id;
        $activity['state'] = '';
        $activity['district'] = '';
        $activity['block'] = '';
        $activity['cluster'] = '';
        $activity['school'] = '';
        $activity['status'] = '';
        $activity['duration'] = $longdistance->duration;
        $activity['details'] = '<strong>From: </strong>'.trim(strip_tags($from_address)).'<br><strong>To: </strong>'.trim(strip_tags($to_address));
        $activity['time'] = date("H:i",strtotime($longdistance->activity_date));
        $activity['source'] = $longdistance->source;
        
        array_push($activities[date("Y-m-d",strtotime($longdistance->activity_date))],$activity);
      }

      
      $state_names = array();
      $state_names[0] = '';
      if (count($state_ids) > 0)
      {
        $this->load->model('states_model');  
        $states = $this->states_model->get_by_ids($state_ids);
        foreach($states as $state)
        {
          $state_names[$state->id] = $state->name;
        }
      }

      $district_names = array();
      $district_names[0] = '';
      if (count($district_ids) > 0)
      {
        $this->load->model('districts_model');
        $districts = $this->districts_model->getByIds($district_ids);
        foreach($districts as $district)
        {
          $district_names[$district->id] = $district->name;
        }
      }

      $block_names = array();
      $block_names[0] = '';
      if (count($block_ids) > 0)
      {
        $this->load->model('blocks_model');
        $blocks = $this->blocks_model->getByIds($block_ids);

        $block_names = array();
        $block_names[0] = '';
        foreach($blocks as $block)
        {
          $block_names[$block->id] = $block->name;
        }
      }

      $cluster_names = array();
      $cluster_names[0] = '';
      if (count($cluster_ids) > 0)
      {
        $this->load->model('clusters_model');
        $clusters = $this->clusters_model->getByIds($cluster_ids);
        foreach($clusters as $cluster)
        {
          $cluster_names[$cluster->id] = $cluster->name;
        }
      }
   
      $school_names = array();
      $school_names[0] = '';
      if (count($school_ids) > 0)
      {
        $this->load->model('schools_model');
        $schools = $this->schools_model->getByIds($school_ids);
        foreach($schools as $school)
        {
          $school_names[$school->id] = $school->name." (".$school->dise_code.")";
        }
      }
      
      $this->data['school_names'] = $school_names;
      $this->data['cluster_names'] = $cluster_names;
      $this->data['block_names'] = $block_names;
      $this->data['district_names'] = $district_names;
      $this->data['state_names'] = $state_names;
      $this->data['activities'] = $activities;
      $this->data['post_data'] = $_POST;
      $this->data['pdf'] = 0;
      $this->data['excel'] = 0;

      /*if(isset($_POST['download_type']) and $_POST['download_type'] == 'pdf'){
        $this->data['pdf'] = 1;
        $summary = $this->load->view('reports/spark_daily_activity_report',$this->data,true);
        $this->common_functions->create_pdf($summary,'spark_daily_activity_report');
      }
      elseif(isset($_POST['download_type']) and $_POST['download_type'] == 'excel'){
          $this->data['excel'] = 1;
          $myFile = "./uploads/spark_daily_activity_report.xls";
          $this->load->library('parser');

          //pass retrieved data into template and return as a string
          $stringData = $this->parser->parse('reports/spark_daily_activity_report', $this->data, true);

          //open excel and write string into excel
          $fh = fopen($myFile, 'w') or die("can't open file");
          fwrite($fh, $stringData);

          fclose($fh);
          //download excel file
          $this->downloadExcelNew("spark_daily_activity_report");
          unlink($myFile);
      }
      else{*/
        $this->data['partial'] = 'activity/detail';
        $this->load->view('templates/index', $this->data);
      //}

  }
  

  function get_assessment_data()
  {
    $this->load->model('Assessment_model');
    $this->load->model('Observations_model');
    
    $type = $_POST['activity_type'];
    $activity_id = $_POST['activity_id'];
    
    $data = $this->Assessment_model->getByActivityId($type, $activity_id);
    
    $data_arr = array();
    $i = 0;
    foreach($data as $ac_data){
      $data_arr[$i]['question_id'] = $ac_data->question_id;
      $data_arr[$i]['question'] = $ac_data->question;
      $class_id = $ac_data->class_id;
      $question_type = $ac_data->question_type;
      $data_arr[$i]['question_type'] = $question_type;
      $data_arr[$i]['subject_wise'] = $ac_data->subject_wise;
      $data_arr[$i]['class_name'] = '';
      $data_arr[$i]['subject_name'] = '';
      if($class_id != 0)
      {
        $this->load->model('Classes_model');
        $class_data = $this->Classes_model->getById($class_id); 
        $data_arr[$i]['class_name'] = $class_data['name'];
      }
      
      if($ac_data->subject_wise == 1){
        $this->load->model('Subjects_model');
        $sdata = $this->Subjects_model->getById($ac_data->subject_id);
        $data_arr[$i]['subject_name'] = $sdata['name'];
      }
      
      if($question_type == 'subjective'){
        $data_arr[$i]['answer'] = $ac_data->answer;
      }
      else if($question_type == 'objective'){
        if($ac_data->answer != ''){
          $optionData = $this->Observations_model->getOptionById($ac_data->answer);
          $data_arr[$i]['answer'] = (!empty($optionData) ? $optionData[0]->option_value : '');
        }
        else{
          $data_arr[$i]['answer'] = '';
        }
      }
      else if($question_type == 'yes-no'){
        if($ac_data->answer == 0)
          $data_arr[$i]['answer'] = 'No';
        else
          $data_arr[$i]['answer'] = 'Yes';
      }
      else if($question_type == 'yes-no-na'){
        if($ac_data->answer == 0)
          $data_arr[$i]['answer'] = 'N/A';
        else if($ac_data->answer == 1)
          $data_arr[$i]['answer'] = 'Yes';
        else if($ac_data->answer == 2)
          $data_arr[$i]['answer'] = 'No';  
      }
      else{
        $data_arr[$i]['answer'] = $ac_data->answer;
      }
      $i++;
    }
    $newArr = array();
    $m = 0;
    foreach($data_arr as $dataarr)
    {
      if(!empty($dataarr['class_name'])){
        if(!empty($dataarr['subject_name'])){
          if(isset($dataarr['answer']) && $dataarr['answer'] !=''){
            $newArr[$dataarr['class_name']][$dataarr['subject_name']]['question'][] = $dataarr['question'];
            $newArr[$dataarr['class_name']][$dataarr['subject_name']]['answer'][] = $dataarr['answer'];
          }
        }
        else{
          if(isset($dataarr['answer']) && $dataarr['answer'] !=''){
            $newArr[$dataarr['class_name']]['no_subject']['question'][] = $dataarr['question'];
            $newArr[$dataarr['class_name']]['no_subject']['answer'][] = $dataarr['answer'];
          }
        }
      }
      else{
        if(isset($dataarr['answer']) && $dataarr['answer'] !=''){
          $newArr['no_class']['no_subject']['question'][] = $dataarr['question'];
          $newArr['no_class']['no_subject']['answer'][] = $dataarr['answer'];
        }
      }
      $m++;
    }
    $assessment = $newArr;

    $this->data['assessment'] = $assessment;
    
    $this->load->view('activity/assessment_data',$this->data);
  }


  function get_user_type()
  {
    $is_validator = 0;
    //$user_data = $this->users_model->getById($this->data['current_user_id']);

    if($this->data['current_role'] == 'manager'){
      $is_validator = 1;
    }
    else{
      $this->load->model('users_model');
      $user_count = $this->users_model->get_managers_user_count($this->data['current_user_id']);
      if($user_count > 0){
        $is_validator = 1;
      }
    }
    return $is_validator;
  }
  
  public function attendance_list()
  {
    $spark_id = $this->data['current_user_id'];
    $state_id = '';
    $time_filter = '';
    $this->load->Model('states_model');
      
    $attendance_dates_array = array();
    $attendance_array = array();
    $attendance_reg = array();
    $attandance_data = array();
    $activitydatestart = date('Y-m-d');
    $activitydateend = date('Y-m-d');
    $this->data['state_list'] = $this->states_model->get_all_states();
    
    if(isset($_POST) && !empty($_POST['submit']))
    {
      $this->load->Model('Users_model');
      $this->load->Model('Attendance_model');
      $time_filter = $_POST['time_filter'];
      
      if($this->data['current_role'] == 'super_admin' || $this->data['current_role'] == 'accounts' || $this->data['current_role'] == 'HR'){
        $state_id = $_POST['state_id'];
        $sparkData = $this->Users_model->getSparkByCurrentState($state_id);
        $spark_ids = array();
        foreach($sparkData as $sparks){
          $spark_ids[] = $sparks->user_id;
        }
      }
      else if($this->data['current_role'] == 'state_person'){
        $state_id = $this->data['user_state_id'];
        $spark_role = array('field_user', 'manager');  
        $users_list = $this->Users_model->getSparkByState($state_id, $spark_role);
        
        foreach ($users_list as $row){
          $spark_ids[] = $row->user_id;
        }
        $spark_ids[] = $spark_id;
      }
      else if($this->data['current_role'] == 'manager'){
        $this->load->Model('team_members_model');
        $team_members_list = $this->team_members_model->get_all_team_members($this->data['current_user_id']);
        foreach($team_members_list as $team_members)
        {
          $spark_ids[] = $team_members->id;
        }
        $spark_ids[] = $spark_id;
      }
      else{
        $spark_ids[] = $spark_id;
      }
      //print_r($spark_ids);
      $activitydatestart = (!empty($_POST['activitydatestart']) ? date('Y-m-d',strtotime($_POST['activitydatestart'])) : date('Y-m-d'));
      $activitydateend = (!empty($_POST['activitydateend']) ? date('Y-m-d',strtotime($_POST['activitydateend'])) : date('Y-m-d'));
      
      $activity_type = array('attendance-in', 'attendance-out');
      
      $this->load->model('Tracking_model');   
      $attandance_data = $this->Tracking_model->get_track_attendance_list($spark_ids, $activitydatestart, $activitydateend, $activity_type);
      
      //print_r($attandance_data);die;
      $attendance_array = array();
      foreach($attandance_data as $attandances){
          $login_id = $attandances->login_id;
          $login_name = $attandances->name;
          $tracking_time = $attandances->tracking_time;
          $activity_type = $attandances->activity_type;
          $created_by = $attandances->created_by;
          $tracking_location = (!empty($attandances->tracking_location) ? $attandances->tracking_location : '');
          $lat_long_location = (!empty($attandances->lat_long_location) ? $attandances->lat_long_location : '');
          $tracking_date = date('d-m-Y', strtotime($attandances->tracking_time));
          
          
          if(!isset($attendance_array[$tracking_date][$login_id])){
            $attendance_array[$tracking_date][$login_id] = array();
          }
          //else{
            if($activity_type == 'attendance-in'){
              if(!isset($attendance_array[$tracking_date][$login_id]['attendance-in'])){
                $attendance_array[$tracking_date][$login_id]['attendance-in'] = array();
                $attendance_array[$tracking_date][$login_id]['attendance-in'][] = $tracking_time;
                $attendance_array[$tracking_date][$login_id]['attendance-in-location'][] = (!empty($tracking_location) ? $tracking_location : $lat_long_location);
              }
              else{
                $attendance_array[$tracking_date][$login_id]['attendance-in'][] = $tracking_time;
                $attendance_array[$tracking_date][$login_id]['attendance-in-location'][] = (!empty($tracking_location) ? $tracking_location : $lat_long_location);
              }
              $attendance_array[$tracking_date][$login_id]['login_name'][] = $login_name;
              
              if(!in_array($tracking_date, $attendance_dates_array))
								array_push($attendance_dates_array, $tracking_date);
              
            }
            if($activity_type == 'attendance-out'){
              if(!isset($attendance_array[$tracking_date][$login_id]['attendance-out'])){
                $attendance_array[$tracking_date][$login_id]['attendance-out'] = array();
                $attendance_array[$tracking_date][$login_id]['attendance-out'][] = $tracking_time;
                $attendance_array[$tracking_date][$login_id]['attendance-out-location'][] = (!empty($tracking_location) ? $tracking_location : $lat_long_location);
              }
              else{
                $attendance_array[$tracking_date][$login_id]['attendance-out'][] = $tracking_time;
                $attendance_array[$tracking_date][$login_id]['attendance-out-location'][] = (!empty($tracking_location) ? $tracking_location : $lat_long_location);
              }
              $attendance_array[$tracking_date][$login_id]['created_by'][] = $created_by;
            }
          //}
      }
      
      
      $attendancelist = $this->Attendance_model->get_attendance_reg_list($spark_ids, $activitydatestart, $activitydateend, 'approved');
			foreach($attendancelist as $attendances)
			{
				$login_id = $attendances->login_id;
				$login_name = $attendances->name;
				//$tracking_time = $attendances->tracking_time;
				//$activity_type = $attendances->activity_type;
				$created_by = $attendances->created_by;
          
				$tracking_date = date('d-m-Y', strtotime($attendances->attendance_date));
				if(!isset($attendance_reg[$tracking_date][$login_id])){
					$attendance_reg[$tracking_date][$login_id] = array();
				}
				if(!isset($attendance_reg[$tracking_date][$login_id]['attendance-in']))
				{
					$attendance_reg[$tracking_date][$login_id]['attendance-in']= $attendances->in_time;
				}
				if(!isset($attendance_reg[$tracking_date][$login_id]['attendance-out']))
				{
					$attendance_reg[$tracking_date][$login_id]['attendance-out'] = $attendances->out_time;
				}
				$attendance_reg[$tracking_date][$login_id]['login_name'] = $login_name;
				
				if(!in_array($tracking_date, $attendance_dates_array))
					array_push($attendance_dates_array, $tracking_date);
			}			
      //print_r($attendance_array);
      //print_r($attendance_dates_array);
      //die;
    }

    $custom_scripts = ['map.js']; 
    $this->data['time_filter'] = $time_filter; 
    $this->data['custom_scripts'] = $custom_scripts; 
    $this->data['attendance_dates_array'] = $attendance_dates_array; 
    $this->data['attendance_reg'] = $attendance_reg; 
    $this->data['activitydatestart'] = date('d-m-Y',strtotime($activitydatestart)); 
    $this->data['activitydateend'] = date('d-m-Y',strtotime($activitydateend)); 
    $this->data['attendance_array'] = $attendance_array;
    $this->data['state_id'] = $state_id;
    
    $download = (isset($_POST['download']) and $_POST['download'] == 1 ? $_POST['download'] : 0);
    $pdf = (isset($_POST['pdf']) and $_POST['pdf'] == 1 ? $_POST['pdf'] : 0);
    
		if ($download == 1)
    {
      $myFile = "./uploads/reports/attendance_list.xls";
      $this->load->library('parser');
			$this->data['download'] = $download;
      //pass retrieved data into template and return as a string
      $stringData = $this->parser->parse('activity/attendance_list', $this->data, true);
      //open excel and write string into excel
      $fh = fopen($myFile, 'w') or die("can't open file");
      fwrite($fh, $stringData);
      fclose($fh);
      //download excel file
      $this->common_functions->downloadExcel("attendance_list");
      unlink($myFile);
    }
    else if($pdf == 1)
    {
        //pass retrieved data into template and return as a string
        $this->data['pdf'] = 1;
        $summary = $this->load->view('activity/attendance_list',$this->data,true);
        $this->common_functions->create_pdf($summary,'attendance_list');
    }
    else{
      $this->data['partial'] = 'activity/attendance_list';			
      $this->load->view('templates/index', $this->data);
    }
  }
 
  
  public function training_list()
  {
    $spark_id = $this->data['current_user_id'];
    $state_id = '';
    $this->load->Model('states_model');
      
    $training_array = array();
    $attandance_data = array();
    $activitydatestart = date('Y-m-d');
    $activitydateend = date('Y-m-d');
    $this->data['state_list'] = $this->states_model->get_all_states();
    
    $time_filter = '';
    
    if(isset($_POST) && !empty($_POST['submit']))
    {
      $this->load->Model('Users_model');
      
      $time_filter = $_POST['time_filter'];
      
      if($this->data['current_role'] == 'super_admin' || $this->data['current_role'] == 'accounts'){
        $state_id = $_POST['state_id'];
        
        $sparkData = $this->Users_model->getSparkByCurrentState($state_id);
        $spark_ids = array();
        foreach($sparkData as $sparks){
          $spark_ids[] = $sparks->user_id;
        }
      }
      else if($this->data['current_role'] == 'state_person'){
        $state_id = $this->data['user_state_id'];
        $spark_role = array('field_user', 'manager');  
        $users_list = $this->Users_model->getSparkByState($state_id, $spark_role);
        
        foreach ($users_list as $row){
          $spark_ids[] = $row->user_id;
        }
        $spark_ids[] = $spark_id;
      }
      else if($this->data['current_role'] == 'manager'){
        $this->load->Model('team_members_model');
        $team_members_list = $this->team_members_model->get_all_team_members($this->data['current_user_id']);
        foreach($team_members_list as $team_members)
        {
          $spark_ids[] = $team_members->id;
        }
        $spark_ids[] = $spark_id;
      }
      else{
        $spark_ids[] = $spark_id;
      }
      //print_r($spark_ids);
      $activitydatestart = (!empty($_POST['activitydatestart']) ? date('Y-m-d',strtotime($_POST['activitydatestart'])) : date('Y-m-d'));
      $activitydateend = (!empty($_POST['activitydateend']) ? date('Y-m-d',strtotime($_POST['activitydateend'])) : date('Y-m-d'));
      
      $activity_type = array('training', 'training_out');
      
      $this->load->model('Tracking_model');   
      $training_data = $this->Tracking_model->get_track_training_list($spark_ids, $activitydatestart, $activitydateend, $activity_type);
      
      //print_r($training_data);die;
      $activity_ids = array();
      $training_array = array();
      foreach($training_data as $trainings){
          $login_id = $trainings->login_id;
          $login_name = $trainings->name;
          $tracking_time = $trainings->tracking_time;
          $activity_type = $trainings->activity_type;
          $activity_id = $trainings->activity_id;
          $training_duration = $trainings->training_duration;
          $tracking_date = date('d-m-Y', strtotime($trainings->tracking_time));
          
          
          if(!isset($training_array[$tracking_date][$login_id])){
            $training_array[$tracking_date][$login_id] = array();
          }
          //else{
            if($activity_type == 'training'){
              if(!isset($training_array[$tracking_date][$login_id]['training-in'])){
                $training_array[$tracking_date][$login_id]['training-in'] = array();
                $training_array[$tracking_date][$login_id]['training-in'][] = $tracking_time;
              }
              else{
                $training_array[$tracking_date][$login_id]['training-in'][] = $tracking_time;
              }
              $training_array[$tracking_date][$login_id]['login_name'][] = $login_name;
              $training_array[$tracking_date][$login_id]['training_duration'][] = $training_duration;
              $activity_ids[] = $activity_id;
            }
            if($activity_type == 'training_out'){
              if(!isset($training_array[$tracking_date][$login_id]['training-out'])){
                $training_array[$tracking_date][$login_id]['training-out'] = array();
                $training_array[$tracking_date][$login_id]['training-out'][] = $tracking_time;
              }
              else{
                $training_array[$tracking_date][$login_id]['training-out'][] = $tracking_time;
              }
            }
          //}
      }
      //echo implode(', ', $activity_ids);
      //die;
    }

    $custom_scripts = ['map.js']; 
    $this->data['time_filter'] = $time_filter; 
    $this->data['custom_scripts'] = $custom_scripts; 
    $this->data['activitydatestart'] = date('d-m-Y',strtotime($activitydatestart)); 
    $this->data['activitydateend'] = date('d-m-Y',strtotime($activitydateend)); 
    $this->data['training_array'] = $training_array;
    $this->data['state_id'] = $state_id;
    
    $download = (isset($_POST['download']) and $_POST['download'] == 1 ? $_POST['download'] : 0);
    $pdf = (isset($_POST['pdf']) and $_POST['pdf'] == 1 ? $_POST['pdf'] : 0);
    
		if ($download == 1)
    {
      $myFile = "./uploads/reports/training_list.xls";
      $this->load->library('parser');
			$this->data['download'] = $download;
      //pass retrieved data into template and return as a string
      $stringData = $this->parser->parse('activity/training_list', $this->data, true);
      //open excel and write string into excel
      $fh = fopen($myFile, 'w') or die("can't open file");
      fwrite($fh, $stringData);
      fclose($fh);
      //download excel file
      $this->common_functions->downloadExcel("training_list");
      unlink($myFile);
    }
    else if($pdf == 1)
    {
        //pass retrieved data into template and return as a string
        $this->data['pdf'] = 1;
        $summary = $this->load->view('activity/training_list',$this->data,true);
        $this->common_functions->create_pdf($summary,'training_list');
    }
    else{
      $this->data['partial'] = 'activity/training_list';			
      $this->load->view('templates/index', $this->data);
    }
  }
 
 
  public function address_mismatch()
  {
    $this->load->Model('Tracking_model');  
    $this->load->Model('states_model');  
    $spark_id = $this->data['current_user_id'];
    $state_id = '';
    $activity_type = '';
    $activity_data = array();
    $activitydatestart = date('Y-m-d');
    $activitydateend = date('Y-m-d');
    $this->data['state_list'] = $this->states_model->get_all_states();
    
    if(isset($_POST) && !empty($_POST['submit']))
    {
      $activity_type = $_POST['activity_type'];
      
      if($_POST['activitydatestart'] == '' || $_POST['activitydateend'] == '' || $activity_type == '')
      {
        redirect('activity/address_mismatch');
      }
      if($this->data['current_role'] == 'super_admin' || $this->data['current_role'] == 'accounts'){
        $state_id = $_POST['state_id'];
        
        $sparkData = $this->Users_model->getSparkByCurrentState($state_id);
        $spark_ids = array();
        foreach($sparkData as $sparks){
          $spark_ids[] = $sparks->user_id;
        }
      }
      else if($this->data['current_role'] == 'state_person'){
        $state_id = $this->data['user_state_id'];
        $spark_role = array('field_user', 'manager');  
        //$users_list = $this->Users_model->getSparkByState($state_id, $spark_role);
        $users_list = $this->Users_model->getSparkByStateDuration($state_id, $activitydatestart, $activitydateend, '', $spark_role);

        foreach ($users_list as $row){
          //$spark_ids[] = $row->user_id;
          $spark_ids[] = $row['user_id'];
        }
        $spark_ids[] = $spark_id;
        //print_r($spark_ids);
      }
      else if($this->data['current_role'] == 'manager'){
        $this->load->Model('team_members_model');
        $team_members_list = $this->team_members_model->get_all_team_members($this->data['current_user_id']);
        foreach($team_members_list as $team_members)
        {
          $spark_ids[] = $team_members->id;
        }
        $spark_ids[] = $spark_id;
      }
      else{
        $spark_ids[] = $spark_id;
      }
      //print_r($spark_ids);
      $activitydatestart = (!empty($_POST['activitydatestart']) ? date('Y-m-d',strtotime($_POST['activitydatestart'])) : date('Y-m-d'));
      $activitydateend = (!empty($_POST['activitydateend']) ? date('Y-m-d',strtotime($_POST['activitydateend'])) : date('Y-m-d'));
      
      $activity_data = $this->Tracking_model->get_distance("ssc_$activity_type", $spark_ids, $activitydatestart, $activitydateend);
    }
    
    $custom_scripts = ['map.js']; 
    $this->data['custom_scripts'] = $custom_scripts; 
    $this->data['state_id'] = $state_id;			
    $this->data['activity_type'] = $activity_type;			
    $this->data['activitydatestart'] = date('d-m-Y',strtotime($activitydatestart)); 
    $this->data['activitydateend'] = date('d-m-Y',strtotime($activitydateend)); 
    $this->data['activity_data'] = $activity_data;		
    
    $download = (isset($_POST['download']) and $_POST['download'] == 1 ? $_POST['download'] : 0);
    $pdf = (isset($_POST['pdf']) and $_POST['pdf'] == 1 ? $_POST['pdf'] : 0);
    
		if ($download == 1)
    {
      $myFile = "./uploads/reports/address_mismatch.xls";
      $this->load->library('parser');
			$this->data['download'] = $download;
      //pass retrieved data into template and return as a string
      $stringData = $this->parser->parse('activity/address_mismatch', $this->data, true);
      //open excel and write string into excel
      $fh = fopen($myFile, 'w') or die("can't open file");
      fwrite($fh, $stringData);
      fclose($fh);
      //download excel file
      $this->common_functions->downloadExcel("address_mismatch");
      unlink($myFile);
    }
    else if($pdf == 1)
    {
        //pass retrieved data into template and return as a string
        $this->data['pdf'] = 1;
        $summary = $this->load->view('activity/address_mismatch',$this->data,true);
        $this->common_functions->create_pdf($summary,'address_mismatch');
    }
    else{
      $this->data['partial'] = 'activity/address_mismatch';			
      $this->load->view('templates/index', $this->data);
    }
  }
  
  function sparks_data()
  {
      $this->load->Model('states_model');
      $this->load->Model('Users_model');
      $this->load->Model('Dashboard_model');
      $district_id=$this->data['user_district_id'];
      $user_id=$this->data['current_user_id'];
      $state_id=$this->data['user_state_id'];
    
			$month = 0;
			$year = 0;
      $state_data_arr = array();
      $data_arr = array();
      $sparks_name = array();
      $spark_ids = array();
      $field_ids = array();
      $fields = array();
      $field_levels = array();
      $field_level_ids = array();
      $sfield_ids = array();
      $sfields = array();
      $sfield_levels = array();
      $sfield_level_ids = array();
      
      $required_fields = array('SOR','NOR','nationl_rating','state_rating','productivity_rank','training_rank','feedback_rank','mistake_rank','productivity_score','training_score','feedback_score','mistake_score','productivity_rating','training_rating','feedback_rating','mistake_rating','productive_days','unproductive_days','off_days','no_activity_days', 'km_per_visit', 'expense_per_visit', 'total_conveyance_cost', 'spark_color');
      
      $dashboard_fields = $this->Dashboard_model->get_sparks_dashboard_fields();
      foreach($dashboard_fields as $dashboard_field)
      {
        if(in_array($dashboard_field->fields_value, $required_fields)){
          $fields[$dashboard_field->id] = $dashboard_field->fields_value;
          $field_levels[$dashboard_field->id] = $dashboard_field->fields_level;
          $field_level_ids[$dashboard_field->fields_level] = $dashboard_field->id;
          $field_ids[] = $dashboard_field->id;
        }
      }
      //print_r($field_level_ids);
      
      $dashboard_state_fields = $this->Dashboard_model->get_state_dashboard_fields('for_state');
      foreach($dashboard_state_fields as $dashboard_state_field)
      {
        if(in_array($dashboard_state_field->fields_value, $required_fields)){
          $sfields[$dashboard_state_field->id] = $dashboard_state_field->fields_value;
          $sfield_levels[$dashboard_state_field->id] = $dashboard_state_field->fields_level;
          $sfield_level_ids[$dashboard_state_field->fields_level] = $dashboard_state_field->id;
          $sfield_ids[] = $dashboard_state_field->id;
        }
      }
      
      $this->data['sfield_level_ids'] = $sfield_level_ids;
      $this->data['field_level_ids'] = $field_level_ids;
      $this->data['fields'] = $field_levels;
       
      $download = 0 ;    
      $pdf = 0 ;    
      $duration = '';
      $state_id = $this->data['user_state_id'];
      $month = date('m');
      $year = date('Y');
      
      if(isset($_POST['submit']) && $_POST['submit'] == 'Get Report')
      {
        $duration = $_POST['duration'];
        $state_id = $_POST['state_id'];
        $month = $_POST['monthno'];
        $year = $_POST['year'];
      }
        $inpdate			 = '01-'.$month.'-'.$year;

        $this->load->Model('states_model');
        
        $stateData = $this->states_model->get_by_id($state_id);
        if(!empty($stateData))
          $state_name = $stateData[0]->name;
        
        //$current_date = date('Y-m-d');
        $month_year = date('m_Y', strtotime($inpdate));
        $from_date =  date('Y-m-d', strtotime($inpdate));
        $end_date   = date('Y-m-t', strtotime($inpdate));
        
        $sparkData = $this->Dashboard_model->getAllSparkByStateDuration($state_id, $from_date, $end_date, array('field_user'));
        
        $spark_ids = array();
        foreach($sparkData as $sparks)
        {
           $spark_ids[] =  $sparks->user_id;
           $sparks_name[$sparks->user_id] = $sparks->name;
        }
        //print_r($spark_ids);
        
        $state_dashboard_data = $this->Dashboard_model->get_state_dashboard_data($state_id, $sfield_ids, $month_year);
        if(!empty($state_dashboard_data)){
          foreach($state_dashboard_data as $dashboarddata)
          {
            $state_data_arr[$state_name]['month_value'][$dashboarddata->field_id] = $dashboarddata->month_value;
            $state_data_arr[$state_name]['year_value'][$dashboarddata->field_id] = $dashboarddata->year_value;
          }
        }
        
        $dashboard_data = $this->Dashboard_model->get_spark_dashboard_data($state_id, $field_ids, $month_year, $spark_ids);
        if(!empty($dashboard_data)){
          foreach($dashboard_data as $dashboarddata)
          {
            //if(!in_array($dashboarddata->spark_id, $spark_ids))
              //array_push($spark_ids, $dashboarddata->spark_id);
            $data_arr[$dashboarddata->spark_id]['month_value'][$dashboarddata->field_id] = $dashboarddata->month_value;
            $data_arr[$dashboarddata->spark_id]['year_value'][$dashboarddata->field_id] = $dashboarddata->year_value;
          }
        }
        
        /*if(!empty($spark_ids)){
          $spraksData = $this->Users_model->getByIds($spark_ids);
          foreach($spraksData as $spark){
            $sparks_name[$spark->id] = $spark->name;
          }
        }*/
        $this->data['spark_count'] = count($spark_ids);
        
        $download = (isset($_POST['download']) and $_POST['download'] == 1 ? $_POST['download'] : 0);
        $pdf = (isset($_POST['pdf']) and $_POST['pdf'] == 1 ? $_POST['pdf'] : 0);
        
      
      //print_r($data_arr);
      
      $this->data['duration'] = $duration;
      $this->data['state_id'] = $state_id;
      $this->data['state_data_arr'] = $state_data_arr;
      $this->data['data_arr'] = $data_arr;
      $this->data['sparks_name'] = $sparks_name;
      $this->data['year'] = $year;
      $this->data['month'] = $month;
      
      if ($download == 1)
        {
          $myFile = "./uploads/reports/sparks_data.xls";
          $this->load->library('parser');
          $this->data['download'] = $download;
          //pass retrieved data into template and return as a string
          $stringData = $this->parser->parse('activity/sparks_data', $this->data, true);
          //open excel and write string into excel
          $fh = fopen($myFile, 'w') or die("can't open file");
          fwrite($fh, $stringData);
          fclose($fh);
          //download excel file
          $this->common_functions->downloadExcel("sparks_data");
          unlink($myFile);
        }
        else if($pdf == 1)
        {
            //pass retrieved data into template and return as a string
            $this->data['pdf'] = 1;
            $summary = $this->load->view('activity/sparks_data',$this->data,true);
            $this->common_functions->create_pdf($summary,'sparks_data');
        }
      
      if($this->data['current_role']=="state_person" || $this->data['current_role']=="manager"){
        $states = $this->states_model->get_by_id($this->data['user_state_id']);
        $this->data['states'] = $states;
      }
      else{
          $states = $this->states_model->get_all();
          $this->data['states'] = $states;
      }
      $this->data['title'] = 'Sparks Report';
      $this->data['partial'] = 'activity/sparks_data';
      $this->load->view('templates/index', $this->data);
  }
  
  
  public function add_attendance_regularization()
   {
			$this->load->model('Users_model');
			$this->load->model('Attendance_model');
      
      if(!empty($_POST) && $_POST['submit'] == 'Save')
      {
				//$sel_spark = explode('-', $_POST['spark_id']);
				//$spark_id = $sel_spark[0];
				$spark_id = $_POST['spark_id'];
				$attendancedate = date('Y-m-d', strtotime($_POST['attendancedate']));
				$attendance_type = $_POST['attendance_type'];
				$reason = $_POST['reason'];
				
				$check_attendance = $this->Attendance_model->check_attendance($spark_id, $attendancedate);
				
				$already_exist = 0;
				if(!empty($check_attendance))
				{
					$attendance_types = array();	
					foreach($check_attendance as $checkattendance)
					{
							$attendance_types[] = $checkattendance->attendance_type;
					}
					
					if($attendance_type == 'Full Day')
					{
						$already_exist = 1;	
						$error_msg = 'Attendance already marked.';
					}
					else if(($attendance_type == 'First Half' || $attendance_type == 'Second Half') && in_array('Full Day',$attendance_types))
					{
						$already_exist = 1;	
						$error_msg = 'Attendance already marked for full day.';
					}
					else if($attendance_type == 'First Half' && in_array('First Half',$attendance_types))
					{
						$already_exist = 1;	
						$error_msg = 'Attendance already marked for first half.';
					}
					else if($attendance_type == 'Second Half' && in_array('Second Half',$attendance_types))
					{
						$already_exist = 1;	
						$error_msg = 'Attendance already marked for second half.';
					}
				}
				
				if($already_exist == 0)
				{
					if($attendance_type == 'Full Day'){
						$in_time = DAY_START;
						$out_time = DAY_END;
					}
					else if($attendance_type == 'First Half'){
						$in_time = DAY_START;
						$out_time = DAY_MID;
					}
					else if($attendance_type == 'Second Half'){
						$in_time = DAY_MID;
						$out_time = DAY_END;
					}
					$data['spark_id'] = $spark_id;
					$data['attendance_date'] = $attendancedate;
					$data['attendance_type'] = $attendance_type;
					$data['in_time'] = $attendancedate." ".$in_time;
					$data['out_time'] = $attendancedate." ".$out_time;
					$data['created_at'] = date('Y-m-d');
					$data['source'] = 'web';
					$data['status'] = ($this->data['current_role'] != 'accounts' ? 'pending' : 'approved');
					$data['created_by'] = $this->data['current_user_id'];
					$data['reason'] = $reason;
					$this->Attendance_model->insert_regularizations($data);
					$this->session->set_flashdata('success', 'Attendance Mark Successfully.');
					redirect('activity/attendance_regularizations');
				}
				else{
					$this->data['sel_spark_id'] = $spark_id;
					$this->data['current_user_id'] = $spark_id;
					$this->data['attendancedate'] = date('d-m-Y', strtotime($attendancedate));
					$this->data['attendance_type'] = $attendance_type;
					$this->session->set_flashdata('alert', $error_msg);
				}
			}
      
		 $sparks = $this->Users_model->get_all_users('',1);
		 $this->data['sparks'] = $sparks;
		 $this->data['page_title'] = 'Mark Attendance';
		 $this->data['partial'] = 'activity/attendance_mark';
     $this->load->view('templates/index', $this->data);
   }
   
   
   function attendance_regularizations()
	 {
		 $this->load->model('Users_model');
		 $this->load->model('Attendance_model');
		 
		 $this->data['spark_id'] = '';
		 $this->data['attendancestart'] = '';
		 $this->data['attendanceend'] = '';
		 
		 $sparkids = array();
		 
		 if($this->data['current_role'] == 'super_admin' || $this->data['current_role'] == 'HR'){
			$sparks = $this->Users_model->get_all_users();
			//print_r($sparks);
			$this->data['sparks'] = $sparks;
		 }
		 else  if($this->data['current_role'] == 'state_person'){
			$state_id = $this->data['user_state_id'];
			$spark_role = array('field_user', 'manager'); 
			$user_group = array($this->data['current_group']);  
			$users_list = $this->Users_model->getSparkByState($state_id, $spark_role, $user_group);
			//print_r($users_list);
			
			$spark_list = array();
			$i=0;
			foreach ($users_list as $row){
				$sparkids[] = $row->user_id;
				$sparklist['id'] = $row->user_id;
				$sparklist['name'] = $row->name;
				$sparklist['login_id'] = $row->login_id;
				$spark_list[$i] = (object) $sparklist;	
				$i++;
				
			}
			//print_r($spark_list);
			$this->data['sparks'] = $spark_list;
			//$spark_ids[] = $spark_id;
		}
		else if($this->data['current_role'] == 'manager'){
			$this->load->Model('team_members_model');
			$team_members_list = $this->team_members_model->get_all_team_members($this->data['current_user_id']);
			
			//print_r($team_members_list);
			$this->data['sparks'] = $team_members_list;
			foreach($team_members_list as $team_members)
			{
				$sparkids[] = $team_members->id;
			}
			//$spark_ids[] = $spark_id;
		}
		 
		 $attendancelist = array();
		 $spark_ids = array();
		 $sparkNames = array();
		 $spark_id = '';
		 if(!empty($_POST) && $_POST['submit'] == 'Get List')
     {
				if($_POST['spark_id'] != ''){
					$spark_id = $_POST['spark_id'];
					$sparkids = array();
				}					
				
				$attendancestart = ($_POST['attendancestart'] != '' ? date('Y-m-d', strtotime($_POST['attendancestart'])) : '');
				$attendanceend = ($_POST['attendanceend'] != '' ? date('Y-m-d', strtotime($_POST['attendanceend'])) : '');
				$attendancelist = $this->Attendance_model->get_attendance_list($spark_id, $attendancestart, $attendanceend, '', $sparkids);
				foreach($attendancelist as $attendances)
				{
				  if(!in_array($attendances->spark_id, $spark_ids))
						array_push($spark_ids, $attendances->spark_id);	
				}
				if(!empty($spark_ids)){
					 $sparkData = $this->Users_model->getByIds($spark_ids);
					 foreach($sparkData as $sparks)
					 {
						 $sparkNames[$sparks->id] = $sparks->name." (".$sparks->login_id.")";
					 }
				}
				$this->data['spark_id'] = $spark_id;
				$this->data['attendancestart'] = ($attendancestart != '' ? date('d-m-Y', strtotime($attendancestart)) : '');
				$this->data['attendanceend'] = ($attendanceend != '' ? date('d-m-Y', strtotime($attendanceend)) : '');
		 }		 		 
		 
		 $this->data['sparkNames'] = $sparkNames;
		 $this->data['attendancelist'] = $attendancelist;

		 $this->data['page_title'] = 'Attendance List';
		 $this->data['partial'] = 'activity/attendance_mark_list';
     $this->load->view('templates/index', $this->data);
	 } 	
	  	
	
	 function update_regularization()
	 {
			$this->load->model('Users_model');
			$this->load->model('Attendance_model');
      
      if(!empty($_POST))
      {
				$attendance_id = $_POST['attendance_id'];
				$data['status'] = $_POST['status_type'];
				$data['updated_by'] = $this->data['current_user_id'];
				$data['updated_at'] = date('Y-m-d H:i:s');
				if($_POST['status_type'] == 'rejected'){
					$data['rejected_reason'] = $_POST['reason'];
				}
				$this->Attendance_model->updateregularizations($data,$attendance_id);
				
			}
	 }
	 
	 function attendance_calendar()
	 {
			 $inpmonth 	= '';
			 $inpyear 		= '';
			 $posted_login_id 		= '';
		 
				//$spark_id = $_POST['spark_id'];
				$posted_login_id = $this->data['current_user_id'].'-'.$this->data['current_name'].'-'.$this->data['user_state_id'];
					
				if(!empty($_POST) && $_POST['submit'] == 'Get Calendar')
				{
					if($this->input->post('spark_id') != ''){
						$posted_login_id = $this->input->post('spark_id');  
					}
					$inpmonth 	= $this->input->post('start_month');
					$inpyear 		= $this->input->post('start_year');
				}
				else{
					$inpmonth 	= date('m');
					$inpyear 		= date('Y');
				}
				
				$current_year = date('Y');
				$current_month = date('m');
				
				$startdate	= $inpyear.'-'.$inpmonth.'-01';
				$enddate 		= date('Y-m-t', strtotime($inpyear.'-'.$inpmonth.'-01'));
				
				if($inpyear == $current_year && $inpmonth > $current_month)
				{
					$this->session->set_flashdata('alert', 'Can not select future month.');
					redirect('activity/attendance_calendar', 'refresh');
				}
				$this->data['selected_month'] = $startdate;
				
				$session_data = $this->session->userdata('logged_in');
				$login_id   = $session_data['username'];
				$spark_name = $session_data['name'];
				$state_id   = $session_data['state_id'];
				
				$attendance_array = array();
				$attendance_in_array = array();
				$attendance_reg = array();
				$attendance_reg_in = array();
				$attendance_dates_array = array();
				$attendance_befor_array = array();
				$leave_list = array();

				$month_dates = array();
				$leaves_array = array();
				$holiday_dates_array = array();
				$off_dates_array = array();
				$leaves_dates_array = array();
				$dates_arr = array();
				$event_dates_arr = array();

				
				$month_dates = $this->createRange($startdate, $enddate, 'Y-m-d');
				
				$current_group = $this->data['current_group'];
				
				if(!empty($posted_login_id)){
					//Explode data into separte value 
					$final_posted_data = explode("-",$posted_login_id);  
					
					//Check for Empty Login _ID
					if(count($final_posted_data) > 0){
						//Login ID
						$login_id    = $final_posted_data[0]; 
						$spark_name  = $final_posted_data[1];
						$state_id    = $final_posted_data[2];
					}
					
					//Fetch user group
					$sparkData = $this->Users_model->getById($login_id);
					$current_group = $sparkData['user_group'];
				
					$this->load->Model('Tracking_model');
					$this->load->Model('Attendance_model');
					$activity_type = array('attendance-in', 'attendance-out');
					$attandance_data = $this->Tracking_model->get_track_attendance_list(array($login_id), $startdate, $enddate, $activity_type);
					
					foreach($attandance_data as $attandances){
						
							//$login_name = $attandances->name;
							$tracking_time = $attandances->tracking_time;
							$activity_type = $attandances->activity_type;
							$tracking_date = date('d-m-Y', strtotime($attandances->tracking_time));

							if(!isset($attendance_array[$tracking_date])){
								$attendance_array[$tracking_date] = array();
							}

							if($activity_type == 'attendance-in'){
								if(!isset($attendance_array[$tracking_date]['attendance-in'])){
									$attendance_array[$tracking_date]['attendance-in'] = array();
									$attendance_array[$tracking_date]['attendance-in'][] = $tracking_time;
								}
								else{
									$attendance_array[$tracking_date]['attendance-in'][] = $tracking_time;
								}
							}
							if($activity_type == 'attendance-out'){
								if(!isset($attendance_array[$tracking_date]['attendance-out'])){
									$attendance_array[$tracking_date]['attendance-out'] = array();
									$attendance_array[$tracking_date]['attendance-out'][] = $tracking_time;
								}
								else{
									$attendance_array[$tracking_date]['attendance-out'][] = $tracking_time;
								}
							}
					}
					
					
					//attendanc from attendance regulization table
					$attendancelist = $this->Attendance_model->get_attendance_list($login_id, $startdate, $enddate, 'approved');
					foreach($attendancelist as $attendances)
					{
						$tracking_date = date('d-m-Y', strtotime($attendances->attendance_date));
						if(!isset($attendance_reg[$tracking_date])){
							$attendance_reg[$tracking_date] = array();
						}
						if(!isset($attendance_reg[$tracking_date]['attendance-in']))
						{
							$attendance_reg[$tracking_date]['attendance-in'] = array();
							$attendance_reg[$tracking_date]['attendance-in'][]= $attendances->in_time;
						}
						else{
							$attendance_reg[$tracking_date]['attendance-in'][0]= $attendances->in_time;
						}
						if(!isset($attendance_reg[$tracking_date]['attendance-out']))
						{
							$attendance_reg[$tracking_date]['attendance-out'] = array();
							$attendance_reg[$tracking_date]['attendance-out'][] = $attendances->out_time;
						}
						else{
							$attendance_reg[$tracking_date]['attendance-out'][0] = $attendances->out_time;
						}
					}
					
					foreach($attendance_array as $date=>$data_arr) {
						
						if(!key_exists($date, $attendance_reg)) { 
						if(!empty($data_arr['attendance-in'])) { 
							
							//echo "<br>".$date;
							$out_count = (isset($data_arr['attendance-out']) ? count($data_arr['attendance-out']) : 0);
							$in_time = strtotime($data_arr['attendance-in'][0]);
							$out_time = (isset($data_arr['attendance-out'][$out_count-1]) ? strtotime($data_arr['attendance-out'][$out_count-1]) : '');
							
							$time_diff = 0;
							if($out_time != '')
							{
								 $time_diff = ($out_time-$in_time)/(60*60);
							}  
							
							$att_color = "#AEEF9F"; //blue
							$attendance_time = date('H:i', $in_time);
							if($out_time != ''){
								$attendance_time .= '-'.date('H:i', $out_time);
								if(date('H:i', $out_time) < '16'){
									$att_color = "#FFFF8B";	//yellow
									
									$bDate = date('Y-m-d', strtotime($date));
									if(!in_array($bDate, $attendance_befor_array))
										array_push($attendance_befor_array, $bDate); 
								}
							}
							
							$attendance_in_array[$date]['title'] = $attendance_time;
							$attendance_in_array[$date]['color'] = $att_color;
							$attendance_in_array[$date]['url'] = "";
							
							if(!in_array($date, $dates_arr))
							 array_push($dates_arr, $date); 
							
							$newDate = date('Y-m-d', strtotime($date));
							
							if(!in_array($newDate, $attendance_dates_array))
							 array_push($attendance_dates_array, $newDate); 
							
							if(!in_array($newDate, $event_dates_arr))
							 array_push($event_dates_arr, $newDate); 
						}
						}
					}
					
					foreach($attendance_reg as $date=>$data_arr) {
						if(!empty($data_arr['attendance-in'])) { 
							
							$out_count = (isset($data_arr['attendance-out']) ? count($data_arr['attendance-out']) : 0);
							$in_time = strtotime($data_arr['attendance-in'][0]);
							$out_time = (isset($data_arr['attendance-out'][$out_count-1]) ? strtotime($data_arr['attendance-out'][$out_count-1]) : '');
							
							$time_diff = 0;
							if($out_time != '')
							{
								 $time_diff = ($out_time-$in_time)/(60*60);
							}  
							
							$att_color = "#AEEF9F"; //blue
							$attendance_time = date('H:i', $in_time);
							if($out_time != ''){
								$attendance_time .= '-'.date('H:i', $out_time);
								if(date('H:i', $out_time) < '16'){
									$att_color = "#FFFF8B";	//yellow
									
									$bDate = date('Y-m-d', strtotime($date));
									if(!in_array($bDate, $attendance_befor_array))
										array_push($attendance_befor_array, $bDate); 
								}
							}
							
							$attendance_reg_in[$date]['title'] = $attendance_time." (R)";
							$attendance_reg_in[$date]['color'] = $att_color;
							$attendance_reg_in[$date]['url'] = "";
							
							if(!in_array($date, $dates_arr))
							 array_push($dates_arr, $date); 
							
							$newDate = date('Y-m-d', strtotime($date));
							
							if(!in_array($newDate, $attendance_dates_array))
							 array_push($attendance_dates_array, $newDate); 
							
							if(!in_array($newDate, $event_dates_arr))
							 array_push($event_dates_arr, $newDate); 
						}
					}
				
					$leave_data = $this->leaves_model->get_all_leave($login_id, $startdate, $enddate);
					
					$i = 0;
					foreach($leave_data as $leaves)
					{
						$getDates = $this->createRange($leaves->leave_from_date, $leaves->leave_end_date, 'Y-m-d');
						
						foreach($getDates as $getDate)
						{
							$leaves_array[$i]['title'] = $leaves->leave_type;
							$leaves_array[$i]['start_date'] = $getDate;
							//$leaves_array[$i]['end_date'] = $leaves->leave_end_date;
							$leaves_array[$i]['color'] = "#B7D2FF";
							//$leaves_array['url'][$i] = "";
							
							$i++;
							
							if(!in_array($getDate, $leaves_dates_array))
							 array_push($leaves_dates_array, $getDate); 
						}
					}
					
					$this->data['spark_state_id'] = $state_id;
					$this->data['spark_id'] = $login_id;
				}
				else{
					redirect('activity/attendance_calendar');
				}
			
			$this->load->Model('Holidays_model');
			$holidayData = $this->Holidays_model->get_state_holidays_list($state_id, $startdate, $enddate, $current_group);
			$offData = $this->Holidays_model->get_2_3_saturday_list($startdate, $enddate, $current_group);
			
			$i=0;
			$holiday_data = array();
			foreach($holidayData as $holidays)
			{
				$holiday_data[$i]['title'] = $holidays->name;
				$holiday_data[$i]['start_date'] = $holidays->activity_date;
				$holiday_data[$i]['color'] = "#B2FFFF";
				$i++;
				if(!in_array($holidays->activity_date, $holiday_dates_array))
					array_push($holiday_dates_array, $holidays->activity_date); 
			}
			
			$i=0;
			$off_data = array();
			foreach($offData as $offs)
			{
				$off_data[$i]['title'] = $offs->type;
				$off_data[$i]['start_date'] = $offs->activity_date;
				$off_data[$i]['color'] = "#B2F666";
				$i++;
				if(!in_array($offs->activity_date, $off_dates_array))
				 array_push($off_dates_array, $offs->activity_date); 
			}
		  	
		  $date_diff = array_diff($month_dates, $event_dates_arr, $leaves_dates_array);
		  
		  $this->data['holiday_data'] = array_values($holiday_data);
		  $this->data['off_data'] = array_values($off_data);
		  $this->data['holiday_dates_array'] = array_values($holiday_dates_array);
		  $this->data['off_dates_array'] = array_values($off_dates_array);
		  $this->data['start_month'] = $inpmonth;
			$this->data['start_year'] = $inpyear;
		  $this->data['user_login_id'] = $posted_login_id;
		  $this->data['attendance_dates_array'] = $attendance_dates_array;
		  $this->data['attendance_befor_array'] = $attendance_befor_array;
		  $this->data['leaves_array'] = $leaves_array;
			$this->data['attendance_in_array'] = $attendance_in_array;
			$this->data['attendance_reg_in'] = $attendance_reg_in;
			$this->data['month_dates'] = $month_dates;
			$this->data['leaves_dates_array'] = array_values($leaves_dates_array);
			$this->data['date_diff'] = array_values($date_diff);
			$this->data['page_title'] = 'Attendance Calendar';
			$this->data['partial'] = 'activity/attendance_calendar';
			$this->load->view('templates/index', $this->data);
	 }
	 
	  function createRange($start, $end, $format = 'Y-m-d') {
			$start  = new DateTime($start);
			$end    = new DateTime($end);
			$invert = $start > $end;

			$dates = array();
			$dates[] = $start->format($format);
			while ($start != $end) {
					$start->modify(($invert ? '-' : '+') . '1 day');
					$dates[] = $start->format($format);
			}
			return $dates;
		}
		
		
	 public function my_leave_list()	
	 {
		 $this->load->model('Users_model');
		 //$this->load->model('Attendance_model');
		 
		 $leaveslist = array();
		 $creditleavelist = array();
		 $spark_ids = array();
		 $sparkNames = array();
		 $creditleavestart = '';
		 $creditleaveend = '';
		 $spark_id = $this->data['current_user_id'];
		 
		 $inpmonth = date('m');
		 $inpyear = date('Y');
		 $session_start_year = date('Y');
			if($inpmonth < SESSION_START_MONTH)
			{
				$session_start_year = $inpyear-1;
			}
		 
		 $creditleavestart = date("Y-m-d",strtotime($session_start_year."-".SESSION_START_MONTH."-01"));
		 $creditleaveend = date("Y-m-d");
		 
		 /*if(!empty($_POST) && $_POST['submit'] != '')
     {
				$creditleavestart = ($_POST['creditleavestart'] != '' ? date('Y-m-d', strtotime($_POST['creditleavestart'])) : '');
				$creditleaveend = ($_POST['creditleaveend'] != '' ? date('Y-m-d', strtotime($_POST['creditleaveend'])) : '');
		 }*/
		 $leaveslist = $this->leaves_model->get_all($spark_id, $creditleavestart, $creditleaveend,'id desc');
		 
		 $creditleavelist = $this->leaves_model->get_leave_credits($spark_id);
		 $this->data['spark_id'] = $spark_id;	
		 
		 $this->data['creditleavestart'] = $creditleavestart;
		 $this->data['creditleaveend'] = $creditleaveend;
		 $this->data['creditleavelist'] = $creditleavelist;
		 $this->data['leaveslist'] = $leaveslist;

		$download = (isset($_POST['download']) and $_POST['download'] == 1 ? $_POST['download'] : 0);
    $pdf = (isset($_POST['pdf']) and $_POST['pdf'] == 1 ? $_POST['pdf'] : 0);
    
		if ($download == 1)
    {
      $myFile = "./uploads/reports/my_leave_list.xls";
      $this->load->library('parser');
			$this->data['download'] = $download;
      //pass retrieved data into template and return as a string
      $stringData = $this->parser->parse('activity/credit_leave_list', $this->data, true);
      //open excel and write string into excel
      $fh = fopen($myFile, 'w') or die("can't open file");
      fwrite($fh, $stringData);
      fclose($fh);
      //download excel file
      $this->common_functions->downloadExcel("my_leave_list");
      unlink($myFile);
    }
    else if($pdf == 1)
    {
        //pass retrieved data into template and return as a string
        $this->data['pdf'] = 1;
        $summary = $this->load->view('activity/credit_leave_list',$this->data,true);
        $this->common_functions->create_pdf($summary,'my_leave_list');
    }
    else{
			 $this->data['page_title'] = 'Credit Leave List';
			 $this->data['partial'] = 'activity/credit_leave_list';
			 $this->load->view('templates/index', $this->data);
		 }
	 }
  
  function cancel_leave()
  {
		$cur_id = $_REQUEST['cur_id'];
		
		$this->load->model('Leaves_model');
		$leaveData = $this->Leaves_model->getById($cur_id);
		
		$user_id = $leaveData['user_id'];
		$leave_type = $leaveData['leave_type'];
		$total_days = $leaveData['total_days'];
		
		$creditData = $this->Leaves_model->get_leave_credits($user_id, $leave_type);
		if(!empty($creditData)){
			$mdata['leave_taken'] = $creditData[0]->leave_taken-$total_days;  
			$this->Leaves_model->update_leave_credit($mdata, $creditData[0]->id);    
		}
		$this->Leaves_model->delete_spark_leave($cur_id);
	}
	
	
  function block_holidays()
  {
		$this->load->Model('school_holidays_model');
		
		$cur_state = $this->data['user_state_id'];
		
		$inpmonth = date('m');
		$inpyear = date('Y');
		$session_start_year = date('Y');
		if($inpmonth < SESSION_START_MONTH)
		{
			$session_start_year = $inpyear-1;
		}
		$start_date = date("Y-m-d",strtotime($session_start_year."-".SESSION_START_MONTH."-01"));
		$end_date = date("Y-m-d");
		
		$this->data['holidays_list'] = $this->school_holidays_model->get_all_holidays_admin($start_date, $end_date, $cur_state);
    $this->data['partial'] = 'activity/block_holidays';
    $this->data['page_title'] = 'Block Holidays';
    $this->load->view('templates/index', $this->data);
	}
	
	 
  function baithak_summary()
  {
      $this->load->Model('states_model');
      $this->load->Model('Users_model');
      $this->load->Model('Dashboard_model');
      $district_id=$this->data['user_district_id'];
      $user_id=$this->data['current_user_id'];
      $state_id=$this->data['user_state_id'];
    
			$month = 0;
			$year = 0;
      $state_data_arr = array();
      $data_arr = array();
      $sparks_name = array();
      $spark_ids = array();
      $field_ids = array();
      $fields = array();
      $field_levels = array();
      $field_level_ids = array();
      $sfield_ids = array();
      $sfields = array();
      $sfield_levels = array();
      $sfield_level_ids = array();
      
      $required_fields = array('school_registered','registered_teacher','avg_teacher_engagement','spark_engagement','baithak_rank','baithak_rating','baithak_state_rank','baithak_state_rating');
      
      $dashboard_fields = $this->Dashboard_model->get_baithak_fields();
      foreach($dashboard_fields as $dashboard_field)
      {
        if(in_array($dashboard_field->field_name, $required_fields)){
          $fields[$dashboard_field->id] = $dashboard_field->field_name;
          $field_levels[$dashboard_field->id] = $dashboard_field->field_name;
          $field_level_ids[$dashboard_field->field_name] = $dashboard_field->id;
          $field_ids[] = $dashboard_field->id;
        }
      }
            
      $this->data['sfield_level_ids'] = $sfield_level_ids;
      $this->data['field_level_ids'] = $field_level_ids;
      $this->data['fields'] = $field_levels;
       
      $download = 0 ;    
      $pdf = 0 ;    
      $duration = '';
      $state_id = $this->data['user_state_id'];
      $month = date('m');
      $year = date('Y');
      
      if(isset($_POST['submit']) && $_POST['submit'] == 'Get Report')
      {
        $duration = $_POST['duration'];
        $state_id = $_POST['state_id'];
        $month = $_POST['monthno'];
        $year = $_POST['year'];
      }
			$inpdate			 = '01-'.$month.'-'.$year;

			$this->load->Model('states_model');
			
			$stateData = $this->states_model->get_by_id($state_id);
			if(!empty($stateData))
				$state_name = $stateData[0]->name;
			
			//$current_date = date('Y-m-d');
			$month_year = date('m_Y', strtotime($inpdate));
			$from_date =  date('Y-m-d', strtotime($inpdate));
			$end_date   = date('Y-m-t', strtotime($inpdate));
			
			$sparkData = $this->Dashboard_model->getAllSparkByStateDuration($state_id, $from_date, $end_date, array('field_user'));
			
			$spark_ids = array();
			foreach($sparkData as $sparks)
			{
				 $spark_ids[] =  $sparks->user_id;
				 $sparks_name[$sparks->user_id] = $sparks->name;
			}
			
			$dashboard_data = $this->Dashboard_model->get_baithak_fields_data($state_id, $field_ids, $month_year, $spark_ids);
			if(!empty($dashboard_data)){
				foreach($dashboard_data as $dashboarddata)
				{
					$data_arr[$dashboarddata->spark_id]['month_value'][$dashboarddata->field_id] = $dashboarddata->month_value;
					$data_arr[$dashboarddata->spark_id]['year_value'][$dashboarddata->field_id] = $dashboarddata->year_value;
				}
			}
      //print_r($data_arr);
        
			$this->data['spark_count'] = count($spark_ids);
			
			$download = (isset($_POST['download']) and $_POST['download'] == 1 ? $_POST['download'] : 0);
			$pdf = (isset($_POST['pdf']) and $_POST['pdf'] == 1 ? $_POST['pdf'] : 0);
			
      
      $this->data['duration'] = $duration;
      $this->data['state_id'] = $state_id;
      $this->data['state_data_arr'] = $state_data_arr;
      $this->data['data_arr'] = $data_arr;
      $this->data['sparks_name'] = $sparks_name;
      $this->data['year'] = $year;
      $this->data['month'] = $month;
      
      if ($download == 1)
        {
          $myFile = "./uploads/reports/baithak_data.xls";
          $this->load->library('parser');
          $this->data['download'] = $download;
          //pass retrieved data into template and return as a string
          $stringData = $this->parser->parse('activity/baithak_data', $this->data, true);
          //open excel and write string into excel
          $fh = fopen($myFile, 'w') or die("can't open file");
          fwrite($fh, $stringData);
          fclose($fh);
          //download excel file
          $this->common_functions->downloadExcel("baithak_data");
          unlink($myFile);
        }
        else if($pdf == 1)
        {
            //pass retrieved data into template and return as a string
            $this->data['pdf'] = 1;
            $summary = $this->load->view('activity/baithak_data',$this->data,true);
            $this->common_functions->create_pdf($summary,'baithak_data');
        }
      
      if($this->data['current_role']=="state_person" || $this->data['current_role']=="manager"){
        $states = $this->states_model->get_by_id($this->data['user_state_id']);
        $this->data['states'] = $states;
      }
      else{
          $states = $this->states_model->get_all();
          $this->data['states'] = $states;
      }
      $this->data['title'] = 'Biathak Summary Report';
      $this->data['partial'] = 'activity/baithak_data';
      $this->load->view('templates/index', $this->data);
  }
  
   
  function change_password()
  {
		if(isset($_POST['Submit']) && $_POST['Submit'] == 'Save')
    {
			$this->load->Model('users_model');
			$spark_id = $this->data['current_user_id'];
			$new_pwd = $this->input->post('new_pwd');
			
			if($spark_id != '' && $new_pwd != '')
			{
				$data['password'] = md5($new_pwd);
				$this->users_model->edit($data, $spark_id);
				$this->session->set_flashdata('success', 'Password updated successfully.');
				redirect('activity/change_password', 'refresh');
			}
		}
		$this->data['partial'] = 'activity/change_password';
		$this->data['page_title'] = 'Change Password';
		$this->load->view('templates/index', $this->data);	
	} 
	
		
  public function notification_list()
  {
    $spark_id = $this->data['current_user_id'];
    $sel_spark_id = $this->data['current_user_id'];
    $state_id = 0;
    $spark_state_id = 0;
    if (isset($_GET['spark_id']) and $_GET['spark_id'] > 0)
    {
      $spark_state_id = (isset($_GET['spark_state_id']) ? $_GET['spark_state_id'] : 0);
      $spark_detail = $_GET['spark_id'];
      $spark_details = explode("-",$spark_detail);
      $spark_id = $spark_details[0];
      $sel_spark_id = $spark_id;
    }
    if($this->data['current_role'] == 'state_person' || $this->data['current_role'] == 'manager'){
      $spark_state_id = $this->data['user_state_id'];
    }
    
    $this->load->Model('states_model');
    $this->load->Model('districts_model');
    $this->load->Model('tracking_model');
    $this->load->Model('Users_model');
    
    $spark_data = $this->Users_model->getById($spark_id);
    $spark_name = $spark_data['name'];
    
    $claim_data = array();
    $activitydatestart = date('Y-m-d', strtotime('-15 days'));
    $activitydateend = date('Y-m-d');
    
    $user_group = array('field_user');  
    
    if($this->data['current_group'] == 'head_office')
    {
			//$state = 0;
			$spark_role = array('manager', 'accounts', 'field_user');
			$user_group = array('head_office');  
		}
		
		$spark_ids = array();
		
    if($this->data['current_role'] == 'super_admin' || $this->data['current_role'] == 'accounts' || $this->data['current_role'] == 'HR'){
			$state_id = $_POST['state_id'];
			$sparkData = $this->Users_model->getSparkByCurrentState($state_id);
			$spark_ids = array();
			foreach($sparkData as $sparks){
				$spark_ids[] = $sparks->user_id;
			}
		}
		else if($this->data['current_role'] == 'state_person'){
		 $state_id = $this->data['user_state_id'];
        $spark_role = array('field_user', 'manager');  
        $users_list = $this->Users_model->getSparkByState($state_id, $spark_role);
        
        foreach ($users_list as $row){
          $spark_ids[] = $row->user_id;
        }
        $spark_ids[] = $spark_id;
    }
		else if($this->data['current_role'] == 'manager'){
			$this->load->Model('team_members_model');
			$team_members_list = $this->team_members_model->get_all_team_members($this->data['current_user_id']);
			foreach($team_members_list as $team_members)
			{
				$spark_ids[] = $team_members->id;
			}
			$spark_ids[] = $spark_id;
		}
		else{
				//redirect('dashboard');
			$spark_ids[] = $spark_id;
		}
		
		$status = 'approved';
		if(isset($_GET) && !empty($_GET['activitydatestart']) && !empty($_GET['activitydateend']))
    {
			$activitydatestart = (!empty($_GET['activitydatestart']) ? date('Y-m-d',strtotime($_GET['activitydatestart'])) : $activitydatestart);
      //$activitydateend = (!empty($_GET['activitydateend']) ? date('Y-m-d',strtotime($_GET['activitydateend'])) : $activitydateend);
      $activitydateend = date('Y-m-d', strtotime('+15 days', strtotime($activitydatestart)));;
      
      if(isset($_GET['spark_id']) && $_GET['spark_id'] != ''){
				$spark_ids = array();
				$spark_ids[] = $_GET['spark_id'];
			}
      if($_GET['status'] != ''){
				$status = $_GET['status'];
			}
    }    
    
		$local_claim_data = $this->tracking_model->get_notification_claims_list('result', $spark_ids, $activitydatestart, $activitydateend, $status);
    $local_claim_count = count($local_claim_data);
    
    $ld_claim_data = $this->tracking_model->get_longdistance_notification('result', $spark_ids, $activitydatestart, $activitydateend, $status);
    $ld_claim_count = count($ld_claim_data);
    
    $stay_claim_data = $this->tracking_model->get_stay_notification('result', $spark_ids, $activitydatestart, $activitydateend, $status);
    $stay_claim_count = count($stay_claim_data);
    
    foreach($stay_claim_data as $claims)
    {
			$stay_with = json_decode($claims->paid_spark_id);
			if(!empty($stay_with))
			{
				foreach($stay_with as $sparkid){
					if(!in_array($sparkid, $spark_ids))
						array_push($spark_ids, $sparkid);
				}
			}
		}
		
		$food_claim_data = $this->tracking_model->get_food_notification('result', $spark_ids, $activitydatestart, $activitydateend, $status);
		$food_claim_count = count($food_claim_data);
		
    foreach($food_claim_data as $claims)
    {
			$stay_with = json_decode($claims->paid_spark_id);
			if(!empty($stay_with))
			{
				foreach($stay_with as $sparkid){
					if(!in_array($sparkid, $spark_ids))
						array_push($spark_ids, $sparkid);
				}
			}
		}
		
		$other_claim_data = $this->tracking_model->get_other_claims_notification('result', $spark_ids, $activitydatestart, $activitydateend, $status);
		$other_claim_count = count($other_claim_data);
		
		$activities = array();
		$date = $activitydatestart;
		$i = 1;
		
		while($date <= $activitydateend)
		{
			$activities[$date] = array();
			$date = date('Y-m-d', strtotime('+'.$i.' day', strtotime($activitydatestart)));
			$i++;
		}
		
		
		//$state_ids = array();
    //$district_ids = array();
    //$user_ids = array();

		$leaves = $this->tracking_model->get_leave_notifications('result', $spark_ids, $activitydatestart, $activitydateend, $status);   
		
		$activity = array();
		$a = 0;
		if(!empty($leaves)){
			foreach($leaves as $key=>$leave) { 

				$activity[$a]['activity_type'] = 'Leave';
				$activity[$a]['type'] = 'leaves';
				$activity[$a]['leave_from_date'] = $leave->leave_from_date;
				$activity[$a]['user_id'] = $leave->user_id;
				$activity[$a]['state'] = $leave->state_id;
				$activity[$a]['id'] = $leave->id;
				$activity[$a]['district'] = $leave->district_id;
				$activity[$a]['status'] = $leave->status;
				$activity[$a]['block'] = 0;
				$activity[$a]['cluster'] = 0;
				$activity[$a]['school'] = 0;
				$activity[$a]['details'] = '<strong>Leave Type : </strong>'.$leave->leave_type.'<br><strong>Leave From : </strong>'.date("d-m-Y",strtotime($leave->leave_from_date)).'<br><strong>Leave Till : </strong>'.date("d-m-Y",strtotime($leave->leave_end_date)).'<br><strong>No Of Days : </strong>'.$leave->total_days;

				//array_push($activities[date("Y-m-d",strtotime($leave->leave_from_date))],$activity);      
				//array_push($state_ids,$leave->state_id);
				//array_push($district_ids,$leave->district_id);
				//array_push($user_ids,$leave->user_id);
				$a++;
			}
		}
    
    $spark_with_names = array();
		if(!empty($spark_ids))
		{
			$spark_data = $this->Users_model->getByIds($spark_ids);
			foreach($spark_data as $sparkdata)
			{
				$spark_with_names[$sparkdata->id] = $sparkdata->name;	 
			}
		}
		
		/*$state_names = array();
		$state_names[0] = '';
		if (count($state_ids) > 0)
		{
			$states = $this->states_model->get_by_ids($state_ids);
			foreach($states as $state)
			{
				$state_names[$state->id] = $state->name;
			}
		}

		$district_names = array();
		$district_names[0] = '';
		if (count($district_ids) > 0)
		{
			$districts = $this->districts_model->getByIds($district_ids);
			foreach($districts as $district)
			{
				$district_names[$district->id] = $district->name;
			}
		}*/
      
		  //echo "<br>".$this->Leaves_model->defaultdb->last_query();
		
		$this->load->model('travels_model');   
    $travels_mode = $this->travels_model->get_all();
    
    $custom_scripts = ['map.js']; 
    $this->data['activitydatestart'] = date('d-m-Y',strtotime($activitydatestart)); 
    $this->data['activitydateend'] = date('d-m-Y',strtotime($activitydateend)); 
    $this->data['custom_scripts'] = $custom_scripts; 
    $this->data['stay_claim_data'] = $stay_claim_data;
    $this->data['sel_spark_id'] = $sel_spark_id;
    $this->data['spark_id'] = $spark_id;
    $this->data['spark_name'] = $spark_name;
    $this->data['spark_with_names'] = $spark_with_names;
    $this->data['spark_state_id'] = $spark_state_id;
    
    $this->data['ld_claim_data'] = $ld_claim_data;
    $this->data['travels_mode'] = $travels_mode;
    
    $this->data['food_claim_data'] = $food_claim_data;
    
    $this->data['other_claim_data'] = $other_claim_data;
    $this->data['local_claim_data'] = $local_claim_data;
    $this->data['activities'] = $activity;
    $this->data['status'] = $status;
    
    $this->data['count_claim_data'] = $local_claim_count + $ld_claim_count + $stay_claim_count + $food_claim_count + $other_claim_count;
    
    $download = (isset($_POST['download']) and $_POST['download'] == 1 ? $_POST['download'] : 0);
    $pdf = (isset($_POST['pdf']) and $_POST['pdf'] == 1 ? $_POST['pdf'] : 0);
    
		if ($download == 1)
    {
      $myFile = "./uploads/reports/notification_list.xls";
      $this->load->library('parser');
			$this->data['download'] = $download;
      //pass retrieved data into template and return as a string
      $stringData = $this->parser->parse('activity/notification_list', $this->data, true);
      //open excel and write string into excel
      $fh = fopen($myFile, 'w') or die("can't open file");
      fwrite($fh, $stringData);
      fclose($fh);
      //download excel file
      $this->common_functions->downloadExcel("notification_list");
      unlink($myFile);
    }
    else if($pdf == 1)
    {
        //pass retrieved data into template and return as a string
        $this->data['pdf'] = 1;
        $summary = $this->load->view('activity/notification_list',$this->data,true);
        $this->common_functions->create_pdf($summary,'notification_list');
    }
    else{
      $this->data['partial'] = 'activity/notification_list';			
      $this->load->view('templates/index', $this->data);
    }
  }
  
	
	
}
