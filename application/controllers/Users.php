<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends CI_Controller {

	function __construct()	
	{
		parent::__construct();
		$this->load->helper('url');
		$this->data['current_page'] = 'users';
		$this->load->library('form_validation');
		$this->load->library('common_functions');
		$this->load->model('users_model');
		$this->load->helper('date');
    
    if($this->session->userdata('logged_in'))
		{
      $session_data = $this->session->userdata('logged_in');
      $this->data['current_user_id'] = $session_data['id'];
      $this->data['current_username'] = $session_data['username'];
      $this->data['current_role'] = $session_data['role'];
      $this->data['user_state_id'] = $session_data['state_id'];
      $this->data['user_district_id'] = $session_data['district_id'];
      
      if ($this->data['current_role'] == "field_user")
      {
        //If no session, redirect to login page
        $this->session->set_flashdata('alert', "You are not authorized to access users section");
        redirect('dashboard', 'refresh');
      }
    }
    else
    {
      //If no session, redirect to login page
      redirect('login', 'refresh');
    }
	}

	public function index()
	{
    if ($this->data['current_role'] == "super_admin") 
    {
      $this->data['users_list'] = $this->users_model->get_all_users($this->data['id']);
      $this->data['partial'] = 'users/index';
      $this->data['page_title'] = 'Users';
      $this->load->view('templates/index', $this->data);
    }
    else
    {
      //If no session, redirect to login page
      redirect('dashboard', 'refresh');
    }
	}

	public function add()
	{
    if ($this->data['current_role'] == "super_admin") 
    {
      $this->data['partial'] = 'users/form';
      $this->data['action'] = 'create';
      $this->data['page_title'] = 'Add User';
      $this->load->model('states_model');
      $this->data['states'] = $this->states_model->get_all_states();
      $this->load->view('templates/index', $this->data);
    }
    else
    {
      //If no session, redirect to login page
      redirect('dashboard', 'refresh');
    }
	}
	
	public function create()
	{
    if ($this->data['current_role'] == "super_admin") 
    {
      $mdata['login_id'] = $this->input->post('login_id');
      $mdata['role'] = $this->input->post('role');
      $mdata['password'] = md5($this->input->post('password'));
      $mdata['district_id'] = $this->input->post('district_id');
      $mdata['state_id'] = $this->input->post('state_id');
      
      $this->users_model->add($mdata);
          
      header('location:'.base_url()."users/");
    }
    else
    {
      //If no session, redirect to login page
      redirect('dashboard', 'refresh');
    }
	}

	public function edit()
	{
    if ($this->data['current_role'] == "super_admin") 
    {
      $id = $this->uri->segment(3);
      $this->data['action'] = 'update';
      $this->data['user'] = $this->users_model->getById($id);
      $this->load->model('states_model');
      $this->data['states'] = $this->states_model->get_all_states();
      $this->data['page_title'] = 'Edit User';
      $this->data['partial'] = 'users/form';
      $this->load->view('templates/index', $this->data);
    }
    else
    {
      //If no session, redirect to login page
      redirect('dashboard', 'refresh');
    }
	}
  
	public function update()
	{
    if ($this->data['current_role'] == "super_admin") 
    {
      $mdata['login_id'] = $this->input->post('login_id');
      $mdata['role'] = $this->input->post('role');
      $password = $this->input->post('password');
      $mdata['district_id'] = $this->input->post('district_id');
      $mdata['state_id'] = $this->input->post('state_id');
      if ($password != "")
      {
        $mdata['password'] = md5($this->input->post('password'));
      }
      $this->users_model->update_info($mdata, $_POST['id']);
      //code by gagan start
      header('location:'.base_url()."users/".$this->index());
    }
    else
    {
      //If no session, redirect to login page
      redirect('dashboard', 'refresh');
    }
	}
				  
	public function delete($id)
	{
    if ($this->data['current_role'] == "super_admin") 
    {
      $this->users_model->delete_a_user($id);
      header('location:'.base_url()."users".$this->index());
    }
    else
    {
      //If no session, redirect to login page
      redirect('dashboard', 'refresh');
    }
	}
}
