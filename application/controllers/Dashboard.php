<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
error_reporting(0);
    ini_set('display_errors', 'Off');
class Dashboard extends CI_Controller {	 
  
  function __construct()  {    
    parent::__construct();
    $this->output->enable_profiler(FALSE);    
    $this->load->helper('url');    
    $this->load->model('users_model'); 
    $this->load->library('common_functions');
	
    if($this->session->userdata('logged_in'))
		{
      $session_data = $this->session->userdata('logged_in');
      $this->data['current_user_id'] = $session_data['id'];
      $this->data['current_username'] = $session_data['username'];
      $this->data['current_name'] = $session_data['name'];
      $this->data['current_role'] = $session_data['role'];
      $this->data['user_state_id'] = $session_data['state_id'];
      $this->data['user_district_id'] = $session_data['district_id'];
      $this->data['login_id'] = $session_data['login_id'];
      $this->data['current_group'] = $session_data['user_group'];
    }
    else
    {
      // echo "here";exit;
      //If no session, redirect to login page
      redirect('login', 'refresh');
    }
    $current_date=date('d');
    $lstedate=$current_date-4;
    $todaydate = Array();
    for($i=$current_date; $i>=$lstedate; $i--){
      //echo $i;
       array_push($todaydate,$i);
    }
    $this->data['SSS_APP_TEACHER_POINTS'] = unserialize(SSS_APP_TEACHER_POINTS);
    $this->data['SSS_APP_POINTS'] = unserialize(SSS_APP_POINTS);
    $this->data['EMP_ROLES'] = unserialize(EMP_ROLES);
    $this->data['month_arr'] = unserialize(MONTH_ARRAY);
    $this->data['year_arr'] = unserialize(YEAR_ARRAY);
    $this->data['todaydate'] = $todaydate;
  }		

  public function index()		
  {	
    //$start_date = '2018-04-01 00:00:00';
    //$end_date = '2019-03-31 23:59:59';
    $this->load->model('Team_members_model');
    $this->load->model('Leaves_model');
    $this->load->model('Holidays_model');
    $this->load->model('Dashboard_model');

    
    $login_id = $this->data['current_user_id'];
    $state_id = $this->data['user_state_id'];
    $user_ids = array($login_id);
    
    $heading = '';
    
    if($this->data['current_role'] == 'manager')
    {   
        $userData = $this->Team_members_model->get_team($login_id);
        if(count($userData) > 0){
          foreach($userData as $users){
            if(!in_array($users->id,$user_ids)){
              $user_ids[] = $users->id;
            }
          }
        }
        $heading = 'Team';
    }
    else if($this->data['current_role'] == 'state_person')
    {   
      $heading = 'Team';
    }
    else if($this->data['current_role'] == 'super_admin' || $this->data['current_role'] == 'admin' || $this->data['current_role'] == 'accounts'){
      $user_ids = array();
      $heading = 'All';
    }
    $this->data['heading'] = $heading;
    
    if(date('d') < 10){
      $current_date = date('Y-m-d', strtotime('last day of last month'));
    }else{
      $current_date = date('Y-m-d');
    }
    
    if(!empty($_POST['start_month'])){
      $inpmonth = $_POST['start_month'];
    }
    else{
      $inpmonth = (Int) date("m", strtotime($current_date));
    }
    
    if(!empty($_POST['start_year'])){
      $inpyear =  $_POST['start_year'];
    }
    else{
      $inpyear = (Int) date("Y", strtotime($current_date));  
    }
    
    $dates = $this->get_year_month($inpmonth, $inpyear);
    
    $month_year = sprintf('%02d', $inpmonth)."_".$inpyear;
    
    $current_role = trim($this->data['current_role']);
    $this->data['sel_spark_role'] = $current_role;
    $sel_spark = '';
    $created_at = '';  

    $start_date = date('Y-m-01 00:00:00', strtotime($dates['from_date_cur'])); // hard-coded '01' for first day
    $end_date  = date('Y-m-t 23:59:59', strtotime($dates['to_date_cur']));
    
    //$start_date = date('Y-01-m 00:00:00'); // hard-coded '01' for first day
    //$end_date  = date('Y-m-t 23:59:59');
    
    $leave_count = 0;  
    $leaves_arr = $this->Leaves_model->get_all_leave($login_id, $start_date, $end_date);
    if(!empty($leaves_arr)){
      $leaves_data = $this->common_functions->return_month_leave($leaves_arr, $start_date, $end_date);
      $leave_count = count($leaves_data);  
    }
      
    $this->data['month_year'] = $month_year;
    $this->data['sel_spark'] = $login_id;
    $this->data['month'] = $inpmonth;
    $this->data['year'] = $inpyear;
    $this->data['admins_list'] = $this->users_model->get_all_users();		   
    $custom_scripts = ['dashboard.js']; 
    $current_role = $current_role;     
    $this->data['custom_scripts'] = $custom_scripts;         
    $this->data['partial'] = 'dashboard';			    			
    $this->data['page_title'] = 'Dashboard';			
    $this->load->view('templates/dashboard', $this->data);
  }
  
  function get_year_month($inpmonth='', $inpyear='')
  {
    $inpmonth = ($inpmonth == '' ? (Int) date("m", strtotime(date("Y-m-d"))) : $inpmonth);
    $inpyear = ($inpyear == '' ? (Int) date("Y", strtotime(date("Y-m-d"))) : $inpyear);
    $month_name = "";
    $prev_month2_name   = "";
    $prev_month1_name   = "";
    $cm_no 		= 0;
    $pv2_no 	= 0;
    $pv1_no 	= 0;
    $current_year2 = 0;
    $current_year1 = 0;

    switch($inpmonth){

    case "1":
      $month_name = date('F', mktime(0, 0, 0, $inpmonth, 10));
      $cm_no      = $inpmonth;
      $prev_month2_name   = date('F', mktime(0, 0, 0, 12, 10));
      $pv2_no = 12;
      $current_year2 = $inpyear - 1;
      $prev_month1_name   = date('F', mktime(0, 0, 0, 12 - $inpmonth, 10));
      $pv1_no = 12 - $inpmonth;
      $current_year1 = $inpyear - 1;
      break;
    case "2":
      $month_name = date('F', mktime(0, 0, 0, $inpmonth, 10));
      $cm_no      = $inpmonth;
      $prev_month2_name   = date('F', mktime(0, 0, 0, $inpmonth-1, 10));
      $pv2_no = $inpmonth-1;
      $current_year2 = $inpyear;
      $prev_month1_name   = date('F', mktime(0, 0, 0, 12, 10));
      $pv1_no = 12;
      $current_year1 = $inpyear - 1;
      break;
    default:
      $month_name   = date('F', mktime(0, 0, 0, $inpmonth, 10));
      $cm_no      = $inpmonth;
      $prev_month2_name   = date('F', mktime(0, 0, 0, $inpmonth-1, 10));
      $pv2_no = $inpmonth-1;
      $current_year2 = $inpyear;
      $prev_month1_name   = date('F', mktime(0, 0, 0, $inpmonth-2, 10));
      $pv1_no = $inpmonth-2;
      $current_year1 = $inpyear;
    }

    $session_start_year = $inpyear;
    if($inpmonth < SESSION_START_MONTH)
    {
      $session_start_year = $inpyear-1;
    }

    $_fromDate1 = date("Y-m-d",strtotime($current_year1."-".$pv1_no."-01"));
    $_toDate1 = date("Y-m-t",strtotime($current_year1."-".$pv1_no."-01"));
    $productive_days_1 = date("t",strtotime($current_year1."-".$pv1_no."-01"));
    //echo"<br>".$_fromDate1." >> ".$_toDate1." >> ".$productive_days_1;
    
    $_fromDate2 = date("Y-m-d",strtotime($current_year2."-".$pv2_no."-01"));
    $_toDate2 = date("Y-m-t",strtotime($current_year2."-".$pv2_no."-01"));
    $productive_days_2 = date("t",strtotime($current_year2."-".$pv2_no."-01"));
    
    $_fromDate_C = date("Y-m-d",strtotime($inpyear."-".$inpmonth."-01"));
    $_toDate_C = date("Y-m-t",strtotime($inpyear."-".$inpmonth."-01"));
    $productive_days_3 = date("t",strtotime($inpyear."-".$inpmonth."-01"));
    
    $data = array();
    
    $data['from_date_cur'] = $_fromDate_C;
    $data['to_date_cur'] = $_toDate_C;
    $data['productive_days_cur'] = $productive_days_3;
    
    $data['from_date_1'] = $_fromDate1;
    $data['to_date_1'] = $_toDate1;
    $data['productive_days_1'] = $productive_days_1;
    
    $data['from_date_2'] = $_fromDate2;
    $data['to_date_2'] = $_toDate2;
    $data['productive_days_2'] = $productive_days_2;
    
    return $data;
  }
  
	public function activity()		
  {			
    $this->data['admins_list'] = $this->users_model->get_all_users();		   
    // $this->load->view('users/index', $data);			 
    $this->data['partial'] = 'activity';			
    $this->load->view('templates/index', $this->data);
  }			

  public function logout()			
  {			   
    $data['sso_token'] = "";
    $this->users_model->update_info($data, $this->data['current_user_id']);
    $this->session->unset_userdata('logged_in');			   
    redirect('login', 'refresh');			
  }
  
  function select_activity()
  {
    $activity_date = $this->session->userdata('activity_date');
    if ($activity_date == "")
    {
      $this->session->set_flashdata('alert', 'Select date first');
      redirect('dashboard', 'refresh');
    }
    else
    {
      $this->data['activity_date'] = $activity_date;
    }

    // $this->load->view('users/index', $data);			 
    $this->data['activity_date'] = $activity_date;			 
    $this->data['partial'] = 'select_activity';			
    $this->load->view('templates/index', $this->data);
  }
  
  function select_date()
  {
		
    $this->load->model("holidays_model");
    $user_id = $this->data['id'];
		$activity_date = explode("-",$_GET['activity_date']);
		$activity_dat = $activity_date[2]."-".$activity_date[1]."-".$activity_date[0];
    $start_date = $activity_dat;
    $holidays = $this->holidays_model->get_all($user_id, $start_date, $start_date);
    if (count($holidays) == 0)
    {
      // $this->load->view('users/index', $data);			 
      $this->session->unset_userdata('school_visit');	
      $this->session->set_userdata('activity_date', $activity_dat);
      $response['response'] = "done";
    }
    else
    {
      $response['response'] = "Holiday marked for this date.";  
    }
    redirect('select_activity');
  }

  function activities()
  {
    $this->load->model("holidays_model");
    $this->load->model("school_visits_model");
    $this->load->model("meetings_model");
    $this->load->model("trainings_model");
    $user_id = $this->data['id'];
    $start_date = $_POST['start'];
    $end_date = $_POST['end'];
    
    $activities = array();
    
    $holidays = $this->holidays_model->get_all($user_id, $start_date, $end_date);
    foreach($holidays as $holiday){
      $activity = array("title"=>ucwords($holiday->holiday_type)." Holiday", "start"=>$holiday->activity_date, "activity_type" => "holiday", "className" => "holiday");
      array_push($activities, $activity);
    }
    
    $trainings = $this->trainings_model->get_all($user_id, $start_date, $end_date);
    foreach($trainings as $training){
      if ($training->training_type == "internal")
        $activity = array("title"=>ucwords($training->training_type)." Training", "start"=>$training->activity_date, "activity_type" => $training->training_type."-training", "className" => "training");
      else
        $activity = array("title"=>ucwords($training->training_type)." Training", "start"=>$training->activity_date, "activity_type" => $training->training_type."-training", "className" => "training", "url"=>base_url()."training/show/".$training->id);
      
      array_push($activities, $activity);
    }
    
    $meetings = $this->meetings_model->get_all($user_id, $start_date, $end_date);
    foreach($meetings as $meeting){
      $activity = array("title"=>ucwords($meeting->meeting_type)." Meeting", "start"=>$meeting->activity_date, "activity_type" => $meeting->meeting_type."-meeting", "className" => "meeting", "url"=>base_url()."meeting/show/".$meeting->id);
      array_push($activities, $activity);
    }
    
    $school_visits = $this->school_visits_model->get_all($user_id, $start_date, $end_date);
    foreach($school_visits as $school_visit){
      $activity = array("title"=>"School Visit", "start"=>$school_visit->activity_date, "activity_type" => "school_visit", "className" => "school_visit", "url"=>base_url()."school_visit/show/".$school_visit->id);
      array_push($activities, $activity);
    }
    echo json_encode($activities);
  }
  
  function get_districts()
  {
    $this->load->model('districts_model');  
    $this->load->model('schools_model');  
    $state = $_GET['state'];
    $districts = $this->districts_model->get_state_districts($state, $this->data['user_district_id'], "");
    $limited = "all";
    if(count($districts) > 0){
      foreach ($districts as $row){
        if ($limited == "1")
        {
          if (in_array($row->id,$allowed_disricts))
          {
            $value = "";
            if ($row->dise_code != "")
              $value = "(".$row->dise_code.") ";
            $row1['value'] = $value.ucwords(htmlentities(stripslashes($row->name)));
            $row1['id'] = $row->id;
            $row_set[] = $row1;
          }
        }
        else
        {
          $school_count = $this->schools_model->get_school_count(0, $row->id);
          $value = "";
          if ($row->dise_code != "")
            $value = "(".$row->dise_code.") ";
          $row1['school_count'] = $school_count;
          $row1['value'] = $value.ucwords(htmlentities(stripslashes($row->name)));
          $row1['id'] = $row->id;
          $row_set[] = $row1;
        }
      }
      echo json_encode($row_set); die;//format the array into json data
    }    
  }

 
  function get_schools()
  {    
    $this->load->model('schools_model');  
    $session_data = $this->session->userdata('logged_in');
    $limited = "all";
    
    if (isset($_GET['cluster']))
      $cluster = $_GET['cluster'];
    else
      $cluster = 0;
      
    if (isset($_GET['block']))
      $block = $_GET['block'];
    else
      $block = 0;
    $schools = $this->schools_model->search_schools(0,0,$block,$cluster);
   
    $product_id = $_GET['product_id'];
    $this->load->model('product_issued_model');  
    $d = $this->product_issued_model->getSchoolsByProduct($product_id);
    $alloted_schools = array();
    foreach($d as $school_data)
    {
      array_push($alloted_schools,$school_data->school_id);
    }
    
    if(count($schools) > 0){
      foreach ($schools as $row){
        if ($limited == "1")
        {
          if (in_array($row->id,$allowed_schools))
          {
            $value = "";
            if ($row->dise_code != "")
              $value = "(".$row->dise_code.") ";
            $row1['block'] = $value.ucwords(htmlentities(stripslashes($row->block_name)));
            $row1['cluster'] = ucwords(htmlentities(stripslashes($row->cluster_name)));
            $row1['value'] = ucwords(htmlentities(stripslashes($row->name)));
            $row1['id'] = $row->id;
            $row1['cluster_id'] = $row->cluster_id;
            $row1['block_id'] = $row->block_id;
            $row_set[] = $row1;
          }
        }
        else
        {
          $value = "";
          if ($row->dise_code != "")
            $value = "(".$row->dise_code.") ";
          $row1['value'] = $value.ucwords(htmlentities(stripslashes($row->name)));
          $row1['id'] = $row->id;
          $row1['dise_code'] = $row->dise_code;
          $row1['block'] = ucwords(htmlentities(stripslashes($row->block_name)));
          $row1['cluster'] = ucwords(htmlentities(stripslashes($row->cluster_name)));
          $row1['cluster_id'] = $row->cluster_id;
          $row1['block_id'] = $row->block_id;
          if (in_array($row->id,$alloted_schools) > 0)
          {
            $row1['alloted'] = "true";
          }
          else
          {
            $row1['alloted'] = "false";
          }
          $row_set[] = $row1;
        }
      }
      echo json_encode($row_set); //format the array into json data
    }    
  }
  
  function get_blocks()
  {
    $this->load->model('blocks_model');  
    $this->load->model('schools_model');  
    $district = $_GET['district'];
    $blocks = $this->blocks_model->get_district_blocks($district, "");
    $limited = "all";
    if(count($blocks) > 0){
      foreach ($blocks as $row){
        $school_count = $this->schools_model->get_school_count(0, 0, $row->id);
        $value = "";
        if ($row->dise_code != "")
          $value = "(".$row->dise_code.") ";
        $row1['school_count'] = $school_count;
        $row1['value'] = $value.ucwords(htmlentities(stripslashes($row->name)));
        $row1['id'] = $row->id;
        $row_set[] = $row1;
      }
      echo json_encode($row_set);  //format the array into json data
    }    
  }
  
  function get_clusters()
  {
    $this->load->model('clusters_model');  
    $this->load->model('schools_model');  
    $block = $_GET['block'];
    $clusters = $this->clusters_model->get_block_clusters($block, "");
    $limited = "all";
    if(count($clusters) > 0){
      foreach ($clusters as $row){
        $school_count = $this->schools_model->get_school_count(0, 0, 0, $row->id);
        $value = "";
        if ($row->dise_code != "")
          $value = "(".$row->dise_code.") ";
        $row1['school_count'] = $school_count;
        $row1['value'] = $value.ucwords(htmlentities(stripslashes($row->name)));
        $row1['id'] = $row->id;
        $row_set[] = $row1;
      }
      echo json_encode($row_set); die; //format the array into json data
    }    
  }
	
  function get_schools_list()
  {      
    $this->load->model('schools_model');  
    $cluster = $_GET['cluster'];
    $schools = $this->schools_model->get_cluster_schools($cluster, "");
    $limited = "all";
    if(count($schools) > 0){
      foreach ($schools as $row){
        
        $value = "(".$row->dise_code.") ";
        $row1['value'] = $value.ucwords(htmlentities(stripslashes($row->name)));
        $row1['id'] = $row->id;
        $row_set[] = $row1;
      }
      echo json_encode($row_set); //format the array into json data
    }    
  }
  
  function get_All_users()
  {
    $this->load->model('Users_model');   
    $users_list = $this->Users_model->getAlluser();
    if(count($users_list) > 0){
      foreach ($users_list as $row){
        $row1['name'] = $row->name;
        $row1['login_id'] = $row->login_id;
        $row1['state_id'] = $row->state_id;
        $row1['role'] = $row->role;
        $row_set[] = $row1;
      }
      echo json_encode($row_set); 
      die; //format the array into json data
    }    
  }
  
   //Get Spark List based on State
  function  getSparkFromState(){
    $this->load->model('Users_model');   
    $state = $_GET['state'];
    $sdate = (isset($_GET['sdate']) && $_GET['sdate'] !='' ? date('Y-m-d', strtotime($_GET['sdate'])) : '');
    $edate = (isset($_GET['edate']) && $_GET['edate'] !='' ? date('Y-m-d', strtotime($_GET['edate'])) : '');
    
    $user_group = array();  
   /* if($this->data['current_role'] == 'super_admin' || $this->data['current_role'] == 'admin' || $this->data['current_role'] == 'accounts' || $this->data['current_role'] == 'HR' || $this->data['current_role'] == 'reports'){
			$user_group = array('field_user');  
		}else{
			$user_group = array($this->data['current_group']);
		}*/
		
    $row_set= array();
    
    $sparkids = array();
    $spark_role = array('field_user', 'manager', 'state_person');  
    if($this->data['current_role'] == "state_person" || $this->data['current_role'] == "manager"){
			if(!in_array($this->data['login_id'], $sparkids)){
      $row1['name'] = $this->data['current_name']." (".$this->data['login_id'].")";
      $row1['login_id'] = $this->data['login_id'];
      $row1['state_id'] = $this->data['user_state_id'];
      $row1['role'] = $this->data['current_role'];
      $row1['id'] = $this->data['current_user_id'];
      $row_set[] = $row1;
      array_push($sparkids, $this->data['login_id']);
			}
      $spark_role = array('field_user', 'manager');  
      $user_group = array($this->data['current_group']);
    }

    if($sdate != '' && $edate != ''){
			if($this->data['current_group'] == 'head_office' && ($this->data['current_role'] == "state_person" || $this->data['current_role'] == "manager")){
					$spark_role = array('field_user', 'manager', 'state_person');
					$user_group = array('head_office', 'field_user'); 
					$users_list = $this->Users_model->getAllHOTeam($this->data['current_user_id'], $sdate, $edate, $spark_role, $user_group);
			}
			else{
				$users_list = $this->Users_model->getAllSparksByState($state, $sdate, $edate, '', $spark_role, $user_group);
			}
    if(count($users_list) > 0){
				foreach ($users_list as $row){
					if(!in_array($row['login_id'], $sparkids)){
					$row1['name'] = $row['name']." (".$row['login_id'].")";
					$row1['login_id'] = $row['login_id'];
					$row1['state_id'] = $row['state_id'];
					$row1['role'] = $row['role'];
					$row1['id'] = $row['user_id'];
					$row_set[] = $row1;
					array_push($sparkids, $row['login_id']);
					}
				}
			}
		}else{
			if($this->data['current_group'] == 'head_office' && ($this->data['current_role'] == "state_person" || $this->data['current_role'] == "manager")){
					$spark_role = array('field_user', 'manager', 'state_person');
					$user_group = array('head_office', 'field_user'); 
					$users_list = $this->Users_model->getAllHOTeam($this->data['current_user_id'], '', '', $spark_role, $user_group);
			}
			else{
				$users_list = $this->Users_model->getSparkByState($state, $spark_role, $user_group);
			}
			if(count($users_list) > 0){
				foreach ($users_list as $row){
					if(!in_array($row->login_id, $sparkids)){
					$row1['name'] = $row->name." (".$row->login_id.")";
					$row1['login_id'] = $row->login_id;
					$row1['state_id'] = $row->state_id;
					$row1['role'] = $row->role;
					$row1['id'] = $row->user_id;
					
					$row_set[] = $row1;
					array_push($sparkids, $row->login_id);
					}
				}
			}	
		}
    
    if(!empty($row_set))
     echo json_encode($row_set); die; //format the array into json data   
  }
  
   //Get Current Spark List based on State
  function  getCurrentSparkFromState(){
    $this->load->model('Users_model');   
    $state = $_GET['state'];
    $sdate = (isset($_GET['sdate']) && $_GET['sdate'] !=''  ? date('Y-m-d', strtotime($_GET['sdate'])) : '');
    $edate = (isset($_GET['edate']) && $_GET['edate'] !=''  ? date('Y-m-d', strtotime($_GET['edate'])) : '');
    
    $row_set= array();
    if($this->data['current_role'] == 'super_admin' || $this->data['current_role'] == 'admin' || $this->data['current_role'] == 'accounts' || $this->data['current_role'] == 'HR' || $this->data['current_role'] == 'reports'){
			$user_group = array('field_user');  
		}else{
			$user_group = array($this->data['current_group']);
		}
    
    $sparkids = array();
    $spark_role = array('field_user', 'manager', 'state_person');  
    if($this->data['current_role'] == "state_person" || $this->data['current_role'] == "manager"){
			if(!in_array($this->data['login_id'], $sparkids)){
      $row1['name'] = $this->data['current_name']." (".$this->data['login_id'].")";
      $row1['login_id'] = $this->data['login_id'];
      $row1['state_id'] = $this->data['user_state_id'];
      $row1['role'] = $this->data['current_role'];
      $row1['id'] = $this->data['current_user_id'];
      $row_set[] = $row1;
      array_push($sparkids, $this->data['login_id']);
			}
      $spark_role = array('field_user', 'manager');  
    }
    
    if($state == 'HO')
    {
			$state = 0;
			$spark_role = array('field_user', 'manager', 'state_person');  
			$user_group = array('head_office');  
		}
		
		if($sdate != '' && $edate != ''){
			if($this->data['current_group'] == 'head_office' && ($this->data['current_role'] == "state_person" || $this->data['current_role'] == "manager")){
					$spark_role = array('field_user', 'manager', 'state_person');
					$user_group = array('head_office', 'field_user'); 
					$users_list = $this->Users_model->getAllHOTeam($this->data['current_user_id'], $sdate, $edate, $spark_role, $user_group);
			}
			else{
					$users_list = $this->Users_model->getAllSparksByState($state, $sdate, $edate, '', $spark_role, $user_group);
			}
			if(count($users_list) > 0){
				foreach ($users_list as $row){
					if(!in_array($row['login_id'], $sparkids)){
					$row1['name'] = $row['name']." (".$row['login_id'].")";
					$row1['login_id'] = $row['login_id'];
					$row1['state_id'] = $row['state_id'];
					$row1['role'] = $row['role'];
					$row1['id'] = $row['user_id'];
					$row_set[] = $row1;
					array_push($sparkids, $row['login_id']);
					}
				}
			}
		}else{
			if($this->data['current_group'] == 'head_office' && ($this->data['current_role'] == "state_person" || $this->data['current_role'] == "manager")){
					$spark_role = array('field_user', 'manager', 'state_person');
					$user_group = array('head_office', 'field_user'); 
					$users_list = $this->Users_model->getAllHOTeam($this->data['current_user_id'], '', '', $spark_role, $user_group);
			}
			else{
					$users_list = $this->Users_model->getSparksByCurrentState($state, $spark_role, $user_group);
			}
			if(count($users_list) > 0){
				foreach ($users_list as $row){
					if(!in_array($row->login_id, $sparkids)){
					$row1['name'] = $row->name." (".$row->login_id.")";
					$row1['login_id'] = $row->login_id;
					$row1['state_id'] = $row->state_id;
					$row1['role'] = $row->role;
					$row1['id'] = $row->id;
					
					$row_set[] = $row1;
					array_push($sparkids, $row->login_id);
					}
				}
			}
		}
    if(!empty($row_set))
     echo json_encode($row_set); die; //format the array into json data   
  }
  
   //Get Manager List based on State
  function  get_manager_from_state(){
    $this->load->model('Users_model');   
    $state = $_GET['state'];
    
    $users_list = $this->Users_model->getManagerByState($state);
    if(count($users_list) > 0){
      foreach ($users_list as $row){
        $row1['name'] = $row->name." (".$row->login_id.")";
        $row1['login_id'] = $row->login_id;
        $row1['state_id'] = $row->state_id;
        $row1['role'] = $row->role;
        $row1['id'] = $row->user_id;
        $row_set[] = $row1;
      }
      echo json_encode($row_set); die; //format the array into json data
    }   
    else{
      echo json_encode(array('result'=>'No Record'));die;
    }
  }
  
  function return_value_percent($percent_value, $current_value)
  {
    if($current_value > 0)
      $net_value = round(($current_value*$percent_value)/100);
    else  
      $net_value = 0;
    
    return $net_value;  
  }
  
   
  function mark_attendance()
  {
		$this->load->Model('Tracking_model');  
		$current_time = date('Y-m-d H:i:s');
			
		$mdata['tracking_time'] = $current_time;
		$mdata['tracking_latitude'] = $_POST['vlatitude'];
		$mdata['tracking_longitude'] = $_POST['vlongitude'];
		$mdata['activity_type'] = $_POST['activity_type'];
		$mdata['created_at'] = $current_time;
		$mdata['created_by'] = 'web';
		$mdata['spark_id'] = $this->data['current_user_id'];

		$res = $this->Tracking_model->add($mdata);
  }
  
  //Get Spark List based on State
  function  getHRSparkFromState(){
    $this->load->model('Users_model');   
    $state = $_GET['state'];
    $sdate = (isset($_GET['sdate']) && $_GET['sdate'] !=''  ? date('Y-m-d', strtotime($_GET['sdate'])) : '');
    $edate = (isset($_GET['edate']) && $_GET['edate'] !=''  ? date('Y-m-d', strtotime($_GET['edate'])) : '');
    
    $row_set= array();
    
    //if($this->data['current_role'] == "super_admin" || $this->data['current_role'] == "accounts" || $this->data['current_role'] == "hr"){
			$EMP_ROLES = $this->data['EMP_ROLES'];
			
			$spark_role = array();
			foreach($EMP_ROLES as $key=>$value)
			{
				array_push($spark_role, $key);
			}
		
		$user_group = array();	
		//$spark_role = array('field_user', 'manager', 'state_person');  
    if($this->data['current_role'] == "state_person" || $this->data['current_role'] == "manager"){
      $row1['name'] = $this->data['current_name']." (".$this->data['login_id'].")";
      $row1['login_id'] = $this->data['login_id'];
      $row1['state_id'] = $this->data['user_state_id'];
      $row1['role'] = $this->data['current_role'];
      $row1['id'] = $this->data['current_user_id'];
      $row_set[] = $row1;
      
      $spark_role = array('field_user', 'manager');       
      $user_group = array($this->data['current_group']); 
    }
		
		if($sdate != '' && $edate != ''){
			if($this->data['current_group'] == 'head_office' && ($this->data['current_role'] == "state_person" || $this->data['current_role'] == "manager")){
				 $spark_role = array('field_user', 'manager', 'state_person');
				 $user_group = array('head_office', 'field_user'); 
				 $users_list = $this->Users_model->getAllHOTeam($this->data['current_user_id'], $sdate, $edate, $spark_role, $user_group);
			}
			else{
				 $users_list = $this->Users_model->getAllSparksByState($state, $sdate, $edate, '', $spark_role, $user_group);
			}
			if(count($users_list) > 0){
				foreach ($users_list as $row){
					$row1['name'] = $row['name']." (".$row['login_id'].")";
					$row1['login_id'] = $row['login_id'];
					$row1['state_id'] = $row['state_id'];
					$row1['role'] = $row['role'];
					$row1['id'] = $row['user_id'];
					$row_set[] = $row1;
				}
			}
		}else{
			if($this->data['current_group'] == 'head_office' && ($this->data['current_role'] == "state_person" || $this->data['current_role'] == "manager")){
					$spark_role = array('field_user', 'manager', 'state_person');
					$user_group = array('head_office', 'field_user'); 
					$users_list = $this->Users_model->getAllHOTeam($this->data['current_user_id'], '', '', $spark_role, $user_group);
			}
			else{
					$users_list = $this->Users_model->getSparksByCurrentState($state, $spark_role, $user_group);
			}
			if(count($users_list) > 0){
				foreach ($users_list as $row){
					$row1['name'] = $row->name." (".$row->login_id.")";
					$row1['login_id'] = $row->login_id;
					$row1['state_id'] = $row->state_id;
					$row1['role'] = $row->role;
					$row1['id'] = $row->id;
					
					$row_set[] = $row1;
				}
			}
		}
    if(!empty($row_set))
     echo json_encode($row_set); die; //format the array into json data   
  }
    
}/* End of file welcome.php *//* Location: ./application/controllers/welcome.php */
