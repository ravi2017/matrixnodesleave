<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Team_members extends CI_Controller {

	function __construct()	
	{
		parent::__construct();
		$this->load->helper('url');
		$this->data['current_page'] = 'users';
		$this->load->library('form_validation');
		$this->load->library('common_functions');
    $this->load->model('users_model');
    $this->load->model('team_members_model');
    $this->load->model('districts_model');
		$this->load->model('sparks_model');
		$this->load->helper('date');
    $this->team_ids = array();
    
    if($this->session->userdata('logged_in'))
		{
      $session_data = $this->session->userdata('logged_in');
      $this->data['current_user_id'] = $session_data['id'];
      $this->data['current_username'] = $session_data['username'];
      $this->data['current_name'] = $session_data['name'];
      $this->data['current_role'] = $session_data['role'];
      $this->data['user_state_id'] = $session_data['state_id'];
      $this->data['user_district_id'] = $session_data['district_id'];
      $this->data['login_id'] = $session_data['login_id'];
      $this->data['current_group'] = $session_data['user_group'];
      if ($this->data['current_role'] == "field_user")
      {
        //If no session, redirect to login page
        $this->session->set_flashdata('alert', "You are not authorized to access this section");
        redirect('dashboard', 'refresh');
      }
    }
    else
    {
      //If no session, redirect to login page
      redirect('login', 'refresh');
    }
	}

  public function index()
  {
    if ($this->data['current_role'] == "state_person" or $this->data['current_role'] == "super_admin") 
    {
      $this->data['partial'] = 'team_members/index';
      $this->data['page_title'] = 'Team members';
      $this->load->view('templates/index', $this->data);
    }
    else
    {
      //If no session, redirect to login page
      redirect('dashboard', 'refresh');
    }
  }

	public function view()
	{
    if ($this->data['current_role'] == "state_person" or $this->data['current_role'] == "manager" or $this->data['current_role'] == "super_admin") 
    {
      $this->data['team_members_list'] = $this->team_members_model->get_all_team_members($this->data['current_user_id']);
      //print_r($this->data['team_members_list']);exit;
      $this->data['current_role'] = $this->data['current_role'];
      $this->data['current_user'] = $this->data['current_user_id'];
      $this->data['partial'] = 'team_members/view';
      $this->data['page_title'] = 'Team members';
      $this->load->view('templates/index', $this->data);
    }
    else
    {
      //If no session, redirect to login page
      redirect('dashboard', 'refresh');
    }
	}

  public function view_team($id){
    if ($this->data['current_role'] == "state_person" or $this->data['current_role'] == "super_admin") 
    {
      $this->data['team_members_list'] = $this->team_members_model->get_team($id);
      $this->data['current_user'] = $this->data['current_user_id'];
      $this->data['partial'] = 'team_members/view';
      $this->data['page_title'] = 'Team members';
      $this->load->view('templates/index', $this->data);
    }
    else
    {
      //If no session, redirect to login page
      redirect('dashboard', 'refresh');
    }
  }

	public function add()
	{
    if ($this->data['current_role'] == "state_person") 
    {
      $state_id = $this->data['user_state_id'];
      $state_persons = $this->team_members_model->get_state_users($state_id,'manager');
      $this->data['state_persons'] = $state_persons;
      $this->data['logged_in_user'] = $this->data['current_user_id'];
      $this->data['partial'] = 'team_members/form';
      $this->data['action'] = 'create';
      $this->data['page_title'] = 'Add Team member';
      $this->load->model('states_model');
      $this->data['states'] = $this->states_model->get_all_states();
      $this->load->view('templates/index', $this->data);
    }
    else
    {
      //If no session, redirect to login page
      redirect('dashboard', 'refresh');
    }
	}
	
	public function create()
	{
    if ($this->data['current_role'] == "state_person") 
    {
      //echo '<pre>';
      //print_r($_POST); exit;
      $mdata['login_id'] = $this->input->post('login_id');
      $mdata['role'] = $this->input->post('role');
      $mdata['name'] = $this->input->post('name');
      $mdata['email'] = $this->input->post('email');
      $mdata['mobile'] = $this->input->post('mobile');
      $mdata['password'] = md5($this->input->post('password'));
      $mdata['created_by'] = $this->data['current_user_id'];
      $mdata['state_id'] = $this->data['user_state_id'];
      $mdata['district_id'] = 0;
      if(!empty($this->input->post('manager_id'))){
        $mdata['manager_id'] = $this->input->post('manager_id');
      }else{
        $mdata['manager_id'] = $this->data['current_user_id'];
      }
      
      $user_id = $this->users_model->add($mdata);
      if($user_id > 0){
        //$district_id = $this->input->post('district_id');
        $block_ids = $this->input->post('block_id');
        foreach ($block_ids as $block_id) {
          $dist_block = explode('_', $block_id);
          $this->team_members_model->save_spark_district_block($user_id, $dist_block);
        }

        $sdata['user_id'] = $user_id;
        $sdata['state_id'] = $mdata['state_id'];
        $sdata['start_date'] = date("Y-m-d");
        $this->users_model->add_user_state($sdata);
            
        header('location:'.base_url()."team_members/view");
      }else{
        $this->session->set_flashdata('alert', 'User id already exist.');
        redirect('dashboard', 'refresh');
      }
    }
    else
    {
      //If no session, redirect to login page
      redirect('dashboard', 'refresh');
    }
	}

  public function check_user_id(){
    $res = $this->team_members_model->check_user_id($_POST['user_id']);
    if($res == 0){
      echo "User id already exists.";
    }else{
      echo '';
    }
  }

	public function edit()
	{
    if ($this->data['current_role'] == "state_person" or $this->data['current_role'] == "manager") 
    { 
      $id = $this->uri->segment(3);
      $state_id = $this->data['user_state_id'];
      $state_persons = $this->team_members_model->get_state_users($state_id, 'manager');
      $current_user = (object) array('name'=>$this->data['current_name'], 'id'=>$this->data['current_user_id'], 'role'=>$this->data['current_role'], 'login_id'=>$this->data['login_id']);
      array_push($state_persons, $current_user);
      $this->data['state_persons'] = $state_persons;
      $user = $this->users_model->getById($id);
      if($this->data['current_user_id'] == $user['manager_id']){
        $this->data['user'] = $user;
        $districts = $this->team_members_model->get_member_district_ids($id);
        $district_ids = array(); $i = 0;
        foreach ($districts as $district) {
          $district_ids[$i] = $district->district_id;
          $i++;
        }
        $this->data['district_ids'] = json_encode(array_values(array_unique($district_ids)));
        $blocks = $this->team_members_model->get_member_block_ids($id);
        $block_ids = array(); $j = 0;
        foreach ($blocks as $block) {
          $block_ids[$j] = $block->block_id;
          $j++;
        }
        $this->data['block_ids'] = json_encode(array_values(array_unique($block_ids)));
        $this->data['logged_in_user'] = $this->data['current_user_id'];
        $this->data['logged_in_user_role'] = $this->data['current_role'];
        $this->data['partial'] = 'team_members/edit_form';
        $this->data['action'] = 'update';
        $this->data['page_title'] = 'Add Team member';
        $this->load->view('templates/index', $this->data);
      }else{
        //If no session, redirect to login page
        $this->session->set_flashdata('alert', "Permission denied.");
        redirect('dashboard', 'refresh');
      }
    }
    else
    {
      //If no session, redirect to login page
      redirect('dashboard', 'refresh');
    }
	}
  
	public function update()
	{
    if ($this->data['current_role'] == "state_person" or $this->data['current_role'] == "manager") 
    {
      $mdata['login_id'] = $this->input->post('login_id');
      $mdata['role'] = $this->input->post('role');
      $mdata['name'] = $this->input->post('name');
      $mdata['email'] = $this->input->post('email');
      $mdata['mobile'] = $this->input->post('mobile');
      $mdata['created_by'] = $this->data['current_user_id'];
      $mdata['state_id'] = $this->data['user_state_id'];
      $mdata['district_id'] = 0;
      $user_id = $this->input->post('user_id');
      if(!empty($this->input->post('manager_id'))){
        $mdata['manager_id'] = $this->input->post('manager_id');
      }else{
        $mdata['manager_id'] = $this->data['current_user_id'];
      }
      
      $this->users_model->edit($mdata, $user_id);
      
      $districts = $this->team_members_model->get_member_district_ids($user_id);
      $district_ids = array(); $i = 0;
      foreach ($districts as $district) {
        $district_ids[$i] = $district->district_id;
        $i++;
      }
      $blocks = $this->team_members_model->get_member_block_ids($user_id);
      $block_ids = array(); $j = 0;
      foreach ($blocks as $block) {
        $block_ids[$j] = $block->block_id;
        $j++;
      }
      $blocks_old = array();
      for($i = 0; $i < count($district_ids); $i++){
        $temp = array($district_ids[$i], $block_ids[$i]);
        $blocks_old[$i] = implode("_", $temp);
      }
      if($this->input->post('block_id') != ""){
        $blocks_new = $this->input->post('block_id');
      }else{
        $blocks_new = array();
      }
      $blocks_delete = array_diff($blocks_old, $blocks_new);

      foreach ($blocks_delete as $blocks_del) {
        $dist_block = explode('_', $blocks_del);
        $this->team_members_model->delete_spark_district_block($user_id, $dist_block);
      }
      $blocks_save = array_diff($blocks_new, $blocks_old);
      
      foreach ($blocks_save as $blocks_s) {
        $dist_block = explode('_', $blocks_s);
        $this->team_members_model->save_spark_district_block($user_id, $dist_block);
      }   

      $this->get_team_ids($user_id);

      if(!empty($this->team_ids)){
        foreach ($this->team_ids as $team_id) {
          foreach ($blocks_delete as $blocks_del) {
            $dist_block = explode('_', $blocks_del);
            $this->team_members_model->delete_spark_district_block($team_id, $dist_block);
          }
        }
      } 

      header('location:'.base_url()."team_members/view"); 
    }
    else
    {
      //If no session, redirect to login page
      redirect('dashboard', 'refresh');
    }
	}
				  
	public function delete($id)
	{
    if ($this->data['current_role'] == "state_person") 
    {
      $this->users_model->delete_a_user($id);
      $this->team_members_model->delete_spark_district_block($id);
      header('location:'.base_url()."team_members/view");
    }
    else
    {
      //If no session, redirect to login page
      redirect('dashboard', 'refresh');
    }
	}

  function get_districts()
  {  
    $state = $_GET['state'];
    $manager = $_GET['manager'];
    $curuser = $_GET['curuser'];
    if ($manager != "" and $manager != 0){
      $user = $this->db->select('role')->where('id',$manager)->get('sparks')->row();
      if($user->role == 'state_person'){
        $this->load->model('districts_model');
        $districts = $this->districts_model->get_state_districts($state);
      }else{
        $districts = $this->team_members_model->get_member_districts($manager);
      }
    }
    else
    { 
      $this->load->model('districts_model');
      $districts = $this->districts_model->get_state_districts($state);
    }
    //print_r($districts);exit;
    $limited = "all";
    if(count($districts) > 0){
      foreach ($districts as $row){
        if ($limited == "1")
        {
          if (in_array($row->id,$allowed_disricts))
          {
            $value = "";
            $row1['value'] = $value.ucwords(htmlentities(stripslashes($row->name)));
            $row1['id'] = $row->id;
            $row_set[] = $row1;
          }
        }
        else
        {
          $value = "";
          $row1['value'] = $value.ucwords(htmlentities(stripslashes($row->name)));
          $row1['id'] = $row->id;
          $row_set[] = $row1;
        }
      }
      echo json_encode($row_set); die;//format the array into json data
    }    
  }

  function get_blocks()
  { 
    $manager = $_GET['manager'];
    $district = $_GET['district'];
    $role = $_GET['role'];
    $cur_user = (isset($_GET['user_id']) and isset($_GET['user_id']) > 0) ? isset($_GET['user_id']) : 0;
    
    $district_detail = $this->districts_model->getById($district);
    $state_id = $district_detail['state_id'];
    $state_field_user_ids = array();
    $alloted_block_ids = array();

    $musers = $this->db->select('id')->where('manager_id', $manager)->get('sparks')->result();
    foreach ($musers as $muser) {
      $res = $this->db->select('block_id, user_id')->where('user_id', $muser->id)->get('spark_users_district_block')->result();
      foreach ($res as $r) {
        if($cur_user != $r->user_id){
          array_push($alloted_block_ids, $r->block_id);
        }
      }
    }
    if($role == 'manager' and ($manager != "" and $manager != 0)){
      $state_field_users = $this->db->select('id')->where(array('state_id'=>$state_id, "role"=>$role))->where_not_in('id', $manager)->get('sparks')->result();
    }else{
      $state_field_users = $this->db->select('id')->where(array('state_id'=>$state_id, "role"=>$role))->get('sparks')->result();
    }
    foreach($state_field_users as $state_field_user)
      array_push($state_field_user_ids,$state_field_user->id);
    
    if (count($state_field_user_ids) > 0)
    {
      if(isset($_GET['user_id'])){
        $alloted_blocks = $this->db->select('block_id')->where_in('user_id',$state_field_user_ids)->where_not_in('user_id', $_GET['user_id'])->get('spark_users_district_block')->result();
      }else{
        $alloted_blocks = $this->db->select('block_id')->where_in('user_id',$state_field_user_ids)->get('spark_users_district_block')->result();
      }
      
      foreach($alloted_blocks as $alloted_block)
        array_push($alloted_block_ids,$alloted_block->block_id);
    }
    
    //print_r($alloted_block_ids);exit;

    if ($manager != "" and $manager != 0){
      $user = $this->db->select('role')->where('id',$manager)->get('sparks')->row();
      if($user->role == 'state_person'){
        $this->load->model('blocks_model');  
        $this->load->model('schools_model');  
        $district = $_GET['district'];
        $blocks = $this->blocks_model->get_district_blocks($district, "");
      }else{
        $blocks = $this->team_members_model->get_member_blocks($manager,$district);
      }
    }
    else
    {
      $this->load->model('blocks_model');  
      $this->load->model('schools_model');  
      $district = $_GET['district'];
      $blocks = $this->blocks_model->get_district_blocks($district, "");
    }
    
    $limited = "all";
    $row_set = array();
    if(count($blocks) > 0){
      foreach ($blocks as $row){
        //echo " -- ".$row->id;
        if (!in_array($row->id,$alloted_block_ids))
        {
          $value = "";
          $row1['value'] = $value.ucwords(htmlentities(stripslashes($row->name)));
          $row1['id'] = $row->id;
          $row_set[] = $row1;
        }
      }
      echo json_encode($row_set);  //format the array into json data
    }    
  }

  public function get_managers(){
    $spark = $this->sparks_model->getById($_POST['id']);
    $managers = $this->team_members_model->get_state_users($spark['state_id'],'manager');
    echo json_encode($managers);
  }

  public function change_manager(){
    //print_r($_POST);exit;
    $spark = $this->sparks_model->getById($_POST['spark_id']);
    //print_r($spark);exit;
    $this->team_members_model->change_manager($_POST);
    redirect('team_members/view_team/'.$spark['manager_id']);
  }

  public function temp_script_blocks(){
    $res = $this->team_members_model->temp_script_blocks();
    echo $res;
  }

  public function temp_script_managers(){
    $res = $this->team_members_model->temp_script_managers();
    echo $res;
  }

  public function temp_script_activities(){
    $res = $this->team_members_model->temp_script_activities();
    echo $res;
  }

  public function temp_script_spark_states(){
    $res = $this->team_members_model->temp_script_spark_states();
    echo $res;
  }
  
  public function get_team_ids($id){
    $team = $this->team_members_model->get_team_ids($id);
      if(!empty($team)){
        foreach ($team as $t) {
          //echo "<br>".$t->id;
          $this->team_ids[] = $t->id;
          $this->get_team_ids($t->id);
        }
      }
  }
  
}
