<?php
/**
 * Common Functions class
 *
 * Contains common functions to be used in application
 *
 * @author  	Gagan Sharma
 */
class Common_Functions 
{ 
	function __construct()
	{
		$this->ci =& get_instance();
	}
	
	public function upload_folder(){
		$year_folder = "./uploads/".date("Y");
		if (!file_exists($year_folder))
			mkdir($year_folder);
			
		$month_folder = $year_folder."/".date("m");
		if (!file_exists($month_folder))
			mkdir($month_folder);
			
		$day_folder = $month_folder."/".date("d");
		if (!file_exists($day_folder))
			mkdir($day_folder);
		return $day_folder;
	}

	function create_pdf($summary,$name)
	{
	    //Load the library
	    $this->ci->load->library('m_pdf');
	    ///$Mpdf=new M_pdf('utf-8', array(210,360), 0);

	    //$Mpdf->WriteHTML($stylesheet,1);
/////////////	    $Mpdf->WriteHTML($summary);
////////////	    $Mpdf->Output($name.'.pdf','D'); 
      $this->ci->m_pdf->pdf->WriteHTML($summary);

      //download it.
      $this->ci->m_pdf->pdf->Output($name.'.pdf', "D"); 
  }

  /* download created excel file */
  function downloadExcel($name) {
      $myFile = "./uploads/reports/".$name.".xls";
      header("Content-Length: " . filesize($myFile));
      header('Content-Type: application/vnd.ms-excel; charater=utf8_unicode_ci');
      header('Content-Disposition: attachment; filename='.$name.'.xls');
      readfile($myFile);
  }
  
  function retrun_rating($subject_total, $subject_yes)
  {
    $subject_rate = '--';
    if($subject_total > 0){
      if($subject_yes >0){
         $subject_rate_value = round(($subject_yes/$subject_total)*100);
         if($subject_rate_value > 75){
           $subject_rate = 'A';
         }else if($subject_rate_value <= 75 && $subject_rate_value >= 51){
           $subject_rate = 'B';
         }else{
           $subject_rate = 'C';
         }
      }else{
        $subject_rate = 'C';
      }
    }
    return $subject_rate;
  }

  function retrun_rating_value($subject_total, $subject_yes)
  {
    $subject_rate = '--';
    if($subject_total > 0){
      if($subject_yes >0){
        $subject_rate = round($subject_yes/$subject_total)*100;
      }else{
        $subject_rate = 0;
      }
    }
    return $subject_rate;
  }

  
  function return_months($start_date, $end_date)
  {
    $a = date('Y-m-d', strtotime($start_date)); // "2007-01-01";
    $b = date('Y-m-d', strtotime($end_date)); // "2008-02-15"

    $months = array();
    $i = date("Ym", strtotime($a));
    //echo"<br>".date("Ym", strtotime($b));
    while($i <= date("Ym", strtotime($b))){
        //echo $i."\n";
        $months[] = substr($i, 0, 4).'-'.substr($i, 4, 2);
        if(substr($i, 4, 2) == "12")
           $i = (date("Y", strtotime($i."01")) + 1)."01";
        else
            $i++;
    }
    return $months;
    
  }

  
  function return_month_leave($leaves, $start_date, $end_date)
  {
    $leavedate = array();
    $templeave = array();
    
    $i = 0;
    foreach($leaves as $leave)
    {
      $leave_from_date = $leave->leave_from_date;
      $leave_end_date = $leave->leave_end_date;
      
      if($leave_from_date == $leave_end_date)
      {
        if((strtotime($leave_from_date) >= strtotime($start_date)) && (strtotime($leave_from_date) <= strtotime($end_date)))
        {
          $templeave[] = $leave_from_date;
          $leave_date = array();
          $leave_date['id'] = $leave->id;
          $leave_date['user_id'] = $leave->user_id;
          $leave_date['state_id'] = $leave->state_id;
          $leave_date['district_id'] = $leave->district_id;
          $leave_date['source'] = $leave->source;
          $leave_date['status'] = $leave->status;
          $leave_date['leave_type'] = $leave->leave_type;
          $leave_date['leave_date'] = $leave_from_date;
          $leavedate[] = (object) $leave_date;
        }
        $i++;
      }
      else{
        $dates = $this->createRange($leave_from_date, $leave_end_date);
        foreach($dates as $date){
          if(!in_array($date, $templeave)){
            if((strtotime($date) >= strtotime($start_date)) && (strtotime($date) <= strtotime($end_date))){
              $templeave[] = $date;
              $leave_date = array();
              $leave_date['id'] = $leave->id;
              $leave_date['user_id'] = $leave->user_id;
              $leave_date['state_id'] = $leave->state_id;
              $leave_date['district_id'] = $leave->district_id;
              $leave_date['source'] = $leave->source;
              $leave_date['status'] = $leave->status;
              $leave_date['leave_type'] = $leave->leave_type;
              $leave_date['leave_date'] = $date;
              $leavedate[] = (object) $leave_date;
              $i++;
            }
          }
        }  
      }
    }
    return $leavedate;
  }
  
  function return_month_leave_distinct($leaves, $start_date, $end_date, $state_holidays=array(), $sat_sun=array())
  {
    $leavedate = array();
    
    $sun_2_3 = array();
    $state_holiday = array();
    if(!empty($state_holidays)){
      foreach($state_holidays as $holidays){
         $state_holiday[] =  $holidays->activity_date;
      }
    }
    
    if(!empty($sat_sun)){
      foreach($sat_sun as $holidays){
         $sun_2_3[] =  $holidays->activity_date;
      }
    }
    //print_r($sun_2_3);
    //print_r($state_holiday);
    
    foreach($leaves as $leave)
    {
      $leave_from_date = $leave->leave_from_date;
      $leave_end_date = $leave->leave_end_date;
      
      if($leave_from_date == $leave_end_date)
      {
        if((strtotime($leave_from_date) >= strtotime($start_date)) && (strtotime($leave_from_date) <= strtotime($end_date)))
        {
          if(!in_array($leave_from_date, $leavedate) && !in_array($leave_from_date, $state_holiday) && !in_array($leave_from_date, $sun_2_3))
           array_push($leavedate, $leave_from_date);
        }
      }
      else{
        $dates = $this->createRange($leave_from_date, $leave_end_date);
        foreach($dates as $date){
          if((strtotime($date) >= strtotime($start_date)) && (strtotime($date) <= strtotime($end_date))){
            if(!in_array($date, $leavedate) && !in_array($date, $state_holiday) && !in_array($date, $sun_2_3))
              array_push($leavedate, $date);
          }
        }  
      }
    }
    return $leavedate;
  }
  
  
  function return_other_leave_distinct($leaves, $start_date, $end_date, $state_holidays=array(), $sat_sun=array())
  {
    $leavedate = array();
    
    $sun_2_3 = array();
    $state_holiday = array();
    if(!empty($state_holidays)){
      foreach($state_holidays as $holidays){
         $state_holiday[] =  $holidays->activity_date;
      }
    }
    
    if(!empty($sat_sun)){
      foreach($sat_sun as $holidays){
         $sun_2_3[] =  $holidays->activity_date;
      }
    }
    //print_r($sun_2_3);
    //print_r($state_holiday);
    
    foreach($leaves as $leave)
    {
      $leave_from_date = $leave->leave_from_date;
      $leave_end_date = $leave->leave_end_date;
      
      if($leave->leave_type == 'Other leave'){
        if($leave_from_date == $leave_end_date)
        {
          if((strtotime($leave_from_date) >= strtotime($start_date)) && (strtotime($leave_from_date) <= strtotime($end_date)))
          {
            if(!in_array($leave_from_date, $leavedate) && !in_array($leave_from_date, $state_holiday) && !in_array($leave_from_date, $sun_2_3))
             array_push($leavedate, $leave_from_date);
          }
        }
        else{
          $dates = $this->createRange($leave_from_date, $leave_end_date);
          foreach($dates as $date){
            if((strtotime($date) >= strtotime($start_date)) && (strtotime($date) <= strtotime($end_date))){
              if(!in_array($date, $leavedate) && !in_array($date, $state_holiday) && !in_array($date, $sun_2_3))
                array_push($leavedate, $date);
            }
          }  
        }
      }
    }
    return $leavedate;
  }
  
  
  function createRange($start, $end, $format = 'Y-m-d') {
      $start  = new DateTime($start);
      $end    = new DateTime($end);
      $invert = $start > $end;

      $dates = array();
      $dates[] = $start->format($format);
      while ($start != $end) {
          $start->modify(($invert ? '-' : '+') . '1 day');
          $dates[] = $start->format($format);
      }
      return $dates;
  }
  
  function get_employee_exit_date($spark_id)
  {
		$CI =& get_instance();
    $CI->load->model('Employees_model');
    $spark_details = $CI->Employees_model->check_detail_exist($spark_id);
    
    if(!empty($spark_details['exit_date']))
			$exit_date = $spark_details['exit_date'];
    else
			$exit_date = '';
    return $exit_date;
	}
	
	function get_weitage_value($field, $value)
  {
		switch($field)
		{
			case ('school_registered_rank' || 'school_registered'):
				$WEIGHTAGE = 4;	//for 25% we take 4
				break;
			case ('registered_teacher_rank' || 'registered_teacher'):
				$WEIGHTAGE = 4;
				break;
			case ('active_greater_1hr_rank' || 'active_greater_1hr'):
				$WEIGHTAGE = 4;
				break;
			case ('spark_engagement_rank' || 'spark_engagement'):
				$WEIGHTAGE = 4;
				break;
			case ('avg_teacher_engagement_rank' || 'avg_teacher_engagement'):
				$WEIGHTAGE = 4;
				break;
			default:  
				$WEIGHTAGE = 1;
		}
		if($value > 0)
      $number = $value/$WEIGHTAGE;
    else  
      $number = 0;
      
    return $number;
	}
  
}





