<?php
class Activity_Handler{

  function __construct()	
	{
    $CI =& get_instance();
    $CI->load->model('Users_model');
    $CI->load->model("holidays_model");
    $CI->load->model("no_visit_days_model");
    $CI->load->model("user_district_blocks_model");
        
    //if($CI->session->userdata('logged_in'))
		//{
      //$session_data = $CI->session->userdata('logged_in');
      $this->user_id = $CI->user_id;
      $this->role = $CI->role;
      $this->user_state_id = $CI->user_state_id;
      $this->user_district_id = $CI->user_district_id;
      
      /*$methods_with_login = array('no_visit', 'no_visit_day');
      
      $method = $CI->router->fetch_method();

      if ($this->role == "field_user" and !in_array($method, $methods_with_login))
      {
        //If no session, redirect to login page
        $CI->session->set_flashdata('alert', "You are not authorized to access this section");
        //redirect('dashboard', 'refresh');
      }*/
    //}
    
    //if(empty($this->user_id))
  //  {
      //If no session, redirect to login page
      //redirect('login', 'refresh');
  //  }
	}
  
  function create_questions_array($question_array)
  {
    $questions = json_decode($question_array);
    $new_array = array();
    foreach($questions as $question)
    {
      $question_data = array();
      $question_id = $question->id;
      $question_answer = $question->value;
      if ($question_answer == "-Select-")
        $question_answer = "";
      if (is_array($question_answer))
      {
      }
      else
      {
        $new_array[$question_id] = $question_answer;
      }
    }
    return $new_array;
  }
  
  function convert_datetime($time,$source)
  {
    if ($source == "app")
    {
      $datetime = explode("_",$time);
      $year = substr($datetime[0],0,4);
      $month = substr($datetime[0],4,2);
      $day = substr($datetime[0],6,2);
      $hour = substr($datetime[1],0,2);
      $min = substr($datetime[1],2,2);
      $sec = substr($datetime[1],4,2);
      //echo "<br>".$hour." >> ".$min." >> ".$sec." >> ".$month." >> ".$day." >> ".$year;
      return $date = date("Y-m-d H:i:s",mktime($hour,$min,$sec,$month,$day,$year));
    }
    else
    {
      return $date = date("Y-m-d H:i:s",strtotime($time));
    }
  }
  
  function insert_assessment_data($ques_array, $last_id_p ,$activity_type)
  {
    $CI =& get_instance();
      foreach ($ques_array as $qid=>$qdatas) {
        $subject_ids = explode("_",$qid);
        $subject_id = 0;
        if (count($subject_ids) == 2)
        {
          $qid = $subject_ids[0];
          $subject_id = $subject_ids[1];
        }
        error_log(" subject loop started");
        if(is_array($qdatas)){
          foreach($qdatas as $qkey=>$qvalue){
            $assessment_data = array();
            $assessment_data['activity_type'] = $activity_type;
            $assessment_data['activity_id'] = $last_id_p;
            $assessment_data['question_id'] = $qid;
            $assessment_data['subject_id'] = $qkey;
            $assessment_data['answer'] = $qvalue;
            $newAssessmentId = $CI->assessment_model->addNew($assessment_data);
            error_log("account->assessment->inserted new entry id---->".$newAssessmentId);
          }
        }
        else{
          $assessment_data = array();
          $assessment_data['activity_type'] = $activity_type;
          $assessment_data['activity_id'] = $last_id_p;
          $assessment_data['question_id'] = $qid;
          $assessment_data['subject_id'] = $subject_id;
          $assessment_data['answer'] = $qdatas;
          $newAssessmentId = $CI->assessment_model->addNew($assessment_data);
          error_log("account->assessment->inserted new entry id---->".$newAssessmentId);
        }
        //Print last inserted assessment unique id no
        //$newAssessmentId = $CI->assessment_model->add($assessment_data);
      }
  }

  public function no_visit_day($json)
  {
      if (isset($json['Data']) and isset($json['Data']['local_id']))
      {
        $android_local_id = $json['Data']['local_id'];
      }
      else
      {
        $android_local_id = 0;
      }
      $CI =& get_instance();
      $source = (isset($json['Data']['source']) ? $json['Data']['source'] : 'app');
      $date_of_no_visit = $json['Data']['type_info']['date_of_no_visit'];
      $user_id = $this->user_id;
      $mdata['user_id'] = $user_id;
      $state_id = $this->user_state_id;
      $district_id = $this->user_district_id;
      $date_timestamp = strtotime($date_of_no_visit);
      $month = date("m",$date_timestamp);
      $year = date("Y",$date_timestamp);
      $day = date('d', $date_timestamp);		
      $holiday = $CI->holidays_model->get_all($user_id, $date_of_no_visit, $date_of_no_visit);
      if (count($holiday) > 0) {
        $response = array('status' => 'failed', 'sync_error' => true, 'message' => 'No Visit cannot be marked for ' . $date_of_no_visit . ', as holiday already marked');
        return $response;
      }
      //Checking if already no visit marked or not
      
      $count_no_visit = "select count(activity_date) as cnt from ssc_no_visit_days where user_id = '$user_id' and month(activity_date) = '$month' and year(activity_date) = '$year' and day(activity_date) = '$day'"; 
      $select_query = $CI->db->query($count_no_visit);
         
      $row = $select_query->result();
      // print $row;exit;
      $noVisitCount = $row[0]->cnt;		
      
      if($noVisitCount>0){
          $response = array("http_code" => 401, 'local_id' => $android_local_id,'status' => 'failed', 'sync_error' => true, 'message' => 'No Visit already marked for ' . $date_of_no_visit . '.');
          return $response;
      }
      
      //CHECKING FULL DAY ACTIVITY MARKED --------------------
      // error_log("---Checking or No Visit Day----");
      $count_query = "select count(a.activity_date) as count from (select activity_date from ssc_trainings where ssc_trainings.user_id = '$user_id' and month(activity_date) = '$month' and year(activity_date) = '$year' and day(activity_date) = '$day' 
              union select activity_date from ssc_meetings where ssc_meetings.user_id = '$user_id' and month(activity_date) = '$month' and year(activity_date) = '$year' and day(activity_date) = '$day' 
              union select activity_date from ssc_school_visits where ssc_school_visits.user_id = '$user_id' and month(activity_date) = '$month' and year(activity_date) = '$year' and day(activity_date) = '$day') a"; 
      $select_query = $CI->db->query($count_query);
         
      //Execute query 
      $row = $select_query->result();
      // print $row;exit;
      $count = $row[0]->count;
      if($count>0){
        $response = array("http_code" => 401, 'local_id' => $android_local_id, 'status' => 'failed', 'sync_error' => true, 'message' => 'Days already marked for activity , can\'t take this request.');
        return $response;
      }
      else{
        $mdata['source'] = $source;
        $mdata['user_id'] = $user_id;
        $mdata['state_id'] = $state_id;
        $mdata['district_id'] = $district_id;
        $mdata['no_visit_reason'] = $json['Data']['type_info']['no_visit_reason'];
        $mdata['activity_date'] = $json['Data']['type_info']['date_of_no_visit'];
        //if($source == 'app')
          $mdata['status'] = 'approved';
        $CI->no_visit_days_model->add($mdata);
        $last_id_p = $CI->db->insert_id();
        $response = array("http_code" => 200, 'server_id' => $last_id_p, "sync_error" => false, "message" => "Stored Successfully", 'local_id' => $android_local_id);
        return $response;
        if(($this->$last_id_p)){
          $response = array("http_code" => 401, 'local_id' => $android_local_id, 'status' => 'failed', 'sync_error' => true, 'message' => 'Days already marked for activity , can\'t take this request.');
          return $response;
        }
      }
  }

  public function school_visit_activity($json)
  {
    if (isset($json['Data']) and isset($json['Data']['local_id']))
    {
      $android_local_id = $json['Data']['local_id'];
    }
    else
    {
      $android_local_id = 0;
    }
    $CI =& get_instance();
    $source = (isset($json['Data']['source']) ? $json['Data']['source'] : 'app');
    $school_id = $json['Data']['type_info']['school_id'];
		$activity_date = $this->convert_datetime($json['Data']['type_info']['date'],$source);
		$mode = (isset($json['Data']['mode']) ? $json['Data']['mode'] : 'add');
		$is_validator = (isset($json['Data']['is_validator']) ? $json['Data']['is_validator'] : 0);
		$user_id = $this->user_id;
		$date_timestamp = strtotime($activity_date);
		$month = date("m",$date_timestamp);
		$year = date("Y",$date_timestamp);
		$day = date('d', $date_timestamp);
    
    $travel_mode_id = 0;
    if(isset($json['Data']['type_info']['travelMode'])){
      $travel_mode_id = $this->get_travel_mode($json['Data']['type_info']['travelMode']);
    }
    
    $travel_cost = 0;
    if(isset($json['Data']['type_info']['travelCost'])){
      $travel_cost = $json['Data']['type_info']['travelCost'];
    }
    
    if($source == "app"){
      $sql = "select * from ssc_school_visits where school_id = '$school_id' and activity_date = '$activity_date' and user_id = '$user_id' ";
      $qry = $CI->db->query($sql);
      $row_visit = $qry->result();
      if(count($row_visit) > 0){
        $result_id = $row_visit[0]->id;			
        
        $response = array("http_code" => 200, 'server_id' => $result_id, "sync_error" => false, "message" => "Stored Successfully", 'local_id' => $android_local_id);
        return $response;
      }
    }
		
		//Activity is already marked for full day training. It cannot be allowed to be marked as visit day
		//$training_no_visit_query = "SELECT count(*)as count FROM ssc_trainings where training_duration = '24:00' and user_id = '$user_id' and month(activity_date) = '$month' and year(activity_date) = '$year' and day(activity_date) = '$day'";
    
		$training_no_visit_query = "SELECT count(*)as count FROM ssc_trainings where LOWER(training_type) = 'annual' and user_id = '$user_id' and month(activity_date) = '$month' and year(activity_date) = '$year' and day(activity_date) = '$day'";
		$select_query = $CI->db->query($training_no_visit_query);
		$row = $select_query->result();
		$fullDayTrainingCount = $row[0]->count;
		if($fullDayTrainingCount > 0){
			//$response = array("http_code" => 200, 'local_id' => $android_local_id,'delete_local' => '1', 'status' => 'failed', 'sync_error' => true, 'message' => 'Activity is already marked for annual training. It cannot be allowed to be marked as visit day.');
      //return $response;
      // open it for live
		}
		
		//No school visit on the day when leave is marked
		$date_leave = $year.'-'.$month.'-'.$day;
		$leave_count_query = "select count(*) as count from ssc_leaves where user_id= '$user_id' and leave_from_date<=date_format('$date_leave','%Y-%m-%d') and leave_end_date>=date_format('$date_leave','%Y-%m-%d') and status = 'approved'";
		$select_query = $CI->db->query($leave_count_query);
        $row = $select_query->result();
		$leaveCount = $row[0]->count;			
		if($leaveCount > 0){
			$response = array("http_code" => 200, 'local_id' => $android_local_id,'delete_local' => '1', 'sync_error' => true, 'status' => 'failed', 'message' => 'Activity day already marked for leave.');
      return $response;
		}
		
		$no_visit_count = "select count(activity_date) as cnt from ssc_no_visit_days where ssc_no_visit_days.user_id = '$user_id' and month(activity_date) = '$month' and year(activity_date) = '$year' and day(activity_date) = '$day'"; 
		$select_query = $CI->db->query($no_visit_count);
       
		//Execute query 
        $row = $select_query->result();
		// print $row;exit;
		$noVisitCount = $row[0]->cnt;	
		// $response = array("http_code" => 200, 'local_id' => $android_local_id, 'status' => 'failed', 'sync_error' => true, 'message' => $noVisitCount));
		if ($noVisitCount>0){
			$delete_query = "delete from ssc_no_visit_days where user_id = '$user_id' and month(activity_date) = '$month' and year(activity_date) = '$year' and day(activity_date) = '$day'";
			$select_query = $CI->db->query($delete_query);
			//Execute query 
			// $row = $select_query->result();
			// $response = array("http_code" => 200, 'local_id' => $android_local_id, 'status' => 'failed', 'sync_error' => true, 'message' => $select_query));
		}

    $school_classes = (isset($json['Data']['type_info']['selectedClasses']) ? $json['Data']['type_info']['selectedClasses'] : '');
    $school_subjects = (isset($json['Data']['type_info']['selectedSubjects']) ? $json['Data']['type_info']['selectedSubjects'] : '');
    
		$count = 0;
    if ($school_id > 0)
    {
      $count_query = "select  count(*) as count from ssc_school_visits where school_id = '$school_id' and month(activity_date) = '$month' and year(activity_date) = '$year' and day(activity_date) = '$day' and user_id = '$user_id'"; 
      $select_query = $CI->db->query($count_query);
      $school_dise_code = "";
      
      //Execute query 
      $row = $select_query->result();
      // print $row;exit;
      $count = $row[0]->count;
    } else {
      $school_dise_code = (isset($json['Data']['type_info']['school_dise_code']) ? $json['Data']['type_info']['school_dise_code'] : '');
    }
		
    if($count > 0){
			$response = array("http_code" => 200, 'local_id' => $android_local_id, 'sync_error' => true, 'delete_local' => '1', 'status' => 'failed', 'message' => 'Same school visit on same day not allowed');
      return $response;
		}
		else
    {	
        error_log("inside controller account - school visit - entered in method ", 0);
       
        error_log("inside controller account - school visit - after decode json ", 0);
        
        error_log("School visit - activity date time = "+ $activity_date);
        //$activity_date = date("Y-m-d", strtotime($activity_date));
		
        $duration = $json['Data']['type_info']['duration'];
        /*if (!$CI->validate_activity_date(date("Y-m-d", strtotime($activity_date))) && $mode == 'add'){
          $response = array("http_code" => 200, 'sync_error' => true, 'local_id' => $android_local_id, 'status' => 'failed', 'message' => 'Activity date cannot be in future date or older more than '.MAX_PREVIOUS_DAYS.' days from now.');
          return $response;
        }*/
        $user_id = $this->user_id;
        $state_id = $this->user_state_id;

        $district_ids = array();
        if($this->role != 'state_person'){
          $user_district_blocks = $CI->user_district_blocks_model->get_all($this->user_id);
          foreach($user_district_blocks as $user_district_block)
          {
            if (!in_array($user_district_block->district_id,$district_ids))
            {
              array_push($district_ids,$user_district_block->district_id);
            }
          }
        }
        else{
          array_push($district_ids, $json['Data']['type_info']['district_id']);
        }
        //$district_id = $this->user_district_id;
        $block_id = $json['Data']['type_info']['block_id'];
        $cluster_id = $json['Data']['type_info']['cluster_id'];
        
        if ($cluster_id > 0 and $school_id > 0)
        {
          $CI->validate_hierarchy($state_id, $district_ids, $block_id, $cluster_id, $android_local_id);
          error_log("Account.php school_visit_post: After validation hierarchy");    
        }        
        //$holiday = 
        $activity_date_only = date("Y-m-d", strtotime($activity_date));
        //-------------------------------------------
        //              HOLIDAY VALIDATION
        //-------------------------------------------
        
        /*$CI->load->model("holidays_model");
        if ($CI->holidays_model->isHoliday($activity_date_only,$state_id,$user_id)) { 
            //$response = array('status' => 'failed','delete_local' => '1', 'sync_error' => true, 'message' => 'School Visit cannot be marked for ' . $activity_date . ', as holiday already marked');
            //return $response;
            // open it for live
        } else 
        {
            error_log("Account.php school visit no HOLIDAY found ");*/
            $CI->load->model("schools_model");
            if ($school_id > 0)
            {
              $school_detail = $CI->schools_model->getById($school_id);
            }
            error_log("Account.php school visit get school model");
            if (isset($school_detail) and isset($school_detail['id']) and $school_detail['id'] == $school_id) 
            {
              error_log("-------------school visit -- 1", 0);
              if ($this->user_state_id > 0 and $this->user_district_id > 0) 
              {
                  error_log("-------------school visit -- 2", 0);
                  if ($this->user_state_id > 0) {
                      error_log("-------------school visit -- state id validated", 0);
                      if ($this->user_state_id != $school_detail['state_id']) {
                          $response = array('status' => 'failed', 'sync_error' => true, 'message' => 'You are not authorized to enter data for this state dise code');
                          return $response;
                      }
                  }
                  if ($this->user_district_id > 0) {
                      error_log("-------------school visit -- district id validated", 0);
                      if ($this->user_district_id != $school_detail['district_id']) {
                          error_log("-------------school visit -- district id validated", 0);
                          $response = array('status' => 'failed', 'sync_error' => true, 'message' => 'You are not authorized to enter data for this district dise code');
                          return $response;
                      }
                  }
              }
              if ($school_detail['cluster_id'] != null) 
              {
                  error_log("-------------school visit -- cluster id validated", 0);
                  if ($school_detail['cluster_id'] != $json['Data']['type_info']['cluster_id']){
                      $response = array("http_code" => 200, 'local_id' => $android_local_id, 'status' => 'failed', 'sync_error' => true, 'message' => 'School does not exist in this cluster');
                      return $response;
                    }
              }
              if ($school_detail['block_id'] != null) 
              {
                  if ($school_detail['block_id'] != $json['Data']['type_info']['block_id']){
                      $response = array("http_code" => 200, 'local_id' => $android_local_id, 'status' => 'failed', 'sync_error' => true, 'message' => 'School does not exist in this block');
                      return $response;
                    }
              }
              
              //-----------------------------------------
              //          TIME OVERLAP VALIDATION
              //-----------------------------------------
              $CI->load->model("Timeoverlaptest_model");

              //CHECKING FULL DAY ACTIVITY MARKED --------------------
              error_log("---From School Visit -- Going for Full day Activity check----");
              
              if (!($CI->Timeoverlaptest_model->checkFullDayActivity($user_id, $activity_date, 'PRODUCTIVE',null))) 
              {
                  $response = array("http_code" => 200, 'local_id' => $android_local_id, 'status' => 'failed', 'sync_error' => true, 'message' => 'Leave, Local-Holiday or No-Visit-Day is already marked : can\'t add activity.');
                  return $response;
              }
              //DEEPAK MARCH 25-2017
              $CI->Timeoverlaptest_model->checkAvailability($user_id,$activity_date,$duration);
              if ($CI->Timeoverlaptest_model->checkAvailability($user_id,$activity_date,$duration)) 
              {
                  error_log("School visit no time confilict found. ", 0);
                  //new entry
                  $mdata['source'] = $source;
                  $mdata['state_id'] = $school_detail['state_id'];
                  $mdata['district_id'] = $school_detail['district_id'];
                  $mdata['block_id'] = $school_detail['block_id'];
                  $mdata['cluster_id'] = $school_detail['cluster_id'];
                  //shool_id is dicecode
                  $mdata['school_id'] = $school_detail['id'];
                  $mdata['user_id'] = $user_id;
                  $mdata['duration'] = $json['Data']['type_info']['duration'];
                  $mdata['is_validate'] = $is_validator;
                  $mdata['activity_date'] = $activity_date;
                  if ($school_dise_code != "") {
                    $mdata['school_dise_code'] = $school_dise_code;
                  }
                  if ($school_classes != "") {
                    $mdata['classes'] = json_encode(explode(",",$school_classes));
                  }
                  if ($school_subjects != "") {
                    $mdata['subjects'] = json_encode(explode(",",$school_subjects));
                  }
                  $mdata['travel_mode_id'] = $travel_mode_id;
                  $mdata['travel_cost'] = $travel_cost;
                  //if($source == 'app')
                    $mdata['status'] = 'approved';
                  
                  error_log("Account.php school visit calling Add data");
                  $CI->load->model("school_visits_model"); 
                  $last_id_p = $CI->school_visits_model->add($mdata);
                  //$last_id_p = $CI->db->insert_id();
                  error_log("Account.php school visit calling assesment model");
                  $CI->load->model("assessment_model");
                  if ($source == "app")
                    $ques_array = $this->create_questions_array($json['Data']['type_info']['assesmentQuestion']);
                  else
                    $ques_array = $json['Data']['type_info']['assessment'];
                    
                  error_log("Account.php -- school_visit_post ->assess loaded-> Subject array".json_encode($ques_array)."last school visit id->".$last_id_p);
                  if (!empty($ques_array) && $last_id_p != null) {
                      error_log("account->sv_visit condtion checked and enter to loop---->");
                      $this->insert_assessment_data($ques_array, $last_id_p ,'school_visit');
                      error_log("account->sv_visit condtion checked after from subject loop---->");
                  }
              } 
              else
              {
                $response = array("http_code" => 200, 'local_id' => $android_local_id, 'status' => 'failed', 'sync_error' => true, 'message' => 'Activity duration conflicts with another activity, please choose different time slot.');
                return $response;
              }
              $response = array("http_code" => 200, 'server_id' => $last_id_p, "sync_error" => false, "message" => "Stored Successfully", 'local_id' => $android_local_id);
              return $response;
            } 	
            else if ($school_id == 0) 
            {              
              //-----------------------------------------
              //          TIME OVERLAP VALIDATION
              //-----------------------------------------
              $CI->load->model("Timeoverlaptest_model");

              //CHECKING FULL DAY ACTIVITY MARKED --------------------
              error_log("---From School Visit -- Going for Full day Activity check----");
              
              if (!($CI->Timeoverlaptest_model->checkFullDayActivity($user_id, $activity_date, 'PRODUCTIVE',null))) 
              {
                  $response = array("http_code" => 200, 'local_id' => $android_local_id, 'status' => 'failed', 'sync_error' => true, 'message' => 'Leave, Local-Holiday or No-Visit-Day is already marked : can\'t add activity.');
                  return $response;
              }
              //DEEPAK MARCH 25-2017
              $CI->Timeoverlaptest_model->checkAvailability($user_id,$activity_date,$duration);
              if ($CI->Timeoverlaptest_model->checkAvailability($user_id,$activity_date,$duration)) 
              {
                  error_log("School visit no time confilict found. ", 0);
                  //new entry
                  $mdata['source'] = $source;
                  $mdata['state_id'] = $json['Data']['type_info']['state_id'];
                  $mdata['district_id'] = $json['Data']['type_info']['district_id'];
                  $mdata['block_id'] = $json['Data']['type_info']['block_id'];
                  $mdata['cluster_id'] = 0;
                  //shool_id is dicecode
                  $mdata['school_id'] = 0;
                  $mdata['user_id'] = $user_id;
                  $mdata['duration'] = $json['Data']['type_info']['duration'];
                  $mdata['is_validate'] = $is_validator;
                  $mdata['activity_date'] = $activity_date;
                  if ($school_dise_code != "") {
                    $mdata['school_dise_code'] = $school_dise_code;
                  }
                  if ($school_classes != "") {
                    $mdata['classes'] = json_encode(explode(",",$school_classes));
                  }
                  if ($school_subjects != "") {
                    $mdata['subjects'] = json_encode(explode(",",$school_subjects));
                  }
                  $mdata['travel_mode_id'] = $travel_mode_id;
                  //if($source == 'app')
                    $mdata['status'] = 'approved';
                    
                  error_log("Account.php school visit calling Add data");
                  $CI->load->model("school_visits_model"); 
                  $last_id_p = $CI->school_visits_model->add($mdata);
                  //$last_id_p = $CI->db->insert_id();
                  error_log("Account.php school visit calling assesment model");
                  $CI->load->model("assessment_model");
                  if ($source == "app")
                    $ques_array = $this->create_questions_array($json['Data']['type_info']['assesmentQuestion']);
                  else
                    $ques_array = $json['Data']['type_info']['assessment'];
                    
                  error_log("Account.php -- school_visit_post ->assess loaded-> Subject array".json_encode($ques_array)."last school visit id->".$last_id_p);
                  if (!empty($ques_array) && $last_id_p != null) {
                      error_log("account->sv_visit condtion checked and enter to loop---->");
                      $this->insert_assessment_data($ques_array, $last_id_p ,'school_visit');
                      error_log("account->sv_visit condtion checked after from subject loop---->");
                  }
              } 
              else
              {
                $response = array("http_code" => 200, 'local_id' => $android_local_id, 'status' => 'failed', 'sync_error' => true, 'message' => 'Activity duration conflicts with another activity, please choose different time slot.');
                return $response;
              }
              $response = array("http_code" => 200, 'server_id' => $last_id_p, "sync_error" => false, "message" => "Stored Successfully", 'local_id' => $android_local_id);
              return $response;
            } 	
            else
            {
                $response = array("http_code" => 200, 'local_id' => $android_local_id, 'status' => 'failed', 'sync_error' => true, 'message' => 'Invalid DISE Code');
                return $response;
            }
        //}
    }  
  }
  
  public function attendance_out_activity($json)
  {
    if (isset($json['Data']) and isset($json['Data']['local_id']))
    {
      $android_local_id = $json['Data']['local_id'];
    }
    else
    {
      $android_local_id = 0;
    }
    $CI =& get_instance();
    $source = (isset($json['Data']['source']) ? $json['Data']['source'] : 'app');
		$activity_date = $this->convert_datetime($json['Data']['type_info']['date'],$source);
		$user_id = $this->user_id;
		$date_timestamp = strtotime($activity_date);
		$month = date("m",$date_timestamp);
		$year = date("Y",$date_timestamp);
		$day = date('d', $date_timestamp);
    
    $travel_mode_id = 0;
    if(isset($json['Data']['type_info']['travelMode'])){
      $travel_mode_id = $this->get_travel_mode($json['Data']['type_info']['travelMode']);
    }
    
    $travel_cost = 0;
    if(isset($json['Data']['type_info']['travelCost'])){
      $travel_cost = $json['Data']['type_info']['travelCost'];
    }

    error_log("inside controller account - school visit - entered in method ", 0);
   
    error_log("inside controller account - school visit - after decode json ", 0);
    
    error_log("School visit - activity date time = "+ $activity_date);
    //$activity_date = date("Y-m-d", strtotime($activity_date));

    $duration = $json['Data']['type_info']['duration'];
    /*if (!$CI->validate_activity_date(date("Y-m-d", strtotime($activity_date))) && $mode == 'add'){
      $response = array("http_code" => 200, 'sync_error' => true, 'local_id' => $android_local_id, 'status' => 'failed', 'message' => 'Activity date cannot be in future date or older more than '.MAX_PREVIOUS_DAYS.' days from now.');
      return $response;
    }*/
    $user_id = $this->user_id;

    $activity_date_only = date("Y-m-d", strtotime($activity_date));
    $CI->load->model("Attendance_model");
    
    //new entry
    $mdata['source'] = $source;
    $mdata['spark_id'] = $user_id;
    $mdata['address'] = $json['Data']['type_info']['location'];;
    $mdata['attendance_date'] = $activity_date;
    $mdata['travel_mode_id'] = $travel_mode_id;
    $mdata['travel_cost'] = $travel_cost;
    //if($source == 'app')
      $mdata['status'] = 'approved';
    
    $last_id_p = $CI->Attendance_model->add($mdata);
        
    $response = array("http_code" => 200, 'server_id' => $last_id_p, "sync_error" => false, "message" => "Stored Successfully", 'local_id' => $android_local_id);
    return $response;
  }
  
  public function other_activity($json)
  {
      if (isset($json['Data']) and isset($json['Data']['local_id']))
      {
        $android_local_id = $json['Data']['local_id'];
      }
      else
      {
        $android_local_id = 0;
      }
      $CI =& get_instance();
      $source = (isset($json['Data']['source']) ? $json['Data']['source'] : 'app');
      $activity_date = $this->convert_datetime($json['Data']['type_info']['date'],$source);
      $mode = (isset($json['Data']['mode']) ? $json['Data']['mode'] : 'add');
      $date_timestamp = strtotime($activity_date);
      $month = date("m",$date_timestamp);
      $year = date("Y",$date_timestamp);
      $day = date('d', $date_timestamp);
      $duration = $json['Data']['type_info']['duration'];
      /*if (!$CI->validate_activity_date($activity_date) && $mode == 'add'){
          $response = array("http_code" => 200, 'sync_error' => true, 'local_id' => $android_local_id, 'status' => 'failed', 'message' => 'Activity date cannot be in future date or older more than '.MAX_PREVIOUS_DAYS.' days from now.');
            return $response;
      }*/

      $user_id = $this->user_id;
      $state_id = $this->user_state_id;
      $district_id = $this->user_district_id;
      $CI->load->model("holidays_model");
      //$holiday = $this->holidays_model->get_all($user_id, $activity_date, $activity_date);

      //if ($CI->holidays_model->isHoliday($activity_date, $state_id,$user_id)) {
          //$response = array('status' => 'failed', 'sync_error' => true, 'message' => 'Training cannot be marked for ' . date($activity_date) . ', as holiday already marked');
          //return $response;
          // open it for live
      //}
    
      //No school visit on the day when leave is marked
      $date_leave = $year.'-'.$month.'-'.$day;
      $leave_count_query = "select count(*) as count from ssc_leaves where user_id= '$user_id' and leave_from_date<=date_format('$date_leave','%Y-%m-%d') and leave_end_date>=date_format('$date_leave','%Y-%m-%d') and status = 'approved'";
      $select_query = $CI->db->query($leave_count_query);
          $row = $select_query->result();
      $leaveCount = $row[0]->count;			
      if($leaveCount > 0){
        $response = array("http_code" => 200, 'local_id' => $android_local_id, 'status' => 'failed', 'sync_error' => true, 'message' => 'Activity day already marked for leave.');
            return $response;
      }		

      $CI->load->model("Timeoverlaptest_model");
      //CHECKING FULL DAY ACTIVITY MARKED --------------------
      if (!($CI->Timeoverlaptest_model->checkFullDayActivity($user_id, $activity_date, 'productive',null))) {
          $response = array("http_code" => 200, 'local_id' => $android_local_id, 'status' => 'failed', 'sync_error' => true, 'message' => 'Leave, Local-Holiday or No-Visit-Day is already marked : can\'t add activity.');
            return $response;
      }
      if (!$CI->Timeoverlaptest_model->checkAvailability($user_id,$activity_date,$duration)) {
          $response = array("http_code" => 200, 'local_id' => $android_local_id, 'status' => 'failed', 'sync_error' => true, 'message' => 'Activity duration conflicts with another activity, please choose different time slot.');
            return $response;
      }
      
      //Activity is already marked for meeting. It cannot be allowed to be marked as full day training
      $meeting_query = "SELECT count(*)as count FROM ssc_meetings where user_id = '$user_id' and month(activity_date) = '$month' and year(activity_date) = '$year' and day(activity_date) = '$day'";
      $select_query = $CI->db->query($meeting_query);
      $row = $select_query->result();
      $meetingCount = $row[0]->count;
      
      //Activity is already marked for school visit. It cannot be allowed to be marked as full day training
      $school_visit_query = "SELECT count(*)as count FROM ssc_school_visits where user_id = '$user_id' and month(activity_date) = '$month' and year(activity_date) = '$year' and day(activity_date) = '$day'";
      $select_query = $CI->db->query($school_visit_query);
      $row = $select_query->result();
      $schoolVisitCount = $row[0]->count;
      if($schoolVisitCount > 0 && $training_type == 'annual'){
        //$response = array("http_code" => 200, 'local_id' => $android_local_id,'delete_local' => '1', 'status' => 'failed', 'sync_error' => true, 'message' => 'Activity is already marked for school visit. It cannot be allowed to be marked as annual training.');
        //    return $response;
        // open it for live
      }
      
      $count_training = "select count(activity_date) as cnt from ssc_trainings where user_id = '$user_id' and month(activity_date) = '$month' and year(activity_date) = '$year' and day(activity_date) = '$day'"; 
      $select_query = $CI->db->query($count_training);
         
      //Execute query 
          $row = $select_query->result();
      // print $row;exit;
      $TrainingCount = $row[0]->cnt;		
      
      if($TrainingCount>0){
//          $response = array("http_code" => 200, 'local_id' => $android_local_id,'delete_local' => '1','status' => 'failed', 'sync_error' => true, 'message' => 'Training already marked for ' . $day.'-'.$month.'-'.$year . '.');
  //          return $response;
      }
      
      $temp_state_id =  $json['Data']['type_info']['state_id'];
      if($temp_state_id==0){
       $temp_state_id = $state_id;
      }
			
			$travel_mode_id = 0;
			if(isset($json['Data']['type_info']['travelMode'])){
				$travel_mode_id = $this->get_travel_mode($json['Data']['type_info']['travelMode']);
			}
			
			$travel_cost = 0;
			$from_local_cost = (isset($json['Data']['type_info']['from_local_cost']) ? $json['Data']['type_info']['from_local_cost'] : 0);
			$destination_local_cost = (isset($json['Data']['type_info']['destination_travel_cost']) ? $json['Data']['type_info']['destination_travel_cost'] : 0);
			$public_transport_cost = (isset($json['Data']['type_info']['public_transport_cost']) ? $json['Data']['type_info']['public_transport_cost'] : 0);

			if(isset($json['Data']['type_info']['travelCost'])){
				$travel_cost = $json['Data']['type_info']['travelCost'];
			}
			else{
				$travel_cost =  $from_local_cost + $destination_local_cost + $public_transport_cost;
			}
			
			$stay_with_sparks = (isset($json['Data']['type_info']['paid_for_sparks']) ? $json['Data']['type_info']['paid_for_sparks'] : '');

      $mdata['source'] = $source;
      $mdata['activity_date'] = $activity_date;
      $mdata['user_id'] = $user_id;
      $mdata['duration'] = $json['Data']['type_info']['duration'];
      $mdata['reason'] = $json['Data']['type_info']['reason'];
      $mdata['mode'] = $json['Data']['type_info']['travelMode'];
      $mdata['travel_mode_id'] = $travel_mode_id;
      $mdata['travel_cost'] = $travel_cost;
      
      $mdata['from_lat'] = $json['Data']['type_info']['pickuplat'];
      $mdata['from_lon'] = $json['Data']['type_info']['pickuplon'];
      $mdata['from_address'] = $json['Data']['type_info']['pickupAddres'];
      $mdata['from_accuracy'] = $json['Data']['type_info']['pickupaccuracy'];
      
      $mdata['to_lat'] = $json['Data']['type_info']['droplat'];
      $mdata['to_lon'] = $json['Data']['type_info']['droplon'];
      $mdata['to_address'] = $json['Data']['type_info']['dropAddres'];
      $mdata['to_accuracy'] = $json['Data']['type_info']['dropaccuracy'];
      
      $mdata['paid_for_sparks'] = $stay_with_sparks;
      $mdata['from_local_cost'] = $from_local_cost;
      $mdata['destination_local_cost'] = $destination_local_cost;
      $mdata['public_transport_cost'] = $public_transport_cost;
      $mdata['stay_require'] = (isset($json['Data']['type_info']['stays_require']) ? $json['Data']['type_info']['stays_require'] : '');
      $mdata['travel_to'] = (isset($json['Data']['type_info']['travel_to']) ? $json['Data']['type_info']['travel_to'] : '');
      $mdata['claim_status'] = 'approved';
      
      $CI->load->model("Claim_model");
      
      $last_id_p = $CI->Claim_model->addOtherActivity($mdata);
      
      if($stay_with_sparks != '')
      {
				$staywithsparks = json_decode($stay_with_sparks);
				
				foreach($staywithsparks as $staywithspark)
				{
					$sdata['other_activity_id'] = $last_id_p;
					$sdata['spark_id'] = $staywithspark;
					
					$CI->Claim_model->addStayActivity($sdata, 'other_activity_sparks');
				}
			}
      
      /*$CI->load->model("assessment_model");
      if ($source == "app")
        $ques_array = $this->create_questions_array($json['Data']['type_info']['assesmentQuestion']);
      else
        $ques_array = $json['Data']['type_info']['assessment'];
      
      error_log("Account.php -- other activity ->assess loaded-> Subject array".json_encode($ques_array)."last other activity id->".$last_id_p);
      if (!empty($ques_array) && $last_id_p != null) {
          error_log("account->other activity condtion checked and enter to loop---->");
          $this->insert_assessment_data($ques_array, $last_id_p ,'distance_travel');
          error_log("account->other_activity condtion checked after from subject loop---->");
      }*/

      $no_visit_count = "select count(activity_date) as cnt from ssc_no_visit_days where ssc_no_visit_days.user_id = '$user_id' and month(activity_date) = '$month' and year(activity_date) = '$year' and day(activity_date) = '$day'"; 
      $select_query = $CI->db->query($no_visit_count);
         
      //Execute query 
      $row = $select_query->result();
      // print $row;exit;
      $noVisitCount = $row[0]->cnt;	
      
      if ($noVisitCount>0){
        $delete_query = "delete from ssc_no_visit_days where user_id = '$user_id' and month(activity_date) = '$month' and year(activity_date) = '$year' and day(activity_date) = '$day'";
        $select_query = $CI->db->query($delete_query);
      }

      $response = array("http_code" => 200, 'server_id' => $last_id_p, "sync_error" => false, "message" => "Stored Successfully", 'local_id' => $android_local_id);
      return $response;
  }

  public function meeting_activity($json)
  {
    if (isset($json['Data']) and isset($json['Data']['local_id']))
    {
      $android_local_id = $json['Data']['local_id'];
    }
    else
    {
      $android_local_id = 0;
    }
    $CI =& get_instance();
    $source = (isset($json['Data']['source']) ? $json['Data']['source'] : 'app');
    $activity_date = $this->convert_datetime($json['Data']['type_info']['date'],$source);
		$mode = (isset($json['Data']['mode']) ? $json['Data']['mode'] : 'add');
		
		$duration = $json['Data']['type_info']['duration'];
		$date_timestamp = strtotime($activity_date);
		$month = date("m",$date_timestamp);
		$year = date("Y",$date_timestamp);
		$day = date('d', $date_timestamp);
		$user_id = $this->user_id;
    
    $travel_mode_id = 0;
    if(isset($json['Data']['type_info']['travelMode'])){
      $travel_mode_id = $this->get_travel_mode($json['Data']['type_info']['travelMode']);
    }
    $travel_cost = 0;
    if(isset($json['Data']['type_info']['travelCost'])){
      $travel_cost = $json['Data']['type_info']['travelCost'];
    }
    
    if($source == "app"){
      $sql = "select * from ssc_meetings where activity_date = '$activity_date' and user_id = '$user_id' ";
      $qry = $CI->db->query($sql);
      $row_visit = $qry->result();
      if(count($row_visit) > 0){
        $result_id = $row_visit[0]->id;			
        
        $response = array("http_code" => 200, 'server_id' => $result_id, "sync_error" => false, "message" => "Stored Successfully", 'local_id' => $android_local_id);
        return $response;
      }
    }
		
		//Activity is already marked for meeting. It cannot be allowed to be marked as full day training
		//$training_meeting_query = "SELECT count(*) as count FROM ssc_trainings where user_id = '$user_id' and training_duration = '24:00' and month(activity_date) = '$month' and year(activity_date) = '$year' and day(activity_date) = '$day'";
		$training_meeting_query = "SELECT count(*) as count FROM ssc_trainings where user_id = '$user_id' and LOWER(training_type) = 'annual' and month(activity_date) = '$month' and year(activity_date) = '$year' and day(activity_date) = '$day'";
		$select_query = $CI->db->query($training_meeting_query);
		$row = $select_query->result();
		$trainingCount = $row[0]->count;
		
		if($trainingCount > 0){
			//$response = array("http_code" => 200, 'local_id' => $android_local_id,'delete_local' => '1', 'status' => 'failed', 'sync_error' => true, 'message' => 'Activity is already marked for annual training. It cannot be allowed to be marked as meeting.');
      //    return $response;
      // open it for live
		}
		
    /*if (!$CI->validate_activity_date($activity_date) && $mode == 'add') {
      $response = array("http_code" => 200, 'sync_error' => true, 'local_id' => $android_local_id, 'status' => 'failed', 'message' => 'Activity date cannot be in future date or older more than '.MAX_PREVIOUS_DAYS.' days from now.');
          return $response;
    }*/
        
    if (isset($json['Data']['type_info']['stateid']) and $json['Data']['type_info']['stateid'] > 0)
    {
      $state_id = $json['Data']['type_info']['stateid'];
    }
    else
    {
      $state_id = $this->user_state_id;
    }
    
    if (isset($json['Data']['type_info']['district_id']) and $json['Data']['type_info']['district_id'] > 0)
    {
      $district_id = $json['Data']['type_info']['district_id'];
    }
    else if (isset($json['Data']['type_info']['districtid']) and $json['Data']['type_info']['districtid'] > 0)
    {
      $district_id = $json['Data']['type_info']['districtid'];
    }
    else
    {
      $district_id = $this->user_district_id;
    }
    // user can submit meeting activity on holiday
    //$CI->load->model("holidays_model");
    //if ($CI->holidays_model->isHoliday($activity_date, $state_id,$user_id)) {
        //send error in case activity is heald on holiday
        //$response = array('status' => 'failed', 'sync_error' => true, 'message' => 'Meeting cannot be marked for ' . $activity_date . ', as holiday already marked');
        //  return $response;
        // open it for live
    //}
		
		//No school visit on the day when leave is marked
		$date_leave = $year.'-'.$month.'-'.$day;
		$leave_count_query = "select count(*) as count from ssc_leaves where user_id= '$user_id' and leave_from_date<=date_format('$date_leave','%Y-%m-%d') and leave_end_date>=date_format('$date_leave','%Y-%m-%d') and status = 'approved'";
		$select_query = $CI->db->query($leave_count_query);
    $row = $select_query->result();
		$leaveCount = $row[0]->count;			
		if($leaveCount > 0){
			$response = array("http_code" => 200, 'local_id' => $android_local_id, 'status' => 'failed', 'sync_error' => true, 'message' => 'Activity day already marked for leave.');
      return $response;
		}		
		
    $CI->load->model("Timeoverlaptest_model");
    //CHECKING FULL DAY ACTIVITY MARKED --------------------
    if (!($CI->Timeoverlaptest_model->checkFullDayActivity($user_id, $activity_date, 'productive',null))) {
        $response = array("http_code" => 200, 'local_id' => $android_local_id, 'status' => 'failed', 'sync_error' => true, 'message' => 'Leave, Local-Holiday or No-Visit-Day is already marked : can\'t add activity.');
        return $response;
    }
    if(!$CI->Timeoverlaptest_model->checkAvailability($user_id,$activity_date,$duration)) {
        $response = array("http_code" => 200, 'local_id' => $android_local_id, 'status' => 'failed', 'sync_error' => true, 'message' => 'Activity duration conflicts with another activity, please choose different time slot.');
        return $response;           
    } 

		
    $CI->load->model("meetings_model");
    $mdata['source'] = $source;
    $mdata['activity_date'] = $activity_date;
    $mdata['user_id'] = $user_id;
    $mdata['state_id'] = $state_id;
    $mdata['district_id'] =  $district_id;
    $mdata['block_id'] = $json['Data']['type_info']['block_id'];
    $mdata['duration'] = $json['Data']['type_info']['duration'];
    $mdata['cluster_id'] = $json['Data']['type_info']['cluster_id'];
    $mdata['is_internal'] = ($json['Data']['type_info']['is_internal'] == "false") ? 0 : 1;
    $mdata['meet_with'] = $json['Data']['type_info']['designation'];
    $mdata['contact_person_name'] = $json['Data']['type_info']['contact_person_name'];
    $mdata['contact_person_number'] = $json['Data']['type_info']['contact_person_number'];
    $mdata['meeting_type'] = strtolower($json['Data']['type_info']['meetingType']);
    $mdata['meeting_mode'] = $json['Data']['type_info']['meeting_mode'];
    $mdata['no_of_invites'] = $json['Data']['type_info']['no_of_invites'];
    $mdata['no_of_attendees'] = $json['Data']['type_info']['no_of_attendees'];
    $mdata['subject'] = $json['Data']['type_info']['subject'];
    $mdata['remarks'] = $json['Data']['type_info']['remarks'];
    $mdata['agenda'] = $json['Data']['type_info']['agenda'];
    $mdata['location'] = $json['Data']['type_info']['location'];
    $mdata['travel_mode_id'] = $travel_mode_id;
    $mdata['travel_cost'] = $travel_cost;
    //if($source == 'app')
      $mdata['status'] = 'approved';
    
    $last_id_p = $CI->meetings_model->add($mdata);
    $CI->load->model("assessment_model");
    if ($source == "app")
      $ques_array = $this->create_questions_array($json['Data']['type_info']['assesmentQuestion']);
    else
      $ques_array = $json['Data']['type_info']['assessment'];
      
    error_log("Account.php -- meeting ->assess loaded-> Subject array".json_encode($ques_array)."last meeting_id->".$last_id_p);
    if (!empty($ques_array) && $last_id_p != null) {
        error_log("account->meeting condtion checked and enter to loop---->");
        $this->insert_assessment_data($ques_array, $last_id_p ,'meeting');
        error_log("account->meeting condtion checked after from subject loop---->");
    }
		
		$no_visit_count = "select count(activity_date) as cnt from ssc_no_visit_days where ssc_no_visit_days.user_id = '$user_id' and month(activity_date) = '$month' and year(activity_date) = '$year' and day(activity_date) = '$day'"; 
		$select_query = $CI->db->query($no_visit_count);
       
		//Execute query 
    $row = $select_query->result();
		// print $row;exit;
		$noVisitCount = $row[0]->cnt;	
		
		 // $response = array("http_code" => 200, 'server_id' => $last_id_p, "sync_error" => false, "message" => ));
		if ($noVisitCount>0){
			$delete_query = "delete from ssc_no_visit_days where ssc_no_visit_days.user_id = '$user_id' and month(activity_date) = '$month' and year(activity_date) = '$year' and day(activity_date) = '$day'";
			$select_query = $CI->db->query($delete_query);
		}

		
    $response = array("http_code" => 200, 'server_id' => $last_id_p, "sync_error" => false, "message" => "Stored Successfully", 'local_id' => $android_local_id);
          return $response;  
  }

  public function training_activity($json)
  {
      if (isset($json['Data']) and isset($json['Data']['local_id']))
      {
        $android_local_id = $json['Data']['local_id'];
      }
      else
      {
        $android_local_id = 0;
      }
      $CI =& get_instance();
      $source = (isset($json['Data']['source']) ? $json['Data']['source'] : 'app');
      $activity_date = $this->convert_datetime($json['Data']['type_info']['date'],$source);
      $mode = (isset($json['Data']['mode']) ? $json['Data']['mode'] : 'add');
      $date_timestamp = strtotime($activity_date);
      $month = date("m",$date_timestamp);
      $year = date("Y",$date_timestamp);
      $day = date('d', $date_timestamp);
      $duration = $json['Data']['type_info']['training_duration'];
      $training_type = strtolower($json['Data']['type_info']['training_type']);
      /*if (!$CI->validate_activity_date($activity_date, 45) && $mode == 'add'){
          $response = array("http_code" => 200, 'sync_error' => true, 'local_id' => $android_local_id, 'status' => 'failed', 'message' => 'Activity date cannot be in future date or older more than 45 days from now.');
            return $response;
      }*/
      $user_id = $this->user_id;
      $state_id = $this->user_state_id;
      $district_id = $this->user_district_id;
      $CI->load->model("holidays_model");
      
      if($source == "app"){
        $sql = "select * from ssc_trainings where activity_date = '$activity_date' and user_id = '$user_id' ";
        $qry = $CI->db->query($sql);
        $row_visit = $qry->result();
        if(count($row_visit) > 0){
          $result_id = $row_visit[0]->id;			
          
          $response = array("http_code" => 200, 'server_id' => $result_id, "sync_error" => false, "message" => "Stored Successfully", 'local_id' => $android_local_id);
          return $response;
        }
      }
      
      $travel_mode_id = 0;
      if(isset($json['Data']['type_info']['travelMode'])){
        $travel_mode_id = $this->get_travel_mode($json['Data']['type_info']['travelMode']);
      }
      $travel_cost = 0;
      if(isset($json['Data']['type_info']['travelCost'])){
        $travel_cost = $json['Data']['type_info']['travelCost'];
      }
      //$holiday = $this->holidays_model->get_all($user_id, $activity_date, $activity_date);

      //if ($CI->holidays_model->isHoliday($activity_date, $state_id,$user_id)) {
          //$response = array('status' => 'failed', 'sync_error' => true, 'message' => 'Training cannot be marked for ' . date($activity_date) . ', as holiday already marked');
          //return $response;
          // open it for live
      //}
      
      //No school visit on the day when leave is marked
      $date_leave = $year.'-'.$month.'-'.$day;
      $leave_count_query = "select count(*) as count from ssc_leaves where user_id= '$user_id' and leave_from_date<=date_format('$date_leave','%Y-%m-%d') and leave_end_date>=date_format('$date_leave','%Y-%m-%d') and status = 'approved'";
      $select_query = $CI->db->query($leave_count_query);
          $row = $select_query->result();
      $leaveCount = $row[0]->count;			
      if($leaveCount > 0){
        $response = array("http_code" => 200, 'local_id' => $android_local_id, 'status' => 'failed', 'sync_error' => true, 'message' => 'Activity day already marked for leave.');
            return $response;
      }		

      $CI->load->model("Timeoverlaptest_model");
      //CHECKING FULL DAY ACTIVITY MARKED --------------------
      if (!($CI->Timeoverlaptest_model->checkFullDayActivity($user_id, $activity_date, 'productive',null))) {
          $response = array("http_code" => 200, 'local_id' => $android_local_id, 'status' => 'failed', 'sync_error' => true, 'message' => 'Leave, Local-Holiday or No-Visit-Day is already marked : can\'t add activity.');
            return $response;
      }
      if (!$CI->Timeoverlaptest_model->checkAvailability($user_id,$activity_date,$duration)) {
          $response = array("http_code" => 200, 'local_id' => $android_local_id, 'status' => 'failed', 'sync_error' => true, 'message' => 'Activity duration conflicts with another activity, please choose different time slot.');
            return $response;
      }
      
      //Activity is already marked for meeting. It cannot be allowed to be marked as full day training
      $meeting_query = "SELECT count(*)as count FROM ssc_meetings where user_id = '$user_id' and month(activity_date) = '$month' and year(activity_date) = '$year' and day(activity_date) = '$day'";
      $select_query = $CI->db->query($meeting_query);
      $row = $select_query->result();
      $meetingCount = $row[0]->count;
      if($meetingCount > 0 && $training_type == 'annual'){
        //$response = array("http_code" => 200, 'local_id' => $android_local_id,'delete_local' => '1', 'sync_error' => true, 'status' => 'failed', 'message' => 'Activity is already marked for meeting. It cannot be allowed to be marked as annual training.');
        //return $response;
        // open it for live
      }
      
      
      //Activity is already marked for school visit. It cannot be allowed to be marked as full day training
      $school_visit_query = "SELECT count(*)as count FROM ssc_school_visits where user_id = '$user_id' and month(activity_date) = '$month' and year(activity_date) = '$year' and day(activity_date) = '$day'";
      $select_query = $CI->db->query($school_visit_query);
      $row = $select_query->result();
      $schoolVisitCount = $row[0]->count;
      if($schoolVisitCount > 0 && $training_type == 'annual'){
        //$response = array("http_code" => 200, 'local_id' => $android_local_id,'delete_local' => '1', 'sync_error' => true, 'status' => 'failed', 'message' => 'Activity is already marked for school visit. It cannot be allowed to be marked as annual training.');
        //    return $response;
        // open it for live
      }
      
      $count_training = "select count(activity_date) as cnt from ssc_trainings where user_id = '$user_id' and month(activity_date) = '$month' and year(activity_date) = '$year' and day(activity_date) = '$day'"; 
      $select_query = $CI->db->query($count_training);
         
      //Execute query 
          $row = $select_query->result();
      // print $row;exit;
      $TrainingCount = $row[0]->cnt;		
      
      if($TrainingCount>0){
//          $response = array("http_code" => 200, 'local_id' => $android_local_id,'delete_local' => '1', 'sync_error' => true,'status' => 'failed', 'message' => 'Training already marked for ' . $day.'-'.$month.'-'.$year . '.');
  //          return $response;
      }
      
      $temp_state_id =  $json['Data']['type_info']['state_id'];
      if($temp_state_id==0){
       $temp_state_id = $state_id;
      }

      $user_block_count = $CI->user_district_blocks_model->get_count($user_id, $json['Data']['type_info']['block_id']);

      $mdata['source'] = $source;
      $mdata['activity_date'] = $activity_date;
      $mdata['user_id'] = $user_id;
      $mdata['state_id'] = $temp_state_id;
      $mdata['district_id'] = $json['Data']['type_info']['district_id'];
      $mdata['block_id'] = $json['Data']['type_info']['block_id'];
      $mdata['cluster_id'] = $json['Data']['type_info']['cluster_id'];
      $mdata['role'] = $json['Data']['type_info']['role'];
      if(isset($json['Data']['type_info']['training_type'])){
        $mdata['training_type'] = strtolower($json['Data']['type_info']['training_type']);
      }
      else if(isset($json['Data']['type_info']['traning_type'])){
        $mdata['training_type'] = strtolower($json['Data']['type_info']['traning_type']);
      }
        
      $mdata['mode'] = ($user_block_count[0]->total > 0 ? 'internal' : 'external');
      $mdata['training_type'] = ($mdata['mode'] == "external" ? 'annual' : $mdata['training_type']);
      if(isset($json['Data']['type_info']['training_duration']))
        $mdata['training_duration'] = $json['Data']['type_info']['training_duration'];
      else if(isset($json['Data']['type_info']['duration']))
        $mdata['training_duration'] = $json['Data']['type_info']['duration'];  
      $mdata['no_of_days'] = $json['Data']['type_info']['no_of_days'];
      $mdata['no_of_teachers_trained'] = $json['Data']['type_info']['no_of_attendees'];
      $mdata['no_of_invitees'] = $json['Data']['type_info']['no_of_invitees'];
      $mdata['subject_id'] = $json['Data']['type_info']['training_subject'];
      $mdata['topic'] = $json['Data']['type_info']['topic'];
      $mdata['agenda'] = $json['Data']['type_info']['agenda'];
      $mdata['location'] = $json['Data']['type_info']['location'];
      $mdata['travel_mode_id'] = $travel_mode_id;
      $mdata['travel_cost'] = $travel_cost;
      //if($source == 'app')
        $mdata['status'] = 'approved';
      $CI->load->model("trainings_model");
      
      $last_id_p = $CI->trainings_model->add($mdata);
      
      $CI->load->model("assessment_model");
      if ($source == "app")
        $ques_array = $this->create_questions_array($json['Data']['type_info']['assesmentQuestion']);
      else
        $ques_array = $json['Data']['type_info']['assessment'];
      
      error_log("Account.php -- training ->assess loaded-> Subject array".json_encode($ques_array)."last training_id->".$last_id_p);
      if (!empty($ques_array) && $last_id_p != null) {
          error_log("account->training condtion checked and enter to loop---->");
          $this->insert_assessment_data($ques_array, $last_id_p ,'training');
          error_log("account->training condtion checked after from subject loop---->");
      }

      $no_visit_count = "select count(activity_date) as cnt from ssc_no_visit_days where ssc_no_visit_days.user_id = '$user_id' and month(activity_date) = '$month' and year(activity_date) = '$year' and day(activity_date) = '$day'"; 
      $select_query = $CI->db->query($no_visit_count);
         
      //Execute query 
      $row = $select_query->result();
      // print $row;exit;
      $noVisitCount = $row[0]->cnt;	
      
      if ($noVisitCount>0){
        $delete_query = "delete from ssc_no_visit_days where user_id = '$user_id' and month(activity_date) = '$month' and year(activity_date) = '$year' and day(activity_date) = '$day'";
        $select_query = $CI->db->query($delete_query);
      }

      $response = array("http_code" => 200, 'server_id' => $last_id_p, "sync_error" => false, "message" => "Stored Successfully", 'local_id' => $android_local_id);
      return $response;
  }

  public function call_activity($json)
  {
      if (isset($json['Data']) and isset($json['Data']['local_id']))
      {
        $android_local_id = $json['Data']['local_id'];
      }
      else
      {
        $android_local_id = 0;
      }
      $CI =& get_instance();
      $source = (isset($json['Data']['source']) ? $json['Data']['source'] : 'app');
      $mode = (isset($json['Data']['date']) ? $json['Data']['date'] : 'add');
      $activity_date = date("Y-m-d", strtotime($json['Data']['date']));
      $date_timestamp = strtotime($activity_date);
      $month = date("m",$date_timestamp);
      $year = date("Y",$date_timestamp);
      $day = date('d', $date_timestamp); 
 
      if (!$CI->validate_activity_date($activity_date) && $mode == 'add'){
        $response = array("http_code" => 200, 'sync_error' => true, 'local_id' => $android_local_id, 'status' => 'failed', 'message' => 'Activity date cannot be in future date or older more than '.MAX_PREVIOUS_DAYS.' days from now.');
        return $response;
      }
      $user_id = $this->user_id;
      $state_id = $this->user_state_id;
      $district_id = $this->user_district_id;
      $CI->load->model("holidays_model");
      $CI->load->model("calls_model");
      $CI->load->model("contact_model");

      //$holiday = $CI->holidays_model->get_all($user_id, $activity_date, $activity_date);

      //if (count($holiday) > 0) {
          //$response = array('status' => 'failed', 'sync_error' => true, 'message' => 'Call cannot be marked for ' . $activity_date . ', as holiday already marked');
          //return $response;
      //}
		
      //No school visit on the day when leave is marked
      $date_leave = $year.'-'.$month.'-'.$day;
      $leave_count_query = "select count(*) as count from ssc_leaves where user_id= '$user_id' and leave_from_date<=date_format('$date_leave','%Y-%m-%d') and leave_end_date>=date_format('$date_leave','%Y-%m-%d') and status = 'approved'";
      $select_query = $CI->db->query($leave_count_query);
      $row = $select_query->result();
      $leaveCount = $row[0]->count;			
      if($leaveCount > 0){
        $response = array("http_code" => 200, 'local_id' => $android_local_id, 'status' => 'failed', 'sync_error' => true, 'message' => 'Activity day already marked for leave.');
        return $response;
      }

      $CI->load->model("districts_model");
      $data = $CI->districts_model->getById($json['Data']['type_info']['district_id']);
      //$state_id = $data['state_id'];

      $CI->load->model("calls_model");
      $mdata['source'] = $source;
      $mdata['activity_date'] = $activity_date;
      $mdata['user_id'] = $user_id;
      $mdata['state_id'] = $state_id;
      $mdata['district_id'] = $json['Data']['type_info']['district_id'];
      $mdata['cluster_id'] = $json['Data']['type_info']['cluster_id'];
      $mdata['block_id'] = $json['Data']['type_info']['block_id'];
      $mdata['contact_person_id'] = $json['Data']['type_info']['contact_person_id'];

      $CI->calls_model->add($mdata);
      $last_id_p = $CI->db->insert_id();
    
      $response = array("http_code" => 200, 'server_id' => $last_id_p, "sync_error" => false, "message" => "Stored Successfully", 'local_id' => $android_local_id);
      return $response;
  }

  public function leave_activity($json)
  {
    if (isset($json['Data']) and isset($json['Data']['local_id']))
    {
      $android_local_id = $json['Data']['local_id'];
    }
    else
    {
      $android_local_id = 0;
    }
    $CI =& get_instance();
    $source = (isset($json['Data']['source']) ? $json['Data']['source'] : 'app');
    $leave_from_date = date("Y-m-d", strtotime($json['Data']['type_info']['leave_from_date']));
    $leave_end_date = date("Y-m-d", strtotime($json['Data']['type_info']['leave_end_date']));
    $user_id = $this->user_id;
    $state_id = $this->user_state_id;
    $district_id = $this->user_district_id;
    $CI->load->model("leaves_model");
    $holiday = $CI->leaves_model->get_all_applied($user_id, $leave_from_date, $leave_end_date);

    if (count($holiday) > 0) {
        $response = array('status' => 'failed', 'sync_error' => true, 'message' => 'Can not apply leave for ' . $leave_from_date . ', as  already applied');
        return $response;
    }

    //LOAD TIMEOVERLAP MODEL
    $CI->load->model("Timeoverlaptest_model");
    //CHECKING FULL DAY ACTIVITY MARKED -------------
    if (!($CI->Timeoverlaptest_model->checkFullDayActivity($user_id, $leave_from_date, 'LEAVE',$leave_end_date))) {
        $response = array("http_code" => 200, 'local_id' => $android_local_id, 'status' => 'failed', 'sync_error' => true, 'message' => 'Days already marked for activity , can\'t take this request.');
        return $response;
    }
    $mdata['status']          = 'approved';
    $mdata['source']          = $source;
    $mdata['leave_from_date'] = $leave_from_date;
    $mdata['leave_end_date']  = $leave_end_date;
    $mdata['user_id']         = $user_id;
    $mdata['state_id']        = $state_id;
    $mdata['district_id']     = $district_id;
    $mdata['leave_type']      = $json['Data']['type_info']['leave_type'];
    $mdata['total_days']      = $json['Data']['type_info']['total_days'];
    if(isset($json['Data']['type_info']['day_type']))
			$mdata['day_type']      = $json['Data']['type_info']['day_type'];
    if(isset($json['Data']['type_info']['reason']))
			$mdata['reason']      = $json['Data']['type_info']['reason'];

    $CI->leaves_model->add($mdata);
    $last_id_p = $CI->db->insert_id();
    $response = array("http_code" => 200, 'server_id' => $last_id_p, "sync_error" => false, "message" => "Applied Successfully", 'local_id' => $android_local_id);
    return $response;
  }

  public function addContact_post($json)
  {
      error_log("Inside account - contact post method-----", 0);
      if (isset($json['Data']) and isset($json['Data']['local_id']))
      {
        $android_local_id = $json['Data']['local_id'];
      }
      else
      {
        $android_local_id = 0;
      }
      $CI =& get_instance();
      $activity_date = date("Y-m-d", strtotime($json['Data']['date']));
      // if (!$this->validate_activity_date($activity_date) && $mode == 'add')
      //$this->response(array("http_code" => 200, 'local_id' => $android_local_id, 'status' => 'failed', 'sync_error' => true, 'message' => 'Add contact date cannot be in future date or older more than '.MAX_PREVIOUS_DAYS.' days from now.'));
      $user_id = $this->user_id;

      $CI->load->model("holidays_model");
      $CI->load->model("contact_model");

      $holiday = $CI->holidays_model->get_all($user_id, $activity_date, $activity_date);

      if (count($holiday) > 0) {
        $response = array('status' => 'failed', 'sync_error' => true, 'message' => 'contact cannot be marked for ' . $activity_date . ', as holiday already marked');
        return $response;
      }
      $CI->load->model("contact_model");
      //$mdata['id'] = $this . id;
      $mdata['activity_date'] = $activity_date;
      $mdata['user_id'] = $user_id;
      //$mdata['state_id'] = $state_id;
      $mdata['district_id'] = $json['Data']['district_id'];
      $mdata['block_id'] = $json['Data']['block_id'];
      $mdata['cluster_id'] = $json['Data']['cluster_id'];
      $mdata['firstname'] = $json['Data']['firstname'];
      $mdata['lastname'] = $json['Data']['lastname'];
      $mdata['mobile'] = $json['Data']['mobile'];
      $mdata['designation'] = $json['Data']['designation'];
      $mdata['source'] = $json['Data']['source'];

      $last_id_p = $CI->contact_model->add($mdata);

      $response = array("http_code" => 200, 'server_id' => $last_id_p, "sync_error" => false, "contact_id" => $last_id_p, "message" => "Stored Successfully", 'local_id' => $android_local_id);
      return $response;
  }

  public function getcontact_post($json)
  {
      //print_r($json);
      if (isset($json['Data']) and isset($json['Data']['local_id']))
      {
        $android_local_id = $json['Data']['local_id'];
      }
      else
      {
        $android_local_id = 0;
      }
      $district_id = $json['Data']['district_id'];
      $block_id = $json['Data']['block_id'];
      $cluster_id = $json['Data']['cluster_id'];
      //$id = $json['Data']['id'];
      $source = $json['Data']['source'];
      error_log(" -- inside account - getcontact_post dist id=" . $district_id . " blk id=" . $block_id . " clul id=" . $cluster_id . " source=" . $source, 0);
      $this->load->model('contact_model');
      $result = array();
      $result = $this->contact_model->getContacts('', $district_id, $block_id, $cluster_id, $source);
      // print_r($result);
     if(!($source=="BL"||$source=="CL"||$source=="DS"||$source=="bl"||$source=="cl"||$source=="ds") ){
         $this->response(array("http_code" => 200, 'local_id' => $android_local_id, 'message' => 'Provide VAILD source'));
     }else
      if ($result > 0) {
          $this->response(array("http_code" => 402, 'contact_detail' => $result));
      } else {
          $this->response(array("http_code" => 200, 'local_id' => $android_local_id, 'message' => 'You are not authorized to access the app'));
      }
      //$this->response(array("http_code" => 200, 'local_id' => $android_local_id, 'message' => 'Invalid username or password'));
  }

  public function get_activity_list($json)
  {
      $CI =& get_instance();
      if (isset($json['Data']) and isset($json['Data']['local_id']))
      {
        $android_local_id = $json['Data']['local_id'];
      }
      else
      {
        $android_local_id = 0;
      }
      $limit = $json['Data']['limit'];
      $user_id = $this->user_id;
      $state_id = $this->user_state_id;
      $district_id = $this->user_district_id;

      //Get Activity info for school visit
      $CI->load->model("school_visits_model");
      $CI->load->model("assessment_model");
      $school_visit_array = $CI->school_visits_model->getActivityInfoForVisit($user_id, $limit);

      $activity_info = array();
      $activity_info['school_visit'] = array();
      foreach ($school_visit_array as $school_visit) {

          $assessment_array = $CI->assessment_model->getBySchoolVisitId($school_visit->id);
          if (empty($assessment_array))
              break;
          $school_visited['created_at'] = $school_visit->created_at;
          $school_visited['block_id'] = $school_visit->block_id;
          $school_visited['cluster_id'] = $school_visit->cluster_id;
          $school_visited['school_id'] = $school_visit->school_id;
          $school_visited['activity_date'] = $school_visit->activity_date;
          $school_visited['duration'] = $school_visit->duration;
          $school_visited['why'] = $school_visit->why;
          $school_visited['what'] = $school_visit->what;
          $school_visited['subjects'] = array();

          foreach ($assessment_array as $assessment) {
              $subject_array['subject_id'] = $assessment->subject_id;
              $subject_array['trained_teachers'] = $assessment->trained_teachers;
              $subject_array['tlm_usage'] = $assessment->tlm_usage;
              $subject_array['prog_chart_usage'] = $assessment->prog_chart_usage;
              $subject_array['sequence_usage'] = $assessment->sequence_usage;
              array_push($school_visited['subjects'], $subject_array);
          }
          array_push($activity_info['school_visit'], $school_visited);
      }

      //Get Activity info for training 
      $CI->load->model("trainings_model");
      $training_array = $CI->trainings_model->getActivityInfoForTraining($user_id, $limit);

      $activity_info['training'] = array();
      foreach ($training_array as $training) {
          $training_attended['created_at'] = $training->created_at;
          $training_attended['block_id'] = $training->block_id;
          $training_attended['cluster_id'] = $training->cluster_id;
          $training_attended['activity_date'] = $training->activity_date;
          $training_attended['role'] = $training->role;
          $training_attended['training_type'] = $training->training_type;
          $training_attended['training_mode'] = $training->mode;
          $training_attended['duration'] = $training->training_duration;
          $training_attended['no_of_days'] = $training->no_of_days;
          $training_attended['no_of_attendees'] = $training->no_of_teachers_trained;
          $training_attended['training_subject'] = $training->subject_id;
          $training_attended['topic'] = $training->topic;
          $training_attended['agenda'] = $training->agenda;
          $training_attended['location'] = $training->location;

          array_push($activity_info['training'], $training_attended);
      }

      //Get Activity info for meeting 
      $CI->load->model("meetings_model");
      $meeting_array = $CI->meetings_model->getActivityInfoForMeeting($user_id, $limit);

      $activity_info['meeting'] = array();
      foreach ($meeting_array as $meeting) {
          $meeting_attended['created_at'] = $meeting->created_at;
          $meeting_attended['block_id'] = $meeting->block_id;
          $meeting_attended['cluster_id'] = $meeting->cluster_id;
          $meeting_attended['activity_date'] = $meeting->activity_date;
          $meeting_attended['duration'] = $meeting->duration;
          $meeting_attended['meet_with'] = $meeting->meet_with;
          $meeting_attended['is_internal'] = $meeting->is_internal;
          $meeting_attended['contact_person_name'] = $meeting->contact_person_name;
          $meeting_attended['contact_person_number'] = $meeting->contact_person_number;
          $meeting_attended['no_of_invites'] = $meeting->no_of_invites;
          $meeting_attended['no_of_attendees'] = $meeting->no_of_attendees;
          $meeting_attended['meeting_type'] = $meeting->meeting_type;
          $meeting_attended['meeting_mode'] = $meeting->meeting_mode;
          $meeting_attended['subject'] = $meeting->subject;
          $meeting_attended['remarks'] = $meeting->remarks;
          $meeting_attended['agenda'] = $meeting->agenda;
          $meeting_attended['location'] = $meeting->location;

          array_push($activity_info['meeting'], $meeting_attended);
      }

      //Get Activity info for call 
      $CI->load->model("calls_model");
      $call_array = $CI->calls_model->getActivityInfoForCall($user_id, $limit);

      $activity_info['call'] = array();
      foreach ($call_array as $call) {
          $call_attended['created_at'] = $call->created_at;
          $call_attended['block_id'] = $call->block_id;
          $call_attended['cluster_id'] = $call->cluster_id;
          $call_attended['activity_date'] = $call->activity_date;
          $call_attended['contact_person'] = $call->contact_person_id;
          $call_attended['teacher_orientation'] = $call->orientation;
          $call_attended['tlm_usage'] = $call->usage;
          $call_attended['progress_report'] = $call->report;

          array_push($activity_info['call'], $call_attended);
      }

      //Get Activity info for No Visit 
      $CI->load->model("no_visit_days_model");
      $no_visit_array = $CI->no_visit_days_model->getActivityInfoForNoVisit($user_id, $limit);

      $activity_info['no_visit'] = array();
      foreach ($no_visit_array as $no_visit) {
          $no_visit_attended['created_at'] = $no_visit->created_at;
          $no_visit_attended['date_of_no_visit'] = $no_visit->activity_date;
          $no_visit_attended['no_visit_reason'] = $no_visit->no_visit_reason;
          array_push($activity_info['no_visit'], $no_visit_attended);
      }
      $response = array("http_code" => 200, 'activity_info' => $activity_info);
      return $response;
  }
    
  public function get_block_district($id)
  {
    $CI =& get_instance();
    $query = "select state_id, district_id from ssc_blocks where id= '$id'";
		$select_query = $CI->db->query($query);
    $row = $select_query->result();
    return $row;
  }
    
  public function get_travel_mode($travelMode)
  {
		if($travelMode == 'With Other'){
			return '9999';
		}
    $CI =& get_instance();
    $query = "SELECT * FROM `ssc_travels` where travel_mode = '$travelMode' ";
		$select_query = $CI->db->query($query);
    $row = $select_query->row_array();
    return $row['id'];
  }

	public function stay_activity($json)
  {
      if (isset($json['Data']) and isset($json['Data']['local_id']))
      {
        $android_local_id = $json['Data']['local_id'];
      }
      else
      {
        $android_local_id = 0;
      }
      $CI =& get_instance();
      $source = (isset($json['Data']['source']) ? $json['Data']['source'] : 'app');
      $activity_date = $this->convert_datetime($json['Data']['type_info']['date'],$source);
      $mode = (isset($json['Data']['mode']) ? $json['Data']['mode'] : 'add');
      $date_timestamp = strtotime($activity_date);
      $month = date("m",$date_timestamp);
      $year = date("Y",$date_timestamp);
      $day = date('d', $date_timestamp);
      $duration = $json['Data']['type_info']['duration'];
      
      $user_id = $this->user_id;
      $state_id = $this->user_state_id;
      $district_id = $this->user_district_id;
      
      $CI->load->model("Claim_model");
      
      $getActivity = $CI->Claim_model->get_data('stay_activities', array('spark_id'=>$user_id, 'Date(activity_date)'=>date('Y-m-d', strtotime($activity_date))));
      if(count($getActivity) > 0){
				$response = array("http_code" => 200, 'sync_error' => true, 'local_id' => $android_local_id, 'status' => 'failed', 'message' => 'Today stay claim already marked.');
            return $response;
      }
      
      //No school visit on the day when leave is marked
      $date_leave = $year.'-'.$month.'-'.$day;
      $leave_count_query = "select count(*) as count from ssc_leaves where user_id= '$user_id' and leave_from_date<=date_format('$date_leave','%Y-%m-%d') and leave_end_date>=date_format('$date_leave','%Y-%m-%d') and status = 'approved'";
      $select_query = $CI->db->query($leave_count_query);
          $row = $select_query->result();
      $leaveCount = $row[0]->count;			
      if($leaveCount > 0){
        $response = array("http_code" => 200, 'local_id' => $android_local_id, 'status' => 'failed', 'sync_error' => true, 'message' => 'Activity day already marked for leave.');
            return $response;
      }		

      $CI->load->model("Timeoverlaptest_model");
      //CHECKING FULL DAY ACTIVITY MARKED --------------------
      if (!($CI->Timeoverlaptest_model->checkFullDayActivity($user_id, $activity_date, 'productive',null))) {
          $response = array("http_code" => 200, 'local_id' => $android_local_id, 'status' => 'failed', 'sync_error' => true, 'message' => 'Leave, Local-Holiday or No-Visit-Day is already marked : can\'t add activity.');
            return $response;
      }
      if (!$CI->Timeoverlaptest_model->checkAvailability($user_id,$activity_date,$duration)) {
          $response = array("http_code" => 200, 'local_id' => $android_local_id, 'status' => 'failed', 'sync_error' => true, 'message' => 'Activity duration conflicts with another activity, please choose different time slot.');
            return $response;
      }
      
      $stay_with_sparks = (isset($json['Data']['type_info']['stay_with_sparks']) ? $json['Data']['type_info']['stay_with_sparks'] : '');
      $hotel_logo = (isset($json['Data']['type_info']['hotel_logo']) ? $json['Data']['type_info']['hotel_logo'] : '');
      
      $mdata['source'] = $source;
      $mdata['activity_date'] = $activity_date;
      $mdata['spark_id'] = $user_id;
      $mdata['accommodation_type'] = $json['Data']['type_info']['accomodation_type'];
      $mdata['manual_rent'] = (isset($json['Data']['type_info']['manual_rent']) ? $json['Data']['type_info']['manual_rent'] : 0);
      $mdata['stay_reason'] = (isset($json['Data']['type_info']['reason']) ? $json['Data']['type_info']['reason'] : '');
      $mdata['stay_with_sparks'] = $stay_with_sparks;
      
      $mdata['from_lat'] = (isset($json['Data']['type_info']['lat']) ? $json['Data']['type_info']['lat'] : '');
      $mdata['from_lon'] = (isset($json['Data']['type_info']['lon']) ? $json['Data']['type_info']['lon'] : '');
      $mdata['from_address'] = (isset($json['Data']['type_info']['hotel_address']) ? $json['Data']['type_info']['hotel_address'] : '');
      $mdata['from_accuracy'] = (isset($json['Data']['type_info']['accuracy']) ? $json['Data']['type_info']['accuracy'] : '');
      $mdata['captured_address_in'] = (isset($json['Data']['type_info']['captured_address']) ? $json['Data']['type_info']['captured_address'] : '');
      
      $mdata['duration'] = '';
      
      $mdata['created_on'] = date('Y-m-d H:i:s');
      $mdata['created_by'] = $user_id;
      
      
      $last_id_p = $CI->Claim_model->addStayActivity($mdata);
      
      if($stay_with_sparks != '')
      {
				$staywithsparks = json_decode($stay_with_sparks);
				
				foreach($staywithsparks as $staywithspark)
				{
					$sdata['stay_activity_id'] = $last_id_p;
					$sdata['spark_id'] = $staywithspark;
					
					$CI->Claim_model->addStayActivity($sdata, 'stay_activity_sparks');
				}
			}
			
			$logoData['stay_activity_id'] = $last_id_p;
			$logoData['hotel_logo'] = $hotel_logo;
			$CI->Claim_model->addStayActivity($logoData, 'stay_logos');
			
      $no_visit_count = "select count(activity_date) as cnt from ssc_no_visit_days where ssc_no_visit_days.user_id = '$user_id' and month(activity_date) = '$month' and year(activity_date) = '$year' and day(activity_date) = '$day'"; 
      $select_query = $CI->db->query($no_visit_count);
         
      //Execute query 
      $row = $select_query->result();
      // print $row;exit;
      $noVisitCount = $row[0]->cnt;	
      
      if ($noVisitCount>0){
        $delete_query = "delete from ssc_no_visit_days where user_id = '$user_id' and month(activity_date) = '$month' and year(activity_date) = '$year' and day(activity_date) = '$day'";
        $select_query = $CI->db->query($delete_query);
      }

      $response = array("http_code" => 200, 'server_id' => $last_id_p, "sync_error" => false, "message" => "Stored Successfully", 'local_id' => $android_local_id);
      return $response;
  }

}

?>
