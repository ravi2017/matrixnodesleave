<?php

function sso_token() {
  $CI =& get_instance();
  $sso_data = $CI->session->userdata('logged_in');
  return $sso_data['sso_token'];
}

function role_name($role)
{
  switch ($role)
  {
    case 'super_admin': return "Super Admin";
    case 'report_user': return "Full Inventory Report Access";
    case 'inventory_admin': return "Inventory Admin";
    case 'sampark_admin': return "Sampark Admin";
    case 'field_user': return "Spark";
    case 'state_person': return "State Person";
    case 'store_head': return "Store Head";
    case 'employee': return "Employee";
    default: return $role;
  }
}

function get_my_team($id)
{
  $CI =& get_instance();
  $CI->load->model('team_members_model');
  $data = $CI->team_members_model->get_all_team_members($id);
  return $data;
}

function get_states()
{
  $CI =& get_instance();
  $CI->load->model('states_model');
  $data = $CI->states_model->get_all();
  return $data;
}

function get_school_info($sid)
{
  $CI =& get_instance();
  $CI->load->model('schools_model');
  $data = $CI->schools_model->get_schools_details($sid);
  return $data;
}

function get_block_details($bid)
{
  $CI =& get_instance();
  $CI->load->model('blocks_model');
  $data = $CI->blocks_model->getById($bid);
  return $data;
}

function get_district_details($did)
{
  $CI =& get_instance();
  $CI->load->model('districts_model');
  $data = $CI->districts_model->getById($did);
  return $data;
}

function get_cluster_details($cid)
{
  $CI =& get_instance();
  $CI->load->model('clusters_model');
  $data = $CI->clusters_model->getById($cid);
  return $data;
}

function get_contact_details($contact_id)
{
  $CI =& get_instance();
  $CI->load->model('contact_model');
  $data = $CI->contact_model->geContacttById($contact_id);
  return $data;
}

function get_state_info($sid)
{
  $CI =& get_instance();
  $CI->load->model('states_model');
  $data = $CI->states_model->get_by_ids($sid);
  return $data;
}

function get_class_info($cid)
{
  $CI =& get_instance();
  $CI->load->model('classes_model');
  $data = $CI->classes_model->getByName($cid);
  return $data;
}

function get_class_by_id($cid)
{
  $CI =& get_instance();
  $CI->load->model('classes_model');
  $data = $CI->classes_model->getById($cid);
  return $data;
}


function get_subjects_info($sids)
{
  $CI =& get_instance();
  $CI->load->model('subjects_model');
  $data = $CI->subjects_model->getByIds($sids);
  return $data;
}

function get_class_state_subject_ids($class_id,$state_id)
{
  $CI =& get_instance();
  $CI->load->model('observations_model');
  $data = $CI->observations_model->get_mapping($class_id,$state_id);
  $subjects = array();
  foreach($data as $d)
  {
    array_push($subjects,$d->subject_id);
  }
  return $subjects;
}


function get_activity_type($activity_type)
{
    if(strtolower($activity_type) == 'review_meeting' || strtolower($activity_type) == 'meeting'){
      $activity_type = 'Meeting';
    }
    else if(strtolower($activity_type) == 'schoolvisit'){
      $activity_type = 'School Visit';
    }
    else if(strtolower($activity_type) == 'training'){
      $activity_type = 'Training';
    }
    else if(strtolower($activity_type) == 'attendance-in'){
      $activity_type = 'Attendance-IN';
    }
    else if(strtolower($activity_type) == 'attendance-out'){
      $activity_type = 'Attendance-OUT';
    }
    return $activity_type;
}


function get_activity_data($activity_id, $activity_type)
{
  $data = array();
  $data['name'] = "";

  $CI =& get_instance();
  if(strtolower($activity_type) == 'review_meeting' || strtolower($activity_type) == 'meeting'){
    $CI->load->model('Meetings_model');
    $result = $CI->Meetings_model->getById($activity_id);
    if(!empty($result)){
      $data['duration'] = $result['duration'];
      $data['travel_mode_id'] = (isset($result['travel_mode_id']) ? $result['travel_mode_id'] : 0);
      $data['travel_cost'] = (isset($result['travel_cost']) ? $result['travel_cost'] : 0);
      $data['user_id'] = (isset($result['user_id']) ? $result['user_id'] : 0);
    }
    else{
      $data['duration'] = 0;
      $data['travel_mode_id'] = 0;
      $data['travel_cost'] = 0;
      $data['user_id'] = 0;
    }
  }
  else if(strtolower($activity_type) == 'schoolvisit'){
    $CI->load->model('School_visits_model');
    $result = $CI->School_visits_model->getById($activity_id);
    if(!empty($result)){
      $data['duration'] = $result['duration'];
      $data['name'] = $result['school_name']." (DISE CODE : ".$result['dise_code'].")";
      $data['travel_mode_id'] = (isset($result['travel_mode_id']) ? $result['travel_mode_id'] : 0);
      $data['travel_cost'] = (isset($result['travel_cost']) ? $result['travel_cost'] : 0);
      $data['user_id'] = (isset($result['user_id']) ? $result['user_id'] : 0);
    }
    else{
      $data['duration'] = 0;
      $data['travel_mode_id'] = 0;
      $data['travel_cost'] = 0;
      $data['user_id'] = 0;
    }
  }
  else if(strtolower($activity_type) == 'training'){
    $CI->load->model('Trainings_model');
    $result = $CI->Trainings_model->getById($activity_id);
    if(!empty($result)){
      $data['duration'] = $result['training_duration'];
      $data['travel_mode_id'] = (isset($result['travel_mode_id']) ? $result['travel_mode_id'] : 0);
      $data['travel_cost'] = (isset($result['travel_cost']) ? $result['travel_cost'] : 0);
      $data['user_id'] = (isset($result['user_id']) ? $result['user_id'] : 0);
    }
    else{
      $data['duration'] = 0;
      $data['travel_mode_id'] = 0;
      $data['travel_cost'] = 0;
      $data['user_id'] = 0;
    }
  }
  else if(strtolower($activity_type) == 'attendance-out'){
    $CI->load->model('Attendance_model');
    $result = $CI->Attendance_model->getById($activity_id);
    if(!empty($result)){
      $data['duration'] = (isset($result['training_duration']) ? $result['training_duration'] : 0);
      $data['travel_mode_id'] = (isset($result['travel_mode_id']) ? $result['travel_mode_id'] : 0);
      $data['travel_cost'] = (isset($result['travel_cost']) ? $result['travel_cost'] : 0);
      $data['user_id'] = (isset($result['spark_id']) ? $result['spark_id'] : 0);
    }
    else{
      $data['duration'] = 0;
      $data['travel_mode_id'] = 0;
      $data['travel_cost'] = 0;
      $data['user_id'] = 0;
    }
  }
  else if(strtolower($activity_type) == 'distance_travel'){
    $CI->load->model('Claim_model');
    $result = $CI->Claim_model->getTravelById($activity_id);
    if(!empty($result))
      $data['duration'] = $result['duration'];
    else
      $data['duration'] = 0;
  }
  return $data;
}

function get_activity_complete_data($activity_id, $activity_type)
{
  $CI =& get_instance();
  $result = array();
  if(strtolower($activity_type) == 'distance_travel'){
    $CI->load->model('Claim_model');
    $result = $CI->Claim_model->getTravelById($activity_id);
  }
  return $result;
}

function get_tracking_data($tracking_id)
{
   $CI =& get_instance();
   $CI->load->model('tracking_model');
   $trackingData = $CI->tracking_model->getById($tracking_id); 
   
   return $trackingData;
}


function getAttendanceData($spark_id, $date, $status = 'in',$order = 'tracking_time desc',$limit = '')
{
  $CI =& get_instance();
  $CI->load->model('tracking_model');
  $trackingData = $CI->tracking_model->getAttendance($spark_id, $date, $status, $order, $limit);
  
  return $trackingData;
}

function retrun_rating($subject_total, $subject_yes)
{
  $subject_rate = '--';
  $subject_rate_value = 0;
  if($subject_total > 0){
    if($subject_yes >0){
       $subject_rate_value = round(($subject_yes/$subject_total)*100);
       if($subject_rate_value > 75){
         $subject_rate = 'A';
       }else if($subject_rate_value <= 75 && $subject_rate_value >= 51){
         $subject_rate = 'B';
       }else{
         $subject_rate = 'C';
       }
    }else{
      $subject_rate = 'C';
    }
  }
  //return $subject_rate."(".$subject_rate_value."%) - ".$subject_yes." : ".$subject_total;
  return $subject_rate;
}

function show_training_notification($state_id)
{
  $CI =& get_instance();
  $CI->load->model('Trainings_model');
  $data = $CI->Trainings_model->check_future_trainings($state_id);
  if ($data == 1)
  {
    $url = INVENTORY_DOMAIN."?token=".sso_token();
    echo '<div class="alert alert-info icons-alert"><p><strong>Trainings Opened</strong>. <a href="'.$url.'" target="_blank">Click here to raise your request</a></p></div>';
  }
}

function show_360_url($current_role, $current_user_id, $user_state_id)
{
    $CI =& get_instance();
    $CI->load->model('Sparks_model');
    $data = $CI->Sparks_model->getById($current_user_id);
    $spark_id = $data['login_id'];
    
    $login_id = rtrim(strtr(base64_encode($spark_id), '+/', '-_'), '=');
    if($current_role == 'field_user') {
        $CI =& get_instance();
        $CI->load->model('states_model');
        $data = $CI->states_model->get_by_ids($user_state_id);
        $contact_name = $data[0]->name;
    }
    else if($current_role == 'manager' || $current_role == 'state_person'){
        $contact_name = 'HOSH';
    } 
    $decoded_val = rtrim(strtr(base64_encode($contact_name), '+/', '-_'), '=');
    $newurl = 'http://feedback.samparksmartshala.org/index.php?t=dHlwZTI&q='.$decoded_val.'&mac='.$login_id;
    
    echo '<div class="alert alert-info icons-alert"><a href="'.$newurl.'" target="_blank">Click here for 360° LEADERSHIP FEEDBACK</a></div>';
    
    //$newurl = 'http://sparkactivity.samparksmartshala.org/reports/feedback_results';
    //echo '<div class="alert alert-info icons-alert"><a href="'.$newurl.'">Click here for 360° Leadership Survey Result</a></div>';
    
}


function get_spark_districts($spark_id)
{
  $CI =& get_instance();
  $CI->load->model('Sparks_model');
  $data = $CI->Sparks_model->user_distinct_districts($spark_id);
  
  $district_name = array();
  foreach($data as $distict_data)
  {
    $district_name[] = $distict_data->name;
  }
  return $district_name;
}



function return_rating_val($rating_val, $score = 0, $type='')
{
  $rating = '--';
  if($score > 0){
    if((int) $rating_val >= 9999){
      return '--';
    }
    if($rating_val < 1)
    {
      $rating = ($type == 'sm' ? 'O' : 'OUTSTANDING');
    }
    else if($rating_val >=1 && $rating_val < 2)
    {
      $rating = ($type == 'sm' ? 'G' : 'GOOD');
    }
    else if($rating_val >= 2)
    {
      $rating = ($type == 'sm' ? 'P' : 'POOR');
    }
  }
  return $rating;
}

function return_rating_val_summary($rating_val, $score = 0)
{
  $rating_data = array('value'=>'--', 'color'=>'');
  if($score > 0){
    if((int) $rating_val >= 9999){
      return $rating_data;
    }
    if($rating_val < 1)
    {
      $rating['value'] = 'O';
      $rating['color'] = '';
    }
    else if($rating_val >=1 && $rating_val < 2)
    {
      $rating['value'] = 'G';
      $rating['color'] = '';
    }
    else if($rating_val >= 2)
    {
      $rating['value'] = 'P';
      $rating['color'] = 'score-red';
    }
  }
  return $rating;
}


function check_ranking($rank)
{
  if((int) $rank >= 9999){
    return '--';
  }
  else{
    return (int) $rank;
  }
}

function return_value_percent($percent_value, $current_value)
  {
    if($current_value > 0)
      $net_value = round(($current_value*$percent_value)/100);
    else  
      $net_value = 0;
    
    return $net_value;  
  }

function return_field_rating($user_id, $month_year, $field_name, $duration, $role, $sel_spark)
{
  $CI =& get_instance();
  $CI->load->model('Dashboard_model');
  $data = $CI->Dashboard_model->get_state_person_meeting_data($field_name, $month_year, $duration, $role); 
  
  $returnt_value = '--';
  $rating_array = array();
  if(!empty($data)){
    $countData = count($data);
    $max_value = $data[$countData-1]->$duration;
    
    $val_25 = return_value_percent(25, $max_value);
    $val_75 = return_value_percent(75, $max_value);
    
    foreach($data as $dataVal)
    {
      $current_value = $dataVal->$duration;
      if($current_value <= $val_25)
      {
        $value = 'Poor'; //0
      }
      else if($current_value > $val_75){
        $value = 'Outstanding'; //2
      }
      else{
        $value = 'Good'; //1
      }
      $returnt_value = $returnt_value." >> ".$current_value;
      $rating_array[$dataVal->spark_id] = $value;
    }
    $returnt_value = $rating_array[$sel_spark];
  }
  return $returnt_value;
}


function return_percent($net_value, $current_value)
{
  if($net_value > 0)
    $percent = round(($current_value*100)/$net_value);
  else  
    $percent = 0;
  
  return $percent;  
}


function get_spark_data($id)
{
  $CI =& get_instance();
  $CI->load->model('Users_model');
  $data = $CI->Users_model->getById($id);
  return $data;
}


function get_option_order_value($ques_id, $order)
{
  $CI =& get_instance();
  $CI->load->Model('Observations_model');
  
  $options = $CI->Observations_model->getObtionsByObservation($ques_id, '', '1');
  //print_r($options);
  return $options[$order-1]->option_value;
  
}


function return_index_color($color_val)
{
  $color = "";
  if($color_val == 1){
    $color = 'background-color:#44e287'; // green
  }
  else if($color_val == 2){
    $color = 'background-color:#fd6252'; // red
  }
  else if($color_val == 3){
    $color = 'background-color:#ffff00'; // yellow
  }
  return $color;
}

function get_survey_data($id)
{
  $CI =& get_instance();
  $CI->load->model('Survey_model');
  $data = $CI->Survey_model->getById($id);
  return $data;
}

function get_upload_file($data)
{
  $CI =& get_instance();
  $CI->load->model('Claim_model');
  $data = $CI->Claim_model->get_claim_proof($data);
  return $data;
}

function get_baithak_weitage_value($field, $value)
  {
		switch($field)
		{
			case ('school_registered_rank' || 'school_registered'):
				$WEIGHTAGE = 4;	//for 25% we take 4
				break;
			case ('registered_teacher_rank' || 'registered_teacher'):
				$WEIGHTAGE = 4;
				break;
			case ('active_greater_1hr_rank' || 'active_greater_1hr'):
				$WEIGHTAGE = 4;
				break;
			case ('spark_engagement_rank' || 'spark_engagement'):
				$WEIGHTAGE = 4;
				break;
			default:  
				$WEIGHTAGE = 1;
		}
		if($value > 0)
      $number = $value/$WEIGHTAGE;
    else  
      $number = 0;
      
    return $number;
	}


function  getCurrentSparkFromState($current_role = '', $sdate = '', $edate = '', $stateid='', $current_group='', $current_user_id=''){
		
		$CI =& get_instance();	
	
    $CI->load->model('Users_model');   
    $state = $stateid;
    $sdate = ($sdate !=''  ? date('Y-m-d', strtotime($sdate)) : '');
    $edate = ($edate !=''  ? date('Y-m-d', strtotime($edate)) : '');
    
    $row_set= array();    
    if($current_role == 'super_admin' || $current_role == 'admin' || $current_role == 'HR' || $current_role == 'reports'){
			$user_group = array('field_user');  
		}else{
			$user_group = array($current_group);
		}
    
    $spark_role = array('field_user', 'manager', 'state_person');  
    if($current_role == "state_person" || $current_role == "manager"){
      //$row1['name'] = $CI->data['current_name']." (".$CI->data['login_id'].")";
      //$row1['login_id'] = $CI->data['login_id'];
      //$row1['state_id'] = $CI->data['user_state_id'];
      //$row1['role'] = $CI->data['current_role'];
      //$row1['id'] = $CI->data['current_user_id'];
      //$row_set[] = $row1;
      
      $spark_role = array('field_user', 'manager');  
    }
    
    if($state == 'HO')
    {
			$state = 0;
			//$spark_role = array('manager', 'accounts', 'executive', 'HR', 'field_user');  
			$spark_role = array('field_user', 'manager', 'state_person'); 
			$user_group = array('head_office');  
		}
		
		$sparkids = array();
		if($sdate != '' && $edate != ''){
			if($current_group == 'head_office' && ($current_role == "state_person" || $current_role == "manager")){
					$spark_role = array('field_user', 'manager', 'state_person');
					$user_group = array('head_office', 'field_user'); 
					$users_list = $CI->Users_model->getAllHOTeam($current_user_id, $sdate, $edate, $spark_role, $user_group);
			}
			else{
					$users_list = $CI->Users_model->getAllSparksByState($state, $sdate, $edate, '', $spark_role, $user_group);
			}
			if(count($users_list) > 0){
				foreach ($users_list as $row){
					if(!in_array($row['login_id'], $sparkids) && $row['state_id'] != '0'){
							$row1['name'] = $row['name'];
							$row1['login_id'] = $row['login_id'];
							$row1['state_id'] = $row['state_id'];
							$row1['role'] = $row['role'];
							$row1['id'] = $row['user_id'];
							$row_set[] = $row1;
							array_push($sparkids, $row['login_id']);							
					}
					
				}
			}
		}else{
			if($current_group == 'head_office' && ($current_role == "state_person" || $current_role == "manager")){
					$spark_role = array('field_user', 'manager', 'state_person');
					$user_group = array('head_office', 'field_user'); 
					$users_list = $CI->Users_model->getAllHOTeam($current_user_id, '', '', $spark_role, $user_group);
			}
			else{
					$users_list = $CI->Users_model->getSparksByCurrentState($state, $spark_role, $user_group);
			}
			if(count($users_list) > 0){
				foreach ($users_list as $row){
					if(!in_array($row['login_id'], $sparkids) && $row['state_id'] != '0'){
						$row1['name'] = $row->name;
						$row1['login_id'] = $row->login_id;
						$row1['state_id'] = $row->state_id;
						$row1['role'] = $row->role;
						$row1['id'] = $row->id;					
						$row_set[] = $row1;
						array_push($sparkids, $row['login_id']);							
					}
				}
			}
		}
    if(!empty($row_set))
     return $row_set;
     //return json_encode($row_set);
  }
